<?php
namespace api\models;

use Yii;
use yii\base\Model;
use common\models\User;

/**
 * Login form
 */
class ApiLoginForm extends Model
{
    public $tel_no;
    public $password;
    public $rememberMe = true;
    public $store_id;

    private $_user;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // username and password are both required
            [['tel_no', 'password'], 'required'],
            // rememberMe must be a boolean value
            ['rememberMe', 'boolean'],
            // password is validated by validatePassword()
            ['password', 'validatePassword'],
        ];
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();
            if (!$user || !$user->validatePassword($this->password)) {
                $this->addError($attribute, '電話番号またはパスワードが正しくありません。');
            }
        }
    }

    /**
     * Logs in a user using the provided username and password.
     *
     * @return boolean whether the user is logged in successfully
     */
    public function login($user = null)
    {   
        if($user != null){
            return Yii::$app->user->login($user, $this->rememberMe ? 3600 * 24 * 30 : 0);
        }
        
        if ($this->validate()) {
            return Yii::$app->user->login($this->getUser(), $this->rememberMe ? 3600 * 24 * 30 : 0);
        } else {
            return false;
        }
    }
    
    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'tel_no' => '電話番号',
            'password' => 'パスワード',
            'rememberMe' => '情報を保持する。'
        ];
    }

    /**
     * Finds user by [[username]]
     *
     * @return User|null
     */
    protected function getUser()
    {
        if ($this->_user === null) {
            $this->_user = User::findByTelno($this->tel_no);
            if(!empty($this->store_id) && !empty($this->_user)){
                $customer = \common\models\MasterCustomer::findOne(['user_id' => $this->_user->id]);
                $customer_store = \common\models\CustomerStore::findOne(['store_id' => $this->store_id, 'customer_id' => $customer->id]);
                if(empty($customer_store)){
                    $customer_store = new \common\models\CustomerStore();
                    $customer_store->store_id = $this->store_id;
                    $customer_store->customer_id = $customer->id;
                    $customer_store->rank_id = '00';
                    $customer_store->black_list_flg = '0';
                    $customer_store->updated_by = $customer->id;
                    $customer_store->created_by = $customer->id;
                    $customer_store->detachBehavior('BlameableBehavior');
                    $customer_store->save();
                }
            }
        }

        return $this->_user;
    }
}
