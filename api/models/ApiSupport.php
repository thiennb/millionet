<?php

namespace api\models;
use common\models\User;
use common\models\MasterCustomer;
use api\models\ApiLoginForm;
use yii\base\Model;
    
class ApiSupport extends Model{
        /*
         * Get token by login method
         * 
         * @param int $id
         * @param int $endtime
         * @return string
         */
        public static function getApiToken($id, $endtime){
                $date = date('Y/m/d H:i:s');
                $rand = rand(10000,99999);
                $code = (strtotime($date) - $endtime)*$rand*$id;
                $token = base64_encode("rand={$rand};code={$code};id={$id};scope={$endtime};datetime={$date}");
                return $token;
        }
        
        /*
         * is Valid Api Token method
         * 
         * @param string $token
         * @return boolean
         */
        public static function isValidApiToken($token){
                if(!$token){
                        return false;
                }
                
                $tokenInfo = base64_decode($token);
                $tokenDetail = explode(';', $tokenInfo);
                
                $tokenObject = [];
                foreach($tokenDetail as $value){
                        list($k, $v) = explode('=', $value);
                        $tokenObject[$k] = $v;
                }
                
                // Get second of token
                $seconds = strtotime($tokenObject['datetime']);
                $maxSeconds = $seconds + $tokenObject['scope'];
                $currentSeconds = strtotime(date('Y/m/d H:i:s'));
                if($currentSeconds > $maxSeconds){
                        return false;
                }
                
                // Check rand token
//                if(($tokenObject['code']/$tokenObject['rand']/$tokenObject['id']) != ($seconds - $tokenObject['scope'])){
//                        return false;
//                }
                
                return (object)$tokenObject;
        }
        
        /*
         * is device agent switch method
         * @param string $device
         */
        public static function isAccessDevice($device){
                //"iPod", "iPhone", "iPad", "Android", "webOS"
                if(empty($_SERVER['HTTP_USER_AGENT'])){
                    return false;
                }
                
                if( stripos($_SERVER['HTTP_USER_AGENT'], $device) ){
                        return true;
                }
                
                return false;
        }
        
        /*
         * is valid header authentication method
         * 
         * @return boolean
         */
        public static function isValidHeaderAuthentication($token){
                // Get token from header
                return static::isValidApiToken($token);
        }
        
        /*
         * is valid header authentication method
         * 
         * @return boolean
         */
        public static function isValidParameterAuthentication(){
                // Get token from header
                $token = \Yii::$app->request->get('token');
                return static::isValidApiToken($token);
        }
        
        /*
         * isCustomerLogged
         * 
         * @return boolean
         */
        public static function isCustomerLogged(){
                // Get token object
                $token = \Yii::$app->request->headers->get('Authorization');
                $tokenObject = ApiSupport::isValidHeaderAuthentication($token);
                
                if(!$tokenObject){
                        return null;
                }
                
                return MasterCustomer::find()->where(['user_id' => $tokenObject->id])->one();
        }
        
        public static function isLoginOnWebview($token){            
            $login = new ApiLoginForm();
            
            $tokenObject = static::isValidApiToken($token);
            $user = User::find()->where(['id' => $tokenObject->id])->one();
            $login->login($user, 3600 * 24 * 30);

            
            return MasterCustomer::find()->where(['user_id' => $tokenObject->id])->one();
        }
        
        /*
         * Get barcode url
         * 
         * @return string barcode
         */
        public static function getBarcodeUrl($janCode, $ext = 'png'){
            return static::getProtocal() . $_SERVER['HTTP_HOST'] .  \Yii::$app->homeUrl . 'general/barcode/' . $janCode . ($ext ? ".{$ext}" : '');
        }
        
        /*
         * Get file full url
         * 
         * @return url string
         */
        public static function getFileFullUrl($fileName){
            if(!$fileName){
                    return $fileName;
            }

            return static::getProtocal() . $_SERVER['HTTP_HOST'] .  '/api/uploads/'.$fileName;
        }
        
        /*
         * Get file full url
         * 
         * @return url string
         */
        public static function getRankUrl($fileName){
            if(!$fileName){
                    return $fileName;
            }

            return static::getProtocal() . $_SERVER['HTTP_HOST'] .  '/api/rank/'.$fileName;
        }
        
        /**
         *  Get protocal of request
         *  @return string
         */
        public static function getProtocal(){
            return (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
        }
}