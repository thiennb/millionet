<?php

namespace api\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii2tech\ar\softdelete\SoftDeleteBehavior;
use yii\behaviors\BlameableBehavior;
use common\components\Util;
use common\components\Constants;
use common\models\CustomerStore;
use common\models\User;

/**
 * This is the model class for table "mst_member".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $first_name
 * @property string $last_name
 * @property string $first_name_kana
 * @property string $last_name_kana
 * @property string $birth_date
 * @property string $sex
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 * @property string $del_flg
 *
 * @property User $user
 */
class MasterMember extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'mst_customer';
    }

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            TimestampBehavior::className(),
//            BlameableBehavior::className(),
            'softDeleteBehavior' => [
                'class' => SoftDeleteBehavior::className(),
                'softDeleteAttributeValues' => [
                    'del_flg' => '1'
                ],
            ],
        ];
    }

    public $phone_1;
    public $phone_2;
    public $phone_3;
    public $key_confirm;
    public $key_confirm_again;
    public $password;
    public $password_re;
    public $password_old;
    public $email_re;
    public $store_id;
    public $mode;

    const REGISTER_PHONE = 1;
    const REGISTER_EMAIL = 2;

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['first_name', 'last_name', 'first_name_kana', 'last_name_kana', 'password', 'password_re'], 'required', 'on' => 'step_3'],
            [['first_name', 'last_name', 'first_name_kana', 'last_name_kana', 'password', 'password_re'], 'required', 'on' => 'step_3_email'],
            [['first_name', 'last_name', 'first_name_kana', 'last_name_kana'], 'required', 'on' => 'update'],
            [['email'], 'required', 'on' => 'step_1_email'],
            [['email'], 'string', 'max' => 60],
            [['key_confirm_again', 'updated_by'], 'safe'],
            ['key_confirm', 'required', 'on' => 'step_2'],
            [['password', 'key_confirm', 'password_re'], 'string', 'min' => 6, 'message' => Yii::t('backend', "The number of digits is not enough"),],
            [['password', 'key_confirm', 'password_re'], 'string', 'max' => 20],
            [['last_name_kana', 'first_name_kana'], 'match', 'pattern' => '/^([ｧ-ﾝﾞﾟァ-ヺ・ーヽヾヿ]+)$/u', 'message' => Yii::t('backend', 'Please input Kana character.')],
            [['birth_date'], 'date', 'format' => 'yyyy/MM/dd', 'message' => Yii::t('backend', "Date format don't match"), 'skipOnEmpty' => true],
            [['email', 'email_re'], 'checkEmail'],
            [['first_name', 'last_name', 'first_name_kana', 'last_name_kana'], 'string', 'max' => 40],
            [['phone_1', 'phone_2', 'phone_3', 'store_id', 'updated_by'], 'integer'],
            [['phone_1'], 'string', 'min' => 3, 'max' => 3],
            [['phone_2', 'phone_3'], 'string', 'min' => 4, 'max' => 4],
            [['phone_1', 'phone_2', 'phone_3'], 'required', 'on' => 'create_phone'],
            [['sex'], 'string', 'max' => 1],
//            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
            ['password_re', 'compare', 'compareAttribute' => 'password', 'message' => Yii::t('backend', "Input content does not match"), 'skipOnEmpty' => false],
            ['email_re', 'compare', 'compareAttribute' => 'email', 'message' => Yii::t('backend', "Email don't match"), 'skipOnEmpty' => false, 'on' => 'step_3'],
            [['phone_1', 'phone_2', 'phone_3'], 'required', 'on' => 'update_phone'],
            [['email', 'email_re'], 'required', 'on' => 'update_email'],
            [['password', 'password_re', 'password_old'], 'required', 'on' => 'update_password'],
            [['password_re', 'password'], 'match', 'pattern' => "/^([a-zA-Z0-9!@#$%^&*]+)$/u", 'message' => Yii::t('backend', 'Please enter alphanumeric symbols.')],
            [['post_code'], 'string', 'max' => 7, 'min' => 7],
            [['prefecture'], 'string', 'max' => 80],
            [['address',], 'string', 'max' => 240],
//            [['birth_date',],'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => Yii::t('backend', 'ID'),
            'user_id' => Yii::t('backend', 'User ID'),
            'first_name' => Yii::t('backend', 'First name'),
            'last_name' => Yii::t('backend', 'Last name'),
            'first_name_kana' => Yii::t('backend', 'First name kana'),
            'last_name_kana' => Yii::t('backend', 'Last name kana'),
            'birth_date' => Yii::t('backend', 'Birthday'),
            'sex' => Yii::t('backend', 'Sex'),
            'created_at' => Yii::t('backend', 'Created At'),
            'updated_at' => Yii::t('backend', 'Updated At'),
            'del_flg' => Yii::t('backend', 'Del Flg'),
            'email' => Yii::t('backend', 'Email'),
            'email_re' => Yii::t('backend', 'Email Re'),
            'password' => '※ パスワード', //Yii::t('backend', 'Password'),
            'password_re' => '※ パスワード確認', //Yii::t('backend','Password Re'),
            'phone_1' => '※ 電話番号',
            'phone_2' => '※ 電話番号',
            'phone_3' => '※ 電話番号',
            'key_confirm' => '※ 認証キー',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser() {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public static function find() {
        return parent::find()->where(['mst_customer.del_flg' => '0']);
    }

    /**
     * Save data member register
     * @return none
     */
    public function saveData(MasterMember $model = null) {
        //save user
        $user = new User();
        $user->tel_no = $this->phone_1 . $this->phone_2 . $this->phone_3;
        $user->setPassword($model->password);
        $user->generateAuthKey();
        $user->save();

        $store_id = $this->store_id;

        Yii::$app->user->login($user, 3600 * 24 * 30);

        //save member
        $member = new MasterMember();
        $member = $model;
        $member->customer_jan_code = Util::genJanCodeAuto(Constants::JAN_TYPE_CUSTORMER);
        $member->user_id = $user->id;
        $member->save();

        if (!empty($store_id)) {
            $customer_store = new CustomerStore();
            $customer_store->store_id = $store_id;
            $customer_store->customer_id = $member->id;
            $customer_store->save();
        }
    }

    /**
     * Send mail after register success
     * @return none
     */
    public function sendEmailRegister(MasterMember $model = null) {
        Yii::$app->mailer->compose(
                        ['html' => 'registerMember-html', 'text' => 'registerMember-text'], ['model' => $model]
                )
                ->setFrom('amslaypham@gmail.com')
                ->setTo($model->email)
                ->setSubject('Message subject')
                ->send();
    }

    /**
     * Process before save into database
     * @return none
     */
    public function beforeSave($insert) {
        if (parent::beforeSave($insert)) {
            (!empty($this->phone_1) && !empty($this->phone_2) && !empty($this->phone_3)) ? $this->mobile = $this->phone_1 . $this->phone_2 . $this->phone_3 : $this->mobile;

            $this->updated_by = 0;
            return true;
        } else {
            return false;
        }
    }

    /**
     * Check email
     * @param string $attribute, $params
     * @return error 
     * @throws 
     */
    public function checkEmail($attribute, $params) {

        $check = preg_match(
                '/^[A-z]+[.A-z0-9_\-]+[@][A-z0-9_\-]+([.][A-z0-9_\-]+)+[A-z.]{2,4}$/', $this->$attribute
        );

        //check valid email
        if (!$check) {
            $key = $attribute;
            //add message error
            $this->addError($key, Yii::t('backend', "email not valid"));
        }
    }

    /**
     * Send mail after register success
     * @return none
     */
    public function sendKeyConfirmEmail(MasterMember $model = null) {
        Yii::$app->mailer->compose(
                        ['html' => 'registerMemberKeyConfirm-html', 'text' => 'registerMemberKeyConfirm-text'], ['model' => $model]
                )
                ->setFrom('amslaypham@gmail.com')
                ->setTo($model->email)
                ->setSubject('Message subject')
                ->send();
    }

}
