<?php
namespace api\models;

use yii\base\Model;
use common\models\User;
use common\models\MasterCustomer;
use Yii;
/**
 * Signup form
 */
class ApiRegisterForm extends Model
{
    public $username;
    public $email;
    public $password;
    public $first_name;
    public $last_name;
    public $first_name_kana;
    public $last_name_kana;
    public $mobile;
    public $confirm_password;

        /**
         * @inheritdoc
         */
        public function rules()
        {
            return [
                ['username', 'trim'],
                ['username', 'required'],
                ['username', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This username has already been taken.'],
                ['username', 'string', 'min' => 2, 'max' => 255],

                ['password', 'required'],
                ['password', 'string', 'min' => 6],
                
                ['confirm_password', 'required'],
                ['confirm_password', 'string', 'min' => 6],
                
                ['first_name_kana', 'required'],
                ['first_name_kana', 'string', 'max' => 20],
                
                ['last_name_kana', 'required'],
                ['last_name_kana', 'string', 'max' => 20],
                
                ['first_name', 'required'],
                ['first_name', 'string', 'max' => 20],
                
                ['last_name', 'required'],
                ['last_name', 'string', 'max' => 20],
                
                ['mobile', 'required'],
                ['mobile', 'string', 'max' => 20],
                
                ['email', 'trim'],
                ['email', 'required'],
                ['email', 'email'],
                ['email', 'string', 'max' => 255],
                ['email', 'unique', 'targetClass' => '\common\models\MasterCustomer', 'message' => 'This email address has already been taken.'],
            ];
        }
        
        /**
        * @inheritdoc
        */
       public function attributeLabels()
       {
           return [
               'username' => Yii::t('backend', 'Username'),
               'password' => Yii::t('backend', 'Password'),
               'confirm_password' => Yii::t('backend', 'Password Re'),
               'first_name_kana' => Yii::t('backend', 'First name kana'),
               'last_name_kana' => Yii::t('backend', 'Last name kana'),
               'first_name' => Yii::t('backend', 'First name'),
               'last_name' => Yii::t('backend', 'Last name'),
               'mobile' => Yii::t('backend', 'Mobile'),
               'email' => Yii::t('backend', 'Email')
           ];
       }

        /**
         * Signs user up.
         *
         * @return User|null the saved model or null if saving fails
         */
        public function signup()
        {
                try{
                        if (!$this->validate()) {

                            return null;
                        }

                        $user = new User();
                        $user->username = $this->username;
                        $user->setPassword($this->password);
                        $user->generateAuthKey();
                        $date = strtotime(date('Y-m-d H:i:s'));
                        $user->created_at = $date;
                        $user->updated_at = $date;
                        $user->created_by = 0;
                        $user->updated_by = 0;
                        $user->save();
                        
                        if($user->save()){
                                // Insert to customer table
                                $customer = new MasterCustomer();                                
                                $customer->email = $this->email;
                                $customer->user_id = $user->id;
                                $customer->first_name = $this->first_name;
                                $customer->last_name = $this->last_name;
                                $customer->first_name_kana = $this->first_name_kana;
                                $customer->last_name_kana = $this->last_name_kana;
                                $customer->mobile = $this->mobile;
                                $customer->created_at = $date;
                                $customer->updated_at = $date;
                                $customer->created_by = 0;
                                $customer->updated_by = 0;
                                $customer->save(false);
                                
                                print_r($customer);
                                exit;
                                
                                return $user;
                        }
                        
                        return null;
                } catch (Exception $ex) {
                        echo $ex->getMessage();
                        exit();
                        return null;
                }
        }
}
