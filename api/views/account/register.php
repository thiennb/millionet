<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

$this->title = 'Sign In';

$mailOption = [
    'options' => ['class' => 'form-group has-feedback'],
    'inputTemplate' => "{input}<span class='glyphicon glyphicon-envelope form-control-feedback'></span>"
];

$userOption = [
    'options' => ['class' => 'form-group has-feedback'],
    'inputTemplate' => "{input}<span class='glyphicon glyphicon-user form-control-feedback'></span>"
];

$passOption = [
    'options' => ['class' => 'form-group has-feedback'],
    'inputTemplate' => "{input}<span class='glyphicon glyphicon-lock form-control-feedback'></span>"
];

$infoOption = [
    'options' => ['class' => 'form-group has-feedback'],
    'inputTemplate' => "{input}<span class='glyphicon glyphicon-info-sign form-control-feedback'></span>"
];
?>
<script>
    function registerCallback(){
        
    }
</script>
<div class="login-box">
    <div class="login-logo">
        <a href="#"> <?= Yii::$app->name  ?></a>
    </div>
    <!-- /.login-logo -->
    <div class="login-box-body">
        

        <?php $form = ActiveForm::begin(['id' => 'login-form', 'enableClientValidation' => false]); ?>
        <div><?= isset($message) ? $message : null ?></div>
        <?= $form
            ->field($model, 'username', $userOption)
            ->label(false)
            ->textInput(['placeholder' => $model->getAttributeLabel('username')]) ?>

        <?= $form
            ->field($model, 'password', $passOption)
            ->label(false)
            ->passwordInput(['placeholder' => $model->getAttributeLabel('password')]) ?>
        
        <?= $form
            ->field($model, 'confirm_password', $passOption)
            ->label(false)
            ->passwordInput(['placeholder' => $model->getAttributeLabel('confirm_password')]) ?>
        <hr/>
        
        <?= $form
            ->field($model, 'first_name', $infoOption)
            ->label(false)
            ->textInput(['placeholder' => $model->getAttributeLabel('first_name')]) ?>
        
        <?= $form
            ->field($model, 'last_name', $infoOption)
            ->label(false)
            ->textInput(['placeholder' => $model->getAttributeLabel('last_name')]) ?>
        
        <?= $form
            ->field($model, 'first_name_kana', $infoOption)
            ->label(false)
            ->textInput(['placeholder' => $model->getAttributeLabel('first_name_kana')]) ?>
        
        <?= $form
            ->field($model, 'last_name_kana', $infoOption)
            ->label(false)
            ->textInput(['placeholder' => $model->getAttributeLabel('first_name_kana')]) ?>
        
        <?= $form
            ->field($model, 'mobile', $infoOption)
            ->label(false)
            ->textInput(['placeholder' => $model->getAttributeLabel('mobile')]) ?>
        
        <?= $form
            ->field($model, 'email', $mailOption)
            ->label(false)
            ->textInput(['placeholder' => $model->getAttributeLabel('email')]) ?>

        <div class="row">
            <!-- /.col -->
            <div class="col-xs-4">
                <?= Html::submitButton('Sign Up', ['class' => 'btn btn-default common-button-submit', 'name' => 'login-button']) ?>
            </div>
            <!-- /.col -->
        </div>
        <?php ActiveForm::end(); ?>

        

    </div>
    <!-- /.login-box-body -->
</div><!-- /.login-box -->
