<?php if($type == 'LoginCheck'){ ?>
    <div>You must login before!</div>
<?php } ?>

<script type="text/javascript">
<?php if(api\models\ApiSupport::isAccessDevice('Android')){?>
        <?php if($type == 'LoginCheck'){ ?>
        // Call login json to android
        BeautyPOS.onLogin('<?= $json ?>');
        <?php }else if ($type == 'onUserInfoChanged'){ ?>
        // Call login json to android
        BeautyPOS.onUserInfoChanged('<?= $json ?>');
		<?php }else if ($type == 'onReservationCompleted'){ ?>
        // Call login json to android
        BeautyPOS.onReservationCompleted('<?= $json ?>');
        <?php } ?>
<?php }else{ ?>
        <?php if($type == 'LoginCheck'){ ?>
        // Call login json to android
        webkit.messageHandlers.onLogin.postMessage('<?= $json ?>');
        <?php }else if ($type == 'onUserInfoChanged'){ ?>
        // Call login json to android
        webkit.messageHandlers.onUserInfoChanged.postMessage('<?= $json ?>');
		<?php }else if ($type == 'onReservationCompleted'){ ?>
        // Call login json to android
        webkit.messageHandlers.onReservationCompleted.postMessage('<?= $json ?>');
        <?php } ?>
<?php } ?>
</script>
