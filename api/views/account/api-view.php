<script type="text/javascript">
<?php if(api\models\ApiSupport::isAccessDevice('Android')){?>
        <?php if($type == 'Register'){ ?>
        // Call login json to android
        BeautyPOS.onRegister('<?= $json ?>');
        <?php }else{ ?>
        // Call login json to android
        BeautyPOS.onLogin('<?= $json ?>');
        <?php } ?>
<?php }else{ ?>
        <?php if($type == 'Register'){ ?>
        // Call login json to android
        webkit.messageHandlers.onRegister.postMessage('<?= $json ?>');
        <?php }else{ ?>
        // Call login json to android
        webkit.messageHandlers.onLogin.postMessage('<?= $json ?>');
        <?php } ?>
<?php } ?>
</script>