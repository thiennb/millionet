<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\MasterMember */


$this->registerCssFile('@web/css/common.css', ['depends' => [yii\bootstrap\BootstrapAsset::className()]]);
$this->registerCssFile('@web/css/member.css', ['depends' => [yii\bootstrap\BootstrapAsset::className()]]);
$this->registerJsFile('@web/js/member.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->title = Yii::t('backend', 'Member information change');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Master Members'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="row">
    <div class="col-lg-12 col-md-12">
        <div class="box box-info box-solid">
            <div class="box-header with-border">
                <h4 class="text-title"><b><?= Html::encode($this->title) ?></b></h4>
            </div>
            <div class="box-body content no-padding">
                <div class="common-box col-md-12 no-padding">
                    <div class="row">
                        <div class="box-header with-border common-box-h4 col-md-6">
                            <h4 class="text-title"><b><?= Yii::t('backend', 'Change of member information 1') ?></b></h4>
                        </div>
                    </div>
                </div>
                <div class="col-md-12 no-padding">
                    <div class="col-md-12 no-padding">
                        <div class="col-md-8 padding-rl-10">
                            <div class="box box-info box-solid">
                                <div class="box-body content no-padding">
                                    <div class="col-md-12">
                                        <div class="row ">
                                            <div class="col-md-3 bg_header padding-10">
                                                <?= Yii::t('backend', 'Phone'); ?>
                                            </div>
                                            <div class="col-md-5 clear-form-group ">
                                                <?php if(!empty($model->mobile)){ ?>
                                                    <?php if(strlen($model->mobile) == 10){ ?>                
                                                        <?= substr($model->mobile, 0,2) ?>-
                                                        <?= substr($model->mobile, 2,4) ?>-
                                                        <?= substr($model->mobile, 6,4) ?>
                                                    <?php } else{ ?>
                                                        <?= substr($model->mobile, 0,3) ?>-
                                                        <?= substr($model->mobile, 3,4) ?>-
                                                        <?= substr($model->mobile, 7,4) ?>
                                                    <?php } ?>
                                                <?php } else{
                                                    echo Yii::t('backend',"No Setting");
                                                } ?>
                                            </div>
                                            <div class="col-md-4">
                                                <?= Html::a('<i class="fa fa-caret-right bt-caret-right" aria-hidden="true"></i>'.Yii::t('backend', 'Update member phone number'), 
                                                ['updatephone', 'id' => $model->id], ['class' => 'btn btn-default common-button-1']) ?>
                                            </div>
                                        </div>
                                        <div class="row ">
                                            <div class="col-md-3 bg_header padding-10">
                                                <?= Yii::t('backend', 'Password'); ?>
                                            </div>
                                            <div class="col-md-5 clear-form-group">
                                                 <span><?= '**********' ?></span><br>
                                                <span  class="text-pink"><?= Yii::t('backend', 'not displayed for security protection') ?></span>
                                            </div>
                                            <div class="col-md-4 ">
                                                <?= Html::a('<i class="fa fa-caret-right bt-caret-right" aria-hidden="true"></i>'.Yii::t('backend', 'Change password'), 
                                                ['updatepassword', 'id' => $model->id], ['class' => 'btn btn-default common-button-1']) ?>
                                            </div>
                                        </div>
                                        <div class="row ">
                                            <div class="col-md-3 bg_header padding-10">
                                                <?= Yii::t('backend', 'Email'); ?>
                                            </div>
                                            <div class="col-md-5 clear-form-group ">
                                                <?= !empty($model->email)?$model->email:Yii::t('backend',"No Setting") ?>
                                            </div>
                                            <div class="col-md-4 ">
                                                <?= Html::a('<i class="fa fa-caret-right bt-caret-right" aria-hidden="true"></i>'.Yii::t('backend', 'E-mail address change'), 
                                                ['updateemail', 'id' => $model->id], ['class' => 'btn btn-default common-button-1']) ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="common-box col-md-12 no-padding">
                    <div class="row">
                        <div class="box-header with-border common-box-h4 col-md-6">
                            <h4 class="text-title"><b><?= Yii::t('backend', 'Change of member information 2') ?></b></h4>
                        </div>
                    </div>
                </div>
                <div class="col-md-12 no-padding">
                    <div class="col-md-12 no-padding">
                        <div class="col-md-8 padding-rl-10">
                            <div class="box box-info box-solid">
                                <div class="box-body content no-padding">
                                    <div class="col-md-12">
                                        <?=
                                        $this->render('_form_update', [
                                            'model' => $model,
                                            'prefecture'   =>  $prefecture,
                                        ])
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    function getBackPress(){
        <?php if(api\models\ApiSupport::isAccessDevice('Android')){?>
            // return on android
            BeautyPOS.isBackAllowing(1);
        <?php }else{ ?>
            // return on IOS
            webkit.messageHandlers.isBackAllowing.postMessage(1);
        <?php } ?>
    }
</script>
