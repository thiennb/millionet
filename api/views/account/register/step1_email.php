<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use common\models\MasterMember;
use dmstr\widgets\Alert;

/* @var $this yii\web\View */
/* @var $searchModel common\models\MasterMemberSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->registerCssFile('@web/css/common.css', ['depends' => [yii\bootstrap\BootstrapAsset::className()]]);
$this->registerCssFile('@web/css/member.css', ['depends' => [yii\bootstrap\BootstrapAsset::className()]]);
$this->registerJsFile('@web/js/member.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->title = Yii::t('backend', 'Register Member');
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('backend', 'Enter email address');
?>
<div class="row">
    <div class="col-md-4 col-md-offset-4">
        <div class="box box-info box-solid">
            <div class="box-header with-border">
                <h4 class="text-title text-center"><b><?= Yii::t('backend', 'Enter email address') ?></b></h4>
            </div>
            <div class="box-body content">
                <div class="box-header with-border common-box-h4-1 col-md-12">
                    <?= Yii::t('backend', 'E-mail address registration') ?>                                                    
                </div>
                
                <div class="form-group col-sm-12"><?= Alert::widget() ?></div>
                <div class="text-center">
                    <?php $form = ActiveForm::begin(['enableClientValidation' => FALSE]); ?>
                         <div class="row clear-form-group">
                                <?= $form->field($model, 'email',[
                                    'template' => '<div class="col-md-8 col-md-offset-2">{input}'
                                                    . '{error}'
                                                    . '</div>'
                                ])->textInput(['maxlength' => true,'class'=>'form-control']) ?>
                            </div>
                            <p><?= Yii::t('backend', '※ You will receive an authentication key to your registered destination.') ?></p>
                            <div class="form-group"></div>
                            <div class="row text-center">
                                <?= Html::submitButton(Yii::t('backend', 'Register'), ['class' => 'btn cm-btn-submit']) ?>
                            </div>

                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    function getBackPress(){
        <?php if(api\models\ApiSupport::isAccessDevice('Android')){?>
            // return on android
            BeautyPOS.isBackAllowing(1);
        <?php }else{ ?>
            // return on IOS
            webkit.messageHandlers.isBackAllowing.postMessage(1);
        <?php } ?>
    }
</script>