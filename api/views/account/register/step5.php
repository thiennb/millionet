<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\bootstrap\ActiveForm;
use yii\web\Session;
use dmstr\widgets\Alert;

/* @var $this yii\web\View */
/* @var $searchModel common\models\MasterMemberSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->registerCssFile('@web/css/common.css', ['depends' => [yii\bootstrap\BootstrapAsset::className()]]);
$this->registerCssFile('@web/css/member.css', ['depends' => [yii\bootstrap\BootstrapAsset::className()]]);
$this->title = Yii::t('backend', 'Register Member');
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Input phone number'), 'url' => ['step1']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Authentication key input'), 'url' => ['step2']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Input of member information'), 'url' => ['step3']];
$this->params['breadcrumbs'][] = Yii::t('backend', 'Confirmation of member information');
?>

<div class="row">
    <div class="col-md-4 col-md-offset-4">
        <div class="box box-info box-solid">
            <div class="box-header with-border">
                <h4 class="text-title text-center"><b><?= Yii::t('api', 'Completion of registration') ?></b></h4>
            </div>
            <div class="box-body content">
                <div class="box-header with-border common-box-h4 col-md-8">
                    <?= Yii::t('api', 'Member registration completion') ?>                                                    
                </div>
                <div class="col-sm-12">
                    <p><?= Html::encode(Yii::t('api','Your membership registration has been completed.')) ?></p>
                </div>
                <div class="col-sm-12 text-center">
                    <?php $form = ActiveForm::begin(); ?>
                    <?= Html::submitButton(Yii::t('api', 'next'), ['class' => 'btn cm-btn-submit']) ?>
                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    function getBackPress(){
        <?php if(api\models\ApiSupport::isAccessDevice('Android')){?>
            // return on android
            BeautyPOS.onRegister('<?= $json ?>');
        <?php }else{ ?>
            // return on IOS
            webkit.messageHandlers.onRegister.postMessage('<?= $json ?>');
        <?php } ?>
    }
</script>