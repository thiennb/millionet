<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\bootstrap\ActiveForm;
use yii\web\Session;

/* @var $this yii\web\View */
/* @var $searchModel common\models\MasterMemberSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */


$this->title = Yii::t('backend', 'Member information change');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Master Members'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
$this->registerCssFile('@web/css/common.css', ['depends' => [yii\bootstrap\BootstrapAsset::className()]]);
?>
<div class="row">
    <div class="col-lg-12 col-md-12">
        <div class="box box-info box-solid">
            <div class="box-header with-border">
                <h4 class="text-title"><b><?= Yii::t('api', 'Confirmation of member information') ?></b></h4>
            </div>
            <div class="box-body content">
                <div class="col-md-12">
                    <div class="common-box col-md-12 no-padding">
                        <div class="col-md-6 col-md-offset-3 no-padding">
                             <div class="row">
                                <div class="box-header with-border common-box-h4 col-md-8">
                                    <h4 class="text-title"><b><?= Yii::t('api', 'Confirmation of member information') ?></b></h4>
                                </div>
                            </div>
                            <div class="row row-inline"><div class="form-group"></div></div>
                            <div class="row">
                                <div class="col-md-12 text-center">
                                    <?= Yii::t('api','Change of member information has been completed.') ?>
                                    <div class="row row-inline"><div class="form-group"></div></div>
                                    <?php $form = ActiveForm::begin(); ?>
                                    <?= Html::submitButton(Yii::t('backend', 'Back to Service').'&nbsp;<i class="fa fa-caret-right" aria-hidden="true" style="color:#FF0066;">&nbsp;</i>', ['class' => 'btn common-back']) ?>
                                    <?php ActiveForm::end(); ?>
                                    <div class="row row-inline"><div class="form-group"></div></div>
                                    <?= Html::a(Yii::t('backend', 'To member information change').'&nbsp;<i class="fa fa-caret-right" aria-hidden="true" style="color:#FF0066;">&nbsp;</i>', ['updateinfo', 'id' => $model->id], ['class' => 'btn common-back']) ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    function getBackPress(){
        <?php if(api\models\ApiSupport::isAccessDevice('Android')){?>
            // return on android
            BeautyPOS.onUserInfoChanged('<?= $json ?>');
        <?php }else{ ?>
            // return on IOS
            webkit.messageHandlers.onUserInfoChanged.postMessage('<?= $json ?>');
        <?php } ?>
    }
</script>
