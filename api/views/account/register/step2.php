<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use dmstr\widgets\Alert;

/* @var $this yii\web\View */
/* @var $searchModel common\models\MasterMemberSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->registerCssFile('@web/css/common.css', ['depends' => [yii\bootstrap\BootstrapAsset::className()]]);
$this->registerCssFile('@web/css/member.css', ['depends' => [yii\bootstrap\BootstrapAsset::className()]]);
$this->title = Yii::t('backend', 'Register Member');
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Input phone number'), 'url' => ['step1']];
$this->params['breadcrumbs'][] = Yii::t('backend', 'Authentication key input');
?>
<div class="row">
    <div class="row">
        <div class="col-sm-4 col-sm-offset-4">
            <div class="box box-info box-solid">
                <div class="box-header with-border">
                    <h4 class="text-title text-center"><b><?= Yii::t('backend', 'Input phone number') ?></b></h4>
                </div>
                <div class="box-body content">
                    <div class="box-header with-border common-box-h4-1 col-sm-12">
                        <?= Yii::t('backend', '電話番号を入力してください') ?>                                                    
                    </div>

                    <div class="form-group"><?= Alert::widget() ?></div>
                    <div class="text-center col-sm-12">
                        <?php $form = ActiveForm::begin(['enableClientValidation' => FALSE]); ?>
                            <div class="row clear-form-group">
                                <?= $form->field($model, 'key_confirm',[
                                    'template' => '<div class="col-sm-12">{input}{error}'
                                                    . '<p class="help-block help-block-error"></p>'
                                                . '</div>'
                                ])->textInput() ?>
                            </div>
                            <p><?= Yii::t('backend', 'ご登録いただいた電話番号宛に届いた認証キーを入力してください。認証キーの有効期限は 30 分です。') ?></p>
                            <div class="row text-center">
                                <?php //Html::a(Yii::t('backend', 'Return'), ['step1'], ['class' => 'btn btn-default common-button-default']) ?>
                                <?= Html::submitButton(Yii::t('backend', 'Input key confirm'), ['class' => 'btn cm-btn-submit']) ?>
                            </div>

                        <?php ActiveForm::end(); ?>
                    </div>
                    <div class="form-group margin-bottom-10 col-sm-12"></div>
                    <div class="col-sm-12 text-center">
                        
                        <?php
                            $model = $model->mode;
                            if($model == common\models\MasterMember::REGISTER_EMAIL){?>
                                <p><?= Html::a(Yii::t('frontend','Here if you want to send the other over the degree of message'),'/api/register/step=email')?></p>
                            <?php }else{ ?>
                                <p><?= Html::a(Yii::t('frontend','Here if you want to send the other over the degree of message'),'/api/register/step=1')?></p>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php if($model == common\models\MasterMember::REGISTER_EMAIL){?>
    <script>
        function getBackPress(){
        <?php if(api\models\ApiSupport::isAccessDevice('Android')){?>
            // return on android
            BeautyPOS.isBackAllowing(0);
        <?php }else{ ?>
            // return on IOS
            webkit.messageHandlers.isBackAllowing.postMessage(0);
        <?php } ?>
        window.location.href = '/api/register/step=email';
        }
    </script>
<?php }else{ ?>
<script>
    function getBackPress(){
        <?php if(api\models\ApiSupport::isAccessDevice('Android')){?>
            // return on android
            BeautyPOS.isBackAllowing(0);
        <?php }else{ ?>
            // return on IOS
            webkit.messageHandlers.isBackAllowing.postMessage(0);
        <?php } ?>
        window.location.href = '/api/register/step=1';
    }
</script>
<?php } ?>