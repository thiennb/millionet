<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\MasterMember */
/* @var $form yii\widgets\ActiveForm */
$session = Yii::$app->session;
?>

<div class="master-member-form col-xs-12">

    <?php $form = ActiveForm::begin(); ?>
    <div class="row-inline">
        <div class="col-xs-4 bg_header div-horizontal">
            <?= Yii::t('backend', 'Name'); ?>
        </div>
        <div class="col-xs-8 ">
            <div class="row row-inline clear-padding">
                <div class="col-xs-6 div-padding text-left">
                    <?php  $first_name = !empty($model->first_name)?
                            $model->first_name
                            : Yii::t('backend','No Setting');
                    ?>
                    <?= Html::encode($first_name) ?>
                </div>
                <div class="col-xs-6 div-padding text-left">
                    <?php  $last_name = !empty($model->last_name)?
                            $model->last_name
                            : Yii::t('backend','No Setting');
                    ?>
                    <?= Html::encode($last_name) ?>
                </div>
            </div>
            <div class="row row-inline clear-padding">
                <div class="col-xs-6 div-padding text-left">
                    <?php  $first_name_kana = !empty($model->first_name_kana)?
                            $model->first_name_kana
                            : Yii::t('backend','No Setting');
                    ?>
                    <?= Html::encode($first_name_kana) ?>
                </div>
                <div class="col-xs-6 div-padding text-left">
                    <?php  $last_name_kana = !empty($model->last_name_kana)?
                            $model->last_name_kana
                            : Yii::t('backend','No Setting');
                    ?>
                    <?= Html::encode($last_name_kana) ?>
                </div>
            </div>
        </div>
    </div>
    
    <div class="row-inline">
        <div class="col-xs-4 bg_header div-horizontal">
            <?= Yii::t('backend', 'Password'); ?>
        </div>
        <div class="col-xs-8 clear-padding div-padding">
            <div class="col-xs-12 text-left">
                <?php
                if(!empty($model->password)){
                    $start = '';
                    for($i = 0; $i < strlen($model->password);$i++){
                        $start .= '*';
                    }
                    echo $start.'<br>';
                    echo Yii::t('backend','※ password that you input');
                }else{
                    echo Yii::t('backend','No Setting');
                }                
                ?>
            </div>
        </div>
    </div>
    
    <div class="row-inline">
        <div class="col-xs-4 bg_header div-horizontal">
            <?= Yii::t('backend', 'Phone'); ?>
        </div>
        <div class="col-xs-8 div-padding text-left">
            <?php  if(!empty($model->phone_1)){?>
                <?= Html::encode($model->phone_1) ?>-
                <?= Html::encode($model->phone_2) ?>-
                <?= Html::encode($model->phone_3) ?>
            <?php }else{ 
                echo Yii::t('backend','No Setting');?>
            <?php } ?>
        </div>
    </div>
        
    <div class="row-inline">
        <div class="col-xs-4 bg_header div-horizontal">
            <?= Yii::t('backend', 'Email'); ?>
        </div>
        <div class="col-xs-8 div-padding text-left">
            <?php  $email = !empty($model->email)?
                    $model->email
                    : Yii::t('backend','No Setting');
            ?>
            <?= Html::encode($email) ?>
        </div>
    </div>
    
    <div class="row-inline">
        <div class="col-xs-4 bg_header div-horizontal">
            <?= Yii::t('backend', 'Birthday'); ?>
        </div>
        <div class="col-xs-8 div-padding text-left">
            
            <?php   
                    $birth_date = !empty($model->birth_date)?
                    date('Y 年 m 月 d 日', strtotime($model->birth_date))
                    : Yii::t('backend','No Setting');
            ?>
            <?= Html::encode($birth_date) ?>
        </div>
    </div>
    
    <div class="row-inline">
        <div class="col-xs-4 bg_header div-horizontal">
            <?= Yii::t('backend', 'Sex'); ?>
        </div>
        <div class="col-xs-8 div-padding text-left">
            <?php
            if (!empty($model->sex) || $model->sex == '0'){
                if($model->sex == '1'){
                    echo Yii::t('backend', 'Female');
                } else {
                    echo Yii::t('backend', 'Male');
                }
            }else{
                echo Yii::t('backend', 'No Setting');
            }
            ?>
        </div>
    </div>
    <div class="form-group">     
    </div>
    <div class="col-xs-12">
        <div class="form-group text-center">
            <?php //Html::a(Yii::t('backend', 'Return'), ['step3'], ['class' => 'btn btn-default common-button-default']) ?>
            <?= Html::submitButton(Yii::t('backend', 'Register'), ['class' => 'btn cm-btn-submit'])
            ?>
        </div>
    </div>
    <?php ActiveForm::end(); ?>

</div>
