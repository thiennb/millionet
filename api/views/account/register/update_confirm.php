<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\MasterMember */

$this->title = Yii::t('backend', 'Member information change');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Master Members'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
$this->registerCssFile('@web/css/common.css', ['depends' => [yii\bootstrap\BootstrapAsset::className()]]);
?>
<div class="row">
    <div class="col-lg-12 col-md-12">
        <div class="box box-info box-solid">
            <div class="box-header with-border">
                <h4 class="text-title"><b><?= Html::encode($this->title) ?></b></h4>
            </div>
            <div class="box-body content no-padding">
                <div class="col-md-12">
                    <div class="common-box col-md-12 no-padding">
                        <div class="row no-margin">
                            <div class="box-header with-border common-box-h4 col-md-6">
                                <h4 class="text-title"><b><?= Yii::t('backend', 'Change of member information 2') ?></b></h4>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 no-padding">
                        <div class="col-md-12 no-padding">
                            <div class="col-md-8 no-padding">
                                <div class="box box-info box-solid">
                                    <div class="box-body content">
                                        <div class="col-md-12">
                                            <?=
                                            $this->render('_form_update_confirm', [
                                                'model' => $model,
                                            ])
                                            ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    function getBackPress(){
        <?php if(api\models\ApiSupport::isAccessDevice('Android')){?>
            // return on android
            BeautyPOS.isBackAllowing(0);
        <?php }else{ ?>
            // return on IOS
            webkit.messageHandlers.isBackAllowing.postMessage(0);
        <?php } ?>
        window.location.href = '/api/account/updateinfo?id=<?php echo \Yii::$app->request->get('id') ?>';
    }
</script>
