<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\bootstrap\ActiveForm;
use dmstr\widgets\Alert;

/* @var $this yii\web\View */
/* @var $searchModel common\models\MasterMemberSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */


$this->registerCssFile('@web/css/common.css', ['depends' => [yii\bootstrap\BootstrapAsset::className()]]);
$this->registerCssFile('@web/css/member.css', ['depends' => [yii\bootstrap\BootstrapAsset::className()]]);
$this->registerJsFile('@web/js/member.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->title = Yii::t('backend', 'Register Member');
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Input phone number'), 'url' => ['step1']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Authentication key input'), 'url' => ['step2']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <div class="col-md-4 col-md-offset-4">
        <div class="box box-info box-solid">
            <div class="box-header with-border">
                <h4 class="text-title text-center"><b><?= Yii::t('backend', 'Input phone number') ?></b></h4>
            </div>
            <div class="box-body content">
                <div class="box-header with-border common-box-h4-1 col-md-12">
                    <?= Yii::t('backend', '電話番号を入力してください') ?>                                                    
                </div>
                
                <div class="form-group"><?= Alert::widget() ?></div>
                <?= $this->render('_form', [
                    'model' => $model,
                ]) ?>
            </div>
        </div>
    </div>
</div>
<script>
    function getBackPress(){
        <?php if(api\models\ApiSupport::isAccessDevice('Android')){?>
            // return on android
            BeautyPOS.isBackAllowing(0);
        <?php }else{ ?>
            // return on IOS
            webkit.messageHandlers.isBackAllowing.postMessage(0);
        <?php } ?>
        window.location.href = '/api/register/step=3';
    }
</script>