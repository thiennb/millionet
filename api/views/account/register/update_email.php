<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use common\models\MasterMember;
use dmstr\widgets\Alert;

/* @var $this yii\web\View */
/* @var $searchModel common\models\MasterMemberSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('backend', 'E-mail address change');
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('backend', 'Input phone number');
$this->registerCssFile('@web/css/common.css', ['depends' => [yii\bootstrap\BootstrapAsset::className()]]);
?>
<div class="row">
    <div class="col-lg-12 col-md-12">
        <div class="box box-info box-solid">
            <div class="box-header with-border">
                <h4 class="text-title"><b><?= $this->title ?></b></h4>
            </div>
            <div class="box-body content no-padding">
                <div class="col-md-12 no-padding">
                    <div class="common-box col-md-10 col-md-offset-1 booking-container no-padding">
                        <div class="row">
                            <div class="box-header with-border common-box-h4 col-md-6">
                                <h4 class="text-title"><b><?= $this->title ?></b></h4>
                            </div>
                        </div>

                        <div class="form-group">
                        </div>
                        <div class="row">
                            <div class="col-md-8 col-md-offset-2 no-padding">
                                <div class="box box-info box-solid">
                                    <div class="box-body content">
                                        <div class="col-md-12 no-padding">
                                            <div class="col-md-10 col-md-offset-1 booking-container">
                                                <div class="form-group"><?= Alert::widget() ?></div>
                                                <div class="row text-center">
                                                    <?= Yii::t('backend','If you want change email, please input new email below') ?>
                                                </div>
                                                <div class="form-group"></div>
                                                <div class="row padding">
                                                    <?php $form = ActiveForm::begin(['enableClientValidation' => FALSE]); ?>
                                                    <div class="row margin-rl-neg-10">
                                                        <div class="col-md-5 bg_header padding-10">
                                                            <?= Yii::t('backend', 'Email before change'); ?>
                                                        </div>
                                                        <div class="col-md-7 clear-form-group no-padding">
                                                            <div class="col-md-12">
                                                                <div class="col-md-12">
                                                                    <?=
                                                                    !empty($email)?Html::encode($email):Yii::t('backend', 'No Setting')
                                                                    ?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <?php $model->email = ''; ?>
                                                    <div class="row margin-rl-neg-10">
                                                        <div class="col-md-5 bg_header padding-10">
                                                            <?= Yii::t('backend', 'New Email'); ?>
                                                        </div>
                                                        <div class="col-md-7 clear-form-group no-padding">
                                                            <div class="col-md-12">
                                                                <?=
                                                                $form->field($model, 'email', [
                                                                    'template' => '<div class="col-md-12">{input}{error}</div>'
                                                                ])->textInput(['maxlength' => true])
                                                                ?>
                                                            </div>
                                                            <div class="col-md-12">
                                                                <?=
                                                                $form->field($model, 'email_re', [
                                                                    'template' => '<div class="col-md-12">{input}{error}</div>'
                                                                ])->textInput(['maxlength' => true])
                                                                ?>
                                                            </div>
                                                            <div class="col-md-12">
                                                                <div class="col-md-12">
                                                                    <?= Yii::t('backend', 'Please enter again to confirm'); ?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group"></div>
                                                    <div class="row text-center">
                                                        <?= Html::a('<i class="fa fa-caret-left" aria-hidden="true" style="color:#FF0066;">&nbsp</i>'.Yii::t('backend', 'Return'), ['updateinfo','id'=>$model->id], ['class' => 'btn common-back']) ?>
                                                        <?= Html::submitButton(Yii::t('backend', 'Register'), ['class' => 'btn cm-btn-submit width-auto margin-b-0 font-size-18']) ?>
                                                    </div>
                                                    <?php ActiveForm::end(); ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    function getBackPress(){
        <?php if(api\models\ApiSupport::isAccessDevice('Android')){?>
            // return on android
            BeautyPOS.isBackAllowing(0);
        <?php }else{ ?>
            // return on IOS
            webkit.messageHandlers.isBackAllowing.postMessage(0);
        <?php } ?>
        window.location.href = '/api/account/updateinfo?id=<?php echo \Yii::$app->request->get('id') ?>';
    }
</script>
