<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\Session;
use yii\jui\DatePicker;
use common\components\Constants;
use common\models\MasterMember;
/* @var $this yii\web\View */
/* @var $model common\models\MasterMember */
/* @var $form yii\widgets\ActiveForm */
$session = Yii::$app->session;
?>

<div class="master-member-form col-xs-12">

    <?php $form = ActiveForm::begin(['enableClientValidation' => FALSE]); ?>
    <div class="col-xs-12 bg_header div-padding-header">
        <label class='mws-form-label'><?= Yii::t('backend','Name'); ?></label>
        <span class="required-text">必須</span>
    </div>
    <div class="col-xs-12 clear-padding">
            <?= $form->field($model, 'first_name',[
                'template' => "<div class='col-xs-4 clear-padding clear-label-requir'>{label}</div>"
                            . '<div class="col-xs-8 clear-padding">{input}{error}</div>'
            ])->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'last_name',[
                'template' => "<div class='col-xs-4 clear-padding clear-label-requir'>{label}</div>"
                            . '<div class="col-xs-8 clear-padding">{input}{error}</div>'
            ])->textInput(['maxlength' => true]) ?>
            <div class="div-margin"></div>
            <?= $form->field($model, 'first_name_kana',[
                'template' => "<div class='col-xs-4 clear-padding clear-label-requir'>{label}</div>"
                            . '<div class="col-xs-8 clear-padding">{input}{error}</div>'
            ])->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'last_name_kana',[
                'template' => "<div class='col-xs-4 clear-padding clear-label-requir'>{label}</div>"
                            . '<div class="col-xs-8 clear-padding">{input}{error}'
                            . '</div>'
            ])->textInput(['maxlength' => true]) ?>
    </div>
    <div class="col-xs-12 bg_header div-padding-header">
        <label class='mws-form-label'><?= Yii::t('backend','Password'); ?></label>
        <span class="required-text">必須</span>
    </div>
    <div class="col-xs-12 clear-padding">
            <?= $form->field($model, 'password',[
                'template' => '<div class="col-xs-12 clear-padding">{input}'.Yii::t('backend','Please enter in the 6 to 20-digit alphanumeric').'{error}</div>'
            ])->passwordInput(['maxlength' => true]) ?>
            <div class="div-margin"></div>
            <?= $form->field($model, 'password_re',[
                'template' => '<div class="col-xs-12 clear-padding">{input}'.Yii::t('backend','For confirmation, please enter the other over the degree of').'{error}</div>'
            ])->passwordInput(['maxlength' => true]) ?>
    </div>
    <?php if($model->mode == MasterMember::REGISTER_PHONE){ ?>
        <div class="col-xs-12 bg_header div-padding-header">
            <label class='mws-form-label'><?= Yii::t('backend','Phone'); ?></label>
        </div>
        <div class="col-xs-12">
            &nbsp;&nbsp;&nbsp;&nbsp;
            <?php if(strlen($model->mobile) == 10){ ?>                
                <?= substr($model->mobile, 0,2) ?>-
                <?= substr($model->mobile, 2,4) ?>-
                <?= substr($model->mobile, 6,4) ?>
            <?php } else{ ?>
                <?= substr($model->mobile, 0,3) ?>-
                <?= substr($model->mobile, 3,4) ?>-
                <?= substr($model->mobile, 7,4) ?>
            <?php } ?>
        </div>
        <div class="col-xs-12 bg_header div-padding-header">
            <label class='mws-form-label'><?= Yii::t('backend','Email'); ?></label>
        </div>
        <div class="col-xs-12 clear-padding">
            <?= $form->field($model, 'email',[
                'template' => '<div class="col-xs-12 clear-padding">{input}{error}</div>'
            ])->input(['email'],['placeholder' => Yii::t('backend','Example︓○○○@exalmple.com')]) ?>
            <div class="div-margin"></div>
            <?= $form->field($model, 'email_re',[
                'template' => '<div class="col-xs-12 clear-padding">{input}'.Yii::t('backend','For confirmation, please enter the other over the degree of').'{error}</div>'
            ])->input('email') ?>
        </div>
    <?php }else if($model->mode == MasterMember::REGISTER_EMAIL){ ?>
        <div class="col-xs-12 bg_header div-padding-header">
            <label class='mws-form-label'><?= Yii::t('backend','Phone'); ?></label>
            <span class="required-text">必須</span>
        </div>
        <div class="col-xs-12 clear-padding">
            <?= $form->field($model, 'phone_1',[
                'template' => '<div class="col-xs-12 clear-padding">{input}'
                                . '{error}'
                            . '</div>'
            ])->textInput(['maxlength' => true,'class'=>'form-control text-right input-number']) ?>
            <?= $form->field($model, 'phone_2',[
                'template' => '<div class="col-xs-12 text-center"><i class="fa fa-minus" aria-hidden="true"></i></div>'
                            . '<div class="col-xs-12 clear-padding">{input}'
                                . '{error}'
                            . '</div>'
            ])->textInput(['maxlength' => true,'class'=>'form-control text-right input-number']) ?>
            <?= $form->field($model, 'phone_3',[
                'template' => '<div class="col-xs-12 text-center"><i class="fa fa-minus" aria-hidden="true"></i></div>'
                            . '<div class="col-xs-12 clear-padding">{input}'
                                . '{error}'
                            . '</div>'
            ])->textInput(['maxlength' => true,'class'=>'form-control text-right input-number']) ?>
        </div>
        <div class="col-xs-12 bg_header div-padding-header">
            <label class='mws-form-label'><?= Yii::t('backend','Email'); ?></label>
        </div>
        <div class="col-xs-12">
            <?= $model->email; ?>
        </div>
    <?php } ?>
    <div class="col-xs-12 bg_header div-padding-header">
        <label class='mws-form-label'><?= Yii::t('backend','Birthday'); ?></label>
    </div>
    <div class="col-xs-12 clear-padding">
        <?= $form->field($model, 'birth_date',[
            'template' => '<div class="col-xs-12 clear-padding">{input}{error}</div>'
        ])->widget(DatePicker::classname(), [
            'language' => 'ja',
            'dateFormat' => 'yyyy/MM/dd',
            'clientOptions' => [
                "changeMonth" => true,
                "changeYear" => true,
                "yearRange"=> "1900:+0"
            ]
        ]) ?>
    </div>
    <div class="col-xs-12 bg_header div-padding-header">
        <label class='mws-form-label'><?= Yii::t('backend','Sex'); ?></label>
    </div>
    <div class="col-xs-12 clear-padding">
        <?php echo $form->field($model, 'sex',[
            'template' => '<div class="col-xs-9 clear-padding">{input}{error}</div>'
        ])->radioList(Constants::LIST_SEX); ?>
    </div>
    <div class="col-xs-12">
        <div class="form-group text-center">
            
            <?php 
//                $model->mode==MasterMember::REGISTER_PHONE?
//                Html::a(Yii::t('backend', 'Return'), ['step2'], ['class' => 'btn common-button-default'])
//                :Html::a(Yii::t('backend', 'Return'), ['step1'], ['class' => 'btn common-button-default']) 
            ?>
            <?= Html::submitButton(Yii::t('backend', 'Go to the confirmation screen'), 
                    ['class' => 'btn cm-btn-submit']) ?>
        </div>
    </div>
    <?php ActiveForm::end(); ?>

</div>
