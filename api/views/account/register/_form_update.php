<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\Session;
use yii\jui\DatePicker;
use common\components\Constants;
/* @var $this yii\web\View */
/* @var $model common\models\MasterMember */
/* @var $form yii\widgets\ActiveForm */
$session = new Session;
?>

<div class="master-member-form">

    <?php $form = ActiveForm::begin(['enableClientValidation' => FALSE]); ?>
    <div class="row ">
        <div class="col-md-3 bg_header padding-10">
            <?= Yii::t('backend','Name'); ?>
        </div>
        <div class="col-md-9 clear-form-group">
            <?= $form->field($model, 'first_name',[
                'template' => "<div class='col-md-2 clear-label-requir'>{label}</div>"
                            . '<div class="col-md-4">{input}{error}</div>'
            ])->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'last_name',[
                'template' => "<div class='col-md-2 clear-label-requir'>{label}</div>"
                            . '<div class="col-md-4">{input}{error}</div>'
            ])->textInput(['maxlength' => true]) ?>
            <div class="div-margin"></div>
            <?= $form->field($model, 'first_name_kana',[
                'template' => "<div class='col-md-2 clear-label-requir'>{label}</div>"
                            . '<div class="col-md-4">{input}{error}</div>'
            ])->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'last_name_kana',[
                'template' => "<div class='col-md-2 clear-label-requir'>{label}</div>"
                            . '<div class="col-md-4">{input}{error}</div>'
            ])->textInput(['maxlength' => true]) ?>
        </div>
    </div>
    <div class="row ">
        <div class="col-md-3 bg_header padding-10">
            <?= Yii::t('backend','Birthday'); ?>
        </div>
        <div class="col-md-9 clear-form-group">
            <?= $form->field($model, 'birth_date',[
                'template' => '<div class="col-md-12 no-padding">{input}{error}</div>'
            ])->widget(DatePicker::classname(), [
                'language' => 'ja',
                'dateFormat' => 'yyyy/MM/dd',
                'clientOptions' => [
                    "changeMonth" => true,
                    "changeYear" => true,
                    "yearRange"=> "1900:+0"
                ]
            ]) ?>
        </div>
    </div>
    <div class="row ">
        <div class="col-md-3 bg_header padding-10">
            <?= Yii::t('backend','Sex'); ?>
        </div>
        <div class="col-md-9 clear-form-group">
            <?php echo $form->field($model, 'sex',[
                'template' => '<div class="col-md-9">{input}{error}</div>'
            ])->radioList(Constants::LIST_SEX); ?>
        </div>
    </div>
    
    <div class="row ">
        <div class="col-md-1 bg_header padding-10">
            <?= Yii::t('backend','Address'); ?>
        </div>
        <div class="col-md-2 bg_header div-padding">
            <?= Yii::t('backend','PostCode'); ?>
        </div>
        <div class="col-md-9">
            <div class="col-xs-5 clear-padding clear-help-block clear-form-group">
                <?= $form->field($model, 'post_code_1',[
                    'template' => '{input}{error}'
                ])->textInput(['maxlength' => true])->widget(\yii\widgets\MaskedInput::className(), [
                    'mask' => '999',
                    'clientOptions' => [
                        'removeMaskOnSubmit' => true
                    ]
                ]) ?>
            </div>
            <div class="col-xs-2 div-horizontal-all padding-10"><i class="fa fa-minus padding-10" aria-hidden="true"></i></div>
            <div class="col-xs-5 clear-help-block clear-form-group clear-padding">
                <?= $form->field($model, 'post_code_2',[
                    'template' => '{input}{error}'
                ])->textInput(['maxlength' => true])->widget(\yii\widgets\MaskedInput::className(), [
                    'mask' => '9999',
                    'clientOptions' => [
                        'removeMaskOnSubmit' => true
                    ]
                ]) ?>
            </div>
            <div class="col-xs-12 text-left">
                <?= Html::a('<i class="fa fa-caret-right bt-caret-right" aria-hidden="true"></i>'.Yii::t('backend', 'Automatic input an address'), ['#'],
                        ['class' => 'btn btn-default common-button-1','id'=>'search-code']) ?>
            </div> 
        </div>
    </div>
    
    <div class="row ">
        <div class="col-md-1">
        </div>
        <div class="col-md-2 bg_header div-padding">
            <?= Yii::t('backend','Prefectures'); ?>
        </div>
        <div class="col-md-9 clear-padding clear-form-group-1">
            <?= $form->field($model, 'prefecture',[
            'template' => '<div class="col-md-12">{input}{error}'
                        . '</div>'
            ])->dropDownList($prefecture, ['prompt' => Yii::t('backend', 'Please Select')]) ?>
        </div>
    </div>
    
    <div class="row ">
        <div class="col-md-1"></div>
        <div class="col-md-2 bg_header div-padding">
                <?= Yii::t('backend','The city where your business is located below'); ?>
        </div>
        <div class="col-md-9 clear-padding clear-form-group-1">
                <?= $form->field($model, 'address',[
                'template' => '<div class="col-md-12">{input}{error}'
                            . '</div>'
                ])->textInput(['maxlength' => true]) ?>
            <div  class="col-md-12 text-pink"><?= Yii::t('frontend','※ If there is a building name (apartment name apartment name), please be sure to fill in') ?></div>
        </div>
    </div>
    
    <div class="form-group"></div>
    <div class="row">
        <div class="form-group text-center">
            <?= Html::submitButton(Yii::t('backend', 'Check'), 
                    ['class' => 'btn cm-btn-submit'])
//                    ['class' => 'btn cm-btn-submit',"onclick" => 'load_waiting()']) ?>
        </div>
    </div>
    <?php ActiveForm::end(); ?>

</div>
<script> 
    $("#search-code").click(function (e) {
        var code_ele_1 = '#mastermember-post_code_1',
            code_ele_2 = '#mastermember-post_code_2',
            address_ele = '#mastermember-prefecture';
        var code = $(code_ele_1).val()+''+$(code_ele_2).val();
        code = code.replace(/[^0-9\.]+/g, "");

    //       $(address_ele).val(code);
        if (code != "") {
            var pathControler = SITE_ROOT + 'account/getpostcode';
            $.ajax({
                url: pathControler,
                type: 'post',
                data: {
                    postCode: code
                },
                success: function (data) {
                    $(code_ele_1).closest('.form-group').removeClass('has-error').find('.help-block').text('');
                    $(code_ele_2).closest('.form-group').removeClass('has-error').find('.help-block').text('');
                    if (data != '') {
                        $(address_ele).val(data);
                    } else {
                        $(code_ele_1).closest('.form-group').addClass('has-error').find('.help-block').text('入力した郵便番号に該当する住所は存在しません。');
                        $(code_ele_2).closest('.form-group').addClass('has-error').find('.help-block');
                    }
                }
            });
        } else {
            $(code_ele_1).closest('.form-group').addClass('has-error').find('.help-block').text('郵便番号を入力してください。');
            $(code_ele_2).closest('.form-group').addClass('has-error').find('.help-block');
        }
        return false;
    });
    
    var waitingDialog = waitingDialog || (function ($) {
        'use strict';

            // Creating modal dialog's DOM
            var $dialog = $(
                    '<div class="modal fade" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-hidden="true" style="padding-top:15%; overflow-y:visible;">' +
                    '<div class="modal-dialog modal-m">' +
                    '<div class="modal-content">' +
                            '<div class="modal-header"><h3 style="margin:0;"></h3></div>' +
                            '<div class="modal-body">' +
                                    '<div class="progress progress-striped active" style="margin-bottom:0;"><div class="progress-bar" style="width: 100%"></div></div>' +
                            '</div>' +
                    '</div></div></div>');

            return {
                    /**
                     * Opens our dialog
                     * @param message Custom message
                     * @param options Custom options:
                     * 				  options.dialogSize - bootstrap postfix for dialog size, e.g. "sm", "m";
                     * 				  options.progressType - bootstrap postfix for progress bar type, e.g. "success", "warning".
                     */
                    show: function (message, options) {
                            // Assigning defaults
                            if (typeof options === 'undefined') {
                                    options = {};
                            }
                            if (typeof message === 'undefined') {
                                    message = 'しばらくお待ちください。';
                            }
                            var settings = $.extend({
                                    dialogSize: 'm',
                                    progressType: '',
                                    onHide: null // This callback runs after the dialog was hidden
                            }, options);

                            // Configuring dialog
                            $dialog.find('.modal-dialog').attr('class', 'modal-dialog').addClass('modal-' + settings.dialogSize);
                            $dialog.find('.progress-bar').attr('class', 'progress-bar');
                            if (settings.progressType) {
                                    $dialog.find('.progress-bar').addClass('progress-bar-' + settings.progressType);
                            }
                            $dialog.find('h3').text(message);
                            // Adding callbacks
                            if (typeof settings.onHide === 'function') {
                                    $dialog.off('hidden.bs.modal').on('hidden.bs.modal', function (e) {
                                            settings.onHide.call($dialog);
                                    });
                            }
                            // Opening dialog
                            $dialog.modal();
                    },
                    /**
                     * Closes dialog
                     */
                    hide: function () {
                            $dialog.modal('hide');
                    }
            };

    })(jQuery);

    function load_waiting(){
        waitingDialog.show();
    }
</script>
