<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use dmstr\widgets\Alert;

/* @var $this yii\web\View */
/* @var $searchModel common\models\MasterMemberSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->registerCssFile('@web/css/common.css', ['depends' => [yii\bootstrap\BootstrapAsset::className()]]);
$this->registerCssFile('@web/css/member.css', ['depends' => [yii\bootstrap\BootstrapAsset::className()]]);
$this->title = Yii::t('backend', 'Update member phone number');
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Input phone number'), 'url' => ['step1']];
$this->params['breadcrumbs'][] = Yii::t('backend', 'Authentication key input');
?>
<div class="row">
    <div class="col-lg-12 col-md-12">
        <div class="box box-info box-solid">
            <div class="box-header with-border">
                <h4 class="text-title"><b><?= $this->title ?></b></h4>
            </div>
            <div class="box-body content">
                <div class="common-box col-md-12 clear-padding">
                    <div class="row">
                        <div class="box-header with-border common-box-h4 col-md-6">
                            <h4 class="text-title"><b><?= Yii::t('backend', 'Authentication key input') ?></b></h4>
                        </div>
                    </div>
                    <div class="form-group">
                    </div>
                    <div class="row">
                        <div class="col-md-4 col-md-offset-4 no-padding">
                            <div class="box box-info box-solid">
                                <div class="box-body content">
                                    <div class="col-md-12">
                                        <div class="col-md-12 booking-container">
                                            <div class="row">
                                                <div class="box-header with-border common-box-h4-1 col-md-12">
                                                    <?= Yii::t('backend', '認証キーを入力してください') ?>
                                                </div>
                                            </div>
                                            <div class="form-group"><?= Alert::widget() ?></div>
                                            <div class="row">
                                                <?php $form = ActiveForm::begin(['enableClientValidation' => FALSE]); ?>
                                                    <div class="row clear-form-group">
                                                        <?= $form->field($model, 'key_confirm',[
                                                            'template' => '<div class="col-md-12">{input}{error}'
                                                                            . '<p class="help-block help-block-error"></p>'
                                                                        . '</div>'
                                                        ])->textInput(['maxlength' => true]) ?>
                                                    </div>
                                                    <p><?= Yii::t('backend', 'ご登録いただいた電話番号宛に届いた認証キーを入力してください。認証キーの有効期限は 30 分です。') ?></p>
                                                    <div class="row text-center">
                                                        <?php //Html::a(Yii::t('backend', 'Return'), ['step1'], ['class' => 'btn btn-default common-button-default']) ?>
                                                        <?= Html::submitButton(Yii::t('backend', 'Input key confirm'), ['class' => 'btn cm-btn-submit padding-5']) ?>
                                                    </div>

                                                <?php ActiveForm::end(); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12 text-center">
                    <?= Html::a(Yii::t('backend', 'Send phone key confirm again'), ['updatephone','id'=>$model->id]) ?>
                </div>
                <div class="form-group"></div>
                <div class="form-group"></div>
            </div>
        </div>
    </div>
</div>
<script>
    function getBackPress(){
        <?php if(api\models\ApiSupport::isAccessDevice('Android')){?>
            // return on android
            BeautyPOS.isBackAllowing(0);
        <?php }else{ ?>
            // return on IOS
            webkit.messageHandlers.isBackAllowing.postMessage(0);
        <?php } ?>
        window.location.href = '/api/account/updatephone?id=<?php echo \Yii::$app->request->get('id') ?>';
    }
</script>
