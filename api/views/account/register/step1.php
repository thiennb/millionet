<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use common\models\MasterMember;
use dmstr\widgets\Alert;

/* @var $this yii\web\View */
/* @var $searchModel common\models\MasterMemberSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->registerCssFile('@web/css/common.css', ['depends' => [yii\bootstrap\BootstrapAsset::className()]]);
$this->registerCssFile('@web/css/member.css', ['depends' => [yii\bootstrap\BootstrapAsset::className()]]);
$this->registerJsFile('@web/js/member.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->title = Yii::t('backend', 'Register Member');
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('backend', 'Input phone number');
?>
<div class="row">
    <div class="col-md-4 col-md-offset-4">
        <div class="box box-info box-solid">
            <div class="box-header with-border">
                <h4 class="text-title text-center"><b><?= Yii::t('backend', 'Input phone number') ?></b></h4>
            </div>
            <div class="box-body content">
                <div class="box-header with-border common-box-h4-1 col-md-12">
                    <?= Yii::t('backend', '電話番号を入力してください') ?>                                                    
                </div>
                
                <div class="form-group col-md-12"><?= Alert::widget() ?></div>
                <div class="text-center">
                    <?php $form = ActiveForm::begin(['enableClientValidation' => FALSE]); ?>
                        <div class="row clear-form-group">
                            <?= $form->field($model, 'phone_1',[
                                'template' => '<div class="col-md-12"></div>'
                                            . '<div class="col-md-12">{input}'
                                                . '{error}'
                                            . '</div>'
                            ])->textInput(['maxlength' => true,'class'=>'form-control text-right input-number']) ?>
                            <?= $form->field($model, 'phone_2',[
                                'template' => '<div class="col-md-12 div-horizontal-all"><i class="fa fa-minus" aria-hidden="true"></i></div>'
                                            . '<div class="col-md-12">{input}'
                                                . '{error}'
                                            . '</div>'
                            ])->textInput(['maxlength' => true,'class'=>'form-control text-right input-number']) ?>
                            <?= $form->field($model, 'phone_3',[
                                'template' => '<div class="col-md-12 div-horizontal-all"><i class="fa fa-minus" aria-hidden="true"></i></div>'
                                            . '<div class="col-md-12">{input}'
                                                . '{error}'
                                            . '</div>'
                                            . '<div class="col-md-12"></div>'
                            ])->textInput(['maxlength' => true,'class'=>'form-control text-right input-number']) ?>
                        </div>
                        <p><?= Yii::t('backend', '※ ご登録された電話番号に認証キーをお送りします。') ?></p>
                        <div class="form-group"></div>
                        <div class="row text-center">
                            <?= Html::submitButton(Yii::t('backend', 'Register'), ['class' => 'btn cm-btn-submit']) ?>
                        </div>

                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    function getBackPress(){
        <?php if(api\models\ApiSupport::isAccessDevice('Android')){?>
            // return on android
            BeautyPOS.isBackAllowing(1);
        <?php }else{ ?>
            // return on IOS
            webkit.messageHandlers.isBackAllowing.postMessage(1);
        <?php } ?>
    }
</script>