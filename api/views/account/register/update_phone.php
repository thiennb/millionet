<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use common\models\MasterMember;
use dmstr\widgets\Alert;

/* @var $this yii\web\View */
/* @var $searchModel common\models\MasterMemberSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->registerCssFile('@web/css/common.css', ['depends' => [yii\bootstrap\BootstrapAsset::className()]]);
$this->registerCssFile('@web/css/member.css', ['depends' => [yii\bootstrap\BootstrapAsset::className()]]);
$this->registerJsFile('@web/js/member.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->title = Yii::t('backend', 'Update member phone number');
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('backend', 'Input phone number');
?>
<div class="row">
    <div class="col-lg-12 col-sm-12 no-padding">
        <div class="box box-info box-solid">
            <div class="box-header with-border">
                <h4 class="text-title"><b><?= $this->title ?></b></h4>
            </div>
            <div class="box-body content">
                <div class="common-box col-sm-12">
                    <div class="col-sm-12 clear-padding">
                        <div class="box-header with-border common-box-h4 col-sm-6">
                            <h4 class="text-title"><b><?= $this->title ?></b></h4>
                        </div>
                    </div>
                    <div class="form-group">
                    </div>
                    <div class="col-sm-12 clear-padding">
                        <div class="box box-info box-solid">
                            <div class="form-group col-sm-12"></div>
                            <div class="col-sm-12 clear-border-bot">
                                <div class="box-header with-border common-box-h4-1 col-sm-12">
                                    <?= Yii::t('backend', '電話番号を入力してください') ?>                                                    
                                </div>
                            </div>
                            <div class="form-group"><?= Alert::widget() ?></div>
                            <div class="col-sm-12 text-center clear-padding">
                                <div class="col-md-12 text-left"><p><?= Yii::t('frontend','To set up a new phone number, please proceed in the change.') ?></p></div>
                                <?php $form = ActiveForm::begin(['enableClientValidation' => FALSE]); ?>
                                    <div class="col-sm-12 row-inline">
                                        <div class="col-xs-3 bg_header div-horizontal">
                                            <?= Yii::t('frontend', 'Before change the phone number'); ?>
                                        </div>
                                        <div class="col-xs-9 clear-form-group clear-padding">
                                            <div class="col-sm-12">
                                                <div class="col-sm-3">
                                                    <?= $form->field($model, 'phone_1',[
                                                        'template' => '{input}{error}'
                                                    ])->textInput(['maxlength' => true,'class'=>'form-control text-right input-number']) ?>
                                                </div>
                                                <div class="col-sm-1 text-center padding-10"><i class="fa fa-minus" aria-hidden="true"></i></div>
                                                <div class="col-sm-3">
                                                    <?= $form->field($model, 'phone_2',[
                                                        'template' => '{input}{error}'
                                                    ])->textInput(['maxlength' => true,'class'=>'form-control text-right input-number']) ?>
                                                </div>
                                                <div class="col-sm-1 text-center padding-10"><i class="fa fa-minus" aria-hidden="true"></i></div>
                                                <div class="col-sm-3">
                                                    <?= $form->field($model, 'phone_3',[
                                                        'template' => '{input}{error}'
                                                    ])->textInput(['maxlength' => true,'class'=>'form-control text-right input-number']) ?>
                                                </div>
                                                <div class="col-sm-2"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 row-inline">
                                        <div class="col-xs-3 bg_header div-horizontal">
                                            <?= Yii::t('frontend', 'New phone number'); ?>
                                        </div>
                                        <div class="col-xs-9 clear-form-group clear-padding">
                                            <div class="col-sm-12">
                                                <div class="col-sm-3">
                                                    <?= $form->field($model, 'phone_1_new',[
                                                        'template' => '{input}{error}'
                                                    ])->textInput(['maxlength' => true,'class'=>'form-control text-right input-number']) ?>
                                                </div>
                                                <div class="col-sm-1 text-center padding-10"><i class="fa fa-minus" aria-hidden="true"></i></div>
                                                <div class="col-sm-3">
                                                    <?= $form->field($model, 'phone_2_new',[
                                                        'template' => '{input}{error}'
                                                    ])->textInput(['maxlength' => true,'class'=>'form-control text-right input-number']) ?>
                                                </div>
                                                <div class="col-sm-1 text-center padding-10"><i class="fa fa-minus" aria-hidden="true"></i></div>
                                                <div class="col-sm-3">
                                                    <?= $form->field($model, 'phone_3_new',[
                                                        'template' => '{input}{error}'
                                                    ])->textInput(['maxlength' => true,'class'=>'form-control text-right input-number']) ?>
                                                </div>
                                                <div class="col-sm-2"></div>
                                            </div>
                                            <div class="col-sm-12 text-left"><div class="col-sm-12"><?= Yii::t('frontend', 'Please enter the 10 to 11-digit alphanumeric'); ?></div></div>
                                            <div class="col-sm-12">
                                                <div class="col-sm-3">
                                                    <?= $form->field($model, 'phone_1_re',[
                                                        'template' => '{input}{error}'
                                                    ])->textInput(['maxlength' => true,'class'=>'form-control text-right input-number']) ?>
                                                </div>
                                                <div class="col-sm-1 text-center padding-10"><i class="fa fa-minus" aria-hidden="true"></i></div>
                                                <div class="col-sm-3">
                                                    <?= $form->field($model, 'phone_2_re',[
                                                        'template' => '{input}{error}'
                                                    ])->textInput(['maxlength' => true,'class'=>'form-control text-right input-number']) ?>
                                                </div>
                                                <div class="col-sm-1 text-center padding-10"><i class="fa fa-minus" aria-hidden="true"></i></div>
                                                <div class="col-sm-3">
                                                    <?= $form->field($model, 'phone_3_re',[
                                                        'template' => '{input}{error}'
                                                    ])->textInput(['maxlength' => true,'class'=>'form-control text-right input-number']) ?>
                                                </div>
                                                <div class="col-sm-2"></div>
                                            </div>
                                            <div class="col-sm-12 text-left"><div class="col-sm-12"><?= Yii::t('backend', 'For confirmation, please enter the other over the degree of'); ?></div></div>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 form-group"></div>
                                    <div class="col-sm-12 row-inline text-center">
                                        <?= Html::a('<i class="fa fa-caret-left" aria-hidden="true" style="color:#FF0066;">&nbsp</i>'.Yii::t('backend', 'Return'), ['updateinfo','id'=>$model->id], ['class' => 'btn common-back']) ?>
                                        <?= Html::submitButton(Yii::t('backend', 'Change'), ['class' => 'btn cm-btn-submit width-auto margin-b-0 font-size-18']) ?>
                                    </div>
                                    <div class="col-sm-12 form-group"></div>
                                <?php ActiveForm::end(); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    function getBackPress(){
        <?php if(api\models\ApiSupport::isAccessDevice('Android')){?>
            // return on android
            BeautyPOS.isBackAllowing(0);
        <?php }else{ ?>
            // return on IOS
            webkit.messageHandlers.isBackAllowing.postMessage(0);
        <?php } ?>
        window.location.href = '/api/account/updateinfo?id=<?php echo \Yii::$app->request->get('id') ?>';
    }
</script>