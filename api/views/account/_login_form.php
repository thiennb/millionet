<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
?>
<div class="error hidden alert alert-danger"></div>
<?php $form = ActiveForm::begin(['id' => 'login-form',
			'enableClientValidation' => false,
			'fieldClass' => 'justinvoelker\awesomebootstrapcheckbox\ActiveField']); ?>
<?= $form
    ->field($model, 'tel_no')
    ->textInput(['maxlength' => true]) ?>

<?= $form
    ->field($model, 'password')
    ->passwordInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'rememberMe')
                ->checkbox() ?>

<?= Html::submitButton(Yii::t('backend', 'Sign In'), ['class' => 'btn common-button-submit', 'name' => 'login-button']) ?>

<?php ActiveForm::end(); ?>