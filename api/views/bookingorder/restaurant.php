<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use Carbon\Carbon;

/* @var $this yii\web\View */
/* @var $searchModel common\models\BookingSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = "理美容 POS システム";
$this->params['breadcrumbs'][] = $this->title;

// Build urls
$request = Yii::$app->request;

$baseQuery  = 'storeId=' . $request->get('storeId') . '&coupons=' . $request->get('coupons') . '&executeTime=' . $request->get('executeTime') . '&products=' . $request->get('products') . '&options=' . $request->get('options') . '&noOption=' . $request->get('noOption');

// 2 tab urls
$toStaffURL = 'staff?' . $baseQuery;
$toCalendarURL = 'salonschedule?' . $baseQuery;
// base url for buttons
$baseURL = 'detail?' . $baseQuery . '&capacity=' . $request->get('capacity') . '&seat=' . $request->get('seat') . '&date=';

$time_left = $time_open;
$time_right = $time_open;
$time_center = $time_open;
?>
<?php echo $this->render('part/header') ?>
<div class="top-floated" style="border-bottom: 0">
    <?php echo $this->render('part/nav', ['step' => 2]) ?>
</div>
<div class="container booking-container" style="top: 5px">
	<div class="row" style="padding-top: 10px">
		<div class="col-xs-12">
            <div class="col-xs-12">
    			<?php echo Yii::t('frontend', 'Total duration (approximate)') ?> : <?php echo $timeString ?>
    		</div>
    		<div class="col-xs-12">
    			<div class="mT10 pr">
    				<ul class="couponTypeFilter cFix">
    					<li>
    						<?= Html::a(Yii::t('frontend', '< 前の一週間'), $toCalendarURL, ['class' => 'btnCouponTypeFilter isCr btn_prev_week_seat']) ?>
    					</li>
    					<li>
    						<?= Html::a(Yii::t('frontend', '次の一週間 >'), $toStaffURL, ['class' => 'btnCouponTypeFilter isCr btn_next_week_seat']) ?>
    					</li>
    				</ul>
    			</div>
    		</div>
		</div>
        <div class="clearfix"></div>
		<div>
			<div id="jsRsvCdTbl" class="ReserveConditionTable underTabContents">
				<div class="coverTable">
					<div class="whiteTable2">
						<table id="calendarM" class="innerTable taC nowrap" cellpadding="0" cellspacing="0">
							<tbody>
								<tr id="headCal"></tr>
								<tr id="dayRow"></tr>
								<tr id="valRow">
									<?php if ( $count > 0 ) : ?>

									<!-- Begin Time Left -->
									<th class="innerCell">
										<table cellpadding="0" cellspacing="0" class="moreInnerTable vaT">
											<tbody>
												<?php for ($i = 0; $i <= $count; $i++) :?>
														<?php if ($i == 0) : ?>
															<tr><th class="timeCell"><p class="hourR"><?= $time_open ?></p></th></tr>
															<tr><th class="separate"></th></tr>
														<?php else : ?>
															<?php $time_left = date('H:i', strtotime("+30 minutes", strtotime($time_left))); ?>
															<tr><th class="timeCell"><p class="hourR"><?= $time_left ?></p></th></tr>
															<tr><th class="separate"></th></tr>
														<?php endif; ?>
												<?php endfor; ?>
											</tbody>
										</table>
									</th>
									<!-- End Time Left -->

									<!-- Begin Time Body -->
									<?php foreach ($model as $day) :?>
                                        <?php if (isset($store_schedule[$day['date']])) : ?>
                                            <?php
                                                $current_schedule = $store_schedule[$day['date']];
                                                $is_holiday = $current_schedule['work_flg'] === '0';
                                                $open_timestamp = Carbon::createFromFormat('Y-m-d H:i', $day['date'] . ' ' . $current_schedule['start_time'])->timestamp;
                                                $close_timestamp = Carbon::createFromFormat('Y-m-d H:i', $day['date'] . ' ' . $current_schedule['end_time'])->timestamp;
                                            ?>
                                        <?php else : ?>
                                            <?php
                                                $is_holiday = false;
                                                $open_timestamp = Carbon::createFromFormat('Y-m-d H:i', $day['date'] . ' ' . $store->time_open)->timestamp;
                                                $close_timestamp = Carbon::createFromFormat('Y-m-d H:i', $day['date'] . ' ' . $store->time_close)->timestamp;

                                            ?>
                                        <?php endif ?>
                                        <?php $real_count = ($close_timestamp - $open_timestamp) / 1800; ?>

										<?php $time_center = $time_open;?>
										<th class="innerCell">
											<table cellpadding="0" cellspacing="0" class="moreInnerTable" data-date="<?= $day['date'] ?>">
												<tbody>
                                                    <?php $current_timestamp = time(); ?>
                                                    <?php $invalid_block_html = '
                                                        <td class="openColor" data-time=""><p class="scheduleR"><span class="bS db reserveImpossible offL">×</span></p></td>
                                                        <tr><th class="separate"></th></tr>'; ?>
                                                    <?php $tel_block_html = '
                                                        <td class="closeColor" data-time=""><p class="scheduleR"><span class="bS db reserveTel offL">TEL</span></p></td>
                                                        <tr><th class="separate"></th></tr>'; ?>
													<?php for ($i = 0; $i <= $count; $i++) :?>
														<?php if ($i == 0) : ?>
                                                            <?php $block_date_time = $day['date'] . ' ' . $time_open. ':00' ?>
                                                            <?php $block_timestamp = Carbon::createFromFormat('Y-m-d H:i:s', $block_date_time)->timestamp ?>
                                                            <?php if ($is_holiday || $block_timestamp < $current_timestamp || $block_timestamp < $open_timestamp || $block_timestamp > $close_timestamp || $i > $real_count - $number_row || $seat_count === 0) : ?>
                                                                <?php echo $invalid_block_html ?>
                                                            <?php elseif ($block_timestamp < $current_timestamp + $reservation_possible_seconds): ?>
                                                                <?php echo $tel_block_html ?>
                                                            <?php elseif ( !isset($schedule[$block_date_time]) || count($schedule[$block_date_time]) < $seat_count ) : ?>
                                                                <td class="openColor" data-time="<?= $time_center ?>"><p class="scheduleR"><a href="
                                                                    <?php echo $baseURL . $block_timestamp ?>" title="◎" class="bS db reserveImmediately offL vaT">◎</a></p></td>
                                                                <tr><th class="separate"></th></tr>
                                                            <?php else: ?>
                                                                <?php echo $invalid_block_html ?>
                                                            <?php endif; ?>
														<?php else : ?>
                                                            <?php $time_center = date('H:i:00', strtotime("+30 minutes", strtotime($time_center))); ?>
                                                            <?php $block_date_time = $day['date'] . ' ' . $time_center ?>
                                                            <?php $block_timestamp = Carbon::createFromFormat('Y-m-d H:i:s', $block_date_time)->timestamp ?>
                                                            <?php if ($is_holiday || $block_timestamp < $current_timestamp || $block_timestamp < $open_timestamp || $block_timestamp > $close_timestamp || $i > $real_count - $number_row || $seat_count === 0) : ?>
                                                                <?php echo $invalid_block_html ?>
                                                            <?php elseif ($block_timestamp < $current_timestamp + $reservation_possible_seconds): ?>
                                                                <?php echo $tel_block_html ?>
                                                            <?php elseif ( !isset($schedule[$block_date_time]) || count($schedule[$block_date_time]) < $seat_count ) : ?>
                                                                <td class="openColor" data-time="<?= $time_center ?>"><p class="scheduleR"><a href="
                                                                    <?php echo $baseURL . $block_timestamp ?>" title="◎" class="bS db reserveImmediately offL vaT">◎</a></p></td>
                                                                <tr><th class="separate"></th></tr>
                                                            <?php else: ?>
                                                                <?php echo $invalid_block_html ?>
                                                            <?php endif; ?>
														<?php endif; ?>
													<?php endfor; ?>
												</tbody>
											</table>
										</th>
									<?php endforeach; ?>
									<!-- End Time Body -->

									<?php else : ?>
										<td colspan="16"><?= Yii::t('frontend', 'Not Found') ?></td>
									<?php endif; ?>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
var selectedDay = "<?= $setting['date'] ?>";
var week = "<?= $setting['week'] ?>";
var time_execute = "<?= $request->get('executeTime') ?>";
var store_id = "<?= $setting['store_id'] ?>";
var coupons = "<?= $request->get('coupons') ?>";
var products = "<?= $request->get('products') ?>";
var options = "<?= $request->get('options') ?>";
var staff = "<?= $request->get('staff') ?>";
var numberOfDay = "<?php echo $numberOfDay ?>";
</script>

<?php $this->registerCSSFile('@web/css/salon_header.css',['position' => \yii\web\View::POS_HEAD]);?>
<?php $this->registerCSSFile('@web/css/choose_menu.css',['position' => \yii\web\View::POS_HEAD]);?>
<?php $this->registerJsFile('@web/js/salonschedule.js',['depends' => [\yii\web\JqueryAsset::className()]]); ?>
<?php echo $this->render('part/submitButton') ?>
<?php echo $this->render('part/backButton', ['screen' => 4]) ?>
