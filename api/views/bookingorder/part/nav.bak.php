<div class="clearfix"></div>
<div class="col-xs-12 steps at-step-<?php echo $step ?>">
	<div class="row">
		<div class="step step-1">
			商品選択
		</div>
		<div class="step step-2">
			スタッフ・日時選択
		</div>
		<div class="step step-3">
			お客様情報の入力
		</div>
		<div class="step step-4">
			予約内容の確認
		</div>
		<div class="step step-5">
			予約完了
		</div>
	</div>
</div>
<div class="clearfix"></div>
