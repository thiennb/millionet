<?php
	use yii\helpers\Html;
	use common\components\Util;
	/* $item */
	/* $price */
	/* display_condition */
	/* $mode */
?>
<tr>
    <th class="col-xs-6">
        <div>
            <ul class="list-inline categories">
                <?php if ($mode === 'coupon') : ?>
                    <?php $cateList = array_filter(explode(',', strtr($item['listcate'], '{}"', ',,,'))); ?>
                    <?php foreach ($cateList as $cateName) : ?>
                        <?php if ($cateName && $cateName != 'NULL') : ?>
                            <li><?php echo HTML::encode($cateName) ?></li>
                        <?php endif ?>
                    <?php endforeach ?>
                <?php elseif ($mode === 'product') : ?>
                    <?php if ($item['category_name']) : ?>
                            <li><?php echo HTML::encode($item['category_name']) ?></li>
                    <?php endif ?>
                <?php endif ?>
            </ul>
        </div>
        <div>
            <?php echo HTML::encode($item['name']) ?>
        </div>
        <div> <p class="normal-text"><?php echo HTML::encode($item['description']) ?> </p></div>
        <div>
            <?php if (false) : // ($mode === 'coupon') : ?>
                <span>
                    <?php echo Yii::t('api', 'Displaying condition') ?> : <span class="normal-text"><?php echo $display_condition ?></span>
                </span>
                <span style="margin-left: 5px">
                    <?php echo Yii::t('api', 'Applying condition') ?> : <span class="normal-text"><?php echo $item['apply_condition'] == 1 ? '対象者限定' : '全員' ?>
                </span>
                <span style="margin-left: 5px">
                    <?php echo Yii::t('api', 'Expire date') ?> :
                    <span class="normal-text">
                        <?php
                            if(!empty($item['expire_date'])) {
                                sscanf($item['expire_date'], '%d-%d-%d', $year, $month, $day);
                                echo $year . '年' . $month . '月' . $day . '日まで';
                            }
                        ?>
                    </span>
                </span>
            <?php endif ?>
        </div>
    </th>
    <td class="col-xs-3">
        <?php echo $price ?>
    </td>
    <td class="col-xs-3">
        <?php echo (int) $item['time_require'] ?><?php echo Yii::t('api', 'minutes') ?>
    </td>
</tr>
