<?php
    use yii\helpers\Html;
    use common\components\Util;
?>
<div class="col-xs-12 select-item select-<?php echo $mode ?>">
    <div class="row">
        <div class="col-xs-12">
            <div class="media">
                <div class="media-left">
                    <a href="#">
                        <img class="media-object" src="<?php echo Util::getUrlImage($image) ?>" alt="#">
                    </a>
                </div>
            <div class="media-body">
                <div class="clearfix"></div>
                <div>
                    <div class="col-xs-7 name">
                        <?php echo HTML::encode($item['name']) ?>
                    </div>
                    <div class="col-xs-5 price" style="font-weight: normal">
                        <span style="color: #292929;"><?php echo Yii::t('api', 'Nomination fee') ?> : </span>¥<?php echo $price ?>
                    </div>
                </div>
                <div class="clearfix"></div>
                <p class="normal-text">
                    <?php echo HTML::encode($item['position']) ?> (<?php echo HTML::encode($item['career']) ?>)
                </p>
                <div class="clearfix"></div>
                <p class="normal-text">
                    <?php echo HTML::encode($item['catch']) ?>
                </p>
                </div>
            </div>
        </div>
		<a class="select-booking-<?php echo $mode ?>" title="<?php echo Yii::t('api', 'Select this staff') ?>" href="<?php
                    $request = Yii::$app->request;
                    echo sprintf(
                        'salon?storeId=%d&products=%s&coupons=%s&options=%s&executeTime=%d&staff=%d&noOption=%d',
                        $request->post('storeId'),
                        $request->post('products'),
                        $request->post('coupons'),
                        $request->post('options'),
                        $request->post('executeTime'),
                        $item['id'],
                        $request->post('noOption')
                    );
                     ?>">
                    <i class="fa fa-chevron-right"></i>
                </a>
	</div>
</div> 