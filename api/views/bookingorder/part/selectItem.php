<?php
	use yii\helpers\Html;
	use common\components\Util;
	/* $item */
	/* $price */
	/* $image */
	/* display_condition */
	/* $mode */
?>
<div class="col-xs-12 select-item select-<?php echo $mode ?>">
	<div class="row">
		<div class="col-xs-8">
			<ul class="list-inline categories">
				<?php if ($mode === 'coupon') : ?>
					<?php $cateList = array_filter(explode(',', strtr($item['listcate'], '{}"', ',,,'))); ?>
					<?php foreach ($cateList as $cateName) : ?>
                                            <?php if ($cateName && $cateName != 'NULL') : ?>
						<li><?php echo HTML::encode($cateName) ?></li>
                                            <?php endif ?>
					<?php endforeach ?>
				<?php elseif ($mode === 'product') : ?>
					<?php if ($item->category) : ?>
						<li><?php echo HTML::encode($item->category->name) ?></li>
					<?php endif ?>
				<?php endif ?><!--echo Yii::t('api', 'Nomination fee')-->
			</ul>
		</div>
		<div class="col-xs-4 price">
			<?php if ($mode !== 'staff') : ?>
				¥<?php echo $price ?>
			<?php endif ?>
		</div>
	</div>
	<div class="col-xs-12 name">
            <?php echo HTML::encode($item['name']) ?>
        </div>
	<div class="row">
		<div class="col-xs-12">
			<div class="media">
                            <div class="media-left">
                                <img class="media-object" src="<?php echo Util::getUrlImage($image) ?>" alt="#">
                            </div>
                            <div class="media-body">
                                <?php if ($mode === 'product') : ?>
                                    <p class="normal-text">
                                        <?php echo Util::truncateString(HTML::encode($item['description']), 50, '。。。'); ?>
                                    </p>
                                <?php endif ?>
                            <?php if ($mode === 'coupon') : ?>
                                <p class="normal-text">
                                    <?php echo Yii::t('api', 'Displaying condition') ?> : <?php echo Util::truncateString($display_condition, 50, '。。。') ?>
                                </p>
                                <p class="normal-text">
                                    <?php echo Yii::t('api', 'Applying condition') ?> : <?php echo $item['apply_condition'] == 1 ? '対象者限定' : '全員' ?>
                                </p>
                                <p class="normal-text">
                                    <?php echo Yii::t('api', 'Expire date') ?> :
                                    <?php
                                        if(!empty($item['expire_date'])) {
                                            sscanf($item['expire_date'], '%d-%d-%d', $year, $month, $day);
                                            echo Util::truncateString($year . '年' . $month . '月' . $day . '日まで', 50, '。。。');
                                        }
                                    ?>
                                </p>
                            <?php endif ?>
			  </div>
			</div>
		</div>
		<div class="col-xs-12">
                    <button type="button" class="pull-right btn btn-booking select-booking-<?php echo $mode ?>" value="<?php echo $item['id'] ?>" data-toggle-text="<?php echo Yii::t('api', 'Deselect this') ?>"><?php echo Yii::t('api', 'Select this') ?></button>
                </div>
	</div>
</div>
