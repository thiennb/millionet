<?php

use yii\helpers\Html;
use common\components\Util;

/* $item */
/* $price */
/* $image */
/* display_condition */
/* $mode */
?>
<div class="col-xs-12 select-item select-<?php echo $mode ?>">
    <div class="row">
        <div class="col-xs-8">
            <ul class="list-inline categories">
                <?php if ($item->category) : ?>
                    <li><?php echo HTML::encode($item->category->name) ?></li>
                <?php endif ?>
            </ul>
        </div>
        <div class="col-xs-4 price">
            <?php echo $price ?>
        </div>
    </div>
    <div class="col-xs-12 name">
        <?php echo HTML::encode($item['name']) ?>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="media">
                <div class="media-left">
                    <img class="media-object" src="<?php echo Util::getUrlImage($image) ?>" alt="#">
                </div>
                <div class="media-body">
                    <p class="normal-text">
                        <?php echo Util::truncateString(HTML::encode($item['description']), 50, '。。。'); ?>
                    </p>
                </div>
            </div>
        </div>
        <div class="col-xs-12">
            <button type="button" class="pull-right btn btn-booking select-booking-<?php echo $mode ?>" value="<?php echo $item['id'] ?>" data-toggle-text="<?php echo Yii::t('api', 'Deselect this') ?>"><?php echo Yii::t('api', 'Select this') ?></button>
        </div>
    </div>
</div>
