<?php use yii\helpers\Html; ?>
<tr>
	<td class="col-xs-7">
		<label class="radio" for="<?php echo $id ?>">
			<input class="select-option" type="radio" name="<?php echo $name ?>" id="<?php echo $id ?>" value="<?php echo $option_id . '-' . $item['id'] . '-' . $item['search_by'] ?>">
			<?php echo HTML::encode($item['name']) ?>
			<input type="hidden" class="option-name" value="<?php echo $item['name'] ?>">
			<input type="hidden" class="option-price" value="<?php echo $price ?>">
			<input type="hidden" class="option-time" value="<?php echo $time ?>">
		</label>
	</td>
	<td class="col-xs-2">
		¥<?php echo Yii::$app->formatter->asDecimal($price, 0) ?>
	</td>
	<td class="col-xs-3">
		<?php echo $time ?><?php echo Yii::t('api', 'minutes') ?>
	</td>
</tr>
