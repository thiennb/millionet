<div class="clearfix"></div>
<div class="<?php if (isset($class)) { echo $class; } else echo 'booking-error'; ?>" role="alert" <?php if (isset($id)) { echo 'id="'. $id .'"'; } ?>>
	<button type="button" class="close btn-close-alert" data-dismiss="alert" aria-label="Close">&times;</button>
	<label for="booking-error"><?php if (isset($message)) echo $message; ?></label>
</div>
<div class="clearfix"></div>
