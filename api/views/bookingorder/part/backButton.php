<?php use api\models\ApiSupport; use api\controllers\BookingorderController; ?>
<?php $request = Yii::$app->request; ?>
<?php $isAndroidUser = ApiSupport::isAccessDevice('Android') ?>
<?php                   // 1    ,   2     ,  3     ,    4   ,   5     ,   6      ,    7        ?>
<?php $screenActions = ['index', 'options', 'staff', 'salon', 'detail', 'confirm', 'success']; ?>
<!-- BACK BUTTON -->
<?php echo '<script>' ?>
<?php echo 'function getBackPress(){'; ?>
<?php if ($screen === 7) : ?>
	$('#btn-final').trigger('click');
<?php elseif ($screen === 1) : ?>
	<?php if($isAndroidUser) : ?>
		BeautyPOS.isBackAllowing(1); // return on android
	<?php else : ?>
		webkit.messageHandlers.isBackAllowing.postMessage(1); // return on IOS
	<?php endif; ?>
<?php else : ?>
	<?php if($isAndroidUser) : ?>
		BeautyPOS.isBackAllowing(0); // return on android
	<?php else : ?>
		webkit.messageHandlers.isBackAllowing.postMessage(0); // return on IOS
	<?php endif; ?>
	<?php $previousScreen = $screenActions[$screen - 2]; if ($previousScreen == 'options' && $request->get('noOption')) $previousScreen = 'index'; ?>
	<?php $urlParts = [
		$previousScreen,
		'storeId',
		'coupons',
		'products',
		'options',
		'staff',
		'date',
		'executeTime',
        'seat',
        'capacity'
	];

        if ($previousScreen !== 'index') {
            $urlParts[] = 'noOption';
	}
        if ($screen > 3) {
            $urlParts[] = 'noStaff';
	}
        ?>
	window.location.href="<?php echo BookingorderController::buildURL($urlParts, $request->get()) ?>";
<?php endif ?>
<?php echo '}</script>' ?>
<!-- END BACK BUTTON -->
