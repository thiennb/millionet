<div class="footer-attachment">
    <div class="clearfix"></div>
    <div class="col-xs-12 text-center">
        <?php if (isset($step)) : ?>
            <button type="submit" class="btn btn-submit" value="<?php echo $step ?>">
                <?php if (isset($text)) :?>
                    <?php echo $text; ?>
                <?php else : ?>
                    この内容で次へ
                <?php endif ?>
            </button>
        <?php endif ?>
        
        <button type="button" class="btn btn-top">
            ↑上に戻る
        </button>
    </div>
    <div class="clearfix"></div>
</div>