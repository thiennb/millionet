<?php
    use yii\helpers\Html;
    use common\components\Util;
?>
<div class="col-xs-12 select-item select-<?php echo $mode ?>">
    <div class="row">
        <div class="col-xs-12">
            <div class="media">
                <div class="media-left">
                    <a href="#">
                        <img class="media-object" src="<?php echo Util::getUrlImage($image) ?>" alt="#">
                    </a>
                </div>
            <div class="media-body">
                <div class="clearfix"></div>
                <div>
                    <div class="col-xs-12 name">
                        <?php echo HTML::encode($item['name']) ?>
                    </div>
                </div>
                <div class="clearfix"></div>
                <p class="normal-text">
                    <?php echo $item['capacity_max'] ?>人
                </p>
                <div class="clearfix"></div>
                <p class="normal-text">
                    <?php echo HTML::encode($item['introduce']) ?>
                </p>
                </div>
            </div>
        </div>
        <?php echo Html::hiddenInput('seat', $item['id'], ['class' => 'seat_id']) ?>
		<a class="select-booking-<?php echo $mode ?>" title="<?php echo Yii::t('api', 'Select this staff') ?>" href="#">
            <i class="fa fa-chevron-right"></i>
        </a>
	</div>
</div>
