<?php
    use yii\widgets\ActiveForm;
    use yii\helpers\Url;
    use yii\helpers\Html;
?>
<?php echo $this->render('part/header') ?>
<div class="top-floated" style="border-bottom: 0">
       <?php echo $this->render('part/nav', ['step' => 2]) ?>
</div>
<div class="container booking-container">
    <!-- CAPACITY LOAD -->
    <div class="clearfix"></div>
    <div class="col-xs-12 heading">
        人数の選択
    </div>
    <div class="clearfix"></div>
    <?php $form = ActiveForm::begin([
        'method' => 'GET',
        'id' => 'booking-seat-capacity',
        'action' => Url::home() . Yii::$app->controller->id . '/seat',
        'validateOnBlur' => false,
        'enableClientValidation' => false,
        'fieldConfig' => [
            'options' => [
                'tag' => false,
            ],
        ]
    ]) ?>
    <div class="col-xs-4" style="line-height: 40px">
        人数を入力
    </div>
    <div class="col-xs-4" style="padding: 0; padding-top: 5px">
        <?php echo $form->field($formModel, 'storeId')->hiddenInput(['name' => 'storeId'])->label(false)->error(false); ?>
        <?php echo $form->field($formModel, 'products')->hiddenInput(['name' => 'products'])->label(false)->error(false); ?>
        <?php echo $form->field($formModel, 'options')->hiddenInput(['name' => 'options'])->label(false)->error(false); ?>
        <?php echo $form->field($formModel, 'coupons')->hiddenInput(['name' => 'coupons'])->label(false)->error(false); ?>
        <?php echo $form->field($formModel, 'noOption')->hiddenInput(['name' => 'noOption'])->label(false)->error(false); ?>
        <?php echo $form->field($formModel, 'executeTime')->hiddenInput(['name' => 'executeTime'])->label(false)->error(false); ?>

        <?php echo $form->field($formModel, 'capacity', [
                'template' => '<div class="input-group">{input}<span class="input-group-addon" style="background: transparent; border: 0">人</span>
                <div class="clearfix"></div></div>{error}'
            ])->textInput([
                'name' => 'capacity',
                'style' => 'border-radius: 4px'
            ]) ?>
    </div>
    <div class="col-xs-4">
        <?php echo Html::submitButton('表示', ['class' => 'btn btn-default btn-submit', 'style' => 'border-radius: 4px; height: 35px']) ?>
    </div>
    <?php ActiveForm::end() ?>
    <div class="clearfix"></div>
    <div class="col-xs-12 heading">
        席の選択
    </div>
    <!-- END CAPACITY LOAD -->

	<div class="clearfix"></div>
        <div class="col-xs-12 select-item select-seat" style="margin-top: 15px;">
            <div class="row">
                <div class="col-xs-12 name">指名なし</div>
                <?php echo Html::hiddenInput('seat', 0, ['class' => 'seat_id']) ?>
                <a class="select-booking-seat" style="top: 30%" title="<?php echo Yii::t('api', 'Select this seat') ?>" href="#">
                    <i class="fa fa-chevron-right"></i>
                </a>
            </div>
	</div>
	<div class="clearfix"></div>
    <?php echo $this->render('ajax/selectItemList', [
        'items' => $seats
    ]); ?>
</div>
<?php echo $this->render('part/submitButton') ?>
<?php echo $this->render('part/backButton', ['screen' => 3]) ?>
