
<?php

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use Carbon\Carbon;
?>
<?php
    $form = ActiveForm::begin(['id' => '', 'enableClientValidation' => false, 'fieldConfig' => [
    'options' => [
            'tag' => false,
            ],
    ]]);
?>
<?php echo $this->render('part/header') ?>
<div class="top-floated" style="border-bottom: 0">
       <?php echo $this->render('part/nav', ['step' => 4]) ?>
</div>
<div class="container booking-container">
	<div class="col-xs-12 text-center" style="margin-top: 15px;">
		<h4>以下の内容で予約を確定します。</h4>
	</div>
	<div class="col-xs-12 error-list">
		<?php if (count($formModel->errors)) : ?>
			<div class="booking-error show">
		<?php endif ?>
			<?php $request = Yii::$app->request ?>
			<?php $staff = $request->get('staff'); if($staff == '' && $request->get('noStaff') == 1) { $staff = 0; } ?>
			<?php echo $form->field($formModel, 'storeId')->hiddenInput(['value' => $request->get('storeId')])->label(false) ?>
			<?php echo $form->field($formModel, 'products')->hiddenInput(['value' => $request->get('products')])->label(false) ?>
			<?php echo $form->field($formModel, 'coupons')->hiddenInput(['value' => $request->get('coupons')])->label(false) ?>
			<?php echo $form->field($formModel, 'options')->hiddenInput(['value' => $request->get('options')])->label(false) ?>
			<?php echo $form->field($formModel, 'staff')->hiddenInput(['value' => $staff])->label(false) ?>
			<?php echo $form->field($formModel, 'date')->hiddenInput(['value' => $request->get('date')])->label(false) ?>
            <?php echo $form->field($formModel, 'seat')->hiddenInput(['value' => $request->get('seat')])->label(false) ?>
            <?php echo $form->field($formModel, 'capacity')->hiddenInput(['value' => $request->get('capacity')])->label(false) ?>
		<?php if (count($formModel->errors)) : ?>
			</div>
		<?php endif ?>
	</div>
	<div class="col-xs-12">
		<table class="table table-bordered text-center">
			<tr>
				<th class="col-xs-4 col-sm-3">
					来店日時
				</th>
				<td>
                                    <?php echo Carbon::createFromTimestamp($request->get('date'))->format('Y/m/d H:i') ?>
				</td>
			</tr>
			<tr>
				<th>
					時間合計(目安)
				</th>
				<td>
                                    <?php $executeTime = $request->get('executeTime') ?>
                                    <?php $h = floor($executeTime / 60) ?>
                                    <?php $m = $executeTime % 60; ?>
                                    <?php if ($h > 0) : ?>
                                        <?php echo $h ?>時間
                                    <?php endif ?>
                                    <?php if ($m > 0) : ?>
                                        <?php echo $m ?>分
                                    <?php endif ?>
				</td>
			</tr>
		</table>

		<table class="table table-bordered text-center">
			<tbody id="load-order-detail">

			</body>
		</table>

		<table class="table table-bordered text-center">
			<tr>
				<th class="col-xs-4 col-sm-3">
					ご予約者名
				</th>
				<td>
					<?php echo HTML::encode($customer->name) ?>
				</td>
			</tr>
			<tr>
				<th>
					ご連絡先
				</th>
				<td>
					<?php echo HTML::encode($customer->mobile) ?>
				</td>
			</tr>
			<tr>
				<th>
					ご要望
				</th>
				<td>
					<?php echo HTML::encode(Yii::$app->session->get('booking-demand')) ?>
				</td>
			</tr>
		</table>
                <?php if(count($checklists) > 0): ?>
                <table class="table table-bordered">
                    <tbody>
                    <?php foreach($checklists as $checklist): ?>
                    <!-- HTML for checklist -->
                        <tr>
                            <?php if($checklist->type == 2): ?>
                            <th  class="col-xs-4 col-sm-3 text-center">
                                <div style="word-break: break-all">
                                    <?php echo $checklist->question->question_content; ?>
                                </div>
                                <?php if($checklist->question->option == 2): ?>
                                <i style="font-size: 12px;">(<?php echo Yii::t('frontend', 'Max choice') ?>: <?php echo $checklist->question->max_choice ?>)</i>
                                <?php endif; ?>
                            </th>
                            <td class="vaThT" data-maxchoice="<?php echo $checklist->question->max_choice ?>">
                                <!-- HTML when option is 一つのみ選択 -->
                                <?php if($checklist->question->option == 1): ?>
                                    <?php foreach ($checklist->questionAnswers as $aKey => $answer): ?>
                                    <div>
                                        <input type="radio" disabled="true" checked/>
                                        <label><?php echo $answer->content ?></label>
                                    </div>
                                    <?php endforeach; ?>
                                <!-- HTML when option is 複数選択 -->
                                <?php elseif($checklist->option == 2): ?>
                                    <?php foreach ($checklist->questionAnswers as $aKey => $answer): ?>
                                    <div>
                                        <input type="checkbox" disabled="true" checked/>
                                        <label><?php echo $answer->content ?></label>
                                    </div>
                                    <?php endforeach; ?>
                                <!-- HTML when option is フリー入力 -->
                                <?php else: ?>
                                <div style="word-break: break-all"><?php echo $checklist->questionAnswers; ?></div>
                                <?php endif; ?>
                            </td>
                            <?php elseif($checklist->type == 1): ?>
                            <th class="col-xs-4 col-sm-3 text-center">
                                <div style="word-break: break-all">
                                    <?php echo $checklist->question->notice_content; ?>
                                </div>
                            </th>
                            <td class="vaThT">
                                <div><input type="checkbox" disabled="true" checked/> <label><?php echo Yii::t('frontend', 'Confirmed') ?></label></div>
                            </td>
                            <?php endif; ?>
                        </tr>
                    <!-- End HTML for checklist -->
                    <?php endforeach; ?>
                    </tbody>
                </table>
                <?php endif; ?>
	</div>
</div>

<?php echo $this->render('/bookingorder/part/submitButton', ['step' => 3, 'text' => '予約を確定する']) ?>
<?php echo $this->render('part/backButton', ['screen' => 6]) ?>
<?php ActiveForm::end() ?>
