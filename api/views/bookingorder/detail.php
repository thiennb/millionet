<?php

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
?>
<?php
        $form = ActiveForm::begin(['id' => '', 'fieldConfig' => [
        'options' => [
                'tag' => false,
                ],
        ]]);
?>
<?php echo $this->render('part/header') ?>
<div class="top-floated">
       <?php echo $this->render('part/nav', ['step' => 3]) ?>
</div>
<div class="container booking-container">
	<div class="clearfix"></div>
        <div class="col-xs-12">
                <div class="row">
                    <div class="col-md-12">
                        <span class="booking-help-text"><?= $form->errorSummary($formModel); ?></span>
                    </div>
                </div>
		<div class="row heading">
			お客様の情報を入力してください
		</div>

		<div class="row">
			<table class="table table-bordered td-align-left">
				<tr>
					<th class="col-xs-4 col-sm-3 text-center">
						氏名
					</th>
					<td>
                                            <?php echo HTML::encode($customer->name) ?>
					</td>
				</tr>
				<tr>
					<th class="text-center">
						電話番号
					</th>
					<td>
                                            <?php echo HTML::encode($customer->mobile) ?>
					</td>
				</tr>
				<tr>
					<th class="text-center">
						ご要望
					</th>
					<td>
						<?php echo $form->field($formModel, 'demand')->textInput(['maxlength' => true])->label(false) ?>
					</td>
				</tr>
                                 <!-- Question with question option -->
                                <?php foreach($checklists as $qKey => $checklist): ?>
                                <!-- HTML when type is (質問事項) -->
                                <?php if($checklist->type == 2): ?>
                                <tr class="check-row">
                                    <th>
                                        <div style="word-break: break-all">
                                            <?php echo $checklist->question->question_content; ?>
                                        </div>
                                        <?php if($checklist->question->option == 2): ?>
                                        <i style="font-size: 12px;">(<?php echo Yii::t('frontend', 'Max choice') ?>: <?php echo $checklist->question->max_choice ?>)</i>
                                        <?php endif; ?>
                                    </th>
                                    <td class="vaThT" data-maxchoice="<?php echo $checklist->question->max_choice ?>">
                                        <!-- HTML when option is 一つのみ選択 -->
                                        <?php if($checklist->question->option == 1): ?>
                                            <?php foreach ($checklist->question->questionAnswers as $aKey => $answer): ?>
                                            <div >
                                                <input type="radio" id="one_<?php echo $checklist->id.'_'.$answer->id ?>" name="checklist[one][<?php echo $checklist->id ?>]" value="<?php echo $answer->id ?>"/>
                                                <label for="one_<?php echo $checklist->id.'_'.$answer->id ?>"><?php echo $answer->content ?></label>
                                            </div>
                                            <?php endforeach; ?>
                                        <!-- HTML when option is 複数選択 -->
                                        <?php elseif($checklist->question->option == 2): ?>
                                            <?php foreach ($checklist->question->questionAnswers as $aKey => $answer): ?>
                                            <div>
                                                <input type="checkbox" class="max-choice-checkbox" id="multi_<?php echo $checklist->id.'_'.$answer->id ?>" name="checklist[multi][<?php echo $checklist->id ?>][<?php echo $answer->id ?>]"/>
                                                <label for="multi_<?php echo $checklist->id.'_'.$answer->id ?>"><?php echo $answer->content ?></label>
                                            </div>
                                            <?php endforeach; ?>
                                        <!-- HTML when option is フリー入力 -->
                                        <?php else: ?>
                                            <textarea class="form-control freedom-text" name="checklist[freedom][<?php echo $checklist->id ?>]"></textarea>
                                        <?php endif; ?>
                                    </td>
                                </tr>
                                <?php
                                // Clear this checklist
                                unset($checklists[$qKey]);
                                endif;?>
                                <?php endforeach; ?>
                                <!-- End Question with question option -->
			</table>
		</div>
	</div>
        <!-- HTML for checklist when option is (注意事項) -->
        <?php if(count($checklists) > 0): ?>
        <div class="col-xs-12">
            <div class="row heading">
                <?php //echo Yii::t('frontend', 'Checklist') ?>
                注意事項の確認
            </div>
            <div class="row">
                <table class="table table-bordered td-align-left">
                    <tbody>
                        <?php foreach($checklists as $qKey => $checklist): ?>
                        <?php if($checklist->type == 1): ?>
                        <tr>
                            <th class="col-xs-4 col-sm-3 text-center">
                                <div style="word-break: break-all">
                                    <?php echo $checklist->question->notice_content; ?>
                                    <span class="booking-help-text">※</span>
                                </div>
                            </th>
                            <td class="vaThT">
                                <div><input type="checkbox" id="agree_<?php echo $checklist->id ?>" name="checklist[agree][<?php echo $checklist->id ?>]" class="agree-checkbox"/> <label for="agree_<?php echo $checklist->id ?>"><?php echo Yii::t('frontend', 'Confirmed') ?></label></div>
                            </td>
                        </tr>
                        <?php endif; ?>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
         <?php endif; ?>
        <!-- End HTML for checklist with 注意事項 option -->
	<div class="col-xs-12">
		<div class="row heading">
			ご利用ポイントの設定
		</div>
		<div class="row">
			<table class="table table-bordered td-align-left">
				<tbody>
					<tr>
						<th class="col-xs-4 col-sm-3 text-center">
							合計金額
						</th>
						<td>
							¥<?php echo Yii::$app->formatter->asDecimal($total_price, 0) ?>
						</td>
					</tr>
					<tr>
						<th class="text-center">
							ご利用ポイント
						</th>
						<td>
							<?php echo $form->field($formModel, 'point_use', [
								'template' => '{input} P (現在ポイント '. $total_point .'P)<div class="clearfix"></div>{error}'
							])->input('text', ['class' => 'small-input'])->label(false) ?>
						</td>
					</tr>
					<tr>
						<th class="text-center">
							お支払予定金額
						</th>
						<td id="booking-need-to-pay">
							¥<?php echo Yii::$app->formatter->asDecimal($total_price, 0) ?>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
        </div>
	<?php echo $this->render('part/backButton', ['screen' => 5]) ?>
	<input type="hidden" id="point-yen" value="<?php echo $pointYen ?>">
	<input type="hidden" id="point-p" value="<?php echo $pointP ?>">
	<input type="hidden" id="total-money" value="<?php echo $total_price ?>">
	<input type="hidden" id="total-point" value="<?php echo $total_point ?>">
</div>
<?php echo $this->render('/bookingorder/part/submitButton', ['step' => 3, 'text' => '予約内容を確認する']) ?>
<?php ActiveForm::end() ?>
