<?php echo $this->render('part/header') ?>
<div class="top-floated">
    <?php echo $this->render('part/nav', ['step' => 1]) ?>
    <div id="list-filters"></div>
</div>

<div class="container booking-container booking-container-screen-1">
	<div class="col-xs-12">
		<?php echo $this->render('part/error', ['id' => 'booking-error']) ?>
	</div>
	<div id="select-items"></div>
</div>

<?php echo $this->render('part/nextButton', ['step' => 1]) ?>
<?php echo $this->render('part/backButton', ['screen' => 1]) ?>
