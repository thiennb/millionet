<?php echo $this->render('part/header') ?>
<div class="top-floated" style="border-bottom: 0">
    <?php echo $this->render('part/nav', ['step' => 2]) ?>
</div>
<div class="container booking-container">
	<div class="clearfix"></div>
        <div class="col-xs-12 select-item select-staff" style="margin-top: 15px;">
            <div class="row">
                <div class="col-xs-12 name">指名なし</div>
                <a class="select-booking-staff" style="top: 30%" title="<?php echo Yii::t('api', 'Select this staff') ?>" href="<?php
                $request = Yii::$app->request;
                echo sprintf(
                    'salon?storeId=%d&products=%s&coupons=%s&options=%s&executeTime=%d&noOption=%d&noStaff=1',
                    $request->get('storeId'),
                    $request->get('products'),
                    $request->get('coupons'),
                    $request->get('options'),
                    $request->get('executeTime'),
                    $request->get('noOption')
                );
                 ?>">
                    <i class="fa fa-chevron-right"></i>
                </a>
            </div>
	</div>
	<div class="clearfix"></div>
	<?php echo $this->render('ajax/selectItemList', [
        'items' => $staffs,
        'price_table' => $price_table
    ]); ?>
</div>
<?php echo $this->render('part/submitButton') ?>
<?php echo $this->render('part/backButton', ['screen' => 3]) ?>
