<?php
use yii\helpers\Html;
?>
<div class="container booking-container" style="margin-top: 60px">
	<?php echo $this->render('part/header') ?>
    <div class="top-floated">
	<?php echo $this->render('part/nav', ['step' => 1]) ?>
    </div>

	<div class="clearfix"></div>
	<!-- OPTION MENU -->
	<div class="col-xs-12 option-list">
		<?php $idx = 0; // seperate duplicated products ?>
		<?php foreach ($options as $option_id => $option) : ?>
			<?php if(count($option) === 0) continute; ?>
			<div class="row heading">
				<?php echo HTML::encode($option_info[$option_id]['name']) ?>
			</div>
			<div class="row">
				<?php if ($option_info[$option_id]['type'] == 1) : $products = $option; $idx++; $name = 'option-' . $idx . '@' . $option_id; ?>
					<table class="table table-bordered">
						<thead>
							<tr>
								<th>
									<?php echo Yii::t('api', 'Option name') ?>
								</th>
								<th>
									<?php echo Yii::t('api', 'Cost') ?>
								</th>
								<th>
									<?php echo Yii::t('api', 'Total time (approximate)') ?>
								</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td colspan="3">
									<label class="radio" for="option<?php echo $idx ?>--1">
										<input class="select-option" type="radio" name="<?php echo $name ?>" id="option<?php echo $idx ?>--1" value="-1">
										<?php echo Yii::t('api', 'No product selected') ?>
									</label>
								</td>
							</tr>
							<?php foreach ($products as $product): ?>
								<?php echo $this->render('part/option', [
									'option_id' => $option_id,
									'item' => $product,
									'id' => 'option-' . $option_id . '-' . $product['id'] . '-' . $idx,
									'name' => $name,
									'time' => $product['time_require'] ? $product['time_require'] : 0,
									'price' => isset($price_table[$product['id']]) ? (float)$price_table[$product['id']]['total_price'] : 0
								]) ?>
							<?php endforeach ?>
						</tbody>
					</table>
				<?php else : ?>
					<?php foreach ($option as $search_by => $products) : $idx++; $name = 'option-' . $idx . '@' . $option_id . '-' . $search_by;?>
						<table class="table table-bordered">
							<thead>
								<tr>
									<th colspan="3">
										<?php echo HTML::encode($search_by_names[$option_id][$search_by]) ?>
									</th>
								</tr>
								<tr>
									<th>
										<?php echo Yii::t('api', 'Option name') ?>
									</th>
									<th>
										<?php echo Yii::t('api', 'Cost') ?>
									</th>
									<th>
										<?php echo Yii::t('api', 'Total time (approximate)') ?>
									</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td colspan="3">
										<label class="radio" for="option<?php echo $idx ?>--1">
											<input class="select-option" type="radio" name="<?php echo $name ?>" id="option<?php echo $idx ?>--1" value="-1">
											<?php echo Yii::t('api', 'No product selected') ?>
										</label>
									</td>
								</tr>
								<?php foreach ($products as $product): ?>
									<?php echo $this->render('part/option', [
										'option_id' => $option_id,
										'item' => $product,
										'id' => 'option-' . $option_id . '-' . $product['id'] . '-' . $idx,
										'name' => $name,
										'time' => $product['time_require'] ? $product['time_require'] : 0,
										'price' => isset($price_table[$product['id']]) ? (float)$price_table[$product['id']]['total_price'] : 0
									]) ?>
								<?php endforeach; ?>
							</tbody>
						</table>
					<?php endforeach; ?>
				<?php endif; ?>
			</div>
		<?php endforeach; ?>
	</div>
	<!-- END OPTION MENU -->
	<div class="clearfix"></div>
	<!-- CONFIRM MENU -->
	<div class="col-xs-12 confirm-menu">
		<div class="row heading">
			選択済みメニューの確認
		</div>
		<div class="row">
			<div id="load-confirm-menu">

			</div>
		</div>
	</div>
	<!-- END CONFIRM MENU -->
</div>

<?php echo $this->render('part/nextButton', ['step' => $mode]) ?>
<?php echo $this->render('part/backButton', ['screen' => 2]) ?>
