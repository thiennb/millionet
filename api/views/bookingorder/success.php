<?php

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use Carbon\Carbon;

$request = Yii::$app->request;
?>
<?php echo $this->render('part/header') ?>
<div class="top-floated" style="border-bottom: 0">
       <?php echo $this->render('part/nav', ['step' => 5]) ?>
</div>
<div class="container booking-container">
    <div class="col-xs-12 text-center" style="margin-top: 10px;">
		<h4>以下の内容で予約を受け付けました。<br>
		予約のキャンセルはマイページの予約履歴からお願い致します。<br>
		内容変更につきましては店舗に直接ご確認ください。</h4>
	</div>
	<div class="col-xs-12">
		<table class="table table-bordered text-center">
			<tr>
				<th class="col-xs-4 col-sm-3">
					来店日時
				</th>
				<td>
                                    <?php echo Carbon::createFromTimestamp($request->get('date'))->format('Y/m/d H:i') ?>
				</td>
			</tr>
			<tr>
				<th>
					時間合計(目安)
				</th>
				<td>
                                    <?php $executeTime = $request->get('executeTime') ?>
                                    <?php $h = floor($executeTime / 60) ?>
                                    <?php $m = $executeTime % 60; ?>
                                    <?php if ($h > 0) : ?>
                                        <?php echo $h ?>時間
                                    <?php endif ?>
                                    <?php if ($m > 0) : ?>
                                        <?php echo $m ?>分
                                    <?php endif ?>
				</td>
			</tr>
		</table>

		<table class="table table-bordered text-center">
			<tbody id="load-order-detail">

			</body>
		</table>

		<table class="table table-bordered text-center">
			<tr>
				<th class="col-xs-4 col-sm-3">
					ご予約者名
				</th>
				<td>
					<?php echo HTML::encode($customer->name) ?>
				</td>
			</tr>
			<tr>
				<th>
					ご連絡先
				</th>
				<td>
					<?php echo HTML::encode($customer->mobile) ?>
				</td>
			</tr>
			<tr>
				<th>
					ご要望
				</th>
				<td>
					<?php echo HTML::encode(Yii::$app->session->get('booking-demand')) ?>
				</td>
			</tr>
		</table>
	</div>
	<div class="col-xs-12">
		<button type="button" class="btn btn-submit" id="btn-final">
			戻る
		</button>
	</div>
</div>
<?php echo $this->render('part/backButton', ['screen' => 7]) ?>
