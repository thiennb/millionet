<?php use common\components\Constants; ?>
<?php $display_conditions = Constants::DISPLAY_CONDITION; ?>
<?php if (count($items)) : ?>
    <div class="clearfix"></div>
    <?php foreach ($items as $item) : ?>
        <?php
        if (key_exists('apply_condition', $item)) {
            $mode  = 'coupon';
            $image = $item['image'];
            $price = 0;
            if (isset($price_table[$item['id']])) {
                $price = \common\models\BookingBusiness::displayMoney($price_table[$item['id']]);
            }
            //$price = isset($price_table[$item['id']]) ? Yii::$app->formatter->asDecimal((float) $price_table[$item['id']]['total_price'], 0) : 0;
        } elseif (key_exists('avatar', $item)) {
            $mode = 'staff';
            $image = $item['avatar'];
            $price = isset($price_table[$item['assign_product_id']]) ? Yii::$app->formatter->asDecimal((float) $price_table[$item['assign_product_id']]['total_price'], 0) : 0;
        } elseif (key_exists('type_seat_name', $item)) {
            $mode = 'seat';
            $image = $item['image1'];
            $price = 0;
        } else {
            $mode = 'product';
            $image = $item['image1'];
            $price = '¥' . (isset($price_table[$item['id']]) ? Yii::$app->formatter->asDecimal((float) $price_table[$item['id']]['total_price'], 0) : 0);
        }
        if (!empty($item['display_condition']) && isset($display_conditions[$item['display_condition']])) {
            $display_condition = $display_conditions[$item['display_condition']];
        } else {
            $display_condition = '';
        }
        ?>
        <?php
        echo $this->render('/bookingorder/part/selectItem_' . $mode, [
            'item' => $item,
            'mode' => $mode,
            'price' => $price,
            'display_condition' => $display_condition,
            'image' => $image
        ])
        ?>
    <?php endforeach ?>
    <div class="clearfix"></div>
<?php endif ?>
