<?php use common\components\Constants; ?>
<?php $display_conditions = Constants::DISPLAY_CONDITION; ?>
<?php if (count($items)) : ?>
    <div class="clearfix"></div>
    <?php foreach ($items as $item) : ?>
        <?php
        if (key_exists('apply_condition', $item)) {
            $mode  = 'coupon';
            $image = $item['image'];
            $price = 0;
            if (isset($price_table[$mode][$item['id']])) {
                $price = \common\models\BookingBusiness::displayMoney($price_table[$mode][$item['id']]);
            }
        } else {
            $mode = 'product';
            $image = $item['image1'];
            $price = 0;
            if (isset($price_table[$mode][$item['id']])) {
                $price = \common\models\BookingBusiness::displayMoney($price_table[$mode][$item['id']]);
            }
        }
        if (!empty($item['display_condition']) && isset($display_conditions[$item['display_condition']])) {
            $display_condition = $display_conditions[$item['display_condition']];
        } else {
            $display_condition = '';
        }
        ?>
        <?php
        echo $this->render('/bookingorder/part/selectItem_' . $mode, [
            'item' => $item,
            'mode' => $mode,
            'price' => $price,
            'display_condition' => $display_condition,
            'image' => $image
        ])
        ?>
    <?php endforeach ?>
    <div class="clearfix"></div>
<?php endif ?>
