<?php
	use yii\helpers\Html;
	?>
<div class="clearfix"></div>
<!-- CATEGORIES FILTER LIST -->
<div class="col-xs-12">
	<div class="panel panel-default filter-list" role="filter_menu" id="filter-list" aria-multiselectable="true">
                <div class="hidden-xs"><a data-toggle="collapse" id="a-filter-collapse" data-parent="#filter-list" href="#collapseOne"></a></div>
		<div id="btn-filter-collapse" class="panel-heading collapsed" aria-controls="collapseOne" aria-expanded="false">
                    <span><?php echo Yii::t('api', 'Coupon and product filters') ?></span>
		</div>
		<div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
			<div class="col-xs-12 filter-table">
				<div class="row">
				  <div class="col-xs-6">
					  <label class="checkbox" for="checkbox-coupons">
						<input type="checkbox" id="checkbox-coupons" class="filter-mode" value="coupons">
						<?php echo Yii::t('api', 'Filter by coupons') ?>
					</label>
				  </div>
				  <div class="col-xs-6 bordered">
					  <label class="checkbox" for="checkbox-products">
						<input type="checkbox" id="checkbox-products" class="filter-mode" value="products">
						<?php echo Yii::t('api', 'Filter by products') ?>
					</label>
				  </div>
				</div>
				<?php $last_index = end(array_keys($items)); ?>
				<?php foreach ($items as $i => $item) : ?>
				  <?php
					$className = ($item['coupon_count'] == 0) ? 'filter-category filter-no-coupons' : 'filter-category';
				  ?>
				  <?php if ($i % 2 == 0) : ?>
					  <div class="row">
				  <?php endif ?>
					  <div class="col-xs-6 <?php if ($i % 2 !== 0) echo 'bordered'; ?>">
						  <label class="checkbox" for="checkbox-category-<?php echo $i ?>">
							  <input type="checkbox" id="checkbox-category-<?php echo $i ?>" class="<?php echo $className ?>" value="<?php echo $item['id'] ?>">
							  <?php echo HTML::encode($item['name']) ?>
						  </label>
					  </div>
				  <?php if ($i === $last_index && $i % 2 == 0) : ?>
					  <div class="col-xs-6 bordered">
					  </div>
				  <?php endif ?>
				  <?php if ($i % 2 !== 0) : ?>
					  </div>
				  <?php endif ?>
				<?php endforeach ?>
			</div>
			<div class="panel-footer" style="z-index: 300">
			  <button type="button" class="btn btn-next text-center" id="btn-filter" style="text-align: center !important;">
				  <?php echo Yii::t('api', 'Filter') ?>
			  </button>
			</div>
			</div>
		</div>
	</div>
</div>
<!-- END CATEGORIES FILTER LIST -->
<div class="clearfix"></div>
