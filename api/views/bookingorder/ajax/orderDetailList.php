<?php

use common\models\BookingBusiness ?>
<?php if (count($coupons)) : ?>
    <tr>
        <th class="col-xs-4 col-sm-3 text-center" rowspan="<?php echo count($coupons) ?>">
            ご予約商品
        </th>
        <td style="text-align: center">
    <?php echo $items[$coupons[0]]['name'] ?>
        </td>
    </tr>
            <?php for ($i = 1; $i < count($coupons); $i++) : $id = $coupons[$i] ?>
        <tr>
            <td style="text-align: center">
        <?php echo $items[$id]['name'] ?>
            </td>
        </tr>
    <?php endfor ?>
<?php endif; ?>
<?php if (count($products)) : ?>
    <tr>
        <th class="col-xs-4 col-sm-3 text-center" rowspan="<?php echo count($products) ?>">
            ご予約商品
        </th>
        <td style="text-align: center">
    <?php echo $items[$products[0]]['name'] ?>
        </td>
    </tr>
            <?php for ($i = 1; $i < count($products); $i++) : $id = $products[$i] ?>
        <tr>
            <td style="text-align: center">
        <?php echo $items[$id]['name'] ?>
            </td>
        </tr>
    <?php endfor ?>
<?php endif; ?>
        <?php foreach ($options as $id => $option_name): ?>
    <tr>
        <th>
            <?php echo $option_name ?>
        </th>
        <td style="text-align: center">
    <?php echo $items[$id]['name'] ?>
        </td>
    </tr>
<?php endforeach ?>
<?php if ($staff_id !== false) : ?>
    <tr>
        <th>
            指名スタッフ
        </th>
        <td style="text-align: center">
    <?php echo $items[$staff_id]['name'] ?>
        </td>
    </tr>
<?php endif ?>
<tr>
    <th class="col-xs-4 col-sm-3 text-center">
        合計金額
    </th>
    <td style="text-align: center">
        ¥<?php echo BookingBusiness::formatMoney($total_price) ?>
    </td>
</tr>
<tr>
    <th class="text-center">
        ご利用ポイント
    </th>
    <td style="text-align: center">
<?php echo $point_use ?> P (現在ポイント <?php echo $total_point ?> P)
    </td>
</tr>
<tr>
    <th class="text-center">
        お支払予定金額
    </th>
    <td style="text-align: center">
        ¥<?php echo BookingBusiness::formatMoney($final_price) ?>
    </td>
</tr>
<?php if (isset($point_gained)) : ?>
    <tr>
        <th class="text-center">
            付与予定ポイント
        </th>
        <td style="text-align: center">
            <?php echo $point_gained ?>P
        </td>
    </tr>
<?php endif; ?>
