<?php $display_conditions = common\components\Constants::DISPLAY_CONDITION; ?>
<table class="table table-bordered">
    <thead>
        <th>
            選択済みメニュー
        </th>
        <th>
            料金
        </th>
        <th>
            所要時間(目安)
        </th>
    </thead>
    <tbody>
        <?php foreach ($items as $id => $item) : ?>
            <?php echo $this->render('/bookingorder/part/orderItem', [
                'item'  => $item,
                'mode'  => isset($item['apply_condition']) ? 'coupon' : 'product',
                'price' => isset($price_table[$id]) ? $price_table[$id] : 0,
                'display_condition' => !empty($item['display_condition']) && isset($display_conditions[$item['display_condition']]) ? $display_conditions[$item['display_condition']] : ''
            ]) ?>
        <?php endforeach ?>
    </tbody>
    <tbody id="selected-option-list"></tbody>
    <tbody>
        <tr>
            <td class="col-xs-6 text-align-right">
                <?php echo Yii::t('api', 'Total') ?>
                <input type="hidden" id="base-price" value="<?php echo $total_price ?>">
                <input type="hidden" id="base-time" value="<?php echo $total_time ?>">
            </td>
            <td class="col-xs-3" id="total-price">
                ¥<?php echo Yii::$app->formatter->asDecimal($total_price, 0) ?>
            </td>
            <td class="col-xs-3" id="total-time">
                <?php echo $total_time ?><?php echo Yii::t('api', 'minutes') ?>
            </td>
        </tr>
    </tbody>
</table>
