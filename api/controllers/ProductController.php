<?php
namespace api\controllers;
use common\models\MstProduct;
use common\models\MasterCoupon;
use api\models\ApiSupport;
use common\components\Constants;
use yii\helpers\ArrayHelper;
use common\models\BookingSearch;

class ProductController extends ApiController{
    public $modelClass = 'common\models\MstProduct';
    
    /*
     * Get list product method
     * 
     * @return json
     */
    public function actionList(){
        try{
            $store_id = \Yii::$app->getRequest()->getQueryParam('store_id');
            
            // Check valid parameters
            if(!$store_id){
                    return parent::result(1, \Yii::t('api', 'Parameter "{param}" is missing', ['param' => 'store_id']));
            }
            if(!is_numeric($store_id)){
                    return parent::result(1, \Yii::t('api', 'Parameter "{param}" is invalid', ['param' => 'store_id']));
            }
            
            $category_id= \Yii::$app->getRequest()->getQueryParam('category_id');
            $page = \Yii::$app->getRequest()->getQueryParam('page');
            $limit = \Yii::$app->getRequest()->getQueryParam('limit');
            
            // Check valid parameters
            if(!$page){
                    return parent::result(1, \Yii::t('api', 'Parameter "{param}" is missing', ['param' => 'page']));
            }
            if(!is_numeric($page)){
                    return parent::result(1, \Yii::t('api', 'Parameter "{param}" is invalid', ['param' => 'page']));
            }
            
            // Check valid parameters
            if(!$limit){
                    return parent::result(1, \Yii::t('api', 'Parameter "{param}" is missing', ['param' => 'limit']));
            }
            if(!is_numeric($limit)){
                    return parent::result(1, \Yii::t('api', 'Parameter "{param}" is invalid', ['param' => 'limit']));
            }
            
            $categories = null;
            if($category_id){
                // Check valid categories id
                $categories = explode('||', $category_id);
                foreach($categories as $cate){
                    if( !$cate || !is_numeric($cate)){
                        return parent::result(1, \Yii::t('api', 'Parameter "{param}" is invalid', ['param' => 'category_id']));
                    }
                }
            }
            
            $page_num = ($page - 1) * $limit;
            
            $products = $this->getProducts($store_id, $categories, $page_num, $limit);
            return parent::result(0, '', ['product_list' => $products, 'page' => $page]);
        } catch (Exception $ex) {
            return parent::result(1, $ex->getMessage());
        }
    }
    
    /*
     * Get products method
     * 
     * @return arrray product
     */
    
    private function getProducts($store_id, $categories, $page_num = 0, $limit = 20){
                $queryObject = MstProduct::find()
                                        ->select(['id',
                                                'store_id',
                                                'assign_fee_flg',
                                                'name',
                                                'name_kana',
                                                'unit_price',
                                                'description',
                                                'jan_code',
                                                'image1',
                                                'image2',
                                                'image3',
                                                'show_flg',
                                                'time_require',
                                                'created_at',
                                                'updated_at',
                                                'del_flg',
                                                'category_id',
                                                'time_require'
                                            ])
                                        ->where(['mst_product.del_flg' => 0])
                                        ->andWhere(['mst_product.show_flg' => 1])
                                        ->andWhere(['or', ['tel_booking_flg' => null], ['tel_booking_flg' => '0']]) // 27.10
                                        ->andWhere(['or', ['option_condition_flg' => null], ['option_condition_flg' => '0']]) // 27.10
                                        ->andWhere(['store_id' => $store_id]);
                
                if($categories){
                        $queryObject->andWhere(['category_id' => $categories]);
                }
                
                // Get product
                $products = $queryObject->with(['category', 'productOption.option', 'store'])
                                        ->groupBy('mst_product.id, mst_product.store_id')
                                        ->orderBy('mst_product.category_id ASC')
                                        ->limit($limit)
                                        ->offset($page_num)
                                        ->all();
                
                $price_table = ArrayHelper::index(BookingSearch::getProductPriceTable(implode(',', ArrayHelper::getColumn($products, 'id'))), 'id');

                // sort using price
                BookingorderController::sortProducts($products, $price_table);

                $__products = [];

                foreach ($products as $key => $value){
                        $__value = null;
                        foreach($value as $k => $pr){
                                $__value[$k] = $pr;
                        }
                        
                        $__value['barcode_url'] = ApiSupport::getBarcodeUrl($value['jan_code']);
                        
                        $__value['price'] = $value['unit_price'];
                        unset($__value['unit_price']);
                        
                        $__value['image'] = [];
                        if($value['image1']){
                                $__value['image'][] = ApiSupport::getFileFullUrl($value['image1']);
                        }
                        unset($__value['image1']);
                        
                        if($value['image2']){
                                $__value['image'][] = ApiSupport::getFileFullUrl($value['image2']);
                        }
                        unset($__value['image2']);
                        
                        if($value['image3']){
                                $__value['image'][] = ApiSupport::getFileFullUrl($value['image3']);
                        }
                        unset($__value['image3']);
                        
                        $_options = [];
                        
                        if($value->productOption){
                                foreach($value->productOption as $productOption){
                                        $_options[] = [
                                                "id" => $productOption->option? $productOption->option->id : null,
                                                "name" => $productOption->option? $productOption->option->name: null
                                        ];
                                }
                        }
                        
                        if($value->category){
                                $__value['categories'][] = [
                                        'id' => $value->category->id,
                                        'name' => $value->category->name
                                ];
                        } else {
                                $__value['categories'] = [];
                        }
                        unset($__value['category_id']);
                        
                        $__value['options'] = $_options;
                        
                        $__value['total_price'] = $price_table[$__value['id']]['total_price'];
                        
                        
                        $__products[] = $__value;
                }
                
                return $__products;
    }
}