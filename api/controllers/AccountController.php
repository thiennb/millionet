<?php
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace api\controllers;
use api\models\ApiLoginForm;
use api\models\ApiRegisterForm;
use common\models\User;
use common\models\MasterMember;
use common\models\MasterCustomer;
use api\models\ApiSupport;
use common\models\DeviceUser;
use yii;
use yii\helpers\ArrayHelper;
use DateTime;
use common\models\BookingBusiness;
use common\models\PostCode;

class AccountController extends ApiController
{
    //variable connection database
    public $db;
    
    public function init()
    {
        //get connection
        $this->db = Yii::$app->db;
    }
    
    public $modelClass = 'common\models\User';
    public $layout  = "main-login";

    /*
     * Login method
     */
    public function actionLogin(){
        try{
            \Yii::$app->response->format = \yii\web\Response::FORMAT_HTML;
            if (!\Yii::$app->user->isGuest) {
                return $this->getApiOnLogin();
            }

            $store_id = \Yii::$app->getRequest()->getQueryParam('store_id');
            if(!$store_id){
                return $this->getApiOnRegister(true, 'Store_id Null');
            }
            
            //return 123;
            $model = new ApiLoginForm();
            $model->store_id = $store_id;
            
            if(\Yii::$app->request->isPost){
                if ($model->load(\Yii::$app->request->post()) && $model->login()) {
                    return $this->getApiOnLogin();
                }
                
                return $this->render('_login_form', [
                         'model' => $model,
                ]);
            }
            
            return $this->render('login', [
                         'model' => $model,
                ]);
        } catch (Exception $ex) {
            // Todo when have error
            return $this->getApiOnLogin(true, $ex->getMessage());
        }
    }

    /*
     * Register action method
     * 
     */
    public function actionRegisterphone(){
        $store_id = \Yii::$app->getRequest()->getQueryParam('store_id');
        if(!$store_id){
            return $this->getApiOnRegister(true, 'Store_id Null');
        }
        try{
            \Yii::$app->response->format = \yii\web\Response::FORMAT_HTML;

            if(method_exists($this, 'registerStep1')){
                $session = Yii::$app->session;
                //remove session
                $session->remove('Step_1_mobile');
                $session->remove('Step_1_email');
                $session->remove('Key_confirm_email');
                $session->remove('Step_2');
                $session->remove('Step_3');
                $session->remove('store_id');
                return $this->{'registerStep1'}($store_id);
            }

            return $this->redirect('/api/register');
        } catch (Exception $ex) {
            return $this->getApiOnRegister(true, $ex->getMessage());
        }
    }
    /*
     * Register action method
     * 
     */
    public function actionRegisteremail(){
        $store_id = \Yii::$app->getRequest()->getQueryParam('store_id');
        if(!$store_id){
            return $this->getApiOnRegister(true, 'Store_id Null');
        }
        try{
            \Yii::$app->response->format = \yii\web\Response::FORMAT_HTML;

            if(method_exists($this, 'registerStepemail')){
                $session = Yii::$app->session;
                //remove session
                $session->remove('Step_1_mobile');
                $session->remove('Step_1_email');
                $session->remove('Key_confirm_email');
                $session->remove('Step_2');
                $session->remove('Step_3');
                $session->remove('store_id');
                return $this->{'registerStepemail'}($store_id);
            }

            return $this->redirect('/api/register');
        } catch (Exception $ex) {
            return $this->getApiOnRegister(true, $ex->getMessage());
        }
    }

    /*
     * Register action method
     * 
     */
    public function actionRegisterStep($step = 0){
        try{
            \Yii::$app->response->format = \yii\web\Response::FORMAT_HTML;
            //check exists session
            if(empty($step)){
                return $this->getApiOnRegister(true, 'No step');
            }
            if(method_exists($this, 'registerStep'.$step)){
                return $this->{'registerStep'.$step}();
            }

            return $this->redirect('/api/register');
        } catch (Exception $ex) {
            return $this->getApiOnRegister(true, $ex->getMessage());
        }
    }

    /*
     * registerStep1
     * 
     * @return object
     */
    private function registerStep1($store_id = 0){
        try{
            //check exists session
            $session = Yii::$app->session;
            $session->remove('Step_1_email');
            if (empty($session->get('Step_1_mobile'))) {
                $model = new MasterMember();
            }else{
                $model = new MasterMember();
                $model->mobile = $session->get('Step_1_mobile');
            }
            //set Scenario for rules validate in model
            $model->setScenario('create_phone');
            //check init or submit form
            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                //check phone exists
                $phone = MasterCustomer::findOne(['mobile'=>$model->phone_1.$model->phone_2.$model->phone_3]);
                if(!empty($phone->mobile)){//if exists phone in database
                    $model->addError('phone_1', Yii::t('api',"Sory Phone Number was been used."));
                    $model->addError('phone_2', '');
                    $model->addError('phone_3', '');
                    return $this->render('register/step1', [
                            'model' => $model,
                    ]);
                }
                $session->set('Step_1_mobile', $model->phone_1.$model->phone_2.$model->phone_3);
                $session->set('store_id', $store_id);
                return $this->redirect('/api/register/step=2');
            } else {            
                if(strlen($model->mobile) == 10){
                    $model->phone_1 = substr($model->mobile, 0, 2);
                    $model->phone_2 = substr($model->mobile, 2, 4);
                    $model->phone_3 = substr($model->mobile, 6, 4);
                } else{
                    $model->phone_1 = substr($model->mobile, 0, 3);
                    $model->phone_2 = substr($model->mobile, 3, 4);
                    $model->phone_3 = substr($model->mobile, 7, 4);
                }
                return $this->render('register/step1', [
                            'model' => $model,
                ]);
            }
        } catch (Exception $ex) {//if have exception 
            //set error message
            Yii::$app->getSession()->setFlash('error', [
                'error'=>Yii::t('api',"System error happen. Please contact with System manager.")
                ]);
            //return index
            return $this->redirect('/api/registerphone/store='.$store_id);
        } 
    }
    
    /**
     * Create member step 1 with email
     * @return mixed
     */
    public function registerStepemail($store_id = 0) {
        try{
            //check exists session
            $session = Yii::$app->session;
            $session->remove('Step_1_mobile');
            if (empty($session->get('Step_1_email'))) {
                $model = new MasterMember();
            }else{
                $model = new MasterMember();
                $model->email = $session->get('Step_1_email');
            }
            //set Scenario for rules validate in model
            $model->setScenario('step_1_email');
            //check init or submit form
            if ($model->load(Yii::$app->request->post()) && $model->validate()) {//submit form
                //check phone exists
                $email = MasterCustomer::findOne(['email'=>$model->email]);
                if(!empty($email->email)){//if exists phone in database
                    Yii::$app->getSession()->setFlash('error', [
                    'error'=>Yii::t('api',"Sory Email was been used.")
                    ]);
                    return $this->render('register/step1_email', [
                            'model' => $model,
                    ]);
                }
                $model->key_confirm_again = Yii::$app->security->generateRandomString(6);
                $model->sendKeyConfirmEmail($model);
                $session->set('Step_1_email', $model->email);
                $key_confirm_email = [];
                $key_confirm_email['key'] =    $model->key_confirm_again;
                $key_confirm_email['time'] =    date('Y-m-d H:i:s');
                $session->set('Key_confirm_email', $key_confirm_email);
                $session->set('store_id', $store_id);
                return $this->redirect('/api/register/step=2');
            } else {//init
                return $this->render('register/step1_email', [
                            'model' => $model,
                ]);
            }
        } catch (Exception $ex) {//if have exception 
            //set error message
            Yii::$app->getSession()->setFlash('error', [
                'error'=>Yii::t('api',"System error happen. Please contact with System manager.")
                ]);
            //return index
            return $this->redirect('/api/registeremail/store_id='.$store_id);
        }        
    }

    /*
     * registerStep2
     * 
     * @return object
     */
    private function registerStep2($store = 0){
        try {
            //get session
            $session = Yii::$app->session;
            if(!empty($session->get('Step_1_mobile'))){
                $model = new MasterMember();
                $model->mobile = $session->get('Step_1_mobile');
                $model->mode = MasterMember::REGISTER_PHONE;
            }else if(!empty($session->get('Step_1_email'))){
                $model = new MasterMember();
                $model->email = $session->get('Step_1_email');
                $model->mode = MasterMember::REGISTER_EMAIL;
                $key_confirm_again = $session->get('Key_confirm_email');
                $model->key_confirm_again = $key_confirm_again['key'];
                $now = DateTime::createFromFormat('Y-m-d H:i:s',date('Y-m-d H:i:s'));
                $time_key = DateTime::createFromFormat('Y-m-d H:i:s',date('Y-m-d H:i:s',  strtotime($key_confirm_again['time'])));
                if(($now->getTimestamp() - $time_key->getTimestamp()) > 30*60){
                    //set error message
                    Yii::$app->getSession()->setFlash('error', [
                        'error'=>Yii::t('api',"Time of the authentication key input has exceeded the time limit.")
                        ]);
                    //return index
                    return $this->redirect('/api/register/step=email');
                }
            }else{
                Yii::$app->getSession()->setFlash('error', [
                    'error'=>Yii::t('frontend',"You must create step 1!")
                ]);
                return $this->redirect('step1');
            }
            //set Scenario for rules validate in model
            $model->setScenario('step_2');
            //check init or submit
            if ($model->load(Yii::$app->request->post()) && $model->validate()) {//if submit
                if ($model->mode == \common\models\MasterMember::REGISTER_EMAIL && !($model->key_confirm == $model->key_confirm_again)) {                    
                    $model->addError('key_confirm', Yii::t('frontend',"Key confirm is not correct."));
                    return $this->render('register/step2', [
                                'model' => $model,
                    ]);
                }
                //save data in session
                $session->set('Step_2',  'true');
                return $this->redirect('/api/register/step=3');
            } else {
                return $this->render('register/step2', [
                            'model' => $model,
                ]);
            }
        } catch (Exception $ex) {
            //set error message
            Yii::$app->getSession()->setFlash('error', [
                'error'=>Yii::t('api',"System error happen. Please contact with System manager.")
                ]);
            //return index
            return $this->redirect('/api/register/step=2');
        }
    }

    /*
     * registerStep3
     * 
     * @return object
     */
    private function registerStep3($store = 0){
        try {
            //get session
            $session = Yii::$app->session;
            if (!empty($session->get('Step_3'))) {
                $model = new MasterMember();
                $model->load($session->get('Step_3')->post());
                if(!empty($session->get('Step_1_mobile'))){
                    $model->mobile = $session->get('Step_1_mobile');
                    $model->mode = MasterMember::REGISTER_PHONE;
                }else{
                    $model->email = $session->get('Step_1_email');
                    $model->mode = MasterMember::REGISTER_EMAIL;
                }
            }else if(!empty($session->get('Step_1_mobile'))  && !empty($session->get('Step_2'))){
                $model = new MasterMember();
                $model->mobile = $session->get('Step_1_mobile');
                $model->mode = MasterMember::REGISTER_PHONE;
            }else if(!empty($session->get('Step_1_email'))  && !empty($session->get('Step_2'))){
                $model = new MasterMember();
                $model->email = $session->get('Step_1_email');
                $model->mode = MasterMember::REGISTER_EMAIL;
            }else{
                Yii::$app->getSession()->setFlash('error', [
                    'error'=>Yii::t('frontend',"You must create step 1!")
                ]);
                return $this->redirect('step1');
            }
            //set Scenario for rules validate in model
            ($model->mode == \common\models\MasterMember::REGISTER_EMAIL)?$model->setScenario('step_3_email'):$model->setScenario('step_3');
            
            //check init or submit
            if ($model->load(Yii::$app->request->post()) && $model->validate()) {//if submit form
                //check exists email
                $email = MasterCustomer::findOne(['email'=>$model->email]);
                if(!empty($email->email)){
                    $key = 'email';
                    $model->addError($key, Yii::t('api',"Sory Email was been used."));
                    return $this->render('register/step3', [
                                'model' => $model,
                    ]);
                }
                //check exists phone
                $mobile = (!empty($model->mobile))?$model->mobile:$model->phone_1.$model->phone_2.$model->phone_3;
                $phone = MasterCustomer::findOne(['mobile'=>$mobile]);
                if(!empty($phone->mobile)){
                    $model->addError('phone_1', Yii::t('api',"Sory Phone Number was been used."));
                    $model->addError('phone_2', '');
                    $model->addError('phone_3', '');
                    return $this->render('register/step3', [
                                'model' => $model,
                    ]);
                }
                //save data in session
                $session->set('Step_3', Yii::$app->request);
                return $this->redirect('/api/register/step=4');
            } else {//if init     
                return $this->render('register/step3', [
                            'model' => $model,
                ]);
            }
        } catch (Exception $ex) {
            //set error message
            Yii::$app->getSession()->setFlash('error', [
                'error'=>Yii::t('api',"System error happen. Please contact with System manager.")
                ]);
            //return index
            return $this->redirect('/api/register/step=1');
        }
    }

    /*
     * registerStep4
     * 
     * @return object
     */
    private function registerStep4($store = 0){
        try {
            //create transaction
            $transaction = $this->db->beginTransaction(); 
            //get session
            $session = Yii::$app->session;
            if ( empty($session->get('Step_3')) ){
                Yii::$app->getSession()->setFlash('error', [
                    'error'=>Yii::t('frontend',"You must create step 1!")
                ]);
                return $this->redirect('step1');
            }
            $model = new MasterMember();
            $model->mobile = $session->get('Step_1_mobile');
            $model->email = $session->get('Step_1_email');
            $model->load($session->get('Step_3')->post());            
            $model->first_name_kana = mb_convert_kana($model->first_name_kana,'k');
            $model->last_name_kana = mb_convert_kana($model->last_name_kana,'k');
            //check init or submit form
            if (Yii::$app->request->post()) {//if submit
                $model->mobile = (!empty($model->mobile))?$model->mobile:$model->phone_1.$model->phone_2.$model->phone_3;
                $store_id = $session->get('store_id');
                $model->register_store_id = $store_id;
                $store = \common\models\MasterStore::findOne($store_id);
                if(!empty($store)){
                    $model->company_id = $store->company_id;
                }else{
                    //rollback transaction
                    $transaction->rollBack();
                    //set error message
                    Yii::$app->getSession()->setFlash('error', [
                        'error'=>Yii::t('api',"System error happen. Please contact with System manager.")
                        ]);
                    //return step 3
                    return $this->redirect('/api/register/step=3');
                }
                
                $model->detachBehavior('CompanyIdBehavior');
                //save database and login
                $model->saveData($model);
                //send email
                if(!empty($model->email)){
                    $model->sendEmailRegister($model);
                }
                BookingBusiness::PushMessage(\common\models\BookingBusiness::PUSH_TIMING_REGISTER_MEMBER,$model->id);
                BookingBusiness::PushMessage(\common\models\BookingBusiness::PUSH_TIMING_REGISTER_MEMBER_AFTER,$model->id);
                //remove session
                $session->remove('Step_1_mobile');
                $session->remove('Step_1_email');
                $session->remove('Key_confirm_email');
                $session->remove('Step_2');
                $session->remove('Step_3');
                $session->remove('store_id');
                $transaction->commit();
                //login
                $login = new ApiLoginForm();
                $login->login(User::findOne(\Yii::$app->user->getId()));
                
                return $this->redirect('/api/register/step=5');
                //return $this->redirect('/api/register/step=5');
            } else {//if init
                if(!empty($model->mobile)){
                    if(strlen($model->mobile) == 10){
                        $model->phone_1 = substr($model->mobile, 0, 2);
                        $model->phone_2 = substr($model->mobile, 2, 4);
                        $model->phone_3 = substr($model->mobile, 6, 4);
                    } else{
                        $model->phone_1 = substr($model->mobile, 0, 3);
                        $model->phone_2 = substr($model->mobile, 3, 4);
                        $model->phone_3 = substr($model->mobile, 7, 4);
                    }
                }
                return $this->render('register/step4',[
                            'model' =>  $model
                    ]);
            }
        } catch (Exception $ex) {
            //rollback transaction
            $transaction->rollBack();
            //set error message
            Yii::$app->getSession()->setFlash('error', [
                'error'=>Yii::t('api',"System error happen. Please contact with System manager.")
                ]);
            //return step 3
            return $this->redirect('/api/register/step=3');
        }
    }

    /*
     * registerStep4
     * 
     * @return object
     */
    private function registerStep5(){
        try {
            if (Yii::$app->request->post()) {//if submit                
                return $this->getApiOnRegister();
                //return $this->redirect('/api/register/step=5');
            } else {//if init
                $customerinfo = MasterCustomer::find()->where(['user_id' => \Yii::$app->user->getId()])->one();
                $json = json_encode([
                'error_code' => 0,
                'message' => '',
                'user_id' => \Yii::$app->user->getId(),
                'customer_jan_code' => $customerinfo->customer_jan_code,
                'token' => ApiSupport::getApiToken(\Yii::$app->user->getId(), 12*30*24*60*60)
                ]);
                return $this->render('register/step5',['json' => $json]);
            }
        } catch (Exception $ex) {
            //set error message
            Yii::$app->getSession()->setFlash('error', [
                'error'=>Yii::t('api',"System error happen. Please contact with System manager.")
                ]);
            //return step 3
            return $this->redirect('/api/register/step=3');
        }
    }

    /*
     * logout method
     */
    public function actionLogout(){
        $transaction = \Yii::$app->db->beginTransaction();

        try{
            $requestData = \Yii::$app->getRequest()->post();

            if(!$requestData){
                return parent::result(1, \Yii::t('api', 'Request method is not supported'));
            }

            $user = ApiSupport::isCustomerLogged();
            // Check valid parameters
            $message = "";
            if(!$user){
                $message = \Yii::t('api', "You must login before");
            }elseif(!isset($requestData['device_token'])){
                $message =  \Yii::t('api', 'Parameter "{param}" is missing', ['param' => 'device_token']);
            }elseif(!isset($requestData['store_id']) || !is_numeric($requestData['store_id'])){
                $message =  \Yii::t('api', 'Parameter "{param}" is missing', ['param' => 'store_id']);
            }

            if($message){
                return parent::result(1, $message);
            }

            $transaction->commit();

            \Yii::$app->user->logout();

            return parent::result(0, '');
        } catch (Exception $ex) {
            $transaction->rollBack();

            return parent::result(1, $ex->getMessage());
        }
    }

    /*
     * Get api on login
     * 
     * @param $exception
     * @return string
     */
    private function getApiOnLogin($exception = false, $message = ''){
        
        if(!\Yii::$app->user->isGuest && !$exception){
            $customerinfo = MasterCustomer::find()->where(['user_id' => \Yii::$app->user->getId()])->one();

            $json = json_encode([
                'error_code' => 0,
                'message' => '',
                //'action_code' => 'ACTION_LOGIN',
                'user_id' => \Yii::$app->user->getId(),
                'customer_jan_code' => $customerinfo->customer_jan_code,
                'token' => ApiSupport::getApiToken(\Yii::$app->user->getId(), 12*30*24*60*60)
            ]);
        } else{
            $json = json_encode([
                'error_code' => 1,
                'message' => $message,
                //'action_code' => 'ACTION_LOGIN',
                'user_id' => '',
                'customer_jan_code' => '',
                'token' => ''
            ]);
        }
        $this->layout = false;
        return $this->render('api-view', ['json'  => $json, 'type' => 'Login']);
    }

    /*
     * Get api on login
     * 
     * @param $exception
     * @return string
     */
    private function getApiCheck($message = '', $type, $id = null){
        if($type == 'LoginCheck'){
            $json = json_encode([
            'error_code' => 1,
            'customer_jan_code' => '',
            'message' => $message,
            ]);
        } else if($type == 'onUserInfoChanged'){
            $customerinfo = MasterCustomer::find()->where(['user_id' => $id])->one();
            $json = json_encode([
                'error_code' => 0,
                'customer_jan_code' => $customerinfo->customer_jan_code,
                'message' => '',
            ]);
        }
        return $this->render('api-check', ['json'  => $json, 'type' => $type]);
    }
    
    /*
     * Get api on register
     * 
     * @param $exception
     * @return string
     */
    private function getApiOnRegister($exception = false, $message = ''){
        if(!$exception){
            $customerinfo = MasterCustomer::find()->where(['user_id' => \Yii::$app->user->getId()])->one();
            $json = json_encode([
                'error_code' => 0,
                'message' => '',
                'user_id' => \Yii::$app->user->getId(),
                'customer_jan_code' => $customerinfo->customer_jan_code,
                'token' => ApiSupport::getApiToken(\Yii::$app->user->getId(), 12*30*24*60*60)
            ]);
        } else {
            $json = json_encode([
                'error_code' => 1,
                'message' => $message,
                'user_id' => '',
                'customer_jan_code' => '',
                'token' => ''
            ]);
        }
        $this->layout = false;
        return $this->render('api-view', ['json'  => $json, 'type' => 'Register']);
    }
    
    /**
     * Update info of member
     * @param integer $id
     * @return mixed
     */
    public function actionUpdateinfo($id = null) {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_HTML;
        //Get parameter
        $token = \Yii::$app->request->headers->get('Authorization');
        if($token == null){
            $token = \Yii::$app->session->get('token_param');
            if($token == null){
                return $this->getApiCheck('You must login before', 'LoginCheck', null);
            }
        }
        
        $session = \Yii::$app->session;
        \Yii::$app->session->set('token_param',$token);
        $tokenObject = ApiSupport::isValidHeaderAuthentication($token);

        $isLogged = ApiSupport::isLoginOnWebview($token);
        $id = $isLogged->id;
        try {
            
            //find member
            $model = self::findModel($id);
            $request = $session->get('Update_member_request');
            if (!empty($request) && !empty($id)) {
                $model->load($request->post());
                $model->post_code_1 = $request->post()['MasterMember']['post_code_1'];
                $model->post_code_2 = $request->post()['MasterMember']['post_code_2'];
                $model->post_code = $model->post_code_1.$model->post_code_2;
            }
            //set Scenario for rules validate
            $model->setScenario('update');
            $prefecture = PostCode::getListPrefecture();
            //check init or submit
            if ($model->load(Yii::$app->request->post()) && $model->validate()) {//if submit form and validate success  
                (!empty($model->post_code_1) && !empty($model->post_code_2)) ? $model->post_code = $model->post_code_1 . $model->post_code_2 : $model->post_code;
                $session->set('Update_member_request', Yii::$app->request);
                return $this->redirect(['updateconfirm','id'=>$id]);
            } else {//if init
                if(!empty($model->post_code)){
                    $model->post_code_1 = substr($model->post_code,0,3);
                    $model->post_code_2 = substr($model->post_code,3,4);
                };
                return $this->render('register/update', [
                    'model' => $model,
                    'prefecture'   =>  $prefecture,
                ]);
            }
        } catch (Exception $ex) {
            //set error message
            Yii::$app->getSession()->setFlash('error', [
                'error'=>Yii::t('api',"System error happen. Please contact with System manager.")
                ]);
            //return index
            return $this->redirect(['/']);
        }        
    }
    
    /**
     * Confirm update member
     * @return mixed
     */
    public function actionUpdateconfirm($id) {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_HTML;
        $token = \Yii::$app->session->get('token_param');
        if($token == null){
            return $this->getApiCheck('You must login before', 'LoginCheck', null);
        }
        try {
            //get session
           $session = \Yii::$app->session;
            if (!empty($session->get('Update_member_request'))) {
                $model = self::findModel($id);
                $model->load($session->get('Update_member_request')->post());                
                $model->post_code_1 = $session->get('Update_member_request')->post()['MasterMember']['post_code_1'];
                $model->post_code_2 = $session->get('Update_member_request')->post()['MasterMember']['post_code_2'];
                $model->post_code = $model->post_code_1.$model->post_code_2;
                $model->first_name_kana = mb_convert_kana($model->first_name_kana,'k');
                $model->last_name_kana = mb_convert_kana($model->last_name_kana,'k');
                $model->setScenario('update');
            }else{
                //set error message
                Yii::$app->getSession()->setFlash('error', [
                    'error'=>Yii::t('api',"System error happen. Please contact with System manager.")
                    ]);
                //return index
                return $this->redirect(['updateinfo','id'=>$id]);
            }
            //create transaction
            $transaction = $this->db->beginTransaction(); 
            //check init or submit
            if (Yii::$app->request->post()) {//if submit form and save success      
                $model->updated_by = $model->user_id;
                $model->detachBehavior('CompanyIdBehavior');
                $model->save();
                $transaction->commit();
                $session->remove('Update_member_request');
                return $this->redirect(['updateconfirmcomplete', 'id' => $model->id]);
//                return $this->getApiCheck('', 'OnExit');
            } else {//if init
                //init data for field in screen
                return $this->render('register/update_confirm', [
                            'model' => $model,
                ]);
            }
        } catch (Exception $ex) {
            //rollback transaction
            $transaction->rollBack();
            //set error message
            Yii::$app->getSession()->setFlash('error', [
                'error'=>Yii::t('api',"System error happen. Please contact with System manager.")
                ]);
            //return index
            return $this->redirect(['/']);
        }        
    }
    
    
    /**
     * Confirm update member
     * @return mixed
     */
    public function actionUpdateconfirmcomplete($id) {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_HTML;
        $token = \Yii::$app->session->get('token_param');
        if($token == null){
            return $this->getApiCheck('You must login before', 'LoginCheck', null);
        }
        try {
            //get session
            $model = self::findModel($id);
            //check init or submit
            if (Yii::$app->request->post()) {
                //exit api
                return $this->getApiCheck('', 'onUserInfoChanged', $model->user_id);
            } else {//if init
                //init data for field in screen
                $customerinfo = MasterCustomer::findOne(['id'=>$id]);
                $json = json_encode([
                'error_code' => 0,
                'message' => '',
                'user_id' => $model->user_id,
                'customer_jan_code' => $customerinfo->customer_jan_code,
                'token' => ApiSupport::getApiToken($model->user_id, 12*30*24*60*60)
                ]);
                
                return $this->render('register/update_complete', [
                            'model' => $model,'json' => $json]);
            }
        } catch (Exception $ex) {
            //set error message
            Yii::$app->getSession()->setFlash('error', [
                'error'=>Yii::t('api',"System error happen. Please contact with System manager.")
                ]);
            //return index
            return $this->redirect(['/']);
        }        
    }
    /**
     * Update phone of member
     * @param integer $id
     * @return mixed
     */
    public function actionUpdatephone($id) {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_HTML;
        $token = \Yii::$app->session->get('token_param');
        if($token == null){
            return $this->getApiCheck('You must login before', 'LoginCheck', null);
        }
        try {
            //create transaction
            $transaction = $this->db->beginTransaction(); 
            //get session
            $session = \Yii::$app->session;
            //find member
            $model = self::findModel($id);
            //set Scenario for rules validate
            $model->setScenario('update_phone');
            $model->birth_date = !empty($model->birth_date)?date('Y/m/d', strtotime($model->birth_date)):$model->birth_date;
            //check init or submit
            if ($model->load(Yii::$app->request->post()) && $model->validate()) {//if submit
                //save data in session
                $mobile = $model->phone_1_new.$model->phone_2_new.$model->phone_3_new;
                $check_mobile = MasterMember::find()->andWhere(['<>','id',$id])->andWhere(['mobile'=>$mobile])->one();
                if(!empty($check_mobile)){
                    $model->addError('phone_1_new', Yii::t('frontend', "Sory Phone Number was been used."));
                    $model->addError('phone_2_new', '');
                    $model->addError('phone_3_new', '');                    
                    return $this->render('register/update_phone', [
                                'model' => $model,
                    ]);
                }
                //save data in session
                $session->set('Update_phone_request', $mobile);
                return $this->redirect(['updatephonekeyconfirm', 'id' => $model->id]);
            } else {//if init
                if(strlen($model->mobile) == 10){
                    $model->phone_1 = substr($model->mobile, 0, 2);
                    $model->phone_2 = substr($model->mobile, 2, 4);
                    $model->phone_3 = substr($model->mobile, 6, 4);
                } else{
                    $model->phone_1 = substr($model->mobile, 0, 3);
                    $model->phone_2 = substr($model->mobile, 3, 4);
                    $model->phone_3 = substr($model->mobile, 7, 4);
                }
                return $this->render('register/update_phone', [
                            'model' => $model,
                ]);
            }
        } catch (Exception $ex) {
            //rollback transaction
            $transaction->rollBack();
            //set error message
            Yii::$app->getSession()->setFlash('error', [
                'error'=>Yii::t('api',"System error happen. Please contact with System manager.")
                ]);
            //return index
            return $this->redirect(['updateinfo', 'id' => $model->id]);
        }        
    }

    /**
     * Input key confirm when update phone number
     * @param integer $id
     * @return mixed
     */
    public function actionUpdatephonekeyconfirm($id) {     
        \Yii::$app->response->format = \yii\web\Response::FORMAT_HTML;
        $token = \Yii::$app->session->get('token_param');
        if($token == null){
            return $this->getApiCheck('You must login before', 'LoginCheck', null);
        }
        try {
            //create transaction
            $transaction = $this->db->beginTransaction(); 
            //get session
            $session = \Yii::$app->session;
            $mobile = $session->get('Update_phone_request');            
            if ( empty($mobile) ){
                Yii::$app->getSession()->setFlash('error', [
                    'error'=>Yii::t('backend',"You must create step 1!")
                ]);
                return $this->redirect(['update-phone','id' => $id]);
            }
            $model = $model = self::findModel($id);
            $model->mobile = $mobile;
            $model->birth_date = !empty($model->birth_date)?date('Y/m/d', strtotime($model->birth_date)):$model->birth_date;
            $model_user = User::findOne($model->user_id);
            //check if init or submit form
            if ($model->load(Yii::$app->request->post())) {//if submit form and save success
                $model->updated_by = $model->user_id;
                $model->save();
                $model_user->tel_no = $model->mobile;
                $model_user->save();
                $transaction->commit();
                $session->remove('Update_phone_request');
                return $this->redirect(['updateconfirmcomplete', 'id' => $model->id]);
            } else {//if init
                return $this->render('register/update_phone_key_confirm', [
                            'model' => $model,
                ]);
            }
        } catch (Exception $ex) {
            //rollback transaction
            $transaction->rollBack();
            //set error message
            Yii::$app->getSession()->setFlash('error', [
                'error'=>Yii::t('api',"System error happen. Please contact with System manager.")
                ]);
            //return index
            return $this->redirect(['updateinfo', 'id' => $model->id]);
        }
    }
    
    /**
     * Update email for member
     * @param integer $id
     * @return mixed
     */
    public function actionUpdateemail($id) {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_HTML;
        $token = \Yii::$app->session->get('token_param');
        if($token == null){
            return $this->getApiCheck('You must login before', 'LoginCheck', null);
        }   
        try {
            //create transaction
            $transaction = $this->db->beginTransaction(); 
            //find member
            $model = self::findModel($id);
            //set Scenario for rules validate 
            $model->setScenario('update_email');
            $model->birth_date = !empty($model->birth_date)?date('Y/m/d', strtotime($model->birth_date)):$model->birth_date;
            //check init or submit form
            if ($model->load(Yii::$app->request->post())) {//if submit form and save data success
                //send email
                $model->updated_by = $model->user_id;
                if(!$model->save()){
                    return $this->render('register/update_email', [
                        'model' => $model,
                        'email' =>  self::findModel($id)->email,
                    ]);
                }
                $model->sendEmailRegister($model); 
                $transaction->commit();
                return $this->redirect(['updateconfirmcomplete', 'id' => $model->id]);
            } else {
                return $this->render('register/update_email', [
                            'model' => $model,
                            'email' =>  self::findModel($id)->email,
                ]);
            }
        } catch (Exception $ex) {
            //rollback transaction
            $transaction->rollBack();
            //set error message
            Yii::$app->getSession()->setFlash('error', [
                'error'=>Yii::t('api',"System error happen. Please contact with System manager.")
                ]);
            //return index
            return $this->redirect(['updateinfo', 'id' => $model->id]);
        }
    }
    
    /**
     * Update password of member
     * @param integer $id
     * @return mixed
     */
    public function actionUpdatepassword($id) {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_HTML;
        try {
            $token = \Yii::$app->session->get('token_param');
            if($token == null){
                return $this->getApiCheck('You must login before', 'LoginCheck', null);
            }
            //create transaction
            $transaction = $this->db->beginTransaction(); 
            //find member
            $model = self::findModel($id);
            //Set Scenario for rules validate
            $model->setScenario('update_password');
            $model->birth_date = !empty($model->birth_date)?date('Y/m/d', strtotime($model->birth_date)):$model->birth_date;
            //check init or submit form
            if ($model->load(Yii::$app->request->post()) && $model->validate()) {//if submit form
                //check user password old
                $user = User::findOne($model->user_id);
                if (!$user->validatePassword($model->password_old)){
                    $model->addError('password_old', Yii::t('backend','Old password is incorrect.'));
                    return $this->render('register/update_password', [
                            'model' => $model,
                    ]);
                }
                //save password new
                $user->setPassword($model->password);
                $user->save();
                //send mail
                if(!empty($model->email)){
                    $model->sendEmailRegister($model);
                }
                $transaction->commit();
                return $this->redirect(['updateconfirmcomplete', 'id' => $model->id]);
            } else {
                return $this->render('register/update_password', [
                            'model' => $model,
                ]);
            }
        } catch (Exception $ex) {
            //rollback transaction
            $transaction->rollBack();
            //set error message
            Yii::$app->getSession()->setFlash('error', [
                'error'=>Yii::t('api',"System error happen. Please contact with System manager.")
                ]);
            //return index
            return $this->redirect(['updateinfo', 'id' => $model->id]);
        }
        
    }
    
    /**
     * Finds the MasterMember model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return MasterMember the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = MasterMember::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    /**
     * Lists all MasterStaff models.
     * @return mixed
     */
    public function actionGetpostcode()
    {
      
      $code = Yii::$app->request->post('postCode');
      $result = PostCode::find()->select(['country_name'])->andWhere(['post_code' => $code])->one();
      if($result != false ) return $result->country_name;
      return false;
      
    }
}