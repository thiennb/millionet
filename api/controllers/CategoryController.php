<?php

    /* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace api\controllers;
use common\models\MstCategoriesProduct;
use common\models\MstProduct;

class CategoryController extends ApiController
{
    public $modelClass = 'common\models\MstCategoriesProduct';
    
        /*
         * Get category list method
         * 
         * @return json
         */
        public function actionList(){
            try{
                    $store_id = \Yii::$app->getRequest()->getQueryParam('store_id');
                    if(!$store_id){
                            $categories = MstCategoriesProduct::find()
                                    ->select([
                                            'mst_product_category.id',
                                            'mst_product_category.name',
                                            'mst_product_category.del_flg'
                                    ])
                                    ->andWhere(['mst_product_category.del_flg' => 0])
                                    ->orderBy('mst_product_category.name')
                                    ->all();
                    }else{
                            $categories = MstCategoriesProduct::find()
                                    ->select([
                                            'mst_product_category.id',
                                            'mst_product_category.name',
                                            'mst_product_category.del_flg'
                                    ])
                                    ->innerJoin('mst_product', 'mst_product.category_id = mst_product_category.id ')
                                    ->innerJoin('mst_store', 'mst_store.id = mst_product.store_id')
                                    ->where(['mst_store.id' => $store_id])
                                    //->andWhere(['mst_product_category.del_flg' => 0])
                                    ->orderBy('mst_product_category.name')
                                    ->all();
                    }
                    
                    return parent::result(0, '',['categories' => $categories]);
            } catch (Exception $ex) {
                    return parent::result(1, $ex->getMessage());
            }
        }
}