<?php
namespace api\controllers;
use common\models\CustomerStore;
use common\models\MasterCustomer;
use common\components\Constants;
use api\models\ApiSupport;
use common\models\ChargeHistory;
use common\models\MasterRank;

class CustomerController extends ApiController{
        public $modelClass = 'common\models\MasterRank';
        
        /*
         * Get rank info
         * 
         * @return json
         */
        public function actionRank_info(){
            try{
                //Get parameter
                $isLogged = ApiSupport::isCustomerLogged();

                // Check user_id is invalid or null
                if(!$isLogged){
                    return parent::result(1, \Yii::t('api','You must login before'));
                }

                $store_id = \Yii::$app->getRequest()->getQueryParam('store_id');

                if(!$store_id){
                    return parent::result(1, \Yii::t('api', 'Parameter "{param}" is missing', ['param' => 'store_id']));
                }

                if(!is_numeric($store_id)){
                    return parent::result(1, \Yii::t('api', 'Parameter "{param}" is invalid', ['param' => 'store_id']));
                }

                // Get customer
                $customer = CustomerStore::find()
                            ->select(['customer_store.*'])
                            ->with(['masterStore.masterCompany'])
                            ->where(['customer_store.store_id' => $store_id])
                            ->andWhere(['customer_store.customer_id' => $isLogged->id])
                            ->andWhere(['customer_store.del_flg' => 0])
                            ->one();

                if(!$customer ){
                    return parent::result(1, \Yii::t('api', '{param} is not found', ['param' => 'Customer']));
                }

                // Get rank setting
                $setting = MasterRank::find()
                                ->where(['rank.del_flg' => 0])
                                ->andWhere(['rank.company_id' => $customer->masterStore->masterCompany->id])
                                ->one();

                if(!$setting){
                    return parent::result(1, \Yii::t('api', '{param} is not found', ['param' => 'Rank-Setting']));
                }
                
                // Get user experience
                $__userprocess = $this->getNextRatioRank($customer, $setting);
 
                return parent::result(0, '', ['rank' => $__userprocess]);
            } catch (Exception $ex) {
                    return parent::result(1, $ex->getMessage());
            }
        }
        
        /*
         * Get rank ratio rank
         * 
         * @param object $customer
         * @param object $setting
         * @return array
         */
        private function getNextRatioRank($__CUS, $__SETTING){
            $__RANK = Constants::RANK;

            // Get next rank
            $currentRankInt = (int)($__CUS->rank_id === null || $__CUS->rank_id == '' ? 0 : $__CUS->rank_id);
            
//            if($customerRankInt < 4){
//                $customerRankInt++;
//            }
            $nextRankInt = '';
            if($__SETTING->black_status == 1 && $currentRankInt < 4){
                $nextRankInt = 'black';
            }
            
            if($__SETTING->gold_status == 1 && $currentRankInt < 3){
                $nextRankInt = 'gold';
            }
            
            if($__SETTING->silver_status == 1 && $currentRankInt < 2){
                $nextRankInt = 'sliver';
            }
            
            if($__SETTING->bronze_status == 1 && $currentRankInt < 1){
                $nextRankInt = 'bronze';
            }
            
            $current = isset($__RANK[$__CUS->rank_id]) ? $__RANK[$__CUS->rank_id] : '';
            
            $rankList = $this->getRankList($__CUS, $__SETTING, $__RANK);

           $lastday_thismonth = date('Y/m/d', strtotime('last day of this month'));
            $firstday_nextmonth =  date('Y/m/d', strtotime('first day of next month'));
            
            if($nextRankInt != ''){
                $next = $rankList[$nextRankInt];
                $next['time_visit'] = empty($next['time_visit']) || $next['time_visit'] == 0 ? '' : $__CUS->number_visit.'/'.$next['time_visit'];
                $next['point_total'] = empty($next['point_total']) || $next['point_total'] == 0 ? '' : $__CUS->total_point.'/'.$next['point_total'];
                $next['use_money'] = empty($next['use_money']) || $next['use_money'] == 0 ? '' : $__CUS->total_amount.'/'.$next['use_money'];
            } else {
                $next = null;
            }
            
            $conditionName = '';
            // Type of setting is '01' : 2つ以上達成
            if($__SETTING->rank_condition == '01'){
                $conditionName = '2つ以上達成';
            }
            // Type of setting is '02' : 1つ以上達成
            elseif($__SETTING->rank_condition == '02'){
                $conditionName = '1つ以上達成';
            }// Type of setting is '00' : すべて達成、
            else{
                $conditionName = 'すべて達成、';
            }
            
            $detail = [
                'condition_name' => $conditionName,
                'rank_condition' => $__SETTING->rank_condition,
                'rank_apply_type' => $__SETTING->rank_apply_type,
                'days_reset_rank' => $__SETTING->days_reset_rank,
                'bronze_status' => $__SETTING->bronze_status,
                'silver_status' => $__SETTING->silver_status,
                'gold_status' => $__SETTING->gold_status,
                'black_status' => $__SETTING->black_status,
                'created_at' => $__SETTING->created_at,
                'updated_at' => $__SETTING->updated_at
            ];
            
            return ['current' => $current, 'next' => $next, 'rank_list' => $rankList, 'info' => $detail, 
                    'lastday_thismonth' =>$lastday_thismonth, 'firstday_nextmonth' =>$firstday_nextmonth];
        }
        
        /*
         * Get rank ratio rank
         * 
         * @param object $customer
         * @param object $setting
         * @return array
         */
        private function getRankList($__CUS, $__SETTING, $__RANK){

            $rankList['normal'] = [
                'time_visit' => empty($__SETTING->normal_time_visit) || $__SETTING->normal_time_visit == 0 ? '' :$__SETTING->normal_time_visit,
                'point_total' => empty($__SETTING->normal_point_total) || $__SETTING->normal_point_total == 0 ? '' : $__SETTING->normal_point_total,
                'use_money' => empty($__SETTING->normal_use_money) || $__SETTING->normal_time_visit == 0 ? '' : $__SETTING->normal_use_money,
                'image' => ApiSupport::getRankUrl('01_rank.png'),
                'name' => $__RANK['00']
            ];
            if($__SETTING->bronze_status == 1){
                 $rankList['bronze'] = [
                    'time_visit' => empty($__SETTING->bronze_time_visit) || $__SETTING->bronze_time_visit == 0 ? '' : $__SETTING->bronze_time_visit,
                    'point_total' => empty($__SETTING->bronze_point_total) || $__SETTING->bronze_point_total == 0 ? '' : $__SETTING->bronze_point_total,
                    'use_money' => empty($__SETTING->bronze_use_money) || $__SETTING->bronze_use_money == 0 ? '' : $__SETTING->bronze_use_money,
                    'image' => ApiSupport::getRankUrl('02_rank.png'),
                    'name' => $__RANK['01']
                 ];
            }
            
            if($__SETTING->silver_status == 1){
                 $rankList['sliver'] = [
                    'time_visit' => empty($__SETTING->sliver_time_visit) || $__SETTING->sliver_time_visit == 0 ? '' : $__SETTING->sliver_time_visit,
                    'point_total' => empty($__SETTING->sliver_point_total) || $__SETTING->sliver_point_total == 0 ? '' : $__SETTING->sliver_point_total,
                    'use_money' => empty($__SETTING->sliver_use_money) || $__SETTING->sliver_use_money == 0 ? '' : $__SETTING->sliver_use_money,
                    'image' => ApiSupport::getRankUrl('03_rank.png'),
                    'name' => $__RANK['02']
                 ];
            }
            
            if($__SETTING->gold_status == 1){
                 $rankList['gold'] = [
                    'time_visit' => empty($__SETTING->gold_time_visit) || $__SETTING->gold_time_visit == 0 ? '' : $__SETTING->gold_time_visit,
                    'point_total' => empty($__SETTING->gold_point_total) || $__SETTING->gold_point_total == 0 ? '' : $__SETTING->gold_point_total,
                    'use_money' => empty($__SETTING->gold_use_money) || $__SETTING->gold_use_money == 0 ? '' : $__SETTING->gold_use_money,
                    'image' => ApiSupport::getRankUrl('04_rank.png'),
                    'name' => $__RANK['03']
                 ];
            }
            
            if($__SETTING->black_status == 1){
                 $rankList['black'] = [
                    'time_visit' => empty($__SETTING->black_time_visit) || $__SETTING->black_time_visit == 0 ? '' : $__SETTING->black_time_visit,
                    'point_total' => empty($__SETTING->black_point_total) || $__SETTING->black_point_total == 0 ? '' : $__SETTING->black_point_total,
                    'use_money' => empty($__SETTING->black_use_money) || $__SETTING->black_use_money == 0 ? '' : $__SETTING->black_use_money,
                    'image' => ApiSupport::getRankUrl('05_rank.png'),
                    'name' => $__RANK['04']
                 ];
            }
            
            return $rankList;
        }
        
        /*
         * Barcode method
         */
        public function actionBarcode(){
                try{
                        //Get parameter
                        $isLogged = ApiSupport::isCustomerLogged();
                        
                        // Check user_id is invalid or null
                        if(!$isLogged){
                                return parent::result(1, \Yii::t('api', 'You must login before'));
                        }
                        
                        return parent::result(0, '', ['barcodeUrl' => ApiSupport::getBarcodeUrl($isLogged->customer_jan_code)]);
                } catch (Exception $ex) {
                        return parent::result(1, $ex->getMessage());
                }
        }
        
                /*
         * Point history of store
         * 
         * @return json
         */
        public function actionPoints(){
            try{
                // Get logged customer
                $loggedCustomer = ApiSupport::isCustomerLogged();

                if(!$loggedCustomer){
                        return parent::result(1, \Yii::t('api', 'You must login before'));
                }

                // Get parameter and check
                $store_id = \Yii::$app->getRequest()->get('store_id');

                if(!$store_id){
                        return parent::result(1, \Yii::t('api', 'Parameter "{param}" is missing', ['param' => 'store_id']));
                }

                if(!is_numeric($store_id)){
                        return parent::result(1, \Yii::t('api', 'Parameter "{param}" is invalid', ['param' => 'store_id']));
                }

                $customer = CustomerStore::find()
                                ->select('*')
                                ->with(['pointHistory'])
                                ->where(['customer_store.customer_id' => $loggedCustomer->id])
                                ->andWhere(['customer_store.store_id' => $store_id])
                                ->one();
                if(!$customer){
                    return parent::result(1, \Yii::t('api', '{param} is not found', ['param' => 'Customer']));
                }

                $__result = [
                    'user' => [
                        'total_point' => $customer->total_point
                    ],

                    'histories' => $customer->pointHistory
                ];

                return parent::result(0, '', $__result);

            } catch (Exception $ex) {
                    return parent::result(1, $ex->getMessage());
            }
        }
        
        /*
         * Charge history method
         * 
         * @return json
         */
        public function actionCharge_history(){
            try{
                $loggedCustomer = ApiSupport::isCustomerLogged();
                
                if(!$loggedCustomer){
                    return parent::result(1, \Yii::t('api','You must login before'));
                }
                
                $store_id = \Yii::$app->getRequest()->getQueryParam('store_id');
                        
                if(!$store_id){
                        return parent::result(1, \Yii::t('api', 'Parameter "{param}" is missing', ['param' => 'store_id']));
                }elseif(!is_numeric($store_id)){
                        return parent::result(1, \Yii::t('api', 'Parameter "{param}" is invalid', ['param' => 'store_id']));
                }
                
                $result = CustomerStore::find()
                        ->select('*')
                        ->with('chargeHistoryNoUse')
                        ->where(['store_id' => $store_id])
                        ->andWhere(['customer_id' => $loggedCustomer->id])
                        ->one();
                
                if(!$result){
                    return parent::result(1, \Yii::t('api','{param} is not found', ['param' => 'Customer']));
                }
                
                $__result = [
                    'user' => [
                        'amount' => $result->total_amount
                    ],
                    
                    'histories' => $result->chargeHistoryNoUse,
                    'charge_balance_sum' => $result->charge_balance
                ];
               
                return parent::result(0, '', $__result);
            } catch (Exception $ex) {
                return parent::result(1, $ex->getMessage());
            }
        }
}