<?php
namespace api\controllers;
use common\models\MasterStaff;
use api\models\ApiSupport;

/*
 * StoreController object class
 */
class StaffController extends ApiController
{
        public $modelClass = 'common\models\MasterStaff';
        
        /*
         * Info method
         * 
         * @return json
         */
        public function actionInfo(){
                try{
                        $store_id = \Yii::$app->getRequest()->get('store_id');
                        
                        if(!$store_id){
                                return parent::result(1, \Yii::t('api', 'Parameter "{param}" is missing', ['param' => 'store_id']));
                       }
                        
                        $staffs = MasterStaff::find()
                                        ->with('store')
                                        ->where(['mst_staff.store_id' => $store_id])
                                        ->andWhere(['mst_staff.show_flg' => 1])
                                        ->andWhere(['mst_staff.del_flg' => 0])
                                        ->all();
                        
                        $__staffs = [];
                        foreach ($staffs as $key=> $staff){
                                foreach($staff as $k => $v){
                                        $__staffs[$key][$k] = $v;
                                }
                                
                                $__staffs[$key]['avatar'] = ApiSupport::getFileFullUrl($staff->avatar);
                                $__staffs[$key]['image1'] = ApiSupport::getFileFullUrl($staff->image1);
                                $__staffs[$key]['image2'] = ApiSupport::getFileFullUrl($staff->image2);
                                $__staffs[$key]['link_icon_1'] = ApiSupport::getFileFullUrl($staff->link_icon_1);
                                $__staffs[$key]['link_icon_2'] = ApiSupport::getFileFullUrl($staff->link_icon_2);
                                $__staffs[$key]['link_icon_3'] = ApiSupport::getFileFullUrl($staff->link_icon_3);
                                
                                $__staffs[$key]['options'] = [
                                        ['name' => $staff->store->staff_item_1, 'value'=> $staff->item_option_1],
                                        ['name' => $staff->store->staff_item_2, 'value'=> $staff->item_option_2],
                                        ['name' => $staff->store->staff_item_3, 'value'=> $staff->item_option_3],
                                        ['name' => $staff->store->staff_item_4, 'value'=> $staff->item_option_4],
                                        ['name' => $staff->store->staff_item_5, 'value'=> $staff->item_option_5]
                                ];
                                
                                unset($__staffs[$key]['item_option_1']);
                                unset($__staffs[$key]['item_option_2']);
                                unset($__staffs[$key]['item_option_3']);
                                unset($__staffs[$key]['item_option_4']);
                                unset($__staffs[$key]['item_option_5']);
                                unset($__staffs[$key]['del_flg']);
                        }
                        
                        if (!$__staffs){
                                return parent::result(1, 'Staff not found');
                                return parent::result(1, \Yii::t('api', '{param} is not found', ['param' => 'Staff']));
                        }

                        return parent::result(0, '', ['staff' => $__staffs]);
                } catch (Exception $ex) {
                        return parent::result(1, $ex->getMessage());
                }
        }
}
