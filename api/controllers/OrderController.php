<?php

namespace api\controllers;

use common\models\MasterStore;
use Yii;
use api\models\ApiSupport;
use yii\helpers\ArrayHelper;
use common\models\BookingBusiness;
use common\models\MstProduct;
use common\models\MasterCoupon;

class OrderController extends ApiController {

    public $modelClass = 'common\models\MstOrder';

    /*
     * Get list product purchased
     * 
     * @return json
     */

    public function actionHistory() {

        try {
            //Get parameter
            $isCustomerLogged = ApiSupport::isCustomerLogged();
            // Check user_id is invalid or null
            if (!$isCustomerLogged) {
                return parent::result(1, \Yii::t('api', 'You must login before'));
            }

            $store_id = \Yii::$app->getRequest()->get('store_id');
            $page = \Yii::$app->getRequest()->get('page');
            $limit = \Yii::$app->getRequest()->get('limit');

            // Check valid parameters
            if (!$store_id) {
                return parent::result(1, \Yii::t('api', 'Parameter "{param}" is missing', ['param' => 'store_id']));
            }
            if (!is_numeric($store_id)) {
                return parent::result(1, \Yii::t('api', 'Parameter "{param}" is invalid', ['param' => 'store_id']));
            }

            if (!$page) {
                return parent::result(1, \Yii::t('api', 'Parameter "{param}" is missing', ['param' => 'page']));
            }
            if (!is_numeric($page)) {
                return parent::result(1, \Yii::t('api', 'Parameter "{param}" is invalid', ['param' => 'page']));
            }

            if (!$limit) {
                return parent::result(1, \Yii::t('api', 'Parameter "{param}" is missing', ['param' => 'limit']));
            }
            if (!is_numeric($limit)) {
                return parent::result(1, \Yii::t('api', 'Parameter "{param}" is invalid', ['param' => 'limit']));
            }

            $store_code = MasterStore::find()->select(['store_code', 'company_id'])
                    ->where(['id' => $store_id])
                    ->one();

            if (!$store_code) {
                return parent::result(1, \Yii::t('api', '{param} is not found', ['param' => 'store']));
            }

            $page_num = ($page - 1) * $limit;

            $masterOrder = new \yii\db\Query();
            $masterOrder->select(['mst_order.id', 'mst_order.process_date', 'mst_order.money_credit', 'mst_order.money_ticket',
                        'mst_order.money_other', 'mst_order.money_cash', 'order_detail.product_display_name', 'order_detail.price', 'order_detail.jan_code'])
                    ->from('mst_order')
                    ->rightJoin('order_detail', 'mst_order.order_code = order_detail.order_code')
                    ->where(['mst_order.customer_jan_code' => $isCustomerLogged->customer_jan_code])
                    ->andWhere(['left(mst_order.order_code,5)' => $store_code->store_code])
                    ->andWhere(['mst_order.company_id' => $store_code->company_id])
                    ->limit($limit)
                    ->offset($page_num)
                    ->orderBy('mst_order.process_date DESC, mst_order.id ASC');

            $productList = $masterOrder->all();

            $__result = null;
            if ($productList) {
                $product_jan_codes = [];
                $coupon_jan_codes = [];
                foreach ($productList as $orderProduct) {
                    if (substr($orderProduct['jan_code'], 0, 2) === '21') {
                        $product_jan_codes[] = $orderProduct['jan_code'];
                    }
                    else {
                        $coupon_jan_codes[] = $orderProduct['jan_code'];
                    }
                }
                $products = MstProduct::findFrontEnd()
                        ->andWhere([
                            'jan_code' => $product_jan_codes
                        ])->all();
                $coupons = MasterCoupon::findFrontEnd()
                        ->andWhere([
                            'coupon_jan_code' => $coupon_jan_codes
                        ])->all();
                $product_price_table = array_values(BookingBusiness::getPriceTable('', implode(',', ArrayHelper::getColumn($products, 'id')))['products']);
                $coupon_price_table = array_values(BookingBusiness::getPriceTable('', implode(',', ArrayHelper::getColumn($coupons, 'id')))['coupons']);
                foreach ($productList as $key => $value) {
                    $money_cash = '';
                    $point_use = '';
                    $charge_use = '';
                    $money_ticket = '';
                    $money_credit = '';
                    $money_other = '';

                    if (isset($value['money_cash']) && $value['money_cash'] != 0) {
                        $money_cash = Yii::t('frontend', 'CASH');
                    }
                    if (isset($value['point_use']) && $value['point_use'] != 0) {
                        $point_use = Yii::t('frontend', 'POINT');
                    }
                    if (isset($value['charge_use']) && $value['charge_use'] != 0) {
                        $charge_use = Yii::t('frontend', 'CHARGE');
                    }
                    if (isset($value['money_ticket']) && $value['money_ticket'] != 0) {
                        $money_ticket = Yii::t('frontend', 'PRODUCT TICKET');
                    }
                    if (isset($value['money_credit']) && $value['money_credit'] != 0) {
                        $money_credit = Yii::t('frontend', 'CREDIT');
                    }
                    if (isset($value['money_other']) && $value['money_other'] != 0) {
                        $money_other = Yii::t('frontend', 'ORDER');
                    }
                    
                    
                    $price = 0;
                    if (substr($value['jan_code'], 0, 2) === '21') {
                        $id = array_search($value['jan_code'], $product_jan_codes);
                        if ($id !== false && isset($product_price_table[$id])) {
                            $price = $product_price_table[$id]['total_price'];
                        }
                    }
                    else {
                        $id = array_search($value['jan_code'], $coupon_jan_codes);
                        if ($id !== false && isset($coupon_price_table[$id])) {
                            $price = $coupon_price_table[$id]['total_price'];
                        }
                    }

                    $__result[] = [
                        'id' => $value['id'],
                        'purchased_date' => $value['process_date'],
                        'price' => $price,
                        'product_name' => trim($value['product_display_name']),
                        'money_credit' => $money_credit,
                        'money_ticket' => $money_ticket,
                        'money_cash' => $money_cash,
                        'money_other' => $money_other,
                        'charge_use' => $charge_use,
                        'point_use' => $point_use
                    ];
                }
            }

            return parent::result(0, '', ['purchase_histories' => $__result]);
        } catch (Exception $ex) {
            return parent::result(1, $ex->getMessage());
        }
    }

}
