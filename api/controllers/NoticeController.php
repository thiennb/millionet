<?php
namespace api\controllers;
use api\models\ApiToken;
use common\models\MasterNotice;
use common\models\NoticeCustomer;
use api\models\ApiSupport;
use console\models\NoticeSend;

class NoticeController extends ApiController{
        public $modelClass = 'common\models\MasterNotice';
        
        /*
         * GEt update notification method
         */
        public function actionList(){
            try{
                // Get parameters
                $store_id = \Yii::$app->getRequest()->getQueryParam('store_id');
                //$store_id = 1;
                $id = \Yii::$app->getRequest()->getQueryParam('id');
                $isCustomerLogged = ApiSupport::isCustomerLogged();
                //$isCustomerLogged = (object)['user_id' => 1, 'id' => 1];

                if(!$isCustomerLogged){
                    return parent::result(1, \Yii::t('api','You must login before'));
                }

                if(!$store_id){
                    return parent::result(1, \Yii::t('api', 'Parameter "{param}" is missing', ['param' => 'store_id']));
                }

                if(!$id){
                    $id = 0;
                }

                $__noticeList = [];
                $sent_list = \common\models\NoticeCustomer::find()
                        ->where(['notice_customer.customer_id' => $isCustomerLogged->id])
                        ->andWhere(['notice_customer.store_id' => $store_id])
                        ->andWhere(['>', 'notice_customer.id', $id])
                        ->all();

                foreach ($sent_list as $sent){
                    $__data = $sent->getAttributes();
                    $__data['send_date'] = $sent->notice->send_date;
                    $__data['send_time'] = $sent->notice->send_time;
                    $__data['title'] = $sent->notice->title;
                    $__data['content'] = $sent->notice->content;
                    $__data['push_date'] = date('Y/m/d H:s', $sent->push_date);
                    $__noticeList[] = $__data;
                }

                $result = ['notifications' => $__noticeList];

                $result['user'] = [
                        'id' => $isCustomerLogged->user_id
                ];

                return parent::result(0, '', $result);
            } catch (Exception $ex) {
                    return parent::result(1, $ex->getMessage());
            }
        }
        
        /*
         * GEt update notification method
         */
        public function actionDetail(){
            $transaction = \Yii::$app->db->beginTransaction();
            try{
                // Get parameters
                $store_id = \Yii::$app->getRequest()->getQueryParam('store_id');
                $id = \Yii::$app->getRequest()->getQueryParam('id');
                $isCustomerLogged = ApiSupport::isCustomerLogged();
                //$isCustomerLogged = (object)['id' => 1, 'user_id' => 1];
                if(!$isCustomerLogged){
                    return parent::result(1, \Yii::t('api','You must login before'));
                }

                if(!$id){
                    return parent::result(1, \Yii::t('api', 'Parameter "{param}" is missing', ['param' => 'id']));
                }

                if(!$store_id){
                    return parent::result(1, \Yii::t('api', 'Parameter "{param}" is missing', ['param' => 'store_id']));
                }

                $notice = \common\models\NoticeCustomer::find()
                        ->where(['notice_customer.customer_id' => $isCustomerLogged->id])
                        ->andWhere(['notice_customer.store_id' => $store_id])
                        ->andWhere(['notice_customer.id' => $id])
                        ->one();
                
                if(!$notice){
                    return parent::result(1,  \Yii::t('api', '{param} is not found', ['param' => 'Notice']));
                }
                
                $__data = $notice->getAttributes();
                $__data['send_date'] = $notice->notice->send_date;
                $__data['send_time'] = $notice->notice->send_time;
                $__data['title'] = $notice->notice->title;
                $__data['content'] = $notice->notice->content;
                $__data['push_date'] = date('Y/m/d H:s', $notice->push_date);
                $result = ['notifications' => $__data];

                $result['user'] = [
                        'id' => $isCustomerLogged->user_id
                ];
                
                if($notice->read_flg == '0'){
                    NoticeCustomer::updateAll([
                        'read_flg'=>'1',
                        'updated_at' => strtotime(date('Y-m-d H:i:s')),
                        'updated_by' => $isCustomerLogged->user_id],
                        ['id' => $id]);
                    
                    $transaction->commit();
                }
                return parent::result(0, '', $result);
            } catch (Exception $ex) {
                    $transaction->rollBack();
                    return parent::result(1, $ex->getMessage());
            }
        }
        
        /*
         * GEt update notification method
         */
        public function actionNotifytostore(){
            try{
                $isCustomerLogged = ApiSupport::isCustomerLogged();
                if(!$isCustomerLogged){
                    return parent::result(1, \Yii::t('api','You must login before'));
                }
                
                //check param device_token
                $device_token = \Yii::$app->getRequest()->getQueryParam('device_token');
                
                if(!$device_token){
                    return parent::result(1, \Yii::t('api', 'Parameter "{param}" is missing', ['param' => 'device_token']));
                }
                
                //check param distance
                $distance = \Yii::$app->getRequest()->getQueryParam('distance');

                if($distance == null){
                    return parent::result(1, \Yii::t('api', 'Parameter "{param}" is missing', ['param' => 'distance']));
                }
                if(!is_numeric($distance)){
                    return parent::result(1, \Yii::t('api', 'Parameter "{param}" is invalid', ['param' => 'distance']));
                }
                
                //check param store_id
                $store_id = \Yii::$app->getRequest()->getQueryParam('store_id');
                
                if(!$store_id){
                    return parent::result(1, \Yii::t('api', 'Parameter "{param}" is missing', ['param' => 'store_id']));
                }
                if(!is_numeric($store_id)){
                    return parent::result(1, \Yii::t('api', 'Parameter "{param}" is invalid', ['param' => 'store_id']));
                }
                //process send notice
                $result = NoticeSend::scheduleSendNoticeDistance($isCustomerLogged->id,$device_token,$store_id,$distance);
                if($result){
                    return parent::result(0, 'success');
                }
                return parent::result(0, 'No data to send');
            } catch (Exception $ex) {
                return parent::result(1, $ex->getMessage());
            }
        }
}