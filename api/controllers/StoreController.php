<?php

    /* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace api\controllers;
use common\models\MasterStore;
use common\models\PointHistory;
use api\models\ApiSupport;
use Yii;
use common\components\Util;

class StoreController extends ApiController
{
    public $modelClass = 'common\models\MasterStore';
    
        /*
         * Get store images method
         * 
         * @return json
         */
        public function  actionShortstore(){
            try{
                    $store_id = \Yii::$app->getRequest()->getQueryParam('store_id');

                    if(!$store_id){
                        return parent::result(1, Yii::t('api', 'Parameter "{param}" is missing', ['param' => 'store_id']));
                    }

                    if(!is_numeric($store_id)){
                        return parent::result(1, \Yii::t('api', 'Parameter "{param}" is invalid', ['param' => 'store_id']));
                    }
                    
                    $masterstore = new \yii\db\Query();
                    $store = $masterstore->select(['mst_store.id', 'mst_store.image1','mst_store.image2', 'mst_store.image3', 'mst_store.image4', 'mst_store.image5', 'mst_store.show_flg', 
                                                'mst_store.latitude', 'mst_store.longitude', 'mst_store.show_in_booking_flg', 'mst_store.show_in_notice_flg', 'mst_store.show_in_coupon_flg', 'mst_store.app_logo_img',
                                                'mst_store.show_in_product_category_flg', 'mst_store.show_in_my_page_flg', 'mst_store.show_in_staff_flg', 'mst_store.reservations_possible_time', 'mst_store.del_flg',
                                                'company.company_code', 'company.app_header_background_color', 'company.app_content_background_color', 'company.app_footer_background_color',
                                                'company.app_header_character_color', 'company.app_content_character_color', 'company.app_footer_character_color'])
                                         ->from('mst_store')
                                         ->innerJoin('company', 'company.id = mst_store.company_id')
                                         ->where(['mst_store.id' => $store_id])
                                            //->andWhere([ 'del_flg' => 0])
                                         ->one();
                    if (!$store){
                            return parent::result(1,Yii::t('api', '{param} is not found', ['param' => 'Store']) );
                    }

                    // Convert images
                    $images = [];
                    if($store['image1']){
                            //$images['image1'] = $store->image1;
                            $images[] = ApiSupport::getFileFullUrl($store['image1']);
                    }
                    if($store['image2']){
                            //$images['image2'] = $store->image2;
                            $images[] = ApiSupport::getFileFullUrl($store['image2']);
                    }
                    if($store['image3']){
                            //$images['image3'] = $store->image3;
                            $images[] = ApiSupport::getFileFullUrl($store['image3']);
                    }
                    if($store['image4']){
                            //$images['image4'] = $store->image2;
                            $images[] = ApiSupport::getFileFullUrl($store['image4']);
                    }
                    if($store['image5']){
                            //$images['image5'] = $store->image3;
                            $images[] = ApiSupport::getFileFullUrl($store['image5']);
                    }
                    
                    $app_logo = null;
                    if($store['app_logo_img']){
                        $app_logo = ApiSupport::getFileFullUrl($store['app_logo_img']);
                    }
                    
                    $company_setting['company_code'] = $store['company_code'];
                    $company_setting['app_header_background_color'] = $store['app_header_background_color'];
                    $company_setting['app_content_background_color'] = $store['app_content_background_color'];
                    $company_setting['app_footer_background_color'] = $store['app_footer_background_color'];
                    $company_setting['app_header_character_color'] = $store['app_header_character_color'];
                    $company_setting['app_content_character_color'] = $store['app_content_character_color'];
                    $company_setting['app_footer_character_color'] = $store['app_footer_character_color'];

                    return  parent::result(0, '', ['store' => [
                                    'images' => $images,
                                    'show_flg' => $store['show_flg'],
                                    'show_in_booking_flg' => $store['show_in_booking_flg'],
                                    'show_in_notice_flg' => $store['show_in_notice_flg'],
                                    'show_in_coupon_flg' => $store['show_in_coupon_flg'],
                                    'show_in_product_category_flg' => $store['show_in_product_category_flg'],
                                    'show_in_my_page_flg' => $store['show_in_my_page_flg'],
                                    'show_in_staff_flg' => $store['show_in_staff_flg'],
                                    'reservations_possible_time' => $store['reservations_possible_time'],
                                    'latitude' => $store['latitude'],
                                    'longitude' => $store['longitude'],
                                    'del_flg' => $store['del_flg'],
                                    'app_color' => $company_setting,
                                    'app_logo' => $app_logo]
                                ]);
            } catch (Exception $ex) {
                    return parent::result(1, $ex->getMessage());
            }
        }
    
        /*
         * Get store detail method
         * 
         * @return json
         */
        public function actionDetail(){
                try{
                        $store_id = \Yii::$app->getRequest()->get('store_id');
                        
                        // Check exist or valid parameters
                        if(!$store_id){
                                return parent::result(1, Yii::t('api', 'Parameter "{param}" is missing', ['param' => 'store_id']));
                        }
                        
                        if(!is_numeric($store_id)){
                                return parent::result(1, Yii::t('api', 'Parameter "{param}" is invalid', ['param' => 'store_id']));
                        }

                        $store = MasterStore::find()
                                        ->with(['restDaySchedule'])
                                        ->where(['mst_store.id' => $store_id])
                                        ->one();
                        
                        // Store is exists?
                        if(!$store){
                                return parent::result(1, Yii::t('api', '{param} is not found', ['param' => 'Store']));
                        }
                        
                        $result = [
                                'id' => $store->id,
                                'latitude' => $store->latitude,
                                'longitude' => $store->longitude,
                                'tel' => $store->tel,
                                'address' => $store->address,
                                'directions' => $store->directions,
                                'time_open'  => $store->time_open,
                                'regular_holiday' => $store->regular_holiday,
                                'name' => $store->name,
                                'options' => [
                                        ['name' => $store->store_item_1_title, 'value' => $store->store_item_1],
                                        ['name' => $store->store_item_2_title, 'value' => $store->store_item_2],
                                        ['name' => $store->store_item_3_title, 'value' => $store->store_item_3],
                                        ['name' => $store->store_item_4_title, 'value' => $store->store_item_4],
                                        ['name' => $store->store_item_5_title, 'value' => $store->store_item_5],
                                        ['name' => $store->store_item_6_title, 'value' => $store->store_item_6],
                                        ['name' => $store->store_item_7_title, 'value' => $store->store_item_7],
                                        ['name' => $store->store_item_8_title, 'value' => $store->store_item_8],
                                        ['name' => $store->store_item_9_title, 'value' => $store->store_item_9],
                                        ['name' => $store->store_item_10_title, 'value' => $store->store_item_10],
                                ]
                        ];
                        
                        // Get rest day
                        if($store->restDaySchedule){
                                foreach ($store->restDaySchedule as $rest){
                                        $result['rest_day'][] = [
                                                'id' => $rest->id,
                                                'day' => $rest->schedule_date
                                        ];
                                }
                        } else {
                                $result['rest_day'] = [];
                        }
                        
                        return parent::result(0, '', ['store' => $result]);
                        
                } catch (Exception $ex) {
                        return parent::result(1, $ex->getMessage());
                }
        }
}