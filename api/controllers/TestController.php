<?php
namespace api\controllers;

use common\models\Booking;

class TestController extends ApiController{
        public $modelClass = 'common\models\Booking';
        
        public function actionPost(){
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, 'http://millionet.dev/api/logout');
                curl_setopt($ch, CURLOPT_POSTFIELDS, ['store_id' => 1, 'device_token' => 'dXHEKYw2NOU:APA91bEEKDDmDvmHBnIJK3Aj5c1WfybljqpdDGJ_PQHdKePYEMw8YjTn3AhWQeYRlivcgrEFOkPmgdPhiN20Bd6v6hDoyf6HnwsAzkki6Y8NVMYDDuwIhfKYvRuqoYDDfCfKhVUQjOP8', 'enable' => false]);
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
                curl_setopt($ch, CURLOPT_HTTPHEADER,['Authorization: cmFuZD04Mjg4Mztjb2RlPTEuNjc4NDgzMTgxNjE4NkUrMTU7aWQ9MTQ7c2NvcGU9MzExMDQwMDA7ZGF0ZXRpbWU9MjAxNi8xMC8yOCAxMToxNDowNw==']);
                curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                $response = curl_exec($ch);
                curl_close($ch);
                echo $response;
                return;
        }
        
        public function actionGet(){
                $ch = curl_init();

                curl_setopt($ch, CURLOPT_URL, 'http://millionet.dev/api/customer/rank_info?store_id=6');
                curl_setopt($ch, CURLOPT_HTTPHEADER,['Authorization: cmFuZD01OTgxMjtjb2RlPTE2NDUxMzY4MTg0NTM3Njg7aWQ9MTk7c2NvcGU9MzExMDQwMDA7ZGF0ZXRpbWU9MjAxNi8xMS8xMCAxMDo0Njo0Ng==']);

                curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                $response = curl_exec($ch);
                curl_close($ch);
                echo $response;
                return;
        }
}