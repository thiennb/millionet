<?php

namespace api\controllers;

use common\models\MasterCustomer;
use common\models\CustomerStore;
use common\components\Constants;
use api\models\ApiSupport;
use yii\helpers\ArrayHelper;
use common\models\BookingSearch;
use yii\db\Expression;

use Yii;

class CouponController extends ApiController {

    public $modelClass = 'common\models\MasterCoupon';

    /*
     * Action get update coupon
     * 
     * @return json
     */

    public function actionGetupdatecoupon() {
        try {
            // Get parameters
            $store_id = \Yii::$app->getRequest()->getQueryParam('store_id');
            $lasttime = \Yii::$app->getRequest()->getQueryParam('lasttime');
            $isLogged = ApiSupport::isCustomerLogged();

            // Pagination
            $display = \Yii::$app->getRequest()->getQueryParam('display');

            $currentDate = date('Y-m-d');

            if (!$store_id) {
                return parent::result(1, \Yii::t('api', 'Parameter "{param}" is missing', ['param' => 'store_id']));
            }

            if ($lasttime && !is_numeric($lasttime)) {
                return parent::result(1, \Yii::t('api', 'Parameter "{param}" is invalid', ['param' => 'lasttime']));
            }

            if ($display && !is_numeric($display)) {
                return parent::result(1, \Yii::t('api', 'Parameter "{param}" is invalid', ['param' => 'display']));
            } else if (!$display) {
                $display = -1;
            }

            $queryObject = BookingSearch::searchCouponsByStoreId($store_id, '', $isLogged);

            if ($display > 0) {
                $queryObject->limit($display);
            }
            
            //$coupons->orderBy(['expire_date' => \SORT_ASC]);
            $price_search = \common\models\BookingBusiness::getCouponPriceTable(function ($query) {
                        $query->andWhere(['c.store_id' => Yii::$app->getRequest()->getQueryParam('store_id')]);
                    });
            if ($price_search->count() > 0) {
                $queryObject->leftJoin([
                    'price_search' => $price_search
                        ], 'price_search.id = c.id');

                $queryObject->orderBy([
                    'apply_condition' => SORT_ASC,
                    'expire_date' => SORT_ASC,
                    'price_search.type' => SORT_ASC,
                    'price_search.discount_yen' => SORT_DESC,
                    'price_search.discount_percent' => SORT_DESC,
                    'price_search.discount_price_set' => SORT_ASC,
                    'price_search.discount_drink_eat' => SORT_ASC
                ]);
            }
            else {
                $queryObject->orderBy([
                    'expire_date' => SORT_ASC
                ]);
            }

            $coupons = $queryObject->all();

            $price_table = ArrayHelper::index(\common\models\BookingBusiness::getPriceTable(\implode(',', ArrayHelper::getColumn($coupons, 'id')))['coupons'], 'id');

            // sort using price
            //BookingorderController::sortCoupons($coupons, $price_table);

            $__RANK = Constants::RANK;
            // Convert
            $couponList = [];
            foreach ($coupons as $value) {
                $__value = [];
                $__value['coupon_id'] = $value['id'];
                // Get arrray categories from string : {name,name,name}
                $categories = explode(',', preg_replace('/\{|\}|\"/', '', $value['categories']));

                $__value['categories'] = [];

                if ($categories[0] != '||') {
                    foreach ($categories as $categoryValue) {
                        if ($categoryValue) {
                            $category = explode('||', $categoryValue);
                            $__value['categories'][] = [
                                'id' => $category[0],
                                'name' => $category[1]
                            ];
                        }
                    }
                }
                //$__value['price'] = isset($price_table[$value['id']]) ? self::displayMoney($price_table[$value['id']]) . '' : null;
                if ($isLogged) {
                    $__value['first_name'] = $isLogged['first_name'];
                    $__value['last_name'] = $isLogged['last_name'];
                    $__value['first_name_kana'] = $isLogged['first_name_kana'];
                    $__value['last_name_kana'] = $isLogged['last_name_kana'];
                }
                else {
                    $__value['first_name'] = null;
                    $__value['last_name'] = null;
                    $__value['first_name_kana'] = null;
                    $__value['last_name_kana'] = null;
                }
                $__value['image'] = ApiSupport::getFileFullUrl($value['image']);

                $__value['show_coupon'] = $value['show_coupon'];
                $__value['expire_date'] = $value['expire_date'];
                $__value['expire_auto_set'] = $value['expire_auto_set'];
                $__value['benefits_content'] = $value['benefits_content'];
                $__value['introduction_1'] = $value['introduction_1'];
                $__value['introduction_2'] = $value['introduction_2'];
                $__value['introduction_3'] = $value['introduction_3'];
                $__value['description'] = $value['description'];
                $__value['coupon_jan_code'] = $value['coupon_jan_code'];
                $__value['birthday_from'] = $value['birthday_from'];
                $__value['birthday_to'] = $value['birthday_to'];
                $__value['updated_at'] = $value['updated_at'];
                $__value['del_flg'] = $value['del_flg'];
                $__value['title'] = $value['title'];
                $__value['display_barcode_flg'] = $value['display_barcode_flg'];
                $__value['code_membership'] = $value['code_membership'];

                // Get link of barcode
                $__value['barcode_url'] = ApiSupport::getBarcodeUrl($value['coupon_jan_code']);

                if ($isLogged && $value['code_membership']) {
                    $__value['user_id'] = $isLogged->user_id;
                } else {
                    $__value['user_id'] = null;
                }

                // Get rank name from constant
                $__value['rank'] = isset($__RANK[$value['rank_id']]) ? $__RANK[$value['rank_id']] : null;
                $__value['apply_condition'] = $value['apply_condition'];
                $__value['total_price'] = isset($price_table[$value['id']]) ? self::displayMoney($price_table[$value['id']]) . '' : null;
                $couponList[] = $__value;
            }

            return parent::result(0, '', ['coupons' => $couponList]);
        } catch (Exception $ex) {
            return parent::result(1, $ex->getMessage());
        }
    }

    public static function displayMoney ($price_item) {
        if ($price_item['mode'] != 'coupon' || $price_item['type'] > 4) {
            return $price_item['total_price'];
        } elseif ($price_item['type'] == 1 || $price_item['type'] == 3) {
            return abs($price_item['unit_price']) . Yii::t('frontend', 'Off');
        } else {
            return abs($price_item['unit_price']) . '%' . Yii::t('frontend', 'Off');
        }
    }
}
