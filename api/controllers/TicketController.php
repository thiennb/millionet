<?php
namespace api\controllers;
use common\models\MasterTicket;
use common\models\MstOrder;
use api\models\ApiSupport;
use yii\db\Query;
use common\models\TicketHistory;
class TicketController extends ApiController{
        public $modelClass = 'common\models\MasterTicket';
        
        /*
         * Get ticket list
         */
        public function actionList(){
            try{
                $loggedCustomer = ApiSupport::isCustomerLogged();

                if(!$loggedCustomer){
                    return parent::result(1, \Yii::t('api', 'You must login before'));
                }

                // Get store id
                $store_id = \Yii::$app->getRequest()->get('store_id');
//                var_dump($loggedCustomer);exit;
                if(!$store_id){
                    return parent::result(1, \Yii::t('api', 'Parameter "{param}" is missing', ['param' => 'store_id']));
                }
                if(!is_numeric($store_id)){
                    return parent::result(1, \Yii::t('api', 'Parameter "{param}" is invalid', ['param' => 'store_id']));
                }
                
                $search_query = (new Query())
                                ->select([
                                    'ticket_history.ticket_jan_code',
                                    'max(ticket.id) as t_id',
                                    'max(ticket_history.created_at) as created_at_d',
                                    'max(ticket.name) as name',
                                    'max(ticket.price) as price',
                                    'max(ticket.date_expiration) as date_expiration'
                                ])
                ->from(["ticket_history" => TicketHistory::tableName()])
                ->innerJoin(['ticket' => MasterTicket::tableName()], "ticket.ticket_jan_code = ticket_history.ticket_jan_code and ticket.store_id = ticket_history.store_id "
                        . "and ticket.del_flg = :zero");
                $search_query->innerJoin(MstOrder::tableName(), MstOrder::tableName() . '.order_code = ' . TicketHistory::tableName() . '.order_code');
                $search_query->andFilterWhere(['=', MasterTicket::tableName() . '.del_flg', 0]); 
                 $search_query->andFilterWhere(['=', MasterTicket::tableName() . '.store_id', $store_id]); 
                $search_query->andFilterWhere([MstOrder::tableName() . '.customer_jan_code' => $loggedCustomer->customer_jan_code]);                      
                $search_query->groupBy(["ticket_history.ticket_jan_code"]);
                $list_query = (new Query())
                        ->select([
                            't_id as id',
                            'name',
                            'price',
                            'date_expiration',
                            'ticket_history.ticket_balance as set_ticket',
                            'ticket_history.ticket_jan_code',
                            'created_at_d as created_at',
                            'ticket_history.order_code',
                        ])
                        ->from(["search" => $search_query])
                        ->innerJoin(['ticket_history' => TicketHistory::tableName()], "ticket_history.created_at = search.created_at_d "
                        . "and ticket_history.ticket_jan_code = search.ticket_jan_code");
                
                $list_query->addParams([
                    'zero' => '0'
                ]);
                $data = $list_query->all();
//                $masteticket = new \yii\db\Query();
//                $masteticket->select(['ticket.id', 'ticket.name', 'ticket.price', 'ticket.date_expiration', 'ticket_history.ticket_balance as set_ticket', 'ticket.ticket_jan_code' ])
//                                    ->from('ticket')
//                                    ->innerJoin('ticket_history', 'ticket_history.ticket_jan_code = ticket.ticket_jan_code')
//                                    ->innerJoin('mst_order', 'mst_order.order_code = ticket_history.order_code')
//                                    ->where(['ticket.del_flg' => 0])
//                                    ->andWhere(['ticket.store_id' => $store_id])
//                                    ->andWhere(['mst_order.customer_jan_code' => $loggedCustomer->customer_jan_code])
////                                    ->andWhere(['ticket_history.process_type' => '0'])
//                                    ->groupBy('ticket.id, ticket.name, ticket.price, ticket.date_expiration, ticket_history.ticket_balance, ticket.ticket_jan_code');
//
//                    $tickets = $masteticket->all();
                    
//                $tickets = MasterTicket::find()
//                            ->select([
//                                    'ticket.id',
//                                    'ticket.name',
//                                    'ticket.price',
//                                    'ticket.date_expiration',
//                                    'ticket.set_ticket',
//                                    'ticket.ticket_jan_code'])
//                            ->from('ticket')
//                            ->where(['ticket.del_flg' => 0])
//                            ->andWhere(['ticket.store_id' => $store_id])
//                            ->orderBy('ticket.updated_at')
//                            ->all();

                return parent::result(0, '', ['tickets' => $data]);
            } catch (Exception $ex) {
                return parent::result(1, $ex->getMessage(), null);
            }
        }
        
        /*
         * Get ticket info
         * 
         * @return json
         */
        public function actionDetail(){
            try{
                $loggedCustomer = ApiSupport::isCustomerLogged();

                if(!$loggedCustomer){
                    return parent::result(1, \Yii::t('api', 'You must login before'));
                }
                
                // Get parameter
                $ticket_id = \Yii::$app->getRequest()->get('ticket_id');

                // Check ticket id param
                if(!$ticket_id){
                    return parent::result(1, \Yii::t('api', 'Parameter "{param}" is missing', ['param' => 'ticket_id']));
                }
                if(!is_numeric($ticket_id)){
                    return parent::result(1, \Yii::t('api', 'Parameter "{param}" is invalid', ['param' => 'ticket_id']));
                }

                // Get store id
                $store_id = \Yii::$app->getRequest()->get('store_id');

                if(!$store_id){
                    return parent::result(1, \Yii::t('api', 'Parameter "{param}" is missing', ['param' => 'store_id']));
                }
                if(!is_numeric($store_id)){
                    return parent::result(1, \Yii::t('api', 'Parameter "{param}" is invalid', ['param' => 'store_id']));
                }
                $masterticket = new \yii\db\Query();
                
                            $masterticket->select([
                                    'ticket_history.id',
                                    'mst_order.order_code',
                                    'ticket_history.ticket_jan_code',
                                    'ticket_history.process_number',
                                    'ticket_history.process_type',
                                    'ticket_history.ticket_balance',
                                    'ticket_history.created_at',
                                    'ticket_history.created_by',
                                    'ticket_history.updated_at',
                                    'ticket_history.updated_by'])
                            ->from('ticket_history')
                            ->innerJoin('ticket', 'ticket.ticket_jan_code = ticket_history.ticket_jan_code')
                            ->innerJoin('mst_order', 'mst_order.order_code = ticket_history.order_code')                      
                            ->where(['ticket.id' => $ticket_id])
                            ->andWhere(['ticket.store_id' => $store_id])
                             ->andWhere(['mst_order.customer_jan_code' => $loggedCustomer->customer_jan_code])
                            ->andWhere(['ticket.del_flg' => 0]);
//                echo "<pre>";print_r($ticket);
                $ticketHistory = $masterticket->all();
                if(!$ticketHistory){
                    return parent::result(1, \Yii::t('api','{param} is not found', ['param' => 'Ticket']));
                }

                return parent::result(0, '', ['histories' => $ticketHistory]);

            } catch (Exception $ex) {
                return parent::result(1, $ex->getMessage(), null);
            }
        }
}
