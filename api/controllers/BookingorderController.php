<?php

namespace api\controllers;

use Carbon\Carbon;
use yii\helpers\ArrayHelper;
use Yii;
use common\models as Model;
use common\models\BookingSearch;
use common\models\MasterCustomer;
use common\models\MasterStore;
use common\models\BookingBusiness;
use common\models\MasterOption;
use api\models\ApiSupport;
use common\models\BookingChecklist;
use common\models\MasterSeatSearch;

class BookingorderController extends \yii\web\Controller {

    //public $modelClass = 'common\models\Booking';

    public function actionFinal() {
        $json = json_encode([
            'error_code' => 0,
            'message' => '',
        ]);

        return $this->render('/account/api-check', ['json' => $json, 'type' => 'onReservationCompleted']);
    }

    public function actionSuccess() {
        // query customer
        $customer = Model\MasterCustomer::find()->where(['user_id' => Yii::$app->user->id])->one();

        if ($customer) {
            return $this->render('success', [
                        'customer' => $customer
            ]);
        } else {
            return $this->needLogin();
        }
    }

    // STEP4
    public function actionConfirm() {
        if (!$this->getUser())
            return $this->needLogin();

        $store = MasterStore::findFrontEnd()
                ->where(['id' => Yii::$app->request->get('storeId')])
                ->one();

        // query customer
        $customer = Model\MasterCustomer::find()->where(['user_id' => $this->getUser()])->one();

        if ($customer) {
            $formModel = new Model\ApiFormBooking();
            // Get checklist from session
            $checklistSession = Yii::$app->session->get('booking-checklist');

            if (!empty(Yii::$app->request->post())) {
                $formModel->load(Yii::$app->request->post());
                if ($store->booking_resources === '01') {
                    $formModel->scenario = 'restaurant';
                }
                if ($formModel->validate()) {
                    $queryItems = self::queryAllItems($formModel->storeId, $formModel->coupons, $formModel->products, $formModel->staff);

                    $total_time = array_sum(ArrayHelper::getColumn($queryItems->items, 'time_require'));
                    $total_price = $queryItems->total_price;

                    $session = Yii::$app->session;

                    $checklists = [];
                    if (!empty($checklistSession)) {
                        foreach ($checklistSession as $types) {
                            foreach ($types as $checklistID => $answer) {
                                $checklists[$checklistID] = $answer;
                            }
                        }
                    }

                    if ($this->createBooking($formModel->storeId, $formModel->coupons, $formModel->products, (int) $formModel->staff, $formModel->date, $total_time, (int) $session->get('booking-point'), $session->get('booking-demand'), $total_price, $formModel->options, $checklists, $formModel->seat, $formModel->capacity)) {
                        return $this->redirect(['success?' . $_SERVER['QUERY_STRING']]);
                    } else {
                        return $this->redirect('insertfail');
                    }
                }
            }
            // Get data by check list
            $selectedChecklistData = $this->getChecklistFromSession($checklistSession);

            return $this->render('confirm', [
                        'customer' => $customer,
                        'formModel' => $formModel,
                        'checklists' => $selectedChecklistData
            ]);
        } else {
            return $this->needCustomer();
        }
    }

    // ajax
    // load confirm order detail
    public function actionLoadconfirmorderdetail() {
        $request = Yii::$app->request;

        if ($request->isAjax && $request->post('storeId')) {
            $coupons = [];
            $products = [];
            $options = [];
            $product_ids = array_filter(explode(',', $request->post('products')));

            if ($request->post('options')) {
                // build option list
                $product_option = [];
                $raw_options = array_filter(explode(',', $request->post('options')));
                foreach ($raw_options as $option) {
                    $explode = array_filter(explode('-', $option));
                    $product_ids[] = $explode[1];
                    $options[$explode[1]] = Model\MasterOption::findFrontEnd()->where(['id' => $explode[0]])->one();
                }
            }

            // try to query information and price
            $queryItems = self::queryAllItems($request->post('storeId'), $request->post('coupons'), $request->post('products'), $request->post('staff'), $request->post('options'));

            $staff_id = false;

            $display_options = [];
            // separate
            foreach ($queryItems->items as $id => $item) {
                if (isset($item['display_condition'])) {
                    $coupons[] = $id;
                } elseif (isset($item['avatar'])) {
                    $staff_id = $id;
                } elseif (isset($options[$item['id']])) {
                    $display_options[$id] = $options[$item['id']]->name;
                } else {
                    $products[] = $id;
                }
            }


            $total_price = $queryItems->total_price; // array_sum($queryItems->price_table);
            $point_use = (int) Yii::$app->session->get('booking-point');

            $customer = Model\MasterCustomer::find()->where(['user_id' => $this->getUser()])->one();

            // query current user point
            $customerStore = Model\CustomerStore::find()->where([
                        'customer_id' => $customer->id,
                        'store_id' => $request->post('storeId')
                    ])->one();

            // create if not found
            if (!$customerStore) {
                $customerStore = Model\CustomerStore::addCustomerStore($request->post('storeId'), $customer->id);
            }

            $total_point = $customerStore ? (int) $customerStore->total_point : 0;

            $pointSetting = Model\PointSetting::getPointSetting($customerStore->store->company_id);
            $final_price = $total_price - BookingBusiness::getMoneyFromPoint($point_use, $pointSetting);
            $point_gained = BookingBusiness::getPointFromMoney($final_price, $customerStore, $pointSetting);

            return $this->renderAjax('ajax/orderDetailList', [
                        'items' => $queryItems->items,
                        'total_price' => $total_price,
                        'coupons' => $coupons,
                        'products' => $products,
                        'options' => $display_options,
                        'staff_id' => $staff_id,
                        'point_use' => $point_use,
                        'total_point' => $total_point,
                        'final_price' => $final_price,
                        'point_gained' => $point_gained
            ]);
        }
    }

    // STEP3
    public function actionDetail() {
        if (!$this->getUser())
            return $this->needLogin();

        $request = Yii::$app->request;

        $productList = [];

        //$this->buildProductList($productList, $request->get('products'), $request->get('options'));

        $queryItems = self::queryAllItems($request->get('storeId'), $request->get('coupons'), $request->get('products'), $request->get('staff'), $request->get('options'));

        $total_price = $queryItems->total_price; //array_sum($queryItems->price_table);
        //$total_time  = array_sum(ArrayHelper::getColumn($queryItems->items, 'time_require'));
        // query customer
        $customer = Model\MasterCustomer::find()->where(['user_id' => $this->getUser()])->one();

        // Get Check list
        $checklists = \common\models\Checklist::getChecklistByStore(Yii::$app->request->get('storeId'));

        if ($customer) {
            // query current user point
            $point = Model\CustomerStore::find()->where([
                        'customer_id' => $customer->id,
                        'store_id' => $request->get('storeId')
                    ])->one();

            $total_point = $point ? (int) $point->total_point : 0;

            $formModel = new Model\ApiFormBookingDetail();

            if (!empty($request->post())) {
                $formModel->load(Yii::$app->request->post());

                if ($formModel->validate()) {
                    $requestData = $request->post();

                    $strValues = $this->convertPostChecklist($requestData, $checklists);
                    if ($strValues !== false) {
                        Yii::$app->session->set('booking-demand', $formModel->demand);
                        Yii::$app->session->set('booking-point', (int) $formModel->point_use);
                        Yii::$app->session->set('booking-checklist', $strValues);

                        return $this->redirect([
                                    $this->buildURL(['confirm', 'storeId', 'products', 'coupons', 'options', 'executeTime', 'staff', 'date', 'noOption', 'noStaff', 'seat', 'capacity'], Yii::$app->request->get())
                        ]);
                    }

                    $formModel->addError('checklist', Yii::t('frontend', 'Please input for reqired question'));
                }
            }

            $pointSetting = Model\PointSetting::getPointSetting();
            $pointYen = 0;
            $pointP = 1;

            if ($pointSetting) {
                $pointYen = $pointSetting->point_conversion_yen;
                $pointP = $pointSetting->point_conversion_p;
            }

            return $this->render('detail', [
                        'total_price' => $total_price,
                        'total_point' => $total_point,
                        'formModel' => $formModel,
                        'pointYen' => $pointYen,
                        'pointP' => $pointP,
                        'customer' => $customer,
                        'checklists' => $checklists
            ]);
        } else {
            return $this->needCustomer();
        }
    }

    // STEP2
    public function actionStaff() {
        if (!$this->getUser())
            return $this->needLogin();

        $request = Yii::$app->request;
        $store_id = $request->get('storeId');
        $execute_time = $request->get('executeTime');
        $staffs = [];
        $price_table = [];
        if ($store_id) {
            $start_date = Carbon::now()->format('Y-m-d');
            $end_date = Carbon::now()->addDays(7)->format('Y-m-d');

            $time_execute = $execute_time;
            $number_row = $time_execute / 30;

            $store = Model\MasterStore::find()->where(['id' => $store_id])->one();

            if ($store) {
                $staffs = BookingSearch::searchStaffByDateTime($store_id, $number_row, null, $start_date, $end_date, $store['reservations_possible_time']);

                $price_table = ArrayHelper::index(BookingBusiness::getPriceTable('', implode(',', array_filter(ArrayHelper::getColumn($staffs, 'assign_product_id'))))['products'], 'id');
            }
        }

        return $this->render('staff', [
                    'staffs' => $staffs,
                    'price_table' => $price_table
        ]);
    }

    public function actionSeat() {
        if (!$this->getUser())
            return $this->needLogin();

        $request = Yii::$app->request;

        $formModel = new Model\ApiFormBookingSeat();

        $formModel->attributes = $request->get();

//        $start_date = Carbon::now()->format('Y-m-d');
//        $end_date = Carbon::now()->addDays(14)->format('Y-m-d');
//        $execute_time = $request->post('executeTime');
        $seats = [];
        if (isset($_GET['capacity'])) {
            if ($formModel->validate()) {
                $seats = ArrayHelper::index(Model\MasterSeatSearch::getDetailedSeatSchedule([
                                    'store_id' => $request->get('storeId'),
                                    'capacity' => $request->get('capacity')
                                ]), 'id');

                if (isset($_GET['seat'])) {
                    if ($request->get('seat') === '0' || key_exists($request->get('seat'), $seats)) {
                        return $this->redirect(['restaurant',
                                    'storeId' => $formModel->storeId,
                                    'products' => $formModel->products,
                                    'options' => $formModel->options,
                                    'coupons' => $formModel->coupons,
                                    'noOption' => $formModel->noOption,
                                    'executeTime' => $formModel->executeTime,
                                    'capacity' => $formModel->capacity,
                                    'seat' => $formModel->seat
                        ]);
                    }
                }
            }
        }

        return $this->render('seat', [
                    'formModel' => $formModel,
                    'seats' => $seats
        ]);
    }

    // ajax
    // list available staff
    public function actionLoadstaff() {
        $request = Yii::$app->request;
        $store_id = $request->post('storeId');
        $execute_time = $request->post('executeTime');
        if ($request->isAjax && $store_id) {
            $start_date = Carbon::now()->format('Y-m-d');
            $end_date = Carbon::now()->addDays(14)->format('Y-m-d');

            $time_execute = $execute_time;
            $number_row = $time_execute / 30;

            $store = Model\MasterStore::find()->where(['id' => $store_id])->one();

            if ($store) {
                $staffs = BookingSearch::searchStaffByDateTime($store_id, $number_row, false, $start_date, $end_date, $store['reservations_possible_time']);

                $price_table = BookingSearch::getProductPriceTable(implode(',', array_filter(ArrayHelper::getColumn($staffs, 'assign_product_id'))));
                $price_table = ArrayHelper::index($price_table, 'id');

                return $this->renderAjax('ajax/selectItemList', [
                            'items' => $staffs,
                            'price_table' => $price_table
                ]);
            }
        }
    }

    // salon schedule
    public function actionSalon() {
        if (!$this->getUser())
            return $this->needLogin();

        // Variable week current
        $week = 0;
        // Variable date current
        $date = date('Y-m-d');

        $request = Yii::$app->request;

        // Get param time execute and store id
        if (($request->get('executeTime') !== '') && !empty($request->get('storeId'))) {
            $store_id = $request->get('storeId');
            $time_execute = (int) $request->get('executeTime');
            $hours = floor($time_execute / 60);
            $minutes = $time_execute % 60;
            $number_row = ceil($time_execute / 30);
            $time_execute = $hours . ':' . $minutes;
        } else {
            return '<h1>ExecuteTime Not Found</h1>';
        }

        $time_string = '';
        if ($hours)
            $time_string .= $hours . '時間 ';
        if ($minutes)
            $time_string .= $minutes . '分';

        // Get param week
        if (!empty($request->get('week'))) {

            $week = $request->get('week');
            $date = date('Y-m-d', strtotime($date . ' +' . $request->get('week') * 7 . ' day'));
        }

        $start_date = $date;
        $numberOfDay = 7;

        $end_date = date('Y-m-d', strtotime($date . " +$numberOfDay day"));

        $store = Model\MasterStore::find()->where(['id' => $store_id])->one();

        // Get all Store Schedule Temp between start_date and end_date
        $storeScheduleTemps = BookingSearch::searchStoreScheduleBetweenDates($store_id, $number_row, $time_execute, $start_date, $end_date, $store['reservations_possible_time'], $request->get('staff'));

        for ($i = 0; $i < $numberOfDay; $i++) {
            $model[$i]['date'] = date('Y-m-d', strtotime($date . ' +' . $i . ' day'));
            foreach ($storeScheduleTemps as $storeScheduleTemp) {

                $date_storeSchedule = date_create($storeScheduleTemp['booking_date']);
                if (date_format($date_storeSchedule, "Y-m-d") == $model[$i]['date']) {
                    $model[$i][$storeScheduleTemp['booking_time']] = $storeScheduleTemp['booking_status'];
                }
            }
        }

        // Setting request again
        $setting['date'] = $date;
        $setting['week'] = $week;
        $setting['time_execute'] = $time_execute;
        $setting['store_id'] = $store_id;

        // Row number calendar
        $count = (strtotime($store->time_close) - strtotime($store->time_open)) / (60 * 30);
        $store_schedule = ArrayHelper::index(Model\StoreSchedule::find()
                                ->andWhere(['store_id' => $store->id])
                                ->andWhere(['>=', 'schedule_date', $start_date])
                                ->andWhere(['<=', 'schedule_date', $end_date])
                                ->all(), 'schedule_date');

        return $this->render('salon', [
                    'setting' => $setting,
                    'model' => $model,
                    'timeString' => $time_string,
                    'count' => $count,
                    'time_open' => $store->time_open,
                    'numberOfDay' => $numberOfDay,
                    'store_schedule' => $store_schedule,
                    'store' => $store,
                    'number_row' => $number_row,
        ]);
    }

    public function actionRestaurant() {
        if (!$this->getUser())
            return $this->needLogin();

        // Variable week current
        $week = 0;
        // Variable date current
        $date = date('Y/m/d');

        $request = Yii::$app->request;

        // Get param time execute and store id
        if (($request->get('executeTime') !== '') && !empty($request->get('storeId'))) {
            $store_id = $request->get('storeId');
            $time_execute = (int) $request->get('executeTime');
            $hours = floor($time_execute / 60);
            $minutes = $time_execute % 60;
            $number_row = ceil($time_execute / 30);
            $time_execute = $hours . ':' . $minutes;
            $time_minutes = $hours * 60 + $minutes;
        } else {
            return '<h1>ExecuteTime Not Found</h1>';
        }

        $store = MasterStore::findFrontEnd()
                ->where(['id' => $request->get('storeId')])
                ->one();

        $time_string = '';
        if ($hours) {
            $time_string .= $hours . '時間 ';
        }
        if ($minutes) {
            $time_string .= $minutes . '分';
        }

        // Get param week
        if (!empty($request->get('week'))) {

            $week = $request->get('week');
            $date = date('Y/m/d', strtotime($date . ' +' . $request->get('week') * 7 . ' day'));
        }

        $start_date = $date;
        $numberOfDay = 7;
        $end_date = date('Y-m-d', strtotime($date . " +$numberOfDay day"));

        $query_params = [
            'store_id' => $store['id'],
            'from_date' => strtr($start_date, '/', '-'),
            'end_date' => strtr($end_date, '/', '-'),
            'time_require' => $time_minutes
        ];

        $schedule = MasterSeatSearch::getDetailedSchedule($query_params);

        $seat_count = count(MasterSeatSearch::getDetailedSeatSchedule($query_params));
        $model = [];
        for ($i = 0; $i < $numberOfDay; $i++) {
            $model[$i]['date'] = date('Y-m-d', strtotime($date . ' +' . $i . ' day'));
        }

        // Setting request again
        $setting['date'] = $date;
        $setting['week'] = $week;
        $setting['time_execute'] = $time_execute;
        $setting['store_id'] = $store_id;

        // Row number calendar
        $count = (strtotime($store->time_close) - strtotime($store->time_open)) / (60 * 30);
        $store_schedule = ArrayHelper::index(Model\StoreSchedule::find()
                                ->andWhere(['store_id' => $store->id])
                                ->andWhere(['>=', 'schedule_date', $start_date])
                                ->andWhere(['<=', 'schedule_date', $end_date])
                                ->all(), 'schedule_date');

        return $this->render('restaurant', [
                    'setting' => $setting,
                    'model' => $model,
                    'timeString' => $time_string,
                    'count' => $count,
                    'time_open' => $store->time_open,
                    'schedule' => $schedule,
                    'reservation_possible_seconds' => $store['reservations_possible_time'] * 60,
                    'number_row' => $number_row,
                    'seat_count' => $seat_count,
                    'store_schedule' => $store_schedule,
                    'store' => $store,
                    'numberOfDay' => $numberOfDay
        ]);
    }

    // STEP1
    // detect empty options for instantly redirecting to step 3
    // return false if no options found
    public function actionOptions() {
        $request = Yii::$app->request;

        if ($this->getUser()) {
            $store = MasterStore::findFrontEnd()
                    ->where(['id' => $request->get('storeId')])
                    ->one();

            if ($store && $store->booking_resources === '01') {
                $mode = 'seat';
            } else {
                $mode = 'staff';
            }

            $product_options = BookingSearch::searchOptionProductByIdCoupon($request->get('coupons'), $request->get('storeId'));
            $product_options = ArrayHelper::merge($product_options, BookingSearch::searchOptionProductByIdProduct($request->get('products')));

            if (count($product_options) === 0) {
                $queryItems = self::queryAllItems($request->get('storeId'), $request->get('coupons'), $request->get('products'));

                $total_time = array_sum(ArrayHelper::getColumn($queryItems->items, 'time_require'));

                $get = $request->get();
                $get['executeTime'] = $total_time;

                return $this->redirect($this->buildURL([$mode, 'storeId', 'products', 'coupons'], $get) . '&noOption=1&executeTime=' . $total_time);
            } else {
                // arrange and gather option - product
                $options = [];

                $selected_products = array_filter(explode(',', $request->post('products')));
                $option_info = [];
                $search_by_names = [];
                // merge options
                foreach ($product_options as $option) {
                    $id = $option['id'];
                    $option_id = $option['option_id'];
                    $option_type = $option['option_type'];

                    $option_info[$option_id]['name'] = $option['option_name'];
                    $option_info[$option_id]['type'] = $option_type;
                    // due to option_product_id
                    $search_by = $option['search_by'];
                    if ($option_type == '1') {
                        if (!in_array($id, $selected_products)) {
                            if (!isset($option_info[$option_id]['search_by'])) {
                                $option_info[$option_id]['search_by'] = $search_by;
                            }

                            // merge product in option
                            $options[$option_id][$id] = $option;
                        }
                    } else {
                        // separated by search_by
                        $options[$option_id][$search_by][$id] = $option;
                        $search_by_names[$option_id][$search_by] = $option['search_by_name'];
                    }
                }

                $price_table = BookingSearch::getProductPriceTable(implode(',', ArrayHelper::getColumn($product_options, 'id')));
                $price_table = ArrayHelper::index($price_table, 'id');

                return $this->render('options', [
                            'options' => $options,
                            'option_info' => $option_info,
                            'search_by_names' => $search_by_names,
                            'price_table' => $price_table,
                            'mode' => $mode
                ]);
            }
        } else {
            return $this->needLogin();
        }
    }

    // ajax
    // load order detail by coupons or products
    public function actionLoadorderdetail() {
        $request = Yii::$app->request;

        if ($request->isAjax && $request->post('storeId')) {
            // try to query information and price
            $queryItems = self::queryAllItems($request->post('storeId'), $request->post('coupons'), $request->post('products'));

            return $this->renderAjax('ajax/orderItemList', [
                        'items' => $queryItems->items,
                        'price_table' => $queryItems->price_table,
                        'total_price' => $queryItems->total_price,
                        'total_time' => array_sum(ArrayHelper::getColumn($queryItems->items, 'time_require')),
                            //'total_point' => $total_point
            ]);
        }
    }

    // STEP 1
    public function actionIndex() {
        if (!$this->getUser())
            return $this->needLogin();

        return $this->render('index');
    }

    public function actionLoadcouponsandproducts() {
        $request = Yii::$app->request;
        if ($request->post('storeId')) {
            $page = (int) $request->post('page');
            $per_page = 10; // number of items shown on 1 page
            $item_left_count = $per_page;
            $started_at = $page * $per_page;
            $store_id = $request->post('storeId');
            $categories = $request->post('categories');
            $modes = $request->post('modes');
            $items = [];
            $price_tables = [];

            $customer = Model\MasterCustomer::find()->where(['user_id' => $this->getUser()])->one();
            $modes = !empty($modes) ? explode(',', $modes) : ['coupons', 'products'];

            $searches = [];

            $coupon_search = BookingSearch::searchCouponsByStoreId($store_id, $categories, $customer);

            $searches['coupons'] = $coupon_search;
            $searches['products'] = BookingSearch::searchProductsByStoreId($store_id, $categories);

            /** choose which items to display */
            $current_max_index = 0;
            foreach ($modes as $i => $mode) {
                if (key_exists($mode, $searches)) {
                    /* @var \yii\db\Query $search */
                    $search = $searches[$mode];
                    if ($item_left_count <= 0) {
                        break; // exit loop when per_page reach
                    } else {
                        $search_count = $search->count();
                        $current_max_index += $search_count;
                        // check if reach started_at
                        if ($current_max_index < $started_at) {
                            continue; // to next search
                        } else {
                            if ($i != 0) {
                                $started_at = 0;
                            }
                            if ($mode === 'coupons') {
                                /** coupon price search and order */
                                $coupon_price_search = BookingBusiness::getCouponPriceTable(function ($query) use ($store_id) {
                                            $query->andWhere(['c.store_id' => $store_id]);
                                        });
                                $search->leftJoin(['price_search' => $coupon_price_search], 'price_search.id = c.id');
                                $search->orderBy([
                                    'apply_condition' => SORT_ASC,
                                    'expire_date' => SORT_ASC,
                                    'price_search.type' => SORT_ASC,
                                    'price_search.discount_yen' => SORT_DESC,
                                    'price_search.discount_percent' => SORT_DESC,
                                    'price_search.discount_price_set' => SORT_ASC,
                                    'price_search.discount_drink_eat' => SORT_ASC
                                ]);
                                /** end coupon price search and order */
                            } elseif ($mode === 'products') {
                                $search->orderBy(['category_id' => SORT_ASC]);
                            }

                            $search_items = $search->offset($started_at)->limit($item_left_count)->all();

                            if ($mode === 'coupons') {
                                $price_tables['coupon'] = BookingBusiness::getPriceTable(implode(',', ArrayHelper::getColumn($search_items, 'id')))[$mode];
                            } elseif ($mode === 'products') {
                                $price_tables['product'] = BookingBusiness::getPriceTable('', implode(',', ArrayHelper::getColumn($search_items, 'id')))[$mode];
                            }
                            $item_left_count = $per_page - count($search_items);
                            $items = array_merge($items, $search_items);
                            //echo sprintf('%s:%s:%s:%s~%s<br>', $mode, $search_count, $started_at, count($search_items), $item_left_count);
                        }
                    }
                }
            }
            /** end choose which items to display */
            return $this->renderAjax('ajax/selectCouponProductItemList', [
                        'items' => $items,
                        'price_table' => $price_tables
            ]);
        }
    }

    // ajax
    // load all coupons by store id ( or with categories )
    public function actionLoadcoupons() {
        $request = Yii::$app->request;

        if ($request->isAjax && $request->post('storeId')) {
            $customer = Model\MasterCustomer::find()->where(['user_id' => $this->getUser()])->one();

            $items = BookingSearch::searchCouponsByStoreId($request->post('storeId'), $request->post('categories'), $customer)->orderBy(['apply_condition' => SORT_DESC, 'expire_date' => SORT_ASC, 'created_at' => SORT_DESC])->all();

            $price_table = Model\BookingBusiness::getPriceTable(implode(',', ArrayHelper::getColumn($items, 'id')))['coupons'];
            // sort using price
            $this->sortCoupons($items, $price_table);

            return $this->renderAjax('ajax/selectItemList', [
                        'items' => $items,
                        'price_table' => $price_table
            ]);
        }
    }

    // load all products by store id ( or with categories )
    public function actionLoadproducts() {
        $request = Yii::$app->request;

        if ($request->isAjax && $request->post('storeId')) {
            $items = BookingSearch::searchProductsByStoreId($request->post('storeId'), $request->post('categories'));

            $items = $items->orderBy(['category_id' => SORT_ASC])->all();

            $price_table = ArrayHelper::index(BookingSearch::getProductPriceTable(implode(',', ArrayHelper::getColumn($items, 'id'))), 'id');

            // sort using price
            $this->sortProducts($items, $price_table);

            return $this->renderAjax('ajax/selectItemList', [
                        'items' => $items,
                        'price_table' => $price_table
            ]);
        }
    }

    // load all categories by store id
    public function actionLoadcategories() {
        $request = Yii::$app->request;

        if ($request->isAjax && $request->post('storeId')) {
            //$page = $request->post('page') ? $request->post('page') : 1;

            $items = BookingSearch::searchCategoryByStoreId($request->post('storeId'));

            return $this->renderAjax('ajax/filterList', [
                        'items' => $items
            ]);
        }
    }

    public function buildURL($parts, $params) {
        $url = $parts[0];
        array_shift($parts);

        if (count($parts)) {
            $value = isset($params[$parts[0]]) ? $params[$parts[0]] : '';
            $url .= '?' . $parts[0] . '=' . $value;
        }

        array_shift($parts);

        foreach ($parts as $key) {
            $value = isset($params[$key]) ? $params[$key] : '';
            $url .= '&' . $key . '=' . $value;
        }

        return $url;
    }

    //=========================
    // HELPERS
    public function getUser() {
        if (!Yii::$app->user->isGuest) {
            return Yii::$app->user->id;
        }

        $session = \Yii::$app->session;

        $token = \Yii::$app->request->headers->get('Authorization');

        if ($token == null) {
            $token = $session->get('token_param');
        } else {
            $session->set('token_param', $token);
        }

        if (!$token) {
            return false;
        }

        $tokenObject = ApiSupport::isValidApiToken($token);

        if (!$tokenObject) {
            return false;
        } else {
            /* if (!Yii::$app->user->isGuest) {
              $login = new ApiLoginForm();

              $user = User::find()->where(['id' => $tokenObject->id])->one();
              $login->login($user, 3600 * 24 * 30);
              } */

            return $tokenObject->id;
        }
    }

    public function needLogin() {
        $json = json_encode([
            'error_code' => 1,
            'customer_jan_code' => '',
            'message' => 'You must login before',
        ]);

        //return (new \api\models\ApiLoginForm())->login(Model\User::findOne(['id' => 53]));

        return $this->render('/account/api-check', ['json' => $json, 'type' => 'LoginCheck']);
    }

    public function needCustomer() {
        return $this->needLogin(); //return $this->redirect(['customernotfound']);
    }

    public function actionCustomernotfound() {
        return '<h1>NO CUSTOMER FOUND</h1>';
    }

    public function actionInsertfail() {
        return $this->render('insertfail');
    }

    public function createList($list) {
        $ret = [];
        foreach ($list as $item) {
            if ($item !== '') {
                
            }
        }
    }

    public static function sortProducts(&$items, $price_table) {
        uasort($items, function ($p1, $p2) use ($price_table) {
            $pid1 = $p1['id'];
            $pid2 = $p2['id'];

            // obey category_id order
            if ($p1['category_id'] < $p2['category_id']) {
                return -1;
            } else if ($p1['category_id'] > $p2['category_id']) {
                return 1;
            }

            if (!isset($price_table[$pid1])) {
                return -1;
            }

            if (!isset($price_table[$pid2])) {
                return 1;
            }

            $price1 = $price_table[$pid1]['total_price'];
            $price2 = $price_table[$pid2]['total_price'];

            if ($price1 < $price2) {
                return -1;
            } elseif ($price1 > $price2) {
                return 1;
            } else {
                return 0;
            }
        });
    }

    public function sortCoupons(&$items, $price_table) {
        // sort using price
        uasort($items, function ($p1, $p2) use ($price_table) {
            $pid1 = $p1['id'];
            $pid2 = $p2['id'];

            // obey apply_condition order
            if ($p1['apply_condition'] > $p2['apply_condition']) {
                return -1;
            } else if ($p1['apply_condition'] < $p2['apply_condition']) {
                return 1;
            }

            if (!isset($price_table[$pid1])) {
                return -1;
            }

            if (!isset($price_table[$pid2])) {
                return 1;
            }

            $price1 = $price_table[$pid1]['total_price'];
            $price2 = $price_table[$pid2]['total_price'];

            if ($price1 < $price2) {
                return -1;
            } elseif ($price1 > $price2) {
                return 1;
            } else {
                return 0;
            }
        });
    }

    public function createBooking($store_id, $coupons, $products, $staff, $date, $execute_time, $points, $demand, $price, $options = '', $checklists = [], $type_seat_id, $actual_load = 1) {
        $transaction = Yii::$app->db->beginTransaction();
        if (!$staff)
            $staff = null;
        if (!$type_seat_id)
            $type_seat_id = null;
        try {
            $booking = new Model\Booking();
            $booking->status = Model\Booking::BOOKING_STATUS_PENDING;

            // booking method
            $booking->booking_method = '03';
            //----------------------------
            // demand
            $booking->demand = $demand;
            //----------------------------
            // number of people
            $booking->number_of_people = $actual_load;

            //save storeBooked info
            $customer = MasterCustomer::find()->where(['user_id' => Yii::$app->user->id])->one();
            $booking->customer_id = !empty($customer) ? $customer->id : 0;
            $booking->store_id = $store_id;

            // booking date time
            $start_time = Carbon::createFromTimestamp($date);

            //save staffBooked info
            $booking->booking_date = $start_time->format('Y-m-d');
            $booking->staff_id = $staff;
            $booking->start_time = $start_time->format('H:i');
            $booking->end_time = Carbon::createFromTimestamp($date)->addMinutes(ceil($execute_time / 30) * 30)->format('H:i');

            $booking->point_use = $points . '';
            $booking->type_seat_id = $type_seat_id;
            // add staff
            $staff = \common\models\MasterStaff::find()->where(['id' => $staff])->one();
            if ($staff) {
                if ($staff->assign_product_id) {
                    $products .= ',' . $staff->assign_product_id;
                }

                $booking->customer_specify = '1';
            }

            $sumPriceProduct = [];

            //$product_ids  = array_filter(explode(',', $products));
            // DUE TO COUPON_PRODUCT_ID
            $option_list = array_filter(explode(',', $options));
            $option_count = count($option_list);
            $productsOption = [];
            foreach ($option_list as $option) {
                $explode = explode('-', $option);
                $product_id = $explode[1];
                $option_id = $explode[0];
                $search_by = $explode[2];

                $productsOption[] = $option_id . '/' . $product_id . '/' . $search_by;
            }
            $sumPriceProduct = 0;

            $result = BookingBusiness::getPriceTable($coupons, $products, implode(',', $productsOption), $booking->booking_date);
            //money from point to discount
            $moneyPoint = $booking->moneyFromPoint;

            //TODO can xac nhan booking_price
            $booking->booking_price = $result['total_price'];
            $booking->booking_discount = $moneyPoint;
            $booking->booking_total = $result['total_price'] - $moneyPoint;
            $booking->save();

            $errors = $booking->errors;

            // save coupons to table bookingcoupon
            $bookedCoupons = array_filter(explode(',', $coupons));
            foreach ($bookedCoupons as $couponId) {
                $sumPriceProduct = isset($result['coupons'][$couponId]) ? $result['coupons'][$couponId]['total_price'] : 0;
                $bookingCoupon = new \common\models\BookingCoupon();
                $bookingCoupon->booking_id = $booking->id;
                $bookingCoupon->coupon_id = $couponId;
                $bookingCoupon->quantity = 1;
                $bookingCoupon->unit_price = $sumPriceProduct;
                $bookingCoupon->save();

                $errors = ArrayHelper::merge($errors, $bookingCoupon->errors);
            }

            //save product to table booking_product
            //check thoi gian thuc thi
            $bookedProducts = array_filter(explode(',', $products));

            $product_no_option_count = count($bookedProducts);
            // DUE TO COUPON_PRODUCT_ID
            $option_list = array_filter(explode(',', $options));
            $option_count = count($option_list);
            $options = [];

            if ($option_count > 0) {
                foreach ($option_list as $option) {
                    $explode = explode('-', $option);
                    $product_id = $explode[1];
                    $option_id = $explode[0];
                    $search_by = $explode[2];

                    if (!isset($options[$option_id])) {
                        $options[$option_id] = [];
                    }

                    $options[$option_id][$search_by] = $product_id;

                    $bookedProducts[] = $product_id;
                }

                $product_no_option_count = count($bookedProducts) - $option_count;
            }

            foreach ($bookedProducts as $productId) {
                if ($product_no_option_count > 0) {
                    $sumPriceProduct = isset($result['products'][$productId]) ? $result['products'][$productId]['total_price'] : 0;
                    $bookingProduct = new \common\models\BookingProduct();
                    $bookingProduct->booking_id = $booking->id;
                    $bookingProduct->product_id = $productId;
                    $bookingProduct->quantity = 1;

                    $bookingProduct->unit_price = $sumPriceProduct;
                    $product_no_option_count--;
                    $bookingProduct->save();

                    $errors = ArrayHelper::merge($errors, $bookingProduct->errors);
                }
            }

            foreach ($options as $option_id => $option) {
                $option_search = MasterOption::findFrontEnd()->where(['id' => $option_id])->one();
                if ($option_search) {
                    foreach ($option as $search_by => $productId) {

                        $keyOption = $option_id . '/' . $productId . '/' . $search_by;
                        $sumPriceProduct = isset($result['options'][$keyOption]) ? $result['options'][$keyOption]['total_price'] : 0;

                        $bookingProduct = new \common\models\BookingProduct();
                        $bookingProduct->booking_id = $booking->id;
                        $bookingProduct->product_id = $productId;
                        $bookingProduct->quantity = 1;

                        $bookingProduct->unit_price = $sumPriceProduct;
                        $bookingProduct->option_id = $option_id;
                        if ($option_search->type != '1') {
                            $bookingProduct->option_product_id = $search_by;
                        }
                        $bookingProduct->save();

                        $errors = ArrayHelper::merge($errors, $bookingProduct->errors);
                    }
                }
            }

            // Insert checklist
            // With one check list have one, multi, none value (agree checkbox), or Freedom
            // Example data: 123,456,678 or 123 or (empty here) or "Content in here"
            // Agree: When choose type 1: (注意事項) 
            // One: When choose type 2 (質問事項) and choose option 1 (一つのみ選択) then will save 123
            // Multi: When choose type 2 (質問事項) and choose option 2 (複数選択) then will save 123,456
            // Freedom: When choose type 2 (質問事項) and choose option 3 (フリー入力) then will save "Your text"
            // To get data from this string, you must explode value by ','
            foreach ($checklists as $checklistID => $content) {
                $bookingChecklist = new BookingChecklist();
                //$bookingChecklist->id = 1;
                $bookingChecklist->booking_id = $booking->id;
                $bookingChecklist->checklist_id = (int) $checklistID;
                $bookingChecklist->answer_list = $content;
                $bookingChecklist->save();

                $errors = ArrayHelper::merge($errors, $bookingChecklist->errors);
            }

            // END DUE ....

            $ceil_end_time = Carbon::createFromTimestamp($date)->addMinutes(ceil($execute_time / 30) * 30);

            \backend\controllers\BookingController::saveShiftToScheduleDetail($booking->staff_id, $booking->booking_date, $booking->store_id, $booking->booking_code, $booking->start_time, $ceil_end_time->format('H:i'), \common\models\StaffScheduleDetail::BOOKING_STATUS_BUSY);

            if (count($errors) > 0) {
                throw new \Exception("Error when saving into database", 1);
            }


//            var_dump($bookedCoupons);
//              var_dump($errors);
//              var_dump($bookedProducts);
//              var_dump($options);
//              var_dump($booking->staff_id);
//              var_dump($demand);
//              var_dump($points);
//              var_dump($price);
//              var_dump('---------------');
//              var_dump($booking->booking_price);
//              var_dump($booking->booking_discount);
//              var_dump($booking->booking_total);

            BookingBusiness::PushMessage(BookingBusiness::PUSH_TIMING_BOOKING, $booking->id);
            BookingBusiness::PushMessage(BookingBusiness::PUSH_TIMING_BOOKING_BEFORE, $booking->id);
            $transaction->commit();

            return true;
        } catch (\Exception $e) {
//            var_dump($e);
            $transaction->rollback();
            return false;
        }
    }

    /**
     * Getcheck list by checklist id of session
     */
    public static function getChecklistFromSession($checklistSession = []) {
        if (!is_array($checklistSession)) {
            return [];
        }

        $checklistIDs = [];
        foreach ($checklistSession as $type => $types) {
            $checklistIDs = array_merge($checklistIDs, array_keys($types));
        }

        $checklists = \common\models\Checklist::find()->where(['id' => $checklistIDs])->all();

        $resultList = [];
        foreach ($checklists as $checklist) {
            if ($checklist->type == 1) {
                // Note checked
                $resultList[] = (object) ['type' => $checklist->type, 'question' => $checklist->question, 'questionAnswers' => true];
            } elseif ($checklist->type == 2) {
                $value = (object) ['type' => $checklist->type, 'option' => $checklist->question->option, 'question' => $checklist->question, 'questionAnswers' => []];

                if ($checklist->question->option == 1) {
                    // one checked
                    foreach ($checklist->question->questionAnswers as $aKey => $answer) {
                        if ($answer->id == $checklistSession['one'][$checklist->id]) {
                            $value->questionAnswers[] = $answer;
                            break;
                        }
                    }
                } elseif ($checklist->question->option == 2) {
                    // one checked
                    $answers = explode(',', $checklistSession['multi'][$checklist->id]);

                    foreach ($checklist->question->questionAnswers as $aKey => $answer) {
                        if (in_array($answer->id, $answers)) {
                            $value->questionAnswers[] = $answer;
                        }
                    }
                } else {
                    // Freedom
                    $value->questionAnswers = $checklistSession['freedom'][$checklist->id];
                }

                $resultList[] = $value;
            }
        }
        return $resultList;
    }

    public static function convertPostChecklist($requestData, $checklists) {
        $strValues = [];
        $group = ArrayHelper::index($checklists, 'id');

        if (count($checklists) == 0) {
            return $strValues;
        }

        // One: When choose type 2 (質問事項) and choose option 1 (一つのみ選択) then will save 123
        if (!empty($requestData['checklist']['one'])) {
            foreach ($requestData['checklist']['one'] as $key => $value) {
                $strValues['one'][$key] = $value;
                unset($group[$key]);
            }
        }

        // Multi: When choose type 2 (質問事項) and choose option 2 (複数選択) then will save 123,456
        if (!empty($requestData['checklist']['multi'])) {
            foreach ($requestData['checklist']['multi'] as $key => $value) {
                $strValues['multi'][$key] = implode(',', array_keys($value));
                unset($group[$key]);
            }
        }

        // Freedom: When choose type 2 (質問事項) and choose option 3 (フリー入力) then will save "Your text"
        if (!empty($requestData['checklist']['freedom'])) {
            foreach ($requestData['checklist']['freedom'] as $key => $value) {
                if ($value !== '') {
                    $strValues['freedom'][$key] = $value;
                    unset($group[$key]);
                }
            }
        }

        // Agree: When choose type 1: (注意事項), this get by a checkbox
        // when checked its mean will exists key in here also the data is empty
        if (!empty($requestData['checklist']['agree'])) {
            foreach ($requestData['checklist']['agree'] as $key => $value) {
                $strValues['agree'][$key] = '';
                unset($group[$key]);
            }
        }

        if (count($group) > 0) {
            return false;
        }

        return $strValues;
    }

    public function buildProductList(&$productList, $products, $options) {
        $product_ids = array_filter(explode(',', $products));
        $product_count = count($product_ids);
        $option_list = array_filter(explode(',', $options));
        $option_count = count($option_list);
        if ($option_count > 0) {
            foreach ($option_list as $option) {
                // option_id - product_id - search_by
                $explode = explode('-', $option);
                $product_ids[] = $explode[1];
            }
        }

        $productList = $product_ids;
        return $product_count;
    }

    /**
     * 
     * @param type $store_id
     * @param array $coupons
     * @param array $products
     * @param type $staff
     * @param type $options
     * @return \stdClass
     */
    public static function queryAllItems($store_id, $coupons = '', $products = '', $staff = '', $options = '') {
        $product_ids = array_filter(explode(',', $products));
        $coupon_ids = array_filter(explode(',', $coupons));
        // if request coupons
        $coupon_search = ArrayHelper::index(BookingSearch::searchCouponsById(implode(',', $coupon_ids), $store_id), 'id');
        $option_ids = [];

        $extra_products = [];
        if (preg_match('/^\d+\-\d+\-\d+(,\d+\-\d+\-\d+){0,}$/', $options)) {
            $option_ids = array_filter(explode(',', $options));

            foreach ($option_ids as $optionProduct) {
                $ex = explode('-', $optionProduct);
                // option-product-searchby
                /* if(!isset($options[$ex[1]])) {
                  $options[$ex[1]] = [];
                  } */
                //$options[] = MasterOption::findFrontEnd()->where(['id' => $ex[0]])->one();
                $extra_products[] = $ex[1];
                //$option_products[$ex[1]] = $product_search[$ex[1]];
                //$product_no_option_count--;
            }
        }
        // if request products
        $product_search = ArrayHelper::index(BookingSearch::searchProductsById(implode(',', array_merge($product_ids, $extra_products)), $store_id), 'id');

        // if request staff
        $staffs = BookingSearch::searchStaffById($staff, $store_id);

        $product_staff_price_id_list = ArrayHelper::merge(
                        $product_ids,
                        // staff.assign_product_id is nullable!
                        array_filter(ArrayHelper::getColumn($staffs, 'assign_product_id'))
        );

        $price_search = Model\BookingBusiness::getPriceTable(implode(',', $coupon_ids), implode(',', $product_staff_price_id_list), strtr(implode(',', $option_ids), '-', '/'));

        // then build a price_table
        $price_table = [];
        $items = [];
        $coupons = [];
        $products = [];
        $coupon_ids = ArrayHelper::getColumn($price_search['coupons'], 'id');

        foreach ($coupon_ids as $coupon_id) {
            if (isset($coupon_search[$coupon_id])) {
                $items[] = $coupon_search[$coupon_id];
                //$price_table[] = isset($coupon_price_table[$coupon_id]) ? (int) $coupon_price_table[$coupon_id]['total_price'] : 0;
                // new
                $price = '¥0';
                if (isset($price_search['coupons'][$coupon_id])) {
                    $price = BookingBusiness::displayMoney($price_search['coupons'][$coupon_id]);
                }
                $price_table[] = $price;
            }
        }

        foreach ($product_ids as $product_id) {
            if (isset($product_search[$product_id])) {
                $items[] = $product_search[$product_id];
                //price_table[] = isset($product_staff_price_table[$product_id]) ? (int) $product_staff_price_table[$product_id]['total_price'] : 0;
                // new
                $price = '¥0';
                if (isset($price_search['products'][$product_id])) {
                    $price = BookingBusiness::displayMoney($price_search['products'][$product_id]);
                }
                $price_table[] = $price;
            }
        }

        foreach ($option_ids as $option) {
            $ex = explode('-', $option);
            $product_id = $ex[1];
            $option_key = strtr($option, '-', '/');
            if (isset($product_search[$product_id])) {
                $items[] = $product_search[$product_id];
                //price_table[] = isset($product_staff_price_table[$product_id]) ? (int) $product_staff_price_table[$product_id]['total_price'] : 0;
                // new
                $price = '¥0';
                if (isset($price_search['options'][$option_key])) {
                    $price = BookingBusiness::displayMoney($price_search['options'][$option_key]);
                }
                $price_table[] = $price;
            }
        }

        foreach ($staffs as $staff) {
            $items[] = $staff;
            //$price_table[] = $staff['assign_product_id'] && isset($product_staff_price_table[$staff['assign_product_id']]) ? (int) $product_staff_price_table[$staff['assign_product_id']]['total_price'] : 0;
            // new
            $price = '¥0';
            if (isset($staff['assign_product_id']) && isset($price_search['products'][$staff['assign_product_id']])) {
                $price = BookingBusiness::displayMoney($price_search['products'][$staff['assign_product_id']]);
            }
            $price_table[] = $price;
        }

        $ret = new \stdClass();

        $ret->items = $items;
        $ret->price_table = $price_table;
        $ret->price_search = $price_search;
        $ret->total_price = $price_search['total_price'];

        return $ret;
    }

    public static function buildUsableSeatList($store, $seats, $capacity, $date = null) {
        $usable_seats = [];
        $seat_capacity_min_list = [];
        $seat_capacity_max_list = [];

        if ($date) {
            $date = Carbon::createFromTimestamp($date);
            $search_date_time = $date->format('Y-m-d H:i:00');
        }

        if ($date && $date->timestamp < time() + ((int) $store['reservations_possible_time'] * 60)) {
            $usable_seats = [];
            foreach ($seats as $seat) {
                $seat_capacity_min_list[] = $seat->capacity_min;
                $seat_capacity_max_list[] = $seat->capacity_max;
            }
        } elseif ($capacity) {
            $required_capacity = (int) $capacity;
            foreach ($seats as $seat) {
                if ($date) {
                    $shifts = $seat->getDetailedSchedule([
                        'from_date' => $date->format('Y-m-d'),
                        'to_date' => $date->format('Y-m-d'),
                        'from_time' => $date->format('H:i'),
                        'to_time' => $date->format('H:i'),
                    ]);
                } else {
                    $shifts = $seat->getDetailedSchedule();
                }

                if (!isset($search_date_time) || !key_exists($search_date_time, $shifts)) {
                    if ($seat->capacity_min <= $required_capacity && $seat->capacity_max >= $required_capacity) {
                        $seat_capacity_min_list[] = $seat->capacity_min;
                        $seat_capacity_max_list[] = $seat->capacity_max;
                        if ($seat->capacity_max) {
                            $usable_seats[$seat['id']] = $seat;
                        }
                    }
                } else {
                    $current_shift = $shifts[$search_date_time];
                    $seat->_capacity['min'] = $current_shift['capacity_min'];
                    $seat->_capacity['max'] = $current_shift['capacity_max'];
                    if ($seat->capacity_min <= $required_capacity && $seat->capacity_max >= $required_capacity) {
                        $seat_capacity_min_list[] = $seat->capacity_min;
                        $seat_capacity_max_list[] = $seat->capacity_max;
                        if ($seat->capacity_max) {
                            $usable_seats[$seat['id']] = $seat;
                        }
                    }
                }
            }
        } else {
            foreach ($seats as $seat) {
                $shifts = $seat->detailedSchedule;
                if (!isset($search_date_time) || !key_exists($search_date_time, $shifts)) {
                    $seat_capacity_min_list[] = $seat->capacity_min;
                    $seat_capacity_max_list[] = $seat->capacity_max;
                    if ($seat->capacity_max) {
                        $usable_seats[$seat['id']] = $seat;
                    }
                } else {
                    $current_shift = $shifts[$search_date_time];
                    $seat->_capacity['min'] = $current_shift['capacity_min'];
                    $seat->_capacity['max'] = $current_shift['capacity_max'];
                    $seat_capacity_min_list[] = $seat->capacity_min;
                    $seat_capacity_max_list[] = $seat->capacity_max;
                    if ($seat->capacity_max) {
                        $usable_seats[$seat['id']] = $seat;
                    }
                }
            }
        }

        if (!count($seat_capacity_max_list)) {
            $seat_capacity_max_list = [0];
        }

        if (!count($seat_capacity_min_list)) {
            $seat_capacity_min_list = [0];
        }

        return [
            'usable_seats' => $usable_seats,
            'capacity_max' => max($seat_capacity_max_list),
            'capacity_min' => min($seat_capacity_min_list)
        ];
    }

}
