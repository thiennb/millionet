<?php
namespace api\controllers;
use yii\rest\ActiveController;
/*
 * ApiController object class
 * Parent Api controller
 */
class ApiController extends ActiveController{
        
        /*
         * Result method 
         */
        protected  function result($code = 0, $message = '', $content = null){
                $content['error_code'] = $code;
                $content['message'] = $message;
                return $content;
        }
}