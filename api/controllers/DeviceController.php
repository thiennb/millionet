<?php
namespace api\controllers;
use yii\rest\ActiveController;
use common\models\DeviceUser;
use api\models\ApiSupport;

class DeviceController extends ApiController{
    public $modelClass = 'common\models\User';
        
    /*
     * Action device token
     * 
     * @return json
     */
    public function actionUsertoken(){
        $transaction = \Yii::$app->db->beginTransaction();
        try{
            // Get parameter
            $requestData = \Yii::$app->getRequest()->post();
            if(!$requestData){
                return parent::result(1, \Yii::t('api', 'Request method is not supported'));
            }

            $user = ApiSupport::isCustomerLogged();
            // Check valid parameters
            $message = "";
            if(!$user){
                $message =  \Yii::t('api', 'You must login before');
            }elseif(!isset($requestData['device_token'])){
                $message = \Yii::t('api', 'Parameter "{param}" is missing', ['param' => 'device_token']);
            }elseif(!isset($requestData['store_id'])){
                $message =  \Yii::t('api', 'Parameter "{param}" is missing', ['param' => 'store_id']);
            }elseif(!is_numeric($requestData['store_id'])){
                $message =  \Yii::t('api', 'Parameter "{param}" is invalid', ['param' => 'store_id']);
            }

            if($message){
                return parent::result(1, $message);
            }

            $device = new DeviceUser();

            $device->setDeviceToken($requestData['device_token'], $requestData['store_id'], $user->user_id);
            $transaction->commit();

            return parent::result(0, 'success');
        } catch (Exception $ex) {
            $transaction->rollBack();

            return parent::result(1, $ex->getMessage());
        }
    }

    /*
     * Action device token
     * 
     * @return json
     */
    public function actionDevicetoken(){
            $transaction = \Yii::$app->db->beginTransaction();
            try{
                $requestData = \Yii::$app->getRequest()->post();
                if(!$requestData){
                    return parent::result(1, \Yii::t('api', 'Request method is not supported'));
                }

                // Check valid parameters
                $message = "";
                if(!isset($requestData['device_token'])){
                    $message =  \Yii::t('api', 'Parameter "{param}" is missing', ['param' => 'device_token']);
                }elseif(!isset($requestData['store_id'])){
                    $message =  \Yii::t('api', 'Parameter "{param}" is missing', ['param' => 'store_id']);
                }elseif(!is_numeric($requestData['store_id'])){
                    $message =  \Yii::t('api', 'Parameter "{param}" is invalid', ['param' => 'store_id']);
                }

                if($message){
                    return parent::result(1, $message);
                }

                $device = new DeviceUser();

                $device->setDeviceToken($requestData['device_token'], $requestData['store_id'], null);
                $transaction->commit();

                return parent::result(0, 'success'); 
            } catch (Exception $ex) {
                $transaction->rollBack();
                return parent::result(1, $ex->getMessage());
            }
    }

    /*
     * Action enable notice
     * 
     * @return json
     */
    public function actionEnable_notice(){
            $transaction = \Yii::$app->db->beginTransaction();
            try{
                $requestData = \Yii::$app->getRequest()->post();
                if(!$requestData){
                    return parent::result(1, \Yii::t('api', 'Request method is not supported'));
                }

                $isCustomerLogged = ApiSupport::isCustomerLogged();

                if(!$isCustomerLogged){
                    return parent::result(1, \Yii::t('api','You must login before'));
                }

                // Check valid parameters
                $message = "";
                if(!isset($requestData['store_id'])){
                    $message =  \Yii::t('api', 'Parameter "{param}" is missing', ['param' => 'store_id']);
                }elseif(!is_numeric($requestData['store_id'])){
                    $message =  \Yii::t('api', 'Parameter "{param}" is invalid', ['param' => 'store_id']);
                }elseif(!isset($requestData['enable'])){
                    $message =  \Yii::t('api', 'Parameter "{param}" is missing', ['param' => 'enable']);
                }

                if($message){
                    return parent::result(1, $message);
                }

                $device = DeviceUser::setNoticeReceivedFlg($requestData['store_id'], $isCustomerLogged->user_id, $requestData['enable']);

                if(!$device){
                    return parent::result(1, \Yii::t('api', '{param} is not found', ['param' => 'device']));
                }

                $transaction->commit();
                return parent::result(0, 'success');

            } catch (Exception $ex) {
                $transaction->rollBack();
                return parent::result(1, $ex->getMessage());
            }
    }

    /*
     * Action enable notice
     * 
     * @return json
     */
    public function actionEnable_coupon(){
            $transaction = \Yii::$app->db->beginTransaction();
            try{
                $requestData = \Yii::$app->getRequest()->post();
                if(!$requestData){
                    return parent::result(1, \Yii::t('api', 'Request method is not supported'));
                }

                $isCustomerLogged = ApiSupport::isCustomerLogged();

                if(!$isCustomerLogged){
                return parent::result(1, \Yii::t('api','You must login before'));
                }

                $message = '';
                // Check valid parameters
                if(!isset($requestData['store_id'])){
                    $message =  \Yii::t('api', 'Parameter "{param}" is missing', ['param' => 'store_id']);
                }elseif(!is_numeric($requestData['store_id'])){
                    $message =  \Yii::t('api', 'Parameter "{param}" is invalid', ['param' => 'store_id']);
                }elseif(!isset($requestData['enable'])){
                    $message =  \Yii::t('api', 'Parameter "{param}" is missing', ['param' => 'enable']);
                }

                if($message){
                    return parent::result(1, $message);
                }

                $device = DeviceUser::setCouponReceivedFlg($requestData['store_id'], $isCustomerLogged->user_id, $requestData['enable']);

                if(!$device){
                    return parent::result(1, \Yii::t('api', '{param} is not found', ['param' => 'device']));
                }

                $transaction->commit();
                return parent::result(0, 'success');

            } catch (Exception $ex) {
                $transaction->rollBack();
                return parent::result(1, $ex->getMessage());
            }
    }

    /*
     * GEt update notification method
     */
    public function actionSetting_state(){
        try{
            // Get parameters
            $store_id = \Yii::$app->getRequest()->getQueryParam('store_id');
            $isCustomerLogged = ApiSupport::isCustomerLogged();

            if(!$isCustomerLogged){
                return parent::result(1, \Yii::t('api','You must login before'));
            }

            if(!$store_id){
                return parent::result(1, \Yii::t('api', 'Parameter "{param}" is missing', ['param' => 'store_id']));
            }


            // Select when use logged
            $settingState = DeviceUser::find()
                            ->select(['user_id', 'store_id', 'notice_received_flg', 'coupon_received_flg'])
                            ->where(['store_id' => $store_id])
                            ->andWhere(['user_id' => $isCustomerLogged->user_id])
                            ->one();

            $result = ['setting_state' => $settingState];

            return parent::result(0, '', $result);
        } catch (Exception $ex) {
            return parent::result(1, $ex->getMessage());
        }
    }
}