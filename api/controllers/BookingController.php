<?php
namespace api\controllers;
use common\models\Booking;
use common\models\PointSetting;
use common\models\MasterRank;
use common\models\CustomerStore;
use common\components\Constants;
use api\models\ApiSupport;
use Carbon\Carbon;

class BookingController extends ApiController{
        public $modelClass = 'common\models\Booking';
        
        /*
         * Get ticket list
         * 
         * @return json
         */
        public function actionCancel(){
            $transaction = \Yii::$app->db->beginTransaction();

            try{   
                //Get parameter
                $isLogged = ApiSupport::isCustomerLogged();

                // Check user_id is invalid or null
                if(!$isLogged){
                    return parent::result(1, \Yii::t('api','You must login before'));
                }
                
                if(!\Yii::$app->getRequest()->post()){
                    return parent::result(1, \Yii::t('api', 'Request method is not supported'));
                }

                $booking_id = \Yii::$app->getRequest()->post('booking_id');
                // Check is valid parameter
                if(!$booking_id){
                    return parent::result(1, \Yii::t('api', 'Parameter "{param}" is missing', ['param' => 'booking_id']));
                }

                if(!is_numeric($booking_id)){
                    return parent::result(1, \Yii::t('api', 'Parameter "{param}" is invalid', ['param' => 'booking_id']));
                }

                $booking = Booking::find()
                        ->where(['booking.id' => $booking_id])
                        ->andWhere(['booking.customer_id' => $isLogged->id])
                        ->one();

                if(!$booking){
                    return parent::result(1,  \Yii::t('api', '{param} is not found', ['param' => 'Booking']));
                }
                
                if($booking->cancelOffset->timestamp < time() || $booking->booking) {
                    return parent::result(1, 'キャンセル時間が終わりました。この予約がキャンセルできません');
                }

                // Set status cancel
                Booking::updateAll(['status'=> Booking::BOOKING_STATUS_CANCEL_BY_CUSTOMER, 'updated_at' => strtotime(date('Y-m-d H:i:s')), 'updated_by' => $isLogged->id],['and',['id' => $booking_id], ['customer_id' => $isLogged->id]]);

                $transaction->commit();

                return parent::result(0, 'success');
            } catch (Exception $ex) {
                $transaction->rollBack();
                return parent::result(1, $ex->getMessage());
            }
        }
        
        /*
         * History booking method
         * 
         * @return json
         */
        public function actionHistory(){
            try{
                //Get parameter
                $isCustomerLogged = ApiSupport::isCustomerLogged();

                // Check user_id is invalid or null
                if(!$isCustomerLogged){
                    return parent::result(1, \Yii::t('api','You must login before'));
                }
                
                $store_id = \Yii::$app->getRequest()->get('store_id');
                $page = \Yii::$app->getRequest()->get('page');
                $limit = \Yii::$app->getRequest()->get('limit');

                // Check valid parameters
                if(!$store_id){
                    return parent::result(1, \Yii::t('api', 'Parameter "{param}" is missing', ['param' => 'store_id']));
                }

                if(!is_numeric($store_id)){
                    return parent::result(1, \Yii::t('api', 'Parameter "{param}" is invalid', ['param' => 'store_id']));
                }
                
                if(!$page){
                    return parent::result(1, \Yii::t('api', 'Parameter "{param}" is missing', ['param' => 'page']));
                }
                
                if(!is_numeric($page)){
                    return parent::result(1, \Yii::t('api', 'Parameter "{param}" is invalid', ['param' => 'page']));
                }
                
                if(!$limit){
                    return parent::result(1, \Yii::t('api', 'Parameter "{param}" is missing', ['param' => 'limit']));
                }
                
                if(!is_numeric($limit)){
                    return parent::result(1, \Yii::t('api', 'Parameter "{param}" is invalid', ['param' => 'limit']));
                }
                
                 //get point setting
                $point_setting = PointSetting::find()
                        ->orderBy('updated_at DESC')
                        ->one();
                
                //get rank user
                $rank_id = "00";
                $customer_store = CustomerStore::find()
                        ->where(['customer_id' => $isCustomerLogged->id])
                        ->andWhere(['store_id' => $store_id])
                        ->one();
                if($customer_store){
                    $rank_id = $customer_store['rank_id'];
                }
                
                $master_rank = new \yii\db\Query();
                if($rank_id == "01"){
                    $master_rank->select(['bronze_point_rate as rate']);
                } else if($rank_id == "02"){
                    $master_rank->select(['sliver_point_rate as rate']);
                } else if($rank_id == "03"){
                    $master_rank->select(['gold_point_rate as rate']);
                } else if($rank_id == "04"){
                    $master_rank->select(['black_point_rate as rate']);
                } else{
                    $master_rank->select(['normal_point_rate as rate']);
                }
                
                $master_rank->from('rank');
                $rank_rate = $master_rank->one();
                
                $__statusList = Constants::LIST_BOOKING_STATUS;
                $page_num = ($page - 1) * $limit;
                
                //get list booking done
                $bookingList = Booking::find()->select(['booking.id', 'booking.booking_date', 'booking.start_time', 'booking.end_time',
                                    'booking.booking_price', 'booking.booking_discount', 'booking.booking_total', 'booking.staff_id',
                                    'booking.store_id', 'booking.booking_total as point_save', 'booking.status',
                                    new \yii\db\Expression(
                                        'case when booking.status = :three or booking.status = :four or booking.status = :five then 1'
                                            . ' else 0 '
                                            . ' end booking_type'
                                    )])
                            ->with(['bookingProduct', 'bookingProduct.product', 'listSeat', 'listSeat.seat', 'listSeat.seat.seatType', 'staff', 'storeMaster', 'storeCancelTime', 'bookingCoupon', 'bookingCoupon.coupon'])
                            ->where(['booking.customer_id' => $isCustomerLogged->id])
                            ->andWhere(['booking.store_id' => $store_id])
                            ->andWhere(['booking.status' => ['03', '04', '05', '02', '01']])
                            ->limit($limit)
                            ->offset($page_num)
                            ->orderBy('booking_type ASC, booking.booking_date DESC, booking.start_time DESC, booking.id DESC')
                            ->addParams([
                                'three' => '03',
                                'four'  => '04',
                                'five'  => '05'
                            ])
                            ->all();
                   
                $__resultBookingDone = [];
                $__resultBookingRequire = [];
                if($bookingList){
                    foreach ($bookingList as $key => $value){
                        $info = [];
                        $products = [];
                        
                        if($value->bookingCoupon){
                            foreach ($value->bookingCoupon as $coupon) {
                                if($coupon->coupon){
                                    $products[] = ['name' => $coupon->coupon->title];
                                }
                            }
                        }
                        
                        if($value->bookingProduct){
                            foreach ($value->bookingProduct as $product) {
                                if($product->product){
                                    $products[] = ['name' => $product->product->name];
                                }
                            }
                        }
                           
                        if($point_setting['grant_yen_per_point'] != 0){
                            $point_save = (int)($value->booking_total/$point_setting['grant_yen_per_point']*$rank_rate['rate']);
                        } else{
                            $point_save = 0;
                        }
                        
                        //get info type service
                        if($value->storeMaster == null || $value->storeMaster->booking_resources == '00'){
                            $info[] = ['type' => $value->storeMaster ? $value->storeMaster->booking_resources:null, 'value' => null];
                        } else if($value->storeMaster->booking_resources == '01'){
                            //all seat same seat type
                            $info[] = ['type' => $value->storeMaster ? $value->storeMaster->booking_resources:null, 'value' => $value->listSeat ? ($value->listSeat[0]->seat->seatType ? $value->listSeat[0]->seat->seatType->name:null):null];
                        } else if($value->storeMaster->booking_resources == '02'){
                            $info[] = ['type' => $value->storeMaster ? $value->storeMaster->booking_resources:null,'value' => $value->staff ? $value->staff->name:null];
                        } else{
                            $info[] = ['type' => $value->storeMaster ? $value->storeMaster->booking_resources:null, 'value' => null];
                        }

                        if($value->booking_type == 1){
                            $__resultBookingDone[] = [
                                'id' => $value->id,
                                'booking_date' => $value->booking_date,
                                'start_time' => $value->start_time,
                                'end_time' => $value->end_time,
                                'info' => $info,
                                'booking_price' => $value->booking_price,
                                'booking_discount' => $value->booking_discount,
                                'booking_total' => $value->booking_total,
                                'status' => $__statusList[$value->status],
                                'products' => $products,
                                'point_save' => $point_save,
                                'booking_type' => $value->booking_type
                            ];
                        } else{
                            $possibleTimeCancel = $value->storeCancelTime->cancel_possible_time ? $value->storeCancelTime->cancel_possible_time : 0;
                        
                            $dateBooking = Carbon::parse($value->booking_date.' '.$value->start_time);
                            $timeCancel = $dateBooking->subMinute($possibleTimeCancel)->toDateTimeString();

                            $__resultBookingRequire[] = [
                                'id' => $value->id,
                                'booking_date' => $value->booking_date,
                                'start_time' => $value->start_time,
                                'end_time' => $value->end_time,
                                'info' => $info,
                                'booking_price' => $value->booking_price,
                                'booking_discount' => $value->booking_discount,
                                'booking_total' => $value->booking_total,
                                'status' => $__statusList[$value->status],
                                'products' => $products,
                                'point_save' => $point_save,
                                'possibleTimeCancel' => $timeCancel,
                                'booking_type' => $value->booking_type
                            ];
                        }
                        
                    }
                } else{
                    $__resultBookingDone = null;
                    $__resultBookingRequire = null;
                }

                return parent::result(0, '', ['reservation_require' => $__resultBookingRequire, 'reservation_complete' => $__resultBookingDone]);
            } catch (Exception $ex) {
                return parent::result(1, $ex->getMessage());
            }
        }
}
