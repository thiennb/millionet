var calendarM = {
    cur_date : "",
    select_date: "",
    init: function(element, selectedDay) {
        calendarM.loadCalendar(element, selectedDay);
    },
    /**
     * function to load calendar
     */
    loadCalendar: function(element, selectedDay) {
        // get full cerrent day
        //var curSunday = calendarM.getDateGeneral(-1, 5, selectedDay);
        // get month and year current
        var cur_date = calendarM.getDateGeneral(0, 5, selectedDay);
        var curMAY = calendarM.getCurMonthYear(cur_date);

        var next = parseInt(week) + 1;
        var prev = parseInt(week) - 1;
        if ( prev < 0 ) {
            prev = 0;
        }

		var productsAndCoupons = '&coupons='+coupons+'&products='+products+'&options='+options;
        //genderal month and year calender
        var htmlHead = '';
            if ( week > 0 ) {
                //htmlHead += '<th rowspan="2" class="timeColor" id="backCal"><a href="'+ SITE_ROOT +'/booking-order/salonschedule?storeId='+store_id+ productsAndCoupons + '&executeTime='+time_execute+'&week='+prev+'&staff='+staff+'" title="次の一週間" class="iS arrowPagingWeekL">前の一週間</a></th>';
                htmlHead += '<th rowspan="2"></th>';
				$('.btn_prev_week').attr('href', SITE_ROOT +'bookingorder/salon?storeId='+store_id+ productsAndCoupons + '&executeTime='+time_execute+'&week='+prev+'&staff='+staff+'&noOption='+calendarM.getParam('noOption')+'&noStaff='+calendarM.getParam('noStaff'));
                $('.btn_prev_week_seat').attr('href', SITE_ROOT +'bookingorder/restaurant?storeId='+store_id+ productsAndCoupons + '&executeTime='+time_execute+'&week='+prev+'&noOption='+calendarM.getParam('noOption')+'&seat='+calendarM.getParam('seat')+'&capacity='+calendarM.getParam('capacity'));
            } else {
                //htmlHead += '<th rowspan="2" class="timeColor" id="backCal"><span class="iS arrowPagingWeekLOff">前の一週間</span></th>';
                htmlHead += '<th rowspan="2"></th>';
				$('.btn_prev_week').replaceWith($('<button/>', {class : 'btnCouponTypeFilter isCr btn_prev_week'}).text('< 前の一週間').addClass('disabled').prop('disabled', true).css('background', '#ddd'));
                $('.btn_prev_week_seat').replaceWith($('<button/>', {class : 'btnCouponTypeFilter isCr btn_prev_week'}).text('< 前の一週間').addClass('disabled').prop('disabled', true).css('background', '#ddd'));
            }
            htmlHead += curMAY;
            //htmlHead += '<th rowspan="2" class="timeColor" id="nextCal"><a href="'+ SITE_ROOT +'/booking-order/salonschedule?storeId='+store_id+ productsAndCoupons + '&executeTime='+time_execute+'&week='+next+'&staff='+staff+'" title="次の一週間" class="iS arrowPagingWeekR">次の一週間</a></th>';
            $('.btn_next_week').attr('href', SITE_ROOT +'bookingorder/salon?storeId='+store_id+ productsAndCoupons + '&executeTime='+time_execute+'&week='+next+'&staff='+staff+'&noOption='+calendarM.getParam('noOption')+'&noStaff='+calendarM.getParam('noStaff'));
            $('.btn_next_week_seat').attr('href', SITE_ROOT +'bookingorder/restaurant?storeId='+store_id+ productsAndCoupons + '&executeTime='+time_execute+'&week='+next+'&noOption='+calendarM.getParam('noOption')+'&seat='+calendarM.getParam('seat')+'&capacity='+calendarM.getParam('capacity'));
        $("#" + element).find("#headCal").html(htmlHead);
        // general day row calendar
        var html = '';
        for (var j = 0; j < numberOfDay; j++) {
            var classRow = 'dayCell';
            var DoW = calendarM.getDateGeneral(j, 0, selectedDay);
            if (DoW == '日') {
                classRow = 'sun';
            } else if (DoW == '土') {
                classRow = 'sat';
            }
            if (calendarM.curDate() == calendarM.getDateGeneral(j, 5, selectedDay)) {
                html += "<th data-value='" + calendarM.getDateGeneral(j, 5, selectedDay) + "' class='" + classRow + " df_dateCur'> " + calendarM.getDateGeneral(j, 1, selectedDay) + "<br>" + DoW + "</th>";
            } else {
                html += "<th data-value='" + calendarM.getDateGeneral(j, 5, selectedDay) + "' class='" + classRow + "'> " + calendarM.getDateGeneral(j, 1, selectedDay) + "<br>" + DoW + "</th>";
            }
        }
        // append to table
        $('#' + element).find('tr#dayRow').html(html);
    },
    /**
     * function get current month and year
     */
    getCurMonthYear: function(selectedDay) {

        // get info dat, month and year in the first day of the selected week
        var firstDay = new Date(selectedDay);
        var firstMonth = firstDay.getMonth() + 1;
        var firstYear = firstDay.getFullYear();
        var numDayOfMonthnew = new Date(firstMonth, firstYear, 0).getDate();
        var numberFirstDay = numDayOfMonthnew - firstDay.getDate() + 1;
        if ( numberFirstDay == 0 ) {
            numberFirstDay = 1;
        }

        // get info dat, month and year in the end day of the selected week
        var endDay = new Date(firstDay.setDate(firstDay.getDate() + parseInt(numberOfDay - 1)));
        var endMonth = endDay.getMonth() + 1;
        var endYear = endDay.getFullYear();
        var numDayOfMonthnew = new Date(endMonth, endYear, 0).getDate();
        var numberEndDay = numberOfDay - numberFirstDay;

        // check to return month and year
        if (firstMonth == endMonth || numberFirstDay > numberOfDay) {
            return '<th colspan="' + numberOfDay + '" data-id="'+firstYear+firstMonth+'" class="monthColor pV10">'+firstYear + '年' + firstMonth + '月'+'</th>';
        } else {
            if (numberFirstDay >= numberOfDay) {
                $ret = '<th colspan="' + numberFirstDay + '" data-id="' + firstYear + firstMonth + '" class="monthColor pV10">' + firstYear + '年' + firstMonth + '月' + '</th>';
                if (numberEndDay > 0) {
                    $ret += '<th colspan="' + numberEndDay + '" data-id="' + firstYear + firstMonth + '"class="monthColor pV10">' + endYear + '年' + endMonth + '月' + '</th>';
                }

                return $ret;
            } else {
                return '<th colspan="' + numberFirstDay + '" data-id="' + firstYear + firstMonth + '" class="monthColor pV10"></th><th colspan="' + numberEndDay + '" data-id="' + firstYear + firstMonth + '"class="monthColor pV10">' + endYear + '年' + endMonth + '月' + '</th>';
            }
        }
    },
    /**
     * function to get date
     */
    getDateGeneral: function(day, type, selectedDay) {
        var monday = new Date(selectedDay);
        var objToday = new Date(monday.getTime() + day * 24 * 60 * 60 * 1000),
                weekday = new Array('日', '月', '火', '水', '木', '金', '土'),
                dayOfWeek = weekday[objToday.getDay()],
                dayOfMonth = objToday.getDate(),
                curMonth = objToday.getMonth() + 1,
                curYear = objToday.getFullYear();
        switch (type) {
            case 0: // return day of week
                return dayOfWeek;
                break;
            case 1: // return day of month
                return dayOfMonth;
                break;
            case 2: // return current month
                return curMonth;
                break;
            case 3: // return current year
                return curYear;
                break;
            default: // return full format date
                return curMonth + '/' + dayOfMonth + '/' + curYear;
                break;
        }
    },
    /**
     * function to get Monday
     */
    curDate: function() {
        var monday = new Date();
        var objToday = new Date(monday.getTime());
        var daycurOfMonth = objToday.getDate();
        var curMonth = objToday.getMonth() + 1;
        var curYear = objToday.getFullYear();
        return 	curMonth + "/" + daycurOfMonth + "/" + curYear;
    },
    getParam: function(sParam) {
        var sPageURL = window.location.search.substring(1);
        var sURLVariables = sPageURL.split('&');
        for (var i = 0; i < sURLVariables.length; i++)
        {
            var sParameterName = sURLVariables[i].split('=');
            if (sParameterName[0] === sParam)
            {
                return sParameterName[1];
            }
        }

        return '';
    }
};

$(document).ready(function() {
    calendarM.init('calendarM', selectedDay);
});
