// @author HR
// @version 1.0.1
(function ($) {
    'use strict';
    $(document).ready(function () {
        function inherit(constructor, baseClass) {
            var childClass = constructor;
            childClass.prototype = new baseClass();
            childClass.prototype.constructor = constructor;
            return childClass;
        }

        // -------------------
        // BaseList
        var BaseList = function () {
            this.content = [];
        };
        BaseList.prototype = {
            // @return array List of content
            all: function () {
                var ret = [];
                for (var i = 0; i < this.content.length; i++) {
                    if (this.content[i] !== false)
                        ret.push(this.content[i]);
                }

                return ret;
            },
            // @return string List of content element separated by commas
            list: function () {
                return this.all().join(',');
            },
            // @return boolean Check if this list content an id
            has: function (id) {
                return this.content.indexOf(id) !== -1;
            },
            add: function (id) {
                if (!this.has(id)) {
                    this.content.push(id);
                }
                //console.log(this.list());
            },
            remove: function (id) {
                for (var i = 0; i < this.content.length; i++) {
                    if (this.content[i] == id)
                        this.content[i] = false;
                }
            },
            removeOne: function (id) {
                for (var i = 0; i < this.content.length; i++) {
                    if (this.content[i] == id)
                        this.content[i] = false;
                    return true;
                }

                return false;
            }
        };

        // COUPONS
        var coupons = (function (c) {
            var List = inherit(function (coupons) {
                this.combinable = null;
                this.setCoupons = new BaseList();

                if (typeof coupons !== 'string' || !coupons.match(/^\d+(,\d+){0,}$/)) {
                    this.content = [];
                } else {
                    this.content = coupons.split(',');
                }

                var currentList = this;

                $(document).on('click', '.select-booking-coupon', function () {
                    $(this).toggleClass('selected').toggleText();

                    if ($(this).hasClass('selected')) {
                        currentList.add(this.value);
                        currentList.combine(this);
                        if ($(this).hasClass('coupon-benefit-02') || $(this).hasClass('coupon-benefit-03')) {
                            currentList.setCoupons.add(this.value);
                        }
                    } else {
                        currentList.remove(this.value);
                        currentList.combine();
                        currentList.setCoupons.remove(this.value);
                    }
                });
            }, BaseList);

            List.prototype.combine = function (button) {
                if (this.all().length == 0) {
                    $('.select-booking-coupon').prop('disabled', false);
                    return true; // exit
                }

                if (button) {
                    if ($(button).hasClass('coupon-non-combinable')) {
                        this.combinable = false;
                    } else {
                        this.combinable = true;
                    }
                }

                if (this.combinable) {
                    $('.coupon-combinable').prop('disabled', false);
                    $('.coupon-non-combinable').prop('disabled', true);
                } else {
                    $('.select-booking-coupon').prop('disabled', true);
                    var all = this.all();
                    $('.coupon-' + all[all.length - 1]).prop('disabled', false);
                }
            };

            List.prototype.init = function () {
                var currentList = this;
                $('.select-booking-coupon').each(function () {
                    if (currentList.content.indexOf(this.value) !== -1) {
                        if (!$(this).hasClass('selected')) {
                            $(this).trigger('click');
                        }
                    }
                });
            };

            return new List(c);
        })(getParam('coupons'));

        // PRODUCTS
        var products = (function (c) {
            var List = inherit(function (input) {
                if (typeof input !== 'string' || !input.match(/^\d+(,\d+){0,}$/)) {
                    this.content = [];
                } else {
                    this.content = input.split(',');
                }

                var currentList = this;

                $(document).on('click', '.select-booking-product', function () {
                    $(this).toggleClass('selected').toggleText();

                    if ($(this).hasClass('selected')) {
                        currentList.add(this.value);
                    } else {
                        currentList.remove(this.value);
                    }
                });
            }, BaseList);

            List.prototype.init = function () {
                var currentList = this;
                $('.select-booking-product').each(function () {
                    if (currentList.content.indexOf(this.value) !== -1) {
                        //currentList.add(this.value);
                        if (!$(this).hasClass('selected')) {
                            $(this).trigger('click');
                        }
                    }
                });
            };

            return new List(c);
        })(getParam('products'));

        // CATEGORIES
        var categories = (function (c) {
            var List = inherit(function (input) {
                if (typeof input !== 'string' || !input.match(/^\d+(,\d+){0,}$/)) {
                    this.content = [];
                } else {
                    this.content = input.split(',');
                }

                var currentList = this;

                $(document).on('click', '.filter-category', function () {
                    if ($(this).is(':checked')) {
                        currentList.add(this.value);
                    } else {
                        currentList.remove(this.value);
                    }
                });
            }, BaseList);

            return new List(c);
        })();

        // MODES
        var modes = (function (c) {
            var List = inherit(function (input) {
                if (typeof input !== 'string' || !input.match(/^\d+(,\d+){0,}$/)) {
                    this.content = [];
                } else {
                    this.content = input.split(',');
                }

                var currentList = this;

                $(document).on('click', '.filter-mode', function () {
                    if ($(this).is(':checked')) {
                        currentList.add(this.value);
                    } else {
                        currentList.remove(this.value);
                    }
                });
            }, BaseList);

            return new List(c);
        })();

        // OPTIONS
        var options = (function (c) {
            function updateOptionList(currentList) {
                var optionList = [];
                var optionListHTML = [];
                var added_time = 0, added_price = 0;

                $('.select-option').each(function () {
                    if (this.value != -1) {
                        var product_id = this.value.split('-')[1];

                        if ($(this).is(':checked')) {
                            var option = $(this).parent(),
                                    name = option.children('.option-name').val(),
                                    time = parseInt(option.children('.option-time').val()),
                                    price = parseInt(option.children('.option-price').val());

                            optionList.push(this.value);
                            optionListHTML.push('<tr><td>' + name + '</td><td>' + formatMoney(price) + '</td><td>' + time + '分</td></tr>');

                            //products.add(product_id);

                            added_time += time;
                            added_price += price;
                        } else {
                            //products.remove(product_id);
                        }
                    }
                });

                $('#selected-option-list').html(optionListHTML);
                $('#total-time').html(formatTime(currentList.baseTime + added_time));
                $('#total-price').html(formatMoney(currentList.basePrice + added_price));

                return optionList;
            }

            var List = inherit(function (input) {
                if (typeof input !== 'string' || !input.match(/^\d+\-\d+\-\d+(,\d+\-\d+\-\d+){0,}$/)) {
                    this.content = [];
                } else {
                    this.content = input.split(',');
                }

                this.baseTime = 0;
                this.basePrice = 0;

                var currentList = this;

                $(document).on('click', '.select-option', function () {
                    currentList.content = updateOptionList(currentList);
                });
            }, BaseList);

            List.prototype.getTotalTime = function () {
                var added_time = 0, added_price = 0;

                $('.select-option').each(function () {
                    if (this.value != -1 && $(this).is(':checked')) {
                        var option = $(this).parent();
                        var time = parseInt(option.children('.option-time').val()),
                                price = parseInt(option.children('.option-price').val());

                        added_time += time;
                        added_price += price;
                    }
                });

                return this.baseTime + added_time;
            };

            List.prototype.init = function () {
                var currentList = this;

                $('.select-option').each(function () {
                    if (currentList.has(this.value)) {
                        this.checked = true;
                    }
                });

                updateOptionList(this);
            };

            return new List(c);
        })(getParam('options'));

        function loadPartial(selector, url, post, callback) {
            //var loaddingMessage = $('<div/>', {class: 'loading-message'}).text('ローディング ..');
            var container = $(selector);
            if (container.length) {
                //container.append(loaddingMessage);

                $.ajax({
                    url: url,
                    type: 'post',
                    data: post,
                    success: function (data) {
                        callback(container, data);
                        //loaddingMessage.remove();
                    }
                });
            }
        }

        loadPartial('#list-filters', 'loadcategories', {
            storeId: getParam('storeId')
        }, function (container, data) {
            container.replaceWith(filterData(data));
            $('.booking-container').css({height : $(window).height() - $('.top-floated').outerHeight(true) - $('.footer-attachment').outerHeight(true)});
            $('#btn-filter').click(function () {
                loadingPage = 0;
                loadCouponProduct();
                $('#btn-filter-collapse').trigger('click');
            });
        });

        loadPartial('#load-confirm-menu', 'loadorderdetail', {
            storeId: getParam('storeId'),
            coupons: coupons.list(),
            products: products.list(),
            options: options.list()
        }, function (container, data) {
            container.replaceWith(filterData(data));
            options.baseTime = parseInt($('#base-time').val());
            options.basePrice = parseInt($('#base-price').val());

            options.init();
        });

        loadPartial('#list-staff', 'loadstaff', {
            storeId: getParam('storeId'),
            executeTime: getParam('executeTime'),
            products: getParam('products'),
            coupons: getParam('coupons'),
            options: getParam('options'),
            noOption: getParam('noOption')
        }, function (container, data) {
            container.replaceWith(filterData(data));
        });

        loadPartial('#list-seat', 'loadseat', {
            storeId: getParam('storeId'),
            executeTime: getParam('executeTime'),
            products: getParam('products'),
            coupons: getParam('coupons'),
            options: getParam('options'),
            noOption: getParam('noOption')
        }, function (container, data) {
            container.replaceWith(filterData(data));
        });

        loadPartial('#list-money-and-point', 'listmoneyandpoint', {
            storeId: getParam('storeId'),
            executeTime: getParam('executeTime'),
            products: getParam('products'),
            coupons: getParam('coupons'),
            options: getParam('options'),
            date: getParam('date'),
            staff: getParam('staff'),
            noOption: getParam('noOption')
        }, function (container, data) {
            container.replaceWith(filterData(data));
        });

        loadPartial('#load-order-detail', 'loadconfirmorderdetail', {
            storeId: getParam('storeId'),
            executeTime: getParam('executeTime'),
            products: getParam('products'),
            coupons: getParam('coupons'),
            options: getParam('options'),
            date: getParam('date'),
            staff: getParam('staff'),
            noOption: getParam('noOption')
        }, function (container, data) {
            container.html(filterData(data));
        });

        function bindLoadingWhenScrollingTo(scrollContainer, indexOfItem) {
            var scrollContainerTop = $(scrollContainer).offset().top;
            var offsetItem = $('.select-item')[indexOfItem];

            if (offsetItem) {
                var offsetItemHeight = $(offsetItem).height();
                //var offsetScrollTop = offsetItemTop - scrollContainerTop;
                var scrollEvent = function () {
                    if($(this).scrollTop() + $(this).innerHeight() >= $(this)[0].scrollHeight - 3 * offsetItemHeight) {
                    //if ($(this).scrollTop() - scrollContainerTop >= offsetScrollTop) {
                        //console.log($(this).scrollTop() + $(this).innerHeight());
                        //console.log(indexOfItem);
                        //console.log($(this)[0].scrollHeight - 3 * offsetItemHeight);
                        loadingPage++;
                        loadCouponProduct();
                        $(this).off('scroll', scrollEvent);
                    }
                };

                $(scrollContainer).on('scroll', scrollEvent);
            }
        }

        var loadingPage = 0;
        function loadCouponProduct() {
            if (loadingPage === 0) {
                $('#select-items').html('');
            }
            $('#booking-error').hide();
            var loaddingMessage = $('<div/>', {class: 'loading-message'}).text('ローディング ..');
            $('#select-items').append(loaddingMessage);
            loadPartial('#select-items', 'loadcouponsandproducts', {
                storeId: getParam('storeId'),
                categories: categories.list(),
                modes: modes.list(),
                page : loadingPage
            }, function (container, data) {
                loaddingMessage.remove();
                data = filterData(data);
                if (!data) {
                    if (!$('.select-item').length) {
                        showError('何も見つかりませんでした。条件を変えて検索してください。');
                    }
                }
                else {
                    container.append(data);
                    bindLoadingWhenScrollingTo('.booking-container', $('.select-item').length - 3);
                    // var selectItems = $('.select-item');
                    // $(selectItems[selectItems.length - 3]).css({
                    //     background : '#333'
                    // });
                    coupons.init();
                    products.init();
                }
            });
        }

        loadCouponProduct();

        // 2 buttons
        $('.btn-top').click(function () {
            $('.booking-container').scrollTop(0);//window.scrollTo(0, 0);
        });

        $('.btn-next').click(function () {
            switch (this.value) {
                case '1':
                    // nothing chosen
                    if (!products.all().length) {
                        if(!coupons.all().length) {
                            showError('クーポンまたは商品を選択してください。');

                            return false;
                        }

                        if(!coupons.setCoupons.all().length) {
                            showError('セットのクーポンまたは商品を選択してお願います。');

                            return false;
                        }
                    }

                    window.location.href = 'options?storeId=' + getParam('storeId') + '&products=' + products.list() + '&coupons=' + coupons.list();
                    break;
                default:
                    window.location.href = this.value + '?storeId=' + getParam('storeId') + '&products=' + products.list() + '&coupons=' + coupons.list() + '&options=' + options.list() + '&executeTime=' + options.getTotalTime();
                    break;

            }
        });

        //=============================
        // POINT INPUT CHANGE EVENT
        //============================
        var point = (function () {
            var PointInputProfessor = function () {
                this.ratio = 0; // conversionYen / conversionP
                this.totalMoney = 0;
                this.totalPoint = 0;
            };

            PointInputProfessor.prototype.pointToMoney = function (point) {
                return point * this.ratio;
            };

            PointInputProfessor.prototype.moneyToPoint = function (money) {
                if (this.ratio == 0) {
                    return 0;
                }

                return money / this.ratio;
            };

            PointInputProfessor.prototype.suggestPoint = function () {
                return Math.floor(this.moneyToPoint(this.totalMoney));
            };

            function inputFailEvent() {
                $(this).select;
                return false;
            }

            function inputChange(input, professor) {
                var val = $(input).val();

                if (val.match(/^[\-]{0,1}\d+$/) || val === '') {
                    var usingPoints = parseInt($(input).val());
                    if (!usingPoints)
                        usingPoints = 0;

                    if (usingPoints < 0) {
                        showInputError(input, 'ご利用ポイント は0よりてはいけません。');
                        return inputFailEvent.bind(input);
                    }

                    if (usingPoints > professor.totalPoint) {
                        showInputError(input, '利用ポイント は現在ポイントより大きくてはいけません。');
                        return inputFailEvent.bind(input);
                    }

                    var finalMoney = professor.totalMoney - professor.pointToMoney(usingPoints);

                    if (finalMoney < 0) {
                        showInputError(input, usingPoints + "ポイントを利用して購入したら、支払い予定金額が0円より小さくなっていますので再度入力してください。<br>（支払い予定金額が0円なら、" + professor.suggestPoint() + "ポイントをご利用してください。）");
                        return inputFailEvent.bind(input);
                    } else {
                        $('#booking-need-to-pay').html(formatMoney(finalMoney));
                        showInputError(input, ''); //hide error
                        return true;
                    }
                } else {
                    showInputError(input, 'ポイントの書式が正しくありません。');
                    return inputFailEvent.bind(input);
                }
            }

            PointInputProfessor.prototype.bindEventOn = function (selector, callback) {
                var professor = this;
                if (!callback) {
                    callback = function () {};
                }
                $(document).on('keypress', selector, function (e) {
                    if (e.which == 13) // Enter key = keycode 13
                    {
                        e.preventDefault();
                    }
                    //callback.bind(this, inputChange(this, professor));
                });

                $(document).on('keyup', selector, function (e) {
                    // callback input, validate = true/false
                    callback.bind(this, inputChange(this, professor));
                });
            };

            return new PointInputProfessor();
        })();

        point.ratio = parseFloat($('#point-yen').val()) / parseFloat($('#point-p').val());
        point.totalMoney = parseInt($('#total-money').val());
        point.totalPoint = parseInt($('#total-point').val());

        point.bindEventOn('#apiformbookingdetail-point_use');

        //============================
        // jQuery Plugins
        //============================
        $.fn.toggleText = function () {
            // new toggle text
            var newText = $(this).data('toggleText') || $(this).text();

            // backup current text
            $(this).data('toggleText', $(this).text());

            // apply toggleText
            $(this).text(newText);
        };

        //=============================
        // HELPERS
        //============================
        // @ref
        function getParam(sParam) {
            var sPageURL = window.location.search.substring(1);
            var sURLVariables = sPageURL.split('&');
            for (var i = 0; i < sURLVariables.length; i++)
            {
                var sParameterName = sURLVariables[i].split('=');
                if (sParameterName[0] === sParam)
                {
                    return sParameterName[1];
                }
            }

            return '';
        }

        function formatMoney(money) {
            return '¥' + money.toFixed(1).replace(/(\d)(?=(\d{3})+\.)/g, '$1,').split('.')[0];
        }

        function formatTime(min) {
            var h = Math.floor(min / 60);
            var m = min % 60;
            var ret = '';

            if (h > 0) {
                ret += h + '時間';
            } else if (m == 0) {
                ret = 0 + '分';
            }

            if (m > 0) {
                ret += m + '分';
            }

            return ret;
        }

        function filterData(data) {
            return data.replace(/<script.+script>/, '');
        }

        function pointToMoney(point) {
            return parseInt(point) * conversionYen / conversionP;
        }

        function showInputError(input, msg) {
            $(input).parent().find('.help-block-error').html(msg);
        }

        function showError(message) {
            $('#booking-error').children('label').text(message);
            $('#booking-error').show().css('display', 'block !important');
            $('.booking-container').scrollTop(0);
        }

        $(document).on('click', '.btn-close-alert', function () {
            $(this).parent().hide().css('display', 'none !important');
        });

        $('#btn-final').click(function () {
            window.location.href = 'final';
        });

        $(document).click(function (event) {
            //console.log($(event.target).closest('.filter-list').length)
            if (!$(event.target).closest('.filter-list').length) {
                if ($('#collapseOne').is(":visible")) {
                    $('#btn-filter-collapse').trigger('click');
                }
            }
        });

        $(document).on('click', '#btn-filter-collapse', function () {
            $(this).toggleClass('collapsed');
            $('#a-filter-collapse').trigger('click');
        });

        $(document).on('click', '.select-staff', function () {
            window.location.href = $(this).find('.select-booking-staff').attr('href');
        });

        $(document).on('click', '.select-seat', function () {
            $('#booking-seat-capacity').append('<input name="seat" type="hidden" value="' + $(this).children('.row').children('.seat_id').val() + '">');
            $('#booking-seat-capacity').submit();
        });
    });

    $(window).load(function () {
        $('.booking-container').css({height : $(window).height() - $('.top-floated').outerHeight(true) - $('.footer-attachment').outerHeight(true)});
    });
})(jQuery);
