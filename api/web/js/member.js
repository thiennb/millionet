$('.multiple-input-list').load(".input-decimal", function(){
    $(this).numeric({ decimal : ".",  negative : false, scale: 3 });
});
$('#mastermember-phone_1').keyup(function(){
    if($(this).val().length == 3){
        $('#mastermember-phone_2').focus();
    }
});

$('#mastermember-phone_2').keyup(function(){
    if($(this).val().length == 4){
        $('#mastermember-phone_3').focus();
    }
});

$('#loginform-phone_1').keyup(function(){
    if($(this).val().length == 3){
        $('#loginform-phone_2').focus();
    }
});

$('#loginform-phone_2').keyup(function(){
    if($(this).val().length == 4){
        $('#loginform-phone_3').focus();
    }
});

$('#mastermember-phone_1_re').keyup(function(){
    if($(this).val().length == 3){
        $('#mastermember-phone_2_re').focus();
    }
});

$('#mastermember-phone_2_re').keyup(function(){
    if($(this).val().length == 4){
        $('#mastermember-phone_3_re').focus();
    }
});

$('#mastermember-phone_1_new').keyup(function(){
    if($(this).val().length == 3){
        $('#mastermember-phone_2_new').focus();
    }
});

$('#mastermember-phone_2_new').keyup(function(){
    if($(this).val().length == 4){
        $('#mastermember-phone_3_new').focus();
    }
});
$(function () {    
    $(".input-number").keyup(function() {
        $(this).each(function(){
            if (/\D/g.test($(this).val())) {
              $(this).val($(this).val().replace(/\D/g, ''));
        }
        })
    });
    $(".input-money").keyup(function() {
        $(this).each(function(){
            if (/\D/g.test($(this).val())) {
              $(this).val($(this).val().replace(/\D/g, ''));
        }
        })
    });
    $(".input-money").on('change', function () {
        var $val = $(this).val();
        $val = $val.replace(/\,/g, '')
        $(this).val('');
        $val = $val.replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        $(this).val($val);
    });
    
    $(document).on('submit', function () {
        $(".input-money").each(function() {
            $(this).each(function(){
                if (/\D/g.test($(this).val())) {
                  $(this).val($(this).val().replace(/\D/g, ''));
            }
            })
        });
    });
});