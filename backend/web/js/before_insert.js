jQuery.fn.preventDoubleSubmission = function () {

    var last_clicked, time_since_clicked;

//    $("#btn-submit").click(function () {
//        if (last_clicked) {
//            time_since_clicked = event.timeStamp - last_clicked;
//        }
//        last_clicked = event.timeStamp;
//
//        if (time_since_clicked < 2000)
//            return false;
//
//        return true;
//    });

    $("#btn-submit").bind('click', function (event) {

        if (last_clicked)
            time_since_clicked = event.timeStamp - last_clicked;

        last_clicked = event.timeStamp;

        if (time_since_clicked < 2000)
            return false;

        return true;
    });
};
$('#mastercoupon-id').preventDoubleSubmission();

