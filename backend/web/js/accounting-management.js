

function detectIEinputNumber(){
    
    if (navigator.userAgent.indexOf('MSIE') !== -1 || navigator.appVersion.indexOf('Trident/') > 0) {
        // MSIE
        
        $('#detail_products').find('tr').each(function (index, element){
            
            if($(element).attr('jan_code').substring(0,2) == '21' 
                    || ($(element).attr('jan_code').substring(0,2) == '22' && ($(element).attr('benefits_content') == '02'|| $(element).attr('benefits_content') == '03'))
                    || ($(element).attr('jan_code').substring(0,2) == '23' && $(element).attr('status_ticket') == 'buy')
                    || ($(element).attr('jan_code').substring(0,2) == 'ch' && $(element).attr('charge_select') == '1')){
                input_number = $(element).find('.quantity').first();
                if($(input_number).parent().find('.arrow_button').length == 0){
                    $(input_number).parent().append('<span type="button" class="arrow_button arrow_up">▲</span><span type="button" class="arrow_button arrow_down">▼</span>');
                }
            }
        });
    }
}
$(document).on('click', '.arrow_button', function(){
    
    input = $(this).parent().find('input').first();
    oldValue = Number($(input).val());
    newValue = 0;
    if($(this).hasClass('arrow_up')){
        newValue = oldValue+1;
    }else{
        if(oldValue > 0){
            newValue = oldValue-1;
        }
    }
    $(input).val(newValue);
});
function getStaffId(storeId) {
    formData = {
        'depdrop_parents': [storeId.toString(), ],
        'depdrop_all_params': {'mastercoupon-store_id': storeId.toString()}
    };

    $.ajax({
        url: $staff_id_url,
        type: 'post',
        data: formData,
        success: function (data) {
            
            $data = JSON.parse(data);
            $listStaff = $data['output'];
            textListStaff = changeTextStaff($listStaff);
            updateStaffandReji($listStaff);
            
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(errorThrown);
        }
    });
}

function changeTextStaff(listStaff) {
    textList = '<select name="staff_product"><option value="">-----</option>';
    listStaff.forEach(function (currentValue, index, arr) {
        //text_select = $mainId == currentValue['id'] ? 'selected' : '';
        text_select = $rejiStaffId == currentValue['id'] ? 'selected' : '';
        textList += '<option value="' + currentValue["id"] + '" '+ text_select + '>' + currentValue["name"] + '</option>';
    });
    textList += '</select>';
    return textList;
}

function updateStaffandReji(listStaff){
    $('select[name=staff_id]').empty();
    $('select[name="reji_staff_id"]').empty();
    $('select[name=staff_id]').append('<option value="">-----</option>');
    $('select[name="reji_staff_id"]').append('<option value="">-----</option>');
    listStaff.forEach(function (currentValue, index, arr) {
        if ($mainId == currentValue['id']) {
            $('select[name=staff_id]').append('<option value="' + currentValue['id'] + '" selected>' + currentValue['name'] + '</option>');
        }else {
            $('select[name="staff_id"]').append('<option value="' + currentValue['id'] + '">' + currentValue['name'] + '</option>');
        }
        
        if ($rejiStaffId == currentValue['id']) {
            $('select[name="reji_staff_id"]').append('<option value="' + currentValue['id'] + '" selected>' + currentValue['name'] + '</option>');
        } else {
            $('select[name="reji_staff_id"]').append('<option value="' + currentValue['id'] + '">' + currentValue['name'] + '</option>');
        }
    });
}

function getCategoriesProduct(storeId) {
    if(storeId == ''){
        storeId = '-1';
    }
    if(storeId != ''){
        product_form = '&store_id=' + storeId + '&pjax_product=1';
        $.pjax({
            type: 'POST',
            data: product_form,
            container: '#pjax_product',
            url: $urlAccountingManagement,
        }).done(function(){
            changeCustomerInfo();
        });
    }
}

function searchByJancode(jancode, storeId) {

    if (jancode != '' && storeId != '') {
        $.ajax({
            url: 'searchbyjancode',
            type: 'post',
            dataType: 'json',
            data: {'jancode': jancode, 'store_id': storeId},
            success: function (data) {
                $('#inputJanCode').empty();
                if (data['result'] != null)
                    if (data['result'] != false) {
                        $button = '<button type="button" class="btn btn-default btn-add btn-add-product"><i class="fa fa-plus-circle" aria-hidden="true"></i>';
                        if (jancode.substring(0, 2) == '21')
                            $button += '<span jan_code="' + jancode + '" id="' + data['result']['id'] + '" price="' + data['result']['unit_price'] + '" tax-method="'+ data['result']['tax_display_method'] +'" tax-id="'+ data['result']['tax_rate_id'] +'">' + data['result']['name'] + '</span>';

                        if (jancode.substring(0, 2) == '22'){
                            $button += '<span jan_code="' + jancode + '"';
                            $button += ' id="' + data['result']['id'] + '"';
                            $button += ' benefits_content="'+data['result']['benefits_content']+'"';
                            $button += ' discount_yen="'+data['result']['discount_yen']+'"';
                            $button += ' discount_percent="'+data['result']['discount_percent']+'"';
                            $button += ' discount_price_set="'+data['result']['discount_price_set']+'"';
                            $button += ' discount_price_set_tax_type="'+data['result']['discount_price_set_tax_type']+'"';
                            $button += ' discount_drink_eat="'+data['result']['discount_drink_eat']+'"';
                            $button += ' discount_drink_eat_tax_type="'+data['result']['discount_drink_eat_tax_type']+'"';
                            $button += ' grant_point="'+data['result']['grant_point']+'"';
                            $button += ' product-array="'+data['coupon_product'].join()+'">' + data['result']['title'] + '</span>';
                        }

                        if (jancode.substring(0, 2) == '23'){
                            $button += '<span jan_code="' + jancode + '" id="' + data['result']['id'] + '"';
                            $button += ' id="' + data['result']['id'] + '"';
                            $button += ' price="'+ data['result']['price']+'"';
                            $button += ' status="buy"';
                            $button += ' set_ticket="'+data['result']['set_ticket']+'"';
                            $button += ' date_expiration="'+data['result']['date_expiration']+'"';
                            $button += '>' + data['result']['name'] + '</span>';
                        }

                        $button += '</button>';
                        $('#inputJanCode').append($button);
                    }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(errorThrown);
            }
        });
    }
}

function getCurrentTotalProduct(){
    $totalProduct = 0;
    $('#detail_products').find('tr').each(function (index, element){
        if($(element).attr('jan_code').substring(0,2) == '21' 
                || ($(element).attr('jan_code').substring(0,2) == '22' && ($(element).attr('benefits_content') == '02'|| $(element).attr('benefits_content') == '03'))
                || ($(element).attr('jan_code').substring(0,2) == '23' && $(element).attr('status_ticket') == 'buy')
                || ($(element).attr('jan_code').substring(0,2) == 'ch' && $(element).attr('charge_select') == '1')){
            $totalProduct++;
        }
    });    
    return $totalProduct;
}

function getTotalDetailMoney(){
    $totalMoney = 0;
    $('#detail_products').find('tr').each(function(index, element){
        first_jancode = $(element).attr('jan_code').substring(0,2);
        benefits_content = '';
        status_ticket = '';
        if(first_jancode == '22'){
            benefits_content = $(element).attr('benefits_content');
        }
        if(first_jancode == '23'){
            status_ticket = $(element).attr('status_ticket');
        }
        if(first_jancode == '21' || (first_jancode == '22' && (benefits_content == '02' || benefits_content == '03') ) 
                || (first_jancode == '23' && status_ticket == 'buy')){
            $totalMoney += Number($(element).attr('show-price'));
        }
    });
    return $totalMoney;
}

function checkExistProductInCoupon(productCoupon){

    result = false;
    if(productCoupon != ''){
        arrayIdProductCoupon = productCoupon.split(",");
        arrayIdProductTable = [];
        $('#detail_products').find('tr').each(function (index, element){
            if($(element).attr('jan_code').substring(0,2) == '21'){
                arrayIdProductTable.push($(element).attr('product-id'));
            }
        });

        for(i = 0; i < arrayIdProductCoupon.length; i++){
            for(j = 0; j < arrayIdProductTable.length; j++){
                if(arrayIdProductCoupon[i] == arrayIdProductTable[j]){
                    result = true;
                    break;
                }
            }
        }        
    }
    return result;
}

function getTotalMoneyProduct(listProductId){
    
    $totalMoney = 0;
    if(listProductId != null && listProductId != ''){
        //certain product
        arrayProduct = listProductId.split(",");
        $('#detail_products').find('tr').each(function (index, element){
            if($(element).attr('jan_code').substring(0,2) == '21'){
                price = $(element).attr('last-price');
                id = $(element).attr('product-id');
                
                if(arrayProduct.indexOf(id) > -1){
                    $totalMoney += Number(price);
                } 
            }
        });
    }else{
        
        // get all product and coupon
        $('#detail_products').find('tr').each(function (index, element){
            if($(element).attr('jan_code').substring(0,2) == '21'){
                price = $(element).attr('last-price');
                $totalMoney += Number(price);
            }
            
            if($(element).attr('jan_code').substring(0,2) == '22'){
                price = $(element).attr('last-price');
                benefits_content = $(element).attr('benefits_content');
                if(benefits_content == '02' || benefits_content == '03'){
                    $totalMoney += Number(price);
                }
            }
            
            if($(element).attr('jan_code').substring(0,2) == '23'){                
                price = $(element).attr('last-price');
                status_ticket = $(element).attr('status_ticket');
                if(status_ticket == 'buy'){                    
                    $totalMoney += Number(price);
                }            
            }
        });
    }
    return $totalMoney;
}
function removeElementInArray(arrayRemove, ElementRemove) {
    index = arrayRemove.indexOf(ElementRemove);
    if(index > -1){
        arrayRemove.splice(index,1);
    }
}

function removeProduct(element) {
    
    $jancode = $(element).parent().parent().attr('jan_code');
    removeElementInArray($listaddedJancode,$jancode);

    $(element).parent().parent().remove();
    $totalProduct = getCurrentTotalProduct();
    
    //if empty product => remove coupon
    if($totalProduct == 0){
        $('#detail_products').find('tr').each(function (index, element){
            if($(element).attr('jan_code').substring(0,2) == '22'){
                $(element).remove();
            }
        });
        //remove all jancode in list add jancode
        for(i = 0; i< $listaddedJancode.length; i++){
            if($listaddedJancode[i].substring(0,2) == '22'){
                removeElementInArray($listaddedJancode,$listaddedJancode[i]);
            }
        }
        
        if($('#detail_products').find('tr').length == 0){
            resetTypeCheckout();
        }
    }
    
    $('#detail_products').find('tr').each(function (index, element){
        if($(element).attr('jan_code').substring(0,2) == '22'){
            if($(element).attr('benefits_content') == '00' || $(element).attr('benefits_content') == '01'){
                if($(element).attr('product_coupons') != ''){
                    arrayProduct = $(element).attr('product_coupons').split(",");
                    remove_flg = true;
                    $('#detail_products').find('tr').each(function (index, element){
                        if($(element).attr('jan_code').substring(0,2) == '21'){
                            if(arrayProduct.indexOf($(element).attr('product-id')) > -1){
                                remove_flg = false;
                            }
                        }
                    });

                    if(remove_flg == true){

                        removeJancode = $(element).attr('jan_code');
                        removeElementInArray($listaddedJancode, removeJancode);
                        $(element).remove();
                    }
                }
            }
        }
    });
    
    updateReceipt();
}

function addProduct(element) {
    
    $totalProduct = getCurrentTotalProduct();
    $jancode = $(element).find('span').attr('jan_code'); 
    $customerId = $('input[name=customer_id]').val();
    $update_flg = true; $append = true;
    if ($listaddedJancode.indexOf($jancode) < 0) {
        
        $textProduct = $(element).find('span').text();
        $id = ''; $price = '';
        $typeProduct = 1; $textTypeProduct = '';
        $taxDisplayMethod = ''; $taxRateId = '';
        
        $benefits_content = ''; $discount_coupon = ''; $coupon_for = '';
        
        $statusTicket = ''; $setTicket = 0;
        
        switch ($jancode.substring(0, 2)) {
            case '21':
                $typeProduct = 1;
                $textTypeProduct = SETTING.PRODUCT;
                $price = $(element).find('span').attr('price');
                $taxDisplayMethod = $(element).find('span').attr('tax-method');
                $taxRateId = $(element).find('span').attr('tax-id');
                $id = $(element).find('span').attr('id');
                $update_flg = false;
                updateTax($taxRateId, $jancode);
                break;
            case '22':
                $benefits_content = $(element).find('span').attr('benefits_content');
                if( $totalProduct > 0 || ($totalProduct == 0 && ($benefits_content== '02' || $benefits_content == '03'))) {
                    $typeProduct = 2;
                    $textTypeProduct = SETTING.COUPON;
                    $productCoupons = $(element).find('span').attr('product-array');
                    $id = $(element).find('span').attr('id');

                    
                    $append = checkExistProductInCoupon($productCoupons);

                    if($benefits_content == '00'){
                        $discount_coupon = $(element).find('span').attr('discount_yen'); 
                        $price = '-' + $discount_coupon;
                    }
                    if($benefits_content == '01'){
                        $discount_coupon = $(element).find('span').attr('discount_percent'); 
                    }
                    if($benefits_content == '02'){
                        $taxDisplayMethod = $(element).find('span').attr('discount_price_set_tax_type'); 
                        $discount_coupon = $(element).find('span').attr('discount_price_set'); 
                        $price = $discount_coupon;
                    }
                    if($benefits_content == '03'){
                        $taxDisplayMethod = $(element).find('span').attr('discount_drink_eat_tax_type'); 
                        $discount_coupon = $(element).find('span').attr('discount_drink_eat');
                        $price = $discount_coupon;
                    }

                    //set append row
                    if($productCoupons != '' ){  
                        $coupon_for = 'certain_product';
                        if($append == false){
                            if($benefits_content == '02' || $benefits_content == '03'){
                                $append = true;
                            }
                        }

                        //update tax for row
                        if($append == true){
                            if($benefits_content == '02' || $benefits_content == '03'){
                                $update_flg = false;
                                $firstId = $productCoupons.split(',')[0];
                                updateTaxCoupon($jancode, $firstId);
                            }
                        }
                    }else{
                        $coupon_for = 'all_product';
                        if($benefits_content == '02' || $benefits_content == '03'){
                            $append = false;
                        }else{
                            $append = true;
                        }
                    }
                }else{
                    $append = false;
                }
                
                break;
            case '23':
                if($customerId == ''){
                    $append = false;
                    alert_message(MESSAGE.EMPTY_CUSTOMER, failed_class);
                }
                $id = $(element).find('span').attr('id');
                $typeProduct = 3;
                $textTypeProduct = SETTING.TICKET;
                $statusTicket = $(element).find('span').attr('status');
                $taxDisplayMethod = '02';
                $setTicket = $(element).find('span').attr('set_ticket');
                $price = $(element).find('span').attr('price');
                break;
            default:
                break;
        }
        
        if($append == true){
            $readonly = $jancode.substring(0, 2) == '22' || $jancode.substring(0, 2) == '23' ? 'readonly':'';
            $row = '<tr product-id="'+$id+'" jan_code="' + $jancode + '" unit_price="'+$price+'" tax-method="'+$taxDisplayMethod+'" tax-id="'+$taxRateId+'" tax-rate="0" tax-money="0" type="' + $typeProduct + '"';
            if($jancode.substring(0, 2) == '22'){
                $row += ' benefits_content="'+ $benefits_content +'"';
                $row += ' product_coupons="'+ $productCoupons +'"';
                $row += ' discount_coupon="'+ $discount_coupon +'"';
                $row += ' coupon_for="'+ $coupon_for +'"';
            }
            if($jancode.substring(0,2) == '23'){
                $row += ' status_ticket="'+ $statusTicket +'"';
                $row += ' set_ticket="'+$setTicket+'"';
            }
            $row += '>';
            $row += '<td class="col_action"><button type="button" class="btn btn-danger btn-add btn-remove btn-remove-product"><i class="fa fa-times-circle" aria-hidden="true"></i></button></td>';
            $row += '<td class="col_type text-left td-product">' + $textTypeProduct + '</td>';
            $row += '<td class="col_name text-left td-product">' + $textProduct + '</td>';
            $row += '<td class="col_private_setting text-left td-product"></td>';
            $row += '<td class="col_price td-product unit-price"><span class="price">' + $price + '</span></td>';
            $row += '<td class=""><input step="1" type="number" name="quantity" class="quantity" min="1" value="1" '+$readonly+'></td>';
            $row += '<td class="td-product total-price"><span class="price">' + $price + '</span></td>';
            $row += '<td class="">' + textListStaff + '</td>';
            $row += '</tr>';

            $('#detail_products').append($row);
            $listaddedJancode.push($jancode);
        }

    } else {
        if($jancode.substring(0, 2) != '22' && $jancode.substring(0, 2) != '23'){
            trElement = $('#detail_products').find('tr[jan_code=' + $jancode + ']');
            if (trElement != 'null') {
                quantity = $(trElement).find('input[name=quantity]').val();
                $(trElement).find('input[name=quantity]').val(Number(quantity) + 1).change();
            }
        }        
    }
    detectIEinputNumber();
    if($update_flg == true){ 
        updateReceipt();        
    }
}

function updateTax(taxRateId, jancode){
    if(taxRateId != 'null' && jancode != 'null'){
        $excuteAjax = true;
        
        if($excuteAjax == true){
            $.ajax({
                url:'gettaxproduct',
                type: 'post',
                dataType : 'JSON',
                data : {'tax-rate-id': taxRateId},
                success: function (data) {

                    if(data !== null){
                        $('#detail_products').find('tr[jan_code='+ $jancode +']').first().attr('tax-rate', data['tax']['rate']);
                        updateReceipt();
                    }                    
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log(errorThrown);
                }
            });
        }
    }
}

function updateTaxCoupon(jancode, productId){
    if(productId != 'null' && jancode != 'null'){

        $.ajax({
            url:'gettaxcoupon',
            type: 'post',
            dataType : 'JSON',
            data : {'product_id': productId},
            success: function (data) {

                if(data !== null){
                    $('#detail_products').find('tr[jan_code='+ $jancode +']').first().attr('tax-rate', data['tax']['rate']);
                    $('#detail_products').find('tr[jan_code='+ $jancode +']').first().attr('tax-id', data['tax']['tax-id']);
                    updateReceipt();
                }                    
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(errorThrown);
            }
        });
    }
}

function updateAllRowProduct(){
    // 1: update all product in table
    $('#detail_products').find('tr').each(function (index, element){
        if($(element).attr('jan_code').substring(0,2) == '21'){
            unit_price = $(element).attr('unit_price');
            quantity = $(element).find('input[name=quantity]').val();

            total_price = Number(unit_price)*Number(quantity);
            
            $(element).find('.total-price').first().find('.price').text(total_price);
            
            attr = $(element).attr('product_discount_status');
            $(element).attr('show-price', total_price);
            if(typeof attr !== typeof undefined && attr !== false){
                if(attr == '0'){
                    total_price = total_price - total_price*Number($(element).attr('product_reduction'))/100;
                }else if( attr == '1'){
                    total_price = total_price - Number($(element).attr('product_reduction'));
                }
            }            
            $(element).attr('last-price', total_price);
        }
    });
}

function updateAllUseTicket(){
    $('#detail_products').find('tr').each(function (index, element){
        if($(element).attr('jan_code').substring(0,2) == '23'){
            status_ticket = $(element).attr('status_ticket');
            quantity_ticket = $(element).find('input[name=quantity]').val();
            ticket_product = $(element).attr('list_product');
            if(status_ticket == 'use' && Number(quantity_ticket) > 0){
                $('#detail_products').find('tr').each(function (index, element){
                    jancode = $(element).attr('jan_code');
                    if(jancode.substring(0,2) == '21' && ticket_product.indexOf(jancode) > -1){
                        unit_price = $(element).attr('unit_price');
                        quantity = $(element).find('input[name=quantity]').val();

                        total_price = Number(unit_price)*Number(quantity) - Number(unit_price)*Number(quantity_ticket);

                        $(element).find('.total-price').first().find('.price').text(total_price);

                        attr = $(element).attr('product_discount_status');
                        $(element).attr('show-price', total_price);
                        if(typeof attr !== typeof undefined && attr !== false){
                            if(attr == '0'){
                                total_price = total_price - total_price*Number($(element).attr('product_reduction'))/100;
                            }else if( attr == '1'){
                                total_price = total_price - Number($(element).attr('product_reduction'));
                            }
                        }            
                        $(element).attr('last-price', total_price);
                    }
                });
            }
        }
    });
}

function updateAllRowCouponSetPriceOrFreeAndTicket(){
    
    $('#detail_products').find('tr').each(function (index, element){
        if($(element).attr('jan_code').substring(0,2) == '22'){            
            //update coupon with benefits content = 02, 03, set price or free
            if($(element).attr('benefits_content') == '02' || $(element).attr('benefits_content') == '03'){

                type_tax = $(element).attr('tax-method');
                total_price = $(element).attr('unit_price');
                
                $(element).find('.total-price').first().find('.price').text(total_price);
                $(element).attr('show-price', total_price);
                $(element).attr('last-price', total_price);
            }
        }
        
        if($(element).attr('jan_code').substring(0,2) == '23'){            
            //update ticket
            total_price = $(element).attr('unit_price');

            $(element).find('.total-price').first().find('.price').text(total_price);
            
            attr = $(element).attr('product_discount_status');
            if(typeof attr !== typeof undefined && attr !== false){
                if(attr == '0'){
                    total_price = total_price - total_price*Number($(element).attr('product_reduction'))/100;
                }else if( attr == '1'){
                    total_price = total_price - Number($(element).attr('product_reduction'));
                }
            }
            
            $(element).attr('show-price', total_price);
            $(element).attr('last-price', total_price);
        }
    }); 
}

function updateAllRowCouponYenForcertain(){
    $('#detail_products').find('tr').each(function (index, element){
        if($(element).attr('jan_code').substring(0,2) == '22'){
            //update coupon with benefits content = 0, discount yen
            if($(element).attr('benefits_content') == '00'){
                $discount = Number($(element).attr('discount_coupon'));
                text_product_coupon = $(element).attr('product_coupons');
                product_coupons = text_product_coupon.split(",");
                totaldiscount = 0;
                $quantity = 0;
                
                if(text_product_coupon){
                    $('#detail_products').find('tr').each(function (index, element){
                        if($(element).attr('jan_code').substring(0,2) == '21'){
                            id = $(element).attr('product-id');
                            $lastprice = Number($(element).attr('last-price'));

                            if(product_coupons.indexOf(id) > -1){
                                $quantity++;
                                if($lastprice > $discount){
                                    totaldiscount += $discount;
                                    $lastprice = $lastprice - $discount;
                                }else{
                                    totaldiscount += $lastprice;
                                    $lastprice = 0;
                                }
                                
                                $(element).attr('last-price', $lastprice);
                            } 
                        }
                    });

                    //unit_price : '-'+$discount, total-price : '-'+totaldiscount, show-price:'-'+totaldiscount , last-price: '-'+totaldiscount
                    //updateTableRow(element, unit_price, last_price, show_price, total_price, quantity);
                    updateTableRow(element,'-'+$discount, '-'+totaldiscount, '-'+totaldiscount, '-'+totaldiscount, $quantity);                    
                }                
            }
        }
    });
}

function updateAllRowCouponPercentForCertain(){
    $('#detail_products').find('tr').each(function (index, element){
        if($(element).attr('jan_code').substring(0,2) == '22'){
            //update coupon with benefits content = 01, discount %
            if($(element).attr('benefits_content') == '01'){
                $discount = Number($(element).attr('discount_coupon'));
                text_product_coupon = $(element).attr('product_coupons');
                product_coupons = text_product_coupon.split(",");
                totaldiscount = 0;
                $quantity = 0;
                
                if(text_product_coupon){
                    $('#detail_products').find('tr').each(function (index, element){
                        if($(element).attr('jan_code').substring(0,2) == '21'){
                            $lastprice = Number($(element).attr('last-price'));
                            id = $(element).attr('product-id');
                            if(product_coupons.indexOf(id) > -1){
                                $quantity++;
                                totaldiscount += Number($lastprice)*Number($discount)/100;
                                //$(element).attr('last-price', Number($lastprice - moneydiscount));
                            } 
                        }
                    });
                    
                    //unit_price : '-'+totaldiscount, total-price : '-'+totaldiscount, show-price:'-'+totaldiscount , last-price: '-'+totaldiscount
                    //updateTableRow(element, unit_price, last_price, show_price, total_price, quantity);
                    updateTableRow(element,'-'+totaldiscount, '-'+totaldiscount, '-'+totaldiscount, '-'+totaldiscount, $quantity);
                }                
            }
        }
    });
    
    $('#detail_products').find('tr').each(function (index, element){
        if($(element).attr('jan_code').substring(0,2) == '21'){
            $lastprice = Number($(element).attr('last-price'));
            id = $(element).attr('product-id');
            //update coupon with benefits content = 01, discount %
            $totalpercent = 0;
            $('#detail_products').find('tr').each(function (index, element){
                if($(element).attr('jan_code').substring(0,2) == '22'){
                    if($(element).attr('benefits_content') == '01'){
                        $discount = Number($(element).attr('discount_coupon'));
                        text_product_coupon = $(element).attr('product_coupons');
                        product_coupons = text_product_coupon.split(",");

                        if(text_product_coupon){                                
                            if(product_coupons.indexOf(id) > -1){
                                $totalpercent+=$discount;
                            } 
                        }
                    }                
                }
            });
            
            if($totalpercent != 0){
                moneydiscount = Number($lastprice)*Number($totalpercent)/100;
                $(element).attr('last-price', Number($lastprice - moneydiscount));
            }
        }
    });
}

function updateAllRowCouponYenForAll(){
    totalMoney = getTotalMoneyProduct();
    totalDiscount = 0;
    
    //get total discount
    $('#detail_products').find('tr').each(function (index, element){
        if($(element).attr('jan_code').substring(0,2) == '22'){
            //update coupon with benefits content = 0, discount yen
            if($(element).attr('benefits_content') == '00'){
                $discount = Number($(element).attr('discount_coupon'));
                text_product_coupon = $(element).attr('product_coupons');
                $text_discount = $discount;                
                if(!text_product_coupon){
                    if(totalDiscount + $discount > totalMoney){
                        $discount = totalMoney - totalDiscount;
                    }
                    totalDiscount += Number($discount);
                    //unit_price : '-'+$discount, total-price : '-'+$discount, show-price:'-'+$discount , last-price: ''
                    //updateTableRow(element, unit_price, last_price, show_price, total_price, quantity);
                    updateTableRow(element,'-'+$text_discount, '-'+$discount, '-'+$discount, '-'+$discount, 1);
                }                
            }            
        }
        if($(element).attr('jan_code') == 'rd'){
            if($(element).attr('product_discount') == '1'){
                totalDiscount += Number($(element).attr('product_reduction'));
            }
        }
    });
    
    if(totalMoney < totalDiscount ){
        totalDiscount = totalMoney;
    }
    
    $('#detail_products').find('tr').each(function (index, element){
        first_jancode = $(element).attr('jan_code').substring(0,2);
        benefits_content = '';
        status_ticket = '';
        if(first_jancode == '22'){
            benefits_content = $(element).attr('benefits_content');
        }
        
        if(first_jancode == '23'){
            status_ticket = $(element).attr('status_ticket');
        }
        if(first_jancode == '21' || (first_jancode == '22' && (benefits_content == '02' || benefits_content == '03') ) || (first_jancode == '23'&& status_ticket == 'buy') ){
            $lastprice = Number($(element).attr('last-price'));
            moneyDiscount = totalDiscount != 0 ? $lastprice/totalMoney*totalDiscount : 0;
            $(element).attr('last-price', $lastprice - moneyDiscount);

        }
    });
}

function updateAllRowCouponPercentForAll(){

    totalMoney = getTotalMoneyProduct();
    totalDiscount = 0;
    //total all discount percent
    $('#detail_products').find('tr').each(function(index, element){
        if($(element).attr('jan_code').substring(0,2) == '22'){
            if($(element).attr('benefits_content') == '01'){
                $discount = $(element).attr('discount_coupon');
                text_product_coupon = $(element).attr('product_coupons');
                                
                if(!text_product_coupon){
                    totalDiscount += Number($discount);
                    moneyDiscount = Number($discount)*totalMoney/100;

                    //unit_price : '-'+moneyDiscount, total-price : '-'+moneyDiscount, show-price:'-'+moneyDiscount , last-price: '-'+moneyDiscount
                    //updateTableRow(element, unit_price, last_price, show_price, total_price, quantity);
                    updateTableRow(element,'-'+moneyDiscount, '-'+moneyDiscount, '-'+moneyDiscount, '-'+moneyDiscount, 1);
                }                
            }
        }
        
        if($(element).attr('jan_code') == 'rd'){
            if($(element).attr('product_discount') == '0'){
                totalDiscount += Number($(element).attr('product_reduction'));
            }
        }
    });
    
    //$totalMoneyDiscount = totalDiscount*totalMoney/100;
    $('#detail_products').find('tr').each(function (index, element){
        first_jancode = $(element).attr('jan_code').substring(0,2);
        benefits_content = '';
        if(first_jancode == '22'){
            benefits_content = $(element).attr('benefits_content');
        }
        if(first_jancode == '23'){
            status_ticket = $(element).attr('status_ticket');
        }
        if(first_jancode == '21' || (first_jancode == '22' && (benefits_content == '02' || benefits_content == '03')) || (first_jancode == '23'&& status_ticket == 'buy') ){

            $lastprice = $(element).attr('last-price');                            
            moneydiscount = Number($lastprice)*Number(totalDiscount)/100;
            $(element).attr('last-price', Number($lastprice - moneydiscount));

        }
    });
}

function updateTableRow(element, unit_price, last_price, show_price, total_price, quantity){
    $(element).attr('unit_price', unit_price);
    if(last_price != '')
        $(element).attr('last-price', last_price);
    if(show_price != '')
        $(element).attr('show-price', show_price);
    $(element).find('.total-price').find('.price').text(total_price);
    $(element).find('.unit-price').find('.price').text(unit_price);
    $(element).find('input[name=quantity]').val(quantity);
}

function updateAllRowPoint(){
    totalMoney = getTotalMoneyProduct();
    point_allocation = 0; point_increase = 0; point_reduce = 0; point_use = 0; money_conversion_from_point = 0;
    
    $('#detail_products').find('tr').each(function(index, element){
        jancode = $(element).attr('jan_code');
        point_status = $(element).attr('point_status');
        
        if(jancode == 'pt' && point_status == '0'){
            point_use = Number($(element).attr('point_input'));
        }
        
        if(jancode == 'pt' && point_status == '1'){
            point_increase = Number($(element).attr('point_input'));
        }
        
        if(jancode == 'pt' && point_status == '2'){
            point_reduce = Number($(element).attr('point_input'));
        }
    });
    
    money_conversion_from_point = Number(point_use) * point_conversion_ratio;
    
}

function updateAllRowCharge(){
    charge_use = 0; charge_add = 0; charge_add_money = 0;

    $('#detail_products').find('tr').each(function(index, element){
        jancode = $(element).attr('jan_code');
        charge_status = $(element).attr('charge_select');
        
        if(jancode == 'ch' && charge_status == '0'){
            charge_use = Number($(element).attr('charge_input'));
        }
        
        if(jancode == 'ch' && charge_status == '1'){
            charge_add = Number($(element).attr('charge_input'));
            charge_add_money = Number($(element).attr('input_money_charge'));
        }        
    });
    
}

function updateMoneyallRowTable(){
    
    updateAllRowProduct();
    updateAllUseTicket();
    updateAllRowCouponSetPriceOrFreeAndTicket();    
    updateAllRowCouponYenForcertain();
    updateAllRowCouponPercentForCertain();
    updateAllRowCouponYenForAll();
    updateAllRowCouponPercentForAll();
    updateAllRowPoint();
    updateAllRowCharge();
}

function updateSubTotalMoney(){    
    $('.sub_total').text(convertMoneytoString(getsubTotalLastPrice()));
}

function getDiscountCoupon(){
    $totalMoney = 0;
    $('#detail_products').find('tr').each(function (index, element){
        first_jancode = $(element).attr('jan_code').substring(0,2);
        benefits_content = '';
        if(first_jancode == '22'){
            benefits_content = $(element).attr('benefits_content');
        }
        if(first_jancode == '22' && (benefits_content == '00' || benefits_content == '01') ){
            $totalMoney += Number($(element).attr('show-price'));
        }
    });
    
    return $totalMoney;
}

function getsubTotalLastPrice(){
    $subTotalLastPrice = 0;
               
    $('#detail_products').find('tr').each(function (index, element){
        first_jancode = $(element).attr('jan_code').substring(0,2);
        benefits_content = '';
        if(first_jancode == '22'){
            benefits_content = $(element).attr('benefits_content');
        }
        if(first_jancode == '21' || (first_jancode == '22' && (benefits_content == '02' || benefits_content == '03') )){

            $lastprice = $(element).attr('last-price');                            
            $subTotalLastPrice += Number($lastprice);
        }
        if(first_jancode == '23'){                
            price = $(element).attr('last-price');
            status_ticket = $(element).attr('status_ticket');
            if(status_ticket == 'buy'){                    
                $subTotalLastPrice += Number(price);
            }            
        }
    });
    return Math.round($subTotalLastPrice);
}


function calculateTaxMoney(){
    $totalInnerTax = 0;
    $totalOutterTax = 0;
    $subtotalMoney = Number(convertStringtoMoney($('.sub_total').text()));    
    $yinyang = 1;
    
    $('#detail_products').find('tr').each(function (index, element){
        first_jancode = $(element).attr('jan_code').substring(0,2);
        benefits_content = '';
        if(first_jancode == '22'){
            benefits_content = $(element).attr('benefits_content');
        }
        if(first_jancode == '21' || (first_jancode == '22' && (benefits_content == '02' || benefits_content == '03') )){
            type_tax = $(element).attr('tax-method');
            rate = $(element).attr('tax-rate');
            money = $(element).attr('last-price');
            $taxMoney = 0;
            if(type_tax == '00'){
                $taxMoney = (Number(money) - Number(money)/(1+rate/100));
                $totalInnerTax += $taxMoney;
            }
            if(type_tax == '01'){
                $taxMoney = (Number(money)*rate/100);
                $totalOutterTax += $taxMoney;
            }
            $(element).attr('tax-money', Math.round($taxMoney));
        }
    });

    $adjustment = round_down_flg == 0 ? 0 : Math.round(($subtotalMoney + $totalOutterTax)%10);
    
    lastMoney = Math.round($subtotalMoney + $totalOutterTax - $adjustment - Math.round(money_conversion_from_point)) - charge_use + charge_add_money;
    point_allocation = Math.round(lastMoney/grant_yen_per_point);
    
    if(return_good_status == 1){
        $yinyang = -1;
        $('.sub_total').text('-'+$('.sub_total').text());
    }

    $('.use_charge').text(convertMoneytoString(Math.round(money_conversion_from_point + charge_use)*$yinyang));
    $('.sum_tax').text(convertMoneytoString(Math.round($totalInnerTax + $totalOutterTax)*$yinyang));
    $('.adjustment_money').text(convertMoneytoString($adjustment*$yinyang));
    $('.end_money_total').text(convertMoneytoString(lastMoney*$yinyang));
}

function updateDisableSelect(){
    $('#detail_products').find('tr').each(function (index, element){
        first_jancode = $(element).attr('jan_code').substring(0,2);
        if(first_jancode == '22' || first_jancode == '23' || first_jancode == 'ch' || first_jancode == 'pt'){
            $(element).find('select[name=staff_product]').prop('selectedIndex',0);
            $(element).find('select[name=staff_product]').change();
            $(element).find('select[name=staff_product]').prop('disabled', 'disabled');
        }
    });
}

function updateReceipt(){
   updateDisableSelect();
   updateMoneyallRowTable();
   updateSubTotalMoney();
   calculateTaxMoney();
   UpdateMoneyAfterReduce();
}

function getInputCheckoutTotalMoney(){
    TotalMoney = 0;
    TotalMoney += Number($('input[name=type_checkout_1]').val());
    TotalMoney += Number($('input[name=type_checkout_2]').val());
    TotalMoney += Number($('input[name=type_checkout_3]').val());
    TotalMoney += Number($('input[name=real_cash]').val());
    
    return TotalMoney;
}

function getInputCheckoutFirstMoney(){
    TotalMoney = 0;
    TotalMoney += Number($('input[name=type_checkout_1]').val());
    TotalMoney += Number($('input[name=type_checkout_2]').val());
    TotalMoney += Number($('input[name=type_checkout_3]').val());
    return TotalMoney;
}

function UpdateMoneyAfterReduce(){
    endMoney = Number(convertStringtoMoney($('.end_money_total').text()));
    totalFirstMoney = getInputCheckoutFirstMoney();
    Money = endMoney - totalFirstMoney;
    $('.total_money_after').text(convertMoneytoString(Money));
    updateRefundMoney();
}

function updateRefundMoney(){
    realCashMoney = Number(convertStringtoMoney($('input[name=real_cash]').val()));
    endMoney = Number(convertStringtoMoney($('.end_money_total').text()));
    totalFirstMoney = getInputCheckoutFirstMoney();
    if(return_good_status == 1){
        $('.refund_money').text(convertMoneytoString((endMoney-realCashMoney)*-1));
    }else{
        if(totalFirstMoney > endMoney){
            $('.refund_money').text(convertMoneytoString(realCashMoney));
        }else{
            refundMoney = realCashMoney - endMoney + totalFirstMoney;
            $('.refund_money').text(refundMoney > 0 ? convertMoneytoString(refundMoney) : 0);
        }
    }
}

function changeCustomerInfo(){
    var custormer_id = $('input[name=customer_id]').val();
    var store_id = $('select[name=store_id]').val();
    if(custormer_id != '' && store_id != ''){
        var custormer_form = custormer_form + '&custormer_id=' + custormer_id + '&store_id=' + store_id + '&pjax_first=1';
        $.pjax({
            type: 'POST',
            data: custormer_form,
            container: '#pjax_first',
            url: $urlAccountingManagement,
        });
    }
}
function getStoreInfo(){    
    getStaffId($('select[name=store_id]').val());
    getCategoriesProduct($('select[name=store_id]').val());
}
function resetMessageError(){
    $('#error_order').find('span').text('');
}

function initAddJancode(){
    $('#detail_products').find('tr').each(function (index, element){
        $jancode = $(element).attr('jan_code');
        $listaddedJancode.push($jancode);
    });
}

$(document).on('click', '.btn-ajax-modal', function (e) {
    var $store_id = $('select[name="store_id"]').val();
    //check select store before select customer
    if(!$store_id){
        e.preventDefault();
        return false;
    }
    var firstname = $('#mastercustomer-first_name').val();
    var lastname = $('#mastercustomer-last_name').val();
    var firstnamekana = $('#mastercustomer-first_name_kana').val();
    var lastnamekana = $('#mastercustomer-last_name_kana').val();
    var mobile = $('#mastercustomer-mobile').val();
    $('#userModal').modal('show')
        .find('#modalContent')
        .load($(this).attr('value'), {firstname: firstname,
            lastname: lastname,
            firstnamekana: firstnamekana,
            lastnamekana: lastnamekana,
            mobile: mobile
        });
});

$(document).on('click', '#btn-custormer', function () {

    var custormer_id = $('input[name=customerId]:checked').val();
    if (custormer_id == null)
        return false;
    var store_id = $('select[name=store_id]').val();

    var custormer_form = $('#form-booking').serialize();
    custormer_form = custormer_form + '&custormer_id=' + custormer_id + '&store_id=' + store_id + '&pjax_first=1';

    $.pjax({
        type: 'POST',
        data: custormer_form,
        container: '#pjax_first',
        url: $urlAccountingManagement,
    }).done(function () {
        $('#userModal').modal('toggle');
    });
    return false;
});

//get staff and reji
$(document).on('change','select[name=store_id]', function () {
    $('#detail_products').empty();
    getStoreInfo();       
    //$('#btn-search').removeClass('hidden');
    check_select_store();
});

//get jancode
$(document).on('input','input[name=jan_code]', function () {
    
    if ($(this).val().length == 13) {
        $storeId = $('select[name=store_id]').val();
        searchByJancode($(this).val(), $storeId);
    }
});

$(document).on('click', '.btn-add-product', function(){
    addProduct(this);
});

$(document).on('input','input[name=quantity]', function () {
    updateReceipt();
});

$(document).on('click', '.btn-remove-product', function(){
    removeProduct(this);
});
//checkout 
$(document).on('change','select[name=type_checkout_1]', function () {
    updateInputTypeCheckout('type_checkout_1');
});

$(document).on('change','select[name=type_checkout_2]', function () {
    updateInputTypeCheckout('type_checkout_2');
});

$(document).on('change','select[name=type_checkout_3]', function () {
    updateInputTypeCheckout('type_checkout_3');
});

$listvalueCheckOut = [];
function updateInputTypeCheckout(nameSelect){
    reset = false;
    if(nameSelect != ''){
        if($('select[name='+nameSelect+']').val() == 'blank'){
            
            $('input[name='+nameSelect+']').val('');
            $('input[name='+nameSelect+']').prop('disabled', true);
            
        }else{
            
            if($listvalueCheckOut.indexOf($('select[name='+nameSelect+']').val()) < 0){
                
                if(Number(convertStringtoMoney($('.end_money_total').text())) > 0){
                    oldMoney = $('input[name='+nameSelect+']').val();
                    currentValue = 0;
                    endMoney =  Number(convertStringtoMoney($('.end_money_total').text()));
                    currentTotalMoney = getInputCheckoutTotalMoney();
                    if(endMoney > currentTotalMoney){
                        currentValue = endMoney - currentTotalMoney;
                    }
                    if(currentValue > 0){
                        $('input[name='+nameSelect+']').prop('disabled', false);
                        $('input[name='+nameSelect+']').val(currentValue);
                    }else{
                        if(oldMoney == 0)
                            reset = true;
                            
                    }
                }
            }else{
                reset = true;
            }
        }
        
        if(reset == true){
            $('select[name='+nameSelect+']').val('blank').change();
        }
        
        UpdateMoneyAfterReduce();
        $listvalueCheckOut = [];
        if($('select[name=type_checkout_1]').val() != 'blank'){
            $listvalueCheckOut.push($('select[name=type_checkout_1]').val());
        }
        if($('select[name=type_checkout_2]').val() != 'blank'){
            $listvalueCheckOut.push($('select[name=type_checkout_2]').val());
        }
        if($('select[name=type_checkout_3]').val() != 'blank'){
            $listvalueCheckOut.push($('select[name=type_checkout_3]').val());
        }
    }
}

$(document).on('input','input[name=type_checkout_1]', function () {    
    UpdateMoneyAfterReduce();
});
$(document).on('input','input[name=type_checkout_2]', function () {    
    UpdateMoneyAfterReduce();
});
$(document).on('input','input[name=type_checkout_3]', function () {
    UpdateMoneyAfterReduce();
});

$(document).on('input','input[name=real_cash]', function () {    
    updateRefundMoney();
});
$(document).on('change','input[name=real_cash]', function () {    
    updateRefundMoney();
});
$(document).on('change', 'select[name=staff_id]', function(){
    $mainId = $('select[name=staff_id]').val();
    textListStaff = changeTextStaff($listStaff);
    updateStaffandReji($listStaff);
});

$(document).on('change', 'select[name=reji_staff_id]', function(){
    $rejiStaffId = $('select[name=reji_staff_id]').val();
    textListStaff = changeTextStaff($listStaff);
    updateStaffandReji($listStaff);
});

$(document).on('click', '#submit_order', function(){
    resetMessageError();
    
    excute = true;
    errorMessage = '';
    bookingId = $('label[name=booking_code]').attr('booking_id');
    totalProduct = getCurrentTotalProduct();    
    customerId = $('input[name=customer_id]').val();
    storeId = $('select[name=store_id]').val();
    staffId = $('select[name=staff_id]').val();
    rejiStaffId = $('select[name=reji_staff_id]').val();
    totalTax = Math.round($totalInnerTax + $totalOutterTax);
    subTotal = Number(convertStringtoMoney($('.sub_total').text()));
    adjustmentMoney = Number(convertStringtoMoney($('.adjustment_money').text()));
    endMoney = Number(convertStringtoMoney($('.end_money_total').text()));
    afterReduceMoney = Number(convertStringtoMoney($('.total_money_after').text()));
    realCash = Number($('input[name=real_cash]').val());
    refundMoney = Number(convertStringtoMoney($('.refund_money').text()));
    totalDetailMoney = getTotalDetailMoney();
    discountCoupon = getDiscountCoupon();
    totalCheckoutMoney = getInputCheckoutTotalMoney();
    moneyCheckoutType = [
        {'type' : $('select[name=type_checkout_1]').val(), 'money': $('input[name=type_checkout_1]').val() },
        {'type' : $('select[name=type_checkout_2]').val(), 'money': $('input[name=type_checkout_2]').val() },
        {'type' : $('select[name=type_checkout_3]').val(), 'money': $('input[name=type_checkout_3]').val() }
    ];
    
    listProduct = [];    
    
    $('#detail_products').find('tr').each(function (index, element){
        first_jancode = $(element).attr('jan_code').substring(0,2);
        gift_product = $(element).attr('gift_product');

        listProduct.push({
            'product_id': $(element).attr('product-id') ,'jancode': $(element).attr('jan_code'), 'display_name' : $(element).find('.col_name').text().trim(),
            'unit_price': $(element).attr('unit_price'), 'quantity': $(element).find('input[name=quantity]').val(), 
            'normal_price': $(element).attr('show-price'),'last_price' : $(element).attr('last-price'), 
            'tax_method': $(element).attr('tax-method'),  'tax_id' : $(element).attr('tax-id'), 'tax_rate' : $(element).attr('tax-rate'),
            'tax_money': $(element).attr('tax-money'), 'gift_product' : gift_product,
            'benefits_content': $(element).attr('benefits_content'), 'product_coupons':  $(element).attr('product_coupons'),
            'discount_coupon':  $(element).attr('discount_coupon'), 'staff_product' : $(element).find('select[name=staff_product]').val(),
            'status_ticket' : $(element).attr('status_ticket'), 'set_ticket': $(element).attr('set_ticket'),
            'price_down_type' : $(element).attr('product_discount_status'),'price_down_value' : $(element).attr('product_reduction'),
        });

    });    
    
    formData = {
        'return_good_status':return_good_status,'total_product': totalProduct,'storeId' : storeId, 'staffId':staffId, 'rejiStaffId':rejiStaffId, 'totalTax': totalTax , 'subTotal' : subTotal,
        'endMoney' : endMoney, 'afterReduceMoney': afterReduceMoney, 'realCash': realCash, 'refundMoney': refundMoney, 'moneyCheckoutType': moneyCheckoutType,
        'adjustmentMoney':adjustmentMoney, 'totalDetailMoney': totalDetailMoney, 'discountCoupon': discountCoupon, 'totalCheckoutMoney' : totalCheckoutMoney,
        'totalInnerTax': $totalInnerTax,  'totalOutterTax': $totalOutterTax,
        'listProduct': listProduct,'customerId': customerId, 'bookingId':bookingId, 'money_discount_yen' : money_discount_yen, 'money_discount_percent' : money_discount_percent,
        'point_allocation': point_allocation, 'point_increase': point_increase, 'point_reduce': point_reduce, 'point_use': point_use, 'money_conversion_from_point': money_conversion_from_point,
        'charge_use' : charge_use, 'charge_add': charge_add, 'charge_add_money' : charge_add_money,
    }
    
    for(i = 0 ;  i< listProduct.length; i++){
        if(listProduct[i]['jancode'].substring(0,2) == '21'){
            if(listProduct[i]['staff_product'] == ''){
                alert_message(MESSAGE.ERR_STAFF_PRODUCT, failed_class);
                return;
            }
        }
        
    }
    if(totalProduct <= 0){
        alert_message(MESSAGE.EMPTY_PRODUCT, failed_class);
        return;
    }
    
    if(return_good_status == 0){
        if(Number(afterReduceMoney) < 0){
            alert_message(MESSAGE.ERR_MONEY_END_LESS_THAN_0, failed_class);
            return;
        }

        if((totalCheckoutMoney + realCash) < endMoney){
            alert_message(MESSAGE.MONEY_IS_NOT_ENOUGH, failed_class);
            return;
        }
        if(refundMoney > 10000){
            alert_message(MESSAGE.MONEY_IS_TOO_LARGE_THAN_10000, failed_class);
            return;
        }
    }else{
        if(refundMoney < 0){
            alert_message(MESSAGE.MONEY_IS_TOO_LARGE_THAN_0, failed_class);
            return;
        }
    }
    
    if(return_good_status == 1){
        formData['subTotal'] = Number(subTotal)*-1;
        formData['endMoney'] = Number(endMoney)*-1;
        formData['afterReduceMoney'] = Number(afterReduceMoney)*-1;
    }
        
    $.ajax({
        url: $order_checkout_url,
        type: 'post',
        dataType:'JSON',
        data: formData,
        success: function (data) {
            if(data['result']['status'] == '1'){
                alert_message(data['result']['message'], success_class);
                reset_form();
            }else{
                alert_message(data['result']['message'], failed_class);
            }            
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(errorThrown);
        }
    });
});

//limit length
//$(document).on('keydown keyup', 'input[name=real_cash]', function(e){
//    if($(this).val().length > 9 && e.keyCode != 46 && e.keyCode != 8){
//        e.preventDefault();        
//    }
//});

$(document).on('keydown keyup', 'input[name=type_checkout_1]', function(e){
    if($(this).val().length > 9 && e.keyCode != 46 && e.keyCode != 8){
        e.preventDefault();        
    }
});

$(document).on('keydown keyup', 'input[name=type_checkout_2]', function(e){
    if($(this).val().length > 9 && e.keyCode != 46 && e.keyCode != 8){
        e.preventDefault();        
    }
});

$(document).on('keydown keyup', 'input[name=type_checkout_3]', function(e){
    if($(this).val().length > 9 && e.keyCode != 46 && e.keyCode != 8){
        e.preventDefault();        
    }
});

$(document).on('keydown keyup', 'input[name=jan_code]', function(e){
    if($(this).val().length > 16 && e.keyCode != 46 && e.keyCode != 8){
        e.preventDefault();        
    }
});

$(document).on('keydown keyup', 'input[name=quantity]', function(e){
    if(($(this).val().length > 4 && e.keyCode != 46 && e.keyCode != 8) || e.keyCode == 173){
        e.preventDefault();        
    }    
});

function alert_message(_message, _class){
    $('#message_order').attr('class', _class);
    $('#message_order').find('span').text(_message);
    $('html, body').animate({
        scrollTop: $("#message_order").offset().top-200
    }, 500);
}

function check_select_store(){
    var $store_id = $('select[name="store_id"]').val();
    if(!$store_id){        
        resetcustomer();
        $('#btn-search').addClass('hidden');
    }else{
        $('#btn-search').removeClass('hidden');
    }
}

function resetcustomer(){
    $('#pjax_first #customerName').text('');
    $('#customer-point-charge-ticket').remove();
    $('#pjax_first #customerId').val('');    
}

function resetTypeCheckout(){
    $('select[name=type_checkout_1]').change();
    $('select[name=type_checkout_2]').change();
    $('select[name=type_checkout_3]').change();
    
    if(return_good_status == 1){
        $('select[name=type_checkout_1]').prop('disabled', 'disabled');
        $('select[name=type_checkout_2]').prop('disabled', 'disabled');
        $('select[name=type_checkout_3]').prop('disabled', 'disabled');
    }else{
        $('select[name=type_checkout_1]').prop('disabled', false);
        $('select[name=type_checkout_2]').prop('disabled', false);
        $('select[name=type_checkout_3]').prop('disabled', false);
    }
}

function reset_form(){
    
    history.pushState("", document.title, window.location.pathname);
    $urlAccountingManagement = window.location.href;
    $listaddedJancode = [];
    
    $('label[name=booking_code]').attr('booking_id', '');
    $('label[name=booking_code]').text('');
    $('#detail_products').empty();
            
    $('.total_money_after').text('');
    $('input[name=real_cash]').val('');
    $(document).find('select[name=staff_id]').prop('selectedIndex',0);
    $(document).find('select[name=staff_id]').change();
    $(document).find('select[name=reji_staff_id]').prop('selectedIndex',0);
    $(document).find('select[name=reji_staff_id]').change();
    resetTypeCheckout();
    checkBookingStatus();
    resetcustomer();
    updateReceipt();
}

function checkBookingStatus(){
    bookingId = $('label[name=booking_code]').attr('booking_id');
    
    if(bookingId != ''){
        $('#return_goods').addClass('hidden');
        return_good_status = 0;
    }else{        
        $('#return_goods').removeClass('hidden');
    }
}

$(document).on('click', '#return_goods', function(){
    console.log('pass');
    if($(this).hasClass('active')){
        return_good_status = 0;
        $(this).removeClass('active');
    }else{
        return_good_status = 1;
        $('#return_goods').addClass('active');
    }
    updateReceipt();
    resetTypeCheckout();
});

$(document).ready(function () {
    checkBookingStatus();
    getStoreInfo();
    
    initAddJancode();
    check_select_store();
    setTimeout( function(){ 
        updateReceipt();
    }  , 2000 );
});
