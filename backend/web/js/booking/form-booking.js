jQuery.fn.preventDoubleSubmission = function () {

  var last_clicked, time_since_clicked;
  $("#btn-submit").bind('click', function (event) {

    if (last_clicked)
      time_since_clicked = event.timeStamp - last_clicked;

    last_clicked = event.timeStamp;

    if (time_since_clicked < 2000)
      return false;

    return true;
  });
};
$('#form-booking').preventDoubleSubmission();

function saveForm(changeStore) {
  if (changeStore === undefined)
    changeStore = false;
  var store_id = $('#booking-store_id').val();
  var custormer_id = $('#mastercustomer-id').val();
  var custormer_form = $('#form-booking').serialize();
  var customer_specify = $('#booking-customer_specify').is(':checked');
  custormer_form = custormer_form + '&custormer_id=' + custormer_id + '&store_id=' + store_id + '&change_store=' + changeStore + '&customer_specify=' + customer_specify;
  pathControler = 'set-session-booking';
  $.ajax({
    url: pathControler,
    type: 'post',
    data: custormer_form,
    success: function (data) {
//            $('#refrest-option').click();
      $.pjax.reload({container: "#p1", async: false});
      $.pjax.reload({container: "#p2", async: false});
      $.pjax.reload({container: "#p8", async: false});
      //
    },
    error: function (jqXHR, textStatus, errorThrown) {
      console.log(errorThrown);
    }
  });
  return false;
}
$(function () {
  saveForm(false);
})
function submitProduct() {
  $.post('bookingproduct', $("#booking-product").serialize())
          .success(function (data) {
            //          console.log(data);
            $('#productModal').modal('toggle');
            //            $('#refrest-coupon').click();
            $.pjax.reload({container: "#p2"});
          });
//          $("#booking-product").ajaxSubmit({url: "/index", type: "post"})
}



function submitOption() {
  $.post('bookingoption', $("#booking-option").serialize())
          .success(function (data) {
            //          console.log(data);
            $('#optionModal').modal('toggle');
            //            $('#refrest-coupon').click();
            $.pjax.reload({container: "#p2"});
          });
//        $('#optionModal').modal('toggle');
//
//        $('#refrest-option').click();

}
function getAllUrlParams(url) {

  // get query string from url (optional) or window
  var queryString = url ? url.split('?')[1] : window.location.search.slice(1);
  // we'll store the parameters here
  var obj = {};
  // if query string exists
  if (queryString) {

    // stuff after # is not part of query string, so get rid of it
    queryString = queryString.split('#')[0];
    // split our query string into its component parts
    var arr = queryString.split('&');
    for (var i = 0; i < arr.length; i++) {
      // separate the keys and the values
      var a = arr[i].split('=');
      // in case params look like: list[]=thing1&list[]=thing2
      var paramNum = undefined;
      var paramName = a[0].replace(/\[\d*\]/, function (v) {
        paramNum = v.slice(1, -1);
        return '';
      });
      // set parameter value (use 'true' if empty)
      var paramValue = typeof (a[1]) === 'undefined' ? true : a[1];
      // (optional) keep case consistent
      paramName = paramName.toLowerCase();
      paramValue = paramValue.toLowerCase();
      // if parameter name already exists
      if (obj[paramName]) {
        // convert value to array (if still string)
        if (typeof obj[paramName] === 'string') {
          obj[paramName] = [obj[paramName]];
        }
        // if no array index number specified...
        if (typeof paramNum === 'undefined') {
          // put the value on the end of the array
          obj[paramName].push(paramValue);
        }
        // if array index number specified...
        else {
          // put the value at that index number
          obj[paramName][paramNum] = paramValue;
        }
      }
      // if param name doesn't exist yet, set it
      else {
        obj[paramName] = paramValue;
      }
    }
  }

  return obj;
}
$(document).on('click', '.btn-ajax-modal', function () {

  var firstname = $('#mastercustomer-first_name').val();
  var lastname = $('#mastercustomer-last_name').val();
  var firstnamekana = $('#mastercustomer-first_name_kana').val();
  var lastnamekana = $('#mastercustomer-last_name_kana').val();
  var mobile = $('#mastercustomer-mobile').val();
  $('#userModal').modal('show')
          .find('#modalContent')
          .load($(this).attr('value'), {firstname: firstname,
            lastname: lastname,
            firstnamekana: firstnamekana,
            lastnamekana: lastnamekana,
            mobile: mobile
          });
});
$(document).on('change', '#booking-store_id', function () {

  saveForm(true);
});
$(document).on('change', '#booking-customer_specify', function () {

  saveForm(false);
});
// click choose product ,coupon
$(document).on('click', '#btn-search-product', function () {
  var custormer_form = $('#form-booking').serialize();
  $('#productModal').modal('show')
          .find('#modalProductContent')
          .load($(this).attr('value'), custormer_form

                  );
});
// click choose option
$(document).on('click', '#btn-search-option', function () {

  $('#optionModal').modal('show')
          .find('#modalOptionContent')
          .load($(this).attr('value'));
});
// click choose staff
$(document).on('click', '#btn-staff-select', function () {
  var store_id = $('#booking-store_id').val();
  var custormer_id = $('#mastercustomer-id').val();
  var custormer_form = $('#form-booking').serialize();
  custormer_form = custormer_form + '&custormer_id=' + custormer_id + '&store_id=' + store_id;
  pathControler = 'set-session-booking';
  $.ajax({
    url: pathControler,
    type: 'post',
    data: custormer_form,
    success: function (data) {
      idUpdate = getAllUrlParams().id;
      if (idUpdate != null) {
        window.location.href = "searchstaffschedule" + "?id=" + idUpdate;
      } else {
        window.location.href = "searchstaffschedule";
      }
      //
    },
    error: function (jqXHR, textStatus, errorThrown) {
      console.log(errorThrown);
    }
  });
  return false;
//            $('#staffModal').modal('show')
//                    .find('#modalStaffContent')
//                    .load($(this).attr('value'));
//            $('.htDimmed').trigger('click');
});
// click choose seat
$(document).on('click', '#btn-seat-select', function () {
  var store_id = $('#booking-store_id').val();
  var custormer_id = $('#mastercustomer-id').val();
  var custormer_form = $('#form-booking').serialize();
  custormer_form = custormer_form + '&custormer_id=' + custormer_id + '&store_id=' + store_id;
  pathControler = 'set-session-booking';
  $.ajax({
    url: pathControler,
    type: 'post',
    data: custormer_form,
    success: function (data) {
      idUpdate = getAllUrlParams().id;
      if (idUpdate != null) {
        window.location.href = "searchseatchedule" + "?id=" + idUpdate;
      } else {
        window.location.href = "searchseatchedule";
      }
      //
    },
    error: function (jqXHR, textStatus, errorThrown) {
      console.log(errorThrown);
    }
  });
  return false;
//            $('#staffModal').modal('show')
//                    .find('#modalStaffContent')
//                    .load($(this).attr('value'));
//            $('.htDimmed').trigger('click');
});
$(document).on('click', '#btn-custormer', function () {

  var custormer_id = $('input[name=customerId]:checked').val();
  if (custormer_id == null)
    return false;
  var store_id = $('#booking-store_id').val();
  var custormer_form = $('#form-booking').serialize();
  custormer_form = custormer_form + '&custormer_id=' + custormer_id + '&store_id=' + store_id;
  pathControler = 'set-session-booking';
  $.ajax({
    url: pathControler,
    type: 'post',
    data: custormer_form,
    success: function (data) {
      $.pjax.reload({container: "#p1"});
      $('#userModal').modal('toggle');
      //
    },
    error: function (jqXHR, textStatus, errorThrown) {
      console.log(errorThrown);
    }
  });
})


$("input[type=radio][name='CustomerStore[news_transmision_flg]'][value='1']").prop("disabled", true);
$("#search-code").click(function (e) {
  getPostcode('#mastercustomer-post_code', '#mastercustomer-address');
});
    