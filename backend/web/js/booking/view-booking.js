$(document).on('click', '.btn-action', function () {

        var btnAction = $(this).attr('value');

        bootbox.dialog({
            title: '<div class="box-header with-border common-box-h4 col-md-8"><h4 class="text-title"><b>'+ '更新確認?'+'</b></h4></div>',
            message: '<span style="color:red;">'+ '予約の状態を更新します。よろしいですか？'+'</span>',  
            closeButton: false,
            buttons: {
                success: {
                    label: 'OK',
                    className: "btn common-button-action text-center btn-confirm ",
                     callback: function () {
                        pathControler = 'view?id='+modelId;
                        $.ajax({
                            url: pathControler,
                            type: 'post',
                            data: {
                              btnAction : btnAction
                            },
                            success: function (data) {
                              
                              if(data == 'false'){
                                $('.btn-cancel').click();
                                yii.warning('担当者（席）を指定してください','注意');
                              }
                              if(btnAction == '03' && data != 'false'){
                                console.log(data);
                                 window.location.href = data;
                              }
                              $('#refrest-status').click();
                                $.pjax.reload({container: "#p3", async: false,timeout : false});

                            },
                            error: function (jqXHR, textStatus, errorThrown) {
                                console.log(errorThrown);
                            }
                        });
                    }

                },
                danger: {
                    label: 'キャンセル',
                    className: "btn btn-default text-center btn-cancel",
                    callback: function () {
                    }
                }

            }
        });

    });
