$(document).on("click", '.common-button-submit', function ()
{
    var radio_rank_condition = $("input[name='MasterRank[rank_condition]']:checked").val();

    checkRankCondition(radio_rank_condition);


});

function checkRankCondition(radio_rank_condition) {
    // ===============================================
    // Get check masterrank-bronze_status : position 1
    var bronze_status = $('#masterrank-bronze_status').prop('checked');
    // Get check masterrank-silver_status : position 2
    var silver_status = $('#masterrank-silver_status').prop('checked');
    // Get check masterrank-gold_status : position 3
    var gold_status = $('#masterrank-gold_status').prop('checked');
    // Get check masterrank-black_status : position 4
    var black_status = $('#masterrank-black_status').prop('checked');
    //alert('bronze_status :'+bronze_status+',silver_status :'+silver_status+',gold_status :'+gold_status+',black_status :'+black_status);
    // ===============================================

    // ===============================================
    // Get Value masterrank-bronze_time_visit,masterrank-bronze_point_total,masterrank-bronze_use_money : position 1
    var bronze_time_visit = $('#masterrank-bronze_time_visit').val();
    var bronze_point_total = $('#masterrank-bronze_point_total').val();
    var bronze_use_money = $('#masterrank-bronze_use_money').val();

    // Get Value masterrank-sliver_time_visit,masterrank-sliver_point_total,masterrank-sliver_use_money : position 2
    var sliver_time_visit = $('#masterrank-sliver_time_visit').val();
    var sliver_point_total = $('#masterrank-sliver_point_total').val();
    var sliver_use_money = $('#masterrank-sliver_use_money').val();
    // ===============================================

    // Get Value masterrank-gold_time_visit,masterrank-gold_point_total,masterrank-gold_use_money : position 3
    var gold_time_visit = $('#masterrank-gold_time_visit').val();
    var gold_point_total = $('#masterrank-gold_point_total').val();
    var gold_use_money = $('#masterrank-gold_use_money').val();
    // ===============================================

    // Get Value masterrank-black_time_visit,masterrank-black_point_total,masterrank-black_use_money : position 4
    var black_time_visit = $('#masterrank-black_time_visit').val();
    var black_point_total = $('#masterrank-black_point_total').val();
    var black_use_money = $('#masterrank-black_use_money').val();
    // ===============================================

    //alert('black_time_visit :'+black_time_visit+',black_point_total :'+black_point_total+',black_use_money :'+black_use_money);

    if (radio_rank_condition == '00') {
        var check_rank_condition00 = checkValueRankCondition00(bronze_status,
                                   silver_status,
                                   gold_status,
                                   black_status,
                                   bronze_time_visit,
                                   bronze_point_total,
                                   bronze_use_money,
                                   sliver_time_visit,
                                   sliver_point_total,
                                   sliver_use_money,
                                   gold_time_visit,
                                   gold_point_total,
                                   gold_use_money,
                                   black_time_visit,
                                   black_point_total,
                                   black_use_money);
        if(check_rank_condition00 == false){
//            $('#masterrank-check_rank_condition').val(1);
//            $('#check_rank_condition_ms').html('').append('<p class="help-block help-block-error">すべて達成の場合、すべての条件を設定してください。</p>');
        }
    }
    if (radio_rank_condition == '01') {

    }
    if (radio_rank_condition == '02') {

    }
}

function checkValueRankCondition00(bronze_status,
                                   silver_status,
                                   gold_status,
                                   black_status,
                                   bronze_time_visit,
                                   bronze_point_total,
                                   bronze_use_money,
                                   sliver_time_visit,
                                   sliver_point_total,
                                   sliver_use_money,
                                   gold_time_visit,
                                   gold_point_total,
                                   gold_use_money,
                                   black_time_visit,
                                   black_point_total,
                                   black_use_money) 
{
    if(bronze_status == true)
    {
        if(bronze_time_visit.length == 0 ||  bronze_point_total.length == 0 || bronze_use_money.length == 0){
            return false;
        }
    }
    if(silver_status == true)
    {
        if(sliver_time_visit.length == 0 ||  sliver_point_total.length == 0 || sliver_use_money.length == 0){
            return false;
        }
    }
    if(gold_status == true)
    {
        if(gold_time_visit.length == 0 ||  gold_point_total.length == 0 || gold_use_money.length == 0){
            return false;
        }
    }
    if(black_status == true)
    {
        if(black_time_visit.length == 0 ||  black_point_total.length == 0 || black_use_money.length == 0){
            return false;
        }
    }
    return true;
}
