$(document).on("change", '.type_question', function ()
{
    var selectedValue = parseInt(jQuery(this).val());

    var key = $(this).attr("key");

    if (selectedValue === 2) {
        $('[name*="Checklist[content_table][' + key + ']"]').html('');
        addQuestionOption1(key);
    } else {
        $('[name*="Checklist[content_table][' + key + ']"]').html('');
        addAttentionClickChange(key);
    }

});
$(document).on("keyup", '[name*="Question"]', function ()
{
    var key = $(this).attr("key");

    $('[name*="count_text[' + key + ']"]').text($(this).val().length + '/300');

});

// Load _attention (注意事項)
function addAttention(key) {

    var $table = $('[name*="Checklist[content_table][' + key + ']"]');
    $table.html('<div class="col-md-12">' +
            '<div class="col-md-1 font_label">' +
            'Q' +
            '</div>' +
            '<div class="col-md-10 notice_content_store_ct">' +
            '<textarea class="form-control area_question" name="Question[' + key + '][notice_content]" maxlength="300" rows="4" key="' + key + '"></textarea>' +
            '</div>' +
            '<div class="col-md-1 font_label">' +
            '<p id="count_text" name="count_text[' + key + ']">0/300<p>' +
            '</div>' +
            '</div>');
    $table.find('[name*="Question[' + key + '][notice_content]"]').attr('name', "Question[" + key + '][notice_content]').attr('key', key);
    $table.find('[name*="count_text"]').attr('name', 'count_text[' + key + ']');
    $table.find('[name*="count_text[' + key + ']"]').text('0/300');
}

function addAttentionClickChange(key) {

    var $table = $('[name*="Checklist[content_table][' + key + ']"]');
    $table.html('<div class="col-md-12">' +
            '<div class="col-md-1 font_label">' +
            'Q' +
            '</div>' +
            '<div class="col-md-10 notice_content_store_ct">' +
            '<textarea class="form-control area_question" name="Question[' + key + '][notice_content]" maxlength="300" rows="4" key="' + key + '"></textarea>' +
            '</div>' +
            '<div class="col-md-1 font_label">' +
            '<p id="count_text" name="count_text[' + key + ']">0/300<p>' +
            '</div>' +
            '</div>');
    $table.find('[name*="Question[' + key + '][notice_content]"]').attr('name', "Question[" + key + '][notice_content]').attr('key', key);
    $table.find('[name*="count_text"]').attr('name', 'count_text[' + key + ']');
    $table.find('[name*="count_text[' + key + ']"]').text('0/300');
}

// Load _attention (注意事項)
function addAttentionClickAdd(key) {

    var idStore = GetURLParameter('id');

    // if (typeof (idStore) !== "undefined") {
    var $table = $('[name*="Checklist[content_table][' + key + ']"]');
    $table.html('<div class="col-md-12">' +
            '<div class="col-md-1 font_label">' +
            'Q' +
            '</div>' +
            '<div class="col-md-10 notice_content_store_ct">' +
            '<textarea class="form-control area_question" name="Question[' + key + '][notice_content]" maxlength="300" rows="4" key="' + key + '"></textarea>' +
            '</div>' +
            '<div class="col-md-1 font_label">' +
            '<p id="count_text" name="count_text[' + key + ']">0/300<p>' +
            '</div>' +
            '</div>');
    $table.find('[name*="Question[' + key + '][notice_content]"]').attr('name', "Question[" + key + '][notice_content]').attr('key', key);
    $table.find('[name*="count_text"]').attr('name', 'count_text[' + key + ']');
    $table.find('[name*="count_text[' + key + ']"]').text('0/300');
    //}
}

// Load _question (質問事項)
function addQuestionOption1(key) {


    var $table = $('[name*="Checklist[content_table][' + key + ']"]');
    $table.html('<div class="col-md-12">'
            + '<div class="col-md-12"><div class="col-md-1"></div><div class="col-md-12"><div class="msg_error col-md-8" style="color: #dd4b39;margin-bottom: 1em;" name="Error[0]"></div></div></div>'
            + '<div class="col-md-1">'

            + '</div>'
            + '<div class="col-md-4 option_store_st">'
            + '<select class="form-control optiont_select" name="Question[0][option]" key="0">'
            + '<option value="1" selected>一つのみ選択</option>'
            + '<option value="2">複数選択</option>'
            + '<option value="3">フリー入力</option>'
            + '</select>'
            + '</div>'
            + '<div name="CountAnswer[0]">'

            + '</div>'
            + '</div>'
            + '<div class="col-md-12">'
            + '<div class="col-md-1 font_label" style="padding-left: 2.2em;    margin-top: 1em;">'
            + 'Q'
            + '</div>'
            + '<div class="col-md-11 question_content_store_ct" style="margin-bottom: 0.5em;">'
            + '<input type="text" class="form-control ip_question" name="Question[question_content][0]" maxlength="200" key="0" style="margin-top: 0.5em;">'
            + '</div>'
            + '</div>'
            + '<div class="col-md-12 list_answer" name="ListAnswer[0]">'

            + '</div>');
    $table.find('[name*="Question[question_content]"]').attr('name', "Question[" + key + '][question_content]');
    $table.find('[name*="Question[0][option]"]').attr('name', "Question[" + key + '][option]').attr('key', key);
    $table.find('[name*="QuestionAnswer[content]"]').attr('name', "QuestionAnswer[content][" + key + ']');
    $table.find('[name*="ListAnswer"]').attr('name', "ListAnswer[" + key + ']');
    $table.find('[name*="CountAnswer"]').attr('name', "CountAnswer[" + key + ']');

    AddInputAnswer(key, 2);

}

function addQuestionOption2(key) {


    var $table = $('[name*="Checklist[content_table][' + key + ']"]');
    $table.html('<div class="col-md-12">'
            + '<div class="col-md-12"><div class="col-md-1"></div><div class="col-md-12"><div class="msg_error col-md-8" style="color: #dd4b39;margin-bottom: 1em;" name="Error[0]"></div></div></div>'
            + '<div class="col-md-1">'

            + '</div>'
            + '<div class="col-md-4 option_store_ct_sl2">'
            + '<select class="form-control optiont_select" name="Question[0][option]" key="0">'
            + '<option value="1" >一つのみ選択</option>'
            + '<option value="2" selected>複数選択</option>'
            + '<option value="3">フリー入力</option>'
            + '</select>'
            + '</div>'
            + '<div name="CountAnswer[0]">'

            + '</div>'
            + '</div>'
            + '<div class="col-md-12">'
            + '<div class="col-md-1 font_label" style="padding-left: 2.2em;    margin-top: 1em;">'
            + 'Q'
            + '</div>'
            + '<div class="col-md-11 question_content_store_ct" style="margin-bottom: 0.5em;">'
            + '<input type="text" class="form-control ip_question" name="Question[question_content][0]" maxlength="200" key="0" style="margin-top: 0.5em;">'
            + '</div>'
            + '</div>'
            + '<div class="col-md-12 list_answer" name="ListAnswer[0]">'

            + '</div>');
    $table.find('[name*="Question[question_content]"]').attr('name', "Question[" + key + '][question_content]');
    $table.find('[name*="Question[0][option]"]').attr('name', "Question[" + key + '][option]').attr('key', key);
    $table.find('[name*="QuestionAnswer[content]"]').attr('name', "QuestionAnswer[content][" + key + ']');
    $table.find('[name*="ListAnswer"]').attr('name', "ListAnswer[" + key + ']');
    $table.find('[name*="CountAnswer"]').attr('name', "CountAnswer[" + key + ']');
    $table.find('[name*="Error"]').attr('name', "Error[" + key + ']');

    AddInputAnswer(key, 2);

}

// Chosse many question

$(document).on("change", '.optiont_select', function ()
{

    var selectedValue = parseInt(jQuery(this).val());


    var key = $(this).attr("key");


    if (selectedValue === 1) {

        addQuestionOption1(key);

    } else if (selectedValue === 3) {

        $('[name*="CountAnswer[' + key + ']"]').html('');
        $('[name*="ListAnswer[' + key + ']"]').html('');

        $('[name*="Question[' + key + '][question_content]"]').val('');

    } else if (selectedValue === 2) {
        //addQuestion(key);
        addQuestionOption2(key);

        $('[name*="CountAnswer[' + key + ']"]').html(
                '<div class="col-md-3 label-margin" style="padding-left: 4.5em;">最大回答数</div>'
                + '<div class="col-md-3">'
                + '<input style="text-align: right;" onkeypress="return isNumeric(event)" oninput="maxLengthCheck(this)" type = "number" maxlength = "2" min = "1" max = "99" class="form-control" id="count_answer" name="Question[' + key + '][CountAsw]" key = "' + key + '" value="1"></div>');

        //AddInputAnswer(key, 2);
    }

});


// Input Answer
function AddInputAnswer(key, count) {

    var html = '';

    for (var i = 0; i < count; i++) {

        html += '<div class="col-md-6" style="margin-bottom: 1em;">'
                + '<div class="col-md-1 label-margin">A' + (i + 1) + '</div>'
                + '<div class="col-md-10 answer_store_ct_10">'
                + '<input maxlength="100"  class="form-control ip_answer" name="Question[' + key + '][answer][' + i + ']" key = "' + key + '" data_bin= "' + key + '" data-position=' + (i + 1) + ' >'
                + '</div>'
                + '</div>';
    }
    $('[name*="ListAnswer[' + key + ']"]').html('');
    $('[name*="ListAnswer[' + key + ']"]').append(html);

}

$(document).on("focus", '.ip_answer', function ()
{

    var selectedValue = parseInt(jQuery(this).val());

    // var count_answer = $("[name*="").length;

    var key = $(this).attr("key");


    var html = '';

    // Count Input Answer
    var count_answer = $('[data_bin*=' + key + ']').length;

    // Slelect Droplist Option
    //var selectedValue = $('[name*="Question[option]"]').val();
    var option = $('[name*="Question[' + key + '][option]"] option:selected').val();

//    alert(selectedValue);

    // Position Forcus
    var position_answer = $(this).data("position");

    //console.log('key:' + key + ',count_answer :' + count_answer + ',position_answer :' + position_answer + ',count_answer:' + count_answer + ',option' + option);

    if ((count_answer % 2) === 0 && (position_answer % 2) === 0 && (position_answer == count_answer) && (option == 2 || option == 1)) {

        var count = count_answer + 2;

        for (var i = count_answer; i < count; i++) {
            html += '<div class="col-md-6" style="margin-bottom: 1em;">'
                    + '<div class="col-md-1 label-margin">A' + (i + 1) + '</div>'
                    + '<div class="col-md-10 answer_store_ct_10">'
                    + '<input maxlength="100" type="text" class="form-control ip_answer" name="Question[' + key + '][answer][' + i + ']" key = "' + key + '" data_bin= "' + key + '" data-position=' + (i + 1) + ' >'
                    + '</div>'
                    + '</div>';
        }

        $('[name*="ListAnswer[' + key + ']"]').append(html);
    }
});

$("#back_confirm").on("click", function () {
    CheckHaveQuestion();
//    var idStore = GetURLParameter('id');
//    window.location = window.location.href;
//    var cloneIndex = $(".clonedInput").length - 2;
//
//    if (cloneIndex > 0) {
//
//        for (var i = cloneIndex; i > 0; i--) {
//
//            $('[name*="clonedInput' + i + '"]').remove();

    // $('[name*="Question['+i+'][notice_content]"]').val('');

//        }
//    }

    //$('[name*="Checklist[type]"]').val(1);

});

$("#btn-confirm-question").on("click", function () {

    var cloneIndex = $(".clonedInput").length - 1;

    var Question = [];
    var check_error, show_error, count_answer_check, id_option_db, id_type_db, type_question, display_flg, notice_content, option, question_content, answer, CountAsw, id_checklist, id_question, id_answer;
    var j = 0;
    var key = 0;
    check_error = 0;
    var array_answer = [];
    // Get value of table question
    $(".clonedInput").each(function () {

        if (j > 0) {
            //var key = $(this).attr("key");

            type_question = $(this).find(".type_question").val();

            display_flg = $('[name*="Question[' + key + '][display_flg]"]:checked').val();
            notice_content = $('[name*="Question[' + key + '][notice_content]"]').val();
            show_error = $('[name*="Error[' + key + ']');
            //show_error = $('[name*="Error[' + key + ']').val('');
            //option = $('[name*="Question[' + key + '][option]"]').val();
            option = $(this).find(".optiont_select").val();
            id_checklist = $(this).find(".id_checklist").val();
            id_question = $(this).find(".id_question").val();
            id_answer = $(this).find(".id_answer").val();
            id_type_db = $(this).find(".id_type_db").val();
            id_option_db = $(this).find(".id_option_db").val();
            //  console.log('id_checklist :'+id_checklist+',id_question :'+id_question
            if (type_question == 1) {

                if ($('[name*="Question[' + key + '][notice_content]"]').val().length > 0) {
                    Question[j] = {
                        'id_option_db': id_option_db,
                        'option': '0',
                        'id_type_db': id_type_db,
                        'type_question': type_question,
                        'display_flg': display_flg,
                        'notice_content': notice_content,
                        'id_checklist': id_checklist,
                        'id_question': id_question
                    };
                } else if (id_question !== '0' && ($('[name*="Question[' + key + '][notice_content]"]').val().length == 0)) {

                    Question[j] = {
                        'id_option_db': id_option_db,
                        'option': '0',
                        'id_type_db': id_type_db,
                        'type_question': type_question,
                        'display_flg': display_flg,
                        'notice_content': 'no_data',
                        'id_checklist': id_checklist,
                        'id_question': id_question
                    };
                }

            } else if (type_question == 2) {

                var count_answer = $('[data_bin=' + key + ']').length;
                //alert(count_answer);
                var ct_answer = 0;

                if (option == 1 || option == 2) {
                    //console.log('key :'+key+',count_answer :'+count_answer);
                    count_answer_check = null;

                    array_answer = [];

                    for (var i = 0; i < count_answer; i++) {
                        // console.log('i :'+i+',key :'+key+$('[name*="Question[' + key + '][answer][' + i + ']"]').val());
                        if (i == 0) {
                            //console.log('i :'+i+',key :'+key+$('[name*="Question[' + key + '][answer][' + i + ']"]').val());
                            if ($('[name*="Question[' + key + '][answer][' + i + ']"]').val().length == 0) {
                                answer = 'no_data';
                                //array_answer.push(answer);
                            } else {
                                answer = $('[name*="Question[' + key + '][answer][' + i + ']"]').val();
                                // array_answer.push(answer);
                                count_answer_check = $('[name*="Question[' + key + '][answer][' + i + ']"]').val();
                            }
                            //++ct_answer;
                        } else {
                            if ($('[name*="Question[' + key + '][answer][' + i + ']"]').val().length == 0) {
                                //array_answer.push(answer);
                                answer += ',' + 'no_data';
                            } else {

                                answer += ',' + $('[name*="Question[' + key + '][answer][' + i + ']"]').val();
                                count_answer_check += ',' + $('[name*="Question[' + key + '][answer][' + i + ']"]').val();
                            }

                        }
                        if ($('[name*="Question[' + key + '][answer][' + i + ']"]').val().length == 0) {
                            answer = 'no_data';
                            array_answer.push(answer);
                        } else {
                            answer = $('[name*="Question[' + key + '][answer][' + i + ']"]').val();
                            array_answer.push(answer);
                        }
                        ++ct_answer;
                    }
                }
//                console.log(array_answer);
//                return false;

                if (option == 1) {
                    question_content = $('[name*="Question[' + key + '][question_content]"]').val();
                    if (count_answer_check != null) {
                        $('[name*="Error[' + key + ']').hide();
                        var res = count_answer_check.split(",");

                        if (ct_answer != 0 && question_content.length > 0) {
                            Question[j] = {
                                'id_option_db': id_option_db,
                                'id_type_db': id_type_db,
                                'type_question': type_question,
                                'display_flg': display_flg,
                                'option': option,
                                'question_content': question_content,
                                'answer': array_answer,
                                'id_checklist': id_checklist,
                                'id_question': id_question,
                                'id_answer': id_answer
                            };
                            answer = null;
                        }
                    } else {

                        if (question_content.length != 0) {
                            $('[name*="Error[' + key + ']').show();
                            $('[name*="Error[' + key + ']').text('１つ以上回答を入力してください。');
                            check_error++;
                            return false;
                        }

                    }

                } else if (option == 2) {
                    question_content = $('[name*="Question[' + key + '][question_content]"]').val();
                    if (count_answer_check != null) {
                        CountAsw = $('[name*="Question[' + key + '][CountAsw]"]').val();
                        var res = count_answer_check.split(",");
                        //console.log(res.length);
                        if (res.length < CountAsw || CountAsw == 0) {

                            if (res.length < CountAsw) {
                                $('[name*="Error[' + key + ']').show();
                                $('[name*="Error[' + key + ']').text('最大回答数は質問数以下で入力してください。');
                                check_error++;
                                return false;
                            }
                            if (CountAsw == 0) {
                                $('[name*="Error[' + key + ']').show();
                                $('[name*="Error[' + key + ']').text('0より大きく入力してください。');
                                check_error++;
                                return false;
                            }
                            //exit();
                        } else {
                            $('[name*="Error[' + key + ']').hide();
                            check_error = 0;
                            if (ct_answer != 0 && question_content.length > 0) {
                                Question[j] = {
                                    'id_option_db': id_option_db,
                                    'id_type_db': id_type_db,
                                    'type_question': type_question,
                                    'display_flg': display_flg,
                                    'option': option,
                                    'question_content': question_content,
                                    'answer': array_answer,
                                    'CountAsw': CountAsw,
                                    'id_checklist': id_checklist,
                                    'id_question': id_question,
                                    'id_answer': id_answer
                                };
                                answer = null;
                            }
                        }
                    } else {
                        if (question_content.length != 0) {
                            $('[name*="Error[' + key + ']').show();
                            $('[name*="Error[' + key + ']').text('１つ以上回答を入力してください。');
                            check_error++;
                            return false;
                        }
                    }
                } else if (option == 3) {
                    question_content = $('[name*="Question[' + key + '][question_content]"]').val();

                    if (question_content.length > 0) {
                        Question[j] = {
                            'id_option_db': id_option_db,
                            'id_type_db': id_type_db,
                            'type_question': type_question,
                            'display_flg': display_flg,
                            'option': option,
                            'question_content': question_content,
                            'id_checklist': id_checklist,
                            'id_question': id_question,
                            'id_answer': id_answer
                        };
                    } else if (id_question != '0' && $('[name*="Question[' + key + '][question_content]"]').val().length == 0) {
                        Question[j] = {
                            'id_option_db': id_option_db,
                            'id_type_db': id_type_db,
                            'type_question': type_question,
                            'display_flg': display_flg,
                            'option': option,
                            'question_content': 'no_data',
                            'id_checklist': id_checklist,
                            'id_question': id_question,
                            'id_answer': id_answer
                        };
                    }

                }

            }
            key++;
        }
        j++;
    });

    //alert(JSON.stringify(Question, 'Question', " "));
    // exit();
    $('#value_textarea_qs').val('');
    //Question = cleanArray(Question);
    $('#value_textarea_qs').val(JSON.stringify(Question, 'Question', " "));
    //alert('Result :'+JSON.stringify(Question, 'Question', " "));
    CheckHaveQuestion();
//    console.log(JSON.stringify(Question, 'Question', " "));
//    return false;
    if (check_error == 0) {
        back();
    }
});

function cleanArray(actual) {
    var newArray = new Array();
    for (var i = 0; i < actual.length; i++) {
        if (actual[i]) {
            newArray.push(actual[i]);
        }
    }
    return newArray;
}
// Check Have Question
function CheckHaveQuestion() {

    var flag_qs = 0;

    $(".area_question").each(function () {
        if ($(this).val().length > 0) {
            flag_qs = 1;
            DisplayHaveQuestion(flag_qs);
            return;
        }
    });

    $(".ip_question").each(function () {
        if ($(this).val().length > 0) {
            flag_qs = 1;
            DisplayHaveQuestion(flag_qs);
            return;
        }
    });

    $(".ip_answer").each(function () {
        if ($(this).val().length > 0) {
            flag_qs = 1;
            DisplayHaveQuestion(flag_qs);
            return;
        }
    });

    DisplayHaveQuestion(flag_qs);

    return flag_qs;
}

function DisplayHaveQuestion(flag_qs) {

    if (flag_qs == 1) {
        $("#check_have_question").html('あり');
    } else {
        $("#check_have_question").html('なし');
    }
}
$(document).ready(function () {
    CheckHaveQuestion();
});
// =====================================
// Wizard step Start
// =====================================

$(document).ready(function () {
    //Initialize tooltips
    $('.nav-tabs > li a[title]').tooltip();

    //Wizard
    $('a[data-toggle="tab"]').on('show.bs.tab', function (e) {

        var $target = $(e.target);

        if ($target.parent().hasClass('disabled')) {
            return false;
        }
    });

    $(".next-step").click(function (e) {

        var $active = $('.wizard .nav-tabs li.active');
        $active.next().removeClass('disabled');
        nextTab($active);

    });

    $(".prev-step-back").click(function (e) {

        var $active = $('.wizard .nav-tabs li.active');
        prevTab($active);

    });
});

function nextTab(elem) {
    $(elem).next().find('a[data-toggle="tab"]').click();
}
function prevTab(elem) {
    $(elem).prev().find('a[data-toggle="tab"]').click();
}

function back() {
    //$(".prev-step").click(function (e) {

    var $active = $('.wizard .nav-tabs li.active');
    prevTab($active);

    // });
}


//according menu

$(document).ready(function ()
{
    //Add Inactive Class To All Accordion Headers
    $('.accordion-header').toggleClass('inactive-header');

    //Set The Accordion Content Width
    var contentwidth = $('.accordion-header').width();
    $('.accordion-content').css({});

    //Open The First Accordion Section When Page Loads
    $('.accordion-header').first().toggleClass('active-header').toggleClass('inactive-header');
    $('.accordion-content').first().slideDown().toggleClass('open-content');

    // The Accordion Effect
    $('.accordion-header').click(function () {
        if ($(this).is('.inactive-header')) {
            $('.active-header').toggleClass('active-header').toggleClass('inactive-header').next().slideToggle().toggleClass('open-content');
            $(this).toggleClass('active-header').toggleClass('inactive-header');
            $(this).next().slideToggle().toggleClass('open-content');
        } else {
            $(this).toggleClass('active-header').toggleClass('inactive-header');
            $(this).next().slideToggle().toggleClass('open-content');
        }
    });

    return false;
});

// =====================================
// Wizard step End
// =====================================
// **********************
// Get parameter url
// **********************
function GetURLParameter(sParam)
{
    var sPageURL = window.location.search.substring(1);
    var sURLVariables = sPageURL.split('&');
    for (var i = 0; i < sURLVariables.length; i++)
    {
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] === sParam)
        {
            return sParameterName[1];
        }
    }
}
// **********************
// End Get parameter url
// **********************

function maxLengthCheck(object) {
    if (object.value.length > object.maxLength)
        object.value = object.value.slice(0, object.maxLength)
}

function isNumeric(evt) {
    var theEvent = evt || window.event;
    var key = theEvent.keyCode || theEvent.which;
    key = String.fromCharCode(key);
    var regex = /[0-9]|\./;
    if (!regex.test(key)) {
        theEvent.returnValue = false;
        if (theEvent.preventDefault)
            theEvent.preventDefault();
    }
}
// Dont Enter Submit Form
//$(document).ready(function () {
//    $(window).keydown(function (event) {
//        if (event.keyCode == 13) {
//            event.preventDefault();
//            return false;
//        }
//    });
//});