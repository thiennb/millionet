$(document).on('click', '[name="MasterCoupon[benefits_content]"]', function () {
    var self = $(this).val();
    checkHiden(self);
});
//$("#mastercoupon-title").focusout(function() {
//   var self = $(this).val();
//   $("#mastercoupon-title_hidden").val(self);
//   alert(self);
//});
// SET DEFAUL DISABLED coupon_jan_code
$(document).ready(function () {

    //$("#mastercoupon-benefits_content").is(":checked").valueOf();
    //$('input[name=MasterCoupon[benefits_content]]:checked').val();
    //var name = $('input[name=MasterCoupon[benefits_content]]:checked').val();

    var check_radio = $("input[name='MasterCoupon[benefits_content]']:checked").val();
    checkHiden(check_radio);
    $("#btn-confirm-preview").attr("disabled", true).addClass('disabled');

    // Check radio show_coupon Preview
    var value_show_coupon = $('input[name="MasterCoupon[show_coupon][]"]:checked').map(function () {
        return $(this).val();
    });
    if (value_show_coupon.length >= 2) {
        //alert(value_show_coupon.length);
        var index, len, value = null;

        for (index = 0, len = value_show_coupon.length; index < len; ++index) {
            if (value_show_coupon[index] == '01') {
                $("#btn-confirm-preview").removeClass('disabled').removeAttr("disabled");
            }
        }
    }
    
    if (value_show_coupon.length == 1) {
        if (value_show_coupon[0] == '01') {
            $("#btn-confirm-preview").removeClass('disabled').removeAttr("disabled");
        }
    }
});
$(document).on('click', '[name="MasterCoupon[show_coupon][]"]', function () {
    var self = $(this).val();
    
    if ($(this).is(":checked")) {
        if (self == '01') {
             $("#btn-confirm-preview").removeClass('disabled').removeAttr("disabled");
        }
       
    } else {
        if (self == '01') {
            $("#btn-confirm-preview").attr("disabled", true).addClass('disabled');
        }
        
    }
});



function checkHiden(self) {



    if (self === '00') {

        $("#mastercoupon-discount_yen").removeClass('disabled').removeAttr("disabled");
        $("#mastercoupon-discount_percent").attr("disabled", true).addClass('disabled');
        $("#mastercoupon-discount-price_set").attr("disabled", true).addClass('disabled');
        $("#mastercoupon-discount_drink_eat").attr("disabled", true).addClass('disabled');
        $("#mastercoupon-discount_price_set_tax_type").attr("disabled", true).addClass('disabled');
        $("#mastercoupon-discount_price_set").attr("disabled", true).addClass('disabled');
        $("#mastercoupon-discount_drink_eat_tax_type").attr("disabled", true).addClass('disabled');
        $("#discount_price_set").attr("disabled", true).addClass('disabled');
        // Clear value
        $("#mastercoupon-discount_percent").val('');
        $("#mastercoupon-discount-price_set").val('');
        $("#mastercoupon-discount_drink_eat").val('');
        $("#mastercoupon-discount_drink_eat_tax_type").val('00');
        $("#mastercoupon-discount_price_set_tax_type").val('00');
        $("#mastercoupon-discount_price_set").val('');

        $(".field-mastercoupon-discount_percent").removeClass("has-error");
        $(".field-mastercoupon-discount_percent .help-block-error").text('');

        $(".field-mastercoupon-discount_price_set").removeClass("has-error");
        $(".field-mastercoupon-discount_price_set .help-block-error").text('');

        $(".field-mastercoupon-discount_drink_eat").removeClass("has-error");
        $(".field-mastercoupon-discount_drink_eat .help-block-error").text('');


    }
    if (self === '01') {
        $("#mastercoupon-discount_percent").removeClass('disabled').removeAttr("disabled");
        $("#mastercoupon-discount_yen").attr("disabled", true).addClass('disabled');
        $("#mastercoupon-discount_price_set").attr("disabled", true).addClass('disabled');
        $("#mastercoupon-discount_drink_eat").attr("disabled", true).addClass('disabled');
        $("#mastercoupon-discount_price_set_tax_type").attr("disabled", true).addClass('disabled');
        $("#mastercoupon-discount_drink_eat_tax_type").attr("disabled", true).addClass('disabled');
        // Clear value
        $("#mastercoupon-discount_yen").val('');
        $("#mastercoupon-discount_price_set").val('');
        $("#mastercoupon-discount_drink_eat").val('');
        $("#mastercoupon-discount_drink_eat_tax_type").val('00');
        $("#mastercoupon-discount_price_set_tax_type").val('00');

        $(".field-mastercoupon-discount_yen").removeClass("has-error");
        $(".field-mastercoupon-discount_yen .help-block-error").text('');

        $(".field-mastercoupon-discount_price_set").removeClass("has-error");
        $(".field-mastercoupon-discount_price_set .help-block-error").text('');

        $(".field-mastercoupon-discount_drink_eat").removeClass("has-error");
        $(".field-mastercoupon-discount_drink_eat .help-block-error").text('');

    }
    if (self === '02') {
        $("#mastercoupon-discount_price_set").removeClass('disabled').removeAttr("disabled");
        $("#mastercoupon-discount_price_set_tax_type").removeClass('disabled').removeAttr("disabled");
        $("#mastercoupon-discount_yen").attr("disabled", true).addClass('disabled');
        $("#mastercoupon-discount_percent").attr("disabled", true).addClass('disabled');
        $("#mastercoupon-discount_drink_eat").attr("disabled", true).addClass('disabled');
        $("#mastercoupon-discount_drink_eat_tax_type").attr("disabled", true).addClass('disabled');
        // Clear value
        $("#mastercoupon-discount_yen").val('');
        $("#mastercoupon-discount_percent").val('');
        $("#mastercoupon-discount_drink_eat").val('');
        $("#mastercoupon-discount_drink_eat_tax_type").val('00');
        //$("#mastercoupon-discount_price_set_tax_type").val('00');

        $(".field-mastercoupon-discount_yen").removeClass("has-error");
        $(".field-mastercoupon-discount_yen .help-block-error").text('');

        $(".field-mastercoupon-discount_percent").removeClass("has-error");
        $(".field-mastercoupon-discount_percent .help-block-error").text('');

        $(".field-mastercoupon-discount_drink_eat").removeClass("has-error");
        $(".field-mastercoupon-discount_drink_eat .help-block-error").text('');

    }
    if (self === '03') {

        $("#mastercoupon-discount_drink_eat").removeClass('disabled').removeAttr("disabled");
        $("#mastercoupon-discount_drink_eat_tax_type").removeClass('disabled').removeAttr("disabled");
        $("#mastercoupon-discount_yen").attr("disabled", true).addClass('disabled');
        $("#mastercoupon-discount_percent").attr("disabled", true).addClass('disabled');
        $("#mastercoupon-discount_price_set").attr("disabled", true).addClass('disabled');
        $("#mastercoupon-discount_price_set_tax_type").attr("disabled", true).addClass('disabled');
        // Clear value
        $("#mastercoupon-discount_yen").val('');
        $("#mastercoupon-discount_percent").val('');
        $("#mastercoupon-discount_price_set").val('');
        // $("#mastercoupon-discount_drink_eat_tax_type").val('00');
        $("#mastercoupon-discount_price_set_tax_type").val('00');

        $(".field-mastercoupon-discount_yen").removeClass("has-error");
        $(".field-mastercoupon-discount_yen .help-block-error").text('');

        $(".field-mastercoupon-discount_percent").removeClass("has-error");
        $(".field-mastercoupon-discount_percent .help-block-error").text('');

        $(".field-mastercoupon-discount_price_set").removeClass("has-error");
        $(".field-mastercoupon-discount_price_set .help-block-error").text('');
    }
}
;


// ===========================================
// Onclick display popup
// ===========================================
function init_product_slect_by_idstrore(e) {
    //   alert('click');
    e.preventDefault();
    // var pathname = '/admin/master-coupon/loadproductbyidstore';
    var idStore = $('#mastercoupon-store_id').val();
    var role = $('#mastercoupon-_role').val();
    //alert(role);
    if((role != 2 || role != 3) && idStore == '' ){
        var idStoreComon = $('#mastercoupon-id_store_comon').val();
        idStore = idStoreComon;
    }
    
    $('.field-mastercoupon-error_product_tax .help-block-error').text('');

    if (idStore) {
        id = idStore;

        var pathname = '/admin/master-coupon/loadproductbyidstore';
        $.ajax({
            url: pathname,
            type: 'post',
            data: {
                idStore: idStore
            },
            success: function (data) {
                $("#_product").html('');
                $("#_product").append(data);
                $('#productModal').modal('show');

                var $options = $('.option-checkbox');
                var $option_hidden = $('#option_hidden').val();
                var $option_hidden = ($option_hidden) ? $option_hidden.split(",") : $option_hidden;
                $options.each(function () {
                    if ($.inArray($(this).val(), $option_hidden) >= 0) {
                        $(this).prop('checked', true);
                    } else {
                        $(this).prop('checked', false);
                    }
                });
            }
        });

    } else {
       // alert("No Select Store");
    }
}
// ===========================================
// End Onclick display popup
// ===========================================
$('#refresh-data-modal').on('click', function (e) {
    e.preventDefault();
    var $option_hidden = $('#option_hidden').val();
    $.pjax({
        type: 'POST',
        data: {
            option_hidden: $option_hidden
        },
        container: '#coupon_table',
        fragment: '#coupon_table',
        url: $(this).attr('href'),
    });
});
// ============================================
// Click check box expire_auto_set start
// ============================================
$('#mastercoupon-expire_auto_set').click(function (event) {
    if (this.checked) {
        $('#mastercoupon-expire_date').val('');
        //$('#mastercoupon-expire_date').prop('value', null);
        $('#mastercoupon-expire_date').addClass("form-control");
        $('#mastercoupon-expire_date').attr("disabled", true).addClass('disabled');

    } else {
        $("#mastercoupon-expire_date").removeClass('disabled').removeAttr("disabled");
    }
});
// ============================================
// Click check box expire_auto_set end
// ============================================