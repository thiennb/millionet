//    $('#btn-confirm').click(function(e){
//        if($("#mastertax-id").valid()){
//            $('#userModal').modal('show')
//        }else{
//            return;
//        }
////        e.preventDefault();
//    });
//
//    $("#mastertax-id").validate({
//        rules: {
//            'MasterTax[name]': {required: true},
//        },
//        messages: {
//            'MasterTax[name]':{required: "MESSAGE.INPUT_REQUIRED"},
//        },
//        onkeyup: false,
//        onchange: false,
//        onclick: false
//    });
    
    $('.multiple-input-list').load(".input-decimal", function(){
        $(this).numeric({ decimal : ".",  negative : false, scale: 3 });
    });
        
    $('.js-input-plus').each(function(){
        $(this).parent().html('<div class="btn multiple-input-list__btn js-input-remove common-button-action">削除</div>');
    });
    $('.multiple-input-list').find('.js-input-remove').each(function(){
        var $check = false;
        var $input_text = $(this).parent().parent().find('input[type=text]');
        var $input_check_box = $(this).parent().parent().find('input[type=checkbox]');
        $input_text.each(function(){
            if($(this).val() != ''){
                $check = true;
            }
        });
        $input_check_box.each(function(e){
            if($(this).prop('checked') == true){
                $check = true;
            }
        });
        if($check){
            $(this).show();
        }else{
            $(this).hide();
        }
    });
    function check_js_input_remove(){
        $('.multiple-input-list').find('.js-input-remove').each(function(){
            var $check = false;
            var $input_text = $(this).parent().parent().find('input[type=text]');
            var $input_check_box = $(this).parent().parent().find('input[type=checkbox]');
            $input_text.each(function(){
                if($(this).val() != ''){
                    $check = true;
                }
            });
            $input_check_box.each(function(e){
                if($(this).prop('checked') == true){
                    $check = true;
                }
            });
            if($check){
                $(this).show();
            }else{
                $(this).hide();
            }
        });
    }
    $('.multiple-input-list').on('change','tr td input[type=checkbox]',function(){
        check_js_input_remove();
        
    });
    $('.multiple-input-list').on('keyup','tr td input[type=text]',function(){
        check_js_input_remove();
        
    });
    
    $('.multiple-input-list').on('change','tr td input[type=text]',function(){
        check_js_input_remove();
        
    });
    
    $('.multiple-input-list').on('keyup','tr:last td input',function(){
        $('#js-input-plus').click();
        check_js_input_remove();
    });
    $('.multiple-input-list').find('tr').find('th').last().html('<div id="js-input-plus" class="btn multiple-input-list__btn js-input-plus btn btn-default" style="display:none;"><i class="glyphicon glyphicon-plus"></i></div>');
//    $(document).on('submit', function () {
//        var $check = true;
//        var $last_tr = $('.multiple-input-list').find('tr').last().find('td').find('input[type="text"]');
//        $last_tr.each(function(){
//            if($(this).val() != ""){
//                $check = false;
//            }
//        });
//        if($check){
//            $('.multiple-input-list').find('tr').last().remove();
//        }
//    });