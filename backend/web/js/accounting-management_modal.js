/*Point status:
 * 0: Use(使用)
 * 1: Increase(増加)
 * 2: Reduction(減少)
*/
//************* Modal Point ***************
$(document).on('click', '#btn_point', function () {
    var $customer_id = $('#customerId').val();
    if(!$customer_id) return false;
    var $store_id = $('select[name="store_id"]').val();
    if(!$store_id) return false;
    if(return_good_status == 1) return false;
    
    $('#point_modal').modal('show')
            .find('#point_modal_content')
            .load($(this).attr('value'), {
                customer_id: $customer_id,
                store_id:    $store_id
            });
});
/* $(document).on('change', '#point_modal_content #point_select', function () {
    checkPointSelect();
    return false;
});

$(document).on('change', '#point_modal_content #point_input', function () {
    checkPointSelect();
    return false;
});
*/

$(document).on('click', '#btn_point_modal', function () {
    if(!checkPointSelect()) return false;
    $('#point_modal').modal('hide');
    var $id = 'pt',
        $jan_code   =   'pt',
        $point_input  = $('#point_modal_content #point_input').val(),
        $text_type_point  = SETTING.POINT,
        $type_use = $('#point_modal_content #point_select').val(),
        $readonly = 'readonly',
        $point_status = $('#point_modal_content #point_select').val();
        $point_input = ($point_input)?$point_input:0;
        if($type_use === '2'){
            $type_use = '減少';
        }else if($type_use === '1'){
            $type_use = '増加';
        }else{
            $type_use = '使用';
        }
    var $point_tr = $('#detail_products').find('tr[point_id="'+$id+'"][jan_code="'+$jan_code+'"][point_status="'+$point_status+'"]');
    if(!$point_tr.text()){
        //insert row point
        insert_row_point($id, $text_type_point, $jan_code, $type_use, $point_input, $readonly, $point_status);
    }else{
        $point_tr.find('td #point_input_1').text($point_input+' P');
        $point_tr.find('td #point_input_2').text($point_input+' P');
        $point_tr.attr('point_input',$point_input);
    }
    
    updateReceipt();
});

function checkPointSelect() {
    var $point_input = $('#point_modal_content #point_input').val();
    if(!$point_input){
        $('#point_modal_content #point_input_error').text(MESSAGE.ERR_CHARGE_INPUT_EMP);
        return false;
    }
    var $point_selects = $('#point_modal_content #point_select').val();
    if($point_selects != 0) {
//        $('#point_modal_content #point_input_error').text(''); 
        return true; 
    }
    
    var $current_point = $('#point_modal_content #total_point_hd').val(),
        $input_point = $('#point_modal_content #point_input').val();
    if(parseInt($current_point) < parseInt($input_point)){
        $('#point_modal_content #point_input_error').text(MESSAGE.CHECK_POINT_USE);
        return false;
    }
    /*
    else{
        $('#point_modal_content #point_input_error').text('');
    }*/
    
    return true;
}

function insert_row_point($id, $text_type_point, $jan_code, $type_use, $point_input, $readonly, $point_status){
    var $row = '';
    $row = '<tr point_id="'+$id+'" jan_code="'+$jan_code+'" point_status="'+$point_status+'" point_input="'+$point_input+'"';
    $row += '>';
    $row += '<td class="col_action"><button type="button" class="btn btn-danger btn-add btn-remove btn-remove-product"><i class="fa fa-times-circle" aria-hidden="true"></i></button></td>';
    $row += '<td class="col_type text-left td-product">' + $text_type_point + '</td>';
    $row += '<td class="col_name text-left td-product">' + $type_use + '</td>';
    $row += '<td class="col_private_setting text-left td-product"></td>';
    $row += '<td class="col_price td-product unit-price"><span class="price" id="point_input_1">' + $point_input + ' P</span></td>';
    $row += '<td class=""><input type="number" name="quantity" min="1" value="1" '+$readonly+'></td>';
    $row += '<td class="td-product total-price"><span class="price" id="point_input_2">' + $point_input + ' P</span></td>';
    $row += '<td class="">' + textListStaff + '</td>';
    $row += '</tr>';

    $('#detail_products').append($row);
}
//************* Modal Point ***************

//************* Modal Ticket **************
$(document).on('click', '#btn_ticket', function () {
    var $have_ticket = $(this).attr('have_ticket');
    if($have_ticket == 'false') return false;
    var $customer_id = $('#customerId').val();
    if(!$customer_id) return false;
    var $store_id = $('select[name="store_id"]').val();
    if(!$store_id) return false;
    if(return_good_status == 1) return false;
    
    $('#ticket_modal').modal('show')
            .find('#ticket_modal_content')
            .load($(this).attr('value'), {
                customer_id: $customer_id,
                store_id:    $store_id
            });
});

$ticket_balance = '';
$product_ticket = '';
$(document).on('change', '#ticket_modal_content #ticket_select', function () {
    $ticket_balance = $('option:selected', this).attr('ticket_balance');
    $product_ticket = $('option:selected', this).attr('list_product');
    $("#ticket_modal_content #ticket_balance").text($ticket_balance);

});

$(document).on('click', '#btn_ticket_modal', function () {
    if(!checkTicketUse()) return false;
    $('#ticket_modal').modal('hide');
    var $ticket_selected = $('#ticket_modal_content #ticket_select').find('option:selected');
    var $id = $ticket_selected.attr('id'),
        $jan_code  = $ticket_selected.attr('ticket_jan_code'),
        $ticket_balance  = $ticket_selected.attr('ticket_balance'),
        $ticket_name  = $ticket_selected.attr('ticket_name'),
        $text_type_ticket  = '回数券',
        $ticket_price = $ticket_selected.attr('price'),
        $readonly = 'readonly',
        $input_point_ticket = $('#ticket_modal_content #input_point_ticket').val(),
        $list_product = $ticket_selected.attr('list_product');
    var $ticket_tr = $('#detail_products').find('tr[ticket_id="'+$id+'"][jan_code="'+$jan_code+'"]');
    if(!$ticket_tr.text()){        
        //insert row ticket
        insert_row_ticket($id, $jan_code, $ticket_balance, $ticket_name, $text_type_ticket, $ticket_price, $readonly, $input_point_ticket,$list_product);
    }else{
        $ticket_tr.attr('input_point_ticket',$input_point_ticket);
        $ticket_tr.attr('list_product',$list_product);
        $ticket_tr.find('input[name="quantity"]').val($input_point_ticket);
    }
    updateReceipt();
});

function checkTicketUse() {
    
    $exists = 0;
    $('#detail_products').find('tr').each(function (index, element){
        jancode = $(element).attr('jan_code');
        if(jancode.substring(0,2) == '21'){
            if($product_ticket.indexOf(jancode) > -1){
                $exists = 1; 
            }
        }
    });
    if($exists === 0){
        $('#ticket_modal_content #input_point_error').text(MESSAGE.ERR_TICKET_PRODUCT_EMPTY);
        return false;
    }
    var $ticket_balance  = $('#ticket_modal_content #ticket_select').find('option:selected').attr('ticket_balance');
    var $value = $('#ticket_modal_content #input_point_ticket').val();
    if(!$value){
        $('#ticket_modal_content #input_point_error').text(MESSAGE.ERR_CHARGE_INPUT_EMP);
        return false;
    }
    if(parseInt($ticket_balance) < parseInt($value)){
        $('#ticket_modal_content #input_point_error').text(MESSAGE.CHECK_TICKET_BALANCE);
        return false;
    }else{
        $('#ticket_modal_content #input_point_error').text('');
        return true;
    }
}

function insert_row_ticket($id, $jan_code, $ticket_balance, $ticket_name, $text_type_ticket, $ticket_price, $readonly, $input_point_ticket, $list_product){
    var $row = '';
    $row = '<tr status_ticket="use" ticket_id="'+$id+'" jan_code="'+$jan_code+'" ticket_balance="'+$ticket_balance+'" ticket_name="'+$ticket_name+'" ticket_price="'+$ticket_price+'" set_ticket="'+$input_point_ticket+'" list_product="'+$list_product+'"';
    $row += '>';
    $row += '<td class="col_action"><button type="button" class="btn btn-danger btn-add btn-remove btn-remove-product"><i class="fa fa-times-circle" aria-hidden="true"></i></button></td>';
    $row += '<td class="col_type text-left td-product">' + $text_type_ticket + '</td>';
    $row += '<td class="col_name text-left td-product">' + $ticket_name + '</td>';
    $row += '<td class="col_private_setting text-left td-product"></td>';
    $row += '<td class="col_price td-product unit-price"><span class="price"></span></td>';
    $row += '<td class=""><input type="number" name="quantity" min="1" value="'+$input_point_ticket+'" '+$readonly+'></td>';
    $row += '<td class="td-product total-price"><span class="price"></span></td>';
    $row += '<td class="">' + textListStaff + '</td>';
    $row += '</tr>';

    $('#detail_products').append($row);
    $listaddedJancode.push($jan_code+'_u');
}
//************* Modal Ticket **************

//************* Modal Charge **************

$(document).on('click', '#btn_charge', function () {
    var $customer_id = $('#customerId').val();
    if(!$customer_id) return false;
    var $store_id = $('select[name="store_id"]').val();
    if(!$store_id) return false;
    if(return_good_status == 1) return false;
    
    $('#charge_modal').modal('show')
            .find('#charge_modal_content')
            .load($(this).attr('value'), {
                customer_id: $customer_id,
                store_id:    $store_id
            });
});

$(document).on('change', '#charge_modal_content #charge_select', function () {
    var $value = $(this).val();
    if ($value === '1') {
        $('#charge_select_div').html('<div class="col-xs-5">現金額</div><div class="col-xs-7 clear-padding"><input type="text" name="input_money_charge" id="input_money_charge" class="form-control input-decimal" maxlength="10" /><div class="help-block-error" id="input_money_charge_error"></div></div>');
    }else if($value === '0'){
        $('#charge_select_div').text('');
    }
});

$(document).on('click', '#btn_charge_modal', function () {
    if(!checkSelectCharge())  return false;
    $('#charge_modal').modal('hide');
    
    var $id = 'ch',
        $text_type_charge  = SETTING.CHARGE,
        $jan_code  = 'ch',
        $total_charge  = $('#charge_modal_content #total_charge_hd').val(),
        $charge_input  = $('#charge_modal_content #charge_input').val(),
        $input_money_charge = $('#charge_modal_content #input_money_charge').val(),
        $charge_select = $('#charge_modal_content #charge_select').val(),
        $readonly = 'readonly';
    var $input_money_charge_lb = ($input_money_charge == null)?'':$input_money_charge;
    
    var $charge_tr = $('#detail_products').find('tr[charge_id="'+$id+'"][jan_code="'+$jan_code+'"][charge_select="'+$charge_select+'"]');
    if(!$charge_tr.text()){
        //insert row charge
        insert_row_charge($id, $text_type_charge, $jan_code, $total_charge, $charge_input, $input_money_charge_lb, $charge_select, $readonly);
    }else{
        var $charge_input_lb = ($charge_select === '0')?$charge_input:$input_money_charge;
        $charge_tr.find('td #input_money_charg_span').text($charge_input_lb);
        $charge_tr.find('td #charge_input_span').text($charge_input_lb);
        $charge_tr.attr('charge_input',$charge_input);
        $charge_tr.attr('input_money_charge',$input_money_charge_lb);
        var $charge_select_lb = ($charge_select === '0')?'使用':'チャージ'+ $charge_input.toString();
        $charge_tr.find('.col_name').text($charge_select_lb);
    }
    updateReceipt();
});

function checkSelectCharge(){
    var $charge_select = $('#charge_modal_content #charge_select').val();
    if($charge_select === '0'){
        var $charge_input = $('#charge_modal_content #charge_input').val();
        if(!$charge_input){
            $('#charge_modal_content #charge_input_error').text(MESSAGE.ERR_CHARGE_INPUT);
            return false;
        }
        var $total_charge_hd = $('#charge_modal_content #total_charge_hd').val();
        if(parseInt($charge_input) > parseInt($total_charge_hd)){
            $('#charge_modal_content #charge_input_error').text(MESSAGE.CHECK_CHARGE_USE);
            return false;
        }
    }else if($charge_select === '1'){
        var $charge_input = $('#charge_modal_content #charge_input').val(),
            $input_money_charge = $('#charge_modal_content #input_money_charge').val(),
            $check = true;
        if(!$charge_input){
            $('#charge_modal_content #charge_input_error').text(MESSAGE.ERR_CHARGE_INPUT);
            $check = false;
        }
        if(!$input_money_charge){
            $('#charge_modal_content #input_money_charge_error').text(MESSAGE.ERR_CHARGE_INPUT);
            $check = false;
        }
        return $check;
    }
    
    return true;
}

function insert_row_charge($id, $text_type_charge, $jan_code, $total_charge, $charge_input, $input_money_charge, $charge_select, $readonly){
    var $row = '';    
    $row = '<tr charge_id="'+$id+'" jan_code="'+$jan_code+'" total_charge="'+$total_charge+'" charge_input="'+$charge_input+'" input_money_charge="'+$input_money_charge+'" charge_select="'+$charge_select+'"';
    $row += '>';
    $row += '<td class="col_action"><button type="button" class="btn btn-danger btn-add btn-remove btn-remove-product"><i class="fa fa-times-circle" aria-hidden="true"></i></button></td>';
    $row += '<td class="col_type text-left td-product">' + $text_type_charge + '</td>';
    var $charge_select_lb = ($charge_select === '0')?'使用':'チャージ'+ $charge_input.toString();
    $row += '<td class="col_name text-left td-product">' + $charge_select_lb + '</td>';
    $row += '<td class="col_private_setting text-left td-product"></td>';
    var $charge_input_lb = ($charge_select === '0')?$charge_input:$input_money_charge;
    $row += '<td class="col_price td-product unit-price"><span class="price" id="input_money_charg_span">' + $charge_input_lb + '</span></td>';
    $row += '<td class=""><input type="number" name="quantity" min="1" value="1" '+$readonly+'></td>';
    $row += '<td class="td-product total-price"><span class="price" id="charge_input_span">' + $charge_input_lb + '</span></td>';
    $row += '<td class="">' + textListStaff + '</td>';
    $row += '</tr>';

    $('#detail_products').append($row);
}
//************* Modal Charge **************

/*
 * product_discount status:
 * 0 : discount percent(割引)
 * 1 : discount yen(値引)
*/
//************* Modal Product *************
$(document).on('click', '#btn_discount_percent', function () {
    $jan_code = 1;
    $('#product_modal').modal('show')
            .find('#product_modal_content')
            .load($(this).attr('value'), {
                discount: 'percent'
            });
});

$(document).on('click', '#btn_discount_yen', function () {
    $jan_code = 1;
    $('#product_modal').modal('show')
            .find('#product_modal_content')
            .load($(this).attr('value'), {
                discount: 'yen'
            });
});

$(document).on('change', '#product_modal_content #product_discount', function () {
    var $value = $(this).val();
    if ($value === '1') {
        $('#discount_type').text('円');
    }else if($value === '0'){
        $('#discount_type').text('％');
    }
    $('#product_modal_content #product_reduction').val(0);
});

$(document).on('click', '#detail_products .td-product', function () {
    var $product_id = $(this).parent().attr('product-id');
    if(!$product_id) return false;
    
    var $jan_code = $(this).parent().attr('jan_code'),
        $unit_price = $(this).parent().attr('unit_price'),
        $product_reduction = $(this).parent().attr('product_reduction'),
        $product_discount_status = $(this).parent().attr('product_discount_status'),
        $gift_product = $(this).parent().attr('gift_product');
    if($jan_code.substring(0,2) != '21') return false;
    $gift_product = ($gift_product != "1")?"0":$gift_product;
    
    var $quantity = $(this).parent().find('input[type=number]').val()
    if ($jan_code) {
        $('#product_modal').modal('show')
                .find('#product_modal_content')
                .load('product-setting', {
                    jan_code: $jan_code,
                    quantity: $quantity,
                    unit_price: $unit_price,
                    product_reduction: $product_reduction,
                    product_discount_status: $product_discount_status,
                    gift_product: $gift_product
                });
    }
});

$(document).on('change', '#product_modal_content .input-decimal', function () {
    total_product_price();
    total_reduction_price();
});

$(document).on('change', '#product_modal_content #product_discount', function () {
    total_reduction_price();
});

$(document).on('change', '#product_modal_content #product_reduction', function () {
    total_reduction_price();
});

$(document).on('change', '#detail_products .quantity', function () {
    var $value = $(this).val(),
        $discount_for_1 = $(this).parent().parent().find('.discount_for_1').val(),
        $total_discount_product = $(this).parent().parent().find('.price_discount_2');
    $value = ($value)?$value:0;
    $discount_for_1 = ($discount_for_1)?$discount_for_1:0;
    var $total_discount = $discount_for_1*$value;
    $total_discount = ($total_discount === '0')?'':$total_discount;
    $total_discount_product.text($total_discount);
});

$(document).on('click', '#btn_product_modal', function () {
    if(!checkSubmitProduct()) return false;
    $('#product_modal').modal('hide');
    var $product_jan_code = $('#product_modal_content #product_jan_code').val(),
        $product_unit_price  = $('#product_modal_content #product_unit_price').val(),
        $product_quantity  = $('#product_modal_content #product_quantity').val(),
        $product_discount  = $('#product_modal_content #product_discount').val(),
        $product_reduction  = $('#product_modal_content #product_reduction').val(),
        $product_total_price_hd = $('#product_modal_content #product_total_price_hd').val(),
        $product_reduction_total = $('#product_modal_content #product_reduction_total_hd').val();
    $product_jan_code = ($product_jan_code)?$product_jan_code:0;
    $product_unit_price = ($product_unit_price)?$product_unit_price:0;
    $product_quantity = ($product_quantity)?$product_quantity:0;
    $product_discount = ($product_discount)?$product_discount:0;
    $product_reduction = ($product_reduction)?$product_reduction:0;
    $product_total_price_hd = ($product_total_price_hd)?$product_total_price_hd:0;
    $product_reduction_total = ($product_reduction_total)?$product_reduction_total:0;
    
    if($product_jan_code == 0){
        var $id = 'rd',
            $text_type_product = '伝票',
            $jan_code   =   'rd',
            $product_discount_select = $product_discount,
            $readonly = 'readonly';
        var $product_discount_tr = $('#detail_products').find('tr[product_id="'+$id+'"][jan_code="'+$jan_code+'"][product_discount="'+$product_discount_select+'"]');
        if(!$product_discount_tr.text()){            
            insert_row_product_discount($id, $text_type_product, $jan_code, $product_discount_select, $product_reduction, $readonly);
        }else{
            $product_discount_tr.attr('product_reduction',$product_reduction);
            var $product_reduction_lb = ($product_discount_select === '0')?'%':'円'
            $product_discount_tr.find('#product_reduction_text').text($product_reduction+$product_reduction_lb);
        }
        
    }else{
        //find product
        var $product_row = $('#accounting_management_form #detail_products').find('tr[jan_code="'+$product_jan_code+'"]');
        var $display_product_discount = ($product_discount == '0')?'<input type="hidden" value="-'+parseInt(0.01*$product_reduction*$product_unit_price)+'" class="discount_for_1" />':"";
        var $reduction_unit_price = ($product_discount == '0')?parseInt($product_unit_price*0.01*$product_reduction).toString():$product_reduction.toString();
    //    $product_row.find('.unit-price').html('<span class="price">'+$product_unit_price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")+'</span><br><span class="price">'+"-"+$reduction_unit_price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")+'</span>');    
        var $gift_product = ($('#product_modal_content #gift_product').is(':checked'))?"1":"0",
            $product_discount_text = ($product_discount == '0')?"割引":"値引";
            
        update_row_product($product_row, $product_quantity, $product_total_price_hd, $product_reduction_total, $display_product_discount, $product_unit_price ,$reduction_unit_price, $gift_product, $product_discount_text, $product_discount, $product_reduction);
    }
    
    updateReceipt();
});

function total_product_price() {
    var $product_unit_price = $('#product_modal_content #product_unit_price').val();
    $product_unit_price = ($product_unit_price)?$product_unit_price:0;
    var $product_quantity = $('#product_modal_content #product_quantity').val();
    $product_quantity = ($product_quantity)?$product_quantity:0;
    var $val = $product_unit_price*$product_quantity;
    var $value = parseInt($val).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    $('#product_modal_content #product_total_price').text($value);
    $('#product_modal_content #product_total_price_hd').val(parseInt($val));
    var $percent_reduction = $('#product_modal_content #product_reduction_total_hd').val();
    $percent_reduction = ($percent_reduction)?$percent_reduction:0;
    $('#product_modal_content #total__price').text(($val + $percent_reduction).toString()+' 円');
}

function total_reduction_price() {
    var $product_discount = $('#product_modal_content #product_discount').val();
    $product_discount = ($product_discount)?$product_discount:'0';
    var $product_reduction = $('#product_modal_content #product_reduction').val();
    $product_reduction = ($product_reduction)?$product_reduction:0;
    var $product_total_price_hd = $('#product_modal_content #product_total_price_hd').val();
    $product_total_price_hd = ($product_total_price_hd)?$product_total_price_hd:0;
    var $product_unit_price = $('#product_modal_content #product_unit_price').val();
    $product_unit_price = ($product_unit_price)?$product_unit_price:0;
    var $product_quantity  = $('#product_modal_content #product_quantity').val();
    $product_quantity = ($product_quantity)?$product_quantity:0;
    var $percent_reduction = 0;
    if($product_discount === '0'){
        $percent_reduction = parseInt(0.01*$product_reduction*$product_unit_price)*$product_quantity;
    }else{
        $percent_reduction = $product_reduction;
    }
    $value = parseInt($percent_reduction);
    var $value = $value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    $('#product_modal_content #product_reduction_total').text('-' + $value.toString());
    $('#product_modal_content #product_reduction_total_hd').val(-parseInt($percent_reduction));
    $('#product_modal_content #total__price').text(parseInt($product_total_price_hd - $percent_reduction).toString()+' 円');
}

function checkSubmitProduct(){
    var $product_unit_price = $('#product_modal_content #product_unit_price').val();
    if(!$product_unit_price){
        $('#product_modal_content #product_unit_price_error').text(MESSAGE.ERR_PRODUCT_PRICE_EMP);
        return false;
    }
    var $product_quantity = $('#product_modal_content #product_quantity').val();
    if(!$product_quantity){
        $('#product_modal_content #product_quantity_error').text(MESSAGE.ERR_PRODUCT_QUANTITY_EMP);
        return false;
    }
    var $product_reduction = $('#product_modal_content #product_reduction').val(),
        $product_discount = $('#product_modal_content #product_discount').val(),
        $check_discount = true;
    if($('#product_modal_content #check_discount').val() == 1){
        $check_discount = false;
    }
    
    if($check_discount && $product_discount === '1' && parseInt($product_reduction) > parseInt($product_unit_price)){
        $('#product_modal_content #product_reduction_error').text(MESSAGE.ERR_PRODUCT_REDUCTION_LARGE_PRICE);
        return false;
    }
    if($check_discount && $product_discount === '0' && parseInt($product_reduction) > 99){
        $('#product_modal_content #product_reduction_error').text(MESSAGE.ERR_PRODUCT_REDUCTION_LARGE_PRICE_1);
        return false;
    }
    
    return true;
}

function insert_row_product_discount($id, $text_type_product, $jan_code, $product_discount_select, $product_reduction, $readonly){
    var $row = '';
    $row = '<tr product_id="'+$id+'" jan_code="'+$jan_code+'" product_discount="'+$product_discount_select+'" product_discount_status="'+$product_discount_select+'" product_reduction="'+ $product_reduction+'" ';
    $row += '>';
    $row += '<td class="col_action"><button type="button" class="btn btn-danger btn-add btn-remove btn-remove-product"><i class="fa fa-times-circle" aria-hidden="true"></i></button></td>';
    $row += '<td class="col_type text-left td-product">' + $text_type_product + '</td>';
    var $product_select_lb = ($product_discount_select === '0')?'割引':'値引';
    $row += '<td class="col_name text-left td-product">' + $product_select_lb + '</td>';
    $row += '<td class="col_private_setting text-left td-product">'+ $product_select_lb +'</td>';
    $row += '<td class="col_price td-product unit-price"><span class="price"></span></td>';
    $row += '<td class=""><input type="number" name="quantity" min="1" value="1" '+$readonly+'></td>';
    var $product_reduction_lb = ($product_discount_select === '0')?'%':'円';
    $row += '<td class="td-product total-price"><span class="price" id="product_reduction_text">' + $product_reduction+$product_reduction_lb + '</span></td>';
    $row += '<td class="">' + textListStaff + '</td>';
    $row += '</tr>';

    $('#detail_products').append($row);
}

function update_row_product($product_row, $product_quantity, $product_total_price_hd, $product_reduction_total, $display_product_discount, $product_unit_price ,$reduction_unit_price, $gift_product, $product_discount_text, $product_discount, $product_reduction){
    $product_row.find('input[name="quantity"]').val($product_quantity);
    var $gift_product_lb = ($gift_product == "1")?"ギフト":"";
    if(parseInt($product_reduction) != 0 && $product_reduction != ''){
        $product_row.find('.col_private_setting').html('<span class="price">'+$gift_product_lb+'</span><br><span class="price_discount_3">'+$product_discount_text+'</span>');
        $product_row.find('.total-price').html('<span class="price">'+$product_total_price_hd+'</span><br><span class="price_discount_2">'+$product_reduction_total+'</span>'+$display_product_discount);
        $product_row.find('.unit-price').html('<span class="price">'+$product_unit_price+'</span><br><span class="price_discount_1">'+"-"+$reduction_unit_price+'</span>');
    }else{
        $product_row.find('.col_private_setting').html('<span class="price">'+$gift_product_lb+'</span><br><span class="price_discount_3"></span>');
        $product_row.find('.total-price').html('<span class="price">'+$product_total_price_hd+'</span><br><span class="price_discount_2"></span>');
        $product_row.find('.unit-price').html('<span class="price">'+$product_unit_price+'</span><br><span class="price_discount_1"></span>');
    }
    $product_row.attr('product_discount_status',$product_discount);
    $product_row.attr('unit_price',$product_unit_price);
    $product_row.attr('product_reduction',$product_reduction);
    $product_row.attr('gift_product',$gift_product);
}
//************* Modal Product *************
//alert($('button[id=submit_order][class="btn btn-default btn-block common-button-submit"]').text());
$('#div_modal').load(".input-decimal", function(){
    $(this).numeric({ decimal : ".",  negative : false, scale: 3 });
});