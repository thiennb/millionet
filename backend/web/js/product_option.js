function init_product_slect(e) {
    e.preventDefault();
    var $store_id = $('#masteroption-store_id').val();
    var role = $('#masteroption-_role').val();
    //alert(role);
    if ((role != 2 || role != 3) && $store_id == '') {
        var idStoreComon = $('#masteroption-id_store_option').val();
        $store_id = idStoreComon;
    }

    if ($store_id == '-1') {
        bootbox.dialog({
            title: '<div class="box-header with-border common-box-h4 col-md-8"><h4 class="text-title"><b>' + MESSAGE.CONFIRM_DEL + '</b></h4></div>',
            message: '<span style="color:red;">' + MESSAGE.OPTION_SEL_SHOP + '</span>',
            closeButton: false,
            buttons: {
                success: {
                    label: '閉じる',
                    className: "btn common-button-default text-center",
                    callback: function () {
                    }
                },
            }
        });
    } else {
        $('#productModal').modal('show');
    }

    var $options = $('.option-checkbox');
    var $option_hidden = $('#option_hidden').val();
    var $option_hidden = ($option_hidden) ? $option_hidden.split(",") : $option_hidden;
    $options.each(function () {
        if ($.inArray($(this).val(), $option_hidden) >= 0) {
            $(this).prop('checked', true);
        } else {
            $(this).prop('checked', false);
        }
    })
}
function refresh_category_product(type, idStoreComon, url, container, fragment){
    $.pjax({
        type: type,
        data: {
            store_id: idStoreComon
        },
        container: container,
        fragment: fragment,
        url: url,
    });
}
$('#masteroption-store_id').on('change', function () {
    $('#product_table').find('tbody').html('');
    $('#option_hidden').val('');
    var $store_id = $(this).val();

    var role = $('#masteroption-_role').val();
    //alert(role);
    if ((role != 2 || role != 3) && $store_id == '') {
        var idStoreComon = $('#masteroption-id_store_option').val();
        $store_id = idStoreComon;
    }
    $.pjax({
        type: 'POST',
        data: {
            store_id: $store_id
        },
        container: '#category_product',
        fragment: '#category_product',
        url: $('#refresh-data-modal').attr('href'),
    });
    return false;
});
$('#refresh-data-modal').on('click', function (e) {
    e.preventDefault();
    var $option_hidden = $('#option_hidden').val();
    $.pjax({
        type: 'POST',
        data: {
            option_hidden: $option_hidden
        },
        container: '#product_table',
        fragment: '#product_table',
        url: $(this).attr('href'),
    });
});
$(document).ready(function () {
    var role = $('#masteroption-_role').val();
    var action = $('#masteroption-_action').val();
    //alert(role);
    if ((role != 2 || role != 3) && action == 'create') {
        var idStoreComon = $('#masteroption-id_store_option').val(),
            url =  $('#refresh-data-modal').attr('href'),
            container = '#category_product',
            type    = 'POST',
            fragment = '#category_product';
        refresh_category_product(type, idStoreComon, url, container, fragment);
        return false;
    }
});