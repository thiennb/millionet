yii.allowAction = function ($e) {
        var message = $e.data('confirm');
        return message === undefined || yii.confirm(message, $e);
    };
yii.confirm = function (message, $e) {
    var $del_url = $(this).attr('href');
    bootbox.dialog({
        title: '<div class="box-header with-border common-box-h4 col-md-8"><h4 class="text-title"><b>'+MESSAGE.CONFIRM_DEL+'</b></h4></div>',
        message: MESSAGE.DELETE,  
        closeButton: false,
        buttons: {
            success: {
                label: 'OK',
                className: "btn common-button-action text-center",
                callback: function () {
                    $.ajax({
                        type: 'POST',
                        url: $del_url,                        
                    }).success(function (data) {
                        if (data == 'false')
                        {
                            bootbox.dialog({
                                title: '<div class="box-header with-border common-box-h4 col-md-8"><h4 class="text-title"><b>'+MESSAGE.CONFIRM_DEL+'</b></h4></div>',
                                message: '<span style="color:red;">'+message+'</span>',  
                                closeButton: false,
                                buttons: {
                                    success: {
                                        label: '閉じる',
                                        className: "btn common-button-default text-center",
                                        callback: function () {
                                        }
                                    },
                                }
                            });
                        }
                    });
                }
            },
            danger: {
                label: 'キャンセル',
                className: "btn btn-default text-center",
                callback: function () {
                }
            }
        }
    });
    // confirm will always return false on the first call
    // to cancel click handler
    return false;
}

yii.warning =function (message, title, $e) { 
    if(title == null){
      title = MESSAGE.CONFIRM_DEL ;
    }
    bootbox.dialog({
    title: '<div class="box-header with-border common-box-h4 col-md-8"><h4 class="text-title"><b>'+title+'</b></h4></div>',
    message: '<span style="color:red;">'+message+'</span>',  
    closeButton: false,
    buttons: {
        success: {
            label: '閉じる',
            className: "btn common-button-default text-center",
            callback: function () {
            }
        },
    }
  });
}