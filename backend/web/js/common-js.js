$(function () {
    $('.haft-size').keypress(function () {
        $(this).each(function () {
            check_haft_size($(this), $(this).val());
        });
    });
    function check_haft_size(e, value)
    {
        //Check Charater
        var unicode = e.charCode ? e.charCode : e.keyCode;
        if (value.indexOf(".") != -1)
            if (unicode == 46)
                return false;
        if (unicode != 8)
            if ((unicode < 48 || unicode > 57) && unicode != 46)
                return false;
    }
    ;
});

function delete_table_row(e) {
    var $id = $(e).attr('id');
    var $option_hidden = $('#option_hidden').val();
    var $option_hidden_arr = [];
    $option_hidden = ($option_hidden) ? $option_hidden.split(",") : $option_hidden;
    for ($i = 0; $i < $option_hidden.length; $i++) {
        ($option_hidden[$i] != $id) ? $option_hidden_arr.push($option_hidden[$i]) : '';
    }
    $('#option_hidden').val($option_hidden_arr);
    $(e).parent().parent().remove();
    return false;
}

function getPostcode(code_ele, address_ele) {
    var code = $(code_ele).val();
    code = code.replace(/[^0-9\.]+/g, "");


//       $(address_ele).val(code);
    if (code != "") {
        var pathControler = SITE_ROOT + '/master-staff/getpostcode';
        $.ajax({
            url: pathControler,
            type: 'post',
            data: {
                postCode: code
            },
            success: function (data) {
                $(code_ele).closest('.form-group').removeClass('has-error').find('.help-block').text('');
                if (data != '') {
                    $(address_ele).val(data);
                } else {
                    $(code_ele).closest('.form-group').addClass('has-error').find('.help-block').text('入力した郵便番号に該当する住所は存在しません。');
                }
            }
        });
    } else {
        $(code_ele).closest('.form-group').removeClass('has-error').find('.help-block').text('');
        $(code_ele).closest('.form-group').addClass('has-error').find('.help-block').text('郵便番号を入力してください。');
    }

    return true;
}
//$(document).on('pjax:complete', function() {
//    $('.delete_table_row').on('click', function(){
//        debugger;
//        $(this).each(function(){
//            var $id = $(this).attr('id');
//            var $option_hidden = $('#option_hidden').val();
//            var $option_hidden_arr = [];
//            $option_hidden = ($option_hidden)?$option_hidden.split(","):$option_hidden;
//            for($i = 0;$i<$option_hidden.length;$i++){
//                ($option_hidden[$i] != $id)?$option_hidden_arr.push($option_hidden[$i]):'';
//            }
//            $('#option_hidden').val($option_hidden_arr);
//            $(this).parent().parent().remove();
//            return false;
//        });
//    });
//    $('#productModal').on('shown.bs.modal',function(){
//        debugger;
//        var $options = $('.option-checkbox');
//        var $option_hidden = $('#option_hidden').val();
//        var $option_hidden = ($option_hidden)?$option_hidden.split(","):$option_hidden;
//        $options.each(function(){
//            if($.inArray($(this).val(), $option_hidden) >= 0){
//                $(this).prop('checked',true);
//            }else{
//                $(this).prop('checked',false);
//            }
//        })
//    });
//});
function callConfirm(modal, confirmBtn) {
    if (modal == null) {
        modal = 'userModal';
    }
    if (confirmBtn == null) {
        confirmBtn = 'btn-confirm';
    }
//  $('form').removeAttr('data-pjax');

//  $('#'+confirmBtn).click();
    $('#' + modal).modal('show');
}
function submitConfirm(confirmBtn) {
    if (confirmBtn == null) {
        confirmBtn = 'btn-confirm';
    }
//  $('form').attr('data-pjax','');
//  $('#'+confirmBtn).attr('type','submit');
    $("#" + confirmBtn).removeAttr("name");
    // $("form").submit();
    if ($("#" + confirmBtn).attr('type') != 'submit') {
        $("form").submit();
    }

}
function daysInMonth(month, year){
    return new Date(year, month, 0).getDate();
}
function convertMoneytoString(value){
    return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

function convertStringtoMoney(text){    
    text=text.split(',').join('');
    return Number(text);
}

function detectIEStore(){
    
    if (navigator.userAgent.indexOf('MSIE') !== -1 || navigator.appVersion.indexOf('Trident/') > 0) {
        // MSIE
        
        console.log('day la IE');
    }
}

function convertFullSizeToHalfSize(stringConvert){    
    tmpVal = stringConvert;
    tmpVal = tmpVal.replace(/[Ａ-Ｚａ-ｚ０-９．＠＿－]/g, function(s){return String.fromCharCode(s.charCodeAt(0)-0xFEE0)}).replace(/\D/g,'');
    //return convertMoneytoString(tmpVal);
    return tmpVal;
}

$(document).on('change', '.convert-haft-size', function(){
    stringConvert = $(this).val().toString();
    stringConvert = convertFullSizeToHalfSize(stringConvert);
    $(this).val(stringConvert);
});
$(document).on('keydown', '.convert-haft-size', function(event){
    var key = event.which;
    stringConvert = $(this).val().toString();    
    if(key == 13){
        event.preventDefault();
        this.blur();
        this.focus();        
        //return false;
    }
});

var waitingDialog = waitingDialog || (function ($) {
    'use strict';

	// Creating modal dialog's DOM
	var $dialog = $(
		'<div class="modal fade" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-hidden="true" style="padding-top:15%; overflow-y:visible;">' +
		'<div class="modal-dialog modal-m">' +
		'<div class="modal-content">' +
			'<div class="modal-header"><h3 style="margin:0;"></h3></div>' +
			'<div class="modal-body">' +
				'<div class="progress progress-striped active" style="margin-bottom:0;"><div class="progress-bar" style="width: 100%"></div></div>' +
			'</div>' +
		'</div></div></div>');

	return {
		/**
		 * Opens our dialog
		 * @param message Custom message
		 * @param options Custom options:
		 * 				  options.dialogSize - bootstrap postfix for dialog size, e.g. "sm", "m";
		 * 				  options.progressType - bootstrap postfix for progress bar type, e.g. "success", "warning".
		 */
		show: function (message, options) {
			// Assigning defaults
			if (typeof options === 'undefined') {
				options = {};
			}
			if (typeof message === 'undefined') {
				message = 'しばらくお待ちください。';
			}
			var settings = $.extend({
				dialogSize: 'm',
				progressType: '',
				onHide: null // This callback runs after the dialog was hidden
			}, options);

			// Configuring dialog
			$dialog.find('.modal-dialog').attr('class', 'modal-dialog').addClass('modal-' + settings.dialogSize);
			$dialog.find('.progress-bar').attr('class', 'progress-bar');
			if (settings.progressType) {
				$dialog.find('.progress-bar').addClass('progress-bar-' + settings.progressType);
			}
			$dialog.find('h3').text(message);
			// Adding callbacks
			if (typeof settings.onHide === 'function') {
				$dialog.off('hidden.bs.modal').on('hidden.bs.modal', function (e) {
					settings.onHide.call($dialog);
				});
			}
			// Opening dialog
			$dialog.modal();
		},
		/**
		 * Closes dialog
		 */
		hide: function () {
			$dialog.modal('hide');
		}
	};
  
})(jQuery);