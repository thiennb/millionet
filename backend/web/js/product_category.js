$('.master-product-category tr td').on('click', function(e){
//        debugger;
    if($(this).find('input').length == 0 && $(this).find('a').length == 0){
        $(this).each(function(){
            var $url_category = $(this).parent().find('td:last').find('a').attr('href');
            var $id = ($url_category)?$url_category.split("=")[1]:$url_category;
            var $edit_category = $(this).parent().find('td:first');
            $edit_category.html('<input type="text" id="'+$id+'" class="form-control edit-category-product" maxlength="100" value="'+$edit_category.text()+'"/>');
        });
    }
});

//$('input[name="MstCategoriesProductSearch[name]"]').parent().parent().attr('id','new_category');
//$('input[name="MstCategoriesProductSearch[name]"]').attr('class','form-control product-category-name');
$('.master-product-category').find('tr').first().after('<tr id="new_category" class="filters"><td><input type="text" class="form-control product-category-name" name="MstCategoriesProductSearch[name]" value="" maxlength="100"></td><td>&nbsp;</td></tr>');
$('.master-product-category').on('keyup','.product-category-name',function(){
    var $check = true;
    $('.product-category-name').each(function(){
        if($(this).val() == ''){
            $check = false;
        }
    });
    if($check){
        $('.master-product-category').find('tr').first().after('<tr><td><input type="text" class="form-control product-category-name" name="MstCategoriesProductSearch[name]" maxlength="100"></td><td>&nbsp;</td></tr>');   
    }
});
//$('input[name="MstCategoriesProductSearch[name]"]').keyup(function (e) {
//    if (e.keyCode == 13) {
//        $('#bt_register_product_category').click();
//    }
//});
//$('.master-product-category tr:not(:eq(1)) td').on('keyup','.edit-category-product',function (e) {
//    if (e.keyCode == 13) {
//        $('#bt_register_product_category').click();
//    }
//});
$('#bt_register_product_category').on('click', function(e){
    waitingDialog.show();
    var $name = $('.master-product-category').find('.product-category-name');
    var $categories = $('.edit-category-product');
    if($name.size() == 1 && $categories.size() == 0){
        e.preventDefault();
        waitingDialog.hide();
        return false;
    }
    var $check = false;
//    debugger;
    $check = register_product_category();
    if($check){
        $check = update_product_category();
    }
    if($check){
        $.ajax({
            type: "POST",
            url: SITE_ROOT + "/mst-categories-product/reload",
        }).success(function (data) {
            waitingDialog.hide();
        });
    }else{
        waitingDialog.hide();
        bootbox.dialog({
            title: '<div class="box-header with-border common-box-h4 col-md-8"><h4 class="text-title"><b>'+MESSAGE.WARNING+'</b></h4></div>',
            message: '<span style="color:red;">'+MESSAGE.PRODUCT_CATEGORY+'</span>',  
            closeButton: false,
            buttons: {
                success: {
                    label: '閉じる',
                    className: "btn common-button-default text-center",
                    callback: function () {
                    }
                },
            }
        });
    }
});
function register_product_category(){
    var $name = $('.master-product-category').find('.product-category-name');
    var $check = 0;
    $name.each(function(){
        if($(this).val() != ""){
            $.ajax({
                type: "POST",
                url: SITE_ROOT + "/mst-categories-product/create/?name="+$(this).val(),
            }).success(function (data) {
                if (data == 'false')
                {
                    bootbox.dialog({
                        title: '<div class="box-header with-border common-box-h4 col-md-8"><h4 class="text-title"><b>'+MESSAGE.WARNING+'</b></h4></div>',
                        message: '<span style="color:red;">'+'category'+$(this).val()+"don't update."+'</span>',  
                        closeButton: false,
                        buttons: {
                            success: {
                                label: '閉じる',
                                className: "btn common-button-default text-center",
                                callback: function () {
                                }
                            },
                        }
                    });
                }
            });
            $check++;
        }        
    });
    return ($check>0 || ($check==0 && $name.size() == 1))?true:false;
}

function update_product_category(){
    var $categories = $('.edit-category-product');
    var $check = 0;
    $categories.each(function(){
        if($(this).val() != ""){
            $.ajax({
                type: "POST",
                data: {
                    name: $(this).val(),
                    id: $(this).attr('id')
                },
                url: SITE_ROOT + "/mst-categories-product/update/",
            }).success(function (data) {
                if (data == 'false')
                {
                    bootbox.dialog({
                        title: '<div class="box-header with-border common-box-h4 col-md-8"><h4 class="text-title"><b>'+MESSAGE.WARNING+'</b></h4></div>',
                        message: '<span style="color:red;">'+'category'+$(this).val()+"don't update."+'</span>',  
                        closeButton: false,
                        buttons: {
                            success: {
                                label: '閉じる',
                                className: "btn common-button-default text-center",
                                callback: function () {
                                }
                            },
                        }
                    });
                }
            });
            $check++;
        }
    });
    return ($check>0 || ($check==0 && $categories.size() == 0))?true:false;
}