
$('#btn-search-customer').on('click',function(){
    var $form = $("#masterenjoylottery-id");
    validate_ajax($form);
});
function validate_ajax($form){
    $.ajax({
        type: 'POST',
        data: {
            form : $form.serialize()
        },
        url: SITE_ROOT + "/enjoy-lottery/validate-search",
    }).success(function ($data_return) {
        //check $data_return == []
        if($data_return.length == 2){
            $form.find('.help-block').each(function(){
                $(this).text('');
                $(this).removeClass('has-error');
                $(this).parent().parent().removeClass('has-error');
            })
        }else{
            var $data_return = JSON.parse($data_return);

            for (var key in $data_return) {
                // skip loop if the property is from prototype
                if (!$data_return.hasOwnProperty(key)) continue;

                var obj = $data_return[key];
                for (var prop in obj) {
                    // skip loop if the property is from prototype
                    if(!obj.hasOwnProperty(prop)) continue;

                    $('#'+key).parent().find('.help-block').text(obj[prop]);
                    $('#'+key).parent().find('.help-block').addClass('has-error');
                    $('#'+key).parent().parent().addClass('has-error');
                }
            }
        }

        if ($($form).find('.has-error').length == 0) {
            get_lottery_pjax($form);
        }
    });
}
function get_lottery_pjax($form){
    $.pjax({
        type: 'POST',
        data: {
            form : $form.serialize()
        },
        container: '#lottery_customer',
        fragment: '#lottery_customer',
        url: $('#refresh-data-table').attr('href'),
    }).success(function (data) {
        var $count_winner = $('#enjoy_lottery_table').find('tbody tr');
        $('#masterenjoylottery-count_winner').val($count_winner.size());
    });
}

$("#masterenjoylottery-winner_announcement_date").on('change', function () {
    var $value_date = $(this).val(),
            $value_time = $("#masterenjoylottery-winner_announcement_time").val();
    if ($value_date != "") {
        var $ymd_str = $value_date.slice(0, 4) + "年 ";
        $ymd_str += $value_date.slice(5, 7) + "月 ";
        $ymd_str += $value_date.slice(-2) + "日 ";
        $value_date = $ymd_str;
    }
    if ($value_time != "") {
        var $time_str = $value_time.slice(0, 2) + "時 ";
        $time_str += $value_time.slice(-2) + "分";
        $value_time = $time_str;
    }
    $('#masterenjoylottery-winner_announcement_date_time').val($value_date + "&nbsp;&nbsp;" + $value_time);
})

$("#masterenjoylottery-winner_announcement_time").on('change', function () {
    var $value_time = $(this).val(),
            $value_date = $("#masterenjoylottery-winner_announcement_date").val();
    if ($value_date != "") {
        var $ymd_str = $value_date.slice(0, 4) + "年 ";
        $ymd_str += $value_date.slice(5, 7) + "月 ";
        $ymd_str += $value_date.slice(-2) + "日 ";
        $value_date = $ymd_str;
    }
    if ($value_time != "") {
        var $time_str = $value_time.slice(0, 2) + "時 ";
        $time_str += $value_time.slice(-2) + "分";
        $value_time = $time_str;
    }
    $('#masterenjoylottery-winner_announcement_date_time').val($value_date + " " + $value_time);
})

if ($("#masterenjoylottery-member_specified").prop('checked') == true) {
    $("#search_member").show();
} else {
    $("#name_member").text('');
    $("#customer_id_hd").val('');
    $("#search_member").hide();
}
$("#masterenjoylottery-member_specified").on('change', function () {
    if ($(this).prop('checked') == true) {
        $("#search_member").show();
    } else {
        $("#name_member").text('');
        $("#customer_id_hd").val('');
        $("#search_member").hide();
    }
})

jQuery.fn.preventDoubleSubmission = function () {

    var last_clicked, time_since_clicked;

    $("#btn-submit").bind('click', function (event) {

        if (last_clicked)
            time_since_clicked = event.timeStamp - last_clicked;

        last_clicked = event.timeStamp;

        if (time_since_clicked < 2000)
            return false;

        return true;
    });
};
$('#masterenjoylottery-id').preventDoubleSubmission();


function search_member_name() {
    var $member_code = $('#member_code').val(),
            $member_first_name = $('#member_first_name').val(),
            $member_last_name = $('#member_last_name').val();
    if($member_code || $member_first_name || $member_last_name){
        $.ajax({
            type: "POST",
            data: {
                member_code: $member_code,
                member_first_name: $member_first_name,
                member_last_name: $member_last_name
            },
            url: SITE_ROOT + "/master-customer/get-customer-lottery",
        }).success(function (data) {
            if (data != 'false')
            {
                $("#name_member").text(data.name);
                $("#customer_id_hd").val(data.id);
            } else {
                $("#name_member").text('');
                $("#customer_id_hd").val('');
            }
        });
    }
}
$(document).on('click', '#btn-confirm', function(){
    var $count_winner = $('#enjoy_lottery_table').find('tbody tr');
    $('#masterenjoylottery-count_winner').val($count_winner.size());
})
