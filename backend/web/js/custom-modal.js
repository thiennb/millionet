$(function () {    
    var $options_init = $('.option-checkbox');
    var $arr_option_init = '';
    $options_init.each(function(){
            if($(this).prop('checked') == true){
                $arr_option_init += $(this).parent().text()+'<br>';
            }
        })
    $('#produc-option').html($arr_option_init);
    $(document).on('click','#save-option-select',function (){
        var $options = $('.option-checkbox');
        var $arr_option = '';
        var $option_hidden = [];
        $options.each(function(){
            if($(this).prop('checked') == true){
                $arr_option += $(this).parent().text()+'<br>';
                $option_hidden.push($(this).val());
            }
        })
        $('#produc-option').html($arr_option);
        $('#option_hidden').val($option_hidden);
//        var $url = $('#refresh-data-modal').attr('href');
//        var $url_value = $('#refresh-data-modal').attr('value');
//        if($url_value != 'update'){
//            $('#refresh-data-modal').attr('href',$url+'?list_product='+$option_hidden);
//        }else{
//            $('#refresh-data-modal').attr('href',$url+'_'+$option_hidden);
//        }
        $('#refresh-data-modal').click();
    });
    $(document).on('click','#clear-option-select',function (){
  
        var $options = $('.option-checkbox');
        var $option_hidden = $('#option_hidden').val();
        var $option_hidden = ($option_hidden)?$option_hidden.split(","):$option_hidden;
        $options.each(function(){
            if($.inArray($(this).val(), $option_hidden) >= 0){
                $(this).prop('checked',true);
            }else{
                $(this).prop('checked',false);
            }
        })
    });
    $(".input-number").keyup(function() {
        $(this).each(function(){
            if (/\D/g.test($(this).val())) {
              $(this).val($(this).val().replace(/\D/g, ''));
        }
        })
    });
    $(".input-money").keyup(function() {
        $(this).each(function(){
            if (/\D/g.test($(this).val())) {
              $(this).val($(this).val().replace(/\D/g, ''));
        }
        })
    });
    $(".input-money").on('change', function () {
        var $val = $(this).val();
        $val = $val.replace(/\,/g, '')
        $(this).val('');
        $val = $val.replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        $(this).val($val);
    });
    
    $(document).on('submit', function () {
        $(".input-money").each(function() {
            $(this).each(function(){
                if (/\D/g.test($(this).val())) {
                  $(this).val($(this).val().replace(/\D/g, ''));
            }
            })
        });
    });
});