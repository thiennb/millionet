function init_option_slect(e) {
    e.preventDefault();
    var $store_id = $('#mstproduct-store_id').val();
    if ($store_id == '') {
        bootbox.dialog({
            title: '<div class="box-header with-border common-box-h4 col-md-8"><h4 class="text-title"><b>' + MESSAGE.WARNING + '</b></h4></div>',
            message: '<span style="color:red;">' + MESSAGE.OPTION_SEL_SHOP + '</span>',
            closeButton: false,
            buttons: {
                success: {
                    label: '閉じる',
                    className: "btn common-button-default text-center",
                    callback: function () {
                    }
                },
            }
        });
    } else {
        $('#optionModal').modal('show');
    }
    var $options = $('.option-checkbox');
    var $option_hidden = $('#option_hidden').val();
    var $option_hidden = ($option_hidden) ? $option_hidden.split(",") : $option_hidden;
    $options.each(function () {
        if ($.inArray($(this).val(), $option_hidden) >= 0) {
            $(this).prop('checked', true);
        } else {
            $(this).prop('checked', false);
        }
    })
}
$('#mstproduct-store_id').on('change',function(){
    $('#produc-option').html('');
    $('#option_hidden').val('');
    var $store_id = $(this).val();
    $.pjax({
        type: 'POST',
        data: {
            store_id: $store_id
        },
        container: '#option_content',
        fragment: '#option_content',
        url: $('#refresh_modal_option').attr('href'),
    });
    return false;
});

function init_size_image(e) {
   $(".file-preview-image").removeAttr("style");
}

//$('#mstproduct-show_flg').click(function (event) {
//    
//    
//     //if (this.checked) {
//         //$('#mstproduct-show_flg').val('');
//    alert($('input:radio[name=MstProduct[show_flg]]:checked').val());
//     //}
//});

$("#mstproduct-show_flg").change(function(){ // bind a function to the change event
        
        var check_radio = $("input[name='MstProduct[show_flg]']:checked").val();
        
        if(check_radio === '1' ){
           $("#tel_booking_flg_hd").removeAttr('style');
           $("#option_condition_flg_hd").removeAttr('style');
        }else{
           $("#mstproduct-tel_booking_flg").prop('checked', false); 
           $("#mstproduct-option_condition_flg").prop('checked', false); 
        }     
});