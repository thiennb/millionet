$(document).ready(function () {
    
    // Load Forcus Input Adrress
    $("#masterstore-address").focusout(function(){
        var geocoder = new google.maps.Geocoder();
        var address =  $(this).val();
        geocoder.geocode({'address': address}, function (results, status) {
            if (status === google.maps.GeocoderStatus.OK) {
                var latitude = results[0].geometry.location.lat();
                var longitude = results[0].geometry.location.lng();
                $('#masterstore-latitude').val(latitude);
                $('#masterstore-longitude').val(longitude);
            }
        });
    });
});