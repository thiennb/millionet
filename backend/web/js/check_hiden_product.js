function checkAssign() {

    if (document.getElementById('mstproduct-assign_fee_flg').checked) {
        // disabled
        $("#mstproduct-description").attr("disabled", true).addClass('disabled');
        $("#mstproduct-cost").attr("disabled", true).addClass('disabled');
        $("#mstproduct-time_require").attr("disabled", true).addClass('disabled');
        // Button delete image
        $(".fileinput-remove-button").attr("disabled", true).addClass('disabled');
        // Button load image
        $(".btn-file").attr("disabled", true).addClass('disabled');
        document.getElementsByClassName("fileinput-remove-button").disabled = true;
        document.getElementById("mstproduct-tmp_image1").disabled = true;
        document.getElementById("mstproduct-tmp_image2").disabled = true;
        document.getElementById("mstproduct-tmp_image3").disabled = true;
        $("#mstproduct-show_flg input:radio").attr('disabled', true);
        $("#btn-option").attr("disabled", true).addClass('disabled');

        // Set value ''
        $("#mstproduct-description").val('');
        $("#mstproduct-cost").val('');
        $("#mstproduct-time_require").val('');
        // Set Image
        var element = document.getElementsByClassName("kv-preview-data");
        var imageUrl = '/api/uploads/no-image.jpg';
        $('.file-preview-image').attr('src', imageUrl).attr('style', '');
        // End Set Image
        $('.file-footer-caption').text('');
        $('.file-upload-indicator').text('');
        $('.file-footer-buttons').text('');
        
        $("#tel_booking_flg_hd").attr("style", "visibility: hidden")
        $("#option_condition_flg_hd").attr("style", "visibility: hidden")

    } else {
        // Display
        $("#mstproduct-description").removeClass('disabled').removeAttr("disabled");
        $("#mstproduct-cost").removeClass('disabled').removeAttr("disabled");
        // Button delete image
        $(".fileinput-remove-button").removeClass('disabled').removeAttr("disabled");
        // Button load image
        $(".btn-file").removeClass('disabled').removeAttr("disabled");
        document.getElementsByClassName("fileinput-remove-button").disabled = false;
        document.getElementById("mstproduct-tmp_image1").disabled = false;
        document.getElementById("mstproduct-tmp_image2").disabled = false;
        document.getElementById("mstproduct-tmp_image3").disabled = false;
        $("#mstproduct-show_flg input:radio").attr('disabled', false);
        $("#btn-option").removeClass('disabled').removeAttr("disabled");
        $("#mstproduct-time_require").removeClass('disabled').removeAttr("disabled");
        
        var check_radio = $("input[name='MstProduct[show_flg]']:checked").val();
        
        if(check_radio === '1' ){
           $("#tel_booking_flg_hd").removeAttr('style');
           $("#option_condition_flg_hd").removeAttr('style');
        }   
    }
}
$(document).on('click', '#mstproduct-assign_fee_flg', function () {

    checkAssign();

});
$(document).ready(function () {

    checkAssign();

    // =======================
// Check tel_booking_flg start
// =======================
    $('#mstproduct-tel_booking_flg').click(function (event) {
        if (this.checked) {
          $("#mstproduct-option_condition_flg").prop('checked',false);
          $("#mstproduct-option_condition_flg").attr("disabled", true).addClass('disabled');
        } else {
           $("#mstproduct-option_condition_flg").removeClass('disabled').removeAttr("disabled");
        }
    });
    $('#mstproduct-option_condition_flg').click(function (event) {
        if (this.checked) {
          $("#mstproduct-tel_booking_flg").prop('checked',false);
          $("#mstproduct-tel_booking_flg").attr("disabled", true).addClass('disabled');
        } else {
           $("#mstproduct-tel_booking_flg").removeClass('disabled').removeAttr("disabled");
        }
    });
// =======================
// Check tel_booking_flg end
// =======================

});
