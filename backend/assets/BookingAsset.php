<?php

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * Main backend application asset bundle.
 */
class BookingAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        "css/booking.css",
        'css/handsontable.full.css',
        "css/fullcalendar.min.css",
        "css/fullcalendar.print.css"
        
    ];
    public $js = [
        'js/moment.min.js',
//        '//cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.9.1/fullcalendar.min.js' ,
        'js/handsontable.full.js',
    ];
    public $jsOptions = [
        'position' => \yii\web\View::POS_HEAD
    ];

//    public $jsOptions = [
//        'position' => \yii\web\View::POS_HEAD
//    ];
//    public $depends = [
//        'yii\web\YiiAsset',
//        'yii\bootstrap\BootstrapAsset',
//    ];
}
