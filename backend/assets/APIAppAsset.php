<?php

namespace backend\assets;

use yii\web\AssetBundle;
use yii\web\View;

/**
 * Main backend application asset bundle.
 */
class APIAppAsset extends AssetBundle {

    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
    ];
    public $js = [
        
    ];
    public $depends = [
        'backend\assets\AdminLteAsset',
    ];
    public function init() {
        $this->jsOptions['position'] = View::POS_HEAD;
        parent::init();
    }

}
