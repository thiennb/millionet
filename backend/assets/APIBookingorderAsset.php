<?php
namespace backend\assets;

use yii\web\AssetBundle;
use yii\web\View;

class APIBookingorderAsset extends AssetBundle
{
	public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $depends = [
        'rmrevin\yii\fontawesome\AssetBundle',
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'yii\bootstrap\BootstrapPluginAsset',
    ];
    public function init() {
        $this->jsOptions['position'] = View::POS_END;
        parent::init();
    }

    public $css = [
		'css/api-booking-order.css'
    ];
    public $js = [
		'js/api-booking-order.js',
        'js/api-booking-checklist.js'
    ];

    public $jsOptions = [
        'position' => \yii\web\View::POS_END
    ];
}
