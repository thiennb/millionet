<?php

namespace backend\assets;

use yii\web\AssetBundle;
use yii\web\View;

/**
 * Main backend application asset bundle.
 */
class AppAsset extends AssetBundle {

    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/common.css',
        'css/responsive.css'
    ];
    public $js = [
        'js/msg.jp.js',
        'js/bootbox.confirm.js',
        'js/custom-modal.js',
        //'js/insert_coupon.js',
        'js/common-js.js',
        '//cdnjs.cloudflare.com/ajax/libs/handlebars.js/2.0.0/handlebars.js',
        // Get Location when input adrress
        'js/get_location.js',
        //chart index backend
        'js/moment.js',
        'js/chartjs2.0.js',

        // End Location when input adrress
        // Check Hiden Of Function Product
        //'js/check_hiden_product.js'
        
    ];
    public $depends = [
        'backend\assets\AdminLteAsset',
    ];
    public function init() {
        $this->jsOptions['position'] = View::POS_HEAD;
        parent::init();
    }

}
