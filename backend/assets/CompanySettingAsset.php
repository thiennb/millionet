<?php

namespace backend\assets;

use yii\web\AssetBundle;


class CompanySettingAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $css = [

    ];

    public $js = [
        'js/colorpicker.js',
        'js/company.js'
    ];
}