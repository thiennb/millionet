<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use common\models\MasterStaff;

/* @var $this yii\web\View */
/* @var $searchModel common\models\MasterBookingHistorySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('backend', "Visit history");
$this->params['breadcrumbs'][] = $this->title;
?>
<style>
    .pagination{
        display: table;
        margin: 0 auto;
        padding: 10px;
    }
    .pagination a{
        margin: 0 2px;
    }
</style>
<div class="row">
    <div class="col-lg-12 col-md-12">
        <div class="box box-info box-solid">
            <div class="box-header with-border">
                <h4 class="text-title"><b><?= $this->title ?></b></h4>
            </div>
            <div class="box-body content">
                <div class="col-md-12">
                    <div class="common-box">
                        <div class="row">
                            <div class="box-header with-border common-box-h4 col-lg-12 col-md-12">
                                <h4 class="text-title"><b><?= Yii::t('backend', 'Visit history search') ?></b></h4>
                            </div>
                        </div>

                        <div class="box-body content">
                            <div class="col-lg-10 col-md-12 col-sm-12">
                                <?php echo $this->render('_search_visit_history', ['model' => $searchModel]); ?>
                            </div>
                        </div>
                    </div>
                    <?php
                    Pjax::begin();
                    ?>
                    <div class="common-box">
                        <div class="row">
                            <div class="box-header with-border common-box-h4 col-lg-12 col-md-12">
                                <h4 class="text-title"><b><?= Yii::t('backend', 'List visit history') ?></b></h4>
                            </div>
                        </div>
                        <div class="box-body content">
                            <div class="col-md-12">
                                <?=
                                GridView::widget([
                                    'dataProvider' => $dataProvider,
                                    'tableOptions' => [
                                        'class' => 'table table-striped table-bordered text-center',
                                    ],
                                    'layout' => "{pager}\n{summary}\n{items}",
                                    'summaryOptions' => ['class' => 'common-clr-fl-right'],
                                    'summary' => false,
                                    'pager' => [
                                        'class' => 'common\components\LinkPagerMillionet',
                                        'options' => ['class' => 'pagination'],
                                    ],
                                    'columns' => [
                                        ['class' => 'yii\grid\SerialColumn', 'header' => 'No', 'options' => ['class' => 'with-table-no']],
                                        [
                                            'attribute' => 'process_date',
                                            'label' => Yii::t('backend', 'Date Go'),
                                            'value' => function ($model) {
                                                return empty($model->process_date) ? "" : date('Y/m/d', strtotime($model->process_date)) . " " . $model->process_time;
                                            }
                                        ],
                                        [
                                            'value' => function ($model) {
                                                if (!empty($model->order_code))
                                                    $store = \common\models\MasterStore::findOne(['store_code' => substr($model->order_code, 0, 5)]);
                                                return empty($store) ? "" : $store->name;
                                            },
                                                    'header' => Yii::t('backend', 'Store Id'),
                                                ],
                                                [
                                                    'attribute' => 'order_code',
                                                ],
                                                [
                                                    'attribute' => 'customer_name',
                                                    'value' => function ($model) {
                                                        if (!empty($model->masterCustomer)) {
                                                            return $model->masterCustomer->first_name . $model->masterCustomer->last_name;
                                                        }
                                                    },
                                                    'header' => Yii::t('backend', 'Name Customer'),
                                                ],
                                                [
                                                    'attribute' => 'product_name',
                                                    'value' => function ($model) {
                                                        $result = [];
                                                        if (!empty($model->masterOrderDetail)) {
                                                            foreach ($model->masterOrderDetail as $detail) {
                                                                if (!empty($detail->masterProduct)) {
                                                                    array_push($result, htmlentities($detail->masterProduct->name));
                                                                }
                                                            }
                                                        }
                                                        return implode("<br /> ", $result);
                                                    },
                                                            'label' => Yii::t('backend', 'Purchase goods'),
                                                            'format' => 'html'
                                                        ],
                                                        [
                                                            'attribute' => 'charge_use',
                                                            'value' => function($model) {
                                                                if (!empty($model->total)) {
                                                                    return '¥' . number_format($model->total, 0, ".", ",");
                                                                } else
                                                                    return "¥0";
                                                            },
                                                            'label' => Yii::t('backend', 'Charge Use'),
                                                        ],
                                                        [
//                                                            'value' => function ($model) {
//                                                                $result = [];
//                                                                if (!empty($model->masterOrderDetail)) {
//                                                                    foreach ($model->masterOrderDetail as $detail) {
//                                                                        if (!empty($detail->management)) {
//                                                                            if (!empty($detail->management->staff))
//                                                                                array_push($result, htmlentities($detail->management->staff->name));
//                                                                        }
//                                                                    }
//                                                                }
//                                                                return implode("<br /> ", $result);
//                                                            },
//                                                                    'format' => 'html',
                                                            'attribute' => 'name_staff',
                                                            'label' => Yii::t('backend', 'People Staff'),
                                                        ],
                                                                [
                                                                    'label' => Yii::t('backend', 'Seat'),
                                                                ],
                                                            ],
                                                        ]);
                                                        ?>
                                                    </div>
                                                </div>
                                            </div>
                                            <?php Pjax::end(); ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
<?php
