<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use common\components\Constants;
use yii\jui\DatePicker;
use common\models\MasterStaff;
use common\models\MasterStore;
use common\models\MstCategoriesProduct;
use yii\helpers\Url;
// Drop List
use kartik\depdrop\DepDrop;

/* @var $this yii\web\View */
/* @var $searchModel common\models\MasterBookingHistorySearch */
/* @var $form yii\widgets\ActiveForm */
?>

<style>
    @media (min-width: 1200px) {
        .col-lg-1_5 {
            width: 10.33333333%;
        }
        .col-lg-0 {
            display: none;
        }
        .col-lg-0_5  {
            width: 3.33333334%;
        }

        .col-lg-1_5_add  {
            width: 7%;
        }
        .col-lg-2_5  {
            width: 13.33333333%;
        }
    }
</style>


<div class="master-coupon-search">
    <?php
    $form = ActiveForm::begin([
                'action' => ['index-visit-history'],
                'enableClientValidation' => false,
                'method' => 'get',
                'enableClientValidation' => false,
    ]);
    ?>


    <?php //= $form->field($model, 'register_staff_id',$fieldOptions1)->dropDownList(MasterStaff::getListStaff(),['prompt'=>Yii::t('backend', 'Please Select')]) ?>

    <div class="col-md-12">
        <div class="col-lg-2 col-md-3 font_label label_visit_history_ct">
            <?= Yii::t('backend', 'Date Go') ?>
        </div>
        <div class="col-lg-2 col-md-4 input_sale_report_ct" >
            <?=
                    $form->field($model, 'visit_date_from')
                    ->widget(DatePicker::classname(), [
                        'language' => 'ja',
                        'dateFormat' => 'yyyy/MM/dd',
                        'clientOptions' => [
                            "changeMonth" => true,
                            "changeYear" => true,
                            "yearRange" => "1900:+0"
                        ],
                    ])->textInput(['maxlength' => 10])->label(FALSE)
            ?>
        </div>
        <div  class="col-md-1 text-center separator">～</div>
        <div class="col-lg-2 col-md-4 input_sale_report_ct">
            <?=
                    $form->field($model, 'visit_date_to')
                    ->widget(DatePicker::classname(), [
                        'language' => 'ja',
                        'dateFormat' => 'yyyy/MM/dd',
                        'clientOptions' => [
                            "changeMonth" => true,
                            "changeYear" => true,
                            "yearRange" => "1900:+0"
                        ],
                    ])->textInput(['maxlength' => 10])->label(FALSE)
            ?>
        </div>
    </div>
</div>
<div class="col-md-12">
    <div class="col-lg-2 col-md-3 font_label label_visit_history_ct">
        <?= Yii::t('backend', 'Store Go') ?>
    </div>
    <div class="col-lg-2 col-md-4 input_sale_report_ct">
        <?= $form->field($model, 'register_store_id')->dropDownList(MasterStore::getListStore(), ['prompt' => Yii::t('backend', 'Please Select')])->label(false) ?>
    </div>

    <div class='col-lg-1_5 col-md-2 label-margin'>
        <?= Yii::t('backend', 'Order Code') ?>
    </div>

    <div class="col-lg-2 col-md-3 input_sale_report_ct">
        <?= $form->field($model, 'order_code')->textInput(['maxlength' => true])->label(FALSE) ?>
    </div>
</div>

<div class="col-md-12 staff-id">
    <div class="col-lg-2 col-md-3 font_label label_visit_history_ct">
        <?= Yii::t('backend', "Last time staff visit history") ?>
    </div>

    <div class="col-lg-2 col-md-4 input_sale_report_ct">
        <?= Html::hiddenInput('staff-id-last', $model->last_staff_id, ['id' => 'staff-id-last']); ?>
        <?=
                $form->field($model, 'last_staff_id')->dropDownList([], ['prompt' => Yii::t('backend', 'Please Select')])
                ->widget(DepDrop::classname(), [
                    'options' => ['id' => Html::getInputId($model, 'last_staff_id')],
                    'pluginOptions' => [
                        //'init' => true,
                        'depends' => [Html::getInputId($model, 'register_store_id')],
                        'placeholder' => ' ',
                        'url' => Url::to(['/master-customer/select-staff-by-id-store']),
                        'initialize' => false,
                        'params' => ['staff-id-last']
                    ]
                ])->label(false)
        ?>
    </div>
</div>
<div class="col-md-12 seat-id">
    <div class="col-lg-2 col-md-3 font_label label_visit_history_ct">
        <?= Yii::t('backend', 'Seat Use') ?>
    </div>
    <div class="col-lg-2 col-md-4 input_sale_report_ct">
        <?= Html::hiddenInput('seat-id-last', $model->seat_id, ['id' => 'seat-id-last']); ?>
        <?=
                $form->field($model, 'seat_id')->dropDownList([], ['prompt' => Yii::t('backend', 'Please Select')])
                ->widget(DepDrop::classname(), [
                    'options' => ['id' => Html::getInputId($model, 'seat_id')],
                    'pluginOptions' => [
                        //'init' => true,
                        'depends' => [Html::getInputId($model, 'register_store_id')],
                        'placeholder' => ' ',
                        'url' => Url::to(['/master-customer/select-seat-by-id-store']),
                        'initialize' => true,
                        'params' => ['seat-id-last']
                    ]
                ])->label(false)
        ?>
    </div>
</div>
<div class="col-md-12">
    <div class="col-lg-2 col-md-3 font_label label_visit_history_ct">

        <?= Yii::t('backend', 'Product Category') ?>
    </div>
    <div class="col-lg-8 col-md-9 font_label">
        <?=
        $form->field($model, 'list_category')->inline(true)->checkboxlist(MstCategoriesProduct::listCategory())->label(false)
        ?>
    </div>
</div>
<div class="col-md-12">
    <div class="col-lg-2 col-md-3 font_label label_visit_history_ct">
        <?= Yii::t('backend', 'Purchase Product Name') ?>

    </div>
    <div class="col-lg-2 col-md-4 input_sale_report_ct">
        <?= $form->field($model, 'product_name')->textInput(['maxlength' => true])->label(Yii::t('backend', 'Purchase Product Name'))->label(false) ?>
    </div>
</div>
<div class="col-md-12">
    <div class="col-lg-2 col-md-3 font_label label_visit_history_ct">
        <?= Yii::t('backend', 'Membership card number') ?>

    </div>
    <div class="col-lg-2 col-md-4 input_sale_report_ct">
        <?=
        $form->field($model, 'customer_jan_code')->widget(\yii\widgets\MaskedInput::className(), [
            'mask' => '9',
            'clientOptions' => ['repeat' => 13, 'greedy' => false]
        ])->textInput(['maxlength' => true])->label(false)
        ?>
    </div>
</div>
<div class="col-md-12">
    <div class="col-lg-2 col-md-3 font_label label_visit_history_ct">
<?= Yii::t('backend', 'Name Customer') ?>

    </div>
    <div class="col-lg-2 col-md-4 input_sale_report_ct">
        <?=
        $form->field($model, 'customer_name')->textInput(['maxlength' => true])->textInput(['maxlength' => true])->label(false)
        ?>
    </div>
</div>

<div class="col-md-12">
    <div class="col-lg-2 col-md-3 font_label label_visit_history_ct">
<?= Yii::t('backend', 'Sex Coupon') ?>

    </div>
    <div class="col-lg-2 col-md-4 input_sale_report_ct">
<?= $form->field($model, 'sex')->dropDownList(Constants::LIST_SEX, ['prompt' => Yii::t('backend', 'Please Select')])->label(false) ?>
    </div>
</div>

<div class="col-md-12">
    <div class="col-lg-2 col-md-3 font_label label_visit_history_ct">
<?= Yii::t('backend', 'Rank Customer') ?>

    </div>
    <div class="col-lg-2 col-md-4 input_sale_report_ct">
<?= $form->field($model, 'rank')->dropDownList(Constants::RANK, ['prompt' => Yii::t('backend', 'Please Select')])->label(false) ?>
    </div>
</div>

<div class="col-md-12">
    <div class="col-lg-2 col-md-3 font_label label_visit_history_ct">
        <?= Yii::t('backend', 'Number Of Store Visiting') ?>
    </div>
    <div class="col-lg-2 col-md-4 input_sale_report_ct">
        <?=
        $form->field($model, 'number_visit_min', [
            'inputTemplate' => '<div class="input-group">{input}<span class="input-group-addon"><b>回以上</b></span></div>',
        ])->widget(\yii\widgets\MaskedInput::className(), [
            'clientOptions' => [
                'groupSeparator' => ',',
                'alias' => 'integer',
                'autoGroup' => true,
                'removeMaskOnSubmit' => true,
                'allowMinus' => false,
            ]
        ])->textInput(['maxlength' => true])->label(false)
        ?>
    </div>
    <div  class="col-md-1 text-center separator"></div>
    <div class="col-lg-2 col-md-4 input_sale_report_ct">
        <?=
        $form->field($model, 'number_visit_max', [
            'inputTemplate' => '<div class="input-group">{input}<span class="input-group-addon"><b>回以下</b></span></div>',
        ])->widget(\yii\widgets\MaskedInput::className(), [
            'clientOptions' => [
                'groupSeparator' => ',',
                'alias' => 'integer',
                'autoGroup' => true,
                'removeMaskOnSubmit' => true,
                'allowMinus' => false,
            ]
        ])->textInput(['maxlength' => true])->label(false)
        ?>
    </div>
</div>

<div class="col-md-12">
    <div class="col-lg-2 col-md-3 font_label label_visit_history_ct">
<?= Yii::t('backend', 'Birth Date') ?>
    </div>

    <div class="col-lg-2 col-md-4 input_sale_report_ct">
        <?=
        $form->field($model, 'birth_date_from')->widget(DatePicker::classname(), [
            'language' => 'ja',
            'dateFormat' => 'yyyy/MM/dd',
            'clientOptions' => [
                "changeMonth" => true,
                "changeYear" => true,
                "yearRange" => "1900:+0"
            ],
        ])->textInput(['maxlength' => 10])->label(FALSE)
        ?>
    </div>
    <div  class="col-md-1 text-center separator">～</div>
    <div class="col-lg-2 col-md-4 input_sale_report_ct">
        <?=
        $form->field($model, 'birth_date_to')->widget(DatePicker::classname(), [
            'language' => 'ja',
            'dateFormat' => 'yyyy/MM/dd',
            'clientOptions' => [
                "changeMonth" => true,
                "changeYear" => true,
                "yearRange" => "1900:+0"
            ],
        ])->textInput(['maxlength' => 10])->label(FALSE)
        ?>
    </div>
</div>
<div class="form-group">
<?= Html::a(Yii::t('backend', 'Return'), ['/'], ['class' => 'btn common-button-default btn-default']) ?>
<?= Html::submitButton(Yii::t('backend', 'Search'), ['class' => 'btn common-button-submit']) ?>
</div>    
<?php ActiveForm::end(); ?>
<script>
    $('#<?= Html::getInputId($model, 'seat_id') ?>').on('depdrop.afterChange', function (event) {
        var $el = $(this);
        var chkOptions = $el.find('option').length;
        if (chkOptions === 0 || $el.find('option[value=""]').length === chkOptions) {
            $('.seat-id').hide();
        } else {
            $('.seat-id').show();
        }
    });

    $('#<?= Html::getInputId($model, 'last_staff_id') ?>').on('depdrop.afterChange', function (event) {
        var $el = $(this);
        var chkOptions = $el.find('option').length;
        if (chkOptions === 0 || $el.find('option[value=""]').length === chkOptions) {
            $('.staff-id').hide();
        } else {
            $('.staff-id').show();
        }
    });
    $('.staff-id').hide();
    $('.seat-id').hide();
</script>