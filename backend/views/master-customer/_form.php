<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Modal;
use common\widgets\preview\Preview;
use common\components\Constants;
use yii\jui\DatePicker;
use common\models\MasterStore;
use common\models\MasterStaff;
// Drop List
use kartik\depdrop\DepDrop;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\MasterCustomer */
/* @var $form yii\widgets\ActiveForm */

$fieldOptions1 = [
    'options' => ['class' => 'form-group '],
    'template' => '<div class="col-md-12"> 
                <div class="col-md-3 label-margin">  
                   {label}
                </div> 
                <div class="col-md-4">
                   {input}{error}
                </div>
                </div>'
];
?>
<style> 
    .field-mastercustomer-last_name, .field-mastercustomer-first_name ,.field-mastercustomer-first_name_kana , .field-mastercustomer-last_name_kana{
        display: inline-flex;
        width: 50%;
    }
    .radio{
        display: inline-block;
    }
    div.required label.control-label:after {
        content: " *";
        color: red;
    }
    .row-name{
        margin-top: -15px;
    }
</style>

<div class="col-md-12">
    <div class="box-header with-border common-box-h4 col-md-6">
        <h4 class="text-title"><b><?= Yii::t('backend', 'Create Master Customer') ?></b></h4>
    </div>
</div>

<div class="master-customer-form col-md-12" style="padding-left: 4em;">
    <?php
    $form = ActiveForm::begin([
                'layout' => 'horizontal',
                'fieldConfig' => [
                    'template' => "{beginWrapper}<div class='col-md-2'>{label}</div><div class='col-md-4 with-form-customer-input'>{input}\n{hint}\n{error}</div>\n{endWrapper}",
                    'horizontalCssClasses' => [
                        'label' => '',
                        'wrapper' => 'row',
                        'error' => '',
                        'hint' => '',
                    ],
                    'horizontalCheckboxTemplate' => "{input}{label}\n{error}\n{hint}"
                ],
                'options' => [
                    'enctype' => 'multipart/form-data',
                    'id' => strtolower($model->formName()) . '-id',
                ],
                // important
                'validationUrl' => 'validate',
                'enableClientValidation' => false,
                'enableAjaxValidation' => true,
                'validateOnChange' => false,
                'validateOnBlur' => false,
                'successCssClass' => '',
    ]);
    ?>
    <?= $form->field($model, 'id_hd_customer')->hiddenInput(['maxlength' => true, 'value' => $model->id])->label(false) ?>  
    <div>
        <?= $form->field($modelCustomerStore, 'store_id')->dropDownList(MasterStore::getListStore(), ['prompt' => Yii::t('backend', 'Please Select'), 'id' => 'customerstore-store_id']) ?>
    </div>
    <div>
        <?=
        $form->field($model, 'customer_jan_code')->widget(\yii\widgets\MaskedInput::className(), [
            'mask' => '9',
            'clientOptions' => ['repeat' => 13, 'greedy' => false]
        ])->textInput(['maxlength' => true])
        ?>  
    </div> 

    <div class="row row-inline row-name">
        <div class="col-md-2 clear-form-group no-padding">
            <div class="form-group " style="display: inline">
                <label class="mws-form-label">
                    <label class="control-label " for="mastercustomer-firstname"> <?= Yii::t('backend', 'Name'); ?></label>

                </label>
            </div>
            <div class="form-group " style="display: inline">
                <label class="mws-form-label">
                    <label class="control-label " for="mastercustomer-firstname"> <?= Yii::t('backend', 'Name kana'); ?></label>

                </label>
            </div>

        </div>
        <div class="col-md-6 clear-form-group">
            <?=
            $form->field($model, 'first_name', [
                'template' => "<div class='col-md-4 with-form-customer-input'><label class='mws-form-label'>{label}</label></div>"
                . '<div class="col-md-7">{input}{error}</p></div>'
            ])->textInput(['maxlength' => true])
            ?>
            <?=
            $form->field($model, 'last_name', [
                'template' => "<div class='col-md-4 with-form-customer-input'><label class='mws-form-label'>{label}</label></div>"
                . '<div class="col-md-7">{input}{error}</p></div>'
            ])->textInput(['maxlength' => true])
            ?>

            <?=
            $form->field($model, 'first_name_kana', [
                'template' => "<div class='col-md-4 with-form-customer-input'><label class='mws-form-label'>{label}</label></div>"
                . '<div class="col-md-7">{input}{error}</div>'
            ])->textInput(['maxlength' => true])
            ?>
            <?=
            $form->field($model, 'last_name_kana', [
                'template' => "<div class='col-md-4 with-form-customer-input'><label class='mws-form-label'>{label}</label></div>"
                . '<div class="col-md-7">{input}{error}'
                . '</div>'
            ])->textInput(['maxlength' => true])
            ?>
        </div>
    </div>

    <?= $form->field($model, 'sex')->dropDownList(Constants::LIST_SEX) ?>
    <?php $model->birth_date = Yii::$app->formatter->asDate($model->birth_date); ?>
    <?=
    $form->field($model, 'birth_date')->widget(DatePicker::classname(), [
        'language' => 'ja',
        'dateFormat' => 'yyyy/MM/dd',
        'clientOptions' => [
            "changeMonth" => true,
            "changeYear" => true,
            "yearRange" => "1900:+0"
        ],
    ])->textInput(['maxlength' => 10])
    ?>

    <?=
    $form->field($model, 'mobile')->widget(\yii\widgets\MaskedInput::className(), [
        'mask' => '9',
        'clientOptions' => ['repeat' => 17, 'greedy' => false]
    ])->textInput(['maxlength' => true])
    ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

    <?=
    $form->field($model, 'post_code', [
        'inputTemplate' => '<div class="col-md-8 no-padding post_code_ctm">{input}</div><div class="col-md-4 button-submit_search_ctm"><a id="search-code" class="btn common-button-submit"> 住所検索</a> </div>',
    ])->textInput(['maxlength' => true])->widget(\yii\widgets\MaskedInput::className(), [
        'mask' => '999-9999',
        'clientOptions' => [
            'removeMaskOnSubmit' => true
        ]
    ])
    ?>

    <?= $form->field($model, 'address')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'address2')->textInput(['maxlength' => true]) ?>
    <?=
    $form->field($model, 'register_staff_id')->widget(DepDrop::classname(), [
        'options' => ['id' => 'mastercustomer-register_staff_id'],
        'pluginOptions' => [
            //'init' => true,
            'depends' => ['customerstore-store_id'],
            'placeholder' => ' ',
            'url' => Url::to(['/master-customer/select-staff-by-id-store-ct'])
        ]
    ]);
    ?>

    <?= $form->field($model, 'memo')->textarea(['rows' => 6, 'maxlength' => true]) ?>
    <div class="font_label">
        <?= $form->field($modelCustomerStore, 'news_transmision_flg')->radioList(Constants::LIST_NEWS_TRANSMISION) ?>
    </div>

    <div class="no-padding"> 
        <?=
        $form->field($modelCustomerStore, 'black_list_flg', [
            'options' => ['tag' => false, 'class' => 'checkbox icheck']])->checkbox()
        ?>
    </div>

    <div class="form-group">
        <?= Html::a(Yii::t('backend', 'Return'), ['index'], ['class' => 'btn btn-default common-button-default']) ?>

        <?=
        Html::submitInput(Yii::t('backend', 'Confirm'), [
            'class' => 'btn common-button-submit',
            'id' => 'btn-confirm',
        ])
        ?>
    </div>
    <?php
    Modal::begin([
        'id' => 'userModal',
        'size' => 'SIZE_LARGE',
        'header' => '<div class="box-header with-border common-box-h4 col-md-8"><h4 class="text-title"><b>' . Yii::t('backend', 'Confirm input') . '</b></h4></div>',
        'footer' => Html::button(Yii::t('backend', 'Close'), ['class' => 'btn common-button-default', 'data-dismiss' => "modal", 'id' => 'btn-close'])
        . Html::submitButton(Yii::t('backend', 'Save'), ['class' => 'btn common-button-submit', 'id' => 'btn-submit']),
        'closeButton' => FALSE,
        'clientOptions' => ['backdrop' => 'static', 'keyboard' => false]
    ]);
    ?>
    <?=
    Preview::widget([
        "data" => [
            'store_id' => [
                'type' => 'select',
                'label' => Yii::t('backend', 'Register store'),
                'model' => 'CustomerStore'
            ],
            'customer_jan_code' => [
                'type' => 'input_customer_jan_code',
                'customer_jan_code_check' => true,
                'label' => Yii::t('backend', 'Membership card number')
            ],
            'first_name' => [
                'type' => 'input',
                'groupWith' => 'last_name',
                'label' => Yii::t('backend', 'Name'),
            ],
            'first_name_kana' => [
                'type' => 'input',
                'groupWith' => 'last_name_kana',
                'label' => Yii::t('backend', 'Name kana'),
            ],
            'sex' => [
                'type' => 'select',
                'label' => Yii::t('backend', 'Sex')
            ],
            'birth_date' => [
                'type' => 'input_birth_date',
                'birth_date_check' => true,
                'label' => Yii::t('backend', 'Birthday')
            ],
            'mobile' => [
                'type' => 'input',
                'label' => Yii::t('backend', 'Phone')
            ],
            'email' => [
                'type' => 'input_birth_email',
                'email_check' => true,
                'label' => Yii::t('backend', 'Email')
            ],
            'post_code' => [
                'type' => 'input_post_code',
                'post_code_check' => true,
                'label' => Yii::t('backend', 'PostCode')
            ],
            'address' => [
                'type' => 'input_post_address',
                'address_check' => true,
                'label' => Yii::t('backend', 'Address1')
            ],
            'address2' => [
                'type' => 'input_post_address2',
                'address2_check' => true,
                'label' => Yii::t('backend', 'Address2')
            ],
            'memo' => [
                'type' => 'input_post_memo',
                'memo_check' => true,
                'label' => Yii::t('backend', 'Memo')
            ],
            'news_transmision_flg' => [
                'type' => 'radio',
                'label' => Yii::t('backend', 'News transmision'),
                'model' => 'CustomerStore'
            ],
            'black_list_flg' => [
                'type' => 'checkbox',
                'label' => Yii::t('backend', 'Blacklist'),
                'model' => 'CustomerStore'
            ],
        ],
        "modelName" => $model->formName(),
        'idBtnConfirm' => 'btn-confirm',
        'formId' => strtolower($model->formName()) . '-id',
        'btnClose' => 'btn-close',
        'btnSubmit' => 'btn-submit',
        'modalId' => 'userModal'
    ])
    ?>
    <?php
    Modal::end();
    ?>
    <script>
        $("input[type=radio][name='CustomerStore[news_transmision_flg]'][value='1']").prop("disabled", true);
        $("#search-code").click(function (e) {
            getPostcode('#mastercustomer-post_code', '#mastercustomer-address');
        });
    </script>
    <?php ActiveForm::end(); ?>

</div>
<script>
    jQuery.fn.preventDoubleSubmission = function () {

        var last_clicked, time_since_clicked;

        $("#btn-submit").bind('click', function (event) {

            if (last_clicked)
                time_since_clicked = event.timeStamp - last_clicked;

            last_clicked = event.timeStamp;

            if (time_since_clicked < 2000)
                return false;

            return true;
        });
    };
    $('#mastercustomer-id').preventDoubleSubmission();

    $('#mastercustomer-register_staff_id').on('depdrop.init', function (event) {
        $('#customerstore-store_id').trigger('change');
    });

    $('#mastercustomer-register_staff_id').on('depdrop.afterChange', function (event, id, value) {
        $('#mastercustomer-register_staff_id').val(<?php echo $model->register_staff_id; ?>);
    });
</script>
