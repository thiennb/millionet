<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel common\models\MasterCustomerSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('backend', 'Customer list');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <div class="col-lg-12 col-md-12">
        <div class="box box-info box-solid">
            <div class="box-header with-border">
                <h4 class="text-title"><b><?= $this->title ?></b></h4>
            </div>
            <div class="box-body content">
                <div class="col-md-12">
                    <div class="common-box">
                        <div class="row">
                            <div class="box-header with-border common-box-h4 col-lg-8 col-md-10">
                                <h4 class="text-title"><b><?= Yii::t('backend', 'Customer search') ?></b></h4>
                            </div>
                        </div>

                        <div class="box-body content">
                            <div class="col-md-12 col-lg-10">
                                <?php echo $this->render('_search', ['model' => $searchModel]); ?>                                
                            </div>
                        </div>
                    </div>

                    <div class="common-box">
                        <div class="row">
                            <div class="box-header with-border common-box-h4 col-lg-8 col-md-10">
                                <h4 class="text-title"><b><?= Yii::t('backend', 'Customer list') ?></b></h4>
                            </div>
                        </div>
                        <div class="box-body content">
                            <div class="col-md-10">
                                <?= Html::a(Yii::t('backend', 'Create New'), ['create'], ['class' => 'btn common-button-submit common-float-right margin-bottom-10']) ?>

                                <?php Pjax::begin(); ?>    
                                <?=
                                GridView::widget([
                                    'dataProvider' => $dataProvider,
                                    'tableOptions' => [
                                        'class' => 'table table-striped table-bordered text-center',
                                    ],
                                    'layout' => "{pager}\n{summary}\n{items}",
                                    'summaryOptions' => ['class' => 'common-clr-fl-right'],
                                    'summary' => false,
                                    'pager' => [
                                        'class' => 'common\components\LinkPagerMillionet',
                                        'options' => ['class' => 'pagination common-float-right'],
                                    ],
                                    'columns' => [
                                        ['class' => 'yii\grid\SerialColumn', 'header' => 'No', 'options' => ['class' => 'with-table-no']],
                                        [
                                            'attribute' => 'store_name',
                                            'value' => 'store_name'
                                        ],
                                        [
                                            'attribute' => 'name',
                                            'value' => function ($model) {
                                                return $model->first_name . $model->last_name;
                                            }
                                        ],
                                        'customer_jan_code',
                                        [
                                            'attribute' => 'rank',
                                            'header' => Yii::t("backend", "Rank"),
                                            'value' => function ($model) {

                                                if ($model->rank_id == '00') {
                                                    return 'ノーマル';
                                                } else if ($model->rank_id == '01') {
                                                    return 'ブロンズ';
                                                } else if ($model->rank_id == '02') {
                                                    return 'シルバー';
                                                } else if ($model->rank_id == '03') {
                                                    return 'ゴールド';
                                                } else if ($model->rank_id == '04') {
                                                    return 'ブラック';
                                                } else {
                                                   return ""; 
                                                }
                                            }
                                        ],
                                        [
                                            'attribute' => 'number_visit',
                                            'value' => function ($model) {
                                                  $model = \common\models\CustomerStore::findOne($model->id_store); 
                                                  return $model->numberOrder;
                                               
                                            }
                                           
                                        ],
                                        [
                                            'attribute' => 'last_visit_date',
                                            'format' => ['date', 'php:Y/m/d'],
                                            'value' => function ($model) {
                                        return $model->last_visit_st;
                                    }
                                        //  'value' => 'last_visit_st',
                                        ],
                                        [
                                            'class' => 'yii\grid\ActionColumn',
                                            'options' => ['class' => 'with-table-button_product'],
                                            'contentOptions' => ['class' => 'text-center'],
                                            'template' => "{view}",
                                            'buttons' => [

                                                'view' => function ($url, $model) {
                                                    //  \common\components\Util::dd($model);
                                                    $id_store = $model->id_store;
                                                    $new_url = yii\helpers\Url::to(['master-customer/view', 'id' => $id_store]);
                                                    return Html::a('<span class="fa fa-delete"></span>' . Yii::t('backend', 'Detail'), $new_url, [

                                                                'class' => 'btn common-button-action',
                                                    ]);
                                                },
                                                    ],
                                                ],
                                            ],
                                        ]);
                                        ?>
                                    </div>
                                </div>
                            </div>
                                        <?php Pjax::end(); ?>
                </div>
            </div>
        </div>
    </div>
</div>
