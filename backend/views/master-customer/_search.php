<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use common\components\Constants;
use yii\jui\DatePicker;
use common\models\MasterStaff;
use common\models\MasterStore;
// Drop List
use kartik\depdrop\DepDrop;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\MasterCustomerSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<style>
    .row.row-inline {
        line-height: 32px;
    }
</style>
<?php
$fieldOptions1 = [
    'options' => ['class' => 'row row-inline'],
    'template' => "<div class='col-md-3'>{label}</div><div class='col-md-4'>{input}\n{hint}\n{error}</div>\n"
];
?>


<div class="master-customer-search col-md-6 with-form-coupon-box">

    <?php
    $form = ActiveForm::begin([
                'action' => ['index'],
                'enableClientValidation' => false,
                'method' => 'get',
                'enableClientValidation' => false,
    ]);
    ?>

    <?=
    $form->field($model, 'customer_jan_code', $fieldOptions1)->widget(\yii\widgets\MaskedInput::className(), [
        'mask' => '9',
        'clientOptions' => ['repeat' => 13, 'greedy' => false]
    ])->textInput(['maxlength' => 13])
    ?>

    <?= $form->field($model, 'name', $fieldOptions1)->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'sex', $fieldOptions1)->dropDownList(Constants::LIST_SEX, ['prompt' => Yii::t('backend', 'Please Select')]) ?>

    <div class="row row-inline">
        <div class="col-md-3">
            <label class='mws-form-label'>
                <?= Yii::t('backend', 'Visit number') ?>
            </label>
        </div>
        <div class="col-md-9 ">
            <?=
            $form->field($model, 'number_visit_min', [
                'options' => ['class' => 'col-md-5 no-padding'],
                'template' => '<div class=" clear-padding">{input}{error}</div>',
                'inputTemplate' => '<div class="input-group">{input}<span class="input-group-addon"><b>回以上</b></span></div>',
            ])->widget(\yii\widgets\MaskedInput::className(), [
                'clientOptions' => [
                    'groupSeparator' => ',',
                    'alias' => 'integer',
                    'autoGroup' => true,
                    'removeMaskOnSubmit' => true,
                    'allowMinus' => false,
                ]
            ])->textInput(['maxlength' => true])
            ?>

            <?=
            $form->field($model, 'number_visit_max', [
                'options' => ['class' => 'col-md-offset-7 no-padding'],
                'template' => '<div class="clear-padding">{input}{error}</div>',
                'inputTemplate' => '<div class="input-group">{input}<span class="input-group-addon"><b>回以下</b></span></div>',
            ])->widget(\yii\widgets\MaskedInput::className(), [
                'clientOptions' => [
                    'groupSeparator' => ',',
                    'alias' => 'integer',
                    'autoGroup' => true,
                    'removeMaskOnSubmit' => true,
                    'allowMinus' => false,
                ]
            ])->textInput(['maxlength' => true])
            ?>
        </div>
    </div>

    <div class="row row-inline">
        <div class="col-md-3">
            <label class='mws-form-label'>
                <?= Yii::t('backend', 'Birthday') ?>
            </label>
        </div>
        <div class="col-md-9 ">
            <?=
            $form->field($model, 'birth_date_from', [
                'options' => ['class' => 'col-md-5 no-padding with-form-customer-date'],
                'template' => '<div class="clear-padding">{input}{error}</div>'
            ])->widget(DatePicker::classname(), [
                'language' => 'ja',
                'dateFormat' => 'yyyy/MM/dd',
                'clientOptions' => [
                    "changeMonth" => true,
                    "changeYear" => true,
                    "yearRange" => "1900:+0"
                ],
            ])->textInput(['maxlength' => 10])
            ?>
            <div class="text-center col-md-2">～</div>
            <?=
            $form->field($model, 'birth_date_to', [
                'options' => ['class' => 'col-md-5 no-padding with-form-customer-date'],
                'template' => '<div class="clear-padding">{input}{error}</div>'
            ])->widget(DatePicker::classname(), [
                'language' => 'ja',
                'dateFormat' => 'yyyy/MM/dd',
                'clientOptions' => [
                    "changeMonth" => true,
                    "changeYear" => true,
                    "yearRange" => "1900:+0"
                ],
            ])->textInput(['maxlength' => 10])
            ?>
        </div>
    </div>

    <div class="row row-inline">
        <div class="col-md-3">
            <label class='mws-form-label'>
                <?= Yii::t('backend', 'Last visit time') ?>
            </label>
        </div>
        <div class="col-md-9 ">
            <?=
            $form->field($model, 'last_visit_from', [
                'options' => ['class' => 'col-md-5 no-padding with-form-customer-date'],
                'template' => '<div class="clear-padding">{input}{error}</div>'
            ])->widget(DatePicker::classname(), [
                'language' => 'ja',
                'dateFormat' => 'yyyy/MM/dd',
                'clientOptions' => [
                    "changeMonth" => true,
                    "changeYear" => true,
                    "yearRange" => "1900:+0"
                ],
            ])->textInput(['maxlength' => 10])
            ?>
            <div class="text-center col-md-2">～</div>
            <?=
            $form->field($model, 'last_visit_to', [
                'options' => ['class' => 'col-md-5 no-padding with-form-customer-date'],
                'template' => '<div class="clear-padding">{input}{error}</div>'
            ])->widget(DatePicker::classname(), [
                'language' => 'ja',
                'dateFormat' => 'yyyy/MM/dd',
                'clientOptions' => [
                    "changeMonth" => true,
                    "changeYear" => true,
                    "yearRange" => "1900:+0"
                ],
            ])->textInput(['maxlength' => 10])
            ?>
        </div>
    </div>

    <?= $form->field($model, 'register_store_id', $fieldOptions1)->dropDownList(MasterStore::getListStore(), ['prompt' => Yii::t('backend', 'Please Select')]) ?> 

    <?= $form->field($model, 'rank', $fieldOptions1)->dropDownList(Constants::RANK, ['prompt' => Yii::t('backend', 'Please Select')]) ?>

    <?=
    $form->field($model, 'last_staff_id', $fieldOptions1)->widget(DepDrop::classname(), [
        'options' => ['id' => 'mastercustomersearch-last_staff_id'],
        'pluginOptions' => [
            //'init' => true,
            'depends' => ['mastercustomersearch-register_store_id'],
            'placeholder' => ' ',
            'url' => Url::to(['/master-customer/select-staff-by-id-store'])
        ]
    ]);
    ?>


    <div class="form-group">
        <?= Html::a(Yii::t('backend', 'Return'), ['/'], ['class' => 'btn common-button-default btn-default']) ?>
        <?= Html::submitButton(Yii::t('backend', 'Search'), ['class' => 'btn common-button-submit']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>


<script>
    $('#mastercustomersearch-last_staff_id').on('depdrop.init', function (event) {
        $('#mastercustomersearch-register_store_id').trigger('change');
    });

    $('#mastercustomersearch-last_staff_id').on('depdrop.afterChange', function (event, id, value) {
        var value = <?php echo (int)$model->last_staff_id; ?>;
        var result;
        
        if(value.toString().length == 1){
            result = '0000'+value;
        }else if(value.toString().length == 2){
            result = '000'+value;
        }else if(value.toString().length == 3){
            result = '00'+value;
        }else if(value.toString().length == 4){
            result = '0'+value;
        }else if(value.toString().length == 5){
            result = value;
        }
        
        $('#mastercustomersearch-last_staff_id').val(result);
    });
</script>
