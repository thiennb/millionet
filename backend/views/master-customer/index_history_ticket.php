

<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use common\models\MasterStaff;

/* @var $this yii\web\View */
/* @var $searchModel common\models\MasterCustomerSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('backend', 'Tickets history');
$this->params['breadcrumbs'][] = $this->title;
?>
<style>
    .pagination{ 
        display: table; 
        margin: 0 auto; 
        padding: 10px;
    }
    .pagination a{ 
        margin: 0 2px; 
    }
</style>
<div class="row">
    <div class="col-lg-12 col-md-12">
        <div class="box box-info box-solid">
            <div class="box-header with-border">
                <h4 class="text-title"><b><?= $this->title ?></b></h4>
            </div>
            <div class="box-body content">
                <div class="col-md-12">
                    <div class="common-box">
                        <div class="row">
                            <div class="box-header with-border common-box-h4 col-lg-8 col-md-12">
                                <h4 class="text-title"><b><?= Yii::t('backend', 'Customer Search History Ticket') ?></b></h4>
                            </div>
                        </div>

                        <div class="box-body content">
                            <div class="col-lg-10 col-md-12">
                                <?php echo $this->render('_search_history_ticket', ['model' => $searchModel]); ?>                                
                            </div>
                        </div>
                    </div>
                    <?php
                    Pjax::begin();
                    ?>    
                    <div class="common-box">
                        <div class="row">
                            <div class="box-header with-border common-box-h4 col-lg-8 col-md-12">
                                <h4 class="text-title"><b><?= Yii::t('backend', 'Customer Search List History Ticket') ?></b></h4>
                            </div>
                        </div>
                        <div class="box-body content">
                            <div class="col-md-10">

                                <?php Pjax::begin(); ?>    
                                <?=
                                GridView::widget([
                                    'dataProvider' => $dataProvider,
                                    'tableOptions' => [
                                        'class' => 'table table-striped table-bordered text-center',
                                    ],
                                    'layout' => "{pager}\n{summary}\n{items}",
                                    'summaryOptions' => ['class' => 'common-clr-fl-right'],
                                    'summary' => false,
                                    'pager' => [
                                        'class' => 'common\components\LinkPagerMillionet',
                                        'options' => ['class' => 'pagination'],
                                    ],
                                    'columns' => [
                                        ['class' => 'yii\grid\SerialColumn', 'header' => 'No', 'options' => ['class' => 'with-table-no']],
                                        [
                                            'value' => function ($model) {
                                                return empty($model->order) ? "" : (empty($model->order->process_date) ? "" : date('Y/m/d', strtotime($model->order->process_date)) . " " . $model->order->process_time);
                                            },
                                            'label' => Yii::t('backend', 'Process Date')
                                        ],
                                        [
                                            'value' => function ($model) {
                                                if (!empty($model->order_code)) {
                                                    $store = \common\models\MasterStore::findOne(['store_code' => substr($model->order_code, 0, 5)]);
                                                }
                                                return empty($store) ? "" : $store->name;
                                            },
                                                    'header' => Yii::t('backend', 'Store Id'),
                                                ],
                                                [
                                                    'value' => function ($model) {
                                                        $customer = empty($model->order) ? "" : $model->order->masterCustomer;
                                                        return empty($customer) ? "" : $customer->first_name . $customer->last_name;
                                                    },
                                                    'label' => Yii::t('backend', 'Name Customer')
                                                ],
                                                [
                                                    'value' => function ($model) {
                                                        return empty($model->ticket) ? "" : $model->ticket->name;
                                                    },
                                                    'label' => Yii::t('backend', 'Ticket')
                                                ],
                                                [
                                                    'value' => function ($model) {
                                                        $staff = empty($model->order) ? "" : $model->order->management;
                                                        return empty($staff->staff) ? "" : $staff->staff->name;
                                                    },
                                                    'label' => Yii::t('backend', 'People Staff')
                                                ],
                                                [
                                                    'attribute' => 'process_type',
                                                    'header' => Yii::t("backend", "Use or change"),
                                                    'value' => function ($model) {
                                                        return ($model->process_type == 1) ? '使用' : ($model->process_type == 0 ? "購入" : "");
                                                    }
                                                ],
                                            ],
                                        ]);
                                        ?>
                                    </div>
                                </div>
                            </div>
                            <?php Pjax::end(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

