<?php

    use yii\helpers\Html;
    use yii\bootstrap\ActiveForm;
    use yii\widgets\Pjax;
    use common\components\Constants;
    use common\models\Booking;
/* @var $this yii\web\View */
    /* @var $model common\models\Booking */
?>
<style>
  .master-booking-view{
    font-size: 17px;
  }
  .master-booking-view .row{
      margin-bottom: 10px;
  }
  .master-booking-view .btn-action{
    padding: 6px 10px;
    font-size: 16px;
  }
</style>
<div class="master-booking-view">
    <div class="row">
      <div class="box-header with-border common-box-h4 col-md-4">
        <h4 class="text-title"><b><?= Yii::t('backend', 'Booking detail') ?></b></h4>
      </div>
    </div>


    
    <div class="row">
      <div class="col-md-3">
        <b><?= Yii::t('backend', 'Customer name') ?></b>
      </div>
      <div class="col-md-3">
        <?= $model->masterCustomer->first_name. $model->masterCustomer->last_name ?>
      </div>
      <div class="col-md-3">
        <?= $model->imageBlackList; ?>
      </div>
      <div class="col-md-3" style="font-size: 20px; color: blue">
        <?= $model->booking_code ?>
      </div>
    </div>
    <div class="row">
      <div class="col-md-3">
        <b><?= Yii::t('backend', 'Contact') ?></b>
      </div>
      <div class="col-md-9">
        <?= $model->masterCustomer->mobile ?>
      </div>
    </div>
    <div class="row">
      <div class="col-md-3">
        <b><?= Yii::t('backend', 'Booking Store') ?></b>
      </div>
      <div class="col-md-9">
        <?= $model->storeMaster->name ?>
      </div>
    </div>
    <div class="row">
      <div class="col-md-3">
        <b><?= Yii::t('backend', 'Booking Date Time') ?></b>
      </div>
      <div class="col-md-9">
        <?= Yii::$app->formatter->asDate($model->booking_date). " ".$model->start_time . " ～ " . Yii::$app->formatter->asDate($model->booking_date). " ".$model->end_time ?>
      </div>
    </div>
  <?php Pjax::begin(['enablePushState'=>false , 'id'=>'p1']) ?>
  <?php
      $form = ActiveForm::begin([
              
            'id' => 'form-booking-view',
            'options'=>[
              'data-pjax' => ''
              ]
      ]);
  ?>
    <div class="row">
      <div class="col-md-3">
        <b><?= Yii::t('backend', 'Booking status') ?></b>
      </div>
      <div class="col-md-9">
        <?= Constants::LIST_BOOKING_STATUS_KANRI[$model->status] ?>
        <?php
            if($model->status == Booking::BOOKING_STATUS_PENDING){
              echo Html::submitButton(Yii::t('backend', 'Approval') ,['class'=>'btn common-button-submit btn-action','name'=>'btnAction','value'=> Booking::BOOKING_STATUS_APPROVE] );
              echo Html::submitButton(Yii::t('backend', 'Denial') ,['class'=>'btn common-button-submit btn-action','name'=>'btnAction','value'=>  Booking::BOOKING_STATUS_DENY] );
              echo Html::submitButton(Yii::t('backend', 'Cancel') ,['class'=>'btn common-button-submit btn-action','name'=>'btnAction','value'=>  Booking::BOOKING_STATUS_CANCEL] );
            }
            if($model->status == Booking::BOOKING_STATUS_APPROVE){
              echo Html::submitButton(Yii::t('backend', 'Cancel') ,['class'=>'btn common-button-submit btn-action','name'=>'btnAction','value'=>  Booking::BOOKING_STATUS_CANCEL] );
            }
            if($model->status == Booking::BOOKING_STATUS_FINISH){
              echo Html::submitButton(Yii::t('backend', 'Cancel') ,['class'=>'btn common-button-submit btn-action','name'=>'btnAction','value'=>  Booking::BOOKING_STATUS_CANCEL] );
            }
            ?>
        
      </div>
    </div>
  <?php ActiveForm::end() ?>
  <?php        Pjax::end() ?>
    <div class="row">
      <div class="col-md-3">
        <b><?= Yii::t('backend', 'Responsible') ?></b>
      </div>
      <div class="col-md-9">
        <?= $model->staff->name ?>
      </div>
    </div>
    <div class="row">
      <div class="col-md-3">
        <b><?= Yii::t('backend', 'Product') ?></b>
      </div>
      <div class="col-md-9">
        <?= count($productName) > 0 ? implode(',', $productName) : "" ?>
      </div>
    </div>
    <div class="row">
      <div class="col-md-3">
        <b><?= Yii::t('backend', 'Option') ?></b>
      </div>
      <div class="col-md-9">
        <?= count($optionName) > 0 ? implode(',', $optionName) : "" ?>
      </div>
    </div>
    <div class="row">
      <div class="col-md-3">
        <b><?= Yii::t('backend', 'Coupon') ?></b>
      </div>
      <div class="col-md-9">
        <?= count($couponName) > 0 ? implode(',', $couponName) : "" ?>
        
      </div>
    </div>
    <div class="row">
      <div class="col-md-3">
        <b><?= Yii::t('backend', 'Demand') ?></b>
      </div>
      <div class="col-md-9">
        <?= $model->demand ?>
      </div>
    </div>
    <div class="row">
      <div class="col-md-3">
        <b><?= Yii::t('backend', 'Memo manager') ?></b>
      </div>
      <div class="col-md-9">
        <?= $model->memo_manager ?>
      </div>
    </div> 
    <div class="row">
      <div class="col-md-3">
        <b><?= Yii::t('backend', 'Create at') ?></b>
      </div>
      <div class="col-md-3">
        <?= date('Y/m/d H:i', $model->created_at)  ?>
      </div>
      <div class="col-md-3">
        <b><?= Yii::t('backend', 'Update at') ?></b>
      </div>
      <div class="col-md-3">
        <?= date('Y/m/d H:i', $model->updated_at)  ?>
      </div>
    </div> 


</div>

<script>
  
  
</script>  
