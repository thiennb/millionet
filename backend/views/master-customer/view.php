<?php

use yii\helpers\Html;
use common\components\Constants;

/* @var $this yii\web\View */
/* @var $model common\models\MasterCustomer */

$this->title = Yii::t('backend', 'Detail customer');
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Customer list'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<style>
    .btn-history{
        min-width: 180px;
    }
    .row.h4 {
        min-height: 40px;
    }
</style>
<div class="row text-setting company">
    <div class="col-lg-12 col-md-12">
        <div class="box box-info box-solid add">
            <div class="box-header with-border">
                <h4 class="text-title"><b><?= $this->title ?></b></h4>
            </div>
            <div class="box-body content">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="common-box">
                                <div class="box-header with-border common-box-h4 col-md-6">
                                    <h4 class="text-title"><b><?= Yii::t('backend', 'Detail customer') ?></b></h4>
                                </div>

                            </div>


                        </div>
                    </div>
                    <div class="row h4">
                        <div class="col-md-2">
                            <?= Html::label(Yii::t('backend', 'Membership card number')) ?> 
                        </div>
                        <div class="col-md-2">
                            <?= $model->customer->customer_jan_code ?> 
                        </div>
                    </div>
                    <div class="row h4">
                        <div class="col-md-2">
                            <?= Html::label(Yii::t('backend', 'Name')) ?> 
                        </div>
                        <div class="col-md-2">
                            <?= $model->customer->first_name . $model->customer->last_name ?> 
                        </div>
                    </div>
                    <div class="row h4">
                        <div class="col-md-2">
                            <?= Html::label(Yii::t('backend', 'Name kana')) ?> 
                        </div>
                        <div class="col-md-2">
                            <?= $model->customer->first_name_kana . $model->customer->last_name_kana ?> 
                        </div>
                    </div>
                    <div class="row h4">
                        <div class="col-md-2">
                            <?= Html::label(Yii::t('backend', 'Sex')) ?> 
                        </div>
                        <div class="col-md-2">
                            <?php
                            if ($model->customer->sex === '1' || $model->customer->sex === '0') 
                            {
                                echo common\components\Constants::LIST_SEX[$model->customer->sex];
                           
                            } else {
                               
                            }
                            ?>
                        </div>
                    </div>
                    <div class="row h4">
                        <div class="col-md-2">
                            <?= Html::label(Yii::t('backend', 'Birthday')) ?> 
                        </div>
                        <div class="col-md-2">
                            <?= Yii::$app->formatter->asDate($model->customer->birth_date) ?> 
                        </div>
                    </div>
                    <div class="row h4">
                        <div class="col-md-2">
                            <?= Html::label(Yii::t('backend', 'Phone')) ?> 
                        </div>
                        <div class="col-md-2">
                            <?= $model->customer->mobile ?> 
                        </div>
                    </div>
                    <div class="row h4">
                        <div class="col-md-2">
                            <?= Html::label(Yii::t('backend', 'Email')) ?> 
                        </div>
                        <div class="col-md-2">
                            <?= $model->customer->email ?> 
                        </div>
                    </div>
                    <div class="row h4">
                        <div class="col-md-2">
                            <?= Html::label(Yii::t('backend', 'PostCode')) ?> 
                        </div>
                        <div class="col-md-2">
                            <?php
                            if (empty($model->customer->post_code) || $model->customer->post_code == null) {
                                
                            } else {
                                echo substr($model->customer->post_code, 0, 3) . '-' . substr($model->customer->post_code, 3, 5);
                            }
                            ?>

                        </div>
                    </div>
                    <div class="row h4">
                        <div class="col-md-2">
                            <?= Html::label(Yii::t('backend', 'Address1')) ?> 
                        </div>
                        <div class="col-md-2">
                            <?= $model->customer->address ?> 
                        </div>
                    </div>
                    <div class="row h4">
                        <div class="col-md-2">
                            <?= Html::label(Yii::t('backend', 'Address2')) ?> 
                        </div>
                        <div class="col-md-2">
                            <?= $model->customer->address2 ?> 
                        </div>
                    </div>
                    <div class="row h4">
                        <div class="col-md-2">
                            <?= Html::label(Yii::t('backend', 'Memo')) ?> 
                        </div>
                        <div class="col-md-2">
                            <?= $model->customer->memo ?> 
                        </div>
                    </div>
                    <div class="row h4">
                        <div class="col-md-2">
                            <?= Html::label(Yii::t('backend', 'News transmision')) ?> 
                        </div>
                        <div class="col-md-2">
                            <?php
                            if ($model->news_transmision_flg == 1) {
                                echo Yii::t('backend', "送信可");
                            } else {
                                echo Yii::t('backend', "送信不可");
                            }
                            //$model->customer->news_transmision ? Yii::t('backend', "送信可") : Yii::t('backend', "送信不可") 
                            ?> 
                        </div>
                    </div>
                    <div class="row h4">
                        <div class="col-md-2">
                            <?= Html::label(Yii::t('backend', 'Blacklist')) ?> 
                        </div>
                        <div class="col-md-2">
                            <?php
                            if ($model->black_list_flg == 1) {
                                echo Yii::t('backend', "あり");
                            } else {
                                echo Yii::t('backend', "なし");
                            }
                            ?>

                        </div>
                    </div>
                    <div class="row h4">
                        <div class="col-md-2">
                            <?= Html::label(Yii::t('backend', 'Register store')) ?> 
                        </div>
                        <div class="col-md-2">
                            <?php
                            if (isset($model->store->name)) {
                                echo $model->store->name;
                            } else {
                                
                            }
                            ?>
                        </div>
                    </div>
                    <div class="row h4">
                        <div class="col-md-2">
                            <?= Html::label(Yii::t('backend', 'Customers rank')) ?> 
                        </div>
                        <div class="col-md-2">
                            <?php
                            if ($model->rank_id == '00') {
                                echo Yii::t('backend', "ノーマル");
                            } else if ($model->rank_id == '01') {
                                echo Yii::t('backend', "ブロンズ");
                            } else if ($model->rank_id == '02') {
                                echo Yii::t('backend', "シルバー");
                            } else if ($model->rank_id == '03') {
                                echo Yii::t('backend', "ゴールド");
                            } else if ($model->rank_id == '04') {
                                echo Yii::t('backend', "ブラック");
                            }
                            ?>
                        </div>
                    </div>
                    <div class="row h4">
                        <div class="col-md-2">
                            <?= Html::label(Yii::t('backend', 'Total money')) ?> 
                        </div>
                        <div class="col-md-2">
                            <?= '￥' . $model->total_amount ?> 
                        </div>
                    </div>


                    <div class="row h4">
                        <div class="col-md-2">
                            <?= Html::label(Yii::t('backend', 'First visit date')) ?> 
                        </div>
                        <div class="col-md-2">
                            <?= Yii::$app->formatter->asDate($model->orderFirstDay) ?> 
                        </div>
                    </div>
                    <div class="row h4">
                        <div class="col-md-2">
                            <?= Html::label(Yii::t('backend', 'Last visit date')) ?> 
                        </div>
                        <div class="col-md-3">
                            <?= Yii::$app->formatter->asDate($model->orderLastDay) ?> 
                           
                        </div>
                    </div>
                    <div class="row h4">
                        <div class="col-md-2">
                            <?= Html::label(Yii::t('backend', 'Visit number')) ?> 
                        </div>
                        <div class="col-md-2">
                            <?= $model->numberOrder ?> 
                        </div>
                        <div class="col-md-3">
                            <?= Html::a(Yii::t('backend', 'Visit history'), ['/master-customer/index-visit-history' . '?customer_id=' . $model->customer_id], ['class' => 'btn common-button-submit btn-history']) ?>
                        </div>
                    </div>

                    <div class="row h4">
                        <div class="col-md-2">
                            <?= Html::label(Yii::t('backend', 'Reservation number')) ?> 
                        </div>
                        <div class="col-md-2">
                            <?= $model->numberBooking ?> 
                        </div>
                        <div class="col-md-3">
                            <?= Html::a(Yii::t('backend', 'Reservation history'), ['/master-customer/index-booking-history' . '?customer_id=' . $model->customer_id], ['class' => 'btn common-button-submit btn-history']) ?>
                        </div>
                    </div>
                    <div class="row h4">
                        <div class="col-md-2">
                            <?= Html::label(Yii::t('backend', 'Point')) ?> 
                        </div>
                        <div class="col-md-2">
                            <?= Yii::$app->formatter->asInteger($model->total_point) . 'P' ?> 
                        </div>
                        <div class="col-md-3">
                            <?= Html::a(Yii::t('backend', 'Point history'), ['/master-customer/index-history-point' . '?customer_id=' . $model->customer_id], ['class' => 'btn common-button-submit btn-history']) ?>
                        </div>
                    </div>
                    <div class="row h4">
                        <div class="col-md-2">
                            <?= Html::label(Yii::t('backend', 'Tickets balance')) ?> 
                        </div>
                        <div class="col-md-2">
                            <?= $model->tickets_balance ?> 
                        </div>
                        <div class="col-md-3">
                            <?= Html::a(Yii::t('backend', 'Tickets history'), ['/master-customer/index-history-ticket?customer_id=' . $model->customer_id], ['class' => 'btn common-button-submit btn-history']) ?>
                        </div>
                    </div>
                    <div class="row h4">
                        <div class="col-md-2">
                            <?= Html::label(Yii::t('backend', 'Charge balance')) ?> 
                        </div>
                        <div class="col-md-2">
                            <?= '￥' . $model->charge_balance ?> 
                        </div>
                        <div class="col-md-3">
                            <?= Html::a(Yii::t('backend', 'Charge history'), ['/master-customer/index-charge-history?customer_id=' . $model->customer_id], ['class' => 'btn common-button-submit btn-history']) ?>
                        </div>
                    </div>

                    <div class="row" >
                        <?= Html::a(Yii::t('backend', 'Return'), ['index'], ['class' => 'btn btn-default common-button-default']) ?>
                        <?= Html::a(Yii::t('backend', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn common-button-submit']) ?>
                        <?=
                        Html::a(Yii::t('backend', 'Delete'), ['delete', 'id' => $model->customer->id], [
                            'class' => 'btn common-button-submit',
                            'data' => [
                                'confirm' => Yii::t('backend', "Delete Customer Error Message"),
                                'method' => 'post',
                            ],
                        ])
                        ?>

                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
