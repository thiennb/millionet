<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use common\models\MasterStaff;

/* @var $this yii\web\View */
/* @var $searchModel common\models\ChargeHistorySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('backend', 'Charge history');
$this->params['breadcrumbs'][] = $this->title;
?>
<style>
    .pagination{ 
        display: table; 
        margin: 0 auto; 
        padding: 10px;
    }
    .pagination a{ 
        margin: 0 2px; 
    }
</style>
<div class="row">
    <div class="col-lg-12 col-md-12">
        <div class="box box-info box-solid">
            <div class="box-header with-border">
                <h4 class="text-title"><b><?= $this->title ?></b></h4>
            </div>
            <div class="box-body content">
                <div class="col-md-12">
                    <div class="common-box">
                        <div class="row">
                            <div class="box-header with-border common-box-h4 col-lg-8 col-md-12">
                                <h4 class="text-title"><b><?= Yii::t('backend', 'Customer Search Charge History') ?></b></h4>
                            </div>
                        </div>

                        <div class="box-body content">
                            <div class="col-lg-10 col-md-12">
                                <?php echo $this->render('_search_charge_history', ['model' => $searchModel]); ?>                                
                            </div>
                        </div>
                    </div>
                    <?php
                    Pjax::begin();
                    ?>    
                    <div class="common-box">
                        <div class="row">
                            <div class="box-header with-border common-box-h4 col-lg-8 col-md-12">
                                <h4 class="text-title"><b><?= Yii::t('backend', 'Customer List Charge History') ?></b></h4>
                            </div>
                        </div>
                        <div class="box-body content">
                            <div class="col-md-10">
                                <?php Pjax::begin(); ?>    
                                <?=
                                GridView::widget([
                                    'dataProvider' => $dataProvider,
                                    'tableOptions' => [
                                        'class' => 'table table-striped table-bordered text-center',
                                    ],
                                    'layout' => "{pager}\n{summary}\n{items}",
                                    'summaryOptions' => ['class' => 'common-clr-fl-right'],
                                    'summary' => false,
                                    'pager' => [
                                        'class' => 'common\components\LinkPagerMillionet',
                                        'options' => ['class' => 'pagination'],
                                    ],
                                    'columns' =>
                                    [
                                        ['class' => 'yii\grid\SerialColumn', 'header' => 'No', 'options' => ['class' => 'with-table-no']],
                                        [
                                            'attribute' => 'process_date',
                                            'header' => Yii::t("backend", 'Process Date'),
                                            'format' => ['date', 'php:Y/m/d'],
                                            'value' => function ($model) {
                                        return empty($model->process_date) ? "" : date('Y/m/d', strtotime($model->process_date)) . " " . $model->process_time;
                                    }
                                        ],
                                        [
                                            'value' => function ($model) {
                                                return !empty($model->store) ? $model->store->name : "";
                                            },
                                            'header' => Yii::t("backend", 'Store Id'),
                                        ],
                                        [
                                            'attribute' => 'name',
                                            'value' => function ($model) {
                                                return !empty($model->customerMaster) ? $model->customerMaster->first_name . $model->customerMaster->last_name : "";
                                            }
                                        ],
                                        [
                                            'attribute' => 'process_type',
                                            'header' => Yii::t("backend", "Use or change"),
                                            'value' => function ($model) {
                                                return ($model->process_type == 1) ? '使用' : ($model->process_type == 0 ? "購入" : "");
                                            }
                                        ],
                                        [
                                            'attribute' => 'process_money',
                                            'header' => Yii::t("backend", "Balance Money"),
                                            'value' => function ($model) {
                                                //return $model->process_money;
                                                if ($model->process_money == null || empty($model->process_money)) {
                                                    return '¥0';
                                                } else {
                                                    return '¥' . Yii::$app->formatter->asDecimal($model->process_money, 0);
                                                }
                                            }
                                        ],
                                        [
                                            'value' => function ($model) {
                                                $staff = !isset($model->management) ? "" : $model->management->staff;
                                                return empty($staff) ? "" : $staff->name;
                                            },
                                            'label' => Yii::t('backend', 'People Staff')
                                        ],
                                    // 'sex',
                                    // 'address',
                                    // 'post_code',
                                    // 'tel',
                                    // 'mobile',
                                    // 'email:email',
                                    // 'memo:ntext',
                                    // 'point',
                                    // 'created_at',
                                    // 'updated_at',
                                    // 'del_flg',
                                    // 'number_visit',
                                    // 'last_visit_date',
                                    // 'first_visit_date',
                                    // 'total_amount',
                                    // 'reservation_number',
                                    // 'tickets_balance',
                                    // 'charge_balance',
                                    // 'note:ntext',
                                    ],
                                ]);
                                ?>
                            </div>
                        </div>
                    </div>
                    <?php Pjax::end(); ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
