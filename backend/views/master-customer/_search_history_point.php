<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use common\components\Constants;
use yii\jui\DatePicker;
use common\models\MasterStaff;
use common\models\MasterStore;
use yii\helpers\Url;
use kartik\depdrop\DepDrop;

/* @var $this yii\web\View */
/* @var $model common\models\MasterCustomerSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<style>
    .row.row-inline {
        line-height: 32px;
    }
</style>
<?php
$fieldOptions1 = [
    'options' => ['class' => 'row row-inline'],
    'template' => "<div class='col-md-3'>{label}</div><div class='col-md-4'>{input}\n{hint}\n{error}</div>\n"
];
?>


<div class="master-customer-search">

    <?php
    $form = ActiveForm::begin([
                'action' => ['index-history-point'],
                'enableClientValidation' => false,
                'method' => 'get',
                'enableClientValidation' => false,
    ]);
    ?>
    <div class="col-md-12">
        <div class="col-lg-2 col-md-3 font_label" >
            <?= Yii::t('backend', 'Store Id') ?>
        </div>
        <div class="col-lg-2 col-md-4 input_store_ct" >
            <?php
            $role = \common\components\FindPermission::getRole();
            if ($role->permission_id == Constants::STORE_MANAGER_ROLE || $role->permission_id == Constants::STAFF_ROLE) {
                echo $form->field($model, 'register_store_id')->label(false)->dropDownList(MasterStore::getListStore());
            } else {
                echo $form->field($model, 'register_store_id')->label(false)->dropDownList(MasterStore::getListStore(), ['prompt' => Yii::t('backend', 'Please Select')]);
            }
            ?>

        </div>
    </div>

    <div class="col-md-12">
        <div class="col-lg-2 col-md-3 font_label" >
            <?= Yii::t('backend', 'People Staff') ?>
        </div>
        <div class="col-lg-2 col-md-4 input_store_ct" >
            <?= Html::hiddenInput('staff-id', $model->register_staff_id, ['id' => 'staff-id']); ?>
            <?=
            $form->field($model, 'register_staff_id')->widget(DepDrop::classname(), [
                'options' => ['id' => 'register_staff_id'],
                'pluginOptions' => [
                    //'init' => true,
                    'depends' => [Html::getInputId($model, 'register_store_id'),],
                    'placeholder' => ' ',
                    'url' => Url::to(['/master-customer/select-staff-by-id-store']),
                    'initialize' => true,
                    'params' => ['staff-id']
                ]
            ])->label(false);
            ?>
        </div>
    </div>

    <div class="col-md-12">
        <div class="col-lg-2 col-md-3 font_label" >
            <?= Yii::t('backend', 'Date History') ?>
        </div>
        <div class="col-lg-2 col-md-4 input_sale_report_ct" >
            <?=
            $form->field($model, 'date_history_from')->widget(DatePicker::classname(), [
                'language' => 'ja',
                'dateFormat' => 'yyyy/MM/dd',
                'clientOptions' => [
                    "changeMonth" => true,
                    "changeYear" => true,
                    "yearRange" => "1900:+0"
                ],
            ])->textInput(['maxLength' => 10])->label(false)
            ?>
        </div>
        <div  class="col-md-1 text-center separator">～</div>
        <div class="col-lg-2 col-md-4 input_sale_report_ct">
            <?=
            $form->field($model, 'date_history_to')->widget(DatePicker::classname(), [
                'language' => 'ja',
                'dateFormat' => 'yyyy/MM/dd',
                'clientOptions' => [
                    "changeMonth" => true,
                    "changeYear" => true,
                    "yearRange" => "1900:+0"
                ],
            ])->textInput(['maxLength' => 10])->label(false)
            ?>
        </div>
    </div>

    <div class="col-md-12">
        <div class="col-lg-2 col-md-3 font_label" >
            <?= Yii::t('backend', 'Process Type') ?>
        </div>
        <div class="col-lg-2 col-md-4 input_store_ct" >
            <?= $form->field($model, 'process_type')->dropDownList(Constants::POINT_HISTORY, ['prompt' => Yii::t('backend', 'Select all')])->label(false) ?>
        </div>
    </div>

    <div class="col-md-12">
        <div class="col-lg-2 col-md-3 font_label" >
            <?= Yii::t('backend', 'Total Point') ?>
        </div>
        <div class="col-lg-2 col-md-4 input_sale_report_ct" >
            <?=
            $form->field($model, 'total_point_from')->widget(\yii\widgets\MaskedInput::className(), [
                'clientOptions' => [
                    'groupSeparator' => ',',
                    'alias' => 'decimal',
                    'autoGroup' => true,
                    'removeMaskOnSubmit' => true,
                    'allowMinus' => false,
                ]
            ])->textInput(['maxlength' => 13])->label(false)
            ?>
        </div>
        <div  class="col-md-1 text-center separator"><i class="fa fa-long-arrow-right"></i></div>
        <div class="col-lg-2 col-md-4 input_sale_report_ct">
            <?=
            $form->field($model, 'total_point_to')->widget(\yii\widgets\MaskedInput::className(), [
                'clientOptions' => [
                    'groupSeparator' => ',',
                    'alias' => 'decimal',
                    'autoGroup' => true,
                    'removeMaskOnSubmit' => true,
                    'allowMinus' => false,
                ]
            ])->textInput(['maxlength' => 13])->label(false)
            ?>
        </div>
    </div>

    <div class="col-md-12">
        <div class="col-lg-2 col-md-3 font_label" >
            <?= Yii::t('backend', "Membership card number") ?>
        </div>
        <div class="col-lg-2 col-md-4 input_store_ct" >
            <?=
            $form->field($model, 'customer_jan_code')->widget(\yii\widgets\MaskedInput::className(), [
                'mask' => '9',
                'clientOptions' => ['repeat' => 13, 'greedy' => false]
            ])->textInput(['maxlength' => true])->label(false)
            ?>
        </div>
    </div>
    <div class="col-md-12">
        <div class="col-lg-2 col-md-3 font_label" >
            <?= Yii::t('backend', 'Name Customer') ?>
        </div>
        <div class="col-lg-2 col-md-4 input_store_ct" >
            <?= $form->field($model, 'name')->textInput(['maxlength' => true])->label(false) ?>
        </div>
    </div>
    <div class="col-md-12">
        <div class="col-lg-2 col-md-3 font_label" >
            <?= Yii::t('backend', 'Sex') ?>
        </div>
        <div class="col-lg-2 col-md-4 input_store_ct" >
            <?= $form->field($model, 'sex')->dropDownList(Constants::LIST_SEX, ['prompt' => Yii::t('backend', 'Please Select')])->label(false) ?>
        </div>
    </div>
    <div class="col-md-12">
        <div class="col-lg-2 col-md-3 font_label" >
            <?= Yii::t('backend', "Customers rank") ?>
        </div>
        <div class="col-lg-2 col-md-4 input_store_ct" >
            <?= $form->field($model, 'rank')->dropDownList(Constants::RANK, ['prompt' => Yii::t('backend', 'Please Select')])->label(false) ?>
        </div>
    </div>

    <div class="col-md-12">
        <div class="col-lg-2 col-md-3 font_label">
            <?= Yii::t('backend', "Visit number") ?>
        </div>
        <div class="col-lg-2 col-md-4 input_store_ct">
            <?=
            $form->field($model, 'number_visit_min', [
                'inputTemplate' => '<div class="input-group">{input}<span class="input-group-addon"><b>回以上</b></span></div>',
            ])->widget(\yii\widgets\MaskedInput::className(), [
                'clientOptions' => [
                    'groupSeparator' => ',',
                    'alias' => 'integer',
                    'autoGroup' => true,
                    'removeMaskOnSubmit' => true,
                    'allowMinus' => false,
                ]
            ])->textInput(['maxlength' => true])->label(false)
            ?>

        </div>
        <div  class="col-md-1 text-center" style="width: 0.4em;"></div>
        <div class="col-lg-2 col-md-4 input_store_ct">
            <?=
            $form->field($model, 'number_visit_max', [
                'inputTemplate' => '<div class="input-group">{input}<span class="input-group-addon"><b>回以下</b></span></div>',
            ])->widget(\yii\widgets\MaskedInput::className(), [
                'clientOptions' => [
                    'groupSeparator' => ',',
                    'alias' => 'integer',
                    'autoGroup' => true,
                    'removeMaskOnSubmit' => true,
                    'allowMinus' => false,
                ]
            ])->textInput(['maxlength' => true])->label(false)
            ?>
        </div>
    </div>

    <div class="col-md-12">
        <div class="col-lg-2 col-md-3 font_label">
            <?= Yii::t('backend', 'Birth Date') ?>
        </div>
        <div class="col-lg-2 col-md-4 input_sale_report_ct">
            <?=
            $form->field($model, 'birth_date_from')->widget(DatePicker::classname(), [
                'language' => 'ja',
                'dateFormat' => 'yyyy/MM/dd',
                'clientOptions' => [
                    "changeMonth" => true,
                    "changeYear" => true,
                    "yearRange" => "1900:+0"
                ],
            ])->textInput(['maxLength' => 10])->label(false)
            ?>

        </div>
        <div  class="col-md-1 text-center separator">～</div>
        <div class="col-lg-2 col-md-4 input_sale_report_ct">
            <?=
            $form->field($model, 'birth_date_to')->widget(DatePicker::classname(), [
                'language' => 'ja',
                'dateFormat' => 'yyyy/MM/dd',
                'clientOptions' => [
                    "changeMonth" => true,
                    "changeYear" => true,
                    "yearRange" => "1900:+0"
                ],
            ])->textInput(['maxLength' => 10])->label(false)
            ?>
        </div>
    </div>

    <div class="form-group">
        <?= Html::a(Yii::t('backend', 'Return'), ['/'], ['class' => 'btn common-button-default btn-default']) ?>
        <?= Html::submitButton(Yii::t('backend', 'Search'), ['class' => 'btn common-button-submit']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
