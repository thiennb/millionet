<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use common\models\MasterStaff;

/* @var $this yii\web\View */
/* @var $searchModel common\models\MasterCustomerSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('backend', 'Point history');
$this->params['breadcrumbs'][] = $this->title;
?>
<style>
    .pagination{ 
        display: table; 
        margin: 0 auto; 
        padding: 10px;
    }
    .pagination a{ 
        margin: 0 2px; 
    }
</style>
<div class="row">
    <div class="col-lg-12 col-md-12">
        <div class="box box-info box-solid">
            <div class="box-header with-border">
                <h4 class="text-title"><b><?= $this->title ?></b></h4>
            </div>
            <div class="box-body content">
                <div class="col-md-12">
                    <div class="common-box">
                        <div class="row">
                            <div class="box-header with-border common-box-h4 col-lg-8 col-md-12">
                                <h4 class="text-title"><b><?= Yii::t('backend', 'Customer Search History Point') ?></b></h4>
                            </div>
                        </div>

                        <div class="box-body content">
                            <div class="col-lg-10 col-md-12">
                                <?php echo $this->render('_search_history_point', ['model' => $searchModel]); ?>                                
                            </div>
                        </div>
                    </div>
                    <?php
                    Pjax::begin();
                    ?>    
                    <div class="common-box">
                        <div class="row">
                            <div class="box-header with-border common-box-h4 col-lg-8 col-md-12">
                                <h4 class="text-title"><b><?= Yii::t('backend', 'Customer Search List History Point') ?></b></h4>
                            </div>
                        </div>
                        <div class="box-body content">
                            <div class="col-md-10">

                                <?php Pjax::begin(); ?>    
                                <?=
                                GridView::widget([
                                    'dataProvider' => $dataProvider,
                                    'tableOptions' => [
                                        'class' => 'table table-striped table-bordered text-center',
                                    ],
                                    'layout' => "{pager}\n{summary}\n{items}",
                                    'summaryOptions' => ['class' => 'common-clr-fl-right'],
                                    'summary' => false,
                                    'pager' => [
                                        'class' => 'common\components\LinkPagerMillionet',
                                        'options' => ['class' => 'pagination'],
                                    ],
                                    'columns' => [
                                        ['class' => 'yii\grid\SerialColumn', 'header' => 'No', 'options' => ['class' => 'with-table-no']],
                                        [
                                            'attribute' => 'Process Date',
                                            'value' => function ($model) {
                                                return empty($model->process_date) ? "" : date('Y/m/d', strtotime($model->process_date)) . " " . $model->process_time;
                                            },
                                            'label' => Yii::t('backend', 'Process Date')
                                        ],
                                        [
                                            'value' => function ($model) {
                                                return !empty($model->store) ? $model->store->name : "";
                                            },
                                            'header' => Yii::t("backend", 'Store Id'),
                                        ],
                                        [
                                            'attribute' => 'name',
                                            'header' => Yii::t('backend', 'Name Customer'),
                                            'value' => function ($model) {
                                                return !isset($model->customerMaster) ? "" : $model->customerMaster->first_name . $model->customerMaster->last_name;
                                            },
                                        ],
                                        [
                                            'attribute' => 'Total Point',
                                            'value' => function ($model) {
                                                return Yii::$app->formatter->asInteger($model->process_point);
                                            },
                                            'label' => Yii::t('backend', 'Total Point')
                                        ],
                                        [
                                            'attribute' => 'Process Type',
                                            'value' => function ($model) {
                                                $type = common\components\Constants::POINT_HISTORY;
                                                return isset($type[$model->process_type]) ? $type[$model->process_type] : "";
                                            },
                                            'label' => Yii::t('backend', 'Process Type')
                                        ],
                                        [
                                            'attribute' => 'People Staff',
                                            'value' => function ($model) {
                                                $staff = !isset($model->management) ? "" : $model->management->staff;
                                                return empty($staff) ? "" : $staff->name;
                                            },
                                            'label' => Yii::t('backend', 'People Staff')
                                        ],
                                    ],
                                ]);
                                ?>
                            </div>
                        </div>
                    </div>
                    <?php Pjax::end(); ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

