<?php

use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel common\models\ChargeHistorySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('backend', 'Sale Report');
$this->params['breadcrumbs'][] = $this->title;
?>
<style>
    .pagination{ 
        display: table; 
        margin: 0 auto; 
        padding: 10px;
    }
    .pagination a{ 
        margin: 0 2px; 
    }
</style>
<div class="row">
    <div class="col-lg-12 col-md-12">
        <div class="box box-info box-solid">
            <div class="box-header with-border">
                <h4 class="text-title"><b><?= $this->title ?></b></h4>
            </div>
            <div class="box-body content">
                <div class="col-md-12">
                    <div class="common-box">
                        <div class="row">
                            <div class="box-header with-border common-box-h4 col-lg-8 col-md-12">
                                <h4 class="text-title"><b><?= Yii::t('backend', 'Sale Reports') ?></b></h4>
                            </div>
                        </div>

                        <div class="box-body content">
                            <div class="col-lg-10 col-md-12">
                                <?php echo $this->render('_search', ['model' => $searchModel]); ?>                                
                            </div>
                        </div>
                    </div>
                    <?php
                    Pjax::begin();
                    ?>    
                    <div class="common-box">
                        <div class="row">
                            <div class="box-header with-border common-box-h4 col-lg-8 col-md-12">
                                <h4 class="text-title"><b><?= Yii::t('backend', 'Sale report') ?></b></h4>
                            </div>
                        </div>
                        <div class="box-body content">
                            <div class="col-md-10">
                                <?php Pjax::begin(); ?>    
                                <?=
                                GridView::widget([
                                    'dataProvider' => $dataProvider,
                                    'tableOptions' => [
                                        'class' => 'table table-striped table-bordered text-center',
                                    ],
                                    'layout' => "{pager}\n{summary}\n{items}",
                                    'summaryOptions' => ['class' => 'common-clr-fl-right'],
                                    'summary' => false,
                                    'pager' => [
                                        'class' => 'common\components\LinkPagerMillionet',
                                        'options' => ['class' => 'pagination'],
                                    ],
                                    'columns' =>
                                    [
                                        ['class' => 'yii\grid\SerialColumn', 'header' => 'No', 'options' => ['class' => 'with-table-no']],
                                        [
                                            'value' => function ($model) {
                                                return empty($model->masterProduct) ? "" : $model->masterProduct->name;
                                            },
                                            'label' => Yii::t('backend', 'product name'),
                                        ],
                                        [
                                            'value' => function ($model) {
                                                $product = empty($model->masterProduct) ? null : $model->masterProduct;
                                                return empty($product->category) ? "" : $product->category->name;
                                            },
                                            'label' => Yii::t('backend', 'category name'),
                                        ],
                                        [
                                            'attribute' => 'revenue',
                                        ],
                                        [
                                            'value' => function ($model) {
                                                return $model->revenue / $model->total_revenue * 100 . "%";
                                            },
                                            'label' => Yii::t('backend', 'ratio revenue'),
                                        ],
                                        [
                                            'attribute' => 'profit',
                                        ],
                                        [
                                            'value' => function ($model) {
                                                return $model->profit / $model->total_profit * 100 . "%";
                                            },
                                            'label' => Yii::t('backend', 'ratio profit'),
                                        ],
                                        [
                                            'attribute' => 'quantitys',
                                            'label' => Yii::t('backend', 'quantities'),
                                        ],
                                        [
                                            'value' => function ($model) {
                                                return $model->quantitys / $model->total_quantitys * 100 . "%";
                                            },
                                            'label' => Yii::t('backend', 'ratio quantities'),
                                        ],
                                    ],
                                ]);
                                ?>
                            </div>
                        </div>
                    </div>
                    <?php Pjax::end(); ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
