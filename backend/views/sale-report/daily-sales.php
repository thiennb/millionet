<?php

use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\jui\DatePicker;
use yii\helpers\ArrayHelper;
use common\models\MasterStore;
use common\models\BookingBusiness;

/* @var $this yii\web\View */
/* @var $searchModel common\models\ChargeHistorySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('backend', 'Daily Sales');
$this->params['breadcrumbs'][] = $this->title;

$startingYear = date('Y', strtotime('-20 year'));
$currentYear = date('Y'); $currentMonth = date('m'); $currentDate = date('d');
$yearParam = Yii::$app->request->get('year') != ''? Yii::$app->request->get('year') : $currentYear;
$monthParam = Yii::$app->request->get('month') != '' ? Yii::$app->request->get('month') : $currentMonth;
if($yearParam !== '' && $monthParam != ''){
    $daysInMonth = cal_days_in_month(CAL_GREGORIAN, $monthParam, $yearParam);
}

$this->registerCssFile('@web/css/accoungting.css', ['depends' => [yii\bootstrap\BootstrapAsset::className()]]);
?>
<div class="row">
    <div class="col-lg-12 col-md-12">
        <div class="box box-info box-solid">
            <div class="box-header with-border">
                <h4 class="text-title"><b><?= $this->title ?></b></h4>
            </div>
            <div class="box-body content">
                <div class="col-md-12">
                    <div class="common-box">
                        <div class="row">
                            <div class="box-header with-border common-box-h4 col-lg-8 col-md-12">
                                <h4 class="text-title"><b><?= Yii::t('backend', 'Daily Sales') ?></b></h4>
                            </div>
                        </div>

                        <div class="box-body content">
                            <div class="col-lg-12 col-md-12">
                                <div class="charge-history-search">
                                <?php
                                $form = ActiveForm::begin([
                                    'action' => ['daily-sales'],
                                    'options' => ['id'=>'daily-sales_form'],
                                    'method' => 'get',
                                    'enableClientValidation' => false,
                                    
                                ]);
                                ?>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-lg-2 col-md-3 font_label label_sale_ct">
                                                    <?= Yii::t('backend', 'Stores') ?> </div>
                                                <div class="col-lg-2 col-md-4 input_store_ct">
                                                    <?= Html::dropDownList('store_id', $data['store_id'], MasterStore::getListStore(), ['class'=>'form-control', 'required'=>'required']) ?>     
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-lg-2 col-md-3 font_label label_sale_ct">
                                                <?= Yii::t('backend', 'Total unit') ?></div>
                                                <?php $totalUnit =  Yii::$app->request->get('total_unit'); ?>
                                                <div class="col-lg-2 col-md-4 input_store_ct">
                                                    <select name="total_unit" id="year" class="form-control">
                                                        <option value="by_year" <?= $totalUnit == 'by_year'? 'selected' : '' ?>><?= Yii::t('backend', 'by Year') ?></option>
                                                        <option value="by_month" <?= $totalUnit == 'by_month'? 'selected' : '' ?>><?= Yii::t('backend', 'by Month') ?></option>
                                                        <option value="by_day" <?= $totalUnit == 'by_day'? 'selected' : '' ?>><?= Yii::t('backend', 'by Day') ?></option>
                                                        <option value="by_hour" <?= $totalUnit == 'by_hour' || $totalUnit == '' ? 'selected' : '' ?>><?= Yii::t('backend', 'by Hours') ?></option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">                                        
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-lg-2 col-md-3 font_label label_sale_ct"><?= Yii::t('backend', 'Year') ?></div>
                                                <div class="col-lg-2 col-md-4 input1_sale_report_ct">
                                                    <select name="year" id="year" class="form-control">
                                                        <?php for($startingYear; $startingYear <= $currentYear ; $startingYear ++ ) : 
                                                            if($startingYear == Yii::$app->request->get('year')):
                                                                echo "<option value='$startingYear' selected='selected'>$startingYear</option>";
                                                            elseif(($startingYear == $currentYear) && Yii::$app->request->get('year') == ''):
                                                                echo "<option value='$startingYear' selected='selected'>$startingYear</option>";
                                                            else :
                                                                echo "<option value='$startingYear'>$startingYear</option>";
                                                            endif;                                                            
                                                        endfor; ?>
                                                    </select>     
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">                                        
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-lg-2 col-md-3 font_label label_sale_ct"><?= Yii::t('backend', 'Month') ?></div>
                                                <div class="col-lg-2 col-md-4 input1_sale_report_ct">
                                                    <select name="month" id="month" class="form-control">
                                                        <?php for($beginMonth = 1 ; $beginMonth <= 12 ; $beginMonth ++ ) : 
                                                            if($beginMonth == Yii::$app->request->get('month')):
                                                                echo "<option value='$beginMonth' selected='selected'>$beginMonth</option>";
                                                            elseif(($beginMonth == $currentMonth) && Yii::$app->request->get('month') == ''):
                                                                echo "<option value='$beginMonth' selected='selected'>$beginMonth</option>";
                                                            else :
                                                                echo "<option value='$beginMonth'>$beginMonth</option>";
                                                            endif;                                                            
                                                        endfor; ?>
                                                    </select>     
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">                                        
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-lg-2 col-md-3 font_label label_sale_ct"><?= Yii::t('backend', 'Day') ?></div>
                                                <div class="col-lg-2 col-md-4 input1_sale_report_ct">
                                                    <select name="day" id="date" class="form-control">
                                                        <?php for($beginDay = 1 ; $beginDay <= $daysInMonth ; $beginDay ++ ) : 
                                                            if($beginDay == Yii::$app->request->get('day')):
                                                                echo "<option value='$beginDay' selected='selected'>$beginDay</option>";
                                                            elseif(($beginDay == $currentDate) && Yii::$app->request->get('day') == ''):
                                                                echo "<option value='$beginDay' selected='selected'>$beginDay</option>";
                                                            else :
                                                                echo "<option value='$beginDay'>$beginDay</option>";
                                                            endif;                                                            
                                                        endfor; ?>
                                                    </select>     
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <span class="error-message" id="productSaleError"></span>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                        <?= Html::a(Yii::t('backend', 'Return'), ['/'], ['class' => 'btn common-button-default btn-default']) ?>
                                        <?= Html::submitButton(Yii::t('backend', 'Display'), ['class' => 'btn common-button-submit']) ?>
                                        <?php if(!empty($data['revenues'])): ?>
                                        <?= Html::button(Yii::t('backend', 'Display switching'),['class' => 'btn common-button-submit pull-right' , 'id'=> 'display_switch']) ?>
                                        <?php endif; ?>
                                        </div>
                                    </div>
                                <?php ActiveForm::end(); ?>
                                </div> 
                            </div>
                        </div>
                    </div>
                    <div class="common-box">                      
                        <div class="box-body content">
                            <div class="col-md-12">
                                <div id="w2" data-pjax-container="" data-pjax-push-state="" data-pjax-timeout="1000">    
                                    <div id="table_product_sate" class="grid-view">
                                        <table class="table table-striped table-bordered text-center">
                                            <thead>
                                                <tr>
                                                    <th><?= Yii::t('backend', 'Total unit') ?></th>
                                                    <th><?= Yii::t('backend', 'Revenue') ?></th>
                                                    <th><?= Yii::t('backend', 'Account number') ?></th>
                                                    <th><?= Yii::t('backend', 'Accounting unit price') ?></th>
                                                    <th><?= Yii::t('backend', 'Number of guests') ?></th>
                                                    <th><?= Yii::t('backend', 'Per-customer price') ?></th>
                                                    <th><?= Yii::t('backend', 'Sales number') ?></th>
                                                    <th><?= Yii::t('backend', 'Payment') ?></th>
                                                </tr>
                                            </thead>
                                            <?php if(!empty($data['revenues'])) : ?>
                                            <tbody>
                                                <?php foreach($data['revenues'] as $revenues ): ?>
                                                <?php if(!empty($revenues)) : ?>
                                                <tr>
                                                    <td><?= $revenues['time'] ?></td>
                                                    
                                                    <td><?= '¥'.BookingBusiness::formatMoney($revenues['total_money']) ?></td>
                                                    <td><?= BookingBusiness::formatMoney((int)$revenues['count']) ?></td>
                                                    <td><?= '¥'.BookingBusiness::formatMoney((int)$revenues['money_per_order']) ?></td>
                                                    <td><?= BookingBusiness::formatMoney((int)$revenues['num_customer']) ?></td>
                                                    <td><?= '¥'.BookingBusiness::formatMoney((int)$revenues['money_per_customer']) ?></td>
                                                    <td><?= BookingBusiness::formatPoint((int)$revenues['quantity']) ?></td>
                                                    <td></td>
                                                </tr>
                                                <?php endif; ?>
                                                <?php endforeach;?>
                                            </tbody>
                                            <?php else: ?>
                                            <tbody>
                                                <tr>
                                                    <?php if(Yii::$app->request->get('store_id') !== null): ?>
                                                    <td colspan="9"><div class="empty"><?= Yii::t('backend', 'No results found')?></div></td>
                                                    <?php else: ?>
                                                    <td colspan="9"><div class="empty"></div></td>
                                                    <?php endif; ?>
                                                </tr>
                                            </tbody>
                                            <?php endif; ?>
                                        </table>
                                    </div>                            
                                </div>
                                <div id="graph_product_sales" class="display-none">
                                    <div class="graph-sale-custom">
                                    <div class="row">
                                        <div class="col-md-1"><span class="left-graph"><?= Yii::t('backend','Number of customers') ?></div>
                                        <div class="col-md-10 chart_top_admin_ct"><canvas id="canvas"></canvas></div>
                                        <div class="col-md-1"><span class="right-graph"><?= Yii::t('backend', 'Sales') ?></div>
                                    </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $('select[name=month]').change( function(){
        year = $('select[name=year]').val();
        month = $(this).val();
        changeMonth(year, month);
    });
    
    $('select[name=year]').change( function(){
        year = $(this).val();
        month = $('select[name=month]').val();
        changeMonth(year, month);
    });
    
    $('select[name=total_unit]').change(function(){
        changeTotalUnit();
    });
    
    function changeTotalUnit(){
        $('select[name=year]').removeAttr('disabled');
        $('select[name=month]').removeAttr('disabled');
        $('select[name=day]').removeAttr('disabled');
        
        if($('select[name=total_unit]').val() == 'by_year'){
            $('select[name=year]').attr('disabled','disabled'); 
            $('select[name=month]').attr('disabled','disabled'); 
            $('select[name=day]').attr('disabled','disabled'); 
        }
        
        if($('select[name=total_unit]').val() == 'by_month'){
            $('select[name=month]').attr('disabled','disabled'); 
            $('select[name=day]').attr('disabled','disabled'); 
        }
        
        if($('select[name=total_unit]').val() == 'by_day'){
            $('select[name=day]').attr('disabled','disabled'); 
        }
    }
    
    function changeMonth(year, month){
        days = daysInMonth(month, year);
        currentDayValue = $('select[name=day]').val();
        $('select[name=day]').html('');
        for(i = 1; i<= days; i++){
            if(i == currentDayValue){
                $('select[name=day]').append('<option value="'+i+'" selected="selected">'+i+'</option>');
            }else{
                $('select[name=day]').append('<option value="'+i+'">'+i+'</option>');
            }            
        }
    }
    
    $(document).ready(function(){
        changeTotalUnit();
    });
</script>

<?php if(!empty($data['revenues'])): ?>
<script>
    dataTime= []; dataMoney = []; dataPeople= []; dataOriginal = [];
    <?php foreach($data['revenues'] as $revenue) : ?>
        <?php if(!empty($revenue)) :?>
            dataTime.push('<?= $revenue['time_mark'] ?>');
            dataMoney.push('<?= $revenue['total_money']?>');
            dataPeople.push('<?= $revenue['num_customer']?>');
        <?php endif; ?>
    <?php endforeach;?>
    dataOriginal = {"totalMoneyDate":0,"totalPeopleDate":0,"averageMoney":0,"countBooking":0,"countItemDelivery":0,"totalMoneyMonth":"6234","totalPeopleMonth":2,"awayPeople":0,"awayGimmickPeople":0,"graphInfo":{"time":{"1":"02:00","2":"04:00","3":"06:00","4":"08:00","5":"10:00","6":"12:00"},"money":[0],"people":[0,0,0,0,0,0]},"dataMaster":[],"stores":[]};
    maxPeople = 10;
    maxMoney = 100;
    minMoney = 0;
    if(dataMoney.length > 0){
        for(i = dataMoney.length-1; i > 0; i--){
            if(dataMoney[i] == '0'){
                dataMoney.splice(i, 1);
            }else{
                break;
            }
        }
    }
    for(i = 0; i < dataPeople.length; i++){
        if(maxPeople< dataPeople[i]){
            maxPeople = Number(dataPeople[i]);
        }
    }

    for(i = 0; i < dataMoney.length; i++){
        if(maxMoney < Number(dataMoney[i])){
            maxMoney = Number(dataMoney[i]);
            if(maxMoney > 1000 && maxMoney <= 10000){
                maxMoney = (Math.floor(maxMoney/1000)+1)*1000;
            }
            if(maxMoney > 10000 && maxMoney <= 100000){
                maxMoney = (Math.floor(maxMoney/10000) +1 )*10000;
            }
            if(maxMoney > 100000 && maxMoney <= 1000000){
                maxMoney = (Math.floor(maxMoney/100000) +1 )*100000;
            }
        }
        if(minMoney > Number(dataMoney[i])){
            minMoney = Number(dataMoney[i]);
            
            if(maxMoney > 10000 && maxMoney <= 100000){
                if( minMoney > -10000){
                    minMoney = -10000;
                }
            }
            
            if(maxMoney > 100000 && maxMoney <= 1000000){
                if(minMoney > -100000){
                    minMoney = -100000;
                }
            }
            if(maxMoney > 1000000){
                if(minMoney > -1000000){
                    minMoney = -1000000;
                }
            }
            
            if(maxMoney > 100000000){
                if(minMoney > -100000000){
                    minMoney = -100000000;
                }
            }
            
            if(maxMoney > 1000000000){
                if(minMoney > -1000000000){
                    minMoney = -1000000000;
                }
            }
        }
    }

    var barChartData = {
        labels : dataTime,
        datasets: [{
            type: 'bar',
            label: "<?= Yii::t('backend','Number of customers') ?>",
            data: dataPeople,
            fill: false,
            backgroundColor: '#5b9bd5',
            borderColor: '#5b9bd5',
            hoverBackgroundColor: '#71B37C',
            hoverBorderColor: '#71B37C',
            yAxisID: 'y-axis-1'
        }, {
            label: "<?= Yii::t('backend', 'Sales') ?>",
            type: 'line',
            data: dataMoney,
            fill: false,
            borderColor: '#EC932F',
            backgroundColor: '#EC932F',
            pointBorderColor: '#EC932F',
            pointBackgroundColor: '#EC932F',
            pointHoverBackgroundColor: '#EC932F',
            pointHoverBorderColor: '#EC932F',
            yAxisID: 'y-axis-2',
            bezierCurve: false,
            tension: 0
        }]
    };
    
    var options_graph = {

        legend: {
            display: false
        },
        responsive: true,
        tooltips: {
            mode: 'label',
        },
        elements: {
            line: {
                fill: false
            }
        },
        scales: {
            xAxes: [{
                display: true,
                gridLines: {
                    display: false,tickMarkLength: 15  
                },
                labels: {
                    show: true,
                },
                barPercentage: 0.3,
            }],
            yAxes: [{
                type: "linear",
                display: true,
                position: "left",
                id: "y-axis-1",                        
                gridLines: {
                    display: false,tickMarkLength: 15  
                },
                labels: {
                    show: true,
                },
                ticks:{max:maxPeople, min: 0},
            }, {
                type: "linear",
                display: true,
                position: "right",
                id: "y-axis-2",
                gridLines: {
                    display: false
                },
                labels: {
                    show: true,
                },
                ticks:{max:maxMoney, min: minMoney},
            }]
        },layout:{
            padding:{top:5}
        }
    }; 

    window.onload = function() {
        var ctx = document.getElementById("canvas").getContext("2d");
        if(dataTime.length != 0)
        window.myBar = new Chart(ctx, {
            type: 'bar',
            data: barChartData,
            options: options_graph,
        });
    };
    
    $(document).ready(function(){
        $('#display_switch').on('click', function(){
            $('#table_product_sate').toggleClass('display-none');
            $('#graph_product_sales').toggleClass('display-none');
        });
    });
</script>
<?php endif; ?>
