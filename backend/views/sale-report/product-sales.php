<?php

use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\jui\DatePicker;
use yii\helpers\ArrayHelper;
use common\models\MasterStore;
use common\components\Util;
use api\models\ApiSupport;
use common\models\BookingBusiness;

/* @var $this yii\web\View */
/* @var $searchModel common\models\ChargeHistorySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('backend', 'Product Sales');
$this->params['breadcrumbs'][] = $this->title;
$listCategories = array();

if(Yii::$app->user->identity->permission_id == 0 || Yii::$app->user->identity->permission_id == 1){
    //var_dump($data['list_categories']);
    foreach($data['list_categories'] as $list_category){
        if($list_category['store_id'] == $data['store_id']){
            $listCategories = $list_category['list_categories'];
        }
    }
}

if(Yii::$app->user->identity->permission_id == 2 || Yii::$app->user->identity->permission_id == 3){
    $listCategories = $data['list_categories'][0]['list_categories'];
}

$this->registerCssFile('@web/css/accoungting.css', ['depends' => [yii\bootstrap\BootstrapAsset::className()]]);
?>

<div class="row">
    <div class="col-lg-12 col-md-12">
        <div class="box box-info box-solid">
            <div class="box-header with-border">
                <h4 class="text-title"><b><?= $this->title ?></b></h4>
            </div>
            <div class="box-body content">
                <div class="col-md-12">
                    <div class="common-box">
                        <div class="row">
                            <div class="box-header with-border common-box-h4 col-lg-8 col-md-12">
                                <h4 class="text-title"><b><?= Yii::t('backend', 'Product Sales') ?></b></h4>
                            </div>
                        </div>

                        <div class="box-body content">
                            <div class="col-lg-12 col-md-12">
                                <div class="charge-history-search">
                                <?php
                                $form = ActiveForm::begin([
                                    'action' => ['product-sales'],
                                    'options' => ['id'=>'sales_product_form'],
                                    'method' => 'get',
                                    'enableClientValidation' => false,
                                    
                                ]);
                                ?>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-lg-2 col-md-3 font_label_sale_ct font_label">
                                                    <?= Yii::t('backend', 'Stores') ?> </div>
                                                <div class="col-lg-2 col-md-4 input_store_ct">
                                                    <?= Html::dropDownList('store_id', $data['store_id'], MasterStore::getListStore(), ['class'=>'form-control', 'required'=>'required']) ?>     
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-lg-2 col-md-3 font_label_sale_ct font_label">
                                                <?= Yii::t('backend', 'Categories') ?></div>
                                                <div class="col-lg-2 col-md-4 input_store_ct">
                                                    <?= Html::dropDownList('category_id', $data['category_id'], $listCategories, ['class'=>'form-control', 'required'=>'required']) ?>     
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-lg-2 col-md-3 font_label_sale_ct font_label">
                                                <?= Yii::t('backend', 'Period') ?></div>
                                                <div class="col-lg-2 col-md-4 input2_sale_report_ct">
                                                    <?=

                                                    DatePicker::widget([
                                                        'name'  => 'start_date',
                                                        'value'  => $data['start_date']!=''? $data['start_date'] : date('Y/m/d'),
                                                        'options' => ['id'=> 'start_date']
                                                    ]);
                                                    ?>       
                                                </div>
                                                <div class="col-md-1 text-center separator">～</div>
                                                <div class="col-lg-2 col-md-4 input2_sale_report_ct">
                                                    <?=
                                                    DatePicker::widget([
                                                        'name'  => 'end_date',
                                                        'value'  => $data['end_date'],
                                                        'options' => ['id'=>'end_date']
                                                    ]);
                                                    ?>       
                                                </div>
                                                <div class="col-md-10 col-xs-offset-2">
                                                    <p class="error-message help-block help-block-error" id="productSaleError"></p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="col-md-12">
                                        <div class="form-group">
                                        <?= Html::a(Yii::t('backend', 'Return'), ['/'], ['class' => 'btn common-button-default btn-default']) ?>
                                        <?= Html::submitButton(Yii::t('backend', 'Display'), ['class' => 'btn common-button-submit']) ?>
                                        </div>
                                    </div>
                                <?php ActiveForm::end(); ?>
                                </div>                                
                            </div>
                        </div>
                    </div>
                    
                    <div class="common-box">                        
                        <div class="box-body content">
                            <div class="col-md-12">
                                <div id="w2" data-pjax-container="" data-pjax-push-state="" data-pjax-timeout="1000">    
                                    <div id="w3" class="grid-view">
                                        <table class="table table-striped table-bordered text-center">
                                            <thead>
                                                <tr>
                                                    <th>No</th>
                                                    <th><?= Yii::t('backend', 'Product name') ?></th>
                                                    <th><?= Yii::t('backend', 'Category') ?></th>
                                                    <th><?= Yii::t('backend', 'Revenue') ?></th>
                                                    <th><?= Yii::t('backend', 'Ratio revenue') ?></th>
                                                    <th><?= Yii::t('backend', 'Profit') ?></th>
                                                    <th><?= Yii::t('backend', 'Ratio profit') ?></th>
                                                    <th><?= Yii::t('backend', 'Quantities') ?></th>
                                                    <th><?= Yii::t('backend', 'Ratio quantities') ?></th>
                                                </tr>
                                            </thead>
                                            <?php if(!empty($data['dataTable'])) : 
                                                $sumInfo = $data['sumInfo']; $revenue = 0; $profit = 0; $quantity = 0;

                                                if(!empty($sumInfo)){
                                                    $revenue = $sumInfo[0]->total_revenue;
                                                    $profit = $sumInfo[0]->total_profit;
                                                    $quantity = $sumInfo[0]->total_quantitys;
                                                }
                                            ?>
                                            <tbody>
                                                <?php 
                                                $index = 0;
                                                if(!empty($data['pages'])){
                                                    $index = 10*((int)$data['pages']->getPage()+1) - 10;
                                                }
                                                foreach($data['dataTable'] as $record ): 
                                                    $index++;
                                                ?>
                                                <tr>
                                                    <td><?= $index; ?></td>
                                                    <td><?= $record->masterProduct['name'] ?></td>
                                                    <td><?= $record->masterProduct->category['name'] ?></td>
                                                    <td><?= BookingBusiness::formatMoney($record->total_revenue) ?></td>
                                                    <td><?= $revenue !== 0 ? round((float)$record->total_revenue/$revenue*100,2) : '-' ?> %</td>
                                                    <td><?= $record->total_profit ?></td>
                                                    <td><?= $profit !== 0 ? round((float)$record->total_profit/$profit*100,2) : '-' ?> %</td>
                                                    <td><?= $record->total_quantitys ?></td>
                                                    <td><?= $quantity !== 0 ? round((float)$record->total_quantitys/$quantity*100,2) : '-' ?> %</td>
                                                </tr>
                                                <?php endforeach;?>
                                            </tbody>
                                            <?php else: ?>
                                            <tbody>
                                                <tr>
                                                    <?php if(Yii::$app->request->get('store_id') !== null): ?>
                                                    <td colspan="9"><div class="empty"><?= Yii::t('backend', 'No results found')?></div></td>
                                                    <?php else: ?>
                                                    <td colspan="9"><div class="empty"></div></td>
                                                    <?php endif; ?>
                                                </tr>
                                            </tbody>
                                            <?php endif; ?>
                                        </table>
                                    </div>                            
                                </div>
                                
                                <div class="col-md-12">
                                    <div class="top-pagination-rank">

                                    <?php
                                    if(!empty($data['pages'])) :
                                        echo \yii\widgets\LinkPager::widget([
                                        'pagination' => $data['pages'],
                                        'prevPageLabel' => '◀ '.Yii::t('frontend', 'Prev page'),
                                        'nextPageLabel' => Yii::t('frontend', 'Next page'). ' ▶',
                                        'pageCssClass' => 'hidden', 
                                        ]);               
                                    ?>
                                        <?php if($data['pages']->getPageCount()>0) : ?>
                                            <div class="count-page-pagination"><span><?= ((int)$data['pages']->getPage()+1).'/'.$data['pages']->getPageCount().' '.Yii::t('frontend', 'ページ')?></span></div>
                                        <?php endif; ?>
                                    <?php endif; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                    </div>                    
                </div>
            </div>
        </div>
    </div>
    
    
</div>
<script>
    listCategory = null;
<?php if(Yii::$app->user->identity->permission_id == 0 || Yii::$app->user->identity->permission_id == 1) : ?>
    listCategory = <?= \GuzzleHttp\json_encode($data['list_categories'])?>;
    
    $('select[name=store_id]').change(function(){

       for(i = 0; i < listCategory.length; i++){

           if($(this).val() == listCategory[i]['store_id']){
               $('select[name=category_id]').empty();
               if(listCategory[i]['list_categories'].length != 0){
                   $('select[name=category_id]').append('<option value="all">'+listCategory[i]['list_categories']['all']+'</option');
                   for(var key in listCategory[i]['list_categories']){
                       if(key != 'all')
                        $('select[name=category_id]').append('<option value="'+key+'">'+listCategory[i]['list_categories'][key]+'</option');
                   }
               }
           }
       }
    });
<?php endif; ?>
    
    $('#sales_product_form').submit(function(evt){  
        evt.preventDefault();
        valid = true;
//        startDate = document.getElementById('start_date');
//        if(moment(startDate.value,'YYYY/MM/DD',true).isValid() == false){
//            valid = false;
//            $('#productSaleError').html('<span>生年月日の書式が正しくありません</span>');
//        }
//        
//        endDate = document.getElementById('end_date');
//        if(moment(endDate.value,'YYYY/MM/DD',true).isValid() == false){
//            $('#productSaleError').html('<span>生年月日の書式が正しくありません</span>');
//            valid = false;
//        }
        if (valid) 
            this.submit();
    });
</script>