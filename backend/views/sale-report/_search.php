<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\jui\DatePicker;
use common\models\MasterStore;

/* @var $this yii\web\View */
/* @var $model common\models\MasterCustomerSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="charge-history-search">
    <?php
    $form = ActiveForm::begin([
                'action' => ['index'],
                'method' => 'get',
                'enableClientValidation' => false,
    ]);
    ?>

    <div class="col-md-12">
        <div class="col-lg-2 col-md-3 font_label" >
            <?= Yii::t('backend', 'Store Id') ?>
        </div>
        <div class="col-lg-2 col-md-4 input_store_ct" >
            <?= $form->field($model, 'store_id')->dropDownList(MasterStore::getListStore(), ['prompt' => Yii::t('backend', 'Please Select')])->label(false) ?>
        </div>
    </div>

    <div class="col-md-12">
        <div class="col-lg-2 col-md-3 font_label" >
            <?= Yii::t('backend', 'Date Time') ?>
        </div>
        <div class="col-lg-2 col-md-4 input_sale_report_ct" >
            <?=
            $form->field($model, 'start_date')->widget(DatePicker::classname(), [
                'language' => 'ja',
                'dateFormat' => 'yyyy/MM/dd',
                'clientOptions' => [
                    "changeMonth" => true,
                    "changeYear" => true,
                    "yearRange" => "1900:+0"
                ],
            ])->textInput(['maxLength' => 10])->label(false)
            ?>
        </div>
        <div  class="col-md-1 text-center separator">～</div>
        <div class="col-lg-2 col-md-4 input_sale_report_ct">
            <?=
            $form->field($model, 'end_date')->widget(DatePicker::classname(), [
                'language' => 'ja',
                'dateFormat' => 'yyyy/MM/dd',
                'clientOptions' => [
                    "changeMonth" => true,
                    "changeYear" => true,
                    "yearRange" => "1900:+0"
                ],
            ])->textInput(['maxLength' => 10])->label(false)
            ?>
        </div>
    </div>
    

    <div class="form-group">
    <?= Html::a(Yii::t('backend', 'Return'), ['/'], ['class' => 'btn common-button-default btn-default']) ?>
    <?= Html::submitButton(Yii::t('backend', 'Search'), ['class' => 'btn common-button-submit']) ?>
    </div>    
<?php ActiveForm::end(); ?>

</div>