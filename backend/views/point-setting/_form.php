<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\bootstrap\Modal;
use common\components\Constants;

/* @var $this yii\web\View */
/* @var $model common\models\PointSetting */
/* @var $form yii\widgets\ActiveForm */
$role = \common\components\FindPermission::getRole();
?>

<div class="point-setting-form">
    <?php
    $form = ActiveForm::begin([
                'action' => ['create'],
                'id' => 'point-setting-form',
                'enableClientValidation' => false,
                'options' => ['class' => 'form-horizontal'],
            ])
    ?>

    <div class="col-md-12"> 
        <div class="col-md-2 label-margin">
            <?= Yii::t("backend", "Point Grant"); ?>
        </div>
        <div class="col-md-3">
            <?=
            $form->field($model, 'grant_yen_per_point', [
            ])->widget(\yii\widgets\MaskedInput::className(), [
                'clientOptions' => ['alias' => 'integer',
                    'groupSeparator' => ',',
                    'autoGroup' => true,
                    'removeMaskOnSubmit' => true,
                    'allowMinus' => false,
                    'greedy' => false]
            ])->label(false)->textInput(['maxlength' => 13,'class'=>'form-control convert-haft-size'])
            ?>
        </div>
        <div class="col-md-3 label-margin">
            <?= Yii::t("backend", "Get 1 Free Point"); ?>
        </div>
    </div>

    <div class="col-md-12"> 
        <div class="col-md-2 label-margin">
            <?= Yii::t("backend", "Point Exchange"); ?>
        </div>
        <div class="col-md-3">
            <?=
            $form->field($model, 'point_conversion_p', [
            ])->widget(\yii\widgets\MaskedInput::className(), [
                'clientOptions' => [
                    'groupSeparator' => ',',
                    'alias' => 'decimal',
                    'autoGroup' => true,
                    'removeMaskOnSubmit' => true,
                    'allowMinus' => false,
                //'groupSeparator' => '.',
                // 'removeMaskOnSubmit' => true
                ]
            ])->textInput(['maxlength' => 13,'class'=>'form-control convert-haft-size'])->label(false)
            ?>
        </div>
        <div class="col-md-1 label-margin text-center">
            <?= Yii::t("backend", "Equals"); ?>
        </div>
        <div class="col-md-3 point_conversion_yen_point_ct">
            <?=
            $form->field($model, 'point_conversion_yen', [
            ])->widget(\yii\widgets\MaskedInput::className(), [
                'clientOptions' => [
                    'alias' => 'decimal',
                    'groupSeparator' => ',',
                    'autoGroup' => true,
                    'removeMaskOnSubmit' => true,
                    'allowMinus' => false,
                    'greedy' => false
                ]
            ])->label(false)->textInput(['maxlength' => 13,'class'=>'form-control convert-haft-size'])
            ?>
        </div>
        <div class="col-md-2 label-margin">
            <?= Yii::t("backend", "Yen"); ?>
        </div>
    </div>

    <div class="col-md-12">
        <div class="col-md-2 font_label">
            <?= Yii::t("backend", "Does store services point"); ?>
        </div>
        <div class="col-md-3 font_label">
            <?php isset($model->grant_point_visit_flg) ? $model->grant_point_visit_flg : '0' ?>
            <?= $form->field($model, 'grant_point_visit_flg')->radioList(Constants::LIST_OPTION_SELECT_POINT_GRANT, ['separator' => '', 'class' => 'grant_point_visit_flg'])->label(false) ?>
        </div>
    </div>

    <!-- Button submit and back-->
    <div class="col-md-12">
        <div class="col-md-3 magin_button text-right">
            <div class="form-group">
                <?= Html::a(Yii::t('backend', 'Return'), ['/'], ['class' => 'btn btn-default common-button-default']) ?>
            </div>
        </div>
        <?php if ($role->permission_id == Constants::APP_MANAGER_ROLE || $role->permission_id == Constants::COMPANY_MANAGER_ROLE) { ?>
            <div class="col-md-4 magin_button btn_sumit_giff_ct">
                <div class="form-group">
                    <?= Html::submitButton(Yii::t('backend', 'Save'), ['id' => 'btn-add', 'class' => 'btn common-button-submit']) ?>
                </div>
            </div>  
        <?php } ?>
    </div>
    <!-- End Button submit and back-->


    <?php $form = ActiveForm::end() ?>

</div>

<?php if (!($role->permission_id == Constants::APP_MANAGER_ROLE || $role->permission_id == Constants::COMPANY_MANAGER_ROLE)) { ?>
    <script>
        $(':input').prop("disabled", true);
    </script>
<?php } ?> 
<script>
    $('#btn-add').click(function (event) {
        //Some code
        // Get value form
        var grant_yen_per_point = $("#pointsetting-grant_yen_per_point").val();
        var point_conversion_yen = $("#pointsetting-point_conversion_yen").val();
        var point_conversion_p = $('#pointsetting-point_conversion_p').val();

        var number_point_conversion_yen = Number(point_conversion_yen.replace(/[^0-9\.]+/g, ""));
        var number_grant_yen_per_point = Number(grant_yen_per_point.replace(/[^0-9\.]+/g, ""));

        var result = number_point_conversion_yen / parseInt(point_conversion_p);
        console.log(result);

        if (result > number_grant_yen_per_point) {
            event.preventDefault();
            bootbox.dialog({
                title: '<div class="box-header with-border common-box-h4 col-md-8"><h4 class="text-title"><b>更新確認</b></h4></div>',
                message: MESSAGE.CONFIRM_SETTING_POINT,
                closeButton: false,
                buttons: {
                    success: {
                        label: 'OK',
                        className: "btn common-button-action text-center",
                        callback: function () {
                            document.getElementById("point-setting-form").submit();
                        }
                    },
                    danger: {
                        label: 'キャンセル',
                        className: "btn btn-default text-center",
                        callback: function () {
                        }
                    }
                }
            });
        } else if (result < number_grant_yen_per_point) {

        }

    });
</script>    