<?php

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use common\models\MasterStore;
use yii\jui\DatePicker;

/* @var $this yii\web\View */
/* @var $model common\models\MasterTicketSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="master-ticket-search">

    <?php
    $form = ActiveForm::begin([
                'action' => ['index'],
                'enableClientValidation' => false,
                'method' => 'get',
    ]);
    ?>
    <div class="col-md-12">
        <div class="col-lg-2 col-md-3 font_label" >
            <?= Yii::t('backend', 'Store Ticket') ?>
        </div>
        <div class="col-lg-2 col-md-4 input_sale_report_ct" >
            <?= $form->field($model, 'store_id')->dropDownList(MasterStore::getListStore(), ['prompt' => Yii::t('backend', 'Please Select')])->label(false) ?>
        </div>
    </div>

    <div class="col-md-12">
        <div class="col-lg-2 col-md-3 font_label" >
            <?= Yii::t('backend', 'Product Name') ?>
        </div>
        <div class="col-lg-2 col-md-4 input_sale_report_ct" >
            <?= $form->field($model, 'product_name')->textInput(['maxlength' => true])->label(false) ?>
        </div>
    </div>
    <div class="col-md-12">
        <div class="col-lg-2 col-md-3 font_label" >
            <?= Yii::t('backend', 'Set Ticket') ?>
        </div>
        <div class="col-lg-2 col-md-4 input_sale_report_ct" >
            <?=
            $form->field($model, 'set_ticket_min', [
                'inputTemplate' => '<div class="input-group">{input}<span class="input-group-addon"><b>' . Yii::t('backend', 'Set Ticket First') . '</b></span></div>',
            ])->widget(\yii\widgets\MaskedInput::className(), [
                'clientOptions' => [
                    'groupSeparator' => ',',
                    'alias' => 'integer',
                    'autoGroup' => true,
                    'removeMaskOnSubmit' => true,
                    'allowMinus' => false,
                ]
            ])->textInput(['maxlength' => true])->label(false);
            ?>  
        </div>
        <div  class="col-md-1 text-center separator"></div>
        <div class="col-lg-2 col-md-4 input_sale_report_ct">
            <?=
            $form->field($model, 'set_ticket_max', [
                'inputTemplate' => '<div class="input-group">{input}<span class="input-group-addon"><b>' . Yii::t('backend', 'Set Ticket Last') . '</b></span></div>',
            ])->widget(\yii\widgets\MaskedInput::className(), [
                'clientOptions' => [
                    'groupSeparator' => ',',
                    'alias' => 'integer',
                    'autoGroup' => true,
                    'removeMaskOnSubmit' => true,
                    'allowMinus' => false,
                ]
            ])->textInput(['maxlength' => true])->label(false);
            ?> 
        </div>
    </div>

    <div class="col-md-12">
        <div class="col-lg-2 col-md-3 font_label" >
            <?= Yii::t('backend', 'Date Expiration Ticket') ?>
        </div>
        <div class="col-lg-2 col-md-4 input_sale_report_ct" >
            <?=
            $form->field($model, 'date_expiration_from', [
                'inputTemplate' => '<div class="input-group">{input}<span class="input-group-addon"><b>' . Yii::t('backend', 'First Day Ticket') . '</b></span></div>',
            ])->widget(\yii\widgets\MaskedInput::classname(), 
           [
                'clientOptions' => [
                    'alias' => 'integer',
                    'autoGroup' => true,
                    'removeMaskOnSubmit' => true,
                    'allowMinus' => false,
                ]
            ])->textInput(['maxLength' => 3])->label(false)
            ?>
        </div>
        <div  class="col-md-1 text-center separator"></div>
        <div class="col-lg-2 col-md-4 input_sale_report_ct">
            <?=
            $form->field($model, 'date_expiration_to', [
                'inputTemplate' => '<div class="input-group">{input}<span class="input-group-addon"><b>' . Yii::t('backend', 'Last Day Ticket') . '</b></span></div>',
            ])->widget(\yii\widgets\MaskedInput::classname(), [
                'clientOptions' => [
                    'alias' => 'integer',
                    'autoGroup' => true,
                    'removeMaskOnSubmit' => true,
                    'allowMinus' => false,
                ]
            ])->textInput(['maxLength' => 3])->label(false)
            ?>
        </div>
    </div>

    <div class="col-md-12">
        <div class="col-lg-2 col-md-3 font_label" >
            <?= Yii::t('backend', 'Price Ticket') ?>
        </div>
        <div class="col-lg-2 col-md-4 input_sale_report_ct" >
            <?=
            $form->field($model, 'price_from')->widget(\yii\widgets\MaskedInput::className(), [
                'clientOptions' => [
                    'alias' => 'decimal',
                    'groupSeparator' => ',',
                    'autoGroup' => true,
                    'removeMaskOnSubmit' => true,
                    'allowMinus' => false,
                    'allowPlus' => false,
                ]
            ])->textInput(['maxlength' => 13])->label(false);
            ?> 
        </div>
        <div  class="col-md-1 text-center separator">～</div>
        <div class="col-lg-2 col-md-4 input_sale_report_ct">
            <?=
            $form->field($model, 'price_to')->widget(\yii\widgets\MaskedInput::className(), [
                'clientOptions' => [
                    'alias' => 'decimal',
                    'groupSeparator' => ',',
                    'autoGroup' => true,
                    'removeMaskOnSubmit' => true,
                    'allowMinus' => false,
                    'allowPlus' => false,
                ]
            ])->textInput(['maxlength' => 13])->label(false);
            ?> 
        </div>
    </div>
    <div class="form-group" style="padding-left: 1.7em;">
        <?= Html::a(Yii::t('backend', 'Return'), ['/'], ['class' => 'btn common-button-default btn-default']) ?>
        <?= Html::submitButton(Yii::t('backend', 'Search'), ['class' => 'btn common-button-submit']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
