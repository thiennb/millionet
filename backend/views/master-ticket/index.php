<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\LinkPager;
use yii\widgets\Pjax;
use common\components\Constants;
/* @var $this yii\web\View */
/* @var $searchModel common\models\MasterTicketSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$role = \common\components\FindPermission::getRole();
$this->title = Yii::t('backend', 'Master Ticket');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <div class="col-lg-12 col-md-12">
        <div class="box box-info box-solid">
            <div class="box-header with-border">
                <h4 class="text-title"><b><?= $this->title ?></b></h4>
            </div>
            <div class="box-body content">
                <div class="col-md-12">
                    <div class="common-box">
                        <div class="row">
                            <div class="box-header with-border common-box-h4 col-lg-8 col-md-12">
                                <h4 class="text-title"><b><?= Yii::t('backend', 'Search Ticket') ?></b></h4>
                            </div>
                        </div>

                        <div class="box-body content">
                            <div class="col-lg-10 col-md-12">
                                <?php echo $this->render('_search', ['model' => $searchModel]); ?>                                
                            </div>
                        </div>
                    </div>
                    <div class="common-box">
                        <div class="row">
                            <div class="box-header with-border common-box-h4 col-lg-8 col-md-12">
                                <h4 class="text-title"><b><?= Yii::t('backend', 'Ticket List') ?></b></h4>
                            </div>
                        </div>
                        <div class="box-body content">
                            <div class="col-md-10">
                                <?php
                                Pjax::begin();
                                ?>
                                <?php if ($role->permission_id != Constants::STAFF_ROLE)
                                    echo Html::a(Yii::t('backend', 'Create New Ticket'), ['create'], ['class' => 'pagination btn common-button-submit common-float-right']);
                                    ?>
                                <?=
                                GridView::widget([
                                    'dataProvider' => $dataProvider,
                                    'tableOptions' => [
                                        'class' => 'table table-striped table-bordered text-center',
                                    ],
                                    'layout' => "{pager}\n{summary}\n{items}",
                                    'summaryOptions' => ['class' => 'common-clr-fl-right'],
                                    'summary' => false,
                                    'pager' => [
                                        'class' => 'common\components\LinkPagerMillionet',
                                        'options' => ['class' => 'pagination common-float-right'],
                                    ],
                                    'columns' => [
                                        ['class' => 'yii\grid\SerialColumn', 'header' => 'No', 'options' => ['class' => 'with-table-no']],
                                        [
                                            'attribute' => 'name',
                                            'header' => Yii::t('backend', 'Name'),
                                        ],
                                        [
                                            'value' => function ($model) {
                                                $result = [];

                                                if (isset($model->productTicket)) {
                                                    foreach ($model->productTicket as $product) {
                                                        if (isset($product->product->name)) {
                                                            array_push($result, htmlentities($product->product->name));
                                                        }
                                                    }
                                                }
                                                return implode("<br /> ", $result);
                                            },
                                                    'label' => Yii::t('backend', 'Product Object'),
                                                    'format' => 'html',
                                                ],
                                                [
                                                    'attribute' => 'set_ticket',
                                                    'label' => Yii::t('backend', 'Set Ticket Search'),
                                                ],
                                                [
                                                    'attribute' => 'price',
                                                    'label' => Yii::t('backend', 'Price Search'),
                                                    'value' => function ($model) {
                                                        if ($model->price == null || empty($model->price)) {
                                                            return '¥0';
                                                        } else {
                                                            return '¥' . Yii::$app->formatter->asDecimal($model->price, 0);
                                                        }
                                                    },
                                                ],
                                                !($role->permission_id == Constants::STAFF_ROLE) ?                
                                                [
                                                    'class' => 'yii\grid\ActionColumn', 'template' => "{delete} {update}",
                                                    'options' => ['class' => 'with-table-button'],
                                                    'buttons' => [
                                                        //view button
                                                        'delete' => function ($url, $model) {
                                                            return Html::a('<span class="fa fa-delete"></span>' . Yii::t('backend', 'Delete'), $url, [
                                                                        //'title' => Yii::t('backend', 'Delete'),
                                                                        'class' => 'btn common-button-action',
                                                                        'aria-label' => "Delete",
                                                                        'data-confirm' => Yii::t('backend', "Delete Option Error Message"),
                                                                        'data-method' => "post"
                                                            ]);
                                                        },
                                                                'update' => function ($url, $model) {
                                                            return Html::a('<span class="fa fa-delete"></span>' . Yii::t('backend', 'Update'), $url, [
                                                                        //'title' => Yii::t('backend', 'Update'),
                                                                        'class' => 'btn common-button-action',
                                                            ]);
                                                        },
                                                            ],
                                                ]  
                                                :
                                                [
                                                    'class' => 'yii\grid\ActionColumn', 'template' => "{delete}",
                                                    'options' => ['class' => 'with-table-button'],
                                                    'buttons' => [
                                                        //view button
                                                        'delete' => function ($url, $model) {
                                                            return Html::a('<span class="fa fa-delete"></span>' . Yii::t('backend', 'Delete'), $url, [
                                                                        //'title' => Yii::t('backend', 'Delete'),
                                                                        'class' => 'btn common-button-action',
                                                                        'aria-label' => "Delete",
                                                                        'data-confirm' => Yii::t('backend', "Delete Option Error Message"),
                                                                        'data-method' => "post"
                                                            ]);
                                                        }
                                                            ],
                                                        ]            
                                                    ],
                                                ]);
                                                ?>
                                                <?php
                                                Pjax::end();
                                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


