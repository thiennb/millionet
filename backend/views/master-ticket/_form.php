<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\jui\DatePicker;
use yii\bootstrap\Modal;
use common\widgets\preview\Preview;
use common\models\MasterStore;
use yii\widgets\Pjax;
use yii\grid\GridView;
use common\components\Constants;

/* @var $this yii\web\View */
/* @var $model common\models\MasterTicket */
/* @var $form yii\widgets\ActiveForm */
$fieldOptions1 = [
    'options' => ['class' => 'form-group '],
    'template' => '<div class="col-md-12">
                <div class="col-lg-3 col-md-4 label-margin label_master_ticket_ct">
                   {label}
                </div>
                <div class="col-lg-4 col-md-5 input_sale_report_ct">
                   {input}{error}
                </div>
                </div>'
];
$role = \common\components\FindPermission::getRole();
?>
<style>

  .panel-heading .collapse-toggle[aria-expanded=true]:after {
    /* symbol for "opening" panels */
    font-family: 'Glyphicons Halflings';  /* essential for enabling glyphicon */
    content: "\e114";    /* adjust as needed, taken from bootstrap.css */
    float: right;        /* adjust as needed */
    color: grey;         /* adjust as needed */
  }
  .panel-heading .collapse-toggle.collapsed:after ,.panel-heading .collapse-toggle:after {
    /* symbol for "collapsed" panels */
    font-family: 'Glyphicons Halflings'; 
     float: right;        /* adjust as needed */
    color: grey;         /* adjust as needed */
    content: "\e080";    /* adjust as needed, taken from bootstrap.css */
  }
  .panel-heading .collapse-toggle {
      width: 100%;
      display: inherit;
  }
</style>
<div class="master-ticket-form">

    <?php
    $form = ActiveForm::begin([
                'options' => [
                    'enctype' => 'multipart/form-data',
                    'id' => strtolower($model->formName()) . '-id',
                ], // important
                'enableClientValidation' => false,
                // important
                'validationUrl' => 'validate',
                'enableClientValidation' => false,
                'enableAjaxValidation' => true,
                'validateOnChange' => false,
                'validateOnBlur' => false,
                'successCssClass' => '',
    ]);
    ?>
    <?= $form->field($model, '_role')->label(false)->hiddenInput([]) ?>
    <?= $form->field($model, 'id_store_comon')->label(false)->hiddenInput([]) ?>

    <div class="col-md-12">
        <div class="box-header with-border common-box-h4 col-lg-8 col-md-12">
            <h4 class="text-title"><b><?= Yii::t('backend', 'Ticket Infomation') ?></b></h4>
        </div>
    </div>
    <div class="col-md-12">
        <div class="col-lg-3 col-md-4 label-margin label_master_ticket_ct required-star" style="padding-left: 2em;">
            <?= Yii::t('backend', 'Store Ticket') ?>
        </div>
        <div class="col-lg-4 col-md-5 input_sale_report_ct ip_ticket_store" style="width: 32.5%; margin-left: 0.5em;">
            <?= $form->field($model, 'store_id')->dropDownList(MasterStore::getListStoreNotStoreCommon(), ['prompt' => Yii::t('backend', 'Please Select')])->label(false) ?>
        </div>
    </div>
    <div class="col-md-12">
        <?= $form->field($model, 'name', $fieldOptions1)->textInput(['maxlength' => true]) ?>
    </div>
    <div class="col-md-12">
        <?=
        $form->field($model, 'set_ticket', $fieldOptions1)->widget(\yii\widgets\MaskedInput::className(), [
            'clientOptions' => [
                'groupSeparator' => ',',
                'alias' => 'integer',
                'autoGroup' => true,
                'removeMaskOnSubmit' => true,
                'allowMinus' => false,
            ]
        ])->textInput(['maxlength' => true])
        ?>
    </div>
    <div class="col-md-12">
        <div class="col-lg-3 col-md-4 label-margin label_master_ticket_ct required-star" style="padding-left: 2em;">
            <?= Yii::t('backend', 'Date Expiration Ticket') ?>
        </div>
        <div class="col-lg-4 col-md-5 input_sale_report_ct">
            <?=
            $form->field($model, 'date_expiration', [
                'inputTemplate' => '<div class="input-group">{input}<span class="input-group-addon"><b>' . Yii::t('backend', 'Date Ticket') . '</b></span></div>',
            ])->widget(\yii\widgets\MaskedInput::className(), [
                'clientOptions' => [
                    'alias' => 'integer',
                    'autoGroup' => true,
                    'removeMaskOnSubmit' => true
                ]
            ])->textInput(['maxlength' => 3])->label(false);
            ?>
        </div>
    </div>
    <div class="col-md-12">
        <?=
        $form->field($model, 'price', $fieldOptions1)->widget(\yii\widgets\MaskedInput::className(), [
            'clientOptions' => [
                'alias' => 'decimal',
                'groupSeparator' => ',',
                'autoGroup' => true,
                'removeMaskOnSubmit' => true,
                'allowMinus' => false,
                'allowPlus' => false,
            ]
        ])->textInput(['maxlength' => true])
        ?>
    </div>
    <div class="col-md-12">
        <?=
        $form->field($model, 'ticket_jan_code', $fieldOptions1)->widget(\yii\widgets\MaskedInput::className(), [
            'clientOptions' => [
                'alias' => 'integer',
                'autoGroup' => true,
                'removeMaskOnSubmit' => true,
                'allowMinus' => false,
                'allowPlus' => false,
            ]
        ])->textInput(['maxlength' => true])
        ?>
        <?= $form->field($model, 'ticket_jan_code_old')->hiddenInput()->label(false) ?>
    </div>

    <?php Pjax::begin(['enablePushState' => false, 'id' => 'boxPajax']); ?>

    <div class="col-md-12">
        <div class="box-header with-border common-box-h4 col-lg-8 col-md-12">
            <h4 class="text-title">
                <b><?= Yii::t('backend', 'Title Product Ticket') ?></b>
            </h4>
        </div>
    </div>
    <div class="col-md-12">
        <div class="form-group">
            <?=
            Html::a(Yii::t('backend', 'Add Product Object'), ['#'], [
                'class' => 'btn common-button-submit',
                'id' => 'btn-option-product',
                'onclick' => 'init_product_slect_by_idstrore(event)'
            ])
            ?>
        </div>

        <?php
        Modal::begin([
            'id' => 'productModal',
            'size' => 'SIZE_LARGE',
            'header' => '<div class="box-header with-border common-box-h4 col-md-8"><h4 class="text-title"><b>' . Yii::t('backend', 'Product selection') . '</b></h4></div>',
            'footer' => Html::button(Yii::t('backend', 'Close'), ['class' => 'btn common-button-default', 'data-dismiss' => "modal", 'id' => 'clear-option-select'])
            . Html::button(Yii::t('backend', 'Choice'), ['class' => 'btn common-button-submit', 'data-dismiss' => "modal", 'id' => 'save-option-select']),
            'footerOptions' => [
                'class' => 'text-center',
            ],
            'closeButton' => FALSE,
            'options' => [
                'data-backdrop' => 'static',
                'data-keyboard' => 'false',
            ]
        ]);
        ?>
        <div class="" id="_product">
        </div>
        <?php Modal::end(); ?>
        <?= $form->field($model, 'option_hidden')->hiddenInput(['id' => 'option_hidden'])->label(FALSE) ?>
        <?=
        GridView::widget([
            'dataProvider' => $list_product_select,
            'layout' => "{pager}\n{items}",
            'summary' => false,
            'pager' => [
                'class' => 'common\components\LinkPagerMillionet',
                'options' => ['class' => 'pagination common-float-right'],
            ],
            'tableOptions' => [
                'class' => 'table table-striped table-bordered text-center',
                'id' => 'coupon_table',
            ],
            'columns' => [
                [
                    'class' => 'yii\grid\SerialColumn',
                    'header' => 'No',
                    'options' => ['class' => 'with-table-no'],
                    'headerOptions' => [
                        'class' => ['col-md-1']
                    ]
                ],
                [
                    'attribute' => 'name',
                    'label' => Yii::t('backend', 'Product name'),
                    'headerOptions' => [
                        'class' => ['col-md-3']
                    ]
                ],
                [
                    'attribute' => 'unit_price',
                    'headerOptions' => [
                        'class' => ['col-md-2']
                    ],
                    'value' =>
                    function ($model) {
                if (empty($model->unit_price)) {
                    return '¥0';
                } else {
                    return '¥' . Yii::$app->formatter->asDecimal($model->unit_price, 0);
                }
            },
                ],
            ],
        ]);
        ?>
        <?= Html::a(Yii::t('backend', 'Refrest'), '#', ['style' => 'display: none', 'id' => 'refresh-data-modal', 'push' => 'false']) ?>
    </div>
    <?php Pjax::end(); ?>
</div>

<div class="col-md-12">
    <div class="from-group">
        <?= Html::a(Yii::t('backend', 'Return'), ['index'], ['class' => 'btn btn-default common-button-default']) ?>
        <?=
        Html::submitButton(Yii::t('backend', 'Confirm'), [
            'class' => 'btn common-button-submit',
            'id' => 'btn-confirm',
        ])
        ?>
    </div>
</div>
<?php
Modal::begin([
    'id' => 'userModal',
    'size' => 'SIZE_LARGE',
    'header' => '<b>入力内容確認</b>',
    'footer' => Html::button(Yii::t('backend', 'Close'), ['class' => 'btn common-button-default', 'data-dismiss' => "modal", 'id' => 'btn-close'])
    . Html::submitButton(Yii::t('backend', 'Save'), ['class' => 'btn common-button-submit', 'id' => 'btn-submit']),
    'closeButton' => FALSE,
]);
?>

<?=
Preview::widget([
    "data" => [
        'store_id' => [
            'type' => 'select',
            'label' => Yii::t('backend', 'Store Ticket')
        ],
        'name' => [
            'type' => 'input',
            'label' => Yii::t('backend', 'Name Ticket')
        ],
        'set_ticket' => [
            'type' => 'input',
            'label' => Yii::t('backend', 'Set Ticket')
        ],
        'date_expiration' => [
            'type' => 'input',
            'label' => Yii::t('backend', 'Date Expiration Ticket')
        ],
        'price' => [
            'type' => 'input',
            'label' => Yii::t('backend', 'Price Ticket')
        ],
        'coupon_table' => [
            'type' => 'table-normal',
            'column' => '"0,1"',
            'table' => 'coupon_table'
        ],
    ],
    "modelName" => $model->formName(),
    'idBtnConfirm' => 'btn-confirm',
    'formId' => strtolower($model->formName()) . '-id',
    'btnClose' => 'btn-close',
    'btnSubmit' => 'btn-submit',
    'modalId' => 'userModal'
])
?>
<?php
Modal::end();
?>
<?php ActiveForm::end(); ?>
</div>


</div>
<script>
    jQuery.fn.preventDoubleSubmission = function () {

        var last_clicked, time_since_clicked;

        $("#btn-submit").bind('click', function (event) {

            if (last_clicked)
                time_since_clicked = event.timeStamp - last_clicked;

            last_clicked = event.timeStamp;

            if (time_since_clicked < 2000)
                return false;

            return true;
        });
    };
    $('#masterticket-id').preventDoubleSubmission();

    function init_product_slect_by_idstrore(e) {
        e.preventDefault();
        // var pathname = '/admin/master-coupon/loadproductbyidstore';
        var idStore = $('#<?= Html::getInputId($model, 'store_id') ?>').val();
        //$('.field-mastercoupon-error_product_tax .help-block-error').text('');
        var role = $('#masterticket-_role').val();
        //alert(role);
        if ((role != 2 || role != 3) && idStore == '') {
            var idStoreComon = $('#masterticket-id_store_comon').val();
            idStore = idStoreComon;
        }
        if (idStore) {
            var id = idStore;

            var pathname = '/admin/master-coupon/loadproductbyidstore';
            $.ajax({
                url: pathname,
                type: 'post',
                data: {
                    idStore: idStore
                },
                success: function (data) {
                    $("#_product").html('');
                    $("#_product").append(data);
                    $('#productModal').modal('show');

                    var $options = $('.option-checkbox');
                    var $option_hidden = $('#option_hidden').val();
                    var $option_hidden = ($option_hidden) ? $option_hidden.split(",") : $option_hidden;
                    $options.each(function () {
                        if ($.inArray($(this).val(), $option_hidden) >= 0) {
                            $(this).prop('checked', true);
                        } else {
                            $(this).prop('checked', false);
                        }
                    });
                }
            });

        } else {
            //alert("No Select Store");
        }
    }

    $('#refresh-data-modal').on('click', function (e) {
        e.preventDefault();
        var $option_hidden = $('#option_hidden').val();
        var urlController = window.location.href;

        $.pjax({
            type: 'POST',
            data: {
                option_hidden: $option_hidden
            },
            container: '#coupon_table',
            fragment: '#coupon_table',
            url: urlController,

        });
    });

    $(document).ready(function () {
        var idStore = $('#<?= Html::getInputId($model, 'store_id') ?>').val();

        if (idStore) {
            // $("#btn-option-product").attr("disabled", false);
        } else {
            // $("#btn-option-product").attr("disabled", true);
        }


        // ===========================================
        // Onclick select list store
        // ===========================================
        $('#<?= Html::getInputId($model, 'store_id') ?>').change(function () {
            var value = $(this).val();
            if (value) {

                // $("#btn-option-product").removeClass('disabled').removeAttr("disabled");

                $("#option_hidden").val('');

                $("#coupon_table").find('tbody').html('');

                var $options = $('.option-checkbox');
                var $option_hidden = $('#option_hidden').val();
                var $option_hidden = ($option_hidden) ? $option_hidden.split(",") : $option_hidden;
                $options.each(function () {
                    if ($.inArray($(this).val(), $option_hidden) >= 0) {
                        $(this).prop('checked', true);
                    } else {
                        $(this).prop('checked', false);
                    }
                });

            } else {
                //  $("#btn-option-product").attr("disabled", true).addClass('disabled');
            }
        });
    });

    $(document).on('pjax:success', function () {

        var $options = $('.option-checkbox');
        var $arr_option = '';
        var $option_hidden = [];
        $options.each(function () {
            if ($(this).prop('checked') == true) {
                $arr_option += $(this).parent().text() + '<br>';
                $option_hidden.push($(this).val());
            }
        });
        var list_id_product = null;
        var check = true;

        // GET value set_ticket and price
        var set_ticket = $('#masterticket-set_ticket').val();
        var price = $('#masterticket-price').val();

        if (set_ticket != '' && price == '') {

            if ($option_hidden.length > 0) {
                $option_hidden.forEach(function (element) {
                    if (check == true) {
                        list_id_product = element;
                        check = false;
                    } else {
                        list_id_product += ',' + element;
                    }
                });

                var pathControler = 'get-total-unit-price-by-id-product';

                $.ajax({
                    url: pathControler,
                    type: 'post',
                    data: {
                        listIdProduct: list_id_product
                    },
                    success: function (data) {

                        $('#masterticket-price').val(parseInt(data) * parseInt(set_ticket));
                    }
                });
            }

        }

    });

</script>
