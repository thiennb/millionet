<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\jui\DatePicker;
use yii\bootstrap\Modal;
use yii\helpers\ArrayHelper;
use common\models\MasterStore;
use yii\grid\GridView;
use common\components\Constants;
use yii\widgets\Pjax;



/* @var $this yii\web\View */
/* @var $model common\models\MasterStore */
/* @var $form yii\widgets\ActiveForm */
?>
<style>
    select{
        min-width: 79px;
    }
    input{
        min-width: 75px;
    }
</style>
<div class="store-shift-form">

    <div class="col-md-12">
        <?php $form = ActiveForm::begin([
                'enableClientValidation' => false,
                'action' => ['index'],
                'id' => 'search-store-shift-form',
                'options' => ['class' => 'form-horizontal']
        ]) ?>
        <div class="col-md-2 label-margin no-padding">
            <?= Yii::t("backend", "Select a store"); ?>
        </div>
        <div class="col-md-2">
            <?= $form->field($model, 'store_id')->label(false)->dropDownList(MasterStore::getListStore(), [
            'tag' => false,
        ]) ?>
        </div>
        <div class="col-md-3">
            <?= Html::submitButton(Yii::t('backend', 'display'),['class' => 'btn common-button-submit', 'id' => 'search'])?>
        </div>
        <?php $form = ActiveForm::end() ?>
    </div>

    <div class="col-md-12">
        <table class = "table table-striped table-bordered text-center col-lg-12">
            <thead>
                <tr>
                    <th class="col-md-3"><?= Yii::t('backend', 'Name Tax') ?></th>
                    <th class="col-md-3"><?= Yii::t('backend', 'Short Name') ?></th>
                    <th><?= Yii::t('backend', 'Setting Time') ?></th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <?php $i=0;
                foreach ( $storeShifts as $storeShift ): ?>
                <tr data-id="<?= $storeShift->id ?>">
                    <td><?= \yii\helpers\Html::encode($storeShift->name) ?></td>
                    <td><?= \yii\helpers\Html::encode($storeShift->short_name) ?></td>
                    <td><?= \yii\helpers\Html::encode($storeShift->start_time) ?> ～ <?= \yii\helpers\Html::encode($storeShift->end_time) ?></td>
                    <td><?php if( $i >0 ){
                        echo Html::a(Yii::t('backend', 'Delete'), ['delete', 'id' => $storeShift->id,'store_id' => $storeShift->store_id], [
                                                                    'title' => Yii::t('backend', 'Delete'),
                                                                    'class' => 'btn common-button-submit ',
                                                                    'aria-label' => "Delete",
                                                                    'data-confirm' => Yii::t('backend', "Because of this shift information is in use, it can not be deleted."),
                                                                    'data-method' => "post" ]);} ?></td>
                </tr>
                <?php $i++ ;
                endforeach; ?>
                <tr class="last-row">
                    <?php $form = ActiveForm::begin([
                            'action' => ['create'],
                            'id' => 'store-shift-form',
                            'options' => ['class' => 'form-horizontal'],
//                            'enableAjaxValidation' => true,
                    ]) ?>
                    <?= $form->field($model, 'store_id')->hiddenInput(['id' => 'store_id' ])->label(false); ?>
                    <td><?= $form->field($model, 'name')->textInput(['maxlength'=> '50' ,'class' => 'form-control'])->label(false) ?></td>
                    <td><?= $form->field($model, 'short_name')->textInput(['maxlength'=> '20','class' => 'form-control'])->label(false) ?></td>
                    <td class="col-lg-5 col-md-6">
                        <div class="col-lg-5 col-md-12">
                            <?= $form->field($model, 'start_time')->label(false)->dropDownList($list_time, ['prompt' => Yii::t('backend', '')]) ?>
                        </div>
                        <div class="col-lg-2 col-md-12 label-margin label-center store_shift_sp">～</div>
                        <div class="col-lg-5 col-md-12">
                            <?= $form->field($model, 'end_time')->label(false)->dropDownList($list_time, ['prompt' => Yii::t('backend', '')]) ?>
                        </div>
                    </td>
                    <td><?= Html::submitButton(Yii::t('backend', 'Add'), ['class' => 'btn common-button-submit', 'id' => 'save'])?></td>
                    <?php $form = ActiveForm::end() ?>
                </tr>

            </tbody>
        </table>
    </div>

    <div class="col-md-12">
        <div class="col-md-6 magin_button text-right">
            <div class="form-group">
                <?= Html::a(Yii::t('backend', 'Return'), ['/'], ['class' => 'btn btn-default common-button-default']) ?>
            </div>
        </div>
    </div>

</div>
