<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use common\components\Constants;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $searchModel common\models\MasterRankSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$role = \common\components\FindPermission::getRole();
$this->title = Yii::t('backend', 'Rank list');
$this->params['breadcrumbs'][] = $this->title;
$this->registerJsFile('@web/js/rank/check_rank.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
?>
<style>
    .control-label{
        line-height: 30px;
    }
</style>
<div class="row">
    <div class="col-lg-12 col-md-12">
        <div class="box box-info box-solid">
            <div class="box-header with-border">
                <h4 class="text-title"><b><?= $this->title ?></b></h4>
            </div>

            <div class="box-body content">
                <div class="col-md-12">
                    <div class="common-box">
                        <div class="box-header with-border common-box-h4 col-lg-8 col-md-12">
                            <h4 class="text-title"><b><?= Yii::t('backend', 'Rank list') ?></b></h4>
                        </div>
                        <div class="box-body content">
                            <div class="col-md-8">
                                <?php
                                $form = ActiveForm::begin([
                                            'enableClientValidation' => false,
                                ]);
                                ?>
                                <?= $form->field($model, 'rank_apply_type', ['template' => "<div class='col-md-2 no-padding'>{label}</div><div class='col-md-3'>{input}</div>\n{hint}\n{error}\n"])->dropDownList(Constants::LIST_RANKUP_TYPE); ?>
                                <br>
                                <div class="rankup-intermediate  form-group"> 
                                    <br>


                                    <?=
                                    $form->field($model, 'days_reset_rank', ['template' => "・ランクアップ条件を満たせばすぐランクアップします。<br>  ・前回来店日から {input} 日来店がないとランクを初期化します。\n{error}"])->widget(\yii\widgets\MaskedInput::className(), [
                                        'clientOptions' => [
                                            'alias' => 'integer',
                                            'autoGroup' => true,
                                            'removeMaskOnSubmit' => true
                                        ]
                                    ])->textInput(['style' => "width : 100px;display: inline;", 'maxlength' => true, 'class'=>'form-control convert-haft-size'])
                                    ?>

                                </div>

                                <div class="rankup-next-month  form-group"> 
                                    <br>
                                    ・先月のランクアップ条件の達成状況によって、毎月 1 日にランクアップを行います。</br>
                                    ・毎月 1 日にランクを初期化します。ランクアップはランク初期化後に行います。
                                </div>

                                <?php $model->rank_condition = trim($model->rank_condition); ?>
                                <div class="row">
                                    <div class='col-md-2 font_label'><?= Yii::t('backend', 'Rank up conditions') ?></div>
                                    <div class='col-md-9'>
                                        <?= $form->field($model, 'rank_condition')->label(false)->inline(true)->radioList(Constants::LIST_CONDITION_RANK, ['separator' => '', 'tabindex' => 3]) ?>
                                    </div>
                                </div>

                                <div class="form-group has-error check_rank_condition_ms">
                                    <?= $form->field($model, 'check_rank_condition', ['template' => "{error}"]) ?>
                                </div>
                                <table class="table table-bordered text-center"> 
                                    <thead> 
                                        <tr> 
                                            <th rowspan="2" style="min-width: 100px;padding-bottom: 2em;"> <?= Yii::t('backend', 'Rank name') ?></th> 
                                            <th> <?= Yii::t('backend', 'Rank award') ?> </th> 
                                            <th colspan="3"> <?= Yii::t('backend', 'Rank up conditions') ?> </th> 

                                        </tr> 
                                        <tr> 
                                            <th> <?= Yii::t('backend', 'Rank point rate') ?></th> 
                                            <th> <?= Yii::t('backend', 'Rank time visit') ?> </th> 
                                            <th> <?= Yii::t('backend', 'Point total') ?></th> 
                                            <th> <?= Yii::t('backend', 'Rank use money') ?></th> 
                                        </tr> 
                                    </thead> 
                                    <tbody> 
                                        <tr> 
                                            <td scope="row" style="font-weight: bold;;padding-right: 2em;"> <?= Yii::t('backend', 'Normal') ?></td> 
                                            <td> 
                                                <?=
                                                $form->field($model, 'normal_point_rate', [
                                                    'inputTemplate' => '<div class="input-group">{input}<span class="input-group-addon">倍</span></div>',
                                                ])->widget(\yii\widgets\MaskedInput::className(), [
                                                    'clientOptions' => [
                                                        'alias' => 'decimal',
                                                        'autoGroup' => true,
                                                        'removeMaskOnSubmit' => true
                                                    ]
                                                ])->textInput(['maxlength' => true,'class'=>'form-control convert-haft-size'])->label(false)
                                                ?>
                                            </td> 
                                            <td> 
                                                <?=
                                                $form->field($model, 'normal_time_visit', [
                                                    'inputTemplate' => '<div class="input-group">{input}<span class="input-group-addon">回</span></div>',
                                                ])->label(false)->widget(\yii\widgets\MaskedInput::className(), [
                                                    'clientOptions' => [
                                                        'alias' => 'integer',
                                                        'autoGroup' => true,
                                                        'removeMaskOnSubmit' => true
                                                    ]
                                                ])->textInput(['maxlength' => true, 'disabled' => true, 'class'=>'form-control convert-haft-size'])->label(false)
                                                ?>
                                            </td> 
                                            <td> 
                                                <?=
                                                $form->field($model, 'normal_point_total', [
                                                    'inputTemplate' => '<div class="input-group">{input}<span class="input-group-addon">P</span></div>',
                                                ])->widget(\yii\widgets\MaskedInput::className(), [
                                                    'clientOptions' => [
                                                        'alias' => 'decimal',
                                                        'autoGroup' => true,
                                                        'removeMaskOnSubmit' => true
                                                    ]
                                                ])->textInput(['maxlength' => true, 'disabled' => true, 'class'=>'form-control convert-haft-size'])->label(false)
                                                ?>
                                            </td> 
                                            <td> 
                                                <?=
                                                $form->field($model, 'normal_use_money', [
                                                    'inputTemplate' => '<div class="input-group">{input}<span class="input-group-addon">円</span></div>',
                                                ])->widget(\yii\widgets\MaskedInput::className(), [
                                                    'clientOptions' => [
                                                        'alias' => 'integer',
                                                        'groupSeparator' => ',',
                                                        'autoGroup' => true,
                                                        'removeMaskOnSubmit' => true
                                                    ]
                                                ])->textInput(['maxlength' => true, 'disabled' => true, 'class'=>'form-control convert-haft-size'])->label(false)
                                                ?>
                                            </td> 
                                        </tr>   
                                        <tr> 
                                            <td scope="row">
                                                <?=
                                                $form->field($model, 'bronze_status')->checkbox([
                                                    'template' => '<div class="checkbox"> <div class="row row-inline font_label">{beginLabel}{labelTitle}{endLabel}</div>{input}{error}{hint}</div>'], true
                                                );
                                                ?>
                                            </td> 
                                            <td> 
                                                <?=
                                                $form->field($model, 'bronze_point_rate', [
                                                    'inputTemplate' => '<div class="input-group">{input}<span class="input-group-addon">倍</span></div>',
                                                ])->label(false)->widget(\yii\widgets\MaskedInput::className(), [
                                                    'clientOptions' => [
                                                        'alias' => 'decimal',
                                                        'autoGroup' => true,
                                                        'removeMaskOnSubmit' => true
                                                    ]
                                                ])->textInput(['maxlength' => true, 'class'=>'form-control convert-haft-size'])
                                                ?>
                                            </td> 
                                            <td> 
                                                <?=
                                                $form->field($model, 'bronze_time_visit', [
                                                    'inputTemplate' => '<div class="input-group">{input}<span class="input-group-addon">回</span></div>',
                                                ])->label(false)->widget(\yii\widgets\MaskedInput::className(), [
                                                    'clientOptions' => [
                                                        'alias' => 'integer',
                                                        'groupSeparator' => ',',
                                                        'autoGroup' => true,
                                                        'removeMaskOnSubmit' => true
                                                    ]
                                                ])->textInput(['maxlength' => true, 'class'=>'form-control convert-haft-size'])
                                                ?>
                                            </td> 
                                            <td> 
                                                <?=
                                                $form->field($model, 'bronze_point_total', [
                                                    'inputTemplate' => '<div class="input-group">{input}<span class="input-group-addon">P</span></div>',
                                                ])->label(false)->widget(\yii\widgets\MaskedInput::className(), [
                                                    'clientOptions' => [
                                                        'groupSeparator' => ',',
                                                        'alias' => 'integer',
                                                        'autoGroup' => true,
                                                        'removeMaskOnSubmit' => true,
                                                        'allowMinus' => false,
                                                    ]
                                                ])->textInput(['maxlength' => true, 'class'=>'form-control convert-haft-size'])
                                                ?>
                                            </td> 
                                            <td> 
                                                <?=
                                                $form->field($model, 'bronze_use_money', [
                                                    'inputTemplate' => '<div class="input-group">{input}<span class="input-group-addon">円</span></div>',
                                                ])->widget(\yii\widgets\MaskedInput::className(), [
                                                    'clientOptions' => [
                                                        'alias' => 'integer',
                                                        'groupSeparator' => ',',
                                                        'autoGroup' => true,
                                                        'removeMaskOnSubmit' => true
                                                    ]
                                                ])->textInput(['maxlength' => true, 'class'=>'form-control convert-haft-size'])->label(false)
                                                ?>
                                            </td> 
                                        </tr> 
                                        <tr> 
                                            <td scope="row"> 
                                                <?=
                                                $form->field($model, 'silver_status')->checkbox([
                                                    'template' => '<div class="checkbox"> <div class="row row-inline font_label">{beginLabel}{labelTitle}{endLabel}</div>{input}{error}{hint}</div>'], true
                                                );
                                                ?>
                                            <td> 
                                                <?=
                                                $form->field($model, 'sliver_point_rate', [
                                                    'inputTemplate' => '<div class="input-group">{input}<span class="input-group-addon">倍</span></div>',
                                                ])->label(false)->widget(\yii\widgets\MaskedInput::className(), [
                                                    'clientOptions' => [
                                                        'alias' => 'decimal',
                                                        'autoGroup' => true,
                                                        'removeMaskOnSubmit' => true
                                                    ]
                                                ])->textInput(['maxlength' => true, 'class'=>'form-control convert-haft-size'])
                                                ?>
                                            </td> 
                                            <td> 
                                                <?=
                                                $form->field($model, 'sliver_time_visit', [
                                                    'inputTemplate' => '<div class="input-group">{input}<span class="input-group-addon">回</span></div>',
                                                ])->label(false)->widget(\yii\widgets\MaskedInput::className(), [
                                                    'clientOptions' => [
                                                        'alias' => 'integer',
                                                        'groupSeparator' => ',',
                                                        'autoGroup' => true,
                                                        'removeMaskOnSubmit' => true
                                                    ]
                                                ])->textInput(['maxlength' => true, 'class'=>'form-control convert-haft-size'])
                                                ?>
                                            </td> 
                                            <td> 
                                                <?=
                                                $form->field($model, 'sliver_point_total', [
                                                    'inputTemplate' => '<div class="input-group">{input}<span class="input-group-addon">P</span></div>',
                                                ])->label(false)->widget(\yii\widgets\MaskedInput::className(), [
                                                    'clientOptions' => [
                                                        'alias' => 'integer',
                                                        'groupSeparator' => ',',
                                                        'autoGroup' => true,
                                                        'removeMaskOnSubmit' => true
                                                    ]
                                                ])->textInput(['maxlength' => true, 'class'=>'form-control convert-haft-size'])
                                                ?>
                                            </td> 
                                            <td> 
                                                <?=
                                                $form->field($model, 'sliver_use_money', [
                                                    'inputTemplate' => '<div class="input-group">{input}<span class="input-group-addon">円</span></div>',
                                                ])->widget(\yii\widgets\MaskedInput::className(), [
                                                    'clientOptions' => [
                                                        'alias' => 'integer',
                                                        'groupSeparator' => ',',
                                                        'autoGroup' => true,
                                                        'removeMaskOnSubmit' => true
                                                    ]
                                                ])->textInput(['maxlength' => true, 'class'=>'form-control convert-haft-size'])->label(false)
                                                ?>
                                            </td>   
                                        </tr> 
                                        <tr> 
                                            <td scope="row"> 
                                                <?=
                                                $form->field($model, 'gold_status')->checkbox([
                                                    'template' => '<div class="checkbox"> <div class="row row-inline font_label">{beginLabel}{labelTitle}{endLabel}</div>{input}{error}{hint}</div>'], true
                                                );
                                                ?>
                                            </td> 
                                            <td> 
                                                <?=
                                                $form->field($model, 'gold_point_rate', [
                                                    'inputTemplate' => '<div class="input-group">{input}<span class="input-group-addon">倍</span></div>',
                                                ])->label(false)->widget(\yii\widgets\MaskedInput::className(), [
                                                    'clientOptions' => [
                                                        'alias' => 'decimal',
                                                        'autoGroup' => true,
                                                        'removeMaskOnSubmit' => true
                                                    ]
                                                ])->textInput(['maxlength' => true, 'class'=>'form-control convert-haft-size'])
                                                ?>
                                            </td> 
                                            <td> 
                                                <?=
                                                $form->field($model, 'gold_time_visit', [
                                                    'inputTemplate' => '<div class="input-group">{input}<span class="input-group-addon">回</span></div>',
                                                ])->label(false)->widget(\yii\widgets\MaskedInput::className(), [
                                                    'clientOptions' => [
                                                        'alias' => 'integer',
                                                        'groupSeparator' => ',',
                                                        'autoGroup' => true,
                                                        'removeMaskOnSubmit' => true
                                                    ]
                                                ])->textInput(['maxlength' => true, 'class'=>'form-control convert-haft-size'])
                                                ?>
                                            </td> 
                                            <td> 
                                                <?=
                                                $form->field($model, 'gold_point_total', [
                                                    'inputTemplate' => '<div class="input-group">{input}<span class="input-group-addon">P</span></div>',
                                                ])->label(false)->widget(\yii\widgets\MaskedInput::className(), [
                                                    'clientOptions' => [
                                                        'alias' => 'integer',
                                                        'groupSeparator' => ',',
                                                        'autoGroup' => true,
                                                        'removeMaskOnSubmit' => true
                                                    ]
                                                ])->textInput(['maxlength' => true, 'class'=>'form-control convert-haft-size'])
                                                ?>
                                            </td> 
                                            <td> 
                                                <?=
                                                $form->field($model, 'gold_use_money', [
                                                    'inputTemplate' => '<div class="input-group">{input}<span class="input-group-addon">円</span></div>',
                                                ])->widget(\yii\widgets\MaskedInput::className(), [
                                                    'clientOptions' => [
                                                        'alias' => 'integer',
                                                        'groupSeparator' => ',',
                                                        'autoGroup' => true,
                                                        'removeMaskOnSubmit' => true
                                                    ]
                                                ])->textInput(['maxlength' => true, 'class'=>'form-control convert-haft-size'])->label(false)
                                                ?>
                                            </td>   
                                        </tr> 
                                        <tr> 
                                            <td scope="row">
                                                <?=
                                                $form->field($model, 'black_status')->checkbox([
                                                    'template' => '<div class="checkbox"> <div class="row row-inline font_label">{beginLabel}{labelTitle}{endLabel}</div>{input}{error}{hint}</div>'], true
                                                );
                                                ?>

                                            </td> 
                                            <td> 
                                                <?=
                                                $form->field($model, 'black_point_rate', [
                                                    'inputTemplate' => '<div class="input-group">{input}<span class="input-group-addon">倍</span></div>',
                                                ])->label(false)->widget(\yii\widgets\MaskedInput::className(), [
                                                    'clientOptions' => [
                                                        'alias' => 'decimal',
                                                        'autoGroup' => true,
                                                        'removeMaskOnSubmit' => true
                                                    ]
                                                ])->textInput(['maxlength' => true, 'class'=>'form-control convert-haft-size'])
                                                ?>
                                            </td> 
                                            <td> 
                                                <?=
                                                $form->field($model, 'black_time_visit', [
                                                    'inputTemplate' => '<div class="input-group">{input}<span class="input-group-addon">回</span></div>',
                                                ])->label(false)->widget(\yii\widgets\MaskedInput::className(), [
                                                    'clientOptions' => [
                                                        'alias' => 'integer',
                                                        'groupSeparator' => ',',
                                                        'autoGroup' => true,
                                                        'removeMaskOnSubmit' => true
                                                    ]
                                                ])->textInput(['maxlength' => true, 'class'=>'form-control convert-haft-size'])
                                                ?>
                                            </td> 
                                            <td> 
                                                <?=
                                                $form->field($model, 'black_point_total', [
                                                    'inputTemplate' => '<div class="input-group">{input}<span class="input-group-addon">P</span></div>',
                                                ])->label(false)->widget(\yii\widgets\MaskedInput::className(), [
                                                    'clientOptions' => [
                                                        'alias' => 'integer',
                                                        'groupSeparator' => ',',
                                                        'autoGroup' => true,
                                                        'removeMaskOnSubmit' => true
                                                    ]
                                                ])->textInput(['maxlength' => true, 'class'=>'form-control convert-haft-size'])
                                                ?>
                                            </td> 
                                            <td> 
                                                <?=
                                                $form->field($model, 'black_use_money', [
                                                    'inputTemplate' => '<div class="input-group">{input}<span class="input-group-addon">円</span></div>',
                                                ])->widget(\yii\widgets\MaskedInput::className(), [
                                                    'clientOptions' => [
                                                        'alias' => 'integer',
                                                        'groupSeparator' => ',',
                                                        'autoGroup' => true,
                                                        'removeMaskOnSubmit' => true
                                                    ]
                                                ])->textInput(['maxlength' => true, 'class'=>'form-control convert-haft-size'])->label(false)
                                                ?>
                                            </td>  
                                        </tr>  
                                    </tbody> 
                                </table>
                                <div class="form-group" style="text-align: center;">
                                    <?= Html::a(Yii::t('backend', 'Return'), ['/'], ['class' => 'btn btn-default common-button-default']) ?>
                                    <?php
                                    if ($role->permission_id == Constants::APP_MANAGER_ROLE || $role->permission_id == Constants::COMPANY_MANAGER_ROLE)
                                        echo Html::submitButton(Yii::t('backend', 'Save'), ['class' => 'btn common-button-submit'])
                                        ?>
                                </div>

                                <?php ActiveForm::end(); ?>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

<script>
<?php if (!($role->permission_id == Constants::APP_MANAGER_ROLE || $role->permission_id == Constants::COMPANY_MANAGER_ROLE)) { ?>
        $(':input').prop("disabled", true);
<?php } ?>

    $(function () {
        function showCondition() {
            var rankup = $("#masterrank-rank_apply_type").val();
            $(".rankup-next-month").show();
            $(".rankup-intermediate").show();
            if (rankup == 0) {
                $(".rankup-next-month").hide();
            } else {
                $(".rankup-intermediate").hide();
            }

        }
        showCondition();
        $("#masterrank-rank_apply_type").on('change', function () {
            showCondition();

        });

    })
</script>
