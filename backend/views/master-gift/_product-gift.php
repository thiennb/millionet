<style>
/*    .select-product-form table tr:hover{
        background: #eee;
        cursor: pointer
    }*/
     .panel-heading .collapse-toggle[aria-expanded=true]:after {
    /* symbol for "opening" panels */
    font-family: 'Glyphicons Halflings';  /* essential for enabling glyphicon */
    content: "\e114";    /* adjust as needed, taken from bootstrap.css */
    float: right;        /* adjust as needed */
    color: grey;         /* adjust as needed */
  }
  .panel-heading .collapse-toggle.collapsed:after ,.panel-heading .collapse-toggle:after {
    /* symbol for "collapsed" panels */
    font-family: 'Glyphicons Halflings'; 
     float: right;        /* adjust as needed */
    color: grey;         /* adjust as needed */
    content: "\e080";    /* adjust as needed, taken from bootstrap.css */
  }
  .panel-heading .collapse-toggle {
      width: 100%;
      display: inherit;
  }
</style>
<div class="select-product-form">
    <div class="panel-group" id="select-group-product">
        <?php
        $count = 0;
        foreach($products as $key => $value):
        $count++;   
        ?>
        <div class="panel panel-default">
            <div class="panel-heading" >
                <h4 class="panel-title ">
                  <a class="collapse-toggle" data-toggle="collapse" data-target="#select-product-<?php echo $count ?>" data-parent="#select-group-product"><?php echo $key ?></a>
                  
<!--                    <span class="pull-right"><i class="glyphicon glyphicon-chevron-down"></i></span>-->
                </h4>
            </div>
            <div id="select-product-<?php echo $count ?>" class="panel-collapse collapse">
                <div class="panel-body">
                    <table class="table" style="margin:0">
                    <?php foreach($value as $p => $product): ?>
                        <tr>
                            <td width="20px"><input type="checkbox" id="detail-product-<?php echo $p ?>" data-code="<?php echo $product['jan_code']; ?>" data-id="<?php echo $product['id']; ?>" data-name="<?php echo $product['name']; ?>"/></td>
                            <td class="text-left"><label for="detail-product-<?php echo $p ?>"><?php echo $product['name']; ?></label></td>
                            <!--<td class="text-left"><?php echo $product['price']; ?></td>-->
                        </tr>
                    <?php endforeach; ?>
                    </table>
                </div>
            </div>
        </div>
        <?php endforeach; ?>
    </div>
</div>