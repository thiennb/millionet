<?php

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\bootstrap\Modal;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$form = ActiveForm::begin();
?>
<style>
    .search-customer-table table tbody tr:hover{
        background: #eee;
        cursor: pointer
    }
</style>
<div class="table-responsive search-customer-table">
    <table class="table-bordered col-md-12">
        <thead>
            <tr>
                <th class="col-md-2">

                </th>
                <th class="col-md-2" style="min-width: 150px;">
                    宛名
                </th>
                <th class="col-md-2" style="min-width: 150px;">
                    電話番号
                </th>
                <th class="col-md-2" style="min-width: 150px;">
                    郵便番号
                </th>
                <th class="col-md-2" style="min-width: 200px;">
                    住所1
                </th>
                <th class="col-md-2" style="min-width: 200px;">
                    住所2
                </th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($list_customer_select as $key => $data) { ?>
                <tr>
                    <td><input type="radio" name="MasterCustomer[select_customer][id]" value="<?= $data->id ?>" key="<?= $key ?>" ></td>
                    <td><span> <?= $data->first_name . $data->last_name ?> </span> 
                    <?= Html::hiddenInput('last_name', $data->last_name, ['id' => 'last_name_'.$key]); ?>
                    <?= Html::hiddenInput('first_name', $data->first_name, ['id' => 'first_name_'.$key]); ?> 
                        <?= Html::hiddenInput('last_name_kana', $data->last_name_kana, ['id' => 'last_name_kana_'.$key]); ?>
                    <?= Html::hiddenInput('first_name_kana', $data->first_name_kana, ['id' => 'first_name_kana_'.$key]); ?>
                        <?= Html::hiddenInput('customer_jan_code', $data->customer_jan_code, ['id' => 'customer_jan_code_'.$key]); ?> 
                    </td>
                    <td><?= $data->mobile ?> <?= Html::hiddenInput('mobile', $data->mobile, ['id' => 'mobile_'.$key]); ?></td>
                    <td><?= $data->post_code ?> <?= Html::hiddenInput('post_code', $data->post_code, ['id' => 'post_code_'.$key]); ?></td>
                    <td><?= $data->address ?> <?= Html::hiddenInput('address', $data->address, ['id' => 'address_'.$key]); ?></td>
                    <td><?= $data->address2 ?> <?= Html::hiddenInput('address2', $data->address2, ['id' => 'address2_'.$key]); ?></td>
                </tr>
           <?php } ?>
        </tbody>
    </table>
</div>