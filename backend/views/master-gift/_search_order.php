<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\jui\DatePicker;
use common\models\MasterStore;
use yii\bootstrap\ActiveForm;


/* @var $this yii\web\View */
/* @var $model common\models\MasterGiftSearch */
/* @var $form yii\widgets\ActiveForm */
?>
<style>
    @media (min-width: 1200px) {
        .col-lg-1_5 {
            width: 10.33333333%;
        }
        .col-lg-0 {
            display: none;
        }
        .col-lg-0_5  {
            width: 3.33333334%;
        }

        .col-lg-1_5_add  {
            width: 7%;
        }
        .col-lg-2_5  {
            width: 13.33333333%;
        }
    }

</style>
<div class="master-order-search">
    <?php
    $form = ActiveForm::begin([
                'action' => ['index-order'],
                'enableClientValidation' => false,
                'method' => 'get',
    ]);
    ?>
    <div class="col-md-12">
        <div class="col-lg-2 col-md-3 font_label" >
            <?= Yii::t('backend', 'Store Id') ?>
        </div>
        <div class="col-lg-4 col-md-6 input_gift_search_ct" >
            <?= $form->field($model, 'store_id')->dropDownList(MasterStore::getListStore(), ['prompt' => Yii::t('backend', 'Please Select')])->label(false) ?>
        </div>
    </div>
    
    <div class="col-md-12">
        <div class="col-lg-2 col-md-3 font_label" >
            <?= Yii::t('backend', 'Order Code') ?>
        </div>
        <div class="col-lg-4 col-md-6 input_gift_search_ct" >
            <?= $form->field($model, 'order_code')->label(false)->textInput(['maxLength' => true]) ?>
        </div>
    </div>
    <div class="col-md-12">
        <div class="col-lg-2 col-md-3 font_label" >
            <?= Yii::t('backend', 'Date') ?>
        </div>
        <div class="col-lg-3 col-md-6 input_gift_search_ct" >
            <?=
            $form->field($model, 'process_date_from')->widget(DatePicker::classname(), [
                'language' => 'ja',
                'dateFormat' => 'yyyy/MM/dd',
                'clientOptions' => [
                    "changeMonth" => true,
                    "changeYear" => true,
                    "yearRange" => "1900:+0"
                ],
            ])->textInput(['maxLength' => 10])->label(false)
            ?>
        </div>
        <div  class="col-md-1 text-center ct_gift" style="width: 0.4em;">～</div>
        <div class="col-lg-3 col-md-4 input_gift_search_ct">
            <?=
            $form->field($model, 'process_date_to')->widget(DatePicker::classname(), [
                'language' => 'ja',
                'dateFormat' => 'yyyy/MM/dd',
                'clientOptions' => [
                    "changeMonth" => true,
                    "changeYear" => true,
                    "yearRange" => "1900:+0"
                ],
            ])->textInput(['maxLength' => 10])->label(false)
            ?>
        </div>
    </div>
    
    <!-- customer_first_name  start -->
    <div class="col-md-12">
        <div class="col-lg-2 col-md-3 font_label">  
            <?= Yii::t('backend', 'Purchaser') ?>
        </div>
        <div class="col-lg-0_5 col-md-1 font_label label-margin">
            <?=
            Yii::t("backend", "First name");
            ?>
        </div>
        <div class="col-lg-3 col-md-3 first_name_staff_ct" >
            <?=
            $form->field($model, 'customer_first_name')->textInput(['maxlength' => true])->label(false)
            ?>
        </div>
        <div class="col-lg-0_5 col-md-1 font_label label-margin">
            <?=
            Yii::t("backend", "Last Name Cuopon");
            ?>
        </div>
        <div class="col-lg-3 col-md-3 last_name_staff_ct">
            <?=
            $form->field($model, 'customer_last_name')->textInput(['maxlength' => true])->label(false)
            ?>
        </div>
    </div>
    <!-- customer_first_name end -->
    
    
    <div class="col-md-12">
        <div class="col-lg-2 col-md-3 font_label" >
            <?= Yii::t('backend', "Product_Name") ?>
        </div>
        <div class="col-lg-4 col-md-6 input_gift_search_ct" >
            <?= $form->field($model, 'product_name')->label(false)->textInput(['maxLength' => true]) ?>
        </div>
    </div>

    <div class="form-group">
       <?= Html::a(Yii::t('backend', 'Return'), ['/'], ['class' => 'btn common-button-default btn-default']) ?>
        <?= Html::submitButton(Yii::t('backend', 'Search'), ['class' => 'btn common-button-submit']) ?>
    </div> 
<?php ActiveForm::end(); ?>

</div>
