<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Modal;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model common\models\MasterGift */
/* @var $form yii\widgets\ActiveForm */
?>

<style> 
    .field-mastercustomer-last_name, .field-mastercustomer-first_name ,.field-mastercustomer-first_name_kana , .field-mastercustomer-last_name_kana{
        display: inline-flex;
        width: 50%;
    }
    .radio{
        display: inline-block;
    }
    div.required label.control-label:after {
        content: " *";
        color: red;
    }
    .row-name{
        margin-top: -15px;
    }
</style>

<?php
$fieldOptions1 = [
    'options' => ['class' => 'form-group row row-inline'],
    'template' => "<div class='col-md-3 no-padding'>{label}</div><div class='col-md-4 no-padding input_store_ct'>{input}\n{hint}\n{error}</div>\n"
];

$fieldOptions2 = [
    'options' => ['class' => 'form-group col-md-5'],
    'template' => "<div class='col-md-3 label_name_gift_ct'>{label}</div><div class='col-md-8 name_gift_ct'>{input}\n{hint}\n{error}</div>\n"
];

$list_product = implode(", ", $product_gift);

?>
<?php Pjax::begin(['id' => 'p2']) ?>
<?php
$form = ActiveForm::begin([
            'layout' => 'horizontal',
            'enableClientValidation' => false,
            'options' => ['data-pjax' => true],
            'fieldConfig' => [
               // 'template' => "{beginWrapper}<div class='col-md-3'>{label}</div><div class='col-md-'>{input}\n{hint}\n{error}</div>\n{endWrapper}",
                'horizontalCssClasses' => [
                    'label' => '',
                    'wrapper' => 'row',
                    'error' => '',
                    'hint' => '',
                ],
                'horizontalCheckboxTemplate' => "{input}{label}\n{error}\n{hint}"
            ],
        ]);
?>

<div class="master-gift-form col-md-12">
    <?= $form->field($model, 'gift_code', $fieldOptions1)->textInput(['disabled' => true]) ?>
    <?= $form->field($model, 'gift_date', $fieldOptions1)->textInput(['disabled' => true]) ?>
    <?= $form->field($model, 'list_product', $fieldOptions1)->textInput(['disabled' => true, 'value' => $list_product])->label(Yii::t('backend', 'Target')) ?>
    <?= $form->field($gift_receiver, '[customer_receiver]id')->hiddenInput()->label(false) ?>
    <div class="form-group row row-inline">
        <div class="col-md-3 no-padding"><label class="control-label" ><?= Yii::t('backend', 'People Send') ?></label></div>
        <div class="col-md-9 no-padding">
            <?= $form->field($gift_receiver, '[customer_receiver]first_name', $fieldOptions2)->textInput(['maxlength' => true]) ?>
           
            <?= $form->field($gift_receiver, '[customer_receiver]last_name', $fieldOptions2)->textInput(['maxlength' => true]) ?>
        </div>
    </div>
    <div class="form-group row row-inline ">
        <div class="col-md-3 no-padding"><label class="control-label" ><?= Yii::t('backend', 'People Send Kana') ?></label></div>
        <div class="col-md-9 no-padding">
            <?= $form->field($gift_receiver, '[customer_receiver]first_name_kana', $fieldOptions2)->textInput(['maxlength' => true]) ?>
           
            <?= $form->field($gift_receiver, '[customer_receiver]last_name_kana', $fieldOptions2)->textInput(['maxlength' => true]) ?>
        </div>
    </div>
    <?= $form->field($gift_receiver, '[customer_receiver]mobile', $fieldOptions1)->textInput(['maxlength' => true]) ?>
    <?=
    $form->field($gift_receiver, '[customer_receiver]post_code', $fieldOptions1)->textInput(['maxlength' => true])->widget(\yii\widgets\MaskedInput::className(), [
        'mask' => '999-9999',
        'clientOptions' => [
            'autoGroup' => true,
            'removeMaskOnSubmit' => true
        ]
    ])
    ?>
    <?= $form->field($gift_receiver, '[customer_receiver]address', $fieldOptions1)->textInput(['maxlength' => true])->label(Yii::t('backend', 'Address')) ?>


    <div id="list-receiver">
        <?php foreach ($list_add_address as $index => $address) { ?>
            <div class="add_people_receiver-<?= $index ?>">
                <div class="box-header with-border common-box-h4">
                    <h4 class="text-title"><b><?php
                            echo Yii::t('backend', 'Destination Gift');
                            echo $index + 1;
                            ?></b></h4>
                </div>
                <?= $form->field($address['list_customer'], '[list_customer][' . $index . ']id')->hiddenInput()->label(false)?>
                <?= $form->field($address['list_customer'], '[list_customer][' . $index . ']customer_id_new')->hiddenInput()->label(false)?>
                <div class="row row-inline">
                    <div class="col-md-3 no-padding"><label class="control-label" ><?= Yii::t('backend', 'Name'); ?></label></div>
                    <div class="col-md-9 no-padding">
                        <?= $form->field($address['list_customer'], '[list_customer][' . $index . ']first_name', $fieldOptions2)->textInput(['maxlength' => true]) ?>
                        <div class="col-md-2"></div>
                        <?= $form->field($address['list_customer'], '[list_customer][' . $index . ']last_name', $fieldOptions2)->textInput(['maxlength' => true]) ?>
                    </div>
                </div>


                <div class="row row-inline ">
                    <div class="col-md-3 no-padding"><label class="control-label" ><?= Yii::t('backend', 'Name kana'); ?>   </label></div>
                    <div class="col-md-9 no-padding">
                        <?= $form->field($address['list_customer'], '[list_customer][' . $index . ']first_name_kana', $fieldOptions2)->textInput(['maxlength' => true]) ?>
                        <div class="col-md-2"></div>
                        <?= $form->field($address['list_customer'], '[list_customer][' . $index . ']last_name_kana', $fieldOptions2)->textInput(['maxlength' => true]) ?>
                    </div>
                </div>
                <?=
                $form->field($address['list_customer'], '[list_customer][' . $index . ']post_code', [
                    'options' => ['class' => 'form-group row row-inline'],
                    'template' => "<div class='col-md-3 no-padding'>{label}</div><div class='col-md-6 no-padding post_gift_ct'>{input}\n{hint}\n{error}</div><div class='col-md-3'><a class='btn common-button-submit search-code' onClick='searchCode(" . $index . ");'> 住所検索</a></div>\n",
                ])->textInput(['maxlength' => true])->widget(\yii\widgets\MaskedInput::className(), [
                    'mask' => '999-9999',
                    'clientOptions' => [
                        'removeMaskOnSubmit' => true
                    ]
                ])
                ?>


                <?= $form->field($address['list_customer'], '[list_customer][' . $index . ']address', $fieldOptions1)->textInput(['maxlength' => true])->label(Yii::t('backend', 'Address')) ?>


                <?=
                $form->field($address['list_customer'], '[list_customer][' . $index . ']mobile', [
                    'options' => ['class' => 'row row-inline'],
                    'template' => "<div class='col-md-3 no-padding'>{label}</div><div class='col-md-6 no-padding post_gift_ct'>{input}\n{hint}\n{error}</div><div class='col-md-3'><a class='btn common-button-submit' onClick=searchCustomerId(" . $index . ")>" . Yii::t('backend', 'Destination Search') . "</a></div>\n",
                ])->textInput(['maxlength' => true])
                ?>


                <?= $form->field($address['list_receiver'], '[' . $index . ']id')->hiddenInput()->label(false) ?>

                <div class="row row-inline">
                    <table class="table-bordered col-md-8">
                        <thead>
                        <th class="col-md-6">
                            <?=
                            $form->field($address['list_receiver'], '[' . $index . ']product_id', [
                                'options' => ['class' => 'col-md-12 no-padding'],
                                'template' => "<label class='mws-form-label'>{label}</label>"])
                            ?>
                        </th>
                        <th class="col-md-6">
                            <?=
                            $form->field($address['list_receiver'], '[' . $index . ']quantity', [
                                'options' => ['class' => 'col-md-12 no-padding'],
                                'template' => "<label class='mws-form-label'>{label}</label>"])
                            ?>
                        </th>
                        </thead>
                        <tr>
                            <td> 
                                <?=
                                $form->field($address['list_receiver'], '[' . $index . ']product_id', [
                                    'template' => "<div class='col-md-12'>{input}</div>\n"
                                ])->dropDownList($product_gift, [
                                    'prompt' => Yii::t('backend', 'Please Select'),
                                    'event-key' => 'receiver-event'
                                    ])->label(false)
                                ?>
                            </td>
                            <td> 
                                <?=
                                $form->field($address['list_receiver'], '[' . $index . ']quantity', [
                                    'template' => "<div class='col-md-12'>{input}</div>\n"
                                ])->textInput([
                                    'maxlength' => true,
                                    'event-key' => 'receiver-event'
                                    ])->label(false)
                                ?>
                            </td>
                        </tr>
                    </table>
                </div>

                <div class="row row-inline">
                    <div class="col-md-8">
                        <div class="col-md-6">
                            <?=
                            $form->field($address['list_receiver'], '[' . $index . ']product_id', [
                                'options' => ['class' => 'col-md-12 no-padding'],
                                'template' => "<label class='mws-form-label'>{hint}\n{error}</label>"])
                            ?>
                        </div>
                        <div class="col-md-6">
                            <?=
                            $form->field($address['list_receiver'], '[' . $index . ']quantity', [
                                'options' => ['class' => 'col-md-12 no-padding'],
                                'template' => "<label class='mws-form-label'>{hint}\n{error}</label>"])
                            ?>
                        </div>
                    </div>
                </div>    
            </div>

        <?php } ?>
    </div>

    <div class="form-group col-md-12 no-padding">
        <?=
        Html::submitInput(Yii::t('backend', 'Add Destination'), [
            'class' => 'btn common-button-submit',
            'id' => 'btn-confirm',
            'name' => 'btn-validate'
        ])
        ?>
    </div>
    

    <div class="form-group col-md-12 no-padding">
        <?= Html::a(Yii::t('backend', 'Return'), ['index'], ['class' => 'btn btn-default common-button-default']) ?>
        <button href="javascript:void(0);" class="btn btn-default common-button-submit" id="save-receiver" ><?php echo Yii::t('backend', 'Save') ?></button>
        <a href="#" class="btn btn-default common-button-submit"><?php echo Yii::t('backend', 'Download CSV') ?></a>
    </div>
</div>
<?php ActiveForm::end(); ?>
<?php Pjax::end() ?>
<?php
Modal::begin([
    'id' => 'userModal',
    'size' => 'SIZE_LARGE',
    'header' => '<b>' . Yii::t('backend', 'Destination selection') . '</b>',
    'footer' => Html::button(Yii::t('backend', 'Close'), ['class' => 'btn common-button-default', 'data-dismiss' => "modal", 'id' => 'btn-close'])
    . Html::submitButton(Yii::t('backend', 'Destination selection'), ['class' => 'btn common-button-submit', 'onClick' => 'ChooseCustomer()']),
    'footerOptions' => ['class' => 'modal-footer text-center'],
]);
?>
<?= Html::hiddenInput('row-click', '', ['id' => 'row-click']); ?>
<div id="modalCustomerGridview">

</div>

<?php
Modal::end();
?>


<script>
    function searchCode(row) {
        getPostcode('#mastercustomer-list_customer-' + row + '-post_code', '#mastercustomer-list_customer-' + row + '-address');
    }

    function ChooseCustomer() {
        $('#userModal').modal('hide');
        var $radio_check = $('input[name*="MasterCustomer[select_customer][id]"]:checked');
        var key = $radio_check.attr('key');
        $('#mastercustomer-list_customer-' + $('#row-click').val() + '-customer_id_new').val($radio_check.val());
        $('#mastercustomer-list_customer-' + $('#row-click').val() + '-first_name').val($('#first_name_'+ key).val());
        $('#mastercustomer-list_customer-' + $('#row-click').val() + '-last_name').val($('#last_name_'+ key).val());
        $('#mastercustomer-list_customer-' + $('#row-click').val() + '-first_name_kana').val($('#first_name_kana_'+ key).val());
        $('#mastercustomer-list_customer-' + $('#row-click').val() + '-last_name_kana').val($('#last_name_kana_'+ key).val());
        $('#mastercustomer-list_customer-' + $('#row-click').val() + '-mobile').val($('#mobile_'+ key).val());
        $('#mastercustomer-list_customer-' + $('#row-click').val() + '-post_code').val($('#post_code_'+ key).val());
        $('#mastercustomer-list_customer-' + $('#row-click').val() + '-address').val($('#address_'+ key).val());

    }

    function searchCustomerId(row) {
        $('#row-click').val(row);
        $('#modalCustomerGridview').html("");
        $.ajax({
            type: 'POST',
            cache: false,
            data: 'first_name=' + $('#mastercustomer-list_customer-' + row + '-first_name').val() + '&last_name=' + $('#mastercustomer-list_customer-' + row + '-last_name').val() + '&post_code=' + +$('#mastercustomer-list_customer-' + row + '-post_code').val().replace(/[^0-9\.]+/g, "") + '&address=' +
                    $('#mastercustomer-list_customer-' + row + '-address').val() + '&mobile=' + $('#mastercustomer-list_customer-' + row + '-mobile').val() + '&first_name_kana=' + $('#mastercustomer-list_customer-' + row + '-first_name_kana').val() + '&last_name_kana=' + $('#mastercustomer-list_customer-' + row + '-last_name_kana').val(),
            url: 'search-customer',
            success: function (response) {
                $('#modalCustomerGridview').html(response);
                $('#userModal').modal('show');
            }
        });
    }
    
    $(document).on('change','*[event-key="receiver-event"]', function(){
        var parent = $(this).closest('tr');
        if($(parent).is(":last-child")){
            var name = $(parent).find('select').attr('name');
//            var index = parseInt(name.replace(/^MasterGiftReceiver\[([0-9]+)\]\[([0-9]+)\]\[product_id\]$/gi, '$2'));
//            var html = $(parent).html()
//                    .replace(/MasterGiftReceiver\[([0-9]+)\]\[([0-9]+)\]\[id\]/gi, 'MasterGiftReceiver[$1]['+(index + 1)+'][id]')
//                    .replace(/MasterGiftReceiver\[([0-9]+)\]\[([0-9]+)\]\[product_id\]/gi, 'MasterGiftReceiver[$1]['+(index + 1)+'][product_id]')
//                    .replace(/MasterGiftReceiver\[([0-9]+)\]\[([0-9]+)\]\[quantity\]/gi, 'MasterGiftReceiver[$1]['+(index + 1)+'][quantity]')
//                    .replace(/has-error/gi, '');

            var index = parseInt(name.replace(/^MasterGiftReceiver\[([0-9]+)\]\[product_id\]$/gi, '$2'));
            var html = $(parent).html()
                    .replace(/MasterGiftReceiver\[([0-9]+)\]\[id\]/gi, 'MasterGiftReceiver[$1]['+(index + 1)+'][id]')
                    .replace(/MasterGiftReceiver\[([0-9]+)\]\[product_id\]/gi, 'MasterGiftReceiver[$1]['+(index + 1)+'][product_id]')
                    .replace(/MasterGiftReceiver\[([0-9]+)\]\[quantity\]/gi, 'MasterGiftReceiver[$1]['+(index + 1)+'][quantity]')
                    .replace(/has-error/gi, '');
            
            var tr = document.createElement('tr');
            $(tr).append(html);
            $(tr).find('select').val('');
            $(tr).find('input').val('');
            $(tr).find('.help-block-error').text('');

            $(parent).parent().append(tr);
        }
    });
</script>


