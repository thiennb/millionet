<?php

use yii\bootstrap\ActiveForm;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$fieldOptions1 = [
    'options' => ['class' => 'form-group row row-inline'],
    'template' => "<div class='col-md-3 no-padding'>{label}</div><div class='col-md-8 no-padding'>{input}\n{hint}\n{error}</div>\n"
];

$fieldOptions2 = [
    'options' => ['class' => 'col-md-6'],
    'template' => "<div class='col-md-3'>{label}</div><div class='col-md-9'>{input}\n{hint}\n{error}</div>\n"
];
?>

<?php
$form = ActiveForm::begin([
            'options' => [
                'id' => 'receiver-form',
            ],
            'enableClientValidation' => false,
        ]);
?>


<?php
ActiveForm::end();
?>

