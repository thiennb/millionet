<style>
    @media print {
        table {
            width: 100%;
            border-spacing: 0;
            border-collapse: collapse;
            margin-bottom: 10px;
        }
        table, tr, th, td{
            border: 1px solid #ddd;
            border-width: 1px;
            padding: 5px;
        }
        
        table table{
            margin: 0;
            border: 0;
        }
        table table td , table table th{
            border-bottom: 0;
            border-top: 0;
        }
        table table tr td:first-child , table table tr th:first-child{
            border-left: 0;
        }
        table table tr td:last-child , table table tr th:last-child{
            border-right: 0;
        }
        table table tr {
            border-left: 0;
            border-right: 0;
        }
        table table tr:last-child {
            border-bottom: 0;
        }
        table table tr:first-child {
            border-top: 0;
        }
        
        th{
            background-color: #ccc;
        }
        .text-center{
            text-align: center;
        }
        
        .text-left{
            text-align: left;
        }
        
        .text-right{
            text-align: right;
        }
        
        h4{
            margin: 5px;
            padding: 0;
            font-weight: bold;
            font-size: 16px;
        }
    } 
</style>

<h4><b><?= Yii::t('backend', 'People Send') ?></b></h4>
<table>
    <tr>
        <th class="text-left" width="30%"><?php echo Yii::t('backend', 'Register Date') ?></th>
        <td class="text-left"><?php echo $gift->gift_date ?></td>
    </tr>
    <tr>
        <th class="text-left"><?php echo Yii::t('backend', 'Member Specified Label') ?></th>
        <td class="text-left"><?php echo $giftSender->customer_jan_code ?></td>
    </tr>
    <tr>
        <th class="text-left"><?php echo Yii::t('backend', 'People Send') ?></th>
        <td class="text-left"><?php echo $giftSender->first_name ?> <?php echo $giftSender->last_name ?></td>
    </tr>
    <tr>
        <th class="text-left"><?php echo Yii::t('backend', 'Phone') ?></th>
        <td class="text-left"><?php echo $giftSender->mobile ?></td>
    </tr>
    <tr>
        <th class="text-left"><?php echo Yii::t('backend', 'People Send Post') ?></th>
        <td class="text-left"><?php echo $giftSender->post_code ?></td>
    </tr>
    <tr>
        <th class="text-left"><?php echo Yii::t('backend', 'Address') ?></th>
        <td class="text-left"><?php echo $giftSender->address ?><br/><?php echo $giftSender->address2 ?></td>
    </tr>
</table>
<!-- End HTML for 差出人情報 -->
                    
<!-- Begin HTML for 差出人情報 -->
<h4><b><?= Yii::t('backend', 'Destination') ?></b></h4>
<?php
$count = 0;
$total = [];
foreach ($receiverCustomers as $key => $receiverCustomer):
$count++;
?>
<table>
    <tr>
        <th class="text-left" width="30%">宛名<?php echo $count ?></th>
        <td class="text-left"><?php echo $receiverCustomer['customer']->first_name ?> <?php echo $receiverCustomer['customer']->last_name ?></td>
    </tr>
    <tr>
        <th class="text-left">宛名カナ<?php echo $count ?></th>
        <td class="text-left"><?php echo $receiverCustomer['customer']->first_name_kana ?> <?php echo $receiverCustomer['customer']->last_name_kana ?></td>
    </tr>
    <tr>
        <th class="text-left">電話番号<?php echo $count ?></th>
        <td class="text-left"><?php echo $receiverCustomer['customer']->mobile ?></td>
    </tr>
    <tr>
        <th class="text-left">郵便番号<?php echo $count ?></th>
        <td class="text-left"><?php echo $receiverCustomer['customer']->post_code ?></td>
    </tr>
    <tr>
        <th class="text-left">住所<?php echo $count ?></th>
        <td class="text-left"><?php echo $receiverCustomer['customer']->address ?> <?php echo $receiverCustomer['customer']->address2 ?></td>
    </tr>
    <tr>
        <th class="text-left">商品<?php echo $count ?></th>
        <td class="text-left" style="padding: 0">
            <table>
                <tr>
                    <th width="60%">商品名</th>
                    <th width="20%">数量</th>
                    <th width="20%">金額</th>
                </tr>
                <?php foreach($receiverCustomer['products'] as $proKey => $giftReceiver): ?>
                    <?php 
                    if(!isset($total[$giftReceiver->product->id])){
                        $total[$giftReceiver->product->id] = [
                            'name' => $giftReceiver->product->name,
                            'quantity' => $giftReceiver->quantity,
                            'price' => $giftReceiver->product->unit_price
                        ];
                    } else {
                        $total[$giftReceiver->product->id]['quantity'] = $total[$giftReceiver->product->id]['quantity'] + $giftReceiver->quantity;
                    }
                    ?>
                <tr>
                    <td class="text-left"><?php echo $giftReceiver->product->name ?></td>
                    <td class="text-right"><?php echo $giftReceiver->quantity ?></td>
                    <td class="text-right"><?php echo number_format($giftReceiver->quantity * $giftReceiver->product->unit_price) ?></td>
                </tr>
                <?php endforeach; ?>
            </table>
        </td>
    </tr>
</table>
<?php endforeach; ?>
<!-- End HTML for 差出人情報 -->

<h4><b>合計</b></h4>
<table>
    <tr>
        <th class="text-center" width="72%">商品名</th>
        <th class="text-center" width="14%">数量</th>
        <th class="text-center">金額</th>
    </tr>
    <?php
    $ttQuantity = 0;
    $ttPrice = 0;
    foreach ($total as $value): ?>
    <tr>
        <td class="text-left" width="72%"><?php echo $value['name'] ?></td>
        <td class="text-right" width="14%"><?php $ttQuantity += $value['quantity'];  echo $value['quantity']; ?></td>
        <td class="text-right"><?php $price = $value['quantity']*$value['price']; echo number_format($value['quantity']*$value['price']); $ttPrice += $price; ?></td>
    </tr>
    <?php endforeach; ?>
    <tr>
        <th class="text-center">合計</th>
        <th class="text-right"><?php echo $ttQuantity; ?></th>
        <th class="text-right"><?php echo number_format($ttPrice); ?></th>
    </tr>
</table>