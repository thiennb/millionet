<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\MasterGift */

$this->title = Yii::t('backend', 'Create Master Gift');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Master Gifts'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row text-setting company">
    <div class="col-lg-12 col-md-12">
        <div class="box box-info box-solid add">
            <div class="box-header with-border">
                <h4 class="text-title"><b><?= $this->title ?></b></h4>
            </div>
            <div class="box-body content">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="box-header with-border common-box-h4 col-md-6 ">
                                <h4 class="text-title"><b><?= Yii::t('backend', 'Gift Infomation') ?></b></h4>
                            </div>
                            <div class="col-md-12">
                             <?= $this->render('_form', [
                                 'model' => $model,
                                 'gift_receiver'=>$gift_receiver,
                                 'add_address1'=>$add_address1,
                                 'mastergiftreceiver'=>$mastergiftreceiver,
                             ]) ?>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
</div>