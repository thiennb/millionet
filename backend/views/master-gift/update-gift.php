<?php

use yii\helpers\Html;
use yii\bootstrap\Modal;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $gift common\models\MasterGift */

$this->title = Yii::t('backend', 'Destination gift voucher');
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Master Gifts'), 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('backend', 'Update');

$normalOption = [
    'options' => ['class' => 'form-group'],
    'template' => '<div class="col-md-3">{label}</div><div class="col-md-4">{input}</div>' //{hint}{error}
];

$smallOption = [
    'options' => [],
    'template' => '<div class="col-md-2">{label}</div><div class="col-md-4">{input}</div>' //{hint}{error}
];

$longOption = [
    'options' => ['class' => 'form-group'],
    'template' => '<div class="col-md-3">{label}</div><div class="col-md-8">{input}</div>' //{hint}{error}
];

$noLabelOption = [
    'options' => ['class' => 'row'],
    'template' => '<div class="col-md-12">{input}</div>' //{hint}{error}
];

$hiddenOption = [
    'options' => ['class' => ''],
    'template' => '{input}' //{hint}{error}
];
//$list_product = implode(", ", $product_gift);
?>
<style>
    .custom-error, .custom-error-border{
        color: red;
        padding: 5px;
        margin: 5px 0;
        display: none;
    }
    
    .custom-error-border{
        color: red;
        background: #fdf7f7;
        border-left: 3px solid red; /*#eed3d7;*/
        padding: 10px 20px;
        margin: 0 0 15px 0;
    }
    
    .error-input{
        background: #fdf7f7
    }
    
    .custom-error.active, .custom-error-border.active{
        display: block;
    }
    
    .gift-receiver-content table > tbody > tr input{
        height: 30px;
    }
    
    .help-block{
        margin: 0;
    }
</style>
<div class="row text-setting company">
    <div class="col-lg-12 col-md-12">
        <div class="box box-info box-solid add">
            <div class="box-header with-border">
                <h4 class="text-title"><b><?= $this->title ?></b></h4>
            </div>
            <div class="box-body content">
                <?php
                $form = ActiveForm::begin([
                            'layout' => 'horizontal',
                            'enableClientValidation' => false,
                            'options' => ['data-pjax' => true],
                            'fieldConfig' => [
                               // 'template' => "{beginWrapper}<div class='col-md-3'>{label}</div><div class='col-md-'>{input}\n{hint}\n{error}</div>\{endWrapper}",
                                'horizontalCssClasses' => [
                                    'label' => '',
                                    'wrapper' => 'row',
                                    'error' => '',
                                    'hint' => '',
                                ],
                                'horizontalCheckboxTemplate' => "{input}{label}\n{error}\n{hint}"
                            ],
                        ]);
                ?>
                <div class="col-md-12">
                    <!-- Begin HTML for 差出人情報 -->
                    <div class="row">
                        <div class="col-md-8">
                            <div class="box-header with-border common-box-h4 col-md-4 ">
                                <h4 class="text-title"><b><?= Yii::t('backend', 'Gift Infomation') ?></b></h4>
                            </div>
                            <div class="col-md-12">
                                <div class="error-sender custom-error-border"></div>
                            </div>
                            
                            <div class="col-md-12">
                                <?= $form->field($gift, 'gift_code', $normalOption)->textInput(['disabled' => true])->label(Yii::t('backend', 'Member Specified Label')) ?>
                                <?= $form->field($gift, 'gift_date', $normalOption)->textInput(['disabled' => true])->label(Yii::t('backend', 'Register Date')) ?>
                                
                                <div class="row form-group">
                                    <div class="col-md-3">
                                        <label class="control-label" ><?= Yii::t('backend', 'People Send') ?></label>
                                    </div>
                                    <div class="col-md-7">
                                        <div class="row">
                                            <?= $form->field($giftSender, '[customer_sender]first_name', $smallOption)->textInput(['disabled' => true])->label(Yii::t('backend', 'First Name')) ?>
                                            <?= $form->field($giftSender, '[customer_sender]last_name', $smallOption)->textInput(['disabled' => true])->label(Yii::t('backend', 'Last name')) ?>
                                        </div>
                                    </div>
                                </div>
                                                                
                                <?= $form->field($giftSender, '[customer_sender]mobile', $normalOption)->textInput(['disabled' => true])->label(Yii::t('backend', 'Phone')) ?>

                                <?= $form->field($giftSender, '[customer_sender]post_code', $normalOption)->textInput(['disabled' => true])->label(Yii::t('backend', 'People Send Post')) ?>

                                <?= $form->field($giftSender, '[customer_sender]address', $longOption)->textInput(['disabled' => true])->label(Yii::t('backend', 'Address1')) ?>

                                <?= $form->field($giftSender, '[customer_sender]address2', $longOption)->textInput(['disabled' => true])->label(Yii::t('backend', 'Address2')) ?>
                                
                                <div class="row form-group">
                                    <div class="col-md-3">
                                        <input type="button" class="btn common-button-submit btn-search-sender" value="<?php echo Yii::t('backend', 'Customer search') ?>">
                                    </div>
                                </div>
                                <?= $form->field($giftSender, '[customer_sender]id', $hiddenOption)->hiddenInput()->label(false) ?>
                            </div>
                        </div>
                    </div>
                    <!-- End HTML for 差出人情報 -->
                    
                    <!-- Begin HTML for 宛先入力 -->
                    <div class="gift-receiver-content">
                    <?php
                    // Foreach receiver
                    $maxKey = 0;
                    $count = 0;
                    foreach ($receiverCustomers as $key => $receiverCustomer):
                        $count++;
                        if($key > $maxKey){$maxKey = $key;} ?>
                    <div class="row">
                        <div class="col-md-8">
                            <div class="box-header with-border common-box-h4 col-md-4 ">
                                <h4 class="text-title"><b><?= Yii::t('backend', 'Destination Gift') ?><?php echo $count ?></b></h4>
                            </div>
                            <div class="col-md-12">
                                <div class="error-receiver-<?php echo $key ?>  custom-error-border"></div>
                                <div class="row form-group">
                                    <div class="col-md-3 required">
                                        <label class="control-label"><?php echo Yii::t('backend', 'Member Specified Label') ?></label>
                                    </div>
                                    <div class="col-md-3">
                                        <span class="customer-code-<?php echo $key ?>"><?php echo $receiverCustomer['customer']->customer_jan_code ?></span>
                                    </div>
                                    <div class="col-md-3">
                                        <input type="submit" class="btn common-button-submit" name="btn-validate" value="顧客検索">
                                    </div>
                                </div>
                                
                                <div class="row form-group">
                                    <div class="col-md-3 required">
                                        <label class="control-label" ><?= Yii::t('backend', 'Name') ?></label>
                                    </div>
                                    <div class="col-md-7">
                                        <div class="row">
                                            <?= $form->field($receiverCustomer['customer'], '[customer_receiver][' . $key . ']first_name', $smallOption)->textInput(['disabled' => true])->label(Yii::t('backend', 'First Name')) ?>
                                            <?= $form->field($receiverCustomer['customer'], '[customer_receiver][' . $key . ']last_name', $smallOption)->textInput(['disabled' => true])->label(Yii::t('backend', 'Last Name')) ?>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="row form-group">
                                    <div class="col-md-3 required">
                                        <label class="control-label" ><?= Yii::t('backend', 'Name kana') ?></label>
                                    </div>
                                    <div class="col-md-7">
                                        <div class="row">
                                            <?= $form->field($receiverCustomer['customer'], '[customer_receiver][' . $key . ']first_name_kana', $smallOption)->textInput(['disabled' => true])->label(Yii::t('backend', 'First Name Kana')) ?>
                                            <?= $form->field($receiverCustomer['customer'], '[customer_receiver][' . $key . ']last_name_kana', $smallOption)->textInput(['disabled' => true])->label(Yii::t('backend', 'Last Name Kana')) ?>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="row form-group">
                                    <div class="col-md-3 required">
                                        <label class="control-label"><?php echo Yii::t('backend', 'People Send Post') ?></label>
                                    </div>
                                    <div class="col-md-3">
                                        <?= $form->field($receiverCustomer['customer'], '[customer_receiver]['.$key.']post_code', $noLabelOption)->textInput(['disabled' => true])->label(false) ?>
                                    </div>
                                    <div class="col-md-3">
                                        <input type="submit" class="btn common-button-submit" name="btn-validate" value="123213">
                                    </div>
                                </div>
                                
                                <?= $form->field($receiverCustomer['customer'], '[customer_receiver][' . $key . ']address', $longOption)->textInput(['disabled' => true])->label(Yii::t('backend', 'Address1')) ?>

                                <?= $form->field($receiverCustomer['customer'], '[customer_receiver][' . $key . ']address2', $longOption)->textInput(['disabled' => true])->label(Yii::t('backend', 'Address2')) ?>

                                <?= $form->field($receiverCustomer['customer'], '[customer_receiver][' . $key . ']mobile', $normalOption)->textInput(['disabled' => true])->label(Yii::t('backend', 'Phone')) ?>
                                <div class="row form-group">
                                    <div class="col-md-3">
                                        <input type="button" class="btn common-button-submit btn-search-customer" data-key="<?php echo $key ?>" value="<?php echo Yii::t('backend', 'Customer search') ?>">
                                    </div>
                                </div>
                                <?= $form->field($receiverCustomer['customer'], '[customer_receiver][' . $key . ']id', $hiddenOption)->hiddenInput()->label(false) ?>
                                <!-- Begin HTML for received product -->
                                <div class="row form-group">
                                    <div class="col-md-3 required">
                                        <label class="control-label"><?=Yii::t('backend', 'Shipped products'); ?></label>
                                    </div>
                                    <div class="col-md-8">
                                        <table class="table table-bordered">
                                            <thead>
                                                <tr>
                                                    <th><label class="control-label">No</label></th>
                                                    <th><label class="control-label"><?=Yii::t('backend', 'Product name'); ?></label></th>
                                                    <th><label class="control-label"><?=Yii::t('backend', 'Quantity'); ?></label></th>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            <?php foreach($receiverCustomer['products'] as $proKey => $giftReceiver): ?>
                                            <tr data-key="<?php echo $key ?>" data-count="<?php echo $proKey ?>">
                                                <td class="product-no">
                                                    <span><?php echo $proKey+1 ?></span>
                                                    <?= $form->field($giftReceiver, '['.$key.']['.$proKey.']id', $hiddenOption)->hiddenInput()->label(false) ?>
                                                </td>
                                                <td class="product-name">
                                                    <span><?php echo $giftReceiver->product->name ?></span>
                                                    <?= $form->field($giftReceiver, '['.$key.']['.$proKey.']product_jan_code', $hiddenOption)->hiddenInput()->label(false) ?>
                                                </td>
                                                <td class="product-quantity">
                                                    <?= $form->field($giftReceiver, '['.$key.']['.$proKey.']quantity', $hiddenOption)->textInput(['maxlength' => true])->label(false) ?>
                                                    <div class="error-product-<?php echo $key . '-' . $proKey ?>  custom-error"></div>
                                                </td>
                                                <td class="product-edit">
                                                    <button type="button" class="btn common-button-submit btn-delete-product"><?php  echo \Yii::t('backend', 'Delete') ?></button>
                                                </td>
                                            </tr>
                                            <?php endforeach; ?>
                                            </tbody>
                                        </table>
                                        <a href="javascript: void(0)" class="btn-add-product" data-key="<?php echo $key ?>"><b>+　商品を選択する</b></a>
                                    </div>
                                </div>
                                <!-- End HTML for received product -->
                                
                                
                            </div>
                        </div>
                    </div>
                    <?php endforeach; ?>
                    </div>
                    
                    <!-- End HTML for 宛先入力1 -->
                    <div class="row form-group">
                        <div class="col-md-3">
                            <input type="button" class="btn common-button-submit btn-add-receiver" value="<?php echo Yii::t('backend', 'Add a destination') ?>">
                        </div>
                    </div>
                    
                    <div class="row form-group">
                        <div class="col-md-12">
                            <?= Html::a(Yii::t('backend', 'Return'), ['index'], ['class' => 'btn btn-default common-button-default']) ?>
                            <button type="submit" class="btn btn-default common-button-submit" id="save-receiver"><?php echo Yii::t('backend', 'Confirm') ?></button>
                        </div>
                    </div>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>

<!--Begin html for add product modal-->
    <?php
    Modal::begin([
        'id' => 'productAddingModal',
        'size' => 'SIZE_LARGE',
        'header' => '<b>' . Yii::t('backend', 'Select product') . '</b>',
        'footer' => Html::button(Yii::t('backend', 'Close'), ['class' => 'btn common-button-default', 'data-dismiss' => "modal", 'id' => 'btn-close'])
        . Html::submitButton(Yii::t('backend', 'Destination selection'), ['class' => 'btn common-button-submit btn-finish-add-product']),
        'footerOptions' => ['class' => 'modal-footer text-center'],
    ]);
    ?>
    <?= Html::hiddenInput('row-click', '', ['id' => 'row-click']); ?>
    <div class="content">

    </div>
    <?php
    Modal::end();
    ?>
<!--End html for add product modal-->

<!--Begin HTML for add customer -->
<?php
Modal::begin([
    'id' => 'modalCustomerGridview',
    'size' => 'SIZE_LARGE',
    'header' => '<b>' . Yii::t('backend', 'Destination selection') . '</b>',
    'footer' => Html::button(Yii::t('backend', 'Close'), ['class' => 'btn common-button-default', 'data-dismiss' => "modal", 'id' => 'btn-close'])
    . Html::submitButton(Yii::t('backend', 'Destination selection'), ['class' => 'btn common-button-submit', 'id' => 'choose-customer']),
    'footerOptions' => ['class' => 'modal-footer text-center'],
]);
?>
<div class="content">

</div>

<?php
Modal::end();
?>
<!--End HTML for add customer -->

<!--Begin HTML for add customer -->
<?php
Modal::begin([
    'id' => 'modalSenderGridview',
    'size' => 'SIZE_LARGE',
    'header' => '<b>' . Yii::t('backend', 'Destination selection') . '</b>',
    'footer' => Html::button(Yii::t('backend', 'Close'), ['class' => 'btn common-button-default', 'data-dismiss' => "modal"])
    . Html::submitButton(Yii::t('backend', 'Destination selection'), ['class' => 'btn common-button-submit', 'id' => 'choose-sender']),
    'footerOptions' => ['class' => 'modal-footer text-center'],
]);
?>
<div class="content">

</div>

<?php
Modal::end();
?>
<!--End HTML for add customer -->

<script id="receiver-template" type="text/x-handlebars-template">
    <div class="row">
        <div class="col-md-8">
            <div class="box-header with-border common-box-h4 col-md-4 ">
                <h4 class="text-title"><b><?= Yii::t('backend', 'Destination Gift') ?>{key}</b></h4>
            </div>
            <div class="col-md-12">
                <div class="error-receiver-{key}  custom-error-border"></div>
                <div class="row form-group">
                    <div class="col-md-3 required">
                        <label class="control-label"><?php echo Yii::t('backend', 'Member Specified Label') ?></label>
                    </div>
                    <div class="col-md-3">
                        <span class="customer-code-{key}"></span>
                    </div>
                    <div class="col-md-3">
                        <input type="submit" class="btn common-button-submit" name="btn-validate" value="顧客検索">
                    </div>
                </div>
                                
                <div class="row form-group">
                    <div class="col-md-3 required">
                        <label class="control-label"><?= Yii::t('backend', 'Name') ?></label>
                    </div>
                    <div class="col-md-7">
                        <div class="row">
                            <div class="field-mastercustomer-customer_receiver-{key}-first_name required">
                                <div class="col-md-2">
                                    <label class="control-label " for="mastercustomer-customer_receiver-{key}-first_name"><?php echo Yii::t('backend', 'First Name') ?></label>
                                </div>
                                <div class="col-md-4">
                                    <input type="text" id="mastercustomer-customer_receiver-{key}-first_name" class="form-control" name="MasterCustomer[customer_receiver][{key}][first_name]" disabled>
                                    <div class="help-block help-block-error "></div>
                                        
                                </div>
                            </div>                                            
                            <div class="field-mastercustomer-customer_receiver-{key}-last_name required">
                                <div class="col-md-2">
                                    <label class="control-label " for="mastercustomer-customer_receiver-{key}-last_name"><?php echo Yii::t('backend', 'Last Name') ?></label>
                                </div>
                                <div class="col-md-4">
                                    <input type="text" id="mastercustomer-customer_receiver-{key}-last_name" class="form-control" name="MasterCustomer[customer_receiver][{key}][last_name]" disabled>
                                    <div class="help-block help-block-error "></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                                
                <div class="row form-group">
                    <div class="col-md-3 required">
                        <label class="control-label"><?= Yii::t('backend', 'Name kana') ?></label>
                    </div>
                    <div class="col-md-7">
                        <div class="row">
                            <div class="field-mastercustomer-customer_receiver-{key}-first_name_kana required">
                                <div class="col-md-2">
                                    <label class="control-label " for="mastercustomer-customer_receiver-{key}-first_name_kana"><?php echo Yii::t('backend', 'First Name Kana') ?></label>
                                </div>
                                <div class="col-md-4">
                                    <input type="text" id="mastercustomer-customer_receiver-{key}-first_name_kana" class="form-control" name="MasterCustomer[customer_receiver][{key}][first_name_kana]" disabled>
                                    <div class="help-block help-block-error "></div>
                                        
                                </div>
                            </div>                                            
                            <div class="field-mastercustomer-customer_receiver-{key}-last_name_kana required">
                                <div class="col-md-2">
                                    <label class="control-label " for="mastercustomer-customer_receiver-{key}-last_name_kana"><?php echo Yii::t('backend', 'Last Name Kana') ?></label>
                                </div>
                                <div class="col-md-4">
                                    <input type="text" id="mastercustomer-customer_receiver-{key}-last_name_kana" class="form-control" name="MasterCustomer[customer_receiver][{key}][last_name_kana]" disabled>
                                    <div class="help-block help-block-error "></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                                
                <div class="row form-group">
                    <div class="col-md-3 required">
                        <label class="control-label"><?= Yii::t('backend', 'People Send Post') ?></label>
                    </div>
                    <div class="col-md-3">
                        <div class="row field-mastercustomer-customer_receiver-{key}-post_code">
                            <div class="col-md-12">
                                <input type="text" id="mastercustomer-customer_receiver-{key}-post_code" class="form-control" name="MasterCustomer[customer_receiver][{key}][post_code]" disabled>
                                <div class="help-block help-block-error "></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <input type="submit" class="btn common-button-submit" name="btn-validate" value="123213">
                    </div>
                </div>
                                
                <div class="row form-group field-mastercustomer-customer_receiver-{key}-address">
                    <div class="col-md-3">
                        <label class="control-label " for="mastercustomer-customer_receiver-{key}-address"><?php echo Yii::t('backend', 'Address1') ?></label>
                    </div>
                    <div class="col-md-8">
                        <input type="text" id="mastercustomer-customer_receiver-{key}-address" class="form-control" name="MasterCustomer[customer_receiver][{key}][address]" disabled>
                        <div class="help-block help-block-error "></div>
                    </div>
                </div>
                                
                <div class="row form-group field-mastercustomer-customer_receiver-{key}-address2">
                    <div class="col-md-3">
                        <label class="control-label " for="mastercustomer-customer_receiver-{key}-address2"><?php echo Yii::t('backend', 'Address2') ?></label>
                    </div>
                    <div class="col-md-8">
                        <input type="text" id="mastercustomer-customer_receiver-{key}-address2" class="form-control" name="MasterCustomer[customer_receiver][{key}][address2]" disabled>
                        <div class="help-block help-block-error "></div>
                    </div>
                </div>
                
                <div class="row form-group field-mastercustomer-customer_receiver-{key}-mobile">
                    <div class="col-md-3">
                        <label class="control-label " for="mastercustomer-customer_receiver-{key}-mobile"><?php echo Yii::t('backend', 'Mobile') ?></label>
                    </div>
                    <div class="col-md-4">
                        <input type="text" id="mastercustomer-customer_receiver-{key}-mobile" class="form-control" name="MasterCustomer[customer_receiver][{key}][mobile]" disabled>
                        <div class="help-block help-block-error "></div>
                    </div>
                </div>  
                <div class="row form-group">
                    <div class="col-md-3">
                        <input type="button" class="btn common-button-submit btn-search-customer" data-key="{key}" value="<?php echo Yii::t('backend', 'Customer search') ?>">
                    </div>
                </div>
                
                <div class="form-group field-mastercustomer-customer_receiver-{key}-id">
                    <div class="row col-sm-offset-3">
                        <input type="hidden" id="mastercustomer-customer_receiver-{key}-id" name="MasterCustomer[customer_receiver][{key}][id]">
                        <div class="help-block help-block-error "></div>
                    </div>
                </div>
                <!-- Begin HTML for received product -->
                <div class="row form-group">
                    <div class="col-md-3 required">
                        <label class="control-label"><?=Yii::t('backend', 'Shipped products'); ?></label>
                    </div>
                    <div class="col-md-8">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th><label class="control-label">No</label></th>
                                    <th><label class="control-label"><?=Yii::t('backend', 'Product name'); ?></label></th>
                                    <th><label class="control-label"><?=Yii::t('backend', 'Quantity'); ?></label></th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                                            
                            </tbody>
                        </table>
                        <a href="javascript: void(0)" class="btn-add-product" data-key="{key}"><b>+　商品を選択する</b></a>
                    </div>
                </div>
                <!-- End HTML for received product -->
            </div>
        </div>
    </div>
</script>
<!-- Begin HTML for received product -->
<script id="product-template" type="text/x-handlebars-template">
    <td class="product-no">
        <span></span>
        <div class=" field-mastergiftreceiver-{key}-{count}-id">
            <input type="hidden" id="mastergiftreceiver-{key}-{count}-id" class="form-control" name="MasterGiftReceiver[{key}][{count}][id]"/>
        </div>
    </td>
    <td class="product-name">
        <span></span>
        <div class="field-mastergiftreceiver-{key}-{count}-product_jan_code">
            <input type="hidden" id="mastergiftreceiver-{key}-{count}-product_jan_code" name="MasterGiftReceiver[{key}][{count}][product_jan_code]"/>
        </div>
    </td>
    <td class="product-quantity">
        <div class=" field-mastergiftreceiver-{key}-{count}-quantity required">
            <input type="text" id="mastergiftreceiver-{key}-{count}-quantity" class="form-control" name="MasterGiftReceiver[{key}][{count}][quantity]">
        </div>                                                    
        <div class="error-product-{key}-{count} custom-error"></div>
    </td>
    <td class="product-edit">
        <button type="button" class="btn common-button-submit btn-delete-product"><?php  echo \Yii::t('backend', 'Delete') ?></button>
    </td>
</script>
<!-- End HTML for received product -->
<script>
$(document).ready(function(){
    var maxCount = <?php echo $maxKey ?>;
    /**
     * Add product event
     * @param {type} param1
     * @param {type} param2
     * @param {type} param3
     */
    var btnAddClick = null;
    $(document).on('click', '.btn-add-product', function(){
        btnAddClick = this;
        $('#productAddingModal').modal('show');
        $.ajax({
            type: 'POST',
            data:{},
            url: SITE_ROOT + '/master-gift/ajax-product-gift',
            success: function(response){
                $('#productAddingModal .content').html(response);
            },
            error: function(){
                
            }
        });
    });
        
    /**
     * Event when click on save product button
     */
    $(document).on('click', '.btn-finish-add-product', function(){
        $('#productAddingModal').modal('hide');
        var tbody = $(btnAddClick).parent().find('table tbody');
        
        var html = '';
        var count = 0;
        var key = $(btnAddClick).attr('data-key');
        
        if($(tbody).children('tr').length != 0){
            count = parseInt($(tbody).find('tr:last-child').attr('data-count'));
            count++;
        }
        
        html = $('#product-template').html();
        html = html.replace(/\{key\}/gi, key);
        html = html.replace(/\{count\}/gi, count);

        var tr = document.createElement('tr');
        $(tr).attr('data-key', key);
        $(tr).attr('data-count', count);
        
        $(tr).append(html);
        
        // No
        $(tr).find('.product-no span').text(count+1);
        //$(tr).find('.product-no input').val(1);
        // Name
        $(tr).find('.product-name input').val(1);
        $(tr).find('.product-name span').text(1);
        // Quantity
        $(tr).find('.product-quantity input').val(1);
        
        $(tbody).append(tr);
    });
    
    /**
     * Event when click on add receiver button
     */
    $(document).on('click', '.btn-add-receiver', function(){
        var html = $('#receiver-template').html();
        html = html.replace(/\{key\}/gi, ++maxCount);
        $('.gift-receiver-content').append(html);
    });
    
    /**
     * Event when searh customer
     */
    var row;
    $(document).on('click','.btn-search-customer',function(){
        $('#modalCustomerGridview .content').html("");
        row = $(this).attr('data-key');
        $.ajax({
            type: 'POST',
            cache: false,
            data: {
                first_name: '',//$('#mastercustomer-customer_receiver-' + row + '-first_name').val(),
                last_name: '',//$('#mastercustomer-customer_receiver-' + row + '-last_name').val(),
                post_code: '',//$('#mastercustomer-customer_receiver-' + row + '-post_code').val().replace(/[^0-9\.]+/g, ""),
                address: '',//$('#mastercustomer-customer_receiver-' + row + '-address').val(),
                mobile: '',//$('#mastercustomer-customer_receiver-' + row + '-mobile').val(),
                first_name_kana: '',//$('#mastercustomer-customer_receiver-' + row + '-first_name_kana').val(),
                last_name_kana: ''//$('#mastercustomer-customer_receiver-' + row + '-last_name_kana').val()
            },
            url: 'search-customer',
            success: function (response) {
                $('#modalCustomerGridview .content').html(response);
                $('#modalCustomerGridview').modal('show');
            }
        });
    });
    /**
     * Event when choose customer
     */
    $(document).on('click', '#choose-customer', function(){
        $('#modalCustomerGridview').modal('hide');
        var radio_check = $('#modalCustomerGridview input[name*="MasterCustomer[select_customer][id]"]:checked');
        var key = radio_check.attr('key');
        $('#mastercustomer-customer_receiver-' + row + '-id').val(radio_check.val());
        $('#mastercustomer-customer_receiver-' + row + '-first_name').val($('#modalCustomerGridview #first_name_'+ key).val());
        $('#mastercustomer-customer_receiver-' + row + '-last_name').val($('#modalCustomerGridview #last_name_'+ key).val());
        $('#mastercustomer-customer_receiver-' + row + '-first_name_kana').val($('#modalCustomerGridview #first_name_kana_'+ key).val());
        $('#mastercustomer-customer_receiver-' + row + '-last_name_kana').val($('#modalCustomerGridview #last_name_kana_'+ key).val());
        $('#mastercustomer-customer_receiver-' + row + '-mobile').val($('#modalCustomerGridview #mobile_'+ key).val());
        $('#mastercustomer-customer_receiver-' + row + '-post_code').val($('#modalCustomerGridview #post_code_'+ key).val());
        $('#mastercustomer-customer_receiver-' + row + '-address').val($('#modalCustomerGridview #address_'+ key).val());
        $('#mastercustomer-customer_receiver-' + row + '-address2').val($('#modalCustomerGridview #address2_'+ key).val());
        $('#customer-code-' + row).val($('#code_'+ key).val());
        $('#modalCustomerGridview .content').html("");
    });
    /**
     * Event when searh sender
     */
    $(document).on('click','.btn-search-sender',function(){
        $('#modalSenderGridview .content').html("");
        $.ajax({
            type: 'POST',
            cache: false,
            data: {
                first_name: '',//$('#mastercustomer-customer_sender-first_name').val(),
                last_name: '',//$('#mastercustomer-customer_sender-last_name').val(),
                post_code: '',//$('#mastercustomer-customer_sender-post_code').val().replace(/[^0-9\.]+/g, ""),
                address: '',//$('#mastercustomer-customer_sender-address').val(),
                mobile: '',//$('#mastercustomer-customer_sender-mobile').val(),
                first_name_kana: '',
                last_name_kana: ''
            },
            url: 'search-customer',
            success: function (response) {
                $('#modalSenderGridview .content').html(response);
                $('#modalSenderGridview').modal('show');
            }
        });
    });
    /**
     * Event when choose customer
     */
    $(document).on('click', '#choose-sender', function(){
        $('#modalSenderGridview').modal('hide');
        var radio_check = $('#modalSenderGridview input[name*="MasterCustomer[select_customer][id]"]:checked');
        var key = radio_check.attr('key');
        $('#mastercustomer-customer_sender-id').val(radio_check.val());
        $('#mastercustomer-customer_sender-first_name').val($('#modalSenderGridview #first_name_'+ key).val());
        $('#mastercustomer-customer_sender-last_name').val($('#modalSenderGridview #last_name_'+ key).val());
        $('#mastercustomer-customer_sender-first_name_kana').val($('#modalSenderGridview #first_name_kana_'+ key).val());
        $('#mastercustomer-customer_sender-last_name_kana').val($('#modalSenderGridview #last_name_kana_'+ key).val());
        $('#mastercustomer-customer_sender-mobile').val($('#modalSenderGridview #mobile_'+ key).val());
        $('#mastercustomer-customer_sender-post_code').val($('#modalSenderGridview #post_code_'+ key).val());
        $('#mastercustomer-customer_sender-address').val($('#modalSenderGridview #address_'+ key).val());
        $('#mastercustomer-customer_sender-address2').val($('#modalSenderGridview #address2_'+ key).val());
        $('#customer-code-' + row).val($('#code_'+ key).val());
        $('#modalSenderGridview .content').html("");
    });
    
    /**
     * ajax submit
     */
    $(document).on('submit', 'form', function(e){
        $('.custom-error, .custom-error-border').removeClass('active');
        $('.custom-error, .custom-error-border').closest('tr').removeClass('error-input');
        e.preventDefault();
        var form = this;
        $.ajax({
            type: 'POST',
            data: $(form).serialize(),
            dataType: 'json',
            url: SITE_ROOT + '/master-gift/update?id=<?php echo $gift->id ?>',
            success: function(response){
                if(!response.status){
                    for(var key in response.errors){
                        $('.'+key).text(response.errors[key]);
                        $('.'+key).addClass('active');
                        $('.'+key).closest('tr').addClass('error-input');
                    }
                } else {
                    window.location.href = SITE_ROOT + '/master-gift/confirm-update?id=<?php echo $gift->id ?>';
                }
            }
        });
    });
    
    /**
     * Event when click on delete product
     */
    $(document).on('click', '.btn-delete-product', function(){
        $(this).closest('tr').remove();
    });
});
</script>