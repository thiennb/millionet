<?php

use yii\helpers\Html;
use yii\bootstrap\Modal;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $gift common\models\MasterGift */

$this->title = Yii::t('backend', 'Destination gift voucher');
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Master Gifts'), 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('backend', 'Update');

$normalOption = [
    'options' => ['class' => 'form-group'],
    'template' => '<div class="col-md-3">{label}</div><div class="col-md-4">{input}</div>' //{hint}{error}
];

$smallOption = [
    'options' => [],
    'template' => '<div class="col-md-2">{label}</div><div class="col-md-4">{input}</div>' //{hint}{error}
];

$longOption = [
    'options' => ['class' => 'form-group'],
    'template' => '<div class="col-md-3">{label}</div><div class="col-md-8">{input}</div>' //{hint}{error}
];

$noLabelOption = [
    'options' => ['class' => 'row'],
    'template' => '<div class="col-md-12">{input}</div>' //{hint}{error}
];

$hiddenOption = [
    'options' => ['class' => ''],
    'template' => '{input}' //{hint}{error}
];
//$list_product = implode(", ", $product_gift);
?>
<div class="row text-setting company">
    <div class="col-lg-12 col-md-12">
        <div class="box box-info box-solid add">
            <div class="box-header with-border">
                <h4 class="text-title"><b><?= $this->title ?></b></h4>
            </div>
            <div class="box-body content">
                <?php
                $form = ActiveForm::begin([
                            'layout' => 'horizontal',
                            'enableClientValidation' => false,
                            'options' => ['data-pjax' => true],
                            'fieldConfig' => [
                               // 'template' => "{beginWrapper}<div class='col-md-3'>{label}</div><div class='col-md-'>{input}\n{hint}\n{error}</div>\{endWrapper}",
                                'horizontalCssClasses' => [
                                    'label' => '',
                                    'wrapper' => 'row',
                                    'error' => '',
                                    'hint' => '',
                                ],
                                'horizontalCheckboxTemplate' => "{input}{label}\n{error}\n{hint}"
                            ],
                        ]);
                ?>
                <div class="col-md-12">
                    <!-- Begin HTML for 差出人情報 -->
                    <div class="row">
                        <div class="col-md-8">
                            <div class="box-header with-border common-box-h4 col-md-4 ">
                                <h4 class="text-title"><b><?= Yii::t('backend', 'People Send') ?></b></h4>
                            </div>
                            <div class="col-md-8">
                                <button type="button" class="btn btn-default common-button-submit pull-right btn-print" style="margin: 5px -15px 5px 0"><i class="fa fa-print"></i> <?php echo Yii::t('backend', 'Print') ?></button>
                            </div>
                        </div>
                        
                        <div class="col-md-8">
                            <table class="table table-bordered">
                                <tr>
                                    <th class="text-left" width="30%"><?php echo Yii::t('backend', 'Register Date') ?></th>
                                    <td class="text-left"><?php echo $gift->gift_date ?></td>
                                </tr>
                                <tr>
                                    <th class="text-left"><?php echo Yii::t('backend', 'Member Specified Label') ?></th>
                                    <td class="text-left"><?php echo $giftSender->customer_jan_code ?></td>
                                </tr>
                                <tr>
                                    <th class="text-left"><?php echo Yii::t('backend', 'People Send') ?></th>
                                    <td class="text-left"><?php echo $giftSender->first_name ?> <?php echo $giftSender->last_name ?></td>
                                </tr>
                                <tr>
                                    <th class="text-left"><?php echo Yii::t('backend', 'Phone') ?></th>
                                    <td class="text-left"><?php echo $giftSender->mobile ?></td>
                                </tr>
                                <tr>
                                    <th class="text-left"><?php echo Yii::t('backend', 'People Send Post') ?></th>
                                    <td class="text-left"><?php echo $giftSender->post_code ?></td>
                                </tr>
                                <tr>
                                    <th class="text-left"><?php echo Yii::t('backend', 'Address') ?></th>
                                    <td class="text-left"><?php echo $giftSender->address ?><br/><?php echo $giftSender->address2 ?></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <!-- End HTML for 差出人情報 -->
                    
                    <!-- Begin HTML for 差出人情報 -->
                    <div class="row">
                        <div class="col-md-8">
                            <div class="box-header with-border common-box-h4 col-md-4 ">
                                <h4 class="text-title"><b><?= Yii::t('backend', 'Destination') ?></b></h4>
                            </div>
                        </div>
                    
                        <?php
                        $count = 0;
                        $total = [];
                        foreach ($receiverCustomers as $key => $receiverCustomer):
                        $count++;
                        ?>
                        <div class="col-md-8">
                            <table class="table table-bordered">
                                <tr>
                                    <th class="text-left" width="30%">宛名<?php echo $count ?></th>
                                    <td class="text-left"><?php echo $receiverCustomer['customer']->first_name ?> <?php echo $receiverCustomer['customer']->last_name ?></td>
                                </tr>
                                <tr>
                                    <th class="text-left">宛名カナ<?php echo $count ?></th>
                                    <td class="text-left"><?php echo $receiverCustomer['customer']->first_name_kana ?> <?php echo $receiverCustomer['customer']->last_name_kana ?></td>
                                </tr>
                                <tr>
                                    <th class="text-left">電話番号<?php echo $count ?></th>
                                    <td class="text-left"><?php echo $receiverCustomer['customer']->mobile ?></td>
                                </tr>
                                <tr>
                                    <th class="text-left">郵便番号<?php echo $count ?></th>
                                    <td class="text-left"><?php echo $receiverCustomer['customer']->post_code ?></td>
                                </tr>
                                <tr>
                                    <th class="text-left">住所<?php echo $count ?></th>
                                    <td class="text-left"><?php echo $receiverCustomer['customer']->address ?> <?php echo $receiverCustomer['customer']->address2 ?></td>
                                </tr>
                                <tr>
                                    <th class="text-left">商品<?php echo $count ?></th>
                                    <td class="text-left" style="padding: 0;">
                                        <table class="table-bordered table" style="margin: 0;">
                                            <tr>
                                                <th width="60%">商品名</th>
                                                <th width="20%">数量</th>
                                                <th width="20%">金額</th>
                                            </tr>
                                            <?php foreach($receiverCustomer['products'] as $proKey => $giftReceiver): ?>
                                                <?php 
                                                if(!isset($total[$giftReceiver->product->id])){
                                                    $total[$giftReceiver->product->id] = [
                                                        'name' => $giftReceiver->product->name,
                                                        'quantity' => $giftReceiver->quantity,
                                                        'price' => $giftReceiver->product->unit_price
                                                    ];
                                                } else {
                                                    $total[$giftReceiver->product->id]['quantity'] = $total[$giftReceiver->product->id]['quantity'] + $giftReceiver->quantity;
                                                }
                                                ?>
                                            <tr>
                                                <td class="text-left"><?php echo $giftReceiver->product->name ?></td>
                                                <td class="text-right"><?php echo $giftReceiver->quantity ?></td>
                                                <td class="text-right"><?php echo number_format($giftReceiver->quantity * $giftReceiver->product->unit_price) ?></td>
                                            </tr>
                                            <?php endforeach; ?>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <?php endforeach; ?>
                    </div>
                    <!-- End HTML for 差出人情報 -->
                    
                    <div class="row">
                        <div class="col-md-8">
                            <div class="box-header with-border common-box-h4 col-md-4 ">
                                <h4 class="text-title"><b>合計</b></h4>
                            </div>
                        </div>
                        
                        <div class="col-md-8">
                            <table class="table table-bordered">
                                <tr>
                                    <th class="text-center" width="72%">商品名</th>
                                    <th class="text-center" width="14%">数量</th>
                                    <th class="text-center">金額</th>
                                </tr>
                                <?php
                                $ttQuantity = 0;
                                $ttPrice = 0;
                                foreach ($total as $value): ?>
                                <tr>
                                    <td class="text-left" width="72%"><?php echo $value['name'] ?></td>
                                    <td class="text-right" width="14%"><?php $ttQuantity += $value['quantity'];  echo $value['quantity']; ?></td>
                                    <td class="text-right"><?php $price = $value['quantity']*$value['price']; echo number_format($value['quantity']*$value['price']); $ttPrice += $price; ?></td>
                                </tr>
                                <?php endforeach; ?>
                                <tr>
                                    <th class="text-center">合計</th>
                                    <th class="text-right"><?php echo $ttQuantity; ?></th>
                                    <th class="text-right"><?php echo number_format($ttPrice); ?></th>
                                </tr>
                            </table>
                        </div>
                    </div>
                    
                    <div class="row form-group">
                        <div class="col-md-8">
                            <?= Html::a(Yii::t('backend', 'Return'), ['update?id='.$gift->id], ['class' => 'btn btn-default common-button-default']) ?>
                            <button class="btn btn-default common-button-submit" id="save-receiver"><?php echo Yii::t('backend', 'Save') ?></button>
                            <button type="button" class="btn btn-default common-button-submit pull-right btn-print"><i class="fa fa-print"></i> <?php echo Yii::t('backend', 'Print') ?></button>
                        </div>
                    </div>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
$(document).ready(function(){
    /**
     * Event when click on print button
     */
    $(document).on('click','.btn-print', function(){
        $.ajax({
            type: 'POST',
            data: {
                id: <?php echo $gift->id ?>
            },
            url: SITE_ROOT + '/master-gift/ajax-print',
            success: function(response){
                var w = window.open();
                w.document.write(response);
                w.print();
                w.close();
            }
        });
    });
});
</script>