<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\LinkPager;
use yii\widgets\Pjax;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $searchModel common\models\MasterGiftSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('backend', 'Master Gift');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row">
    <div class="col-lg-12 col-md-12">
        <div class="box box-info box-solid">
            <div class="box-header with-border">
                <h4 class="text-title"><b><?= $this->title ?></b></h4>
            </div>
            <div class="box-body content">
                <div class="col-md-12">
                    <div class="common-box">
                        <div class="row">
                            <div class="box-header with-border common-box-h4 col-lg-8 col-md-12">
                            <h4 class="text-title"><b><?= Yii::t('backend', 'Gift Search') ?></b></h4>
                        </div>
                        </div>

                        <div class="box-body content">
                            <div class="col-lg-10 col-md-12">
                                <?php echo $this->render('_search', ['model' => $searchModel]); ?>                                
                            </div>
                        </div>
                    </div>
                    <div class="common-box">
                        <div class="row">
                            <div class="box-header with-border common-box-h4 col-lg-8 col-md-12">
                                <h4 class="text-title"><b><?= Yii::t('backend', 'Gift List') ?></b></h4>
                            </div>
                        </div>
                        <div class="box-body content">
                            <div class="col-md-10">
                                <?php
                                $form = ActiveForm::begin([
                                            'options' => [
                                                'id' => 'list-gift',
                                            ], // important
                                            'action' => ['export-csv-gift'],
                                            'method' => 'post',
                                            'enableClientValidation' => false,
                                ]);
                                ?>
                                <?php
                                Pjax::begin();
                                ?> 
                                <?php $pagination = '<div class="row">
                                    <div class="col-md-3 gift_search_vsv_bt">'.Html::submitInput(Yii::t('backend', 'Export Gift CSV'), [
                                            'class' => 'btn common-button-submit magin-button-giff',
                                            'id' => 'get-csv',
                                            'name' => 'btn-confirm',
                                            'disabled' => true,
                                        ]).'</div>
                                    <div class="col-md-6 text-center">{pager}</div>
                                    <div class="col-md-3 gift_search_ct_bt">'.Html::a(Yii::t('backend', 'Create Gift'), ['index-order'], ['class' => 'btn common-button-submit common-float-right', 'onClick' => 'window.location.href = this.href;']).'</div>
                                </div>'; ?>
                                
                                <?=
                                GridView::widget([
                                    'dataProvider' => $dataProvider,
                                    'tableOptions' => [
                                        'class' => 'table table-striped table-bordered text-center',
                                        'id' => 'gift_gridview'
                                    ],
                                    'layout' => $pagination."\n{summary}\n{items}",
                                    'summaryOptions' => ['class' => 'common-clr-fl-right'],
                                    'summary' => false,
                                    'pager' => [
                                        'class' => 'common\components\LinkPagerMillionet',
                                        'options' => ['class' => 'pagination']
                                    ],
                                    'columns' => [
                                        ['class' => 'yii\grid\SerialColumn', 'header'=>'No','options' => ['class' => 'with-table-no']],
                                        [
                                            'class' => 'yii\grid\CheckboxColumn',
                                        ],
                                        [
                                            'attribute' => 'gift_code',
                                        ],
                                        [
                                            'attribute' => 'gift_date',
                                        ],
                                        [
                                            'attribute' => 'customer_send_name',
                                            'value' => function ($model) {
                                                if (isset($model->masterCustomer)) {
                                                    return $model->masterCustomer->first_name . $model->masterCustomer->last_name;
                                                } else {
                                                    return '';
                                                }
                                            },
                                        ],
                                        [
                                            'attribute' => 'quantity_receive_quantity_total',
                                            'value' => function ($model) {
                                                return (new common\models\MasterGiftReceiver)->getTotalQuantityResidual($model->id) . "/" . $model->total_quantity;
                                            },
                                        ],
                                        [
                                            'class' => 'yii\grid\ActionColumn', 'template' => "{delete} {update}",
                                            'options' => ['class' => 'with-table-button'],
                                            'buttons' => [
                                                'delete' => function ($url, $model) {
                                                    return Html::a('<span class="fa fa-delete"></span>' . Yii::t('backend', 'Delete'), $url, [
                                                                //'title' => Yii::t('backend', 'Delete'),
                                                                'class' => 'btn common-button-action',
                                                                'aria-label' => "Delete",
                                                                'data-confirm' => Yii::t('backend', "Delete Option Error Message"),
                                                                'data-method' => "post"
                                                    ]);
                                                },
                                                'update' => function ($url, $model) {
                                                    return Html::a('<span class="fa fa-delete"></span>' . Yii::t('backend', 'Update'), $url, [
                                                                //'title' => Yii::t('backend', 'Update'),
                                                                'class' => 'btn common-button-action',
                                                    ]);
                                                },
                                                    ],
                                                ],
                                            ],
                                        ]);
                                        ?>
                                        <?php
                                        $pjaxRegister = "$(document).on('pjax:complete', function() { 
                                        $(\"#gridid\").yiiGridView(\"setSelectionColumn\", {\"name\":\"selection[]\",\"multiple\":true,\"checkAll\":\"selection_all\"});
                                        selectCheck();
                                    })";
                                        $this->registerJs($pjaxRegister, \yii\web\View::POS_END);
                                        Pjax::end();
                                        ?>
                                        <?php
                                        ActiveForm::end();
                                        ?>
                           </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).on('click', '[name="selection_all"]', function () {
        $("[name='selection[]']").prop('checked', $(this).prop('checked')).trigger('change');
    });
    
    $(document).on('change', '[name="selection[]"]', function () {
        if($('input[name="selection[]"]:checked').length > 0){
            $("#get-csv").prop('disabled', false);
        } else {
            $("#get-csv").prop('disabled', true);
        }
    });
    
    function selectCheck() {
        for (var id in $('#check_list').val().split("_")) {
            if ($('#check_list').val().split("_")[id] != "") {
                $("input[name='selection[]'][value=" + $('#check_list').val().split("_")[id] + "]").prop('checked', true);
            }
        }
        if ($("[name='selection[]']").length === $("[name='selection[]']:checked").length) {
            $('[name="selection_all"]').prop('checked', true);
        }
        $("#get-csv").prop('disabled', $('#check_list').val() === "");
    }
</script>


