<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\LinkPager;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel common\models\MasterGiftSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('backend', 'Master Orders');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row">
    <div class="col-lg-12 col-md-12">
        <div class="box box-info box-solid">
            <div class="box-header with-border">
                <h4 class="text-title"><b><?= $this->title ?></b></h4>
            </div>
            <div class="box-body content">
                <div class="col-md-12">
                    <div class="common-box">
                        <div class="row">
                            <div class="box-header with-border common-box-h4 col-lg-8 col-md-12">
                                <h4 class="text-title"><b><?= Yii::t('backend', 'Order Search') ?></b></h4>
                            </div>
                        </div>

                        <div class="box-body content">
                            <div class="col-lg-10 col-md-12">
                                <?php echo $this->render('_search_order', ['model' => $searchModel]); ?>   
                            </div>
                        </div>
                    </div>
                    <div class="common-box">
                        <div class="row">
                            <div class="box-header with-border common-box-h4 col-lg-8 col-md-12">
                                <h4 class="text-title"><b><?= Yii::t('backend', 'Order List') ?></b></h4>
                            </div>
                        </div>
                        <div class="box-body content">
                            <div class="col-md-10 text-center">
                                <?php
                                Pjax::begin();
                                ?>
                        
                                    <?=
                                    GridView::widget([
                                        'dataProvider' => $dataProvider,
                                        'tableOptions' => [
                                            'class' => 'table table-striped table-bordered',
                                        ],
                                        'layout' => "<div class=\"text-center\">{pager}</div>\n{summary}\n<div class=\"table-responsive\">{items}</div>",
                                        'summaryOptions' => ['class' => 'common-clr-fl-right'],
                                        'summary' => false,
                                        'pager' => [
                                            'class' => 'common\components\LinkPagerMillionet',
                                            'options' => ['class' => 'pagination'],
                                        ],
                                        'columns' => [
                                            ['class' => 'yii\grid\SerialColumn', 'header' => 'No', 'options' => ['class' => 'with-table-no']],
                                            [
                                                'attribute' => 'order_code',
                                            ],
                                            [
                                                'attribute' => 'process_date',
                                            ],
                                            [
                                                'attribute' => 'customer_name',
                                                'value' => function ($model) {
                                                    if (isset($model->masterCustomer)) {
                                                        return $model->masterCustomer->first_name . $model->masterCustomer->last_name;
                                                    } else {
                                                        return '';
                                                    }
                                                },
                                                'label' => Yii::t('backend', 'People Send Name'),
                                            ],
                                            [
                                                'attribute' => 'gift_quantity',
                                                'label' => Yii::t('backend', 'Quantity'),
                                                'value' => function($model){
                                                    if(isset($model->giftOrderDetail)){
                                                        $gifts = $model->giftOrderDetail;
                                                        $quantity = 0;
                                                        foreach($gifts as $gift){
                                                            $quantity+=$gift['quantity'];
                                                        }
                                                        return $quantity;
                                                    }else{
                                                        return '';
                                                    }
                                                },
                                            ],
                                            [
                                                'class' => 'yii\grid\ActionColumn', 'template' => "{create-gift}",
                                                'buttons' => [
                                                    //view button
                                                    'create-gift' => function ($url, $model) {
                                                        return Html::a('<span class="fa fa-delete"></span>' . Yii::t('backend', 'Select Gift'), $url, [
                                                                    'class' => 'btn common-button-action',
                                                        ]);
                                                    },
                                                        ],
                                                    ],
                                                ],
                                            ]);
                                    ?>
                                <?php
                                Pjax::end();
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


