<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Modal;

/* @var $this yii\web\View */
/* @var $model common\models\MasterGift */

$this->title = Yii::t('backend', 'Select Master Gift');
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Select Master Gift'), 'url' => ['create-gift']];
$this->params['breadcrumbs'][] = $this->title;
?>

<?php
$form = ActiveForm::begin(
                [
                    'action' => ['create-gift-gift-detail', 'id' => $id],
                    'enableClientValidation' => false,
                    'method' => 'get',
                    'layout' => 'horizontal',
                    'enableClientValidation' => false,
                    'fieldConfig' => [
                        'template' => "{beginWrapper}<div class='col-md-3'>{label}</div><div class='col-md-9'>{input}\n{hint}\n{error}</div>\n{endWrapper}",
                        'horizontalCssClasses' => [
                            'label' => '',
                            'wrapper' => 'row',
                            'error' => '',
                            'hint' => '',
                        ],
                    ],
        ]);
?>
<div class="row text-setting company">
    <div class="col-lg-12 col-md-12">
        <div class="box box-info box-solid add">
            <div class="box-header with-border">
                <h4 class="text-title"><b><?= $this->title ?></b></h4>
            </div>
            <div class="box-body content">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="box-header with-border common-box-h4 col-md-6 ">
                                <h4 class="text-title"><b><?= Yii::t('backend', 'Select Gift') ?></b></h4>
                            </div>
                            <div class="col-md-12">
                                <?=
                                GridView::widget([
                                    'dataProvider' => $dataProvider,
                                    'tableOptions' => [
                                        'class' => 'table table-striped table-bordered text-center',
                                        'id' => 'grid',
                                    ],
                                    'layout' => "{pager}\n{summary}\n{items}",
                                    'summaryOptions' => ['class' => 'common-clr-fl-right'],
                                    'summary' => false,
                                    'pager' => [
                                        'firstPageLabel' => '<<',
                                        'lastPageLabel' => '>>',
                                        'prevPageLabel' => '<',
                                        'nextPageLabel' => '>',
                                        'options' => ['class' => 'pagination common-float-right'],
                                    ],
                                    'columns' => [
                                        ['class' => 'yii\grid\SerialColumn', 'header' => 'No', 'options' => ['class' => 'with-table-no']],
                                        [
                                            'class' => 'yii\grid\CheckboxColumn',
                                            'checkboxOptions' => function ($model, $key, $index, $column) {
                                                return ['value' => $model->check_list];
                                            }
                                                ],
                                                [
                                                    'attribute' => 'product_name',
                                                    'value' => function ($model) {
                                                        if (isset($model->masterProduct)) {
                                                            return $model->masterProduct->name;
                                                        } else {
                                                            return '';
                                                        }
                                                    },
                                                ],
                                                [
                                                    'attribute' => 'quantity',
                                                ],
                                            ],
                                        ]);
                                        ?>

                                    </div>

                                    <div class="row col-md-12">
                                        <?= Html::a(Yii::t('backend', 'Return'), ['index-order'], ['class' => 'btn btn-default common-button-default']) ?>
                                        <?= Html::submitButton(Yii::t('backend', 'Save'), ['class' => 'btn common-button-submit']) ?>    
                                    </div>

                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php ActiveForm::end(); ?>
        <?php
        Modal::begin([
            'id' => 'userModal',
            'size' => 'SIZE_LARGE',
            'header' => '<div class="text-center"><b>' . Yii::t('backend', 'Create Gift Success') . '</b></div>',
        ]);
        ?>
        <div class="row">
            <div class="col-lg-6 col-lg-offset-3"> 
                <?= Html::a(Yii::t('backend', 'Go to create gift'), ['index-order'], ['class' => 'btn common-button-submit btn-block','style' => 'width: auto']) ?>
            </div>
        </div>
        
        <div class="row">
            <div class="col-lg-6 col-lg-offset-3" style="margin-top: 15px;"> 
                <?= Html::a(Yii::t('backend', 'Go to enter receiver'), ['update', 'id' => $idGift], ['class' => 'btn common-button-submit btn-block' ,'style' => 'width: auto']) ?>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-6 col-lg-offset-3" style="margin-top: 15px;"> 
                <?= Html::a(Yii::t('backend', 'Go to list Gift'), ['index'], ['class' => 'btn btn-default common-button-default btn-block','style' => 'width: auto']) ?>
            </div>
        </div>
        <?php
        Modal::end();
        ?>

        <script>
        <?php
        if ($popup_show) {
            echo '$(document).ready(function () {';
            echo "$('#userModal').modal('show');";
            echo '});';
        }
        ?>
</script>
