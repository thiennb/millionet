<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use unclead\widgets\MultipleInput;
use yii\jui\DatePicker;
use yii\bootstrap\Modal;
use common\widgets\preview\Preview;

/* @var $this yii\web\View */
/* @var $model common\models\MasterTax */
/* @var $form yii\widgets\ActiveForm */

$this->registerJsFile('@web/js/jquery.numeric.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile('@web/js/master_tax.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
?>

<div class="master-tax-form">

    <?php
    $form = ActiveForm::begin([
                'options' => [
                    'enctype' => 'multipart/form-data',
                    'id' => strtolower($model->formName()) . '-id',
                ],
                // important
                'validationUrl' => 'validate',
                'enableClientValidation' => false,
                'enableAjaxValidation' => true,
                'validateOnChange' => false,
                'validateOnBlur' => false,
                'successCssClass' => '',
    ]);
    ?>

    <?=
    $form->field($model, 'id_tax', [
        'template' => '<div class="col-md-12">'
        . '{input}{error}'
        . '</div>'
    ])->hiddenInput(['maxlength' => true, 'value' => $model->id])->label(FALSE)
    ?>
    <?=
    $form->field($model, 'name', [
        'template' => '<div class="row">'
        . '<div class="col-md-2">{label}</div>'
        . '<div class="col-md-4">{input}'
        . '{error}'
        . '</div>'
        . '<div class="col-md-6"></div>'
        . '</div>'
    ])->textInput(['maxlength' => true])
    ?>
    <?=
    $form->field($model, 'error_tax_rate', [
        'template' => '<div class="col-md-12">'
        . '{input}{error}'
        . '</div>'
    ])->hiddenInput(['maxlength' => true])->label(FALSE)
    ?>
    <?=
    $form->field($model, 'mutil_rate')->widget(MultipleInput::className(), [
//        'limit' => 4,
        'removeButtonOptions' => [
            'class' => 'common-button-action',
            'label' => Yii::t('backend', 'Delete'),
        ],
        'columns' => [
            [
                'name' => 'reduced_tax_flg',
                'title' => Yii::t('backend', 'Reduced tax rate'),
                'type' => 'checkbox',
                'headerOptions' => [
                    'class' => 'input-priority col-md-2 text-center table_header'
                ]
            ],
            [
                'name' => 'rate',
                'enableError' => true,
                'title' => Yii::t('backend', 'Tax'),
                'type' => 'textInput',
                'headerOptions' => [
                    'class' => 'input-priority col-md-3 text-center table_header',
                    'colspan' => '2',
                ],
                'options' => [
                    'class' => 'input-decimal text-right',
                    'maxlength' => 3
                ]
            ],
            [
                'name' => 'type',
                'type' => 'static',
                'value' => function() {
                    return '%';
                },
                'options' => [
                    'class' => 'text-left',
                ],
                'headerOptions' => [
                    'style' => 'display:none;'
                ]
            ],
            [
                'name' => 'start_date',
                'enableError' => true,
                'title' => Yii::t('backend', 'Tax date'),
                'type' => DatePicker::className(),
                'value' => function($data) {
                    return $data['start_date'];
                },
                'headerOptions' => [
                    'class' => 'day-css-class col-md-6 text-center table_header',
                    'colspan' => '3',
                ],
                'options' => [
                    'dateFormat' => 'yyyy/MM/dd',
                    'clientOptions' => [
                        "changeMonth" => true,
                        "changeYear" => true,
                        "yearRange" => "1900:+10"
                    ]
                ],
            ],
            [
                'name' => 'type',
                'type' => 'static',
                'value' => function() {
                    return '～';
                },
                'options' => [
                    'class' => 'text-center',
                ],
                'headerOptions' => [
                    'style' => 'display:none;'
                ]
            ],
            [
                'name' => 'end_date',
                'enableError' => true,
                'type' => DatePicker::className(),
                'value' => function($data) {
                    return $data['end_date'];
                },
                'options' => [
                    'dateFormat' => 'yyyy/MM/dd',
                    'clientOptions' => [
                        "changeMonth" => true,
                        "changeYear" => true,
                        "yearRange" => "1900:+10"
                    ]
                ],
                'headerOptions' => [
                    'style' => 'display:none;'
                ]
            ]
        ]
    ])->label(false);
    ?>
    <div class="form-group"></div>

    <div class="form-group">
        <?= Html::a(Yii::t('backend', 'Return'), ['index'], ['class' => 'btn btn-default common-button-default']) ?>
        <?=
        Html::submitButton(Yii::t('backend', 'Confirm'), [
            'class' => 'btn common-button-submit',
            'id' => 'btn-confirm',
        ])
        ?>
    </div>
    <div class="row">
        <div class="col-md-8">
            <?php
            Modal::begin([
                'id' => 'userModal',
                'size' => 'SIZE_LARGE',
                'header' => '<div class="box-header with-border common-box-h4 col-md-8"><h4 class="text-title"><b>' . Yii::t('backend', 'Confirm input') . '</b></h4></div>',
                'footer' => Html::button(Yii::t('backend', 'Close'), ['class' => 'btn common-button-default', 'data-dismiss' => "modal", 'id' => 'btn-close'])
                . Html::submitButton(Yii::t('backend', 'Save'), ['class' => 'btn common-button-submit', 'id' => 'btn-submit']),
                'closeButton' => FALSE,
            ]);
            ?>
            <?=
            Preview::widget([
                "data" => [
                    'name' => [
                        'type' => 'input',
                        'label' => Yii::t('backend', Yii::t('backend', 'Name Tax'))
                    ],
                    'multiple-input-list' => [
                        'type' => 'table',
                        'header' => 'table_header',
                        'table' => 'multiple-input-list'
                    ],
                ],
                "modelName" => $model->formName(),
                'idBtnConfirm' => 'btn-confirm',
                'formId' => strtolower($model->formName()) . '-id',
                'btnClose' => 'btn-close',
                'btnSubmit' => 'btn-submit',
                'modalId' => 'userModal'
            ])
            ?>
            <?php
            Modal::end();
            ?>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
</div>
<script>
    jQuery.fn.preventDoubleSubmission = function () {

        var last_clicked, time_since_clicked;

        $("#btn-submit").bind('click', function (event) {

            if (last_clicked)
                time_since_clicked = event.timeStamp - last_clicked;

            last_clicked = event.timeStamp;

            if (time_since_clicked < 2000)
                return false;

            return true;
        });
    };
    $('#mastertax-id').preventDoubleSubmission();

</script>
