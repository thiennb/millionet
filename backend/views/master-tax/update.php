<?php

use yii\helpers\Html;
use dmstr\widgets\Alert;

/* @var $this yii\web\View */
/* @var $model common\models\MasterTax */

$this->title = Html::encode(Yii::t('backend', 'Tax update'));
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Master Taxes'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row text-setting company">
    <div class="col-lg-12 col-md-12">
        <div class="row">
            <div class="form-group"><?= Alert::widget() ?></div>
        </div>
        <div class="box box-info box-solid add">
            <div class="box-header with-border">
                <h4 class="text-title"><b><?= $this->title ?></b></h4>
            </div>
            <div class="box-body content">
                <div class="common-box">
                    <div class="row">
                        <div class="box-header with-border common-box-h4 col-md-6">
                            <h4 class="text-title"><b><?= $this->title ?></b></h4>
                        </div>
                    </div>
                    <div class="row">
                        <div class="box-body content">
                            <div class="col-md-10">
                                <?=
                                $this->render('_form', [
                                    'model' => $model,
                                ])
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
