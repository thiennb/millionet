<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use dmstr\widgets\Alert;
use common\components\Constants;

/* @var $this yii\web\View */
/* @var $searchModel common\models\MasterTaxSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$role = \common\components\FindPermission::getRole();
$this->title = Yii::t('backend', 'Tax Rate Setting');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <div class="col-lg-12 col-md-12">
        <div class="box box-info box-solid">
            <div class="box-header with-border">
                <h4 class="text-title"><b><?= $this->title ?></b></h4>
            </div>
            <div class="box-body content">
                <div class="col-md-12">
<?php Pjax::begin(); ?>    
                    <div class="common-box">
                        <div class="row">
                            <div class="box-header with-border common-box-h4 col-md-6">
                                <h4 class="text-title"><b><?= Yii::t('backend', 'Tax rate List') ?></b></h4>
                            </div>
                        </div>
                        <div class="row">
                            <div class="box-body content">
                                <div class="col-md-10">
                                    <?= Html::a(Yii::t('backend', 'Return'), ['/'], ['class' => 'btn common-button-default btn-default common-float-left margin-bottom-10']) ?>
                                    <?php
                                    if ($role->permission_id == Constants::APP_MANAGER_ROLE || $role->permission_id == Constants::COMPANY_MANAGER_ROLE)
                                        echo Html::a(Yii::t('backend', 'Create Gift'), ['create'], ['class' => 'btn common-button-submit common-float-right margin-bottom-10']);
                                    ?>        

                                    <?=
                                    GridView::widget([
                                        'dataProvider' => $dataProvider,
                                        'layout' => "{pager}\n{summary}\n{items}",
                                        'summaryOptions' => ['class' => 'common-clr-fl-right'],
                                        'summary' => false,
                                        'pager' => [
                                            'class' => 'common\components\LinkPagerMillionet',
                                            'options' => ['class' => 'pagination common-float-right'],
                                        ],
                                        'tableOptions' => [
                                            'class' => 'table table-striped table-bordered text-center',
                                        ],
                                        'columns' => [
                                            ['class' => 'yii\grid\SerialColumn', 'header' => 'No', 'options' => ['class' => 'with-table-no']],
                                            'name',
                                            [
                                                'attribute' => 'rate',
                                                'label' => Yii::t('backend', 'Tax rate app'),
                                                'value' => function ($model) {
                                                    return $model->rate . '%';
                                                },
                                            ],
                                            [
                                                'attribute' => 'Tax date',
                                                'label' => Yii::t('backend', 'Tax date'),
                                                'value' => function ($model) {
                                                    return date('Y/m/d', strtotime($model->start_date)) . " ～ " . date('Y/m/d', strtotime($model->end_date));
                                                },
                                            ],
                                            ($role->permission_id == Constants::APP_MANAGER_ROLE || $role->permission_id == Constants::COMPANY_MANAGER_ROLE) ?            
                                            [
                                                'class' => 'yii\grid\ActionColumn', 'template' => "{delete}{update}",
                                                'options' => ['class' => 'with-table-button'],
                                                'buttons' => [
                                                    'delete' => function ($url, $model) {
                                                        return Html::a('<span class="fa fa-delete"></span>' . Yii::t('backend', 'Delete'), $url . $model->detail_id, [
                                                                    'title' => Yii::t('backend', 'Delete'),
                                                                    'class' => 'btn common-button-action',
                                                                    'aria-label' => "Delete",
                                                                    'data-confirm' => Yii::t('backend', "Delete Tax Error Message"),
                                                                    'data-method' => "post"
                                                        ]);
                                                    },
                                                            'update' => function ($url, $model) {
                                                        return Html::a('<span class="fa fa-delete"></span>' . Yii::t('backend', 'Update'), $url . $model->detail_id, [
                                                                    'title' => Yii::t('backend', 'Update'),
                                                                    'class' => 'btn common-button-action',
                                                        ]);
                                                    },
                                                        ],
                                            ] 
                                            : 
                                            [],
                                                ],
                                            ]);
                                            ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
        <?php Pjax::end(); ?>
                </div>
            </div>
        </div>
    </div>
</div>
