<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use common\models\MstCategoriesProduct;
use dmstr\widgets\Alert;
use common\models\MasterTax;

/* @var $this yii\web\View */
/* @var $searchModel common\models\MstProductSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('backend', 'Product list');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <div class="col-lg-12 col-md-12">
        <div class="box box-info box-solid">
            <div class="box-header with-border">
                <h4 class="text-title"><b><?= $this->title ?></b></h4>
            </div>
            <div class="box-body content">
                <div class="col-md-12">
                    <div class="common-box">
                        <div class="row">
                            <div class="box-header with-border common-box-h4 col-md-4">
                                <h4 class="text-title"><b><?= Yii::t('backend', 'Product Search') ?></b></h4>
                            </div>
                        </div>
                        <div class="box-body">
                            <div class="col-md-5">
                                <?php echo $this->render('_search', ['model' => $searchModel, 'listStore' => $listStore]); ?>                                
                            </div>
                        </div>
                    </div>
                    <?php
                    Pjax::begin();
                    ?>    
                    <div class="common-box">
                        <div class="row">
                            <div class="box-header with-border common-box-h4 col-md-4">
                                <h4 class="text-title"><b><?= Yii::t('backend', 'Product list') ?></b></h4>
                            </div>
                        </div>
                        <div class="row">
                            <div class="box-body content">
                                <div class="col-md-10">
                                    <?= Html::a(Yii::t('backend', 'Create New'), ['create'], ['class' => 'btn common-button-submit common-float-right margin-bottom-10 btn_ct']) ?>
                                    <?=
                                    GridView::widget([
                                        'dataProvider' => $dataProvider,
                                        //        'filterModel' => $searchModel,
                                        'layout' => "{pager}\n{summary}\n{items}",
                                        'summaryOptions' => ['class' => 'common-clr-fl-right'],
                                        'summary' => false,
                                        'pager' => [
                                            'class' => 'common\components\LinkPagerMillionet',
                                            'options' => ['class' => 'pagination common-float-right'],
                                        ],
                                        'tableOptions' => [
                                            'class' => 'table table-striped table-bordered text-center',
                                        ],
                                        'columns' => [
                                            ['class' => 'yii\grid\SerialColumn', 'header' => 'No', 'options' => ['class' => 'with-table-no']],
                                             [
                                                'attribute' => 'name_store',
//                                                'headerOptions' => [
//                                                    'class' => ['col-md-2']
//                                                ]
//                                            'label' => Yii::t('backend', '商品名'),
                                            ],
                                            [
                                                'attribute' => 'name',
//                                                'headerOptions' => [
//                                                    'class' => ['col-md-2']
//                                                ]
//                                            'label' => Yii::t('backend', '商品名'),
                                            ],
                                            [
                                                'attribute' => 'category_name',
//                                                'headerOptions' => [
//                                                    'class' => ['col-md-2']
//                                                ]
                                            ],
                                            [
                                                'attribute' => 'unit_price',
//                                                'headerOptions' => [
//                                                    'class' => ['col-md-2']
//                                                ],
                                                'value' => function ($model) {
                                            if ($model->unit_price == null || empty($model->unit_price)) {
                                                return '¥0';
                                            } else {
                                                return '¥' . Yii::$app->formatter->asDecimal($model->unit_price, 0);
                                            }
                                        },
                                            ],
                                            [
                                                'attribute' => 'tax_rate_id',
                                                'label' => Yii::t('backend', 'Tax Rate'),
                                                'value' => function ($model) {
                                                    return $model->rate;
                                                },
//                                                'headerOptions' => [
//                                                    'class' => ['col-md-2']
//                                                ]
                                            ],
                                            [
                                                'class' => 'yii\grid\ActionColumn', 'template' => "{delete}{update}",
                                                'options' => ['class' => 'with-table-button_product'],
                                                'buttons' => [
                                                    //view button
                                                    'delete' => function ($url, $model) {
                                                        return Html::a('<span class="fa fa-delete"></span>' . Yii::t('backend', 'Delete'), $url, [
                                                                    'title' => Yii::t('backend', 'Delete'),
                                                                    'class' => 'btn common-button-action',
                                                                    'aria-label' => "Delete",
                                                                    'data-confirm' => Yii::t('backend', "Delete Product Error Message"),
                                                                    'data-method' => "post"
                                                        ]);
                                                    },
                                                            'update' => function ($url, $model) {
                                                        return Html::a('<span class="fa fa-delete"></span>' . Yii::t('backend', 'Update'), $url, [
                                                                    'title' => Yii::t('backend', 'Update'),
                                                                    'class' => 'btn common-button-action',
                                                        ]);
                                                    },
                                                        ],
                                                        'headerOptions' => [
                                                            'class' => ['btn_index_product_ct']
                                                        ]
                                                    ],
                                                ],
                                            ]);
                                            ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php
                            Pjax::end();
                            ?>
                </div>
            </div>
        </div>
    </div>
</div>