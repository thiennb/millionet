<?php

use yii\helpers\Html;
use dmstr\widgets\Alert;

/* @var $this yii\web\View */
/* @var $model common\models\MstProduct */

$this->title = Yii::t('backend', 'Edit Product');
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Product list'), 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('backend', 'Product master management');
?>
<div class="row text-setting company">
    <div class="col-lg-12 col-md-12">
        <div class="row">
            <div class="form-group"><?= Alert::widget() ?></div>
        </div>
        <div class="box box-info box-solid add">
            <div class="box-header with-border">
                <h4 class="text-title"><b><?= $this->title ?></b></h4>
            </div>
            <div class="box-body content">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-8">
                            <?=
                            $this->render('_form', [
                                'model' => $model,
                                'listOption'    =>  $listOption,
                                'listStore'    =>  $listStore,
                            ])
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
