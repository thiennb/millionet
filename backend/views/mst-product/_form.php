<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\MstCategoriesProduct;
use kartik\file\FileInput;
use common\components\Util;
use yii\bootstrap\Modal;
use common\widgets\preview\Preview;
use common\components\Constants;
use common\models\MasterTax;
use yii\widgets\Pjax;
use yii\helpers\Url;

$this->registerJsFile('@web/js/check_hiden_product.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile('@web/js/mst_product.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
/* @var $this yii\web\View */
/* @var $model common\models\MstProduct */
/* @var $form yii\widgets\ActiveForm */

$fieldOptions1 = [
    'options' => ['class' => 'form-group '],
    'template' => '<div class="col-md-12"> 
                <div class="col-md-3 label-margin">  
                   {label}
                </div> 
                <div class="col-md-4 input_store_ct">
                   {input}{error}
                </div>
                </div>'
];

$fieldOption2 = [
    'options' => ['class' => 'form-group '],
    'template' => '<div class="col-md-12"> 
                <div class="col-md-3 label-margin">  
                  
                </div> 
                <div class="col-md-4 input_store_ct">
                   {input}{error}
                </div>
                </div>'
];
?>
<style>
    #mstproduct-jan_code{
        text-align: left !important;
    }
</style>
<div class="mst-product-form" >
    <?php
    $form = ActiveForm::begin([
                'options' => [
                    'enctype' => 'multipart/form-data',
                    'id' => strtolower($model->formName()) . '-id',
                ], // important
                'validationUrl' => 'validate',
                'enableClientValidation' => false,
                'enableAjaxValidation' => true,
                'validateOnChange' => false,
                'validateOnBlur' => false,
                'successCssClass' => '',
    ]);
    ?>    
    <?= $form->field($model, 'id_hd')->hiddenInput(['value' => $model->id])->label(FALSE) ?>
    <div class="common-box">
        <div class="row">
            <div class="box-header with-border common-box-h4 col-md-6">
                <h4 class="text-title"><b><?= Yii::t('backend', 'Input product information') ?></b></h4>
            </div>
        </div>
        <div class="box-body content">
            <!-- Datpdt start-->
            <!-- Store Id start -->

            <?= $form->field($model, 'store_id', $fieldOptions1)->dropDownList($listStore, ['prompt' => Yii::t('backend', 'Please Select')]) ?> 
            <!--            <div class="col-md-12"> 
                            <div class="col-md-3 label-margin">  
            <?=
            Yii::t("backend", "Store Id");
            ?>
                            </div> 
                            <div class="col-md-4">
            <?=
            $form->field($model, 'store_id', [
                'template' => '{input}{error}'
            ])->label(false)->dropDownList($listStore, ['prompt' => Yii::t('backend', 'Please Select')])
            ?>
                            </div>
                        </div>-->
            <!-- Store Id end -->
            <!-- assign_fee_flg start -->
            <div class="col-md-12"> 
                <div class="col-md-3 label-margin">  

                </div> 
                <div class="col-md-4 assign_fee_flg_product_ct">
                    <?= $form->field($model, "assign_fee_flg")->checkbox(); ?>
                </div>
            </div>
            <!-- assign_fee_flg end -->
            <!-- Product name start -->
            <div class="col-md-12"> 
                <div class="col-md-3 label-margin required-star">  
                    <?= Yii::t('backend', 'Product name') ?>
                </div> 
                <div class="col-md-4 input_store_ct">
                    <?=
                    $form->field($model, 'name', [
                        'template' => '{input}{error}'
                    ])->textInput(['maxlength' => 100])->label(false)
                    ?>
                </div>
            </div>
            <!-- Product name end -->
            <!-- Name Order start -->
            <div class="col-md-12"> 
                <div class="col-md-3 label-margin">  
                    <?= Yii::t('backend', 'Name Order') ?>
                </div> 
                <div class="col-md-4 input_store_ct">
                    <?=
                    $form->field($model, 'name_order', [
                        'template' => '{input}{error}'
                    ])->textInput(['maxlength' => 100])->label(false)
                    ?>
                </div>
            </div>
            <!-- Name Order end -->
            <!-- Name Bill start -->
            <div class="col-md-12"> 
                <div class="col-md-3 label-margin">  
                    <?= Yii::t('backend', 'Name Bill') ?>
                </div> 
                <div class="col-md-4 input_store_ct">
                    <?=
                    $form->field($model, 'name_bill', [
                        'template' => '{input}{error}'
                    ])->textInput(['maxlength' => 100])->label(false)
                    ?>
                </div>
            </div>
            <!-- Name Bill end -->
            <!-- Category start -->
            <div class="col-md-12"> 
                <div class="col-md-3 label-margin required-star">  
                    <?= Yii::t('backend', 'Category') ?>
                </div> 
                <div class="col-md-4 input_store_ct">
                    <?=
                    $form->field($model, 'category_id', [
                        'template' => '{input}{error}'
                    ])->dropDownList(MstCategoriesProduct::listCategory(), ['prompt' => Yii::t('backend', 'Please Select')])->label(false)
                    ?>
                </div>
            </div>
            <!-- Category end -->
            <!-- Description start -->
            <div class="col-md-12"> 
                <div class="col-md-3 label-margin">  
                    <?= Yii::t('backend', 'Description') ?>
                </div> 
                <div class="col-md-4 input_store_ct">
                    <?=
                    $form->field($model, 'description', [
                        'template' => '{input}{error}'
                    ])->textarea(['maxlength' => true, 'rows' => '6'])->label(false)
                    ?>
                </div>
            </div>
            <!-- Description end -->
            <!-- Tax Internal Foreign start -->
            <div class="col-md-12"> 
                <div class="col-md-3 label-margin required-star">  
                    <?= Yii::t('backend', 'Tax Internal Foreign') ?>
                </div> 
                <div class="col-md-4 input_store_ct">
                    <?=
                    $form->field($model, 'tax_display_method', [
                        'template' => '{input}{error}'
                    ])->dropDownList(Constants::LIST_TAX_INTERNAL_FOREIGN, ['prompt' => Yii::t('backend', 'Please Select')])->label(false)
                    ?>
                </div>
            </div>
            <!-- Tax Internal Foreign end -->
            <!-- Tax start -->
            <div class="col-md-12"> 
                <div class="col-md-3 label-margin required-star">  
                    <?= Yii::t('backend', 'Tax') ?>
                </div> 
                <div class="col-md-4 input_store_ct">
                    <?=
                    $form->field($model, 'tax_rate_id', [
                        'template' => '{input}{error}'
                    ])->dropDownList(MasterTax::listTax(), ['prompt' => Yii::t('backend', 'Please Select')])->label(false)
                    ?>
                </div>
            </div>
            <!-- Tax end -->
            <!-- Price start -->
            <div class="col-md-12"> 
                <div class="col-md-3 label-margin required-star">  
                    <?= Yii::t('backend', 'Price') ?>
                </div> 
                <div class="col-md-4 input_store_ct">
                    <?=
                    $form->field($model, 'unit_price', [
                        'template' => '{input}{error}'
                    ])->widget(\yii\widgets\MaskedInput::className(), [
                        'clientOptions' => [
                            'alias' => 'integer',
                            'autoGroup' => true,
                            'removeMaskOnSubmit' => true,
                            'allowMinus' => false,
                            'groupSeparator' => ',',
                        ]
                    ])->textInput(['maxlength' => true])->label(false)
                    ?>
                </div>
                <div class="col-md-1 label-margin">
                    <label class="control-label" >
                        <?= Yii::t('backend', 'Price Set') ?>
                    </label>
                </div>
            </div>
            <!-- Price end -->
            <!-- Cost start -->
            <div class="col-md-12"> 
                <div class="col-md-3 label-margin">  
                    <?= Yii::t('backend', 'Cost') ?>
                </div> 
                <div class="col-md-4 input_store_ct">
                    <?=
                    $form->field($model, 'cost', [
                        'template' => '{input}{error}'
                    ])->widget(\yii\widgets\MaskedInput::className(), [
                        'clientOptions' => [
                            'alias' => 'integer',
                            'autoGroup' => true,
                            'removeMaskOnSubmit' => true,
                            'allowMinus' => false,
                            'groupSeparator' => ',',
                        ]
                    ])->textInput(['maxlength' => true])->label(false)
                    ?>
                </div>
                <div class="col-md-1 label-margin">
                    <label class="control-label" >
                        <?= Yii::t('backend', 'Price Set') ?>
                    </label>
                </div>
            </div>
            <!-- Cost end -->
            <!-- The time required start -->
            <div class="col-md-12"> 
                <div class="col-md-3 label-margin">  
                    <?= Yii::t('backend', 'The time required') ?>
                </div> 
                <div class="col-md-4 input_store_ct">
                    <?=
                    $form->field($model, 'time_require', [
                        'template' => '{input}{error}'
                    ])->widget(\yii\widgets\MaskedInput::className(), [
                        'clientOptions' => [
                            'alias' => 'integer',
                            'autoGroup' => true,
                            'removeMaskOnSubmit' => true,
                            'allowMinus' => false,
                        ]
                    ])->textInput(['maxlength' => 3])->label(false)
                    ?>
                </div>
                <div class="col-md-1 label-margin">
                    <label class="control-label" >
                        <?= Yii::t('backend', 'Min') ?>
                    </label>
                </div>
            </div>
            <!-- The time required end -->
            <!-- Jan Code start -->
            <div class="col-md-12"> 
                <div class="col-md-3 label-margin">  
                    <?= Yii::t('backend', 'Jan Code') ?>
                </div> 
                <div class="col-md-4 input_store_ct">
                    <?=
                    $form->field($model, 'jan_code', [
                        'template' => '{input}{error}'
                    ])->widget(\yii\widgets\MaskedInput::className(), [
                        'clientOptions' => [
                            'alias' => 'decimal',
                            'autoGroup' => true,
                            'removeMaskOnSubmit' => true,
                            'allowMinus' => false,
                        ]
                    ])->textInput(['maxlength' => true])->label(false)
                    ?>
                </div>

            </div>
            <!-- Jan Code end -->
            <!-- Image 1 start -->
            <div class="col-md-12">
                <div class="col-md-3 label-margin">  
                    <?= Yii::t('backend', 'Image 1') ?>
                </div> 
                <div class="col-md-4 image_product_ct">
                    <?=
                    $form->field($model, 'tmp_image1', [
                        'template' => '{input}{error}'
                    ])->widget(FileInput::classname(), [
                        'options' => ['accept' => 'image/*'],
                        'pluginOptions' => [
                            'allowedFileExtensions' => ['jpg', 'gif', 'png', 'jpeg'],
                            'initialPreview' => [
                                Html::img(Util::getUrlImage($model->image1))
                            ],
                            'overwriteInitial' => true,
                            'showUpload' => false,
                            'showCaption' => false,
                            'browseLabel' => Yii::t('backend', 'Select an image'),
                            'maxFileSize' => 2048
                        ],
                        'pluginEvents' => [
                            "fileclear" => 'function() { $(this).parent().parent().parent().parent().find(".hidden_image").val("") }',
                        ],
                    ]);
                    ?>
                    <?= $form->field($model, 'hidden_image1')->hiddenInput(['class' => 'hidden_image'])->label(FALSE) ?>
                </div>
            </div>
            <!-- Image 1 end -->
            <!-- Image 2 start -->
            <div class="col-md-12"> 
                <div class="col-md-3 label-margin">  
                    <?= Yii::t('backend', 'Image 2') ?>
                </div> 
                <div class="col-md-4 image_product_ct">
                    <?=
                    $form->field($model, 'tmp_image2', [
                        'template' => '{input}{error}'
                    ])->widget(FileInput::classname(), [
                        'options' => ['accept' => 'image/*'],
                        'pluginOptions' => [
                            'allowedFileExtensions' => ['jpg', 'gif', 'png', 'jpeg'],
                            'initialPreview' => [
                                Html::img(Util::getUrlImage($model->image2))
                            ],
                            'overwriteInitial' => true,
                            'showUpload' => false,
                            'showCaption' => false,
                            'browseLabel' => Yii::t('backend', 'Select an image'),
                            'maxFileSize' => 2048
                        ],
                        'pluginEvents' => [
                            "fileclear" => 'function() { $(this).parent().parent().parent().parent().find(".hidden_image").val("") }',
                        ],
                    ]);
                    ?>
                    <?= $form->field($model, 'hidden_image2')->hiddenInput(['class' => 'hidden_image'])->label(FALSE) ?>
                </div>
            </div>
            <!-- Image 2 end -->
            <!-- Image 3 start -->
            <div class="col-md-12"> 
                <div class="col-md-3 label-margin">  
                    <?= Yii::t('backend', 'Image 3') ?>
                </div> 
                <div class="col-md-4 image_product_ct">
                    <?=
                    $form->field($model, 'tmp_image3', [
                        'template' => '{input}{error}'
                    ])->widget(FileInput::classname(), [
                        'options' => ['accept' => 'image/*'],
                        'pluginOptions' => [
                            'allowedFileExtensions' => ['jpg', 'gif', 'png', 'jpeg'],
                            'initialPreview' => [
                                Html::img(Util::getUrlImage($model->image3))
                            ],
                            'overwriteInitial' => true,
                            'showUpload' => false,
                            'showCaption' => false,
                            'browseLabel' => Yii::t('backend', 'Select an image'),
                            'maxFileSize' => 2048
                        ],
                        'pluginEvents' => [
                            "fileclear" => 'function() { $(this).parent().parent().parent().parent().find(".hidden_image").val("") }',
                        ],
                    ]);
                    ?>
                    <?= $form->field($model, 'hidden_image3')->hiddenInput(['class' => 'hidden_image'])->label(FALSE) ?>
                </div>
            </div>
            <!-- Image 3 end -->
            <?php
            if ($model->show_flg == null) {
                $model->show_flg = 0;
            }
            ?>
            <!-- Show  start -->
            <div class="col-md-12"> 
                <div class="col-md-3 label-margin">  
                    <?= Yii::t('backend', 'Show') ?>
                </div> 
                <div class="col-md-2">
                    <?=
                    $form->field($model, 'show_flg', [
                        'template' => '{input}{error}'
                    ])->radioList(Constants::LIST_SHOW)
                    ?>
                </div>
                <div id='tel_booking_flg_hd' class="col-md-2" style='visibility: hidden'>
                    <?= $form->field($model, 'tel_booking_flg')->checkbox() ?>   
                </div>
                <div id='option_condition_flg_hd' class="col-md-3" style='visibility: hidden'>
                    <?= $form->field($model, 'option_condition_flg')->checkbox() ?>
                </div>
            </div>
            <!-- Show  end -->
            <!-- Datpdt end  -->

        </div>
    </div>
    <?= $form->field($model, 'option_hidden')->hiddenInput(['id' => 'option_hidden'])->label(FALSE) ?>

    <div class="common-box">
        <div class="row">
            <div class="box-header with-border common-box-h4 col-md-6">
                <h4 class="text-title"><b><?= Yii::t('backend', 'Selection of options') ?></b></h4>
            </div>
        </div>
        <div class="box-body">
            <div class="col-md-10">
                <?=
                Html::button(Yii::t('backend', 'Option added'), [
                    'class' => 'btn common-button-submit',
                    'id' => 'btn-option',
                    'onclick' => 'init_option_slect(event)'
                ])
                ?>   
                <div class="form-group"></div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="col-md-8 produc-option">
                            <div class="col-md-12 text-center div-border">
                                <label><?= Yii::t('backend', 'Option name') ?></label>
                            </div>
                            <div class="col-md-12 div-border text-center" id="produc-option">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="from-group">
        <?= Html::a(Yii::t('backend', 'Return'), ['index'], ['class' => 'btn btn-default common-button-default']) ?>
        <?=
        Html::submitButton(Yii::t('backend', 'Confirm'), [
            'class' => 'btn common-button-submit',
            'id' => 'btn-confirm',
//            'onclick' => 'init_size_image(event)'
        ])
        ?>
    </div>
    <div class="row">
        <div class="col-md-8">
            <?php
            Modal::begin([
                'id' => 'userModal',
                'size' => 'SIZE_LARGE',
                'header' => '<div class="box-header with-border common-box-h4 col-md-8"><h4 class="text-title"><b>' . Yii::t('backend', 'Confirm Notice') . '</b></h4></div>',
                'footer' => Html::button(Yii::t('backend', 'Close'), ['class' => 'btn common-button-default', 'data-dismiss' => "modal", 'id' => 'btn-close'])
                . Html::submitButton(Yii::t('backend', 'Save'), ['class' => 'btn common-button-submit', 'id' => 'btn-submit']),
                'closeButton' => FALSE,
            ]);
            ?>
            <?=
            Preview::widget([
                "data" => [
                    'store_id' => [
                        'type' => 'select',
                        'label' => Yii::t('backend', 'Store Id')
                    ],
                    'name' => [
                        'type' => 'input',
                        'label' => Yii::t('backend', 'Name')
                    ],
                    'name_order' => [
                        'type' => 'input',
                        'label' => Yii::t('backend', 'Name Order')
                    ],
                    'name_bill' => [
                        'type' => 'input',
                        'label' => Yii::t('backend', 'Name Bill')
                    ],
                    'category_id' => [
                        'type' => 'select',
                        'label' => Yii::t('backend', 'Category')
                    ],
                    'description' => [
                        'type' => 'input',
                        'label' => Yii::t('backend', 'Description')
                    ],
                    'tax_display_method' => [
                        'type' => 'select',
                        'label' => Yii::t('backend', 'Tax Internal Foreign')
                    ],
                    'tax_rate_id' => [
                        'type' => 'select',
                        'label' => Yii::t('backend', 'Tax')
                    ],
                    'unit_price' => [
                        'type' => 'input',
                        'label' => Yii::t('backend', 'Price')
                    ],
                    'cost' => [
                        'type' => 'input',
                        'label' => Yii::t('backend', 'Cost')
                    ],
                    'jan_code' => [
                        'type' => 'input',
                        'label' => Yii::t('backend', 'Jan Code')
                    ],
                    'tmp_image1' => [
                        'type' => 'image',
                        'label' => Yii::t('backend', 'Image 1')
                    ],
                    'tmp_image2' => [
                        'type' => 'image',
                        'label' => Yii::t('backend', 'Image 2')
                    ],
                    'tmp_image3' => [
                        'type' => 'image',
                        'label' => Yii::t('backend', 'Image 3')
                    ],
                    'show_flg' => [
                        'type' => 'radio',
                        'label' => Yii::t('backend', 'Show')
                    ],
                ],
                "modelName" => $model->formName(),
                'idBtnConfirm' => 'btn-confirm',
                'formId' => strtolower($model->formName()) . '-id',
                'btnClose' => 'btn-close',
                'btnSubmit' => 'btn-submit',
                'modalId' => 'userModal'
            ])
            ?>
            <?php
            Modal::end();
            ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-8">
            <?php Pjax::begin(['enablePushState' => false, 'id' => 'pj_select_option', 'linkSelector' => '#refresh_modal_option']); ?>
            <?php
            Modal::begin([
                'id' => 'optionModal',
                'size' => 'SIZE_LARGE',
                'header' => '<div class="box-header with-border common-box-h4 col-md-8"><h4 class="text-title"><b>' . Yii::t('backend', 'Select Options') . '</b><h4></div>',
                'footer' => Html::button(Yii::t('backend', 'Close'), ['class' => 'btn common-button-default', 'data-dismiss' => "modal", 'id' => 'clear-option-select'])
                . Html::button(Yii::t('backend', 'Choice'), ['class' => 'btn common-button-submit', 'data-dismiss' => "modal", 'id' => 'save-option-select']),
                'footerOptions' => [
                    'class' => 'text-center',
                ],
                'closeButton' => FALSE,
                'options' => [
                    'data-backdrop' => 'static',
                    'data-keyboard' => 'false',
                ]
            ]);
            ?>
            <div class="row">
                <div class="col-lg-8 col-md-8 col-md-offset-2 col-lg-offset-2 popup_product">
                    <div class="box box-info box-solid">
                        <div class="box-header with-border">
                            <h4 class="text-title"><b><?= Yii::t('backend', 'Option') ?></b></h4>
                        </div>
                        <div class="box-body content clear-padding clear-padding-top-bot">
                            <div class="col-md-12 clear-padding clear-padding-top-bot clear-form-group-2" id="option_content">
                                <?=
                                $form->field($model, 'option', [
                                    'template' => '{input}'
                                ])->checkboxList($listOption, ['item' => function ($index, $label, $name, $checked, $value) {
                                        return '<div class="col-md-12 div-border">' . Html::checkbox($name, $checked, [
                                                    'value' => $value,
                                                    'label' => $label,
                                                    'class' => 'option-checkbox',
                                                ]) . '</div>';
                                    }])->label(FALSE);
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php Modal::end(); ?>
                    <?= Html::a(Yii::t('backend', 'Refresh'), Url::current(), ['id' => 'refresh_modal_option', 'style' => 'display:none;']) ?>
                    <?php Pjax::end(); ?>
                </div>
            </div>
            <?php ActiveForm::end(); ?>    
</div>
<script type="text/javascript">
    jQuery.fn.preventDoubleSubmission = function () {

        var last_clicked, time_since_clicked;

        $("#btn-submit").bind('click', function (event) {

            if (last_clicked)
                time_since_clicked = event.timeStamp - last_clicked;

            last_clicked = event.timeStamp;

            if (time_since_clicked < 2000)
                return false;

            return true;
        });
    };
    $('#w0').preventDoubleSubmission();


//    previewDialog(false);
    $('#save-option-select').click();

    $(document).ready(function () {
        var check_radio = $("input[name='MstProduct[show_flg]']:checked").val();

        if (check_radio === '1') {
            $("#tel_booking_flg_hd").removeAttr('style');
            $("#option_condition_flg_hd").removeAttr('style');
        }

    });

</script>