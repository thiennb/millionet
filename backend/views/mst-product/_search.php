<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\MstCategoriesProduct;
use common\components\Constants;

/* @var $this yii\web\View */
/* @var $model common\models\MstProductSearch */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="mst-product-search" style="padding-left: 1em;">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'enableClientValidation'=>false,
    ]); ?>

    <?php //$form->field($model, 'id') ?>

    <?php echo $form->field($model, 'store_id',[
        'template' => '<div class="row">'
                        . "<div class='col-md-3'><label class='mws-form-label'>{label}</label></div>"
                        . '<div class="col-md-6 input_product_ct">{input}</div>'
                    . '</div>'
    ])->dropDownList($listStore,['prompt'=>Yii::t('backend', 'Please Select')])->label(Yii::t('backend', 'Store Id'))  ?>
    <?= $form->field($model, 'name',[
        'template' => '<div class="row">'
                        . "<div class='col-md-3'><label class='mws-form-label'>{label}</label></div>"
                        . '<div class="col-md-6 input_product_ct">{input}</div>'
                    . '</div>'
    ])->textInput(['maxlength' => true]) ?>

    <?php echo $form->field($model, 'category_id',[
        'template' => '<div class="row">'
                        . "<div class='col-md-3'><label class='mws-form-label'>{label}</label></div>"
                        . '<div class="col-md-6 input_product_ct">{input}</div>'
                    . '</div>'
    ])->dropDownList(MstCategoriesProduct::listCategory(),
            ['prompt'=>Yii::t('backend', 'Please Select')])->label(Yii::t('backend', 'Category'))  ?>

    <?php 
    if ($model->show_flg == null) {
        $model->show_flg = 0;
    }
    echo $form->field($model, 'show_flg',[
        'template' => '<div class="row">'
                        . "<div class='col-md-3'><label class='mws-form-label'>{label}</label></div>"
                        . '<div class="col-md-4 input_product_ct">{input}</div>'
                    . '</div>'
    ])->radioList(Constants::LIST_SHOW,['separator' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;', 'tabindex' => 3]); ?>

    <div class="form-group">
        <?= Html::a(Yii::t('backend', 'Return'), ['/'], ['class' => 'btn common-button-default btn-default']) ?>
        <?= Html::submitButton(Yii::t('backend', 'Search'), ['class' => 'btn common-button-submit']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>