<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\bootstrap\Collapse;
?>
<?php
$form = ActiveForm::begin([
            'options' => [
                'enctype' => 'multipart/form-data',
                'id' => strtolower($model->formName()) . '-id',
            ], // important
            'enableClientValidation' => false,
        ]);
?>
<?php if (count($listCategory) > 0) { ?>
    <div class=" popup_product">
        <div class="">
            <div class="box-body content clear-padding clear-padding-top-bot">
                <div class="col-md-12 clear-padding clear-padding-top-bot clear-form-group-2">
                    <?php
                    foreach ($listCategory as $key => $value) {
                        $arrCategoryProduct = [];
                        foreach ($listProduct[$key] as $key1 => $value1) {
                            $arrCategoryProduct[$key1] = $value1;
                        }
                        if (count($arrCategoryProduct) > 0) {
                            echo Collapse::widget([
                                'items' => [
                                    // equivalent to the above
                                    [
                                        'label' => $value,
                                        'content' => $form->field($model, 'option', ['template' => '{input}'])
                                                ->checkboxList($arrCategoryProduct, ['item' => function ($index, $label, $name, $checked, $value) {
                                                        return '<div class="">' . Html::checkbox($name, $checked, ['value' => $value, 'label' => Html::encode($label), 'class' => 'option-checkbox',]) . '</div>';
                                                    }])->label(FALSE),
                                            ],
                                        ]
                                    ]);
                                }
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
<?php } ?>
<?php ActiveForm::end(); ?>