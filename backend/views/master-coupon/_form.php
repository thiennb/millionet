<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\jui\DatePicker;
use common\components\Constants;
use kartik\file\FileInput;
use common\components\Util;
use yii\bootstrap\Modal;
use common\widgets\preview\Preview;
use common\widgets\preview\PreviewCoupon;
use yii\widgets\Pjax;
use yii\grid\GridView;
// Drop List
use kartik\depdrop\DepDrop;
use yii\helpers\Url;

$this->registerJsFile('@web/js/insert_coupon.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile('@web/js/before_insert.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
?>
<div class="col-md-8">
    <div class="box-header with-border common-box-h4 col-md-6 ">
        <h4 class="text-title"><b><?= Yii::t('backend', 'Coupon Infomation') ?></b></h4>
    </div>
    <?php
    $form = ActiveForm::begin([
                'options' => [
                    'enctype' => 'multipart/form-data',
                    'id' => strtolower($model->formName()) . '-id',
                ], // important
                'validationUrl' => 'validate',
                'enableClientValidation' => false,
                'enableAjaxValidation' => true,
                'validateOnChange' => false,
                'validateOnBlur' => false,
                'successCssClass' => '',
    ]);
    ?>
    <!-- Datpdt 20/09/2016 -->  
    <!-- ============================== -->
    <!-- ======== Coupon Infomation === -->
    <!-- ============================== -->
    <?= $form->field($model, '_role')->label(false)->hiddenInput([]) ?>
    <?= $form->field($model, 'id_store_comon')->label(false)->hiddenInput([]) ?>
    <div class="col-md-12">
        <?= $form->field($model, 'jan_code_before_create')->hiddenInput()->label(false) ?>
        <div class="master-coupon-form">
            <!-- Coupon Jan Code -->
            <div class="col-md-12"> 
                <div class="col-md-3 label-margin">  
                    <?=
                    Yii::t("backend", "Coupon Jan Code");
                    ?>
                </div> 
                <div class="col-md-3">
                    <?= $form->field($model, 'coupon_jan_code')->label(false)->textInput(['maxlength' => true, 'disabled' => 'disabled']) ?>
                </div>
                <div class="col-md-1 label-margin with-form-label-coupon">
                    <?=
                    Yii::t("backend", "Display Condition");
                    ?>
                </div>
                <div class="col-md-3 display_condition_ct">
                    <?= $form->field($model, 'display_condition')->label(false)->dropDownList(Constants::DISPLAY_CONDITION, ['prompt' => Yii::t('backend', 'Please Select')]) ?>
                </div>
            </div>
            <!-- Display Start Date -->
            <div class="col-md-12"> 
                <div class="col-md-3 label-margin required-star">  
                    <?=
                    Yii::t("backend", "Display Start Date");
                    ?>
                </div> 
                <div class="col-md-3">
                    <?php $model->start_date = Yii::$app->formatter->asDate($model->start_date); ?>
                    <?=
                    $form->field($model, 'start_date', ['template' => '<div class=" clear-padding">{input}{error}</div>'])->widget(DatePicker::classname(), [
                        'language' => 'ja',
                        'dateFormat' => 'yyyy/MM/dd',
                        'clientOptions' => [
                            "changeMonth" => true,
                            "changeYear" => true,
                            "yearRange" => "1900:+3"
                        ],
                    ])->textInput(['maxlength' => true])->label(false)
                    ?>

                </div>
                <div class="col-md-2 label-margin">
                    <?=
                    Yii::t("backend", "FROM COUPON");
                    ?>
                </div>
            </div>
            <!-- End Display Start Date -->

            <!-- Valid Period -->
            <div class="col-md-12"> 
                <div class="col-md-3 label-margin">  
                    <?=
                    Yii::t("backend", "Valid Period");
                    ?>
                </div> 
                <div class="col-md-3">
                    <?php $model->expire_date = Yii::$app->formatter->asDate($model->expire_date); ?>
                    <?=
                    $form->field($model, 'expire_date', ['template' => '<div class=" clear-padding">{input}{error}</div>'])->widget(DatePicker::classname(), [
                        'language' => 'ja',
                        'dateFormat' => 'yyyy/MM/dd',
                        'clientOptions' => [
                            "changeMonth" => true,
                            "changeYear" => true,
                            "yearRange" => "1900:+3"
                        ],
                    ])->textInput(['maxlength' => true])->label(false)
                    ?>
                </div>
                <div class="col-md-1 label-margin with-form-label-coupon-to">
                    <?=
                    Yii::t("backend", "TO COUPON");
                    ?>
                </div>
                <div class="col-md-3 font_label">
                    <?= $form->field($model, 'expire_auto_set')->checkbox() ?>   
                </div>
            </div>
            <!-- End Valid Period -->

            <!-- Display Coupon-->    
            <div class="col-md-12"> 
                <div class="col-md-3 label-margin">  
                    <?= Yii::t("backend", "Display Coupon"); ?>
                </div>
                <div class="col-md-9 with-form-label-coupon-show_coupon">
                    <?=
                    $form->field($model, 'show_coupon')->inline()->label(false)->checkboxList(Constants::Print_Receipt, ['prompt' => Yii::t('backend', 'Please Select'), 'id' => 'check_show_coupon', 'class' => 'font_label'])
                    ?>
                </div>
            </div>
            <!-- End Display Coupon-->   

            <!-- Code Membership -->
            <div class="col-md-12"> 
                <div class="col-md-3 label-margin">
                    <?= Yii::t("backend", "Code Membership"); ?>
                </div>
                <div class="col-md-3">
                    <?=
                    $form->field($model, 'code_membership')->widget(\yii\widgets\MaskedInput::className(), [
                        'clientOptions' => [
                            'alias' => 'integer',
                            'autoGroup' => true,
                            'removeMaskOnSubmit' => true,
                            'allowMinus' => false,
                        ]
                    ])->label(false)->textInput(['maxlength' => 13])
                    ?>
                </div>
            </div>
            <!-- End Code Membership -->

            <!--  Name and Name Kana -->    
            <div class="col-md-12"> 
                <div class="col-md-3 label-margin">
                    <?= Yii::t("backend", "Name"); ?>
                </div>
                <div class="col-md-1 label-margin">
                    <?= Yii::t("backend", "First Name"); ?>
                </div>
                <div class="col-md-2">
                    <?= $form->field($model, 'first_name')->textInput(['maxlength' => true])->label(false) ?>
                </div>
                <div class="col-md-1 label-margin">
                    <?= Yii::t("backend", "Last Name"); ?>
                </div>
                <div class="col-md-2">
                    <?= $form->field($model, 'last_name')->textInput(['maxlength' => true])->label(false) ?>
                </div>
            </div>
            <div class="col-md-12"> 
                <div class="col-md-3 label-margin">
                </div>
                <div class="col-md-1 label-margin">
                    <?= Yii::t("backend", "First Name Kana"); ?>
                </div>
                <div class="col-md-2">
                    <?= $form->field($model, 'first_name_kana')->textInput(['maxlength' => true])->label(false) ?>
                </div>
                <div class="col-md-1 label-margin">
                    <?= Yii::t("backend", "Last Name Kana"); ?>
                </div>
                <div class="col-md-2">
                    <?= $form->field($model, 'last_name_kana')->textInput(['maxlength' => true])->label(false) ?>
                </div>
            </div>
            <!--  End and Name Kana -->

            <!-- Sex -->
            <div class="col-md-12">
                <div class="col-md-3 label-margin">
                    <?= Yii::t("backend", "Sex"); ?>
                </div>
                <div class="col-md-3">
                    <?= $form->field($model, 'sex')->label(false)->dropDownList(Constants::LIST_SEX, ['prompt' => Yii::t('backend', 'Please Select')]) ?>
                </div>
            </div>
            <!-- End Sex -->

            <!-- Birth Date -->
            <div class="col-md-12"> 
                <div class="col-md-3 label-margin">  
                    <?=
                    Yii::t("backend", "Birth Date");
                    ?>
                </div> 
                <div class="col-md-3">
                    <?php $model->birthday_from = Yii::$app->formatter->asDate($model->birthday_from); ?>
                    <?=
                    $form->field($model, 'birthday_from')->widget(DatePicker::classname(), [
                        'language' => 'ja',
                        'dateFormat' => 'yyyy/MM/dd',
                        'clientOptions' => [
                            "changeMonth" => true,
                            "changeYear" => true,
                            "yearRange" => "1900:+0"
                        ],
                    ])->textInput(['maxlength' => true])->label(false)
                    ?>
                </div> 
                <div class="col-md-1 label-margin label-center date_"> ～ </div> 
                <div class="col-md-3"> 
                    <?php $model->birthday_to = Yii::$app->formatter->asDate($model->birthday_to); ?>
                    <?=
                    $form->field($model, 'birthday_to')->widget(DatePicker::classname(), [
                        'language' => 'ja',
                        'dateFormat' => 'yyyy/MM/dd',
                        'clientOptions' => [
                            "changeMonth" => true,
                            "changeYear" => true,
                            "yearRange" => "1900:+0"
                        ],
                    ])->textInput(['maxlength' => true])->label(false)
                    ?>
                </div> 
            </div>
            <!-- End Birth Date -->

            <!-- Last Visit Date From and Last Visit Date To-->
            <div class="col-md-12">
                <div class="col-md-3 label-margin">  
                    <?=
                    Yii::t("backend", "Last Store Visiting Date");
                    ?>
                </div> 
                <div class="col-md-3">
                    <?php $model->last_visit_date_from = Yii::$app->formatter->asDate($model->last_visit_date_from); ?>
                    <?=
                    $form->field($model, 'last_visit_date_from')->widget(DatePicker::classname(), [
                        'language' => 'ja',
                        'dateFormat' => 'yyyy/MM/dd',
                        'clientOptions' => [
                            "changeMonth" => true,
                            "changeYear" => true,
                            "yearRange" => "1900:+0"
                        ],
                    ])->textInput(['maxlength' => true])->label(false)
                    ?>
                </div> 
                <div class="col-md-1 label-margin label-center date_"> ～ </div> 
                <div class="col-md-3">
                    <?php $model->last_visit_date_to = Yii::$app->formatter->asDate($model->last_visit_date_to); ?>
                    <?=
                    $form->field($model, 'last_visit_date_to')->widget(DatePicker::classname(), [
                        'language' => 'ja',
                        'dateFormat' => 'yyyy/MM/dd',
                        'clientOptions' => [
                            "changeMonth" => true,
                            "changeYear" => true,
                            "yearRange" => "1900:+0"
                        ],
                    ])->textInput(['maxlength' => true])->label(false)
                    ?>
                </div> 
            </div>
            <!-- End Last Visit Date From and Last Visit Date To -->

            <!-- Number of store visiting -->
            <div class="col-md-12"> 
                <div class="col-md-3 label-margin">
                    <?= Yii::t("backend", "Number Of Store Visiting"); ?>
                </div>
                <div class="col-md-3">
                    <?=
                    $form->field($model, 'visit_number_min', [
                        'options' => ['class' => 'col-md-12 no-padding'],
                        'template' => '<div class=" clear-padding">{input}{error}</div>',
                        'inputTemplate' => '<div class="input-group">{input}<span class="input-group-addon"><b>回以上</b></span></div>',
                    ])->widget(\yii\widgets\MaskedInput::className(), [
                        'clientOptions' => [
                            'groupSeparator' => ',',
                            'alias' => 'integer',
                            'autoGroup' => true,
                            'removeMaskOnSubmit' => true,
                            'allowMinus' => false,
                        ]
                    ])->textInput(['maxlength' => true])
                    ?>
                </div>
                <div class="col-md-3" style="margin-left: 2.1em;">
                    <?=
                    $form->field($model, 'visit_number_max', [
                        'options' => ['class' => 'col-md-12 no-padding'],
                        'template' => '<div class=" clear-padding">{input}{error}</div>',
                        'inputTemplate' => '<div class="input-group">{input}<span class="input-group-addon"><b>回以下</b></span></div>',
                    ])->widget(\yii\widgets\MaskedInput::className(), [
                        'clientOptions' => [
                            'groupSeparator' => ',',
                            'alias' => 'integer',
                            'autoGroup' => true,
                            'removeMaskOnSubmit' => true,
                            'allowMinus' => false,
                        ]
                    ])->textInput(['maxlength' => true])
                    ?>
                </div>
            </div>
            <!-- End Number of store visiting -->

            <!-- Distributing Store -->
            <div class="col-md-12"> 
                <div class="col-md-3 label-margin required-star">
                    <?= Yii::t("backend", "Distributing Store"); ?>
                </div>
                <div class="col-md-3">
                    <?php
                    $role = \common\components\FindPermission::getRole();
                    if ($role->permission_id == Constants::STORE_MANAGER_ROLE || $role->permission_id == Constants::STAFF_ROLE) {
                        echo $form->field($model, 'store_id')->label(false)->dropDownList($searchListStore);
                    } else {
                        echo $form->field($model, 'store_id')->label(false)->dropDownList($searchListStore, ['prompt' => Yii::t('backend', 'Please Select'), 'id' => 'mastercoupon-store_id']);
                    }
                    ?>
                </div>
            </div>
            <!-- End Distributing Store-->

            <!-- Rank customer-->
            <div class="col-md-12"> 
                <div class="col-md-3 label-margin">
                    <?= Yii::t("backend", "Rank Customer"); ?>
                </div>
                <div class="col-md-3">
                    <?= $form->field($model, 'rank_id')->label(false)->dropDownList(Constants::RANK, ['prompt' => Yii::t('backend', 'Please Select')]) ?>
                </div>
            </div>
            <!-- Rank customer -->

            <!-- Sex and Staff -->
            <div class="col-md-12">
                <div class="col-md-3 label-margin">
                    <?= Yii::t("backend", "Last time staff"); ?>
                </div>
                <div class="col-md-3">

                    <?=
                    $form->field($model, 'last_staff_id')->label(false)->widget(DepDrop::classname(), [
                        'options' => ['id' => 'mastercoupon-last_staff_id'],
                        'pluginOptions' => [
                            //'init' => true,
                            'depends' => ['mastercoupon-store_id'],
                            'placeholder' => ' ',
                            'url' => Url::to(['/master-coupon/selectstaffbyidstore'])
                        ]
                    ]);
                    ?>

                </div>
            </div>
            <!-- End Sex and Staff -->

            <!-- Black list -->
            <div class="col-md-12">
                <div class="col-md-8 label-margin font_label">
                    <?= $form->field($model, 'black_list_flg')->checkbox() ?>   
                </div>
            </div>
            <!-- End Black list -->
        </div>
    </div>
    <!-- ============================== -->
    <!-- ====== End Coupon Infomation = -->
    <!-- ============================== -->
    <!-- ============================== -->
    <!-- ====== Input Benefit Info ==== -->
    <!-- ============================== -->
    <div class="box-header with-border common-box-h4 col-md-6 ">
        <h4 class="text-title"><b><?= Yii::t('backend', 'Input Benefit Info') ?></b></h4>
    </div>
    <div class="col-md-12">
        <div class="master-coupon-form">
            <!-- Benefits Content Coupon -->
            <div class="col-md-12 clear_error">
                <div class="col-md-3 font_label">
                    <?= Yii::t("backend", "Benefits Content Coupon"); ?>
                </div>
                <div class="col-md-2 font_label">
                    <?= $form->field($model, 'benefits_content')->radioList(Constants::BENEFIT_CONTENTS, ['separator' => '<br>', 'itemOptions' => ['class' => 'radio-coupon']])->label(false) ?>
                </div>
                <div class="col-md-6">
                    <div class="col-md-4 label-margin_price">
                        <?=
                        $form->field($model, 'discount_yen', ['template' => '<div class="clear-padding">{input}</div>'])->widget(\yii\widgets\MaskedInput::className(), [
                            'clientOptions' => [
                                'groupSeparator' => ',',
                                'alias' => 'integer',
                                'autoGroup' => true,
                                'removeMaskOnSubmit' => true,
                                'allowMinus' => false,
                            ]
                        ])->textInput(['disabled' => 'disabled', 'maxlength' => 13])->label(false)
                        ?>
                    </div>
                    <div class="col-md-2 label-margin">
                        <?= Yii::t("backend", "Discount Circle"); ?>
                    </div>
                    <div class="col-md-4">
                        <?= $form->field($model, 'discount_yen', ['template' => '<div class=" clear-padding">{error}</div>'])->label(false) ?>
                    </div>
                </div> 
                <div class="col-md-6">
                    <div class="col-md-4 label-margin_price">
                        <?=
                        $form->field($model, 'discount_percent', ['template' => '<div class="clear-padding">{input}</div>'])->widget(\yii\widgets\MaskedInput::className(), [
                            'clientOptions' => [
                                'groupSeparator' => ',',
                                'alias' => 'integer',
                                'autoGroup' => true,
                                'removeMaskOnSubmit' => true,
                                'allowMinus' => false,
                            ]
                        ])->textInput(['disabled' => 'disabled', 'maxlength' => 2])->label(false)
                        ?>
                    </div>
                    <div class="col-md-2 label-margin">
                        <?= Yii::t("backend", "Discount Pull"); ?>
                    </div>
                    <div class="col-md-4">
                        <?= $form->field($model, 'discount_percent', ['template' => '<div class=" clear-padding">{error}</div>'])->label(false) ?>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="col-md-4 label-margin_price">
                        <?=
                        $form->field($model, 'discount_price_set', ['template' => '<div class="clear-padding">{input}</div>'])->widget(\yii\widgets\MaskedInput::className(), [
                            'clientOptions' => [
                                'groupSeparator' => ',',
                                'alias' => 'integer',
                                'autoGroup' => true,
                                'removeMaskOnSubmit' => true,
                                'allowMinus' => false,
                            ]
                        ])->textInput(['disabled' => 'disabled', 'maxlength' => 13])->label(false)
                        ?>

                    </div>
                    <div class="col-md-1 label-margin" style="margin-top: 1em;">
                        <?= Yii::t("backend", "Price Set"); ?>
                    </div>
                    <div class="col-md-3" style="margin-top: 0.5em;">
                        <?= $form->field($model, 'discount_price_set_tax_type')->dropDownList(Constants::LIST_TAX_INTERNAL_FOREIGN, ['disabled' => 'disabled'])->label(false) ?>
                    </div>
                    <div class="col-md-4">
                        <?= $form->field($model, 'discount_price_set', ['template' => '<div class=" clear-padding">{error}</div>'])->label(false) ?>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="col-md-4 label-margin_price ">
                        <?=
                        $form->field($model, 'discount_drink_eat', ['template' => '<div class="clear-padding">{input}</div>'])->widget(\yii\widgets\MaskedInput::className(), [
                            'clientOptions' => [
                                'groupSeparator' => ',',
                                'alias' => 'integer',
                                'autoGroup' => true,
                                'removeMaskOnSubmit' => true,
                                'allowMinus' => false,
                            ]
                        ])->textInput(['disabled' => 'disabled', 'maxlength' => 13])->label(false)
                        ?>
                    </div>
                    <div class="col-md-1 label-margin" style="margin-top: 1em;">
                        <?= Yii::t("backend", "Drink Eat"); ?>
                    </div>
                    <div class="col-md-3" style="margin-top: 0.5em;">
                        <?= $form->field($model, 'discount_drink_eat_tax_type')->dropDownList(Constants::LIST_TAX_INTERNAL_FOREIGN, ['disabled' => 'disabled'])->label(false) ?>
                    </div>
                    <div class="col-md-4">
                        <?= $form->field($model, 'discount_drink_eat', ['template' => '<div class=" clear-padding">{error}</div>'])->label(false) ?>
                    </div>
                </div>
            </div>
            <!-- End Content Coupon -->
            <!-- Poin-->
            <div class="col-md-12">
                <div class="col-md-3 label-margin">
                    <?= Yii::t("backend", "Grant Point Cuopon"); ?>
                </div>
                <div class="col-md-3">
                    <?=
                    $form->field($model, 'grant_point')->widget(\yii\widgets\MaskedInput::className(), [
                        'clientOptions' => [
                            'groupSeparator' => ',',
                            'alias' => 'integer',
                            'autoGroup' => true,
                            'removeMaskOnSubmit' => true,
                            'allowMinus' => false,
                        ]
                    ])->textInput(['maxlength' => 13])->label(false)
                    ?>
                </div>
                <div class="col-md-3 label-margin">
                    <?= Yii::t("backend", "Poin"); ?>
                </div>
            </div>
            <!-- End Poin-->
        </div>
    </div>
    <!-- ============================== -->
    <!-- == End Input Benefit Info ==== -->
    <!-- ============================== -->
    <!-- ============================== -->
    <!--  Selection Product Object ==== -->
    <!-- ============================== -->

    <?= $form->field($model, 'option_hidden')->hiddenInput(['id' => 'option_hidden'])->label(FALSE) ?>
    <?php Pjax::begin(['enablePushState' => false, 'id' => 'boxPajax']); ?>
    <div class="box-header with-border common-box-h4 col-md-6 ">
        <h4 class="text-title"><b><?= Yii::t('backend', 'Selection Product Object') ?></b></h4>
    </div>
    <div class="col-md-8">
        <div class="form-group">
            <?=
            Html::a(Yii::t('backend', 'Add Product Object'), ['#'], [
                'class' => 'btn common-button-submit',
                'id' => 'btn-option-product',
                'onclick' => 'init_product_slect_by_idstrore(event)'
                    //'onclick' => 'init_product_slect(event)'
            ])
            ?>
        </div>
        <?php
        Modal::begin([
            'id' => 'productModal',
            'size' => 'SIZE_LARGE',
            'header' => '<div class="box-header with-border common-box-h4 col-md-8"><h4 class="text-title"><b>' . Yii::t('backend', 'Product selection') . '</b><h4></div>',
            'footer' => Html::button(Yii::t('backend', 'Close'), ['class' => 'btn common-button-default', 'data-dismiss' => "modal", 'id' => 'clear-option-select'])
            . Html::button(Yii::t('backend', 'Choice'), ['class' => 'btn common-button-submit', 'data-dismiss' => "modal", 'id' => 'save-option-select']),
            'footerOptions' => [
                'class' => 'text-center',
            ],
            'closeButton' => FALSE,
            'options' => [
                'data-backdrop' => 'static',
                'data-keyboard' => 'false',
            ]
        ]);
        ?>  
        <div class="" id="_product">

        </div>
        <?php Modal::end(); ?>
        <div class="col-md-12">

            <?= $form->field($model, 'error_product_tax', ['template' => '<div class=" clear-padding">{error}{input}</div>'])->hiddenInput() ?>
        </div>
        <?=
        GridView::widget([
            'dataProvider' => $list_product_select,
            'layout' => "{pager}\n{items}",
            'summary' => false,
            'pager' => [
                'firstPageLabel' => '<<',
                'lastPageLabel' => '>>',
                'prevPageLabel' => '<',
                'nextPageLabel' => '>',
                'options' => ['class' => 'pagination common-float-right'],
            ],
            'tableOptions' => [
                'class' => 'table table-striped table-bordered text-center',
                'id' => 'coupon_table',
            ],
            'columns' => [
                [
                    'attribute' => 'name',
                    'label' => Yii::t('backend', 'Product name'),
                    'headerOptions' => [
                        'class' => ['col-md-3']
                    ]
                ],
                [
                    'attribute' => 'unit_price',
                    'headerOptions' => [
                        'class' => ['col-md-2']
                    ],
                    'value' => function ($model) {
                if ($model->unit_price == null || empty($model->unit_price)) {
                    return '¥0';
                } else {
                    return '¥' . Yii::$app->formatter->asDecimal($model->unit_price, 0);
                }
            },
                ],
            ],
        ]);
        ?>
        <?php //if ($model->isNewRecord) { ?>
        <?php //Html::a(Yii::t('backend', 'Refrest'), ['create'], ['style' => 'display: none', 'id' => 'refresh-data-modal', 'push' => 'false']) ?>
        <?php //} else {  ?>
        <?php //Html::a(Yii::t('backend', 'Refrest'), ['update', 'id' => $model->id], ['style' => 'display: none', 'id' => 'refresh-data-modal', 'push' => 'false', 'value' => 'update']) ?>
        <?php //}  ?>
        <?= Html::a(Yii::t('backend', 'Refrest'), Url::current(), ['style' => 'display: none', 'id' => 'refresh-data-modal', 'push' => 'false']) ?>
    </div>
    <?php Pjax::end(); ?>

    <!-- ============================== -->
    <!--  End Selection Product Object= -->
    <!-- ============================== -->
    <!-- ============================== -->
    <!--  Input Coupon Informatica    = -->
    <!-- ============================== -->
    <div class="box-header with-border common-box-h4 col-md-6 ">
        <h4 class="text-title"><b><?= Yii::t('backend', 'Input Coupon Info') ?></b></h4>
    </div>
    <!-- Upload Image -->
    <div class="col-md-12">
        <div class="col-md-2 label-margin">
            <?= Yii::t('backend', 'Image Coupon') ?>
        </div>
        <div class="col-md-4 label-margin">
            <?=
            $form->field($model, 'tmp_image')->label(false)->widget(
                    FileInput::classname(), [
                'options' => ['accept' => 'image/*'],
                'pluginOptions' => [
                    'allowedFileExtensions' => ['jpg', 'gif', 'png', 'jpeg'],
                    'maxFileSize' => 2048,
                    'initialPreview' => [
                        Html::img(Util::getUrlImage($model->image))
                    ],
                    'overwriteInitial' => true,
                    'showUpload' => false,
                    'showCaption' => false,
                    'browseLabel' => Yii::t('backend', 'Select an image'),
                    'fileActionSettings' => [
                        'showZoom' => true,
                    ]
                ],
                'pluginEvents' => [
                    "fileclear" => 'function() { $(this).parent().parent().parent().parent().find(".hidden_image").val("") }',
                ],
            ]);
            ?>

        </div>
    </div>
    <!-- End Upload Image -->
    <!-- Title, Font Size, Display Location -->
    <div class="col-md-12">
        <div class="col-md-2 label-margin">
        </div>
        <div class="col-md-2 label-margin">
        </div>
        <div class="col-md-1 label-margin">
        </div>
        <div class="col-md-2 label-margin" style="text-align: center;">
            <?= Yii::t('backend', 'Title Font Size') ?>
        </div>
        <div class="col-md-2 label-margin" style="text-align: center;">
            <?= Yii::t('backend', 'Title Postion') ?>
        </div>
    </div>
    <div class="col-md-12">
        <div class="col-md-2 label-margin">
            <?= Yii::t('backend', 'Title') ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'title')->textInput(['maxlength' => 40])->label(false) ?>
        </div>
        <div class="col-md-2">
            <?= $form->field($model, 'title_font_size')->label(false)->dropDownList(Constants::SIZE) ?>
        </div>
        <div class="col-md-2">
            <?= $form->field($model, 'title_postion')->label(false)->dropDownList(Constants::POSITIONS) ?>
        </div>
    </div>
    <div class="col-md-12">
        <div class="col-md-2 label-margin">
            <?= Yii::t('backend', 'Introduction 1') ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'introduction_1')->textInput(['maxlength' => true])->label(false) ?>
        </div>
        <div class="col-md-2">
            <?= $form->field($model, 'introduction_font_size_1')->label(false)->dropDownList(Constants::SIZE) ?>
        </div>
        <div class="col-md-2">
            <?= $form->field($model, 'introduction_postion_1')->label(false)->dropDownList(Constants::POSITIONS) ?>
        </div>
    </div>
    <div class="col-md-12">
        <div class="col-md-2 label-margin">
            <?= Yii::t('backend', 'Introduction 2') ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'introduction_2')->textInput(['maxlength' => true])->label(false) ?>
        </div>
        <div class="col-md-2">
            <?= $form->field($model, 'introduction_font_size_2')->label(false)->dropDownList(Constants::SIZE) ?>
        </div>
        <div class="col-md-2">
            <?= $form->field($model, 'introduction_postion_2')->label(false)->dropDownList(Constants::POSITIONS) ?>
        </div>
    </div>
    <div class="col-md-12">
        <div class="col-md-2 label-margin">
            <?= Yii::t('backend', 'Introduction 3') ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'introduction_3')->textInput(['maxlength' => true])->label(false) ?>
        </div>
        <div class="col-md-2">
            <?= $form->field($model, 'introduction_font_size_3')->label(false)->dropDownList(Constants::SIZE) ?>
        </div>
        <div class="col-md-2">
            <?= $form->field($model, 'introduction_postion_3')->label(false)->dropDownList(Constants::POSITIONS) ?>
        </div>
    </div>
    <!-- End Title, Font Size, Display Location -->
    <!-- Detail Description-->
    <div class="col-md-12">
        <div class="col-md-2 label-margin">
            <?= Yii::t('backend', 'Coupon Deatails Description') ?>
        </div>
        <div class="col-md-5">
            <?= $form->field($model, 'coupon_details_description')->label(false)->textarea(['rows' => 4, 'maxlength' => 500]) ?>
        </div>
    </div>    
    <!-- End Detail Description-->
    <!-- Display Code -->
    <div class="col-md-12">
        <div class="col-md-6 label-margin font_label">
            <?= $form->field($model, 'display_barcode_flg')->checkbox() ?>
        </div>
    </div>
    <div class="col-md-12">
        <div class="col-md-6 font_label">
            <?= $form->field($model, 'combine_with_other_coupon_flg')->checkbox() ?>
        </div>
    </div>
    <!-- End Display Code -->    
    <!-- ============================== -->
    <!--  End Input Coupon Informatica= -->
    <!-- ============================== -->    
    <!-- ============================== -->
    <!--  Button                      = -->
    <!-- ============================== -->
    <div class="col-md-12">
        <?= Html::a(Yii::t('backend', 'Return'), ['index'], ['class' => 'btn btn-default common-button-default']) ?>
        <?=
        Html::submitButton(Yii::t('backend', 'Confirm'), [
            'class' => 'btn common-button-submit',
            'id' => 'btn-confirm',
        ])
        ?>
        <?=
        Html::button(Yii::t('backend', 'Preview Coupon'), [
            'class' => 'btn common-button-submit',
            'id' => 'btn-confirm-preview',
            'data' => [
                'toggle' => 'modal',
                'target' => '#userModalConfirm',
            ],
        ])
        ?>
    </div>
    <!-- ============================== -->
    <!--  End Button                  = -->
    <!-- ============================== --> 
    <!-- End Datpdt 20/09/2016 -->       
    <?php
    Modal::begin([
        'id' => 'userModalConfirm',
        'size' => 'SIZE_LARGE',
        'header' => '<div class="box-header with-border common-box-h4 col-md-8"><h4 class="text-title"><b>' . Yii::t('backend', 'Confirm Store') . '</b></h4></div>',
        'footer' => Html::button(Yii::t('backend', 'Close'), ['class' => 'btn common-button-default', 'data-dismiss' => "modal", 'id' => 'btn-close2']),
        'footerOptions' => ['class' => 'modal-footer text-center'],
    ]);
    ?>

    <?=
    PreviewCoupon::widget([
        "data" => [
            'title' => [
                'type' => 'input_coupon',
                'label' => Yii::t('backend', ' ')
            ],
            'introduction_1' => [
                'type' => 'input_coupon',
                'label' => Yii::t('backend', ' ')
            ],
            'introduction_2' => [
                'type' => 'input_coupon',
                'label' => Yii::t('backend', ' ')
            ],
            'introduction_3' => [
                'type' => 'input_coupon',
                'label' => Yii::t('backend', ' ')
            ],
            'tmp_image' => [
                'type' => 'image',
                'label' => Yii::t('backend', ' ')
            ],
        ],
        "modelName" => $model->formName(),
        'idBtnConfirm' => 'btn-confirm-preview',
        'btnClose' => 'btn-close',
        'btnSubmit' => 'btn-submit',
        'modalId' => 'userModalConfirm'
    ])
    ?>

    <?php
    Modal::end();
    ?> 
    <?php
    Modal::begin([
        'id' => 'userModal',
        'size' => 'SIZE_LARGE',
        'header' => '<div class="box-header with-border common-box-h4 col-md-8"><h4 class="text-title"><b>' . Yii::t('backend', 'Confirm input') . '</b></h4></div>',
        'footer' => Html::button(Yii::t('backend', 'Close'), ['class' => 'btn common-button-default', 'data-dismiss' => "modal", 'id' => 'btn-close'])
        . Html::submitButton(Yii::t('backend', 'Save'), ['class' => 'btn common-button-submit', 'id' => 'btn-submit']),
        'closeButton' => FALSE,
    ]);
    ?>

    <?=
    Preview::widget([
        "data" => [
            'coupon_jan_code' => [
                'type' => 'input_coupon',
                'label' => Yii::t('backend', 'Coupon Jan Code')
            ],
            'start_date' => [
                'type' => 'input_coupon_date',
                'start_date_coupon' => true,
                'label' => Yii::t('backend', 'Display Start Date')
            ],
            'expire_date' => [
                'type' => 'input_coupon_date',
                'expire_date_coupon' => true,
                'label' => Yii::t('backend', 'Valid Period')
            ],
            'show_coupon' => [
                'type' => 'checkbox',
                'show_coupon_check' => true,
                'label' => Yii::t('backend', 'Show Coupon')
            ],
            'code_membership' => [
                'type' => 'input_coupon',
                'label' => Yii::t('backend', 'Code Membership')
            ],
            'first_name' => [
                'type' => 'input_coupon',
                'groupWith' => 'last_name',
                'label' => Yii::t('backend', 'Name'),
            ],
            'sex' => [
                'type' => 'select',
                'label' => Yii::t('backend', 'Sex Coupon')
            ],
            'birthday_from' => [
                'type' => 'input',
                'groupWithSeat' => 'birthday_to',
                'label' => Yii::t('backend', 'Birthday From')
            ],
            'last_visit_date_from' => [
                'type' => 'input',
                'groupWithSeat' => 'last_visit_date_to',
                'label' => Yii::t('backend', 'Last Visit Date From')
            ],
            'visit_number_min' => [
                'type' => 'input',
                'groupWithSeat' => 'visit_number_max',
                'label' => Yii::t('backend', 'Visit number min')
            ],
            'store_id' => [
                'type' => 'select',
                'label' => Yii::t('backend', 'Distributing Store')
            ],
            'rank_id' => [
                'type' => 'select',
                'label' => Yii::t('backend', 'Rank Customer')
            ],
            'last_staff_id' => [
                'type' => 'select',
                'label' => Yii::t('backend', 'Last time staff')
            ],
            'black_list_flg' => [
                'type' => 'checkbox',
                'label' => Yii::t('backend', 'Black List Cuopon')
            ],
            'benefits_content' => [
                'type' => 'radio',
                'label' => Yii::t('backend', 'Benefits Content Coupon')
            ],
            'grant_point' => [
                'type' => 'input_coupon',
                'label' => Yii::t('backend', 'Grant Point Cuopon')
            ],
            'list_product' => [
                'type' => '',
                'label' => Yii::t('backend', 'List Product')
            ],
            'coupon_table' => [
                'type' => 'table-normal',
                'column' => '"0,1"',
                'table' => 'coupon_table',
            ],
            'tmp_image' => [
                'type' => 'image',
                'label' => Yii::t('backend', 'Image Coupon')
            ],
            'title' => [
                'type' => 'input_coupon',
                'label' => Yii::t('backend', 'Title coupon')
            ],
            'introduction_1' => [
                'type' => 'input_coupon',
                'label' => Yii::t('backend', 'Introduction 1')
            ],
            'introduction_2' => [
                'type' => 'input_coupon',
                'label' => Yii::t('backend', 'Introduction 2')
            ],
            'introduction_3' => [
                'type' => 'input_coupon',
                'label' => Yii::t('backend', 'Introduction 3')
            ],
            'coupon_details_description' => [
                'type' => 'input_coupon',
                'label' => Yii::t('backend', 'Coupon Deatails Description')
            ],
            'display_barcode_flg' => [
                'type' => 'checkbox',
                'label' => Yii::t('backend', 'Display barcode flg')
            ],
            'combine_with_other_coupon_flg' => [
                'type' => 'checkbox',
                'label' => Yii::t('backend', 'Combine with other coupon flg')
            ]
        ],
        "modelName" => $model->formName(),
        'idBtnConfirm' => 'btn-confirm',
        'formId' => strtolower($model->formName()) . '-id',
        'btnClose' => 'btn-close',
        'btnSubmit' => 'btn-submit',
        'modalId' => 'userModal'
    ])
    ?>

    <?php
    Modal::end();
    ?>   
    <?php ActiveForm::end(); ?>    

</div>
<style>

/*    .panel-heading .collapse-toggle:after {
         symbol for "opening" panels 
        font-family: 'Glyphicons Halflings';   essential for enabling glyphicon 
        content: "\e114";     adjust as needed, taken from bootstrap.css 
        float: right;         adjust as needed 
        color: grey;          adjust as needed 
    }
    .panel-heading .collapse-toggle.collapsed:after {
         symbol for "collapsed" panels 
        content: "\e080";     adjust as needed, taken from bootstrap.css 
    }*/
  .panel-heading .collapse-toggle[aria-expanded=true]:after {
    /* symbol for "opening" panels */
    font-family: 'Glyphicons Halflings';  /* essential for enabling glyphicon */
    content: "\e114";    /* adjust as needed, taken from bootstrap.css */
    float: right;        /* adjust as needed */
    color: grey;         /* adjust as needed */
  }
  .panel-heading .collapse-toggle.collapsed:after ,.panel-heading .collapse-toggle:after {
    /* symbol for "collapsed" panels */
    font-family: 'Glyphicons Halflings'; 
     float: right;        /* adjust as needed */
    color: grey;         /* adjust as needed */
    content: "\e080";    /* adjust as needed, taken from bootstrap.css */
  }
  .panel-heading .collapse-toggle {
      width: 100%;
      display: inherit;
  }
</style>
<script>
    $(document).ready(function () {

        var idStore = $('#mastercoupon-store_id').val();

        if (idStore) {
            //$("#btn-option").attr("disabled", true).addClass('disabled');
        } else {
            // $("#btn-option-product").attr("disabled", true).addClass('disabled');
        }

        var i = 0;

        // ===========================================
        // Onclick select list store
        // ===========================================
        $('#mastercoupon-store_id').change(function () {

            if (i == 1) {
                $(".field-mastercoupon-error_product_tax .help-block-error").text('');
                var value = $(this).val();
                if (value) {

                    //$("#btn-option-product").removeClass('disabled').removeAttr("disabled");

                    $("#option_hidden").val('');

                    $("#coupon_table").find('tbody').html('');

                    var $options = $('.option-checkbox');
                    var $option_hidden = $('#option_hidden').val();
                    var $option_hidden = ($option_hidden) ? $option_hidden.split(",") : $option_hidden;
                    $options.each(function () {
                        if ($.inArray($(this).val(), $option_hidden) >= 0) {
                            $(this).prop('checked', true);
                        } else {
                            $(this).prop('checked', false);
                        }
                    });

                } else {
                    //$("#btn-option-product").attr("disabled", true).addClass('disabled');
                }

            } else {
                i = 1;
            }

        });
        // ===========================================
        // End Onclick select list store
        // ===========================================
        $('#mastercoupon-last_staff_id').on('depdrop.init', function (event) {
            $('#mastercoupon-store_id').trigger('change');
        });

        $('#mastercoupon-last_staff_id').on('depdrop.afterChange', function (event, id, value) {
            $('#mastercoupon-last_staff_id').val(<?php echo $model->last_staff_id; ?>);
        });
        //=========================================
        // Delete image zoom
        //=========================================
        $("#btn-confirm").click(function () {
            $(".file-zoom-detail").remove();
        });

        $("#btn-confirm-preview").click(function () {
            $(".file-zoom-detail").remove();

        });

        // ====================================
        // Check 内税 AND 外税
        // ====================================

        // ====================================
        // Check expire_auto_set when  = 1 start
        // ====================================
        if ($('#mastercoupon-expire_auto_set').is(':checked')) {
            $("#mastercoupon-expire_date").addClass("form-control");
            $("#mastercoupon-expire_date").attr("disabled", true).addClass('disabled');
        }
        // ====================================
        // Check expire_auto_set when  = 1 end
        // ====================================
    });


</script>
