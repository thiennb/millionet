<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\jui\DatePicker;
use common\components\Constants;
use yii\helpers\Url;
// Drop List
use kartik\depdrop\DepDrop;

/* @var $this yii\web\View */
/* @var $model common\models\MasterCouponSeach */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="master-coupon-search col-md-8">

    <?php
    $form = ActiveForm::begin([
                'action' => ['index'],
                'method' => 'get',
                'enableClientValidation' => false,
    ]);
    ?>
    <!-- Date to and date from -->
    <div class="col-md-12"> 
        <div class="col-md-2 label-margin">  
            <?=
            Yii::t("backend", "Display Start Date");
            ?>
        </div> 
        <div class="col-md-3">
            <?=
            $form->field($model, 'coupon_date_from', ['template' => '<div class=" clear-padding">{input}{error}</div>'])->widget(DatePicker::classname(), [
                'language' => 'ja',
                'dateFormat' => 'yyyy/MM/dd',
                'clientOptions' => [
                    "changeMonth" => true,
                    "changeYear" => true,
                    "yearRange" => "1900:+0"
                ],
            ])->textInput(['maxlength' => 10])
            ?>
        </div> 
        <div class="col-md-1 label-margin label-center date_ separator">～</div> 
        <div class="col-md-3"> 
            <?=
            $form->field($model, 'coupon_date_to', ['template' => '<div class=" clear-padding">{input}{error}</div>'])->widget(DatePicker::classname(), [
                'language' => 'ja',
                'dateFormat' => 'yyyy/MM/dd',
                'clientOptions' => [
                    "changeMonth" => true,
                    "changeYear" => true,
                    "yearRange" => "1900:+0"
                ],
            ])->textInput(['maxlength' => 10])
            ?>
        </div> 
    </div>
    <!-- End Date to and date from -->
    <!-- Display Coupon-->    
    <div class="col-md-12"> 
        <div class="col-md-2 label-margin">  
            <?= Yii::t("backend", "Display Coupon"); ?>
        </div>
        <div class="col-md-9 font_label">
            <?=
            $form->field($model, 'show_coupon')->inline()->label(false)->checkboxList(Constants::Print_Receipt)
            ?>
        </div>
    </div>
    <!-- End Display Coupon-->

    <!-- Code Membership and Distributing Store-->
    <div class="col-md-12"> 
        <div class="col-md-2 label-margin">
            <?= Yii::t("backend", "Code Membership"); ?>
        </div>
        <div class="col-md-3">
            <?=
            $form->field($model, 'code_membership')->widget(\yii\widgets\MaskedInput::className(), [
                'clientOptions' => [
                    'alias' => 'integer',
                    'autoGroup' => true,
                    'removeMaskOnSubmit' => true,
                    'allowMinus' => false,
                ]
            ])->label(false)->textInput(['maxlength' => 13])
            ?>
        </div>
        <div class="col-md-2 label-margin magin-left-coupon">
            <?= Yii::t("backend", "Distributing Store"); ?>
        </div>
        <div class="col-md-3">
            <?php
            $role = \common\components\FindPermission::getRole();
            if ($role->permission_id == Constants::STORE_MANAGER_ROLE || $role->permission_id == Constants::STAFF_ROLE){
               echo $form->field($model, 'store_id')->label(false)->dropDownList($searchListStore);
            }else{
               echo  $form->field($model, 'store_id')->label(false)->dropDownList($searchListStore, ['prompt' => Yii::t('backend', 'Please Select'), 'id' => 'mastercoupon-store_id']);
            }
            ?>
            
        </div>
    </div>
    <!-- End Code Membership and Distributing Store-->

    <!-- Name and Rank customer-->
    <div class="col-md-12"> 
        <div class="col-md-2 label-margin">
            <?= Yii::t("backend", "Name"); ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'name_customer')->label(false)->textInput(['maxlength' => 80]) ?>
        </div>
        <div class="col-md-2 label-margin magin-left-coupon">
            <?= Yii::t("backend", "Rank Customer"); ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'rank_customer')->label(false)->dropDownList(Constants::RANK, ['prompt' => Yii::t('backend', 'Please Select')]) ?>
        </div>
    </div>
    <!-- End Name and Rank customer -->

    <!-- Sex and Staff -->
    <div class="col-md-12"> 
        <div class="col-md-2 label-margin">
            <?= Yii::t("backend", "Sex"); ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'sex')->label(false)->dropDownList(Constants::LIST_SEX, ['prompt' => Yii::t('backend', 'Please Select')]) ?>
        </div>
        <div class="col-md-2 label-margin magin-left-coupon" >
            <?= Yii::t("backend", "Last time staff"); ?>
        </div>
        <div class="col-md-3">
            <?=
            $form->field($model, 'last_staff_id')->label(false)->widget(DepDrop::classname(), [
                'options' => ['id' => 'last_staff_id'],
                'pluginOptions' => [
                    //'init' => true,
                    'depends' => ['mastercoupon-store_id'],
                    'placeholder' => ' ',
                    'url' => Url::to(['/master-coupon/selectstaffbyidstore'])
                ]
            ]);
            ?>
        </div>
    </div>
    <!-- End Sex and Staff -->

    <!-- Number of store visiting -->
    <div class="col-md-12"> 
        <div class="col-md-2 label-margin">
            <?= Yii::t("backend", "Number Of Store Visiting"); ?>
        </div>
        <div class="col-md-3">
            <?=
            $form->field($model, 'visit_number_min', [
                'options' => ['class' => 'col-md-9 no-padding'],
                'template' => '<div class=" clear-padding">{input}{error}</div>',
                'inputTemplate' => '<div class="input-group">{input}<span class="input-group-addon"><b>回以上</b></span></div>',
            ])->widget(\yii\widgets\MaskedInput::className(), [
                'clientOptions' => [
                    'groupSeparator' => ',',
                    'alias' => 'integer',
                    'autoGroup' => true,
                    'removeMaskOnSubmit' => true,
                    'allowMinus' => false,
                ]
            ])->textInput(['maxlength' => true, 'style' => 'width: 10.6em;'])
            ?>
        </div>
        <div class="col-md-3">
            <?=
            $form->field($model, 'visit_number_max', [
                'options' => ['class' => 'col-md-9 no-padding'],
                'template' => '<div class=" clear-padding">{input}{error}</div>',
                'inputTemplate' => '<div class="input-group">{input}<span class="input-group-addon"><b>回以下</b></span></div>',
            ])->widget(\yii\widgets\MaskedInput::className(), [
                'clientOptions' => [
                    'groupSeparator' => ',',
                    'alias' => 'integer',
                    'autoGroup' => true,
                    'removeMaskOnSubmit' => true,
                    'allowMinus' => false,
                ]
            ])->textInput(['maxlength' => true, 'style' => 'width: 10.6em;margin-left: 2em;'])
            ?>
        </div>
    </div>

    <!-- End Number of store visiting -->

    <!-- Birth Date -->
    <div class="col-md-12"> 
        <div class="col-md-2 label-margin">  
            <?=
            Yii::t("backend", "Birth Date");
            ?>
        </div> 
        <div class="col-md-3">
            <?=
            $form->field($model, 'birthday_from', ['template' => '<div class=" clear-padding">{input}{error}</div>'])->widget(DatePicker::classname(), [
                'language' => 'ja',
                'dateFormat' => 'yyyy/MM/dd',
                'clientOptions' => [
                    "changeMonth" => true,
                    "changeYear" => true,
                    "yearRange" => "1900:+0"
                ],
            ])->textInput(['maxlength' => 10])
            ?>
        </div> 
        <div class="col-md-1 label-margin label-center date_ separator">～</div> 
        <div class="col-md-3"> 
            <?=
            $form->field($model, 'birthday_to', ['template' => '<div class=" clear-padding">{input}{error}</div>'])->widget(DatePicker::classname(), [
                'language' => 'ja',
                'dateFormat' => 'yyyy/MM/dd',
                'clientOptions' => [
                    "changeMonth" => true,
                    "changeYear" => true,
                    "yearRange" => "1900:+0"
                ],
            ])->textInput(['maxlength' => 10])
            ?>
        </div> 
    </div>
    <!-- End Birth Date -->

    <!-- Last Visit Date From and Last Visit Date To-->
    <div class="col-md-12">
        <div class="col-md-2 label-margin">  
            <?=
            Yii::t("backend", "Last Store Visiting Date");
            ?>
        </div> 
        <div class="col-md-3">
            <?=
            $form->field($model, 'last_visit_date_from', ['template' => '<div class=" clear-padding">{input}{error}</div>'])->widget(DatePicker::classname(), [
                'language' => 'ja',
                'dateFormat' => 'yyyy/MM/dd',
                'clientOptions' => [
                    "changeMonth" => true,
                    "changeYear" => true,
                    "yearRange" => "1900:+0"
                ],
            ])->textInput(['maxlength' => 10])
            ?>
        </div> 
        <div class="col-md-1 label-margin label-center date_ separator">～</div> 
        <div class="col-md-3"> 
            <?=
            $form->field($model, 'last_visit_date_to', ['template' => '<div class=" clear-padding">{input}{error}</div>'])->widget(DatePicker::classname(), [
                'language' => 'ja',
                'dateFormat' => 'yyyy/MM/dd',
                'clientOptions' => [
                    "changeMonth" => true,
                    "changeYear" => true,
                    "yearRange" => "1900:+0"
                ],
            ])->textInput(['maxlength' => 10])
            ?>
        </div> 
    </div>
    <!-- End Last Visit Date From and Last Visit Date To -->

    <!-- Benefit content -->
    <div class="col-md-12">
        <div class="col-md-2 label-margin">  
            <?=
            Yii::t("backend", "Benefit Content");
            ?>
        </div> 
        <div class="col-md-3">
            <?= $form->field($model, 'benefit_content')->label(false)->dropDownList(common\components\Constants::LIST_BENEFIT_CONTENT, ['prompt' => Yii::t('backend', 'Please Select')]) ?>
        </div>
    </div>
    <!-- End Benefit content-->

    <!-- List Category -->
    <div class="col-md-12">
        <div class="col-md-2 font_label">  
            <?=
            Yii::t("backend", "Category Coupon");
            ?>
        </div> 
        <div class="col-md-9 font_label">
            <?=
            $form->field($model, 'category', [
                'template' => '{input}{error}'
            ])->inline()->label(false)->checkboxList($searchModelCategory)
            ?>
        </div>
    </div>
    <!-- End List Category -->

    <!-- Name Product -->
    <div class="col-md-12">
        <div class="col-md-2 label-margin">  
            <?=
            Yii::t("backend", "Product name");
            ?>
        </div> 
        <div class="col-md-3">
            <?= $form->field($model, 'name_product')->label(false)->textInput(['maxlength' => 100]) ?>
        </div>
    </div>
    <!-- End Name Product -->

    <!-- Title -->
    <div class="col-md-12">
        <div class="col-md-2 label-margin">  
            <?=
            Yii::t("backend", "Title");
            ?>
        </div> 
        <div class="col-md-3">
            <?= $form->field($model, 'title')->label(false)->textInput(['maxlength' => 40]) ?>
        </div>
    </div>
    <!--  End Title -->

    <!-- Introduction -->
    <div class="col-md-12">
        <div class="col-md-2 label-margin">  
            <?=
            Yii::t("backend", "Introduction Coupon");
            ?>
        </div> 
        <div class="col-md-3">
            <?= $form->field($model, 'introduction')->label(false)->textInput(['maxlength' => 100]) ?>
        </div>
    </div>
    <!--  End Introduction -->
    <!-- Coupon Jan Code -->
    <div class="col-md-12"> 
        <div class="col-md-2 label-margin">  
            <?=
            Yii::t("backend", "Coupon Jan Code");
            ?>
        </div> 
        <div class="col-md-3">
            <?=
            $form->field($model, 'coupon_jan_code')->widget(\yii\widgets\MaskedInput::className(), [
                'mask' => '9',
                'clientOptions' => ['repeat' => 17, 'greedy' => false]
            ])->label(false)->textInput(['maxlength' => 13])
            ?>
        </div>
    </div>
    <!-- End Coupon Jan Code -->
    <!-- Button submit and back-->
    <div class="col-md-12">
        <div class="col-md-1">
            <div class="form-group">
                <button type="button" class="btn btn-default common-button-default">
                    <a style="color: black;" href="<?php echo Url::to('/admin') ?>"><?php echo Yii::t('backend', 'Return') ?></a></button>
            </div>
        </div>
        <div class="col-md-1 magin_button magin-left-coupon-bt">
            <div class="form-group">
                <?= Html::submitButton(Yii::t('backend', 'Search'), ['class' => 'btn common-button-submit']) ?>
            </div>
        </div>      
    </div>
    <!-- End Button submit and back-->

    <?php ActiveForm::end(); ?>

</div>
<script>
    $('#last_staff_id').on('depdrop.init', function (event) {
        $('#mastercoupon-store_id').trigger('change');
    });

    $('#last_staff_id').on('depdrop.afterChange', function (event, id, value) {
        $('#last_staff_id').val(<?php echo $model->last_staff_id; ?>);
    });
</script>
