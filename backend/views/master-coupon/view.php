<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\MasterCoupon */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Master Coupons'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="master-coupon-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'exp_date',
            'memo:ntext',
            'title',
            'content:ntext',
            'start_date',
            'end_date',
            'type_id',
            'display_start_date',
            'show_coupon',
            'code_membership',
            'name_kana',
            'sex',
            'birthday_from',
            'birthday_to',
            'last_visit_date_from',
            'last_visit_date_to',
            'visit_number_min',
            'visit_number_max',
            'rank_customer',
            'staff_id',
            'black_list',
            'benefits_content',
            'discount_circle',
            'discount_pull',
            'price_set',
            'drink_eat',
            'image',
            'grant_point',
            'title_font_size',
            'title_postion',
            'introduction_1',
            'introduction_font_size_1',
            'introduction_postion_1',
            'introduction_2',
            'introduction_font_size_2',
            'introduction_postion_2',
            'introduction_3',
            'introduction_font_size_3',
            'introduction_postion_3',
            'other_coupon_user',
            'del_flg',
            'created_at',
            'updated_at',
            'created_by',
            'updated_by',
        ],
    ]) ?>

</div>
