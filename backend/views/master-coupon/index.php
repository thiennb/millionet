<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\LinkPager;
use yii\widgets\Pjax;
use common\components\Constants;

/* @var $this yii\web\View */
/* @var $searchModel common\models\MasterCouponSeach */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->title = Yii::t('backend', 'Master Coupon');
$this->params['breadcrumbs'][] = $this->title;
$role = \common\components\FindPermission::getRole();
?>


<div class="row">
    <div class="col-lg-12 col-md-12">
        <div class="box box-info box-solid">
            <div class="box-header with-border">
                <h4 class="text-title"><b><?= $this->title ?></b></h4>
            </div>
            <div class="box-body content">
                <div class="col-md-12">

                    <div class="common-box">
                        <div class="box-header with-border common-box-h4 col-md-4">
                            <h4 class="text-title"><b><?= Yii::t('backend', 'Coupon Finder') ?></b></h4>
                        </div>

                        <div class="box-body content">
                            <div class="col-md-12">

                                <?php
                                echo $this->render('_search', ['model' => $searchModel,
                                    'searchListStore' => $searchListStore,
                                    'searchStaff' => $searchStaff,
                                    'searchModelCategory' => $searchModelCategory]);
                                ?>      

                            </div>
                        </div>
                    </div>

                    <?php
                    //    if(count($dataProvider)>1){
                    Pjax::begin();
                    ?>    
                    <div class="common-box">
                        <div class="box-header with-border common-box-h4 col-md-4">
                            <h4 class="text-title"><b><?= Yii::t('backend', 'Coupon List') ?></b></h4>
                        </div>
                        <div class="box-body content">
                            <div class="col-md-10">
                                <?php if ($role->permission_id != Constants::STAFF_ROLE)
                                    echo Html::a(Yii::t('backend', 'Create'), ['create'], ['class' => 'btn common-button-submit common-float-right', 'style' => 'margin-bottom: 0.5em;margin-top: 0.4em'])
                                    ?>
                                <?=
                                GridView::widget([
                                    'dataProvider' => $dataProvider,
                                    'tableOptions' => [
                                        'class' => 'table table-striped table-bordered text-center',
                                    ],
                                    'layout' => "{pager}\n{summary}\n{items}",
                                    'summaryOptions' => ['class' => 'common-clr-fl-right'],
                                    'summary' => false,
                                    'pager' => [
                                        'class' => 'common\components\LinkPagerMillionet',
                                        'options' => ['class' => 'pagination common-float-right'],
                                    ],
                                    'columns' => [
                                        ['class' => 'yii\grid\SerialColumn', 'header' => 'No', 'options' => ['class' => 'with-table-no']],
                                        [
                                            'attribute' => 'coupon_jan_code',
                                        ],
                                        [
                                            'attribute' => 'start_date',
                                            'value' => function ($model) {
                                                return Yii::$app->formatter->asDate($model->start_date);
                                            },
                                        ],
                                        [
                                            'attribute' => 'title',
                                        ],
                                        [
                                            'attribute' => 'benefits_content',
                                            'value' => function($model) {
                                                if ($model->benefits_content == '00') {
                                                    return '値引';
                                                }
                                                if ($model->benefits_content == '01') {
                                                    return '割引';
                                                }
                                                if ($model->benefits_content == '02') {
                                                    return 'セット価格';
                                                }
                                                if ($model->benefits_content == '03') {
                                                    return '食べ飲み放題';
                                                }
                                                //return $model->benefits_content;
                                            },
                                        ],
                                        !($role->permission_id == Constants::STAFF_ROLE) ?
                                                [
                                            'class' => 'yii\grid\ActionColumn', 'template' => "{delete} {update}",
                                            'options' => ['class' => 'with-table-button'],
                                            'buttons' => [
                                                //view button
                                                // Delete
                                                'delete' => function ($url, $model) {
                                                    return Html::a('<span class="fa fa-delete"></span>' . Yii::t('backend', 'Delete'), $url, [
                                                                'class' => 'btn common-button-action',
                                                                'aria-label' => "Delete",
                                                                'data-confirm' => Yii::t('backend', "Delete Coupon Error Message"),
                                                                'data-method' => "post"
                                                    ]);
                                                },
                                                        'update' => function ($url, $model) {
                                                    return Html::a('<span class="fa fa-delete"></span>' . Yii::t('backend', 'Update'), $url, [

                                                                'class' => 'btn common-button-action',
                                                    ]);
                                                },
                                                    ],
                                                        ] :
                                                        [
                                                    'class' => 'yii\grid\ActionColumn', 'template' => "{delete}",
                                                    'options' => ['class' => 'with-table-button'],
                                                    'buttons' => [
                                                        // Delete
                                                        'delete' => function ($url, $model) {
                                                            return Html::a('<span class="fa fa-delete"></span>' . Yii::t('backend', 'Delete'), $url, [
                                                                        'class' => 'btn common-button-action',
                                                                        'aria-label' => "Delete",
                                                                        'data-confirm' => Yii::t('backend', "Delete Coupon Error Message"),
                                                                        'data-method' => "post"
                                                            ]);
                                                        },
                                                            ],
                                                                ]
                                                    ],
                                                ]);
                                                ?>
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                    Pjax::end();
                                    ?>
                </div>
            </div>
        </div>
    </div>
</div>

