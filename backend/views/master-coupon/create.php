<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\MasterCoupon */

$this->title = Yii::t('backend', 'Create Master Coupon');
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Master Coupon'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row text-setting company">
    <div class="col-lg-12 col-md-12">
        <div class="box box-info box-solid add">
            <div class="box-header with-border">
                <h4 class="text-title"><b><?= $this->title ?></b></h4>
            </div>
            <div class="box-body content">
                <div class="col-md-12">
                    <div class="row">
                        <?=
                        $this->render('_form', [
                            'model' => $model,
                            'searchListStore' => $searchListStore,
                            'searchStaff' => $searchStaff,
                            'listProduct' => $listProduct,
                            'list_product_select' => $list_product_select,
                            'listCategory' => $listCategory,
                        ])
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
