<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\components\Constants;

/* @var $this yii\web\View */
/* @var $model common\models\MasterStaffSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="master-staff-search">
<div class="mst-product-search col-md-8">
    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'enableClientValidation'=>false,
    ]); ?>
    <?= $form->field($model, 'management_login_id',[
        'template' => '<div class="row">'
                        . "<div class='col-md-3'><label class='mws-form-label'>{label}</label></div>"
                        . '<div class="col-md-4 input_store_ct">{input}{error}</div>'
                    . '</div>'
    ])->textInput(['maxlength'=>TRUE,'class'=>'form-control input-number']) ?>
    
    <?php if(count($listStore) > 1){
        echo $form->field($model, 'store_id',[
                'template' => '<div class="row">'
                                . "<div class='col-md-3'><label class='mws-form-label'>{label}</label></div>"
                                . '<div class="col-md-4 input_store_ct">{input}{error}</div>'
                            . '</div>'
            ])->dropDownList($listStore,['prompt'=>Yii::t('backend', 'Please Select')]);
    }else{
        echo $form->field($model, 'store_id',[
                'template' => '<div class="row">'
                                . "<div class='col-md-3'><label class='mws-form-label'>{label}</label></div>"
                                . '<div class="col-md-4 input_store_ct">{input}{error}</div>'
                            . '</div>'
            ])->dropDownList($listStore);
    } ?>

    <?= $form->field($model, 'name',[
        'template' => '<div class="row">'
                        . "<div class='col-md-3'><label class='mws-form-label'>{label}</label></div>"
                        . '<div class="col-md-4 input_store_ct">{input}{error}</div>'
                    . '</div>'
    ])->textInput(['maxlength'=>TRUE]) ?>

    <?= $form->field($model, 'memo',[
        'template' => '<div class="row">'
                        . "<div class='col-md-3'><label class='mws-form-label'>{label}</label></div>"
                        . '<div class="col-md-4 input_store_ct">{input}{error}</div>'
                    . '</div>'
    ])->textInput(['maxlength'=>TRUE]) ?>

    <?= $form->field($model, 'email',[
        'template' => '<div class="row">'
                        . "<div class='col-md-3'><label class='mws-form-label'>{label}</label></div>"
                        . '<div class="col-md-4 input_store_ct">{input}{error}</div>'
                    . '</div>'
    ])->input('email')->textInput(['maxlength'=>TRUE]) ?>

    <?= $form->field($model, 'permission_id',[
        'template' => '<div class="row">'
                        . "<div class='col-md-3'><label class='mws-form-label'>{label}</label></div>"
                        . '<div class="col-md-4 input_store_ct">{input}{error}</div>'
                    . '</div>'
    ])->dropDownList(Constants::LIST_PERMISSION,['prompt'=>Yii::t('backend', 'Please Select')]) ?>

    <?php echo $form->field($model, 'sex',[
        'template' => '<div class="row">'
                        . "<div class='col-md-3'><label class='mws-form-label'>{label}</label></div>"
                        . '<div class="col-md-4 input_store_ct">{input}{error}</div>'
                    . '</div>'
    ])->dropDownList(Constants::LIST_SEX,['prompt'=>Yii::t('backend', 'Please Select')]); ?>

    <?php 
    if($model->show_flg == null){
        $model->show_flg = 0;
    }
    echo $form->field($model, 'show_flg',[
        'template' => '<div class="row">'
                        . "<div class='col-md-3'><label class='mws-form-label'>{label}</label></div>"
                        . '<div class="col-md-7">{input}{error}</div>'
                    . '</div>'
    ])->radioList(Constants::LIST_SHOW); ?>

    <div class="form-group">
        <?= Html::a(Yii::t('backend', Yii::t('backend','Return')), ['/'], ['class' => 'btn btn-default common-button-default']) ?>
        <?= Html::submitButton(Yii::t('app', Yii::t('backend','Search')), ['class' => 'btn common-button-submit']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>
</div>
