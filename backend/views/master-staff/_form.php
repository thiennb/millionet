<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;
use common\components\Util;
use yii\bootstrap\Modal;
use common\widgets\preview\Preview;
use common\components\Constants;
use kartik\depdrop\DepDrop;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\MasterStaff */
/* @var $form yii\widgets\ActiveForm */

$list_permission = Constants::LIST_PERMISSION;
$role = \common\components\FindPermission::getRole();
if ($role->permission_id == Constants::APP_MANAGER_ROLE) {
    $list_permission = Constants::LIST_PERMISSION;
} else if ($role->permission_id == Constants::COMPANY_MANAGER_ROLE) {
    $list_permission = array_slice($list_permission, 1, 3, true);
} else {
    $list_permission = array_slice($list_permission, 3, 1, true);
}
?>

<div class="master-staff-form">

    <?php
    $form = ActiveForm::begin([
                'options' => [
                    'enctype' => 'multipart/form-data',
                    'id' => strtolower($model->formName()) . '-id',
                ], // important
                'validationUrl' => 'validate',
                'enableClientValidation' => false,
                'enableAjaxValidation' => true,
                'validateOnChange' => false,
                'validateOnBlur' => false,
                'successCssClass' => '',
    ]);
    ?>
    <?= $form->field($model, 'id_staff_hd')->label(false)->hiddenInput(['value' => $model->id]) ?>
    <?= $form->field($model, 'management_login_id')->label(false)->hiddenInput([]) ?>

    <div class="common-box">
        <div class="row">
            <div class="box-header with-border common-box-h4 col-md-6">
                <h4 class="text-title"><b><?= Yii::t('backend', 'Input of staff information') ?></b></h4>
            </div>
        </div>
        <div class="box-body content">
            <div class="col-md-12">
                <?php if (!$model->isNewRecord) { ?>
                    <div class="col-md-12">
                        <div class="col-md-3">
                            <label class="control-label" >
                                <p><?= Yii::t('backend', 'Management ID') ?></p>
                            </label>
                        </div>
                        <div class="col-md-4 input_store_ct">
                            <?= $model->managementLogin->management_id ?>
                        </div>
                        <div class="col-md-3">
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="col-md-3">
                            <label class="control-label required-star" >

                                <?= Yii::t('backend', 'Password') ?>
                            </label>
                        </div>
                        <div class="col-md-4 input_store_ct">       
                            <?=
                            $form->field($model, 'password', [
                                'template' => '{input}{error}'
                            ])->textInput(['maxlength' => true, 'type' => 'password'])->label(FALSE)
                            ?>
                        </div>
                        <div class="col-md-3">
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="col-md-3">
                            <label class="control-label" >
                                <?= $model->isNewRecord ? Yii::t('backend', 'New Password') : Yii::t('backend', 'Update New Password'); ?>
                            </label>
                        </div>
                        <div class="col-md-4 input_store_ct">       
                            <?=
                            $form->field($model, 'password_new', [
                                'template' => '{input}{error}'
                            ])->textInput(['maxlength' => true, 'type' => 'password'])->label(FALSE)
                            ?>
                        </div>
                        <div class="col-md-3">
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="col-md-3">
                            <label class="control-label" >
                                <?= $model->isNewRecord ? Yii::t('backend', 'New Password Reinput') : Yii::t('backend', 'Update New Password Reinput'); ?>
                            </label>
                        </div>
                        <div class="col-md-4 input_store_ct">       
                            <?=
                            $form->field($model, 'password_new_re', [
                                'template' => '{input}{error}'
                            ])->textInput(['maxlength' => true, 'type' => 'password'])->label(FALSE)
                            ?>
                        </div>
                        <div class="col-md-3">
                        </div>
                    </div>
                <?php } else { ?>
                    <div class="col-md-12">
                        <div class="col-md-3">
                            <label class="control-label required-star" >
                                <?= Yii::t('backend', 'Password') ?>
                            </label>
                        </div>
                        <div class="col-md-4 input_store_ct">       
                            <?=
                            $form->field($model, 'password_new', [
                                'template' => '{input}{error}'
                            ])->textInput(['maxlength' => true, 'type' => 'password'])->label(FALSE)
                            ?>
                        </div>
                        <div class="col-md-3">
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="col-md-3">
                            <label class="control-label" >
                                <?= Yii::t('backend', 'Password Re') ?>
                            </label>
                        </div>
                        <div class="col-md-4 input_store_ct">       
                            <?=
                            $form->field($model, 'password_new_re', [
                                'template' => '{input}{error}'
                            ])->textInput(['maxlength' => true, 'type' => 'password'])->label(FALSE)
                            ?>
                        </div>
                        <div class="col-md-3">
                        </div>
                    </div>
                <?php } ?>

                <!-- 氏名 -->
                <div class="col-md-12">
                    <div class="col-md-3">
                        <label class="control-label required-star" >
                            <?= Yii::t('backend', 'Name') ?>
                        </label>
                    </div>
                    <div class="col-md-4 input_store_ct">       
                        <?=
                        $form->field($model, 'name', [
                            'template' => '{input}{error}'
                        ])->textInput(['maxlength' => TRUE])->label(FALSE)
                        ?>
                    </div>
                    <div class="col-md-3">                    
                        <div class="col-md-4 clear-padding label-margin sex_staff_ct">
                            <label class="control-label" >
                                <?= Yii::t('backend', 'Sex') ?>
                            </label>
                        </div>
                        <div class="col-md-7 clear-padding">       
                            <?=
                            $form->field($model, 'sex', [
                                'template' => '{input}{error}'
                            ])->dropDownList(Constants::LIST_SEX, ['prompt' => Yii::t('backend', 'Please Select')])->label(FALSE);
                            ?>
                        </div>
                    </div>
                </div>

                <!-- 氏名略 -->
                <div class="col-md-12">
                    <div class="col-md-3 required-star">
                        <label class="control-label" >
                            <?= Yii::t('backend', 'Short name') ?>
                        </label>
                    </div>                
                    <div class="col-md-4 input_store_ct">       
                        <?=
                        $form->field($model, 'short_name', [
                            'template' => '{input}{error}'
                        ])->textInput(['maxlength' => TRUE])->label(FALSE)
                        ?>
                    </div>
                    <div class="col-md-3">
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="col-md-3">
                        <label class="control-label" >
                            <?= Yii::t('backend', 'Email') ?>
                        </label>
                    </div>
                    <div class="col-md-4 input_store_ct">       
                        <?=
                        $form->field($model, 'email', [
                            'template' => '{input}{error}'
                        ])->textInput(['maxlength' => true])->label(FALSE)
                        ?>
                    </div>
                    <div class="col-md-3">
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="col-md-3">
                        <label class="control-label required-star" >
                            <?= Yii::t('backend', 'Permission') ?>
                        </label>
                    </div>
                    <div class="col-md-4 input_store_ct">
                        <?php if(count($list_permission) > 1){
                            echo $form->field($model, 'permission_id', [
                                    'template' => '{input}{error}'
                                ])->dropDownList($list_permission, ['prompt' => Yii::t('backend', 'Please Select'), 'disabled' => ($role->id === $model->id && !$model->isNewRecord)])->label(FALSE);
                        }else{
                            echo $form->field($model, 'permission_id', [
                                    'template' => '{input}{error}'
                                ])->dropDownList($list_permission, ['disabled' => ($role->id === $model->id && !$model->isNewRecord)])->label(FALSE);
                        } ?>
                    </div>
                    <div class="col-md-5">
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="col-md-3">
                        <label class="control-label required-star" >
                            <?= Yii::t('backend', 'Affiliation store') ?>
                        </label>
                    </div>
                    <div class="col-md-4 input_store_ct">    
                        <?php if(count($listStore) > 1){
                            echo $form->field($model, 'store_id', [
                                'template' => '{input}{error}'
                            ])->dropDownList($listStore, ['prompt' => Yii::t('backend', 'Please Select')])->label(FALSE);
                        }else{
                            echo $form->field($model, 'store_id', [
                                'template' => '{input}{error}'
                            ])->dropDownList($listStore)->label(FALSE);
                        } ?>
                    </div>
                    <div class="col-md-3">
                    </div>
                </div>

                <!-- 役職 -->
                <div class="col-md-12">
                    <div class="col-md-3">
                        <label class="control-label" >
                            <?= Yii::t('backend', 'Position') ?>
                        </label>
                    </div>                
                    <div class="col-md-4 input_store_ct">       
                        <?=
                        $form->field($model, 'position', [
                            'template' => '{input}{error}'
                        ])->textInput(['maxlength' => TRUE])->label(FALSE)
                        ?>
                    </div>
                    <div class="col-md-3">                        
                        <div class="col-md-4 clear-padding label-margin career_satff_ct">
                            <label class="control-label" >
                                <?= Yii::t('backend', 'Career') ?>
                            </label>
                        </div>
                        <div class="col-md-7 clear-padding">
                            <?=
                            $form->field($model, 'career', [
                                'template' => '{input}{error}'
                            ])->label(false)->widget(\yii\widgets\MaskedInput::className(), [
                                'clientOptions' => [
                                    'alias' => 'decimal',
                                    'autoGroup' => TRUE,
                                    'removeMaskOnSubmit' => TRUE
                                ]
                            ])->textInput(['maxlength' => TRUE]);
                            ?>
                        </div>
                    </div>
                </div>

                <!-- Datpdt 23/09/2016 Update Start -->
                <div class="col-md-12">
                    <div class="col-md-3">
                        <label class="control-label" >
                            <?= Yii::t('backend', 'Assign fee') ?>
                        </label>
                    </div>
                    <div class="col-md-4 input_store_ct">       
                        <?=
                        $form->field($model, 'assign_product_id', [
                            'template' => '{input}{error}'
                        ])->widget(DepDrop::classname(), [
                            'options' => ['id' => 'masterstaff-assign_product_id'],
                            'pluginOptions' => [
                                //'init' => true,
                                'depends' => ['masterstaff-store_id'],
                                'placeholder' => 'なし',
                                'url' => Url::to(['/master-staff/select-assign-product-by-storeid'])
                            ],
                                // 'data' => $listProduct
                        ]);
                        ?>

                    </div>
                    <div class="col-md-3">
                    </div>
                </div>
                <!-- Datpdt 23/09/2016 Update End -->
                <div class="col-md-12">
                    <div class="col-md-3">
                        <label class="control-label" >
                            <?= Yii::t('backend', 'Staff avatar') ?>
                        </label>
                    </div>
                    <div class="col-md-4 img_staff_ct">       
                        <?=
                        $form->field($model, 'file_avatar', [
                            'template' => '{input}{error}'
                        ])->widget(FileInput::classname(), [
                            'options' => ['accept' => 'image/*'],
                            'pluginOptions' => [

                                'initialPreview' => [
                                    Html::img(Util::getUrlImage($model->avatar))
                                ],
                                'overwriteInitial' => true,
                                'showUpload' => false,
                                'showCaption' => false,
                                'browseLabel' => Yii::t('backend', 'Select an image'),
                                'fileActionSettings' => [
                                    'showZoom' => false,
                                ],
                                'allowedFileExtensions' => ['jpg', 'gif', 'png', 'jpeg'],
                                'maxFileSize' => 2048,
                            ],
                            'pluginEvents' => [
                                "fileclear" => 'function() { $(this).parent().parent().parent().parent().find(".hidden_image").val("") }',
                            ],
                        ])->label(FALSE);
                        ?>
                        <?= $form->field($model, 'hidden_avatar')->hiddenInput(['class' => 'hidden_image'])->label(FALSE) ?>
                    </div>
                    <div class="col-md-3">
                    </div>
                </div>
                <!-- Datpdt update 10/13/2016 start-->
                <div class="col-md-12">
                    <div class="col-md-3">
                        <label class="control-label" >
                            <?= Yii::t('backend', 'Catch') ?>
                        </label>
                    </div>
                    <div class="col-md-4 input_store_ct">
                        <?=
                        $form->field($model, 'catch', [
                            'template' => '{input}{error}'
                        ])->textInput(['maxlength' => 25])->label(FALSE)
                        ?>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="col-md-3">
                        <label class="control-label" >
                            <?= Yii::t('backend', 'Introduction') ?>
                        </label>
                    </div>
                    <div class="col-md-4 input_store_ct">
                        <?= $form->field($model, 'introduction')->label(false)->textarea(['rows' => 4, 'maxlength' => 150]) ?>
                    </div>
                </div>
                <!-- Datpdt update 10/13/2016 end-->
                <div class="col-md-12">
                    <div class="col-md-3">
                        <label class="control-label" >
                            <?= Yii::t('backend', 'Home Photo 1') ?>
                        </label>
                    </div>
                    <div class="col-md-4 img_staff_ct">       
                        <?=
                        $form->field($model, 'file_image1', [
                            'template' => '{input}{error}'
                        ])->widget(FileInput::classname(), [
                            'options' => ['accept' => 'image/*'],
                            'pluginOptions' => [
                                'initialPreview' => [
                                    Html::img(Util::getUrlImage($model->image1))
                                ],
                                'overwriteInitial' => true,
                                'showUpload' => false,
                                'showCaption' => false,
                                'browseLabel' => Yii::t('backend', 'Select an image'),
                                'fileActionSettings' => [
                                    'showZoom' => false,
                                ],
                                'allowedFileExtensions' => ['jpg', 'gif', 'png', 'jpeg'],
                                'maxFileSize' => 2048,
                            ],
                            'pluginEvents' => [
                                "fileclear" => 'function() { $(this).parent().parent().parent().parent().find(".hidden_image").val("") }',
                            ],
                        ])->label(FALSE);
                        ?>
                        <?= $form->field($model, 'hidden_image1')->hiddenInput(['class' => 'hidden_image'])->label(FALSE) ?>
                    </div>
                    <div class="col-md-3">
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="col-md-3">
                        <label class="control-label" >
                            <?= Yii::t('backend', 'Home Photo 1 Comment') ?>
                        </label>
                    </div>
                    <div class="col-md-4 input_store_ct">       
                        <?=
                        $form->field($model, 'image1_note', [
                            'template' => '{input}{error}'
                        ])->textInput(['maxlength' => 100])->label(FALSE)
                        ?>
                    </div>
                    <div class="col-md-3">
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="col-md-3">
                        <label class="control-label" >
                            <?= Yii::t('backend', 'Home Photo 2') ?>
                        </label>
                    </div>
                    <div class="col-md-4 img_staff_ct">       
                        <?=
                        $form->field($model, 'file_image2', [
                            'template' => '{input}{error}'
                        ])->widget(FileInput::classname(), [
                            'options' => ['accept' => 'image/*'],
                            'pluginOptions' => [
                                'initialPreview' => [
                                    Html::img(Util::getUrlImage($model->image2))
                                ],
                                'overwriteInitial' => true,
                                'showUpload' => false,
                                'showCaption' => false,
                                'browseLabel' => Yii::t('backend', 'Select an image'),
                                'fileActionSettings' => [
                                    'showZoom' => false,
                                ],
                                'allowedFileExtensions' => ['jpg', 'gif', 'png', 'jpeg'],
                                'maxFileSize' => 2048,
                            ],
                            'pluginEvents' => [
                                "fileclear" => 'function() { $(this).parent().parent().parent().parent().find(".hidden_image").val("") }',
                            ],
                        ])->label(FALSE);
                        ?>
                        <?= $form->field($model, 'hidden_image2')->hiddenInput(['class' => 'hidden_image'])->label(FALSE) ?>
                    </div>
                    <div class="col-md-3">
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="col-md-3">
                        <label class="control-label" >
                            <?= Yii::t('backend', 'Home Photo 2 Comment') ?>
                        </label>
                    </div>
                    <div class="col-md-4 input_store_ct">       
                        <?=
                        $form->field($model, 'image2_note', [
                            'template' => '{input}{error}'
                        ])->textInput(['maxlength' => 100])->label(FALSE)
                        ?>
                    </div>
                    <div class="col-md-3">
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="col-md-3">
                        <label class="control-label" >
                            <?= Yii::t('backend', 'Reservation site member app display') ?>
                        </label>
                    </div>
                    <div class="col-md-5">
                        <?php
                        if ($model->show_flg == null) {
                            $model->show_flg = 0;
                        }
                        ?>   
                        <?=
                        $form->field($model, 'show_flg', [
                            'template' => '{input}{error}'
                        ])->radioList(Constants::LIST_SHOW)->label(FALSE)
                        ?>
                    </div>
                    <div class="col-md-3">
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="common-box">
        <div class="row">
            <div class="box-header with-border common-box-h4 col-md-6">
                <h4 class="text-title"><b><?= Yii::t('backend', 'Input of staff any item') ?></b></h4>
            </div>
        </div>
        <div class="box-body content">
            <div class="col-md-12">
                <div class="col-md-12">
                    <div class="col-md-3">
                        <label class="control-label" >
                            <?= Yii::t('backend', 'Item OPtion 1') ?>
                        </label>
                    </div>
                    <div class="col-md-4 input_store_ct">       
                        <?=
                        $form->field($model, 'item_option_1', [
                            'template' => '{input}{error}'
                        ])->textInput(['maxlength' => true])->label(FALSE)
                        ?>
                    </div>
                    <div class="col-md-3">       
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="col-md-3">
                        <label class="control-label" >
                            <?= Yii::t('backend', 'Item OPtion 2') ?>
                        </label>
                    </div>
                    <div class="col-md-4 input_store_ct">       
                        <?=
                        $form->field($model, 'item_option_2', [
                            'template' => '{input}{error}'
                        ])->textInput(['maxlength' => true])->label(FALSE)
                        ?>
                    </div>
                    <div class="col-md-3">       
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="col-md-3">
                        <label class="control-label" >
                            <?= Yii::t('backend', 'Item OPtion 3') ?>
                        </label>
                    </div>
                    <div class="col-md-4 input_store_ct">       
                        <?=
                        $form->field($model, 'item_option_3', [
                            'template' => '{input}{error}'
                        ])->textInput(['maxlength' => true])->label(FALSE)
                        ?>
                    </div>
                    <div class="col-md-3">       
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="col-md-3">
                        <label class="control-label" >
                            <?= Yii::t('backend', 'Item OPtion 4') ?>
                        </label>
                    </div>
                    <div class="col-md-4 input_store_ct">       
                        <?=
                        $form->field($model, 'item_option_4', [
                            'template' => '{input}{error}'
                        ])->textInput(['maxlength' => true])->label(FALSE)
                        ?>
                    </div>
                    <div class="col-md-3">       
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="col-md-3">
                        <label class="control-label" >
                            <?= Yii::t('backend', 'Item OPtion 5') ?>
                        </label>
                    </div>
                    <div class="col-md-4 input_store_ct">       
                        <?=
                        $form->field($model, 'item_option_5', [
                            'template' => '{input}{error}'
                        ])->textInput(['maxlength' => true])->label(FALSE)
                        ?>
                    </div>
                    <div class="col-md-3">       
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="common-box">
        <div class="row">
            <div class="box-header with-border common-box-h4 col-md-6">
                <h4 class="text-title"><b><?= Yii::t('backend', 'Staff SNS link set') ?></b></h4>
            </div>
        </div>
        <div class="box-body content">
            <div class="col-md-12">
                <div class="col-md-12">
                    <div class="col-md-3">
                        <label class="control-label" >
                            <?= Yii::t('backend', 'Link 1 title') ?>
                        </label>
                    </div>
                    <div class="col-md-4 input_store_ct">       
                        <?=
                        $form->field($model, 'link_title_1', [
                            'template' => '{input}{error}'
                        ])->textInput(['maxlength' => true])->label(FALSE)
                        ?>
                    </div>
                    <div class="col-md-3">       
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="col-md-3">
                        <label class="control-label" >
                            <?= Yii::t('backend', 'Link 1URL') ?>
                        </label>
                    </div>
                    <div class="col-md-4 input_store_ct">       
                        <?=
                        $form->field($model, 'link_url_1', [
                            'template' => '{input}{error}'
                        ])->textInput(['maxlength' => true])->label(FALSE)
                        ?>
                    </div>
                    <div class="col-md-3">       
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="col-md-3">
                        <label class="control-label" >
                            <?= Yii::t('backend', 'Link 1 icon') ?>
                        </label>
                    </div>
                    <div class="col-md-4 img_staff_ct">       
                        <?=
                        $form->field($model, 'file_icon_1', [
                            'template' => '{input}{error}'
                        ])->widget(FileInput::classname(), [
                            'options' => ['accept' => 'image/*'],
                            'pluginOptions' => [
                                'initialPreview' => [
                                    Html::img(Util::getUrlImage($model->link_icon_1))
                                ],
                                'overwriteInitial' => true,
                                'showUpload' => false,
                                'showCaption' => false,
                                'browseLabel' => Yii::t('backend', 'Select an image'),
                                'fileActionSettings' => [
                                    'showZoom' => false,
                                ],
                                'allowedFileExtensions' => ['jpg', 'gif', 'png', 'jpeg'],
                                'maxFileSize' => 2048,
                            ],
                            'pluginEvents' => [
                                "fileclear" => 'function() { $(this).parent().parent().parent().parent().find(".hidden_image").val("") }',
                            ],
                        ])->label(FALSE);
                        ?>
                        <?= $form->field($model, 'hidden_icon1')->hiddenInput(['class' => 'hidden_image'])->label(FALSE) ?>
                    </div>
                    <div class="col-md-3">       
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="col-md-3">
                        <label class="control-label" >
                            <?= Yii::t('backend', 'Link 2 title') ?>
                        </label>
                    </div>
                    <div class="col-md-4 input_store_ct">       
                        <?=
                        $form->field($model, 'link_title_2', [
                            'template' => '{input}{error}'
                        ])->textInput(['maxlength' => true])->label(FALSE)
                        ?>
                    </div>
                    <div class="col-md-3">       
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="col-md-3">
                        <label class="control-label" >
                            <?= Yii::t('backend', 'Link 2URL') ?>
                        </label>
                    </div>
                    <div class="col-md-4 input_store_ct">       
                        <?=
                        $form->field($model, 'link_url_2', [
                            'template' => '{input}{error}'
                        ])->textInput(['maxlength' => true])->label(FALSE)
                        ?>
                    </div>
                    <div class="col-md-3">       
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="col-md-3">
                        <label class="control-label" >
                            <?= Yii::t('backend', 'Link 2 icon') ?>
                        </label>
                    </div>
                    <div class="col-md-4 img_staff_ct">       
                        <?=
                        $form->field($model, 'file_icon_2', [
                            'template' => '{input}{error}'
                        ])->widget(FileInput::classname(), [
                            'options' => ['accept' => 'image/*'],
                            'pluginOptions' => [
                                'initialPreview' => [
                                    Html::img(Util::getUrlImage($model->link_icon_2))
                                ],
                                'overwriteInitial' => true,
                                'showUpload' => false,
                                'showCaption' => false,
                                'browseLabel' => Yii::t('backend', 'Select an image'),
                                'fileActionSettings' => [
                                    'showZoom' => false,
                                ],
                                'allowedFileExtensions' => ['jpg', 'gif', 'png', 'jpeg'],
                                'maxFileSize' => 2048,
                            ],
                            'pluginEvents' => [
                                "fileclear" => 'function() { $(this).parent().parent().parent().parent().find(".hidden_image").val("") }',
                            ],
                        ])->label(FALSE);
                        ?>
                        <?= $form->field($model, 'hidden_icon2')->hiddenInput(['class' => 'hidden_image'])->label(FALSE) ?>
                    </div>
                    <div class="col-md-3">       
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="col-md-3">
                        <label class="control-label" >
                            <?= Yii::t('backend', 'Link 3 title') ?>
                        </label>
                    </div>
                    <div class="col-md-4 input_store_ct">       
                        <?=
                        $form->field($model, 'link_title_3', [
                            'template' => '{input}{error}'
                        ])->textInput(['maxlength' => true])->label(FALSE)
                        ?>
                    </div>
                    <div class="col-md-3">       
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="col-md-3">
                        <label class="control-label" >
                            <?= Yii::t('backend', 'Link 3URL') ?>
                        </label>
                    </div>
                    <div class="col-md-4 input_store_ct">       
                        <?=
                        $form->field($model, 'link_url_3', [
                            'template' => '{input}{error}'
                        ])->textInput(['maxlength' => true])->label(FALSE)
                        ?>
                    </div>
                    <div class="col-md-3">       
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="col-md-3">
                        <label class="control-label" >
                            <?= Yii::t('backend', 'Link 3 icon') ?>
                        </label>
                    </div>
                    <div class="col-md-4 img_staff_ct">       
                        <?=
                        $form->field($model, 'file_icon_3', [
                            'template' => '{input}{error}'
                        ])->widget(FileInput::classname(), [
                            'options' => ['accept' => 'image/*'],
                            'pluginOptions' => [
                                'initialPreview' => [
                                    Html::img(Util::getUrlImage($model->link_icon_3))
                                ],
                                'overwriteInitial' => true,
                                'showUpload' => false,
                                'showCaption' => false,
                                'browseLabel' => Yii::t('backend', 'Select an image'),
                                'fileActionSettings' => [
                                    'showZoom' => false,
                                ],
                                'allowedFileExtensions' => ['jpg', 'gif', 'png', 'jpeg'],
                                'maxFileSize' => 2048,
                            ],
                            'pluginEvents' => [
                                "fileclear" => 'function() { $(this).parent().parent().parent().parent().find(".hidden_image").val("") }',
                            ],
                        ])->label(FALSE);
                        ?>
                        <?= $form->field($model, 'hidden_icon3')->hiddenInput(['class' => 'hidden_image'])->label(FALSE) ?>
                    </div>
                    <div class="col-md-3">       
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="form-group">
        <?= Html::a(Yii::t('backend', Yii::t('backend', 'Return')), ['index'], ['class' => 'btn btn-default common-button-default']) ?>
        <?=
        Html::submitButton(Yii::t('backend', 'Confirm'), [
            'class' => 'btn common-button-submit',
            'id' => 'btn-confirm',
        ])
        ?>
    </div>
    <div class="row">
        <div class="col-md-8">
            <?php
            Modal::begin([
                'id' => 'userModal',
                'size' => 'SIZE_LARGE',
                'header' => '<div class="box-header with-border common-box-h4 col-md-8"><h4 class="text-title"><b>' . Yii::t('backend', 'Confirm Notice') . '</b></h4></div>',
                'footer' => Html::button(Yii::t('backend', 'Close'), ['class' => 'btn common-button-default', 'data-dismiss' => "modal", 'id' => 'btn-close'])
                . Html::submitButton(Yii::t('backend', 'Save'), ['class' => 'btn common-button-submit', 'id' => 'btn-submit']),
                'closeButton' => FALSE,
            ]);
            ?>
            <?=
            Preview::widget([
                "data" => [
                    'name' => [
                        'type' => 'input',
                        'label' => Yii::t('backend', Yii::t('backend', 'Name'))
                    ],
                    'short_name' => [
                        'type' => 'input',
                        'label' => Yii::t('backend', Yii::t('backend', 'Short name'))
                    ],
                    'sex' => [
                        'type' => 'select',
                        'label' => Yii::t('backend', Yii::t('backend', 'Sex'))
                    ],
                    'email' => [
                        'type' => 'input',
                        'label' => Yii::t('backend', Yii::t('backend', 'Email'))
                    ],
                    'permission_id' => [
                        'type' => 'select',
                        'label' => Yii::t('backend', Yii::t('backend', 'Permission'))
                    ],
                    'store_id' => [
                        'type' => 'select',
                        'label' => Yii::t('backend', Yii::t('backend', 'Affiliation store'))
                    ],
                    'position' => [
                        'type' => 'input',
                        'label' => Yii::t('backend', Yii::t('backend', 'Position'))
                    ],
                    'career' => [
                        'type' => 'input',
                        'label' => Yii::t('backend', Yii::t('backend', 'Career'))
                    ],
                    'file_avatar' => [
                        'type' => 'image',
                        'label' => Yii::t('backend', Yii::t('backend', 'Staff avatar'))
                    ],
                    'catch' => [
                        'type' => 'input',
                        'label' => Yii::t('backend', Yii::t('backend', 'Catch'))
                    ],
                    'introduction' => [
                        'type' => 'input',
                        'label' => Yii::t('backend', Yii::t('backend', 'Introduction'))
                    ],
                    'file_image1' => [
                        'type' => 'image',
                        'label' => Yii::t('backend', Yii::t('backend', 'Home Photo 1'))
                    ],
                    'image1_note' => [
                        'type' => 'input',
                        'label' => Yii::t('backend', Yii::t('backend', 'Home Photo 1 Comment'))
                    ],
                    'file_image2' => [
                        'type' => 'image',
                        'label' => Yii::t('backend', Yii::t('backend', 'Home Photo 2'))
                    ],
                    'image2_note' => [
                        'type' => 'input',
                        'label' => Yii::t('backend', Yii::t('backend', 'Home Photo 2 Comment'))
                    ],
                    'show_flg' => [
                        'type' => 'radio',
                        'label' => Yii::t('backend', Yii::t('backend', 'Reservation site member app display'))
                    ],
                    'item_option_1' => [
                        'type' => 'input',
                        'label' => Yii::t('backend', Yii::t('backend', 'Item OPtion 1'))
                    ],
                    'item_option_2' => [
                        'type' => 'input',
                        'label' => Yii::t('backend', Yii::t('backend', 'Item OPtion 2'))
                    ],
                    'item_option_3' => [
                        'type' => 'input',
                        'label' => Yii::t('backend', Yii::t('backend', 'Item OPtion 3'))
                    ],
                    'item_option_4' => [
                        'type' => 'input',
                        'label' => Yii::t('backend', Yii::t('backend', 'Item OPtion 4'))
                    ],
                    'item_option_5' => [
                        'type' => 'input',
                        'label' => Yii::t('backend', Yii::t('backend', 'Item OPtion 5'))
                    ],
                    'link_title_1' => [
                        'type' => 'input',
                        'label' => Yii::t('backend', Yii::t('backend', 'Link 1 title'))
                    ],
                    'link_url_1' => [
                        'type' => 'input',
                        'label' => Yii::t('backend', Yii::t('backend', 'Link 1URL'))
                    ],
                    'file_icon_1' => [
                        'type' => 'image',
                        'label' => Yii::t('backend', Yii::t('backend', 'Link 1 icon'))
                    ],
                    'link_title_2' => [
                        'type' => 'input',
                        'label' => Yii::t('backend', Yii::t('backend', 'Link 2 title'))
                    ],
                    'link_url_2' => [
                        'type' => 'input',
                        'label' => Yii::t('backend', Yii::t('backend', 'Link 2URL'))
                    ],
                    'file_icon_2' => [
                        'type' => 'image',
                        'label' => Yii::t('backend', Yii::t('backend', 'Link 2 icon'))
                    ],
                    'link_title_3' => [
                        'type' => 'input',
                        'label' => Yii::t('backend', Yii::t('backend', 'Link 3 title'))
                    ],
                    'link_url_3' => [
                        'type' => 'input',
                        'label' => Yii::t('backend', Yii::t('backend', 'Link 3URL'))
                    ],
                    'file_icon_3' => [
                        'type' => 'image',
                        'label' => Yii::t('backend', Yii::t('backend', 'Link 3 icon'))
                    ],
                ],
                "modelName" => $model->formName(),
                'idBtnConfirm' => 'btn-confirm',
                'formId' => strtolower($model->formName()) . '-id',
                'btnClose' => 'btn-close',
                'btnSubmit' => 'btn-submit',
                'modalId' => 'userModal'
            ])
            ?>
            <?php
            Modal::end();
            ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<script>
    jQuery.fn.preventDoubleSubmission = function () {

        var last_clicked, time_since_clicked;

        $("#btn-submit").bind('click', function (event) {

            if (last_clicked)
                time_since_clicked = event.timeStamp - last_clicked;

            last_clicked = event.timeStamp;

            if (time_since_clicked < 2000)
                return false;

            return true;
        });
    };
    $('#masterstaff-id').preventDoubleSubmission();


    $('#masterstaff-assign_product_id').on('depdrop.init', function (event) {
        $('#masterstaff-store_id').trigger('change');
    });

    $('#masterstaff-assign_product_id').on('depdrop.afterChange', function (event, id, value) {
        $('#masterstaff-assign_product_id').val(<?php echo $model->assign_product_id; ?>);
    });

    // Focusout Name
    $("#masterstaff-name").focusout(function () {

        if ($("#masterstaff-short_name").val().length == 0) {
            if ($("#masterstaff-name").val().length <= 5) {
                $("#masterstaff-short_name").val($("#masterstaff-name").val());
            } else {
                var res = $("#masterstaff-name").val().substring(0, 5);
                $("#masterstaff-short_name").val(res);
            }
        }
    });


</script>