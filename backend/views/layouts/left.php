<?php

use yii\helpers\Html;
use common\components\Util;
?>
<style>
    .sidebar-menu .fa-angle-left:before {
        content: "\f107" !important;
    }
    .sidebar-menu .active .fa-angle-left:before {
        content: "\f106" !important;
    }
</style>
<aside class="main-sidebar">

    <section class="sidebar">

        <!-- Sidebar user panel 
        <div class="user-panel">
            <div class="pull-left image">
          
                 <?= Html::img(Util::getUrlImage("user.gif"), ['class' => 'img-circle']); ?> 
            
              
            </div>
            <div class="pull-left info">
                <p style="padding-top: 0.9em;"><?= \Yii::$app->user->identity->name; ?></p>
         
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
         
            </div>
        </div>
        -->


        <?=
        dmstr\widgets\Menu::widget(
                [
                    'options' => ['class' => 'sidebar-menu'],
                    'items' => [
                        // ====================== MENU TOP =======================
                        ['label' => Yii::t('backend', ''),],
                        [
                            'label' => Yii::t('backend', 'Top'),
                            'icon' => 'fa fa-home',
                            'url' => '/admin',
                        ],
                        //予約管理
                        [
                            'label' => Yii::t('backend', 'Booking Management'),
                            'icon' => 'fa fa-shopping-bag',
                            'url' => '#',
                            'items' => [
                                ['label' => Yii::t('backend', 'Booking Schedule'), 'icon' => '', 'url' => ['/booking/schedule']],
                                ['label' => Yii::t('backend', 'Booking List'), 'icon' => '', 'url' => ['/booking/index', 'date' => date("Y/m/d")],],
                                ['label' => Yii::t('backend', 'Store Booking Acceptance Setting'), 'icon' => '', 'url' => ['/booking/receptionsetting']],
                                ['label' => Yii::t('backend', 'Staff Booking Acceptance Setting'), 'icon' => '', 'url' => ['/booking/staff-booking-setting'],],
                            ],
                        ],
                        //お知らせ管理
                        [
                            'label' => Yii::t('backend', 'Notice Management'),
                            'icon' => 'fa fa-sticky-note',
                            'url' => '#',
                            'items' => [
                                ['label' => Yii::t('backend', 'Distributing Plan List'), 'icon' => '', 'url' => ['/master-notice-member/list-plan'],],
                                ['label' => Yii::t('backend', 'Auto Distributing Setting List'), 'icon' => '', 'url' => ['/master-notice-member/list-auto-send'],],
                                ['label' => Yii::t('backend', 'Distributing History'), 'icon' => '', 'url' => ['/master-notice-member/list-history'],],
                            ],
                        ],
                        //商品管理
                        [
                            'label' => Yii::t('backend', 'Product Management'),
                            'icon' => 'fa fa-product-hunt',
                            'url' => '#',
                            'items' => [
                                ['label' => Yii::t('backend', 'Product Category'), 'icon' => '', 'url' => ['/mst-categories-product'],],
                                ['label' => Yii::t('backend', 'Product List'), 'icon' => '', 'url' => ['/mst-product'],],
                                ['label' => Yii::t('backend', 'Product Option List'), 'icon' => '', 'url' => ['/master-option'],],
                                ['label' => Yii::t('backend', 'Coupon List'), 'icon' => '', 'url' => ['/master-coupon'],],
                                ['label' => Yii::t('backend', 'Ticket List'), 'icon' => '', 'url' => ['/master-ticket'],],
                            ],
                        ],
                        //顧客管理
                        [
                            'label' => Yii::t('backend', 'Customer Management'),
                            'icon' => 'fa fa-user',
                            'url' => '#',
                            'items' => [
                                ['label' => Yii::t('backend', 'Customer List'), 'icon' => '', 'url' => ['/master-customer/index'],],
                                ['label' => Yii::t('backend', 'Booking History'), 'icon' => '', 'url' => ['/master-customer/index-booking-history'],],
                                ['label' => Yii::t('backend', 'Store Visiting History'), 'icon' => '', 'url' => ['/master-customer/index-visit-history'],],
                                ['label' => Yii::t('backend', 'Point History'), 'icon' => '', 'url' => ['master-customer/index-history-point'],],
                                ['label' => Yii::t('backend', 'Ticket History'), 'icon' => '', 'url' => ['master-customer/index-history-ticket'],],
                                ['label' => Yii::t('backend', 'Charge History'), 'icon' => '', 'url' => ['master-customer/index-charge-history'],],
                                ['label' => Yii::t('backend', 'Enjoy lottery'), 'icon' => '', 'url' => ['/enjoy-lottery'],],
                            ],
                        ],
                        //店舗管理
                        [
                            'label' => Yii::t('backend', 'Store Management'),
                            'icon' => 'fa fa-shopping-cart',
                            'url' => '#',
                            'items' => [
                                ['label' => Yii::t('backend', 'Store List'), 'icon' => '', 'url' => ['/store'],],
                                ['label' => Yii::t('backend', 'Staff List'), 'icon' => '', 'url' => ['/master-staff'],],
                                ['label' => Yii::t('backend', 'Seat Type List'), 'icon' => '', 'url' => ['/master-type-seat'],],
                                ['label' => Yii::t('backend', 'Seat List'), 'icon' => '', 'url' => ['/master-seat'],],
                            ],
                        ],
                        //ギフト伝票管理
                        [
                            'label' => Yii::t('backend', 'Gift Voucher Management'),
                            'icon' => 'fa fa-gift',
                            'url' => '#',
                            'items' => [
                                ['label' => Yii::t('backend', 'Gift Voucher List'), 'icon' => '', 'url' => ['/master-gift'],],
                            ],
                        ],
                        //売上帳票
                        [
                            'label' => Yii::t('backend', 'Sales Report'),
                            'icon' => 'fa fa-tags',
                            'url' => '#',
                            'items' => [
                                ['label' => Yii::t('backend', 'Daily Sales'), 'icon' => '', 'url' => ['/sale-report/daily-sales'],],
                                ['label' => Yii::t('backend', 'Product Sales'), 'icon' => '', 'url' => ['/sale-report/product-sales'],],
                            ],
                        ],
                        //設定
                        [
                            'label' => Yii::t('backend', 'Setting'),
                            'icon' => 'fa fa-cog',
                            'url' => '#',
                            'items' => [
                                ['label' => Yii::t('backend', 'Tax Rate Setting'), 'icon' => '', 'url' => ['/master-tax'],],
                                ['label' => Yii::t('backend', 'Rank Setting'), 'icon' => '', 'url' => ['/master-rank'],],
                                ['label' => Yii::t('backend', 'Point Setting'), 'icon' => '', 'url' => ['/point-setting'],],
                                ['label' => Yii::t('backend', 'Company Setting'), 'icon' => '', 'url' => ['/company'],],
                            ],
                        ],                        
                        //会計管理
                        [
                            'label' => Yii::t('backend', 'Accounting management'),
                            'icon' => 'fa fa-money',
                            'url' => '#',
                            'items' => [
                                ['label' => Yii::t('backend', 'Accounting infomation input'), 'icon' => '', 'url' => ['/booking/accounting-management'],],
                            ],
                        ],
                        // ========================== END MENU TOP ==================
                        

                    ],
                ]
        )
        ?>

    </section>

</aside>