<?php

use yii\widgets\Breadcrumbs;
use dmstr\widgets\Alert;
?>
<div class="content-wrapper">
    <section class="content-header">
        <?php if (isset($this->blocks['content-header'])) { ?>
            <h1><?= $this->blocks['content-header'] ?></h1>
        <?php } else { ?>
            <h1>
                &nbsp;
            </h1>
        <?php } ?>

        <?=
        Breadcrumbs::widget(
                [
                    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                ]
        )
        ?>
    </section>

    <section class="content">
        <?= Alert::widget() ?>
<?= $content ?>
    </section>
</div>

<footer class="main-footer" style="<?php if (Yii::$app->user->isGuest) {
    echo 'margin-left: 0';
} ?>">
    <center>
        <strong>Copyright &copy; 2016 <a href="http://www.millionet.co.jp">Millionet</a>.</strong> All rights
        reserved.
    </center>
</footer>



<!-- Add the sidebar's background. This div must be placed
     immediately after the control sidebar -->
<div class='control-sidebar-bg'></div>