<?php

use yii\helpers\Html;
use common\components\Util;
?>
<style>
    .sidebar-menu .fa-angle-left:before {
        content: "\f107" !important;
    }
    .sidebar-menu .active .fa-angle-left:before {
        content: "\f106" !important;
    }
</style>
<aside class="main-sidebar">

    <section class="sidebar">

        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <?= Html::img(Util::getUrlImage("user.gif"), ['class' => 'img-circle']); ?>

            </div>
            <div class="pull-left info">
                <p><?= \Yii::$app->user->identity->username; ?></p>

                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>



        <?=
        dmstr\widgets\Menu::widget(
                [
                    'options' => ['class' => 'sidebar-menu'],
                    'items' => [
                        // ====================== MENU TOP =======================
                        ['label' => Yii::t('backend', 'Top'), 'options' => ['class' => 'header']],
                        [
                            'label' => Yii::t('backend', 'Booking Management'),
                            'icon' => 'fa fa-minus ',
                            'url' => '#',
                            'items' => [
                                ['label' => Yii::t('backend', 'Booking Schedule'), 'icon' => '', 'url' => ['#'],],
                                ['label' => Yii::t('backend', 'Booking List'), 'icon' => '', 'url' => ['#'],],
                                ['label' => Yii::t('backend', 'Store Booking Acceptance Setting'), 'icon' => '', 'url' => ['/booking/receptionsetting'],],
                                ['label' => Yii::t('backend', 'Staff Booking Acceptance Setting'), 'icon' => '', 'url' => ['/booking/staff-booking-setting'],],
                            ],
                        ],
                        [
                            'label' => Yii::t('backend', 'Notice Management'),
                            'icon' => 'fa fa-minus ',
                            'url' => '#',
                            'items' => [
                                ['label' => Yii::t('backend', 'Distributing Plan List'), 'icon' => '', 'url' => ['/master-notice-member'],],
                                ['label' => Yii::t('backend', 'Auto Distributing Setting List'), 'icon' => '', 'url' => ['/'],],
                                ['label' => Yii::t('backend', 'Distributing History'), 'icon' => '', 'url' => ['#'],],
                            ],
                        ],
                        [
                            'label' => Yii::t('backend', 'Product Management'),
                            'icon' => 'fa fa-minus ',
                            'url' => '#',
                            'items' => [
                                ['label' => Yii::t('backend', 'Product Category'), 'icon' => '', 'url' => ['/mst-categories-product'],],
                                ['label' => Yii::t('backend', 'Product List'), 'icon' => '', 'url' => ['/mst-product'],],
                                ['label' => Yii::t('backend', 'Product Option List'), 'icon' => '', 'url' => ['/master-option'],],
                                ['label' => Yii::t('backend', 'Coupon List'), 'icon' => '', 'url' => ['/master-coupon'],],
                                ['label' => Yii::t('backend', 'Ticket List'), 'icon' => '', 'url' => ['/master-ticket'],],
                            ],
                        ],
                        [
                            'label' => Yii::t('backend', 'Customer Management'),
                            'icon' => 'fa fa-minus ',
                            'url' => '#',
                            'items' => [
                                ['label' => Yii::t('backend', 'Customer List'), 'icon' => '', 'url' => ['/master-customer'],],
                                ['label' => Yii::t('backend', 'Booking History'), 'icon' => '', 'url' => ['/master-booking-history'],],
                                ['label' => Yii::t('backend', 'Store Visiting History'), 'icon' => '', 'url' => ['#'],],
                                ['label' => Yii::t('backend', 'Point History'), 'icon' => '', 'url' => ['#'],],
                                ['label' => Yii::t('backend', 'Ticket History'), 'icon' => '', 'url' => ['#'],],
                                ['label' => Yii::t('backend', 'Charge History'), 'icon' => '', 'url' => ['#'],],
                                ['label' => Yii::t('backend', 'Enjoy lottery'), 'icon' => '', 'url' => ['#'],],
                            ],
                        ],
                        [
                            'label' => Yii::t('backend', 'Store Management'),
                            'icon' => 'fa fa-minus ',
                            'url' => '#',
                            'items' => [
                                ['label' => Yii::t('backend', 'Store List'), 'icon' => '', 'url' => ['/store'],],
                                ['label' => Yii::t('backend', 'Staff List'), 'icon' => '', 'url' => ['/master-staff'],],
                                ['label' => Yii::t('backend', 'Seat Type List'), 'icon' => '', 'url' => ['/master-type-seat'],],
                                ['label' => Yii::t('backend', 'Seat List'), 'icon' => '', 'url' => ['/master-seat'],],
                            ],
                        ],
                        [
                            'label' => Yii::t('backend', 'Gift Voucher Management'),
                            'icon' => 'fa fa-minus ',
                            'url' => '#',
                            'items' => [
                                ['label' => Yii::t('backend', 'Gift Voucher List'), 'icon' => '', 'url' => ['/master-gift'],],
                            ],
                        ],
                        [
                            'label' => Yii::t('backend', 'Sales Report'),
                            'icon' => 'fa fa-minus ',
                            'url' => '#',
                            'items' => [
                                ['label' => Yii::t('backend', 'Daily Sales'), 'icon' => '', 'url' => ['#'],],
                                ['label' => Yii::t('backend', 'Product Sales'), 'icon' => '', 'url' => ['#'],],
                            ],
                        ],
                        [
                            'label' => Yii::t('backend', 'Setting'),
                            'icon' => 'fa fa-minus ',
                            'url' => '#',
                            'items' => [
                                ['label' => Yii::t('backend', 'Tax Rate Setting'), 'icon' => '', 'url' => ['/master-tax'],],
                                ['label' => Yii::t('backend', 'Rank Setting'), 'icon' => '', 'url' => ['/master-rank'],],
                                ['label' => Yii::t('backend', 'Company Setting'), 'icon' => '', 'url' => ['/company'],],
                            ],
                        ],
                        // ========================== END MENU TOP ==================
//                        [
//                            'label' => Yii::t('backend', 'Master Staff'),
//                            'icon' => 'fa fa-minus ',
//                            'url' => '#',
//                            'items' => [
//                                ['label' => Yii::t('backend', 'Staff list'), 'icon' => '', 'url' => ['/master-staff'],],
//                                ['label' => Yii::t('backend', 'Create Staff'), 'icon' => '', 'url' => ['/master-staff/create'],],
//                            ],
//                        ],
//                        [
//                            'label' => Yii::t('backend', 'Master Shop'),
//                            'icon' => 'fa fa-minus ',
//                            'url' => '#',
//                            'items' => [
//                                ['label' => Yii::t('backend', 'Shop List'), 'icon' => '', 'url' => ['/master-shop'],],
//                                ['label' => Yii::t('backend', 'Create Shop'), 'icon' => '', 'url' => ['/master-shop/create'],],
//                            ],
//                        ],
//                        [
//                            'label' => Yii::t('backend', 'Master Categories Product'),
//                            'icon' => 'fa-minus ',
//                            'url' => '#',
//                            'items' => [
//                                ['label' => Yii::t('backend', 'Product Categories List'), 'icon' => '', 'url' => ['/mst-categories-product'],],
//                                ['label' => Yii::t('backend', 'Create Product Categories'), 'icon' => '', 'url' => ['/mst-categories-product/create'],],
//                            ],
//                        ],
//                        [
//                            'label' => Yii::t('backend', 'Master Product'),
//                            'icon' => 'fa-minus ',
//                            'url' => '#',
//                            'items' => [
//                                ['label' => Yii::t('backend', 'Product List'), 'icon' => '', 'url' => ['/mst-product'],],
//                                ['label' => Yii::t('backend', 'Create Product'), 'icon' => '', 'url' => ['/mst-product/create'],],
//                            ],
//                        ],
//                        [
//                            'label' => Yii::t('backend', 'Master Notice'),
//                            'icon' => 'fa-minus ',
//                            'url' => '#',
//                            'items' => [
//                                ['label' => Yii::t('backend', 'List Notice'), 'icon' => '', 'url' => ['/master-notice'],],
//                                ['label' => Yii::t('backend', 'Create Notice'), 'icon' => '', 'url' => ['/master-notice/create'],],
//                            ],
//                        ],
//                        [
//                            'label' => Yii::t('app', 'Master Store'),
//                            'icon' => 'fa-minus ',
//                            'url' => '#',
//                            'items' => [
//                                ['label' => Yii::t('app', 'List Store'), 'icon' => 'fa fa-list', 'url' => ['/store'],],
//                                ['label' => 'Create Store', 'icon' => '', 'url' => ['/store/create'],],
//                            ],
//                        ],
//                        [
//                            'label' => Yii::t('app', 'Master Type Seat'),
//                            'icon' => 'fa-minus ',
//                            'url' => '#',
//                            'items' => [
//                                ['label' => Yii::t('app', 'List Type Seat'), 'icon' => '', 'url' => ['/master-type-seat'],],
//                                ['label' => ' Create Type Seat', 'icon' => '', 'url' => ['/master-type-seat/create'],],
//                            ],
//                        ],
//                        [
//                            'label' => Yii::t('app', 'Master  Seat'),
//                            'icon' => 'fa-minus ',
//                            'url' => '#',
//                            'items' => [
//                                ['label' => Yii::t('app', 'List  Seat'), 'icon' => '', 'url' => ['/master-seat'],],
//                                ['label' => ' Create  Seat', 'icon' => '', 'url' => ['/master-seat/create'],],
//                            ],
//                        ],
//                        [
//                            'label' => Yii::t('app', 'Master  Coupon'),
//                            'icon' => 'fa-minus ',
//                            'url' => '#',
//                            'items' => [
//                                ['label' => Yii::t('app', 'List  Coupon'), 'icon' => '', 'url' => ['/master-coupon'],],
//                                ['label' => ' Create  Coupon', 'icon' => '', 'url' => ['/master-coupon/create'],],
//                            ],
//                        ],
//                        [
//                            'label' => Yii::t('backend', 'Booking'),
//                            'icon' => 'fa-minus ',
//                            'url' => '#',
//                            'items' => [
//                                ['label' => Yii::t('backend', 'Booking'), 'icon' => '', 'url' => ['/booking/schedule']],
////                            ['label' => 'Create Store', 'icon' => 'fa fa-dashboard', 'url' => ['/store/create'],],
//                            ],
//                        ],
//                        [
//                            'label' => Yii::t('backend', 'Booking History'),
//                            'icon' => 'fa-minus ',
//                            'url' => '#',
//                            'items' => [
//                                ['label' => Yii::t('backend', 'Booking History'), 'icon' => '', 'url' => ['/master-booking-history']],
////                            ['label' => 'Create Store', 'icon' => 'fa fa-dashboard', 'url' => ['/store/create'],],
//                            ],
//                        ],
//                        [
//                            'label' => Yii::t('app', 'Master Tax'),
//                            'icon' => 'fa-minus ',
//                            'url' => '#',
//                            'items' => [
//                                ['label' => Yii::t('app', 'List Tax'), 'icon' => '', 'url' => ['/master-tax'],],
//                                ['label' => ' Create Tax', 'icon' => '', 'url' => ['/master-tax/create'],],
//                            ],
//                        ],
//                        [
//                            'label' => Yii::t('backend', 'Customer list'),
//                            'icon' => 'fa-minus ',
//                            'url' => '#',
//                            'items' => [
//                                ['label' => Yii::t('backend', 'Customer list'), 'icon' => '', 'url' => ['/master-customer'],],
//                                ['label' => Yii::t('backend', 'Create'), 'icon' => '', 'url' => ['/master-customer/create'],],
//                            ],
//                        ],
//                        [
//                            'label' => Yii::t('backend', 'Rank list'),
//                            'icon' => 'fa-minus ',
//                            'url' => '#',
//                            'items' => [
//                                ['label' => Yii::t('backend', 'Rank list'), 'icon' => '', 'url' => ['/master-rank']],
//                            ],
//                        ],
//                        ['label' => 'Gii', 'icon' => 'fa fa-file-code-o', 'url' => ['/gii']],
//                        ['label' => 'Login', 'url' => ['site/login'], 'visible' => Yii::$app->user->isGuest],
//                        [
//                            'label' => 'Same tools',
//                            'icon' => 'fa-minus ',
//                            'url' => '#',
//                            'items' => [
//                                ['label' => 'Gii', 'icon' => 'fa fa-file-code-o', 'url' => ['/gii'],],
//                                ['label' => 'Debug', 'icon' => 'fa fa-dashboard', 'url' => ['/debug'],],
//                                [
//                                    'label' => 'Level One',
//                                    'icon' => 'fa fa-circle-o',
//                                    'url' => '#',
//                                    'items' => [
//                                        ['label' => 'Level Two', 'icon' => 'fa fa-circle-o', 'url' => '#',],
//                                        [
//                                            'label' => 'Level Two',
//                                            'icon' => 'fa fa-circle-o',
//                                            'url' => '#',
//                                            'items' => [
//                                                ['label' => 'Level Three', 'icon' => 'fa fa-circle-o', 'url' => '#',],
//                                                ['label' => 'Level Three', 'icon' => 'fa fa-circle-o', 'url' => '#',],
//                                            ],
//                                        ],
//                                    ],
//                                ],
//                            ],
//                        ],
                    ],
                ]
        )
        ?>

    </section>

</aside>
