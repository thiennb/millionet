<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\MasterBookingHistory */

$this->title = Yii::t('backend', 'Create Master Booking History');
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Master Booking Histories'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="master-booking-history-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
