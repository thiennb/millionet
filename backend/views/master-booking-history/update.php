<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\MasterBookingHistory */

$this->title = Yii::t('backend', 'Update {modelClass}: ', [
    'modelClass' => 'Master Booking History',
]) . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Master Booking Histories'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('backend', 'Update');
?>
<div class="master-booking-history-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
