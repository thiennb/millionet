<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\MasterBookingHistory */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Master Booking Histories'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="master-booking-history-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('backend', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('backend', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('backend', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'coupon_id',
            'customer_id',
            'shop_id',
            'memo',
            'memo2',
            'memo3',
            'staff_id',
            'fax',
            'memo1',
            'start_time',
            'end_time',
            'seat_id',
            'menu_coupon_id',
            'status',
            'action',
            'created_at',
            'updated_at',
            'booking_date',
            'demand',
            'black_list',
            'del_flg',
        ],
    ]) ?>

</div>
