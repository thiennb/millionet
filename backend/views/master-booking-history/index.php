<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use dmstr\widgets\Alert;
/* @var $this yii\web\View */
/* @var $searchModel common\models\MasterBookingHistorySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('backend', 'Master Booking Histories');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
  <div class="col-lg-12 col-md-12">
    <div class="box box-info box-solid">
      <div class="box-header with-border">
        <h4 class="text-title"><b><?= $this->title ?></b></h4>
      </div>
      <div class="box-body content">
        <div class="col-md-12">
          <div class="common-box">
            <div class="box-header with-border common-box-h4 col-md-6">
              <h4 class="text-title"><b><?= Yii::t('backend', 'Search Booking History') ?></b></h4>
            </div>
            <div class="box-body content">
              <div class="col-md-8">
                    <?php echo $this->render('_search', ['model' => $searchModel,'searchModelCategory'=>$searchModelCategory,'searchModelStaff'=>$searchModelStaff]); ?>                      
              </div>
            </div>
          </div>
          <?php
              Pjax::begin();
          ?>    
          <div class="common-box">
            <div class="box-header with-border common-box-h4 col-md-6">
              <h4 class="text-title"><b><?= Yii::t('backend', 'Booking History List') ?></b></h4>
            </div>
            <div class="box-body content">
              <div class="col-md-12">
                <?php Pjax::begin(); ?>    
                <?=
                    GridView::widget([
                        'dataProvider' => $dataProvider,
                        'layout' => "{pager}\n{summary}\n{items}",
                                    'summaryOptions' => ['class' => 'common-clr-fl-right'],
                                    'summary'=>false,
                                    'pager' => [
                                        'firstPageLabel' => '<<',
                                        'lastPageLabel' => '>>',
                                        'prevPageLabel' => '<',
                                        'nextPageLabel' => '>',
                                        'options' => ['class' => 'pagination common-float-right'], 
                                    ],
                        'columns' => [
                            ['class' => 'yii\grid\SerialColumn'],
                            'name',
                            'mobile',
                            [
                                'attribute' => 'rank',
                                'value' => function ($model) {
                                    return 'A';
                                }
                            ],
                            'number_visit',
                            [
                                'attribute' => 'last_visit_date',
                                'format' => ['date','php:Y/m/d']
                            ],
                          
                            [
                                'class' => 'yii\grid\ActionColumn', 
                                'template' => "{view}",
                                'buttons' => [
                                    
                                    'view' => function ($url, $model) {
                                        return Html::a('<span class="fa fa-delete"></span>'.Yii::t('backend', 'Update'), $url, [ 
                                                'title' => Yii::t('backend', 'Update'),
                                                'class' =>'btn common-button-action' , 
                                              ]);
                                    },
                                ],
                            ],
                        ],
                    ]);
                ?>
              </div>
            </div>
          </div>
        <?php Pjax::end(); ?>
        </div>
      </div>
    </div>
  </div>
</div>
