<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use common\models\MasterNotice;
use common\components\Constants;
use yii\jui\DatePicker;
/* @var $this yii\web\View */
/* @var $model common\models\MasterStaffSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="master-staff-search">
<div class="mst-product-search col-md-8">
    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'enableClientValidation'=>false,
    ]); ?>
     <div class="row">
          <div class="col-md-3" >
            <label class="control-label" for="masterbookinghistorysearch-rank_customer">
                <?=
                    Yii::t("backend", "Booking Date");
                ?>
            </label>
          </div>
          <div class="col-md-3">
                <?= $form->field($model, 'booking_date_from',['template'=>'{input}'])->widget(DatePicker::classname(), [
                    'language' => 'ja',
                    'dateFormat' => 'yyyy-MM-dd',
                ]) ?>
           </div>
         <div class="col-md-1" style="text-align: center;">
            <label class="control-label" for="masterbookinghistorysearch-rank_customer">
                ～
            </label>
         </div>
            <div class="col-md-3">
                 <?= $form->field($model, 'booking_date_to',['template'=>'{input}'])->widget(DatePicker::classname(), [
                    'language' => 'ja',
                    'dateFormat' => 'yyyy-MM-dd',
                ]) ?>
           </div>
   </div>
   <div class="row">
          <div class="col-md-3" >
            <label class="control-label" for="masterbookinghistorysearch-rank_customer">
                <?=
                    Yii::t("backend", "Category");
                ?>
            </label>
          </div>
          <div class="col-md-9" >
              <?= $form->field($model, 'category',[
                    'template' => '{input}{error}'
                ])->inline()->label(false)->checkboxList($searchModelCategory) ?>
          </div>
    </div>
    
    <?= $form->field($model, 'name_product',[
            'template' => '<div class="row">'
                        . "<div class='col-md-3'><label class='mws-form-label'>{label}</label></div>"
                        . '<div class="col-md-9">{input}</div>'
                    . '</div>'
            ]) ?>
    

    <?= $form->field($model, 'status',[
            'template' => '<div class="row">'
                        . "<div class='col-md-3'><label class='mws-form-label'>{label}</label></div>"
                        . '<div class="col-md-9">{input}</div>'
                    . '</div>'
            ])->dropDownList(common\components\Constants::LIST_BOOKING_STATUS_KANRI,['prompt'=>Yii::t('backend', 'Please select')]) ?>
     
    <?= $form->field($model, 'customer_jan_code',[
            'template' => '<div class="row">'
                        . "<div class='col-md-3'><label class='mws-form-label'>{label}</label></div>"
                        . '<div class="col-md-9">{input}</div>'
                    . '</div>'
            ]) ?>
     
    <?= $form->field($model, 'name_customer',[
            'template' => '<div class="row">'
                        . "<div class='col-md-3'><label class='mws-form-label'>{label}</label></div>"
                        . '<div class="col-md-9">{input}</div>'
                    . '</div>'
            ]) ?>
     
    <?= $form->field($model, 'sex',[
            'template' => '<div class="row">'
                        . "<div class='col-md-3'><label class='mws-form-label'>{label}</label></div>"
                        . '<div class="col-md-9">{input}</div>'
                    . '</div>'
            ])->dropDownList(common\components\Constants::LIST_SEX,['prompt'=>Yii::t('backend', 'Please select')]) ?>
     
    <?= $form->field($model, 'rank_customer',[
            'template' => '<div class="row">'
                        . "<div class='col-md-3'><label class='mws-form-label'>{label}</label></div>"
                        . '<div class="col-md-9">{input}</div>'
                    . '</div>'
            ])->dropDownList(common\components\Constants::RANK,['prompt'=>Yii::t('backend', 'Please select')]) ?>
   
    <div class="row">
          <div class="col-md-3" >
            <label class="control-label" for="masterbookinghistorysearch-rank_customer">
            <?=
                   Yii::t("backend", "Visit");
            ?>
            </label>
          </div>
           <div class="col-md-3">
                <?= $form->field($model, 'number_visit_below',['template'=>'{input}'.'<label class="control-label" for="masterbookinghistorysearch-rank_customer" style="margin-left: 0.5em">'.Yii::t("backend", "Number Visit Below").'</label>'])->textInput(['style' => "width : 6em ; display :inline"]) ?>
           </div>
            <div class="col-md-3">
                <?= $form->field($model, 'number_visit_above',['template'=>'{input}'.'<label class="control-label" for="masterbookinghistorysearch-rank_customer" style="margin-left: 0.5em">'.Yii::t("backend", "Number Visit Above").'</label>'])->textInput(['style' => "width : 6em ; display :inline"]) ?>
           </div>
    </div>
     <div class="row">
          <div class="col-md-3" >
            <label class="control-label" for="masterbookinghistorysearch-rank_customer">
                <?=
                    Yii::t("backend", "Birth Date");
                ?>
            </label>
          </div>
          <div class="col-md-3">
                <?= $form->field($model, 'birth_date_from',['template'=>'{input}'])->widget(DatePicker::classname(), [
                    'language' => 'ja',
                    'dateFormat' => 'yyyy-MM-dd',
                ]) ?>
           </div>
         <div class="col-md-1" style="text-align: center;">
            <label class="control-label" for="masterbookinghistorysearch-rank_customer">
                ～
            </label>
         </div>
            <div class="col-md-3">
                 <?= $form->field($model, 'birth_date_to',['template'=>'{input}'])->widget(DatePicker::classname(), [
                    'language' => 'ja',
                    'dateFormat' => 'yyyy-MM-dd',
                ]) ?>
           </div>
    </div>
    <div class="row">
          <div class="col-md-3" >
            <label class="control-label" for="masterbookinghistorysearch-rank_customer">
                <?=
                    Yii::t("backend", "Visit First");
                ?>
            </label>
          </div>
          <div class="col-md-3">
                <?= $form->field($model, 'first_visit_date_from',['template'=>'{input}'])->widget(DatePicker::classname(), [
                    'language' => 'ja',
                    'dateFormat' => 'yyyy-MM-dd',
                ]) ?>
           </div>
         <div class="col-md-1" style="text-align: center;">
            <label class="control-label" for="masterbookinghistorysearch-rank_customer">
                ～
            </label>
         </div>
            <div class="col-md-3">
                 <?= $form->field($model, 'first_visit_date_to',['template'=>'{input}'])->widget(DatePicker::classname(), [
                    'language' => 'ja',
                    'dateFormat' => 'yyyy-MM-dd',
                ]) ?>
           </div>
    </div>
    <?= $form->field($model, 'staff_id',[
            'template' => '<div class="row">'
                        . "<div class='col-md-3'><label class='mws-form-label'>{label}</label></div>"
                        . '<div class="col-md-9">{input}</div>'
                    . '</div>'
            ])->dropDownList($searchModelStaff,['prompt'=>Yii::t('backend', 'Please select')]) ?>
    
    <?= $form->field($model, 'chairs',[
            'template' => '<div class="row">'
                        . "<div class='col-md-3'><label class='mws-form-label'>{label}</label></div>"
                        . '<div class="col-md-9">{input}</div>'
                    . '</div>'
            ])->dropDownList($searchModelStaff,['prompt'=>Yii::t('backend', 'Please select')]) ?>
    
    
    <div class="form-group">
         <?= Html::submitButton(Yii::t('backend', 'Search'), ['class' => 'btn common-button-submit']) ?>
    </div>
   
    <?php ActiveForm::end(); ?>
</div>
</div>
