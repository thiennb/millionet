<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\components\Constants;
use yii\bootstrap\Modal;
use yii\widgets\Pjax;
use yii\grid\GridView;
use yii\helpers\Url;
use common\widgets\preview\Preview;
use yii\bootstrap\Collapse;

/* @var $this yii\web\View */
/* @var $model common\models\MasterOption */
/* @var $form yii\widgets\ActiveForm */
$this->registerJsFile('@web/js/product_option.js', ['depends' => [\yii\web\JqueryAsset::className()]]);

$fieldOptions1 = [
    'options' => ['class' => 'form-group '],
    'template' => '<div class="col-md-12"> 
                <div class="col-md-3 label-margin">  
                   {label}
                </div> 
                <div class="col-md-4">
                   {input}{error}
                </div>
                </div>'
];
?>

<div class="master-option-form">
    <div class="col-md-12">
        <?php
        $form = ActiveForm::begin([
                    'options' => [
                        'enctype' => 'multipart/form-data',
                        'id' => strtolower($model->formName()) . '-id',
                    ], // important
                    'validationUrl' => 'validate',
                    'enableClientValidation' => false,
                    'enableAjaxValidation' => true,
                    'validateOnChange' => false,
                    'validateOnBlur' => false,
                    'successCssClass' => '',
        ]);
        ?>
        <?= $form->field($model, '_role')->label(false)->hiddenInput([]) ?>
        <input type="hidden" id="masteroption-_action" value="<?= ($model->isNewRecord)?'create':'update' ?>"/>
        <?= $form->field($model, 'id_store_option')->label(false)->hiddenInput([]) ?>
        <div class="row">
            <div class="col-md-8">
                <div class="row">
                    <div class="col-md-3 label_update_option_ct">
                        <label class="control-label required-star" >
                            <?= Yii::t('backend', 'Store Id') ?>
                        </label>
                    </div>
                    <div class="col-md-4 input_store_ct">
                        <?=
                        $form->field($model, 'store_id', [
                            'template' => '{input}{error}'
                        ])->dropDownList($listStore, ['prompt' => Yii::t('backend', 'Please Select')])
                        ?>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-3 label_update_option_ct">
                        <label class="control-label required-star" >
                            <?= Yii::t('backend', 'Option name') ?>
                        </label>
                    </div>
                    <div class="col-md-4 input_store_ct">
                        <?=
                        $form->field($model, 'name', [
                            'template' => '{input}{error}'
                        ])->textInput(['maxlength' => true])
                        ?>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-3 label_update_option_ct">
                        <label class="control-label" >
                            <?= Yii::t('backend', 'When the same option selection') ?>
                        </label>
                    </div>
                    <?php $model->type = isset($model->type) ? $model->type : 0; ?>
                    <div class="col-md-6">
                        <?=
                        $form->field($model, 'type', [
                            'template' => '{input}{error}'
                        ])->radioList(Constants::LIST_OPTION_SELECT, ['separator' => '<br>'])
                        ?>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="form-group">
                <?=
                Html::a(Yii::t('backend', 'Product selection'), ['#'], [
                    'class' => 'btn common-button-submit',
                    'id' => 'btn-option',
                    'onclick' => 'init_product_slect(event)'
                ])
                ?>
                <?= $form->field($model, 'option_hidden')->hiddenInput(['id' => 'option_hidden'])->label(FALSE) ?>
            </div>
            <?php
            Modal::begin([
                'id' => 'productModal',
                'size' => 'SIZE_LARGE',
                'header' => '<div class="box-header with-border common-box-h4 col-md-8"><h4 class="text-title"><b>' . Yii::t('backend', 'Product selection') . '</b><h4></div>',
                'footer' => Html::button(Yii::t('backend', 'Close'), ['class' => 'btn common-button-default', 'data-dismiss' => "modal", 'id' => 'clear-option-select'])
                . Html::button(Yii::t('backend', 'Choice'), ['class' => 'btn common-button-submit', 'data-dismiss' => "modal", 'id' => 'save-option-select']),
                'footerOptions' => [
                    'class' => 'text-center',
                ],
                'closeButton' => FALSE,
                'options' => [
                    'data-backdrop' => 'static',
                    'data-keyboard' => 'false',
                ]
            ]);
            ?>  
            <div class="">
                <div class=" option_product_ct">
                    <div class="">
                        <div class="box-body content clear-padding clear-padding-top-bot">
                            <div class="col-md-12 clear-padding clear-padding-top-bot clear-form-group-2" id="category_product">
                                <?php
                                foreach ($listCategory as $key => $value) {
                                    $arrCategoryProduct = [];
                                    foreach ($listProduct[$key] as $key1 => $value1) {
                                        $arrCategoryProduct[$key1] = $value1;
                                    }
                                    if (count($arrCategoryProduct) > 0) {
                                        echo Collapse::widget([
                                            'items' => [
                                                // equivalent to the above
                                                [
                                                    'label' => $value,
                                                    'content' => $form->field($model, 'option', ['template' => '{input}'])
                                                            ->checkboxList($arrCategoryProduct, ['item' => function ($index, $label, $name, $checked, $value) {
                                                                    return '<div class="">' . Html::checkbox($name, $checked, ['value' => $value, 'label' => Html::encode($label), 'class' => 'option-checkbox',]) . '</div>';
                                                                }])->label(FALSE),
                                                        ],
                                                    ]
                                                ]);
                                            }
                                        }
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php Modal::end(); ?>
                    <?php Pjax::begin(['enablePushState' => false]); ?>
                    <?=
                    GridView::widget([
                        'dataProvider' => $list_product_select,
                        'layout' => "{pager}\n{items}",
                        'summary' => false,
                        'pager' => [
                            'firstPageLabel' => '<<',
                            'lastPageLabel' => '>>',
                            'prevPageLabel' => '<',
                            'nextPageLabel' => '>',
                            'options' => ['class' => 'pagination common-float-right'],
                        ],
                        'tableOptions' => [
                            'class' => 'table table-striped table-bordered text-center',
                            'id' => 'product_table',
                        ],
                        'columns' => [
                            [
                                'attribute' => 'name',
                                'label' => Yii::t('backend', 'Name1'),
                                'headerOptions' => [
                                    'class' => ['col-md-3']
                                ]
                            ],
                            [
                                'attribute' => 'unit_price',
                                'headerOptions' => [
                                    'class' => ['col-md-2']
                                ],
                                'value' => function ($model) {
                                    return '¥' . Yii::$app->formatter->asDecimal($model->unit_price, 0);
                                },
                            ],
                            [
                                'attribute' => 'rate',
                                'label' => Yii::t('backend', 'Tax'),
                                'headerOptions' => [
                                    'class' => ['col-md-2']
                                ]
                            ],
                            [
                                'attribute' => 'tax_display_method',
                                'value' => function ($model) {
                                    return ($model->tax_display_method == '00' || $model->tax_display_method == '01' || $model->tax_display_method == '02') ? Constants::LIST_TAX_INTERNAL_FOREIGN[$model->tax_display_method]:Yii::t('backend', 'No Setting');
                                },
                                'headerOptions' => [
                                    'class' => ['col-md-2']
                                ]
                            ],
                            [
                                'attribute' => 'time_require',
                                'value' => function ($model) {
                                    return $model->time_require . Yii::t('backend', 'Min');
                                },
                                'headerOptions' => [
                                    'class' => ['col-md-2']
                                ]
                            ],
                            [
                                'class' => 'yii\grid\ActionColumn', 'template' => "{delete}",
                                'buttons' => [
                                    //view button
                                    'delete' => function ($url, $model) {
                                        return '<span class="btn common-button-action delete_table_row" id="' . $model->id . '"  onclick="delete_table_row(this)">' . Yii::t('backend', 'Delete') . '</span>';
                                    },
                                ],
                                'headerOptions' => [
                                    'class' => ['text-center col-md-1']
                                ]
                            ],
                        ],
                    ]);
                    ?>
                    <?= Html::a(Yii::t('backend', 'Refrest'), Url::current(), ['style' => 'display: none', 'id' => 'refresh-data-modal']) ?>
                </div>
                <?php Pjax::end(); ?>

                <div class="from-group">
                    <?= Html::a(Yii::t('backend', 'Return'), ['index'], ['class' => 'btn btn-default common-button-default']) ?>
                    <?=
                    Html::submitButton(Yii::t('backend', 'Confirm'), [
                        'class' => 'btn common-button-submit',
                        'id' => 'btn-confirm',
                    ])
                    ?>
                </div>
                <?php
                Modal::begin([
                    'id' => 'userModal',
                    'size' => 'SIZE_LARGE',
                    'header' => '<div class="box-header with-border common-box-h4 col-md-8"><h4 class="text-title"><b>' . Yii::t('backend', 'Confirm Notice') . '</b></h4></div>',
                    'footer' => Html::button(Yii::t('backend', 'Close'), ['class' => 'btn common-button-default', 'data-dismiss' => "modal", 'id' => 'btn-close'])
                    . Html::submitButton(Yii::t('backend', 'Save'), ['class' => 'btn common-button-submit', 'id' => 'btn-submit']),
                    'closeButton' => FALSE,
                ]);
                ?>
                <?=
                Preview::widget([
                    "data" => [
                        'store_id' => [
                            'type' => 'select',
                            'label' => Yii::t('backend', 'Store Id')
                        ],
                        'name' => [
                            'type' => 'input',
                            'label' => Yii::t('backend', 'Option name')
                        ],
                        'product_table' => [
                            'type' => 'table-normal',
                            'column' => '"0,1,2,3,4"',
                            'table' => 'product_table'
                        ],
                    ],
                    "modelName" => $model->formName(),
                    'idBtnConfirm' => 'btn-confirm',
                    'formId' => strtolower($model->formName()) . '-id',
                    'btnClose' => 'btn-close',
                    'btnSubmit' => 'btn-submit',
                    'modalId' => 'userModal'
                ])
                ?>
                <?php
                Modal::end();
                ?>
                <?php ActiveForm::end(); ?>
    </div>
</div>
<style>

  .panel-heading .collapse-toggle[aria-expanded=true]:after {
    /* symbol for "opening" panels */
    font-family: 'Glyphicons Halflings';  /* essential for enabling glyphicon */
    content: "\e114";    /* adjust as needed, taken from bootstrap.css */
    float: right;        /* adjust as needed */
    color: grey;         /* adjust as needed */
  }
  .panel-heading .collapse-toggle.collapsed:after ,.panel-heading .collapse-toggle:after {
    /* symbol for "collapsed" panels */
    font-family: 'Glyphicons Halflings'; 
     float: right;        /* adjust as needed */
    color: grey;         /* adjust as needed */
    content: "\e080";    /* adjust as needed, taken from bootstrap.css */
  }
  .panel-heading .collapse-toggle {
      width: 100%;
      display: inherit;
  }
</style>
<script type="text/javascript">
    jQuery.fn.preventDoubleSubmission = function () {

        var last_clicked, time_since_clicked;

        $("#btn-submit").bind('click', function (event) {

            if (last_clicked)
                time_since_clicked = event.timeStamp - last_clicked;

            last_clicked = event.timeStamp;

            if (time_since_clicked < 2000)
                return false;

            return true;
        });
    };
    $('#w0').preventDoubleSubmission();
</script>