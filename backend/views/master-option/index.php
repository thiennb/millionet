<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use dmstr\widgets\Alert;

/* @var $this yii\web\View */
/* @var $searchModel common\models\MasterOptionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('backend', 'Product option master management');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <div class="col-lg-12 col-md-12">
        <div class="box box-info box-solid">
            <div class="box-header with-border">
                <h4 class="text-title"><b><?= $this->title ?></b></h4>
            </div>
            <div class="box-body content">
                <div class="col-md-12">
                    <div class="common-box">
                        <div class="row">
                            <div class="box-header with-border common-box-h4 col-md-4">
                                <h4 class="text-title"><b><?= Yii::t('backend', 'Product Option Search') ?></b></h4>
                            </div>
                        </div>
                        <div class="box-body">
                            <div class="col-md-5 box_search_option_ct">
                                <?php echo $this->render('_search', ['model' => $searchModel, 'listStore' => $listStore]); ?>                                
                            </div>
                        </div>
                    </div>
                    
                    <div class="common-box">
                        <div class="row">
                            <div class="box-header with-border common-box-h4 col-md-4">
                                <h4 class="text-title"><b><?= Yii::t('backend', 'Product Option List') ?></b></h4>
                            </div>
                        </div>
                        <div class="row">
                            <div class="box-body content">
                                <div class="col-md-7">
                                    <?= Html::a(Yii::t('backend', 'Create New'), ['create'], ['class' => 'btn common-button-submit common-float-right margin-bottom-10']) ?>
                  <?php
//                    Pjax::begin();
                    ?>                                      
                        <?=
                                    GridView::widget([
                                        'dataProvider' => $dataProvider,
                                        'layout' => "{pager}\n{summary}\n{items}",
                                        'summaryOptions' => ['class' => 'common-clr-fl-right'],
                                        'summary' => false,
                                        'pager' => [
                                            'class' => 'common\components\LinkPagerMillionet',
                                            'options' => ['class' => 'pagination common-float-right'],
                                        ],
                                        'tableOptions' => [
                                            'class' => 'table table-striped table-bordered text-center',
                                        ],
                                        'columns' => [
                                            ['class' => 'yii\grid\SerialColumn', 'header'=>'No','options' => ['class' => 'with-table-no']],
                                            [
                                                'attribute' => 'name',
//                                                'headerOptions' => [
//                                                    'class' => ['text-center col-md-10']
//                                                ]
                                            ],
                                            [
                                                'class' => 'yii\grid\ActionColumn', 'template' => "{delete}{update}",
                                                'options' => ['class' => 'with-table-button'],
                                                'buttons' => [
                                                    //view button
                                                    'delete' => function ($url, $model) {
                                                        return Html::a('<span class="fa fa-delete"></span>' . Yii::t('backend', 'Delete'), $url, [
                                                                    'title' => Yii::t('backend', 'Delete'),
                                                                    'class' => 'btn common-button-action',
                                                                    'aria-label' => "Delete",
                                                                    'data-confirm' => Yii::t('backend', "Delete Option Error Message"),
                                                                    'data-method' => "post"
                                                        ]);
                                                    },
                                                            'update' => function ($url, $model) {
                                                        return Html::a('<span class="fa fa-delete"></span>' . Yii::t('backend', 'Update'), $url, [
                                                                    'title' => Yii::t('backend', 'Update'),
                                                                    'class' => 'btn common-button-action',
                                                        ]);
                                                    },
                                                        ],
//                                                        'headerOptions' => [
//                                                            'class' => ['text-center col-md-2']
//                                                        ]
                                                    ],
                                                ],
                                            ]);
                                            ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
        <?php //  Pjax::end(); ?>
                </div>
            </div>
        </div>
    </div>
</div>