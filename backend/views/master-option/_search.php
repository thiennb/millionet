<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\components\Constants;
/* @var $this yii\web\View */
/* @var $model common\models\MasterOptionSearch */
/* @var $form yii\widgets\ActiveForm */
$fieldOptions1 = [
    'options' => ['class' => 'form-group '],
    'template' => '<div class="col-md-12"> 
                <div class="col-md-3 label-margin">  
                   {label}
                </div> 
                <div class="col-md-4 input_store_ct">
                   {input}{error}
                </div>
                </div>'
];
?>

<div class="master-option-search">

    <?php
    $form = ActiveForm::begin([
                'action' => ['index'],
                'method' => 'get',
                'enableClientValidation' => false,
    ]);
    ?>
    <div class="col-md-12">
        <?php
        $role = \common\components\FindPermission::getRole();
        if ($role->permission_id == Constants::STORE_MANAGER_ROLE || $role->permission_id == Constants::STAFF_ROLE) {
           // echo $form->field($model, 'store_id')->label(false)->dropDownList($searchListStore);
            echo $form->field($model, 'store_id', [
                'template' => '<div class="col-md-12"> 
                    <div class="col-md-3 label-margin label_master_ticket_ct">  
                       {label}
                    </div> 
                    <div class="col-md-6 input_store_ct">
                       {input}{error}
                    </div>
                    </div>'
            ])->dropDownList($listStore);
        } else {
            echo $form->field($model, 'store_id', [
                'template' => '<div class="col-md-12"> 
                    <div class="col-md-3 label-margin label_master_ticket_ct">  
                       {label}
                    </div> 
                    <div class="col-md-6 input_store_ct">
                       {input}{error}
                    </div>
                    </div>'
            ])->dropDownList($listStore, ['prompt' => Yii::t('backend', 'Please Select')]);
        }
        ?>
       
    </div>
    <div class="col-md-12">
        <?=
        $form->field($model, 'name', [
            'template' => '<div class="col-md-12"> 
                    <div class="col-md-3 label-margin label_master_ticket_ct">  
                       {label}
                    </div> 
                    <div class="col-md-6 input_store_ct">
                       {input}{error}
                    </div>
                    </div>'
        ])->textInput(['maxlength' => true])
        ?> 
    </div>
    <div class="col-md-12" style="margin-left: 2em;">
        <div class="form-group">
            <?= Html::a(Yii::t('backend', 'Return'), ['/'], ['class' => 'btn btn-default common-button-default']) ?>
            <?= Html::submitButton(Yii::t('backend', 'Search'), ['class' => 'btn common-button-submit']) ?>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
</div>
