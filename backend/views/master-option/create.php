<?php

use yii\helpers\Html;
use dmstr\widgets\Alert;

/* @var $this yii\web\View */
/* @var $model common\models\MasterOption */

$this->title = Yii::t('backend', 'Create Product Option');
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Product option master management'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row master-option-create">
    <div class="col-lg-12 col-md-12">
        <div class="row">
            <div class="form-group"><?= Alert::widget() ?></div>
        </div>
        <div class="box box-info box-solid add">
            <div class="box-header with-border">
                <h4 class="text-title"><b><?= $this->title ?></b></h4>
            </div>
            <div class="box-body content">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="box-header with-border common-box-h4 col-md-4">
                                <h4 class="text-title"><b><?= Yii::t('backend', 'Input data Product Option') ?></b></h4>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-8">
                            <?=
                            $this->render('_form', [
                                'model' => $model,
                                'listStore' => $listStore,
                                'listProduct'   =>  $listProduct,
                                'list_product_select'   =>  $list_product_select,
                                'listCategory' =>  $listCategory,
                            ])
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>