<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use common\models\MasterTypeSeat;
use common\components\Constants;
use common\models\MasterStore;
use yii\helpers\Url;
// Drop List
use kartik\depdrop\DepDrop;

/* @var $this yii\web\View */
/* @var $model common\models\MasterSeatSearch */
/* @var $form yii\widgets\ActiveForm */
$fieldOptions1 = [
    'options' => ['class' => 'form-group '],
    'template' => '<div class="col-md-12"> 
                <div class="col-md-3 label-margin label_master_ticket_ct">  
                   {label}
                </div> 
                <div class="col-md-4 ip_ticket_store">
                   {input}{error}
                </div>
                </div>'
];
?>

<div class="master-seat-search">

    <?php
    $form = ActiveForm::begin([
                'action' => ['index'],
                'enableClientValidation' => false,
                'method' => 'get',
                //'layout' => 'horizontal',
                'enableClientValidation' => false,
                'fieldConfig' => [
                    //'template' => "{beginWrapper}<div class='col-md-2'>{label}</div><div class='col-md-5'>{input}\n{hint}\n{error}</div>\n{endWrapper}",
                    'horizontalCssClasses' => [
                        'label' => '',
                        'wrapper' => 'row',
                        'error' => '',
                        'hint' => '',
                    ],
                ],
    ]);
    ?>
    <?= $form->field($model, 'store_id', $fieldOptions1)->dropDownList(MasterStore::getListStoreTypeSeatCustom(1), ['prompt' => Yii::t('backend', 'Please Select'), 'id' => 'masterseat-store_id']) ?>
    <?= $form->field($model, 'name', $fieldOptions1)->textInput(['maxlength' => 100]) ?>

    <?= $form->field($model, 'capacity_min', $fieldOptions1)->textInput(['maxlength' => 2]) ?>

    <?=
    $form->field($model, 'type_seat_id', $fieldOptions1)->widget(DepDrop::classname(), [
        'options' => ['id' => 'masterseat-type_seat_id'],
        'pluginOptions' => [
            'depends' => ['masterseat-store_id'],
            'placeholder' => ' ',
            'url' => Url::to(['/master-seat/selecttypeseatbyidstore'])
        ]
    ]);
    ?>
    <div class="col-md-12">
        <div class="col-md-3 font_label label_master_ticket_ct">
            <?= Yii::t('backend', 'Smoking Flg') ?>
        </div>
        <div class="col-md-9 font_label ip_ticket_store">
            <?php
            if ($model->smoking_flg == null) {
                $model->smoking_flg = 1;
            }
            echo $form->field($model, 'smoking_flg')->inline()->radioList(Constants::LIST_SMOKING)->label(false);
            ?>

        </div>
    </div>

    <div class="col-md-12">
        <div class="col-md-3 font_label label_master_ticket_ct">
            <?= Yii::t('backend', 'Show Flg Seat') ?>
        </div>
        <div class="col-md-6 font_label ip_ticket_store">
            <?php
            if ($model->show_flg == null) {
                $model->show_flg = 0;
            }
            echo $form->field($model, 'show_flg')->inline()->radioList(Constants::LIST_SHOW)->label(Yii::t('backend', 'Show'))->label(false);
            ?>

        </div>
    </div>

    <div class="form-group" style="margin-left: 1.5em;">
        <a href="<?php echo Url::to('/admin') ?>" class="btn btn-default common-button-default"><?php echo Yii::t('backend', 'Return') ?></a>
        <?= Html::submitButton(Yii::t('backend', 'Search'), ['class' => 'btn common-button-submit']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<script>
    $('#masterseat-type_seat_id').on('depdrop.init', function (event) {
        $('#masterseat-store_id').trigger('change');
    });

    $('#masterseat-type_seat_id').on('depdrop.afterChange', function (event, id, value) {
        $('#masterseat-type_seat_id').val(<?php echo $model->type_seat_id; ?>);
    });
</script>
