<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\MasterSeat */

$this->title = Yii::t('backend', 'Create Seat');
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Master Seats'), 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('backend', 'Update');
?>

<div class="row text-setting company">
    <div class="col-lg-12 col-md-12">
        <div class="box box-info box-solid add">
            <div class="box-header with-border">
                <h4 class="text-title"><b><?= $this->title ?></b></h4>
            </div>
            <div class="box-body content">
                <div class="col-md-12">
                    <div class="row">
                        <div class="box-header with-border common-box-h4 col-md-4">
                                <h4 class="text-title"><b><?= Yii::t('backend', 'Seat Infomation') ?></b></h4>
                         </div>
                        <div class="col-md-12">
                             <?= $this->render('_form', [
                                'model' => $model,
                             ]) ?>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
</div>

