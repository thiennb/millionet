<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\LinkPager;
use yii\widgets\Pjax;
use common\models\MasterNotice;
use common\components\Constants;
use common\models\MasterTypeSeat;

/* @var $this yii\web\View */
/* @var $searchModel common\models\MstProductSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('backend', 'Master Seat');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <div class="col-lg-12 col-md-12">
        <div class="box box-info box-solid">
            <div class="box-header with-border">
                <h4 class="text-title"><b><?= $this->title ?></b></h4>
            </div>
            <div class="box-body content">
                <div class="col-md-8">

                    <div class="common-box">
                        <div class="box-header with-border common-box-h4 col-md-4">
                            <h4 class="text-title"><b><?= Yii::t('backend', 'Seat Finder') ?></b></h4>
                        </div>
                        <div class="box-body content">
                            <div class="col-md-12">
                                <?php echo $this->render('_search', ['model' => $searchModel]); ?>                                
                            </div>
                        </div>
                    </div>

                    <?php
                    //    if(count($dataProvider)>1){
                    Pjax::begin();
                    ?>    
                    <div class="common-box">
                        <div class="box-header with-border common-box-h4 col-md-4">
                            <h4 class="text-title"><b><?= Yii::t('backend', 'Seat List') ?></b></h4>
                        </div>
                        <div class="box-body content">
                            <div class="col-md-12">
                                <?= Html::a(Yii::t('backend', 'Create'), ['create'], ['class' => 'btn common-button-submit common-float-right bt_seat_create', 'style' => 'margin-bottom: 0.5em;']) ?>
                                <?=
                                GridView::widget([
                                    'dataProvider' => $dataProvider,
                                    //        'filterModel' => $searchModel,
                                    'layout' => "{pager}\n{summary}\n{items}",
                                    'summaryOptions' => ['class' => 'common-clr-fl-right'],
                                    'summary' => false,
                                    'pager' => [
                                        'class' => 'common\components\LinkPagerMillionet',
                                        'options' => ['class' => 'pagination common-float-right'],
                                    ],
                                    'columns' => [
                                        ['class' => 'yii\grid\SerialColumn', 'header' => 'No', 'options' => ['class' => 'with-table-no']],
                                        [
                                            'attribute' => 'name',
                                        ],
//                                        [
//                                            'attribute' => 'type_seat_id',
//                                            'filter' => MasterTypeSeat::getListTypeSeat(),
//                                            'value' => function ($model) {
//                                                if (empty($model->seat)) {
//                                                    return '';
//                                                } else {
//                                                    return $model->seat->name;
//                                                }
//                                                //  return $model->seat->name;
//                                            },
//                                        ],
                                        [
                                            'attribute' => 'type_seat_name',
                                            'value' => 'type_seat_name'
                                        ],
                                        [
                                            'attribute' => 'capacity_min',
                                            'value' => function ($model) {
                                                return $model->capacity_min . '～' . $model->capacity_max;
                                            }
                                        ],
                                        [
                                            'attribute' => 'smoking_flg',
                                            'value' => function ($model) {
                                                return Constants::LIST_SMOKING[$model->smoking_flg];
                                            }
                                                ],
                                                [
                                                    'class' => 'yii\grid\ActionColumn', 'template' => "{delete} {update}",
                                                    'options' => ['class' => 'with-table-button'],
                                                    'buttons' => [
                                                        //view button
                                                        'delete' => function ($url, $model) {
                                                            return Html::a('<span class="fa fa-delete"></span>' . Yii::t('backend', 'Delete'), $url, [
                                                                        'title' => Yii::t('backend', 'Delete'),
                                                                        'class' => 'btn common-button-action',
                                                                        'aria-label' => "Delete",
                                                                        'data-confirm' => Yii::t('backend', "Are you sure you want to delete Seat"),
                                                                        'data-method' => "post"
                                                            ]);
                                                        },
                                                                'update' => function ($url, $model) {
                                                            return Html::a('<span class="fa fa-delete"></span>' . Yii::t('backend', 'Update'), $url, [
                                                                        'title' => Yii::t('backend', 'Update'),
                                                                        'class' => 'btn common-button-action',
                                                            ]);
                                                        },
                                                            ],
                                                        ],
                                                    ],
                                                ]);
                                                ?>
                                            </div>
                                        </div>
                                    </div>
                                                <?php
                                                Pjax::end();
                                                ?>
                </div>
            </div>
        </div>
    </div>
</div>
