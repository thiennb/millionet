<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use common\models\MasterTypeSeat;
use common\components\Constants;
use yii\bootstrap\Modal;
use common\widgets\preview\Preview;
use common\models\MasterStore;
use yii\widgets\Pjax;
// Drop List
use kartik\depdrop\DepDrop;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\MasterSeat */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="master-seat-form col-md-8">
    <?php
    $form = ActiveForm::begin([
                'layout' => 'horizontal',
                'options' => ['data-pjax' => true],
                'enableClientValidation' => false,
                'fieldConfig' => [
                    //'template' => "{beginWrapper}<div class='col-md-3'>{label}</div><div class='col-md-4'>{input}\n{hint}\n{error}</div>\n{endWrapper}",
                    'horizontalCssClasses' => [
                        'label' => '',
                        'wrapper' => 'row',
                        'error' => '',
                        'hint' => '',
                    ],
                    'horizontalCheckboxTemplate' => "{input}{label}\n{error}\n{hint}"
                ],
                // important
                'validationUrl' => 'validate',
                'enableClientValidation' => false,
                'enableAjaxValidation' => true,
                'validateOnChange' => false,
                'validateOnBlur' => false,
                'successCssClass' => '',
    ]);
    ?>
    <div class="col-md-12">
        <div class="col-md-2 label-margin required-star">
            <?=
                Yii::t("backend", "Store Id");
            ?>
        </div>
        <div class="col-md-4 input_store_ct">
            <?= $form->field($model, 'store_id')->label(false)->dropDownList(MasterStore::getListStoreTypeSeatCustom(0), ['prompt' => Yii::t('backend', 'Please Select'), 'id' => 'masterseat-store_id']) ?>
        </div>
    </div>
    <div class="col-md-12">
        <div class="col-md-2 label-margin required-star">
            <?=
                Yii::t("backend", "Name Seat");
            ?>
        </div>
        <div class="col-md-4 input_store_ct">
           <?= $form->field($model, 'name')->label(false)->textInput(['maxlength' => 100]) ?>
        </div>
    </div>
    <div class="col-md-12">
        <div class="col-md-2 label-margin required-star">
            <?=
                Yii::t("backend", "Type Seat Name");
            ?>
        </div>
        <div class="col-md-4 input_store_ct">
           <?=
            $form->field($model, 'type_seat_id')->widget(DepDrop::classname(), [
                'options' => ['id' => 'masterseat-type_seat_id'],
                'pluginOptions' => [
                    'depends' => ['masterseat-store_id'],
                    'placeholder' => ' ',
                    'url' => Url::to(['/master-seat/selecttypeseatbyidstore'])
                ]
            ])->label(false);
        ?>
        </div>
    </div>
    <div class="col-md-12">
            <div class="col-md-2 label-margin required-star">
            <?=
                Yii::t("backend", "Capacity Min");
            ?>
            </div>

            <div class="col-md-4 input_store_ct ct_seat_date">
                <?=
                $form->field($model, 'capacity_min', [
                    'options' => ['class' => 'col-md-5 no-padding'],
                    'template' => '<div class="clear-padding">{input}{error}</div>'
                ])->widget(\yii\widgets\MaskedInput::className(), [
                    'clientOptions' => [
                        'alias' => 'integer',
                        'autoGroup' => true,
                        'removeMaskOnSubmit' => true,
                        'allowMinus' => false,
                    ]
                ])->textInput(['maxlength' => '2'])
                ?> 
                <div class="text-center col-md-1 no-padding ct_seat_space">～</div>
                <?=
                $form->field($model, 'capacity_max', [
                    'options' => ['class' => 'col-md-5 no-padding'],
                    'template' => '<div class="clear-padding">{input}{error}</div>'
                ])->widget(\yii\widgets\MaskedInput::className(), [
                    'clientOptions' => [
                        'alias' => 'integer',
                        'autoGroup' => true,
                        'removeMaskOnSubmit' => true,
                        'allowMinus' => false,
                    ]
                ])->textInput(['maxlength' => '2'])
                ?>
            </div>
    </div>

    <div class="col-md-12">
        <div class="col-md-2 label-margin">
            <?= Yii::t('backend', 'Smoking Flg') ?>
        </div>
        <div class="col-md-4 font_label input_store_ct">
            <?php
                if ($model->smoking_flg == null) {
                    $model->smoking_flg = 1;
                }
                echo $form->field($model, 'smoking_flg')->inline()->radioList(Constants::LIST_SMOKING)->label(false);
            ?>
        </div>
    </div>

    <div class="col-md-12">
        <div class="col-md-2 font_label">
            <?= Yii::t('backend', 'Show Flg Seat') ?>
        </div>
        <div class="col-md-4 font_label input_store_ct">
            <?php
            if ($model->show_flg == null) {
                $model->show_flg = 0;
            }
            echo $form->field($model, 'show_flg')->inline()->radioList(Constants::LIST_SHOW)->label(Yii::t('backend', 'Show'))->label(false);
            ?>

        </div>
    </div>
    <div class="row">
        <?= Html::a(Yii::t('backend', 'Return'), ['index'], ['class' => 'btn btn-default common-button-default']) ?>
        <?=
        Html::submitButton(Yii::t('backend', 'Create Type Seat'), [
            'class' => 'btn common-button-submit',
            'id' => 'btn-confirm',
        ])
        ?>
    </div>

    <?php
    Modal::begin([
        'id' => 'userModal',
        'size' => 'SIZE_LARGE',
        'header' => '<div class="box-header with-border common-box-h4 col-md-8"><h4 class="text-title"><b>' . Yii::t('backend', 'Confirm input') . '</b></h4></div>',
        'footer' => Html::button(Yii::t('backend', 'Close'), ['class' => 'btn common-button-default', 'data-dismiss' => "modal", 'id' => 'btn-close'])
        . Html::submitButton(Yii::t('backend', 'Save'), ['class' => 'btn common-button-submit', 'id' => 'btn-submit']),
        'footerOptions' => ['class' => 'modal-footer text-center'],
        'closeButton' => FALSE,
    ]);
    ?>

    <?=
    Preview::widget([
        "data" => [
            'store_id' => [
                'type' => 'select',
                'label' => Yii::t('backend', 'Store Id')
            ],
            'name' => [
                'type' => 'input',
                'label' => Yii::t('backend', 'Name Seat')
            ],
            'type_seat_id' => [
                'type' => 'select',
                'label' => Yii::t('backend', 'Type Seat Name')
            ],
            'capacity_min' => [
                'type' => 'input',
                'label' => Yii::t('backend', 'Capacity Min'),
                'groupWithSeat' => 'capacity_max',
            ],
            'smoking_flg' => [
                'type' => 'radio',
                'label' => Yii::t('backend', 'Smoking Flg'),
            ],
            'show_flg' => [
                'type' => 'radio',
                'label' => Yii::t('backend', 'Show Flg Seat')
            ],
        ],
        "modelName" => $model->formName(),
        'idBtnConfirm' => 'btn-confirm',
        'btnClose' => 'btn-close',
        'btnSubmit' => 'btn-submit',
        'modalId' => 'userModal'
    ])
    ?>
    <?php Modal::end(); ?>
    <?php ActiveForm::end(); ?>  
</div>
<script>
    $(document).ready(function () {
        $('#masterseat-type_seat_id').on('depdrop.init', function (event) {
            $('#masterseat-store_id').trigger('change');
        });

        $('#masterseat-type_seat_id').on('depdrop.afterChange', function (event, id, value) {
            $('#masterseat-type_seat_id').val(<?php echo $model->type_seat_id; ?>);
        });
    });
</script>
