<?php

  use yii\helpers\Html;
  use yii\grid\GridView;
  use yii\jui\DatePicker;
  use yii\widgets\Pjax;

/* @var $this yii\web\View */
  /* @var $searchModel common\models\BookingSearch */
  /* @var $dataProvider yii\data\ActiveDataProvider */
  \backend\assets\BookingAsset::register($this);
  $this->title = Yii::t('backend', 'Master Bookings');
  $this->params['breadcrumbs'][] = $this->title;
?>

<style>

  .obj-booking{
    /*background-color: #e3a1a1 !important;*/
    padding: 20px !important;
    border-radius: 15px; 

    background: #ff5ca6 !important;

  }
  .obj-booking-disable{
    /*background-color: #e3a1a1 !important;*/
    padding: 20px !important;
    border-radius: 15px; 

    background: #e3e3e3 !important;

  }
  /*  #schedule-table{
  
      width: 750px;
      height: 500px;
      overflow: hidden;
  
    }*/
  .scroll-container{
    height: 800px;
    overflow: hidden;
  }
  .ui-datepicker{
    z-index : 999 !important;
  }
</style>
<?php Pjax::begin(); ?>
<div class="row">
  <div class="col-lg-12 col-md-12">
    <div class="box box-info box-solid">
      <div class="box-header with-border">
        <h4 class="text-title"><b><?= $this->title ?></b></h4>
      </div>
      <div class="box-body content">
        <div class="col-md-12">
          <div class="common-box">
            <div class="box-header with-border common-box-h4 col-md-6">
              <h4 class="text-title"><b><?= Yii::t('backend', 'Booking') ?></b></h4>
            </div>
            <div class="box-body content">
              <div class="col-md-12">
                    
                <?= Html::beginForm(['booking/schedule'], 'post', ['data-pjax' => '', 'class' => 'form-inline' ,'id' => "booking-schedule" ] ); ?>
                 
               
                <h3 ><?= $date ?></h3>
                
                <?=
                  DatePicker::widget([
                    'id' => 'dateBooking',
                    'name' => 'dateBooking',
                    'value'  => $date,
                    'language' => 'ja',
                    'dateFormat' => 'yyyy-MM-dd',
                    "clientOptions" => [
                      "changeMonth" => true,
                      "changeYear" => true,
                      "defaultDate"=>$date ,
                    ]
                  ])
                  
                ?>
                <?= Html::submitButton(Yii::t('backend', 'Select a date'), ['class' => 'btn btn-lg btn-primary', 'name' => 'hash-button']) ?>
                 <?= Html::endForm() ?>
                
                <div class="scroll-container">
                  <div id="schedule-table" ></div>
                </div>  
                
                
                
               
              </div>
            </div>
          </div>


        </div>
      </div>
    </div>
  </div>
</div>

<script>
$(document).on('ready pjax:success', function() {
  
  var data = <?= $data ?>;
  var mergeData = <?= $mergeData ?>;
  console.log(data);
  console.log(mergeData);
//  var window.gon.mergeData = $("#mergeData").val();
        console.log(window.gon.data);
//    var mergeData = [{row: 1, col: 1, rowspan: 3, colspan: 1}];
// console.log( window.gon.mergeData);
    var selection;
    var cellRenderer = function (instance, td, row, col, prop, value, cellProperties) {

      Handsontable.renderers.TextRenderer.apply(this, arguments);

      td.innerHTML = '';

      if (typeof (value) === "string" && value != '' && value.indexOf('@') != -1) {

        var arrValue = value.split("@");
        var mergecell = arrValue[0];
        var id = arrValue[1];
        var content = arrValue[2];
        console.log(mergecell);
        td.setAttribute("rowSpan", mergecell);
        //        td.className = 'obj-booking';
        td.className = 'obj-booking-disable';
        td.innerHTML = content;
        td.setAttribute("id", id);

      }
      else {
        td.className = '';

        td.innerHTML = '';
      }


    };
    var megRenderer = function (instance, td, row, col, prop, value, cellProperties) {
      Handsontable.renderers.TextRenderer.apply(this, arguments);

      if (typeof (value) === "string" && value != '') {
        td.innerHTML = '';

        var content = "staff";

        td.className = 'obj-booking';

        td.innerHTML = content;


      }


    };

    var container = document.getElementById('schedule-table');
    var hot = new Handsontable(container, {
      data: data,
      //        width: 500,
      //        height: 500,
      colWidths: 100,
      rowHeights: 80,
      //       rowHeaders: true,
      //    colHeaders: true,
      fixedRowsTop: 1,
      fixedColumnsLeft: 1,
      contextMenu: true,
      mergeCells: mergeData,
      readOnly: true,
      fillHandle: {
        autoInsertRow: false,
      },
      className: "htMiddle",
      //      cells: function (row, col, prop) {
      //        var cellProperties = {};
      //        cellProperties.readOnly = true;
      //        if (col != 0 && row != 0) {
      //          this.renderer = cellRenderer;
      //        }
      //        return cellProperties;
      //      },

      afterSelection: function () {

        var temp = [];
        $.each(window.gon.mergeData, function (i, val) {

          temp.push(val['row'] + val['rowspan'] - 1 + ',' + val['col']);
          temp.push(val['row'] + ',' + val['col']);
        });
        
        tempSelect = this.getSelected()[2] + ',' + this.getSelected()[3];
        
        if ($.inArray(tempSelect, temp) != -1) {
          this.deselectCell();
        
        }


      },
      afterSelectionEnd: function () {

        selection = this.getSelected();
        //        console.log(selection);
        //        var cell = hot.getCellMeta(selection[0], selection[1]);

        var spanRow = ((selection[2] - selection[0]) + 1).toString();
        if (selection[0] != 0 && selection[1] != 0) {
          selectionCell = hot.setDataAtCell(selection[0], selection[1], spanRow);
          mergeData.push({row: selection[0], col: selection[1], rowspan: (selection[2] - selection[0]) + 1, colspan: 1});


          hot.updateSettings({
            mergeCells: mergeData,
            cells: function (row, col, prop) {
              var cellProperties = {};
              cellProperties.readOnly = true;
              if (row == selection[0] && col == selection[1]) {
                this.renderer = megRenderer;
              } else if (col != 0 && row != 0) {
                this.renderer = cellRenderer;
              }
              return cellProperties;
            }


          });

          mergeData.pop();
        }
      


        var id = $(".current").attr('id');
        if (typeof (id) === "string" && id != '') {
                    alert(id);
        }
        //           echo 'Cellclick();';



      },
    });
    hot.updateSettings({
      //        mergeCells: [
      //          {row: 5, col: 1, rowspan: 3, colspan: 1 }
      ////          {row: 2, col: 2, rowspan: 2, colspan: 1}
      //        ],
      cells: function (row, col, prop) {
        var cellProperties = {};
        cellProperties.readOnly = true;
        if (col != 0 && row != 0) {
          this.renderer = cellRenderer;
        }
        return cellProperties;
      }


    });
  })
</script>
 <?php Pjax::end(); ?>