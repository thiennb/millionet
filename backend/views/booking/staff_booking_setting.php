<?php

use yii\helpers\Html;
use janisto\timepicker\TimePicker;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use common\models\MasterStore;
use yii\jui\DatePicker;
use yii\bootstrap\Modal;
use yii\widgets\Pjax;
use common\components\Constants;


/* @var $this yii\web\View */
/* @var $model common\models\Booking */

$this->title = Yii::t('backend', 'Staff reservations accepted setting');

$this->params['breadcrumbs'][] = $this->title;

$this->registerJsFile('@web/js/fullcalendar.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile('@web/js/jquery.dirtyforms.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
    
\backend\assets\BookingAsset::register($this);
?>
<?php Pjax::begin(['id'=>'p2'  ,'timeout'=>5000]) ?>
<style>
    .setting-staff td{
        width: 45px;
    }
    .vcenter {
        /*display: inline-block;*/
        vertical-align: middle;
        /*float: none;*/
    }
    .off-day{
        background-color: #e3e3e3 !important;
    }
</style>
<div class="row">
    <div class="col-lg-12 col-md-12">
        <div class="box box-info box-solid">
            <div class="box-header with-border">
                <h4 class="text-title"><b><?= $this->title ?></b></h4>
            </div>
            <div class="box-body content">
                <div class="col-md-12">
                    <div class="common-box">
                        <div class="row">
                            <div class="box-header with-border common-box-h4 col-md-4">
                                <h4 class="text-title"><b><?= Yii::t('backend', 'Staff reservations accepted setting') ?></b></h4>
                            </div>
                        </div>


                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <?= Html::a(Yii::t('backend', 'Shift setting'), Url::to('/admin/store-shift'), ['class' => 'btn common-button-submit']); ?>
                            <br>
                            <br>

                            <?php
                            $form = ActiveForm::begin([
                                        'id'     => 'staff-setting',
                                        'method' => 'get',
                                        'action' => 'staff-booking-setting', 
                                        'options' => [
                                            'class' => 'form-inline',
                                            'data-pjax' => true
                                        ]
                            ]);
                            ?>
                            <?= Html::label(Yii::t('backend', 'Select a store')); ?>        
                            <?= Html::dropDownList('selectStore', $selectStore, MasterStore::getListStore(), ['class' => 'form-control']) ?>
                            <br>
                            <br>
                            <?= Html::dropDownList('selectYear', $selectYear, $years, ['class' => 'form-control']) ?>
                            <?= Html::label(Yii::t('backend', 'Year')); ?>      
                            <?= Html::dropDownList('selectMonth', $selectMonth, $months, ['class' => 'form-control']) ?>
                            <?= Html::label(Yii::t('backend', 'Month')); ?> 
                            <?= Html::submitButton(Yii::t('backend', 'Show'), ['class' => 'btn common-button-submit', 'id' => 'btn-show']); ?>
                            <?php ActiveForm::end(); ?>
                            <br>
                            <br>
                        </div>

                    </div>
                    <div class="setting-staff">

                        <br>

                        <div class="row text-center" style="overflow: auto">
                            <?php
                            if ($staffCount > 0) {
                                echo Html::beginTag('table', ['class' => 'table table-striped table-bordered text-center']);
                                // for hable head
                                echo Html::beginTag('tr');
                                foreach ($days as $key => $day) {

                                    echo Html::tag('th', $key != 0 ? explode('-', $day)[2] : '', ['data-staff' => $day, 'data-day' => $day, 'id' => $day]);
                                }
                                echo Html::endTag('tr');

                                foreach ($datasStaff as $staff) {
                                    echo Html::beginTag('tr');
                                    foreach ($staff as $item) {
                                        if ($item['day'] == 0) {
                                            $button = Html::a(Yii::t('backend', "Batch setting"), '#', ['class' => 'btn common-button-submit btn-batch btn-bulk-setting']);
//                          $button = '<br><button class="btn common-button-submit" type="submit">' . Yii::t('backend', "Batch setting") . '</button>';
                                            echo Html::tag('td', $item['name'] . $button, ['data-staff' => $item['id'], 'data-day' => $item['day'], 'id' => "1"]);
                                        } else {
                                            echo Html::tag('td', $item['work'], ['data-staff' => $item['id'], 'data-day' => $item['day'], 'data-store' => $selectStore, 'class' => 'cell-input vcenter ' . $item["class"]]);
                                        }
                                    }
                                    echo Html::endTag('tr');
                                }

                                echo Html::endTag('table');
                            } else {

                                echo Html::tag('h4', Yii::t('backend', "Store don't have any staff"), ['class' => 'text-center']);
                            }
                            ?>

                        </div>


                    </div>


                    <div class="row text-center">
                        <?= Html::a(Yii::t('backend', 'Return'), Url::to('/admin'), ['class' => 'btn btn-default common-button-default', 'id' => 'btn-back']) ?>
                        <?= Html::button(Yii::t('backend', 'Set'), ['class' => 'btn common-button-submit', 'id' => 'btn-save-all']) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
         year = '<?php echo $selectYear ?>';
         month = '<?php echo $selectMonth ?>';
         store = '<?php echo $selectStore ?>';
      
    })
</script>
<?php Pjax::end() ?> 
<!--modal popup when choose option--> 
<?php
Modal::begin([
    'id' => 'staffSettingModal',
    'size' => 'SIZE_LARGE',
    'closeButton' => FALSE,
    'header' => '<div class="box-header with-border common-box-h4 col-md-8"><h4 class="text-title"><b id="title-modal">' . Yii::t('backend', 'Confirm input') . '</b></h4></div>',
    'footer' => Html::button(Yii::t('backend', 'Close'), ['class' => 'btn common-button-default', 'data-dismiss' => "modal", 'id' => 'btn-close'])
    . Html::button(Yii::t('backend', 'Set'), ['class' => 'btn common-button-submit', 'id' => 'btn-option']),
]);
?>
<div id="modalStaffSetting"> 

</div>
<?php
Modal::end();
?> 

<script>
    $(document).ready(function () {
        $(document).on('click', '.cell-input', function () {

            var id = $(this).data('staff');
            var day = $(this).data('day');
            var store = $(this).data('store');
            $('#staffSettingModal').modal('toggle')
                    .find('#modalStaffSetting')
                    .load('setting-staff-update', {
                        staffId: id,
                        day: day,
                        store: store
                    });
        })
        $(document).on('click', '#btn-add-schedule', function () {

            var template = $('.template-work-off .row').clone(true).show();

            $('#work-off').append(template);
        })

        $('body').on('beforeSubmit', 'form#form-staff-setting-day', function () {
            var form = $(this);
            // return false if form still have some validation errors
            if (form.find('.has-error').length) {
                return false;
            }
            // submit form
            $.ajax({
                url: form.attr('action'),
                type: 'post',
                data: form.serialize(),
                success: function (response) {
                    // close modal
//                    $('#btn-show').click();
                    $.pjax.reload({container: "#p2", async: false});
                    $('#staffSettingModal').modal('toggle');
                }
            });
            return false;
        });
        
        
        //save all change
        $(document).on('click', '#btn-save-all', function () {
            waitingDialog.show();
            $.ajax({
              type: "GET",
              url: SITE_ROOT + "/booking/save-all-staff-work-day",
            })
            .success(function (data) {
                waitingDialog.hide();
              location.reload();
            })
            .error(function (jqXHR, textStatus, errorThrown) {
                waitingDialog.hide();
                console.log(errorThrown);
                alert('システムエラーが発生しました。システム管理者にお問い合わせください。');
//                location.reload();
            });
        });
        isFormDirty = false ;
        $(document.body).on("beforeSubmit", "form#staff-setting", function (e) {
            //comfirm change 
            if (isFormDirty) {
            
            bootbox.dialog({
                title: '<div class="box-header with-border common-box-h4 col-md-8"><h4 class="text-title"><b>' + MESSAGE.WARNING + '</b></h4></div>',
                message: MESSAGE.SAVE_BEFORE_CHANGE,
                closeButton: false,
                buttons: {
                    success: {
                        label: 'OK',
                        className: "btn common-button-action text-center",
                        callback: function () {
                          isFormDirty = false;
                          $('#staff-setting').submit();
                        }
                    },
                    danger: {
                        label: 'キャンセル',
                        className: "btn btn-default text-center",
                        callback: function () {
                        }
                    }
                }
            });
            return false;
          }
          
        });
        

    });
    $(document).on('click', '#btn-option', function () {
        if (validateStaffWork()) {
            $("#form-staff-setting-day").submit();
            //set form have change will confirm when change month
            isFormDirty = true;
        }
    });
    function validateStaffWork() {
        var result = true;
        var list_staff = [];
        var $start = $('#work-off [name="start_work_off[]"]');
        var $end = $('#work-off [name="end_work_off[]"]');
        for (var i = 0; i < $start.length; i++) {
            list_staff.push({start: $start[i].value, end: $end[i].value});
            clearError($start[i]);
            clearError($end[i]);
        }
        for (var i = 0; i < list_staff.length; i++) {
            clearError($start[i]);
            if (list_staff[i].start >= list_staff[i].end) {
                addError("開始時間以降の時間を指定してください。", $end[i]);
                result = false;
            }
            for (var j = 0; j < list_staff.length; j++) {
                if (list_staff[i].end > list_staff[j].start && list_staff[i].end <= list_staff[j].end && i !== j) {
                    addError("他の時間の範囲を重複してはいけません。", $end[i]);
                    result = false;
                }
            }
        }
        return result;
    }
    
    $(document).on('click', '.btn-remove', function () {
      $(this).closest('.row-padding').remove();
      
    });

    function addError(message, element) {
        $(element).parent().find("p").remove();
        $(element).after('<p class="help-block help-block-error">' + message + '</p>');
        $(element).parent().addClass("has-error form-group");
    }

    function clearError(element) {
        $(element).parent().removeClass("has-error form-group");
        $(element).parent().find("p").remove();
    }

</script>



<!-- BEGIN html for setting bulk staff-booking -->
<!-- HTML for template notworking -->
<div class="template-bulk-work-off hidden">
    <div class="row row-padding" style="margin-top: 15px">
        <div class="col-md-3 ">
            <?= Html::dropDownList('notworking[{{key}}][start_time]', null, Constants::LIST_OPTION_SELECT_TIME, ['class' => 'form-control start_time']) ?>
        </div>
        <div class="col-md-1 ">
            ～
        </div>
        <div class="col-md-3">
            <?= Html::dropDownList('notworking[{{key}}][end_time]', null, Constants::LIST_OPTION_SELECT_TIME, ['class' => 'form-control end_time']) ?>
        </div>
        <div class="col-md-3">
            <?= Html::textInput('notworking[{{key}}][reason]', null , ['class' => 'form-control', 'style' => 'width: 100%']) ?>
        </div>
        <div class="col-md-2">
            <i class="fa fa-times fa-2x btn-remove"  style="color:red;cursor: pointer;"></i>
        </div>
    </div>
</div> 
<!-- END for template notworking -->
<div class="modal fade" id="bulkStaffBookingSettingModal" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <div class="common-box">
                <div class="row">
                    <div class="box-header with-border common-box-h4 col-md-4">
                        <h4 class="text-title"><b><?= Yii::t('backend', 'Store bulk reservation reception setting') ?></b></h4>
                    </div>
                </div>
            </div>
        </div>
        <?php
            $form = ActiveForm::begin([
                    'options' => [
                        'class' => 'form-inline'
                    ],
                    'method' => 'POST',
                    'id' => 'bulk-staff-booking-setting-form'
            ]);
        ?>
        <div class="modal-body">
            <div id="w2-danger" class="alert-danger alert hidden">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <i class="icon fa fa-warning"></i><span></span>
            </div>
            <div id="bulk-staff-booking-setting-content">
                
            </div>
        </div>
        <div class="modal-footer">
            <div class="text-center" style="margin-top: 10px">
                <?= Html::button(Yii::t('backend', 'Close'), ['class' => 'btn btn-default common-button-default', 'data-dismiss' => 'modal']); ?>
                <?= Html::submitButton(Yii::t('backend', 'Set'), ['class' => 'btn common-button-submit']); ?>
            </div>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
  </div>
</div>

<script>
 
$(document).ready(function(){
    $(document).on('click','.btn-bulk-setting',function(){
        $('#w2-danger').addClass('hidden');
        $('#bulkStaffBookingSettingModal').modal('show');
        
        
        var staff_id = $(this).closest('td').attr('data-staff');

        // Call ajax load
        $.ajax({
            type: 'POST',
            url: 'bulk-staff-booking-setting',
            data: {
                year: year,
                month: month,
                store_id: store,
                staff_id: staff_id
            },
            success: function(response){
                $('#bulk-staff-booking-setting-content').html(response);
            },
            error: function(){
                
            }
        });
    });
    
    function checkIsAllChecked(index){
        var isAll = true;
        
        $('#bulk-staff-booking-setting-form .fc-body .fc-content-skeleton thead tr td:nth-child('+index+') input').each(function(){
            if(!$(this).is(':checked')){
                isAll = false;
            }
        });
        
        return isAll;
    }
    
    /**
     * Event when click on submit
     */
    $(document).on('change', '#bulk-staff-booking-setting-form .fc-body .fc-content-skeleton thead tr td input', function(){
        var index = $(this).closest('td').index() + 1;
        $('#bulk-staff-booking-setting-form .fc-head tr th:nth-child('+index+') input').prop('checked', checkIsAllChecked(index));
    });
    
    /**
     * Event when click on checkbox header
     */
    $(document).on('change', '.cbx-mon, .cbx-tues, .cbx-wed, .cbx-thurs, .cbx-fri, .cbx-statur, .cbx-sun', function(){
        var isChecked = $(this).is(':checked');
        var index = ($(this).closest('th').index() + 1);
        $('#bulk-staff-booking-setting-form .fc-body .fc-content-skeleton thead tr td:nth-child('+index+') input').prop('checked', isChecked);
    });
    
    /**
    * Valid time
    * @returns boolean
    */
    function validTime(){
        var status = true;
        $('#bulk-workoff-content .row-padding').each(function(){
            var start = $(this).find('.start_time').val();
            var end = $(this).find('.end_time').val();

            if(start >= end){
                status =  false;
            }
        });
        
        return status;
    }

    /**
    * Event when submit change
    */
    $(document).on("submit", "#bulk-staff-booking-setting-form", function (e) {
        e.preventDefault();
        if(!validTime()){
            $('#w2-danger').removeClass('hidden');
            $('#bulk-staff-booking-setting-form #w2-danger span').text('開始時間以降の時間を指定してください');
            return;
        }
        
        var data = $(this).serialize();

        $.ajax({
            type: 'POST',
            data: data,
            url: 'ajax-set-session-bulk-staff-booking',
           // dataType : "json",
            success: function(response){
                console.log(response);
                      $.pjax.reload({container: "#p2", async: false,timeout : false});
//                    if(!response.status){
//                        alert(1);
//                    }
            },
            error: function(){
            }
        });

        $('#bulkStaffBookingSettingModal').modal('hide');
    });
    
    /**
    * Event when click on add shedule button
    */
    $(document).on('click', '#btn-add-bulk-workoff', function(){
        var content = $('.template-bulk-work-off').html();
        //$('#bulk-add-schedule-content').append();
        
        var count = parseInt($('.count-not-working').val());
        if(isNaN(count)){
            count = 0;
        }
        
        content = content.replace(/\{\{key\}\}/gi, count);
        
        $('#bulk-workoff-content').append(content);
        $('.count-not-working').val(count++);
    });
    
    /**
     * Event when change on work_flg
     */
    $(document).on('change', '#bulk-staff-booking-setting-form input[name="work_flg"]', function(){
        if($(this).val() == 0){
            $('.not-working-area input, .not-working-area select, .not-working-area button, .not-working-area .btn').prop('disabled', true);
        }else{
            $('.not-working-area input, .not-working-area select, .not-working-area button, .not-working-area .btn').prop('disabled', false);
        }
    });
});
</script>
<!-- END html for setting bulk staff-booking -->
