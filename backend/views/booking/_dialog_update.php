<?php

  use yii\helpers\Html;
  use yii\widgets\ActiveForm;
  use Carbon\Carbon;

/* @var $this yii\web\View */
  /* @var $model common\models\Booking */
  /* @var $form yii\widgets\ActiveForm */
?>
<style>
  .dialogContent {
    font-size: 22px;
  }
  .btn-sm{
     font-size: 12px; 
  }
</style>
<div class="master-booking-form">

  <?php $form = ActiveForm::begin(); ?>

  <?= Yii::t('backend', 'Customer name') ?> : <?= $model->customer_id ?> <br>

  <?= Yii::t('backend', 'Contact') ?> : <?= $model->customer_id ?> <br>

  <?= Yii::t('backend', 'Menu') ?> : <?= $model->menu_coupon_id ?> <br>

  <?= Yii::t('backend', 'Option') ?> : <?= $model->menu_coupon_id ?> <br>

  <?= Yii::t('backend', 'Coupon') ?> : <?= $model->coupon_id ?><br>

  <?= Yii::t('backend', 'Date time booking') ?> : <?= $model->start_time . " ～ " . $model->end_time ?>
  
  <br>

  <?= Yii::t('backend', 'Booking status') ?> : <?= $model->status ?>
  <?= Html::a(Yii::t('backend', 'Approval'), "", ['class' => 'btn btn-sm common-button-submit ']) ?>
  <?= Html::a(Yii::t('backend', 'Denial'), "", ['class' => 'btn btn-sm common-button-submit ']) ?>
  <?= Html::a(Yii::t('backend', 'Cancel'), "", ['class' => 'btn btn-sm common-button-submit ']) ?>
  <br>

  <?= Yii::t('backend', 'Responsible') ?> : <?= $model->staff->staff_name_kana ?><br>

  <?= Yii::t('backend', 'Seat') ?> : <?= $model->seat_id ?><br>

  <?= Yii::t('backend', 'Demand') ?> : <?= $model->demand ?><br>

  <?= Yii::t('backend', 'Memo') ?> : <?= $model->memo ?><br>
  
  <?= Yii::t('backend', 'Create at') ?> : <?= \Yii::$app->formatter->asDate($model->created_at, 'php:Y-m-d');  ?><br>

  <?= Yii::t('backend', 'Update at') ?> : <?= \Yii::$app->formatter->asDate($model->updated_at, 'php:Y-m-d');   ?><br>


  <?php ActiveForm::end(); ?>

</div>
