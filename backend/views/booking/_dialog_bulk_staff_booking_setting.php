<?php

    use yii\helpers\Html;
    use janisto\timepicker\TimePicker;
    use yii\widgets\ActiveForm;
    use common\models\MasterStore;
    use yii\jui\DatePicker;
    use yii\helpers\Url;
    use common\components\Constants;
?>

<div class="row">
    <div class="col-md-12">
        <!-- Hidden infomation -->
        <?= Html::hiddenInput('year', $year) ?>
        <?= Html::hiddenInput('month', $month) ?>
        <?= Html::hiddenInput('store_id', $store->id) ?>
        <?= Html::hiddenInput('staff_id', $staff->id) ?>
        <!-- End hidden information -->

        <div class="row">
            <div class="col-md-12 ">
                <h3><b><?php echo $year.Yii::t('backend', 'Year').$month.Yii::t('backend', 'Month') ?></b>  <b style="margin-left: 10px;"><?php echo $store->name; ?></b>  <b style="margin-left: 10px;"><?php echo $staff->name; ?></b></h3>
            </div>
        </div>

        <div class="row">
            <div class="col-md-2"><b><?php echo Yii::t('backend', 'Working shift') ?></b></div>
            <div class="col-md-2 col-lg-2"><?= Html::radio('work_flg', true, ['label' => '出勤', 'value' => 1]); ?></div>
            <div class="col-md-7 not-working-area">
                <?= Html::dropDownList("shift_id", null, $storeShift, ['class' => "form-control", "id" => "idBulkTimeEnd"]); ?>
                <div id="bulk-workoff-content">
                    <input value="<?= count($staffNotWorkingHours) ?>" class="count-not-working" type="hidden"/>
                    <?php foreach($staffNotWorkingHours as $key => $nothours): ?>
                        <div class="row row-padding" style="margin-top: 15px">
                            <?= Html::hiddenInput('notworking['.$key.'][id]', $nothours['id']) ?>
                            <div class="col-md-3 ">
                                <?= Html::dropDownList('notworking['.$key.'][start_time]', $nothours['start_time'], Constants::LIST_OPTION_SELECT_TIME, ['class' => 'form-control start_time']) ?>
                            </div>
                            <div class="col-md-1 ">
                                ～
                            </div>
                            <div class="col-md-3">
                                <?= Html::dropDownList('notworking['.$key.'][end_time]', $nothours['end_time'], Constants::LIST_OPTION_SELECT_TIME, ['class' => 'form-control end_time']) ?>
                            </div>
                            <div class="col-md-3">
                                <?= Html::textInput('notworking['.$key.'][reason]', $nothours['reason'] , ['class' => 'form-control', 'style' => 'width: 100%']) ?>
                            </div>
                            <div class="col-md-2">
                                <i class="fa fa-times fa-2x btn-remove"  style="color:red;cursor: pointer;"></i>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
                <?= Html::button(Yii::t('backend', 'Add a schedule'), ['class' => 'btn common-button-submit' ,'id'=>'btn-add-bulk-workoff','disabled'=> false, 'style' => 'margin-top: 0.6em; margin-left: 0']) ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-2"></div>
            <div class="col-md-2"><?= Html::radio('work_flg', false, ['label' => '休日', 'value' => 0]); ?></div>
        </div>
    </div>
</div>
                
<!-- BEGIN html for calendar -->
<div class="row" style="margin-top: 10px">
    <div class="master-booking-create col-md-12">
        <div id='fulk-calendar' class="table-responsive"></div>
    </div>
</div>
<!-- END html for calendar -->
            
<!-- BEGIN render calendar to #fulk-calendar -->
<style>
    .fc-event{
        border: none;
    }
</style>
<script id="bulk-reception-setting-template" type="text/x-handlebars-template">
    <?= Html::checkbox('{{{name}}}', false, ['label' => false, 'value' => 0]); ?>
</script>
<!-- END render calendar to #fulk-calendar -->

<script> 
    $(document).ready(function () {
        /**
         * Set full calendar
         * @return void
         */
        $('#fulk-calendar').fullCalendar({
            defaultDate: new Date(<?= $year ?>, <?= $month ?> - 1, 1),
            dayNames: ['日', '月', '火', '水', '木', '金', '土'],
            dayNamesShort: ['日', '月', '火', '水', '木', '金', '土'],
            firstDay: 1,
            header: false,
            eventSources: [
                // your event source
                {
                    //get from scheduleMonth gon in controller
                    events: <?php echo json_encode($staffSchedules) ?>
                }
            ],
                    
            // put your options and callbacks here
            defaultView: 'month',
            dayRender: function (date, element, view) {
                $(element).css("background", "red");
            },
                    
            //        dayRender: function (date, cell) {
            //      console.log("day"); 
            //                        cell.append("1");
            //                    
            //    },
            eventRender: function (event, element) {
                //alert($(element).html());
                element.find(".fc-event-title").remove();
                element.find(".fc-title").remove();

                element.find(".fc-event-time").remove();
                var theTemplateScript = $("#bulk-reception-setting-template").html();
                var theTemplate = Handlebars.compile(theTemplateScript);

                var theCompiledHtml = theTemplate(event);
                element.append(theCompiledHtml);
            },
                    
            eventAfterAllRender: function () {
                var name = $('#bulk-staff-booking-setting-form .fc-head tr th:nth-child(1)').text();
                $('#bulk-staff-booking-setting-form .fc-head tr th:nth-child(1)').html('<label style="display: inline-block"><span style="display: inline-block; margin-right: 10px" ><input type="checkbox" class="cbx-mon"/></span>' + name+'</label>');
                
                name = $('#bulk-staff-booking-setting-form .fc-head tr th:nth-child(2)').text();
                $('#bulk-staff-booking-setting-form .fc-head tr th:nth-child(2)').html('<label style="display: inline-block"><span style="display: inline-block; margin-right: 10px" ><input type="checkbox" class="cbx-tues"/></span>' + name+'</label>');
                
                name = $('#bulk-staff-booking-setting-form .fc-head tr th:nth-child(3)').text();
                $('#bulk-staff-booking-setting-form .fc-head tr th:nth-child(3)').html('<label style="display: inline-block"><span style="display: inline-block; margin-right: 10px" ><input type="checkbox" class="cbx-wed"/></span>' + name+'</label>');
                
                name = $('#bulk-staff-booking-setting-form .fc-head tr th:nth-child(4)').text();
                $('#bulk-staff-booking-setting-form .fc-head tr th:nth-child(4)').html('<label style="display: inline-block"><span style="display: inline-block; margin-right: 10px" ><input type="checkbox" class="cbx-thurs"/></span>' + name+'</label>');
                
                name = $('#bulk-staff-booking-setting-form .fc-head tr th:nth-child(5)').text();
                $('#bulk-staff-booking-setting-form .fc-head tr th:nth-child(5)').html('<label style="display: inline-block"><span style="display: inline-block; margin-right: 10px" ><input type="checkbox" class="cbx-fri"/></span>' + name+'</label>');
                
                name = $('#bulk-staff-booking-setting-form .fc-head tr th:nth-child(6)').text();
                $('#bulk-staff-booking-setting-form .fc-head tr th:nth-child(6)').html('<label style="display: inline-block"><span style="display: inline-block; margin-right: 10px" ><input type="checkbox" class="cbx-statur"/></span>' + name+'</label>');
                
                name = $('#bulk-staff-booking-setting-form .fc-head tr th:nth-child(7)').text();
                $('#bulk-staff-booking-setting-form .fc-head tr th:nth-child(7)').html('<label style="display: inline-block"><span style="display: inline-block; margin-right: 10px" ><input type="checkbox" class="cbx-sun"/></span>' + name+'</label>');
                
                $('#bulk-staff-booking-setting-form .fc-body .fc-content-skeleton tbody').each(function(){                    
                    var label = $(this).find('tr td:nth-child(1) label').html();
                    var value = $(this).closest('table').find('thead tr td:nth-child(1)').text();
                    if(label != undefined)
                        $(this).closest('table').find('thead tr td:nth-child(1)').html('<label style="display: inline-block"><span style="display: inline-block; margin-right: 10px" >' + label +  '</span>' + value+'</label>');
                    
                    label = $(this).find('tr td:nth-child(2) label').html();
                    value = $(this).closest('table').find('thead tr td:nth-child(2)').text();
                    if(label != undefined)
                        $(this).closest('table').find('thead tr td:nth-child(2)').html('<label style="display: inline-block"><span style="display: inline-block; margin-right: 10px" >' + label +  '</span>' + value+'</label>');
                    
                    label = $(this).find('tr td:nth-child(3) label').html();
                    value = $(this).closest('table').find('thead tr td:nth-child(3)').text();
                    if(label != undefined)
                        $(this).closest('table').find('thead tr td:nth-child(3)').html('<label style="display: inline-block"><span style="display: inline-block; margin-right: 10px" >' + label +  '</span>' + value+'</label>');
                    
                    label = $(this).find('tr td:nth-child(4) label').html();
                    value = $(this).closest('table').find('thead tr td:nth-child(4)').text();
                    if(label != undefined)
                        $(this).closest('table').find('thead tr td:nth-child(4)').html('<label style="display: inline-block"><span style="display: inline-block; margin-right: 10px" >' + label +  '</span>' + value+'</label>');
                    
                    label = $(this).find('tr td:nth-child(5) label').html();
                    value = $(this).closest('table').find('thead tr td:nth-child(5)').text();
                    if(label != undefined)
                        $(this).closest('table').find('thead tr td:nth-child(5)').html('<label style="display: inline-block"><span style="display: inline-block; margin-right: 10px" >' + label +  '</span>' + value+'</label>');
                    
                    label = $(this).find('tr td:nth-child(6) label').html();
                    value = $(this).closest('table').find('thead tr td:nth-child(6)').text();
                    if(label != undefined)
                        $(this).closest('table').find('thead tr td:nth-child(6)').html('<label style="display: inline-block"><span style="display: inline-block; margin-right: 10px" >' + label +  '</span>' + value+'</label>');
                    
                    label = $(this).find('tr td:nth-child(7) label').html();
                    value = $(this).closest('table').find('thead tr td:nth-child(7)').text();
                    if(label != undefined)
                        $(this).closest('table').find('thead tr td:nth-child(7)').html('<label style="display: inline-block"><span style="display: inline-block; margin-right: 10px" >' + label +  '</span>' + value+'</label>');
                    $(this).remove();
                });
            }
            //        eventRender: function( event, element, view ) {
            //               element.find('.fc-title').prepend('<span class="glyphicon"></span> '); 
            //        }
        });
    });
</script>