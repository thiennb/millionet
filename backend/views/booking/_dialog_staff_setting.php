<?php

    use yii\helpers\Html;
    use yii\widgets\ActiveForm;
    use Carbon\Carbon;
    use common\models\StoreShift;
    use common\components\Constants;

/* @var $this yii\web\View */
    /* @var $model common\models\Booking */
    /* @var $form yii\widgets\ActiveForm */
?>
<style>
  .dialogContent {
    font-size: 22px;
  }
  .btn-sm{
    font-size: 12px; 
  }
  .row-padding{
    margin-top: 15px;
  }
</style>
<div class="master-booking-form">

  <?php $form = ActiveForm::begin([
                'action' => 'save-staff-work-day2', 
                'options' => [
                    'id' =>  'form-staff-setting-day',
                ], // important
                ]); ?>
  <div class="row"> 
    <div class="col-md-12">
      <div class="row">   
        <div class="col-md-2" >  
          <?= Html::radio('work_flg', $model['work_flg'] == 1 || $model['work_flg'] == null  ? true : false , ['value' => 1]); ?>
          <?= Html::label(Yii::t('backend', 'Working')); ?>
        </div> 
        <div class="col-md-10">
          <div class="shift-select">
            <?= Html::dropDownList('shift_id', $model['shift_id'], StoreShift::getAllShift($store_id), ['class' => 'form-control disabled','disabled'=>$model['work_flg'] == '1' || $model['work_flg'] == null ? false : true ]) ?>
          </div>
          <div class="work-off " id="work-off">
              <?php if(!empty($modelNotWorking)) {
                        foreach ($modelNotWorking as $item) { ?>
                            <div class="row row-padding">
                                <div class="col-md-3 ">
                                    <?= Html::dropDownList('start_work_off[]',$item['start_time'] , Constants::LIST_OPTION_SELECT_TIME, ['class' => 'form-control add-time-off']) ?>
                                </div>
                                <div class="col-md-1 ">
                                    ～
                                </div>
                                <div class="col-md-3">
                                    <?= Html::dropDownList('end_work_off[]', $item['end_time'], Constants::LIST_OPTION_SELECT_TIME, ['class' => 'form-control add-time-off']) ?>
                                </div>
                                <div class="col-md-4">
                                    <?= Html::textInput('reason_work_off[]', $item['reason'] , ['class' => 'form-control add-time-off', 'maxlength' => 20]) ?>
                                  
                                </div>
                                <i class="fa fa-times fa-2x btn-remove"  style="color:red;cursor: pointer;" aria-hidden="true"></i>
                            </div>
                       <?php }
                
              } ?>
          </div> 
          <br>
          <?= Html::button(Yii::t('backend', 'Add a schedule'), ['class' => 'btn common-button-submit' ,'id'=>'btn-add-schedule','disabled'=>$model['work_flg'] == '1' || $model['work_flg'] == null ? false : true]) ?>
        </div>
      </div> 
    </div>
    <div class="col-md-12">
      <div class="row">   
        <div class="col-md-2" >  
            
          <?= Html::radio('work_flg', $model['work_flg'] === '0'   ? true : false, ['value' => '0']); ?> 
          <?= Html::label(Yii::t('backend', 'Holiday')); ?>
        </div> 
        <div class="col-md-2">

        </div>
      </div> 
    </div>

       
    
    
  </div>
    <?= Html::hiddenInput('staff_id',$staffId) ?>
    <?= Html::hiddenInput('schedule_date',$schedule_date) ?>
    <?= Html::hiddenInput('store_id',$store_id) ?>
  <?php ActiveForm::end(); ?>
</div>

<div class="template-work-off hidden">
    <div class="row row-padding">
        <div class="col-md-3 ">
            <?= Html::dropDownList('start_work_off[]', null, Constants::LIST_OPTION_SELECT_TIME, ['class' => 'form-control']) ?>
        </div>
        <div class="col-md-1 ">
            ～
        </div>
        <div class="col-md-3">
            <?= Html::dropDownList('end_work_off[]', null, Constants::LIST_OPTION_SELECT_TIME, ['class' => 'form-control']) ?>
        </div>
        <div class="col-md-4">
            <?= Html::textInput('reason_work_off[]', null , ['class' => 'form-control', 'maxlength' => 20]) ?>
        </div>
      <i class="fa fa-times fa-2x btn-remove"  style="color:red;cursor: pointer;" aria-hidden="true"></i>
    </div>
</div> 


<script>
    $(document).ready(function () {
        $('#title-modal').text("<?= $title ?>");
        $(document).on('click', '[name="work_flg"]', function () {
          var self = $(this).val();
          if(self==0){
              $('.work-off').html('');
              $('[name="shift_id"]').attr('disabled',true);
              $('#btn-add-schedule').attr('disabled',true);
              
          }else{
              $('[name="shift_id"]').attr('disabled',false);
              $('#btn-add-schedule').attr('disabled',false);
          }
            
        })

    })
</script>  