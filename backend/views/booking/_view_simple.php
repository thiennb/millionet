<?php

    use yii\helpers\Html;
    use yii\bootstrap\ActiveForm;
    use yii\widgets\Pjax;
    use common\components\Constants;
    use common\models\Booking;
    use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
    /* @var $model common\models\Booking */
?>
<style>
  .master-booking-view{
    font-size: 17px;
  }
  .master-booking-view .row{
    margin-bottom: 10px;
  }
  .master-booking-view .btn-action{
    padding: 6px 10px;
    font-size: 16px;
  }
  .info-box{
    padding: 15px;
    background-color: #F9F9F9;
    padding-bottom: 2px;
  }
  .btn-view {
    background:none!important;
    border:none; 
    padding:0!important;
    font: inherit;
    /*border is optional*/
    border-bottom:1px solid blue; 
    cursor: pointer;
    color: blue;
  }
  .htMiddle.htDimmed {
    font-weight: bold;
  }
  .modal{
    background: none;
  }
</style>
<div class="master-booking-view">

  <div class="row">

    <div class="col-md-6">
      <b>
      <?= $model->masterCustomer->first_name . $model->masterCustomer->last_name . ' ' . Yii::t('backend', 'Sir') ?>
      </b>
    </div>

    <div class="col-md-4" style="font-size: 20px; color: blue">

        (
    <?= Html::a($model->booking_code, yii\helpers\Url::to([ '/booking/view'  , 'id' => $model->id]) , ['class'=>'btn-view']); ?>
        )
    </div>
    <div class="col-md-2">
        <?= $model->imageBlackList; ?>
    </div>
    
  </div>
  <hr>
   <?php if(isset($model->point_use ) && $model->point_use >0){ ?>
  <div class="row text-orange">
    <div class="col-md-3 text-orange" >
      <b><?= Yii::t('backend', 'Point use') ?></b>
    </div>
    <div class="col-md-9">
      <?= isset($model->point_use )?yii::$app->formatter->asInteger($model->point_use) :0?> P
    </div>
  </div>
   <?php } ?>
  
  <?php if(count($couponName) >0){ ?>
  <div class="row">
    <div class="col-md-3">
      <b><?= Yii::t('backend', 'Coupon') ?></b>
    </div>
    <div class="col-md-9">
        <?= implode(',', ArrayHelper::getColumn($couponName, 'name')) ?>
    </div>
  </div>
  <?php } ?>
  
  <?php if(count($productName) >0){ ?>
  <div class="row">
    <div class="col-md-3">
      <b><?= Yii::t('backend', 'Product') ?></b>
    </div>
    <div class="col-md-9">
        <?= implode('<br>', ArrayHelper::getColumn($productName, 'name')) ?>
    </div>
  </div>
  <?php } ?>
  
  <?php if(count($optionName) >0){ ?>
  <div class="row">
    <div class="col-md-3">
      <b><?= Yii::t('backend', 'Option') ?></b>
    </div>
    <div class="col-md-9">
      <?php
          $name = '';
          
            foreach ($optionName as $item) {
              $name .= $item->option->name . " : " . $item->product->name . ' <br>';
            }
          
          echo trim($name, ',');
      ?>
    </div>
  </div>
  <?php } ?>
  
  
  <div class="row">
    <div class="col-md-3">
      <b><?= Yii::t('backend', 'Booking status') ?></b>
    </div>
    <div class="col-md-9">
      <?= Constants::LIST_BOOKING_STATUS_KANRI[$model->status] ?>

    </div>
  </div>    

  <div class="row">
    <div class="col-md-3">
      <b><?= Yii::t('backend', 'Request') ?></b>
    </div>
    <div class="col-md-9">
      <?= $model->demand != '' ? Yii::t('backend', 'Have described') : Yii::t('backend', 'No described')?>
    </div>
  </div>

</div>

