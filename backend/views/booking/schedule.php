<?php

    use yii\helpers\Html;
    use yii\grid\GridView;
    use yii\jui\DatePicker;
    use yii\widgets\Pjax;
    use yii\bootstrap\Modal;
    use yii\helpers\Url;
    use common\components\Constants;
    use common\models\MasterStore;

/* @var $this yii\web\View */
    /* @var $searchModel common\models\BookingSearch */
    /* @var $dataProvider yii\data\ActiveDataProvider */
    \backend\assets\BookingAsset::register($this);
    $this->title = Yii::t('backend', 'Booking Schedule');
    $this->params['breadcrumbs'][] = $this->title;
?>


<?php //Pjax::begin(); ?>
<div class="row">
  <div class="col-lg-12 col-md-12">
    <div class="box box-info box-solid">
      <div class="box-header with-border">
        <h4 class="text-title"><b><?= $this->title ?></b></h4>
      </div>
      <div class="box-body content">
        <div class="col-md-12">
          <div class="common-box">
            <div class="box-header with-border common-box-h4 col-md-4">
              <h4 class="text-title"><b><?= Yii::t('backend', 'Booking Schedule') ?></b></h4>
            </div>
            <div class="box-body content">
              <div class="col-md-12">

                <?= Html::beginForm(['booking/schedule'], 'post', ['data-pjax' => true, 'class' => 'form-inline', 'id' => "booking-schedule"]); ?>
                <?= Html::label(Yii::t('backend', 'Select a store')) ?>
                <?= Html::dropDownList('store_id', $storeId, MasterStore::getListStore(), ['class' => 'form-control ', 'id' => 'btn-select-store']) ?>
                <div class="row col-md-12">
                
                    
                <div class="col-md-02">
                  <h3 ><?= \common\components\Util::dateFormatString($date) //Yii::$app->formatter->asDate($date) ?>  <?= Html::button(Yii::t('backend', 'Select a date'), ['class' => 'btn common-button-submit', 'id' => 'select-date']) ?></h3>
                </div>
                <?=
                    DatePicker::widget([
                        'id' => 'dateBooking',
                        'name' => 'dateBooking',
                        'value' => $date,
                        'language' => 'ja',
                        'dateFormat' => 'yyyy-MM-dd',
                        "clientOptions" => [
                            "changeMonth" => true,
                            "changeYear" => true,
                            "defaultDate" => $date,
                            'onSelect' => new \yii\web\JsExpression('function(dateText, inst) { $("#booking-schedule").submit() }'),
                            

                        ]
                    ])
                ?>
              
                
                <?= Html::endForm() ?>
               
                  <?php if(count($staffList) > 0) {?>
                <div class="scroll-container">
                  <div id="schedule-table" ></div>
                </div>  

                  <?php }else{  ?>
                  <h4 class="text-center"> <?= Yii::t('backend',"Store don't have any staff") ?> </h4>
                   
                <?php  } ?>
              </div>
            </div>
          </div>


        </div>
      </div>
    </div>
  </div>
</div>
  
<?php
    Modal::begin([
        'id' => 'bookingSimplyModal',
        'size' => 'SIZE_LARGE',
        'header' => '<div class="box-header with-border common-box-h4 col-md-8"><h4 class="text-title"><b>' . Yii::t('backend', 'Reservations Simple Details') . '</b></h4></div>',
        'footer' => Html::button(Yii::t('backend', 'Close'), ['class' => 'btn common-button-default pull-right', 'data-dismiss' => "modal"]),
        'closeButton' => [Yii::t('backend', 'Close'), ['class' => 'btn btn-primary']],
    ]);
    echo "<div id='dialogContent'></div>";
    Modal::end();
?>
  
  
 

 <!--modal view simplified booking-->
  <?php
  Modal::begin([
      'id' => 'bookingModal',
      'size' => 'SIZE_LARGE',
      'header' => '<div class="box-header with-border common-box-h4 col-md-8"><h4 class="text-title"><b>' . Yii::t('backend', 'Reservations and accounting details') . '</b></h4></div>',
      'footer' =>  Html::button(Yii::t('backend', 'Close'), ['class' => 'btn common-button-default', 'data-dismiss' => "modal", 'id' => 'btn-close'])
      . Html::a(Yii::t('backend', 'Reservations edit'), null,['class' => 'btn common-button-submit', 'id' => 'btn-edit-booking']),
  ]);
  ?>
  <div id="modalContent">  </div>
  <?php
  Modal::end();
  ?>


<script>
  
    $(document).on('click', '#select-date', function () {
        $('#dateBooking').datepicker("show");
    });
//     $("#booking-schedule").submit();
    $(document).on('change', '#btn-select-store', function () {
        $("#booking-schedule").submit();
    })
//    $(document).on('click', '.btn-view', function () {
//        $('#modalContent').html('');
//        $('#bookingModal').modal('show')
//                .find('#modalContent')
//                .load($(this).attr('value'));
//  })
    
    function CellClick(id){
        pathControler = 'view-simply'+ '?id=' + id;
         $('#dialogContent').html('');
         $('#bookingSimplyModal').modal('show')
            .find('#dialogContent')
            .load(pathControler);

    }

    $(document).on('ready pjax:success', function () {

    data = <?= $data ?>;
    mergeData = <?= $mergeData ?>;
//  console.log(data);
//  console.log(mergeData);
//  var window.gon.mergeData = $("#mergeData").val();


// console.log( window.gon.mergeData);

    cellRenderer = function (instance, td, row, col, prop, value, cellProperties) {

      Handsontable.renderers.TextRenderer.apply(this, arguments);
      td.innerHTML = '';
      if (typeof (value) === "object" && value != '' ) {

        td.setAttribute("rowSpan", value.rowSpan);
        //        td.className = 'obj-booking';
        //is busy booking
        if(value.type == 'booking'&& value.isFinish == false){
          td.className = 'obj-booking text-left';
          td.setAttribute("id", value.bookingId);
        }
        //is  booking finish
        if(value.type == 'booking'&& value.isFinish != false) {
          td.className = 'obj-booking-disable text-left';
          td.setAttribute("id", value.bookingId);
        } 
        //is  day off
        if(value.type == 'offday'&& value.workOffDate != null) {
          td.className = 'obj-day-off text-left';
//          td.setAttribute("id", value.bookingId);
        }
        td.innerHTML = Handsontable.helper.stringify(value.content);
        

      }
      else {
        td.className = value;

//        td.innerHTML = '';
      }


    };
    templateAddNew = "{custormer} \n {start} ～ {end}";
    megRenderer = function (instance, td, row, col, prop, value, cellProperties) {
      Handsontable.renderers.TextRenderer.apply(this, arguments);
      td.className = 'obj-booking';
//        td.innerHTML = content;

    };

    container = document.getElementById('schedule-table');
    hot = new Handsontable(container, {
      data: data,
      //        width: 500,
      //        height: 500,
      colWidths: 100,
      rowHeights: 80,
      renderAllRows: true,
//      autoRowSize: true,
//             rowHeaders: true,
//          colHeaders: true,
      fixedRowsTop: 1,
      fixedColumnsLeft: 1,
      contextMenu: false,
     
      mergeCells: mergeData,
      readOnly: true, 
      fillHandle: {
        autoInsertRow: false,
      },
      className: "htMiddle",
      //      cells: function (row, col, prop) {
      //        var cellProperties = {};
      //        cellProperties.readOnly = true;
      //        if (col != 0 && row != 0) {
      //          this.renderer = cellRenderer;
      //        }
      //        return cellProperties;
      //      },
      afterSelectionEnd: function () {



        var id = $(".current").attr('id');
        if (typeof (id) === "string" && id != '') {
//                    alert(id);
            CellClick(id);
        }
        //           echo 'Cellclick();';



      },

      
    });
    hot.updateSettings({
      cells: function (row, col, prop) {
        var cellProperties = {};
        cellProperties.readOnly = true;
        if (col != 0 && row != 0) {
          this.renderer = cellRenderer;
        }
        return cellProperties;
      }

    });
    

    Date.prototype.addHours = function (h) {
      this.setHours(this.getHours() + h);
      return this;
    }

  })
</script>
<?php// Pjax::end(); ?>
<script>
  $(document).on('ready', function () {
    $(document).on('click', '.obj-day-off', function () {
      console.log($(this).text());
      yii.warning($(this).html(),'予定');
    })
    $(document).on('click', '#btn-add', function () {

      var startTime = $('#btn-select-starttime').val();
      var staffSelect = $('#btn-select-staff').val();
      var dateBooking = $('#dateBooking').val();
      var staffIndex = timeIndex = 1;
      var timeExcute = 125;
      var timeSpan = Math.ceil(timeExcute / 30);
      var count = 1;
      var pathControler = SITE_ROOT + '/booking/check-valid-booking';
      $.ajax({
        url: pathControler,
        type: 'post',
        data: {
          startTime: startTime,
          dateBooking: dateBooking,
          staffSelect: staffSelect,
          timeExcute: timeExcute
        },
        success: function (data) {

          if (data === "false") {
            alert('busy');
          } else {
            addSchedule();
          }
        }
      });



    });

    function addSchedule() {
      var startTime = $('#btn-select-starttime').val();
      var staffSelect = $('#btn-select-staff').val();
      var dateBooking = $('#dateBooking').val();
      var staffIndex = timeIndex = 1;
      // TODO can tinh tong thoi gian
      var timeExcute = 70;
      timeExcute = Math.ceil(timeExcute / 30) * 30;

      var timeSpan = Math.ceil(timeExcute / 30);

//        if (timeSpan <= 0)timeSpan = 1;
//          
      var count = 1;
      
      ($("#btn-select-staff").children()).each(
              function () {
                if ($(this).attr('value') == staffSelect)
                  staffIndex = count;
                else
                  count++;
              }
      );
      var count = 1;
      ($("#btn-select-starttime").children()).each(
              function () {
                if ($(this).attr('value') == startTime)
                  timeIndex = count;
                else
                  count++;
              }
      );
      var options = document.getElementById('btn-select-starttime').options;
      var end = options[timeIndex + timeSpan - 1 ].value;

      content = templateAddNew.replace('{custormer}', 'THien').replace('{start}', startTime).replace('{end}', end);
      hot.setDataAtCell(staffIndex, timeIndex, content);
//                        $('#schedule-table').handsontable('setDataAtCell', 2,2, 2);
      mergeData.push({row: staffIndex, col: timeIndex, rowspan: 1, colspan: timeSpan});

      hot.updateSettings({
        mergeCells: mergeData,
        cells: function (row, col, prop) {
          var cellProperties = {};
          cellProperties.readOnly = true;

          if (row == staffIndex && col == timeIndex) {
            this.renderer = megRenderer;
          } else if (col != 0 && row != 0) {
            this.renderer = cellRenderer;
          }
          return cellProperties;
        }

      });

      mergeData.pop();
    }
  })
</script>