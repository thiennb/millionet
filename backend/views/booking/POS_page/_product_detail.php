<?php 
use yii\widgets\MaskedInput;
?>
<div class="row">
    <div class="col-xs-12">
        <div class="row col-xs-12">
            <div class="col-xs-12 box box-info box-solid div-header">
                <div class="col-xs-12 form-group"></div>
                <div class="col-lg-12 col-xs-12">
                    <div class="col-md-12 text-left  form-group clear-padding">
                        <?= $product->name ?>
                        <input type="hidden" id="product_jan_code" value="<?= $product_jan_code ?>" />
                    </div>
                    <div class="col-xs-12 form-group clear-padding text-right">
                        <input type="checkbox" id="gift_product" name="gift_product" value="1" <?= ($gift_product == "1")?"checked":"" ?> ><?= Yii::t('backend','Gift items') ?>
                    </div>
                    <div class="col-xs-12 form-group clear-padding div-horizontal">
                        <div class="col-xs-2">
                            <?= Yii::t('backend','Selling price') ?>
                        </div>
                        <div class="col-xs-3 clear-padding">
                            <input type="text" name="product_unit_price" id="product_unit_price" class="form-control input-decimal" maxlength="10" min="0" value="<?= $unit_price ?>"/>
                            <div class="help-block-error" id="product_unit_price_error"></div>
                        </div>
                        <div class="col-xs-1 text-center">
                            x
                        </div>
                        <div class="col-xs-3 clear-padding div-horizontal">
                            <div class="col-xs-4 clear-padding"><?= Yii::t('backend','Quantity') ?></div>
                            <div class="col-xs-8 clear-padding">
                                <input type="text" name="product_quantity" id="product_quantity" maxlength="4" class="form-control input-decimal" min="1" value="<?= $quantity ?>" />
                                <div class="help-block-error" id="product_quantity_error"></div>
                            </div>
                        </div>
                        <div class="col-xs-1 text-right clear-padding">
                            =
                        </div>
                        <div class="col-xs-2 clear-padding">
                            <div class="col-xs-12" id="product_total_price"><?php $total_price = (real)$product->unit_price*(real)$quantity; 
                                        $val = number_format($total_price);
                                        echo $val;
                            ?></div>
                            <input type="hidden" id="product_total_price_hd" value="<?= $total_price ?>" />
                        </div>
                    </div>
                    <div class="col-xs-12 form-group clear-padding div-horizontal">
                        <div class="col-xs-2">

                        </div>
                        <div class="col-xs-3 clear-padding">
                            <select id="product_discount" class="form-control" name="product_discount">
                                <?php if($product_discount_status == '1'){ ?>
                                    <option value="0"><?= Yii::t('backend','Discount percent') ?></option>
                                    <option value="1" selected="selected"><?= Yii::t('backend','Discount yen') ?></option>
                                <?php }else{ ?>
                                    <option value="0" selected="selected"><?= Yii::t('backend','Discount percent') ?></option>
                                    <option value="1"><?= Yii::t('backend','Discount yen') ?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="col-xs-1">

                        </div>
                        <div class="col-xs-3 clear-padding">
                            <div class="col-xs-12 clear-padding">
                                <input type="text" name="product_reduction" id="product_reduction" maxlength="10" class="form-control input-decimal" min="0" value="<?= $product_reduction ?>" />
                                <div class="help-block-error" id="product_reduction_error"></div>
                            </div>
                        </div>
                        <div class="col-xs-1 clear-padding">
                            <?php if($product_discount_status == '1'){ ?>
                            <div class="col-xs-6 text-left clear-padding" id="discount_type" style="color: red">円</div>
                            <?php }else{ ?>
                                <div class="col-xs-6 text-left clear-padding" id="discount_type" style="color: red">％</div>
                            <?php } ?>
                            <div class="col-xs-6 text-right clear-padding">=</div>
                        </div>
                        <div class="col-xs-2 clear-padding">
                            <div class="col-xs-12" id="product_reduction_total" style="color: red">
                                <?php 
                                $product_reduction_total = 0;
                                if($product_discount_status == '0'){ 
                                    $product_reduction_total = 0.01*(int)$product_reduction*(int)$total_price;
                                    ?>
                                <?php }else if($product_discount_status == '1'){
                                    $product_reduction_total = (int)$product_reduction;
                                    ?>
                                <?php } ?>
                                    <?= '-'.$product_reduction_total ?>
                            </div>
                            <input type="hidden" id="product_reduction_total_hd" value="<?= $product_reduction_total ?>"/>
                        </div>  
                    </div>
                    <div class="col-xs-12 form-group border-bot-1">
                    </div>
                </div>
                <div class="row col-lg-12 col-xs-12 form-group text-right" id="total__price" style="color: red">
                    <?= $total_price - $product_reduction_total ?> 円
                </div>
            </div>
        </div>
    </div>
</div>
<style type="text/css">
    .border-bot-1{
        border-bottom: 1px solid #000;
    }
</style>