<div class="row">
    <div class="col-xs-12">
        <div class="row col-xs-12">
            <div class="col-xs-12 box box-info box-solid div-header">
                <div class="col-xs-12 form-group"></div>
                <div class="col-lg-12 col-xs-12">
                    <div class="col-md-12 text-left  form-group clear-padding">                        
                        <?php if($discount == 'yen'){ ?>
                            <?= Yii::t('backend','Discount yen') ?>
                        <?php }else{ ?>
                            <?= Yii::t('backend','Discount percent') ?>
                        <?php } ?>
                    </div>
                    <div class="col-xs-12 form-group clear-padding text-right">
                        <input type="checkbox" id="gift_product" name="gift_product" value="1" disabled><?= Yii::t('backend','Gift items') ?>
                    </div>
                    <div class="col-xs-12 form-group clear-padding div-horizontal">
                        <div class="col-xs-2">
                            <?= Yii::t('backend','Selling price') ?>
                        </div>
                        <div class="col-xs-3 clear-padding">
                            <input type="number" name="product_unit_price" id="product_unit_price" class="form-control" min="0" value="0" disabled/>
                        </div>
                        <div class="col-xs-1 text-center">
                            x
                        </div>
                        <div class="col-xs-3 clear-padding div-horizontal">
                            <div class="col-xs-4 clear-padding"><?= Yii::t('backend','Quantity') ?></div>
                            <div class="col-xs-8 clear-padding">
                                <input type="number" name="product_quantity" id="product_quantity" class="form-control" min="1" value="1" disabled />
                            </div>
                        </div>
                        <div class="col-xs-1 text-right clear-padding">
                            =
                        </div>
                        <div class="col-xs-2 clear-padding">
                            <div class="col-xs-12" id="product_total_price">0</div>
                            <input type="hidden" id="product_total_price_hd" value="0"/>
                        </div>
                    </div>
                    <div class="col-xs-12 form-group clear-padding div-horizontal">
                        <div class="col-xs-2">

                        </div>
                        <div class="col-xs-3 clear-padding">
                            <select id="product_discount" class="form-control" name="product_discount">
                                <?php if($discount == 'yen'){ ?>
                                    <option value="0"><?= Yii::t('backend','Discount percent') ?></option>
                                    <option value="1" selected="selected"><?= Yii::t('backend','Discount yen') ?></option>
                                <?php }else{ ?>
                                    <option value="0" selected="selected"><?= Yii::t('backend','Discount percent') ?></option>
                                    <option value="1"><?= Yii::t('backend','Discount yen') ?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="col-xs-1">

                        </div>
                        <div class="col-xs-3 clear-padding">
                            <div class="col-xs-12 clear-padding">
                                <input type="text" name="product_reduction" id="product_reduction" maxlength="10" class="form-control input-decimal" min="0" />
                                <div class="help-block-error" id="product_reduction_error"></div>
                            </div>
                        </div>
                        <div class="col-xs-1 clear-padding">
                            <div class="col-xs-6 text-left clear-padding" id="discount_type" style="color: red"><?= ($discount == 'yen')?'円':'％' ?></div>
                            <div class="col-xs-6 text-right clear-padding">=</div>
                        </div>
                        <div class="col-xs-2 clear-padding">
                            <div class="col-xs-12" id="product_reduction_total" style="color: red">0                              
                            </div>
                            <input type="hidden" id="product_reduction_total_hd"/>
                            <input type="hidden" id="check_discount" value="1"/>
                        </div>  
                    </div>
                    <div class="col-xs-12 form-group border-bot-1">
                    </div>
                </div>
                <div class="row col-lg-12 col-xs-12 form-group text-right" id="total__price" style="color: red">
                    0 円
                </div>
            </div>
        </div>
    </div>
</div>
<style type="text/css">
    .border-bot-1{
        border-bottom: 1px solid #000;
    }
</style>