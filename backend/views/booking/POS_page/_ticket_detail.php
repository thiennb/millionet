<div class="col-xs-12">
    <div class="col-xs-12">
        <div class="col-xs-12 box box-info box-solid div-header">
            <div class="col-xs-12 form-group"></div>
            <div class="col-lg-12 col-xs-12">
                <div class="col-xs-12 form-group">
                    <div class="col-xs-12">
                        <select id="ticket_select" class="form-control" name="ticket_select">
                            <option id="" ticket_balance="0" list_product=''
                                        price="" ticket_name="" ticket_jan_code="" >-------</option>
                            <?php foreach ($list_ticket as $key => $value){ ?>
                            <option id="<?= $value['id'] ?>" ticket_balance="<?= !empty($value['ticket_balance'])?$value['ticket_balance']:0 ?>" list_product='<?= $value['list_product'] ?>'
                                        price="<?= $value['price'] ?>" ticket_name="<?= $value['name'] ?>" ticket_jan_code="<?= $value['ticket_jan_code'] ?>" ><?= $value['name'] ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                <div class="col-xs-12 form-group">
                    <div class="col-xs-6 text-right">
                        <?= Yii::t('backend','Remaining') ?>
                    </div>
                    <div class="col-xs-6 text-right">
                        <span id="ticket_balance">0</span> <?= Yii::t('backend','The time') ?>
                    </div>
                </div>
                <div class="col-xs-12 form-group div-horizontal">
                    <div class="col-xs-6 text-right">
                        <?= Yii::t('backend','Number of times used this time') ?>
                    </div>
                    <div class="col-xs-5 clear-padding ">
                        <input type="text" name="input_point_ticket" id="input_point_ticket" maxlength="4" class="form-control input-decimal" />
                        <div class="help-block-error" id="input_point_error"></div>
                    </div>
                    <div class="col-xs-1 clear-padding">
                        &nbsp;&nbsp;<?= Yii::t('backend','The time') ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>