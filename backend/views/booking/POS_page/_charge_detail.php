<div class="row">
    <div class="row col-xs-8 col-xs-offset-2">
        <div class="col-xs-12 box box-info box-solid div-header">
            <div class="col-xs-12 form-group"></div>
            <div class="col-lg-12 col-xs-12">
                <div class="col-md-12 text-left  form-group clear-padding">
                    <div class="col-xs-6">
                        <?= Yii::t('backend','Charge balance') ?>
                    </div>
                    <div class="col-xs-6 text-right" id="current_point">
                        <?php $current_charge = empty($charge->charge_balance)?0:number_format($charge->charge_balance); ?>
                        <?= $current_charge ?>
                        <input type="hidden" name="total_charge_hd" id="total_charge_hd" value="<?= empty($charge->charge_balance)?0:$charge->charge_balance ?>"/>
                    </div>
                </div>
                <div class="col-xs-12 form-group">
                    <div class="col-xs-6">
                        <select id="charge_select" class="form-control" name="charge_select">
                            <option value="0" selected="selected"><?= Yii::t('backend','Use') ?></option>
                            <option value="1"><?= Yii::t('backend','Charge') ?></option>
                        </select>
                    </div>
                </div>
                <div class="col-xs-12 form-group" id="charge_select_div">                    
                </div>
                <div class="col-xs-12 form-group">
                    <div class="col-xs-5">
                        <?= Yii::t('backend','Charge money') ?>
                    </div>
                    <div class="col-xs-7 clear-padding">
                        <input type="text" name="charge_input" id="charge_input" class="form-control input-decimal" maxlength="10" />
                        <div class="help-block-error" id="charge_input_error"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>