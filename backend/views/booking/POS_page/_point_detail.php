<div class="row">
    <div class="col-xs-12">
        <div class="col-xs-12 box box-info box-solid div-header">
            <div class="col-xs-12 form-group"></div>
            <div class="col-lg-12 col-xs-12">
                <div class="col-md-12 text-left  form-group clear-padding">
                    <div class="col-xs-6">
                        <?= Yii::t('backend','Current point') ?>
                    </div>
                    <div class="col-xs-6 text-right" id="current_point">
                        <?php $current_point = number_format($total_point); ?>
                        <span id="total_point"><?= $current_point ?></span> P
                        <input type="hidden" id="total_point_hd" value="<?= $total_point ?>" > 
                    </div>
                </div>
                <div class="col-xs-12 form-group">
                    <div class="col-xs-6">
                        <select id="point_select" class="form-control" name="point_select">
                            <option value="0"><?= Yii::t('backend','Use') ?></option>
                            <option value="1"><?= Yii::t('backend','Increase') ?></option>
                            <option value="2"><?= Yii::t('backend','Reduction') ?></option>
                        </select>
                    </div>
                </div>
                <div class="col-xs-12 form-group">
                    <div class="col-xs-8">
                        <input type="text" name="point_input" id="point_input" class="form-control input-decimal" maxlength="10" />
                        <div class="help-block-error" id="point_input_error"></div>
                    </div>
                    <div class="col-xs-4 clear-padding">
                        P
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>