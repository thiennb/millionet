<?php

    use yii\helpers\Html;
    use janisto\timepicker\TimePicker;
    use yii\widgets\ActiveForm;
    use common\models\MasterStore;
    use yii\jui\DatePicker;
    use yii\helpers\Url;
?>

<div class="row">
    <div class="col-md-12">
        <!-- Hidden infomation -->
        <?= Html::hiddenInput('year', $year) ?>
        <?= Html::hiddenInput('month', $month) ?>
        <?= Html::hiddenInput('store_id', $store->id) ?>
        <!-- End hidden information -->


        <div class="row">
            <div class="col-md-12 ">
                <h3><b><?php echo $year.Yii::t('backend', 'Year').$month.Yii::t('backend', 'Month') ?></b> <b style="margin-left: 10px;"><?php echo $store->name; ?></b></h3>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <?= Html::label(Yii::t('backend', 'Open time')); ?> &nbsp;   
                <?= Html::dropDownList("startTime", null, $hours, ['class' => "form-control", "id" => "idBulkTimeStart"]); ?> ～
                <?= Html::dropDownList("endTime", null, $hours, ['class' => "form-control", "id" => "idBulkTimeEnd"]); ?>
            </div>
        </div>
    </div>
</div>
                
<!-- BEGIN html for 2 button -->
<div class="row">
    <div class="col-md-12">
        <div class="row">
            <div class="col-lg-4 col-md-4">
                <a class="btn common-button-submit btn-choose-all btn-block" style="margin-top: 0.6em"><?php echo Yii::t('backend', 'Choose all') ?></a>
            </div>
            <div class="col-lg-4 col-md-4">
                <a class="btn common-button-submit btn-normal-day btn-block" style="margin-top: 0.6em"><?php echo Yii::t('backend', 'Only normal day') ?></a>
            </div>
            <div class="col-lg-4 col-md-4">
                <a class="btn common-button-submit btn-sa-sun btn-block" style="margin-top: 0.6em"><?php echo Yii::t('backend', 'Saturday and Sunday') ?></a>
            </div>
        </div>
    </div>
</div>
<!-- END html for 3 button -->
                
<!-- BEGIN html for calendar -->
<div class="row" style="margin-top: 10px">
    <div class="master-booking-create col-md-12">
        <div id='fulk-calendar'></div>
    </div>
</div>
<!-- END html for calendar -->
            
<!-- BEGIN render calendar to #fulk-calendar -->
<style>
    .fc-event{
        border: none;
    }
</style>
<script id="bulk-reception-setting-template" type="text/x-handlebars-template">
    {{#if workFlg}}
        <?php //echo Html::checkbox('{{{name}}}', false, ['label' => false, 'value' => 0]); ?>
    {{else}}
        <?php //echo Html::checkbox('{{{name}}}', true, ['label' => false, 'value' => 0]); ?>
    {{/if}}
    <?php echo Html::checkbox('{{{name}}}', false, ['label' => false, 'value' => 0]); ?>
</script>
<!-- END render calendar to #fulk-calendar -->

<script> 
    $(document).ready(function () {
        /**
         * Set full calendar
         * @return void
         */
        $('#fulk-calendar').fullCalendar({
            defaultDate: new Date(<?= $year ?>, <?= $month ?> - 1, 1),
            dayNames: ['日', '月', '火', '水', '木', '金', '土'],
            dayNamesShort: ['日', '月', '火', '水', '木', '金', '土'],
            firstDay: 1,
            header: false,
            eventSources: [
                // your event source
                {
                    //get from scheduleMonth gon in controller
                    events: <?php echo json_encode($data) ?>
                }
            ],
                    
            // put your options and callbacks here
            defaultView: 'month',
            dayRender: function (date, element, view) {
                $(element).css("background", "red");
            },
                    
            //        dayRender: function (date, cell) {
            //      console.log("day"); 
            //                        cell.append("1");
            //                    
            //    },
            eventRender: function (event, element) {
                element.find(".fc-event-title").remove();
                element.find(".fc-title").remove();

                element.find(".fc-event-time").remove();
                var theTemplateScript = $("#bulk-reception-setting-template").html();
                var theTemplate = Handlebars.compile(theTemplateScript);

                var theCompiledHtml = theTemplate(event);
                element.append(theCompiledHtml);
            },
                    
            eventAfterAllRender: function () {
                var name = $('#bulk-reception-setting-form .fc-head tr th:nth-child(1)').text();
                $('#bulk-reception-setting-form .fc-head tr th:nth-child(1)').html('<label style="display: inline-block"><span style="display: inline-block; margin-right: 10px" ><input type="checkbox" class="cbx-mon"/></span>' + name+'</label>');
                
                name = $('#bulk-reception-setting-form .fc-head tr th:nth-child(2)').text();
                $('#bulk-reception-setting-form .fc-head tr th:nth-child(2)').html('<label style="display: inline-block"><span style="display: inline-block; margin-right: 10px" ><input type="checkbox" class="cbx-tues"/></span>' + name+'</label>');
                
                name = $('#bulk-reception-setting-form .fc-head tr th:nth-child(3)').text();
                $('#bulk-reception-setting-form .fc-head tr th:nth-child(3)').html('<label style="display: inline-block"><span style="display: inline-block; margin-right: 10px" ><input type="checkbox" class="cbx-wed"/></span>' + name+'</label>');
                
                name = $('#bulk-reception-setting-form .fc-head tr th:nth-child(4)').text();
                $('#bulk-reception-setting-form .fc-head tr th:nth-child(4)').html('<label style="display: inline-block"><span style="display: inline-block; margin-right: 10px" ><input type="checkbox" class="cbx-thurs"/></span>' + name+'</label>');
                
                name = $('#bulk-reception-setting-form .fc-head tr th:nth-child(5)').text();
                $('#bulk-reception-setting-form .fc-head tr th:nth-child(5)').html('<label style="display: inline-block"><span style="display: inline-block; margin-right: 10px" ><input type="checkbox" class="cbx-fri"/></span>' + name+'</label>');
                
                name = $('#bulk-reception-setting-form .fc-head tr th:nth-child(6)').text();
                $('#bulk-reception-setting-form .fc-head tr th:nth-child(6)').html('<label style="display: inline-block"><span style="display: inline-block; margin-right: 10px" ><input type="checkbox" class="cbx-statur"/></span>' + name+'</label>');
                
                name = $('#bulk-reception-setting-form .fc-head tr th:nth-child(7)').text();
                $('#bulk-reception-setting-form .fc-head tr th:nth-child(7)').html('<label style="display: inline-block"><span style="display: inline-block; margin-right: 10px" ><input type="checkbox" class="cbx-sun"/></span>' + name+'</label>');
                
                $('#bulk-reception-setting-form .fc-body .fc-content-skeleton tbody').each(function(){                    
                    var label = $(this).find('tr td:nth-child(1) label').html();
                    var value = $(this).closest('table').find('thead tr td:nth-child(1)').text();
                    if(label != undefined)
                        $(this).closest('table').find('thead tr td:nth-child(1)').html('<label style="display: inline-block"><span style="display: inline-block; margin-right: 10px" >' + label +  '</span>' + value+'</label>');
                    
                    label = $(this).find('tr td:nth-child(2) label').html();
                    value = $(this).closest('table').find('thead tr td:nth-child(2)').text();
                    if(label != undefined)
                        $(this).closest('table').find('thead tr td:nth-child(2)').html('<label style="display: inline-block"><span style="display: inline-block; margin-right: 10px" >' + label +  '</span>' + value+'</label>');
                    
                    label = $(this).find('tr td:nth-child(3) label').html();
                    value = $(this).closest('table').find('thead tr td:nth-child(3)').text();
                    if(label != undefined)
                        $(this).closest('table').find('thead tr td:nth-child(3)').html('<label style="display: inline-block"><span style="display: inline-block; margin-right: 10px" >' + label +  '</span>' + value+'</label>');
                    
                    label = $(this).find('tr td:nth-child(4) label').html();
                    value = $(this).closest('table').find('thead tr td:nth-child(4)').text();
                    if(label != undefined)
                        $(this).closest('table').find('thead tr td:nth-child(4)').html('<label style="display: inline-block"><span style="display: inline-block; margin-right: 10px" >' + label +  '</span>' + value+'</label>');
                    
                    label = $(this).find('tr td:nth-child(5) label').html();
                    value = $(this).closest('table').find('thead tr td:nth-child(5)').text();
                    if(label != undefined)
                        $(this).closest('table').find('thead tr td:nth-child(5)').html('<label style="display: inline-block"><span style="display: inline-block; margin-right: 10px" >' + label +  '</span>' + value+'</label>');
                    
                    label = $(this).find('tr td:nth-child(6) label').html();
                    value = $(this).closest('table').find('thead tr td:nth-child(6)').text();
                    if(label != undefined)
                        $(this).closest('table').find('thead tr td:nth-child(6)').html('<label style="display: inline-block"><span style="display: inline-block; margin-right: 10px" >' + label +  '</span>' + value+'</label>');
                    
                    label = $(this).find('tr td:nth-child(7) label').html();
                    value = $(this).closest('table').find('thead tr td:nth-child(7)').text();
                    if(label != undefined)
                        $(this).closest('table').find('thead tr td:nth-child(7)').html('<label style="display: inline-block"><span style="display: inline-block; margin-right: 10px" >' + label +  '</span>' + value+'</label>');
                    $(this).remove();
                });
                
                $('#bulk-reception-setting-form .fc-head tr th').each(function(){
                    var index = $(this).index() + 1;
                    var status = true;
                    var length = $('#bulk-reception-setting-form .fc-body .fc-content-skeleton thead tr td:nth-child('+index+')').length;
                    var count = 0;
                    $('#bulk-reception-setting-form .fc-body .fc-content-skeleton thead tr td:nth-child('+index+')').each(function(){
                        if($(this).find('input').length != 0 && !$(this).find('input').is(':checked')){
                            status = false;
                        }
                        count++;

                        if(count == length){
                            $('#bulk-reception-setting-form .fc-head tr th:nth-child('+index+') input').prop('checked', status);
                        }
                    });
                });
            }
            //        eventRender: function( event, element, view ) {
            //               element.find('.fc-title').prepend('<span class="glyphicon"></span> '); 
            //        }
        });
    });
</script>