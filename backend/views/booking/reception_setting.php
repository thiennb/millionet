<?php

    use yii\helpers\Html;
    use janisto\timepicker\TimePicker;
    use yii\widgets\ActiveForm;
    use common\models\MasterStore;
    use yii\jui\DatePicker;
    use yii\helpers\Url;

/* @var $this yii\web\View */
    /* @var $model common\models\Booking */

    $this->title = Yii::t('backend', 'Store reservation reception setting');

    $this->params['breadcrumbs'][] = $this->title;
    $this->registerJsFile('@web/js/fullcalendar.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
    $this->registerJsFile('@web/js/jquery.dirtyforms.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]);

//    \backend\assets\BookingAsset::register($this);
?>

<style>
  .input-group-addon{
    display: none
  }
  .input-group.time {
    display: inline-flex;
  }
  .from-time.form-control{
    display: inline-flex;
    width: 60px ;
    height: 20px;
  }
  .to-time.form-control{
    display: inline-flex;
    width: 60px;
    margin-left: 0px;
    height: 20px;
  }
  .fc-event{
    border: none;
  }
  @media only screen and (min-width: 1040px) and (max-width: 1280px) {
    .from-time.form-control{
      display: inline-flex;
      width: 30px ;
      height: 20px;
    }
    .to-time.form-control{
      display: inline-flex;
      width: 30px;
      margin-left: 0px;
      height: 20px;
    }
  }

</style>
<div class="row receptionsetting-screen">
    <div class="col-lg-12 col-md-12">
      <div class="box box-info box-solid">
        <div class="box-header with-border">
          <h4 class="text-title"><b><?= $this->title ?></b></h4>
        </div>
        <div class="box-body content">

          <div class="common-box">
            <div class="row">
              <div class="box-header with-border common-box-h4 col-md-4">
                <h4 class="text-title"><b><?= Yii::t('backend', 'Store reservation reception setting') ?></b></h4>
              </div>
            </div>


          </div>
          <div class="row">
            <div class="col-md-12 receptionsetting_booking_ct">
                <?php
                    $form = ActiveForm::begin([
                            'options' => [
                                'class' => 'form-inline'
                            ],
                            'method' => 'GET',
                            'action' => Url::to(['receptionsetting']),
                            'id' => 'change-store'
                    ]);
                ?>
                <?= Html::label(Yii::t('backend', 'Select a store')); ?>      &nbsp;   
                <?= Html::dropDownList('selectStore', $selectStore, $listStore, ['class' => 'form-control']) ?>
                <br>
                <br>
                <?= Html::dropDownList('selectYear', $selectYear, $years, ['class' => 'form-control']) ?>
                <?= Html::label(Yii::t('backend', 'Year')); ?>      
                <?= Html::dropDownList('selectMonth', $selectMonth, $months, ['class' => 'form-control']) ?>
                <?= Html::label(Yii::t('backend', 'Month')); ?>
                <?= Html::submitButton(Yii::t('backend', 'Show'), ['class' => 'btn common-button-submit']); ?>
                <?php ActiveForm::end(); ?>
            </div>
          </div>
          <div class="row">
            <div class="col-md-3 ">
              <?= Html::button(Yii::t('backend', 'Batch setting'), ['class' => 'btn common-button-submit btn-bulk-setting', 'style' => 'margin-top: 0.6em;']); ?>
            </div>
          </div>

            <?php
                $form = ActiveForm::begin([
                        'action' => 'savereceptionsetting',
                        'id' => 'save-store-chedule'
                ]);
            ?>
            <?= Html::hiddenInput('selectStore', $selectStore) ?>
            <?= Html::hiddenInput('selectYear', $selectYear) ?>
            <?= Html::hiddenInput('selectMonth', $selectMonth) ?>
          <div class="master-booking-create">

            <div id='calendar'></div>

          </div>
          <br>
          <div class="text-center">
            <?= Html::a(Yii::t('backend', 'Return'), '/admin', ['class' => 'btn btn-default common-button-default']); ?>
            <?= Html::submitButton(Yii::t('backend', 'Set'), ['class' => 'btn common-button-submit']); ?>
          </div>
          <?php ActiveForm::end(); ?>

        </div>


        <script id="expressions-template" type="text/x-handlebars-template">
            <?php \backend\assets\BookingAsset::register($this); ?>
                    {{#if workFlg}}
            <?= Html::checkbox('{{{name}}}[work]', false, ['label' => '休業日', 'value' => 0]); ?>
                    {{else}}
            <?= Html::checkbox('{{{name}}}[work]', true, ['label' => '休業日', 'value' => 0]); ?>
                    {{/if}}
                    <div class="time-work">営業時間</div>
            <?php $timeStart = "{{timeStart}}";
                $timeEnd = "{{timeEnd}}" ?>
                <div class="time-work">

            <?= Html::dropDownList("{{name}}[starTime]", $timeStart, $hours, ['class' => "form-control from-time", "id" => "{{idTimeStart}}"]); ?>
                    ～
            <?= Html::dropDownList("{{name}}[endTime]", $timeEnd, $hours, ['class' => "form-control to-time", "id" => "{{idTimeEnd}}"]); ?>
                      <div class='error text-red' id='{{idTimeStart}}-error' >  <div>
                </div>
              <script>
              $("#{{idTimeStart}}").val("{{timeStart}}");
              $("#{{idTimeEnd}}").val("{{timeEnd}}");
              </script>
        </script>

        <div class="content-placeholder"></div>
      </div>
    </div>
</div>

<!-- BEGIN html for setting bulk reception -->
<div class="modal fade" id="bulkReceptionSettingModal" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <div class="common-box">
                <div class="row">
                    <div class="box-header with-border common-box-h4 col-md-4">
                        <h4 class="text-title"><b><?= Yii::t('backend', 'Store bulk reservation reception setting') ?></b></h4>
                    </div>
                </div>
            </div>
        </div>
        <?php
            $form = ActiveForm::begin([
                    'options' => [
                        'class' => 'form-inline'
                    ],
                    'method' => 'POST',
                    //'action' => Url::to(['receptionsetting']),
                    'id' => 'bulk-reception-setting-form'
            ]);
        ?>
        <div class="modal-body">
            <div id="w2-danger" class="alert-danger alert hidden">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <i class="icon fa fa-warning"></i><span></span>
            </div>
            <div id="bulk-reception-setting-content">
                
            </div>
        </div>
        <div class="modal-footer">
            <div class="text-center" style="margin-top: 10px">
                <?= Html::button(Yii::t('backend', 'Close'), ['class' => 'btn btn-default common-button-default', 'data-dismiss' => 'modal']); ?>
                <?= Html::submitButton(Yii::t('backend', 'Set'), ['class' => 'btn common-button-submit']); ?>
            </div>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
  </div>
</div>

      <script>
            
        var isFormDirty = false;
        $(document).ready(function () {
          $('form#save-store-chedule').dirtyForms();

          $('form#save-store-chedule').bind('dirty.dirtyforms', function (event) {
            // Access the form that triggered the event
            isFormDirty = true;

          });
          $('#calendar').fullCalendar({
            defaultDate: new Date(<?= $selectYear ?>, <?= $selectMonth ?> - 1, 1),
            dayNames: ['日曜日', '月曜日', '火曜日', '水曜日', '木曜日', '金曜日', '土曜日'],
            dayNamesShort: ['日曜日', '月曜日', '火曜日', '水曜日', '木曜日', '金曜日', '土曜日'],
            firstDay: 1,
            header: false,
            eventSources: [
              // your event source
              {
                //get from scheduleMonth gon in controller
                events: window.gon.scheduleMonth

              }

            ],
            // put your options and callbacks here
            defaultView: 'month',
            dayRender: function (date, element, view) {

              $(element).css("background", "red");

            },
            //        dayRender: function (date, cell) {
            //      console.log("day"); 
            //                        cell.append("1");
            //                    
            //    },
            eventRender: function (event, element) {
              element.find(".fc-event-title").remove();
              element.find(".fc-title").remove();

              element.find(".fc-event-time").remove();
              var theTemplateScript = $("#expressions-template").html();
              var theTemplate = Handlebars.compile(theTemplateScript);

              var theCompiledHtml = theTemplate(event);
              element.append(theCompiledHtml);

            },
            eventAfterAllRender: function () {
              $("#button").click();

            }
            //        eventRender: function( event, element, view ) {
            //               element.find('.fc-title').prepend('<span class="glyphicon"></span> '); 
            //        }
          })

        });
        $(document).on("submit", "#change-store", function (e) {

          if (isFormDirty) {
            
            bootbox.dialog({
                title: '<div class="box-header with-border common-box-h4 col-md-8"><h4 class="text-title"><b>' + MESSAGE.WARNING + '</b></h4></div>',
                message: MESSAGE.SAVE_BEFORE_CHANGE,
                closeButton: false,
                buttons: {
                    success: {
                        label: 'OK',
                        className: "btn common-button-action text-center",
                        callback: function () {
                          isFormDirty = false;
                          $('#change-store').submit();
                        }
                    },
                    danger: {
                        label: 'キャンセル',
                        className: "btn btn-default text-center",
                        callback: function () {
                        }
                    }
                }
            });
            return false;
          }
              
        });
        $(document).on("submit", "#save-store-chedule", function (e) {
            //validate time from much greater than time to
            var timeValidate = true ;
         
            $('.from-time').each(function() {
                var fromTime = $(this).val();
                var toTime =  $(this).next('.to-time').val();
                if(fromTime >= toTime ){
                 $(this).nextAll('.error').text('開始時間以降の時間を指定してください。');
                 timeValidate = false;
                }
            })
              
            if(timeValidate == false ){
             e.preventDefault();
             return false;
            }
        });
        
        

      

//Toi script bulk settting
$(document).ready(function(){
    $('.btn-bulk-setting').click(function(){
        $('#w2-danger').addClass('hidden');
        $('#bulkReceptionSettingModal').modal('show');
        // Call ajax load
        $.ajax({
            type: 'POST',
            url: 'bulk-reception-setting',
            data: {
                year: <?php echo $selectYear ?>,
                month: <?php echo $selectMonth ?>,
                store_id: <?php echo $selectStore ?>
            },
            success: function(response){
                $('#bulk-reception-setting-form #bulk-reception-setting-content').html(response);
            },
            error: function(){
                
            }
        });
    });
    
    /**
     * Event when click on 全選択 button
     * @return void
     */
    $(document).on('click', '.btn-choose-all', function(event){
        $('#fulk-calendar input').prop('checked', true);
    });

    /**
     * Event when click on 平日のみ選択 button
     * @return void
     */
    $(document).on('click', '.btn-normal-day', function(event){
        $('#fulk-calendar input').prop('checked', true);
        $('#fulk-calendar table tbody td:nth-child(7) input').prop('checked', false);
        $('#fulk-calendar table tbody td:nth-child(6) input').prop('checked', false);
        $('#fulk-calendar table thead th:nth-child(6) input').prop('checked', false);
        $('#fulk-calendar table thead th:nth-child(7) input').prop('checked', false);
    });

    /**
     * Event when click on 土日のみ選択 button
     * @return void
     */
    $(document).on('click', '.btn-sa-sun', function(event){
        $('#fulk-calendar input').prop('checked', false);
        $('#fulk-calendar table tbody td:nth-child(6) input').prop('checked', true);
        $('#fulk-calendar table tbody td:nth-child(7) input').prop('checked', true);
        $('#fulk-calendar table thead th:nth-child(6) input').prop('checked', true);
        $('#fulk-calendar table thead th:nth-child(7) input').prop('checked', true);
    });
    
    function checkIsAllChecked(index){
        var isAll = true;
        
        $('#bulk-reception-setting-form .fc-body .fc-content-skeleton thead tr td:nth-child('+index+') input').each(function(){
            if(!$(this).is(':checked')){
                isAll = false;
            }
        });
        
        return isAll;
    }
    
    /**
     * Event when click on submit
     */
    $(document).on('change', '#bulk-reception-setting-form .fc-body .fc-content-skeleton thead tr td input', function(){
        var index = $(this).closest('td').index() + 1;
        $('#bulk-reception-setting-form .fc-head tr th:nth-child('+index+') input').prop('checked', checkIsAllChecked(index));
    });
    
    /**
     * Event when click on checkbox header
     */
    $(document).on('change', '.cbx-mon, .cbx-tues, .cbx-wed, .cbx-thurs, .cbx-fri, .cbx-statur, .cbx-sun', function(){
        var isChecked = $(this).is(':checked');
        var index = ($(this).closest('th').index() + 1);
        $('#bulk-reception-setting-form .fc-body .fc-content-skeleton thead tr td:nth-child('+index+') input').prop('checked', isChecked);
    });
    
    /**
    * Valid time
    * @returns boolean
    */
    function validTime(){
       var timeValidate = true;

       var fromTime = $('#idBulkTimeStart').val();
       var toTime =  $('#idBulkTimeEnd').val();
       if(fromTime >= toTime ){
           timeValidate = false;
       }

       return timeValidate;
    }

    /**
    * Event when submit change
    */
    $(document).on("submit", "#bulk-reception-setting-form", function (e) {
        $('#w2-danger').addClass('hidden');
        e.preventDefault();
        if (validTime()) {
            var data = $(this).serializeArray();
            //$('#calendar table tr td input[type="checkbox"]').prop('checked', false);
            var startDate = $(this).find('#idBulkTimeStart').val();
            var endDate = $(this).find('#idBulkTimeEnd').val();
            data.map(function(each){
                if(each.name != '_csrf-backend'
                    && each.name != 'year'
                    && each.name != 'month'
                    && each.name != 'store_id'
                    && each.name != 'startTime'
                    && each.name != 'endTime'){
                    $('#calendar table tr td input[name="'+each.name+'[work]"]').prop('checked', false);
                    $('#calendar table tr td select[name="'+each.name+'[starTime]"]').val(startDate);
                    $('#calendar table tr td select[name="'+each.name+'[endTime]"]').val(endDate);
                }
            });
            isFormDirty = true;
            $('#bulkReceptionSettingModal').modal('hide');
        }else{
            $('#w2-danger').removeClass('hidden');
            $('#w2-danger span').text('開始時間以降の時間を指定してください');
        }
    });
});
</script>
<!-- END html for setting bulk reception -->