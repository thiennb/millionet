<?php

use yii\bootstrap\Modal;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\widgets\MaskedInput;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\jui\DatePicker;
use yii\helpers\ArrayHelper;
use common\models\MasterStore;
use common\models\Booking;
use common\models\BookingBusiness;
use yii\helpers\Url;
use common\components\Util;

$this->title = Yii::t('backend', 'Accounting management');
$this->params['breadcrumbs'][] = $this->title;
$booking = array();
$bookingCode = '';
$storeId = '';

$listStore = array();
$permissionId = Yii::$app->user->identity->permission_id;
if($permissionId === 0 || $permissionId == 1){
    $listStore = ['' => Yii::t('backend', 'Select Store')] + MasterStore::getListStore();
}else if($permissionId == 2 || $permissionId == 3){
    $listStore = MasterStore::getListStore();
}

$bookingId = Yii::$app->request->get('bookingId');
if($bookingId !== ''){
    $booking = Booking::find()->where(['id'=>$bookingId])->one();
    if(!empty($booking)){
        $bookingCode = $booking['booking_code'];
        $storeId = $booking['store_id'];
    }
    
    if($booking['status'] == Booking::BOOKING_STATUS_FINISH){
        return Yii::$app->response->redirect(['booking/accounting-management']);
    }
}
$this->registerCssFile('@web/css/accoungting.css', ['depends' => [yii\bootstrap\BootstrapAsset::className()]]);
$this->registerJsFile('@web/js/jquery.numeric.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile('@web/js/accounting-management.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile('@web/js/accounting-management_modal.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile('@web/js/input.mask.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
?>
<div id="message_order" class="alert-success alert fade out display-none">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <i class="icon fa fa-check"></i><span></span>
</div>
<div class="row">
    <div class="col-lg-12 col-md-12">
        <div class="box box-info box-solid">
            <div class="box-header with-border">
                <h4 class="text-title"><b><?= $this->title ?></b></h4>
            </div>
            <div class="box-body content">
                
                <div class="col-md-12">
                    <div class="common-box">
                        <div class="row">
                            <div class="box-header with-border common-box-h4 col-lg-12 col-md-12">
                                <h4 class="text-title"><b><?= Yii::t('backend', 'Accounting infomation input') ?></b></h4>
                            </div>
                            <div class="col-lg-12 col-md-12">
                                <?php
                                $form = ActiveForm::begin([
                                    'action' => ['accounting-management'],
                                    'options' => ['id'=>'accounting_management_form'],
                                    'method' => 'POST',
                                    'enableClientValidation' => false,                                    
                                ]);
                                ?>
                                
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="row">                                        
                                                <div class="col-xs-12">
                                                    <label value="<?= $bookingCode ?>" name="booking_code" booking_id="<?= $bookingId ?>"><?= $bookingCode ?></label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-lg-3 col-md-3 font_label">
                                                    <?= Yii::t('backend', 'Stores') ?> </div>
                                                <div class="col-lg-6 col-md-9 input_store_ct">
                                                    <?= Html::dropDownList('store_id', $storeId, $listStore, ['class'=>'form-control', 'required'=>'required']) ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <?php Pjax::begin(['enablePushState' => false, 'id' => 'pjax_first']) ?>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="row" id="customer-point-charge-ticket"> 
                                                <?php if(!empty($data['pjax_first']['customer']) || !empty($booking)): ?>
                                                <?php
                                                    $point_user = 0; $money_user = 0; $ticket_status = false;
                                                    if(!empty($booking)){
                                                        if($booking['status'] != Booking::BOOKING_STATUS_FINISH){
                                                            $point_user = $booking->customerStore['total_point'];
                                                            $money_user = $booking->customerStore['charge_balance'];
                                                            $ticket_status = $booking->customerStore['tickets_balance']!== 0 ? true:false;
                                                        }
                                                    }
                                                    if(!empty($data['pjax_first']['customerStore'])){
                                                        $point_user = $data['pjax_first']['customerStore']['total_point'];
                                                        $money_user = $data['pjax_first']['customerStore']['charge_balance'];
                                                        $ticket_status = $data['pjax_first']['customerStore']['tickets_balance'] !== 0 ? true:false;
                                                    }          
                                                ?>
                                                <div class="col-xs-4">
                                                    <?= Html::button(BookingBusiness::formatPoint($point_user).' P', ['class'=>'btn btn-primary btn-block', 'id'=> 'btn_point', 'value' => 'point-setting']) ?>    
                                                </div>
                                                <div class="col-xs-4">
                                                    <?= Html::button(BookingBusiness::formatMoney($money_user).' 円', ['class'=>'btn btn-primary btn-block', 'id'=> 'btn_charge', 'value' => 'charge-setting']) ?>
                                                </div>
                                                <div class="col-xs-4">
                                                    <?= Html::button($ticket_status == true ? Yii::t('backend', 'Have ticket') : Yii::t('backend', 'Have not ticket'), ['class'=>'btn btn-primary btn-block', 'id'=> 'btn_ticket', 'value' => 'ticket-setting' ,'have_ticket'=>$ticket_status == true ? 'true':'false']) ?>
                                                </div>
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-lg-3 col-md-3 font_label">
                                                    <?= Yii::t('backend', 'Customer Name') ?> </div>
                                                <div class="col-lg-9 col-md-9 input_store_ct">
                                                    <div class="row">
                                                        <div class="col-lg-8">
                                                            <label id="customerName">
                                                                <?php 
                                                                    $customerName = ''; $customerId = ''; 
                                                                    if(!empty($data['pjax_first']['customer']) || !empty($booking)){
                                                                        if(!empty($booking)){
                                                                            $customerName = $booking->masterCustomer['first_name'] . ' ' .$booking->masterCustomer['last_name'];
                                                                            $customerId = $booking->masterCustomer['id'];
                                                                        }
                                                                        if(!empty($data['pjax_first']['customer'])){
                                                                            $pjax_first = $data['pjax_first'];
                                                                            $customerName = $pjax_first['customer']['first_name'] . ' ' .$pjax_first['customer']['last_name'];
                                                                            $customerId = $pjax_first['customer']['id'];
                                                                        }
                                                                    }
                                                                ?>
                                                                <?= $customerName ?>
                                                            </label>
                                                            <input name="customer_id" id="customerId" value="<?= $customerId ?>" type="hidden" >
                                                        </div>
                                                        <div class="col-lg-4"><?=
                                                            Html::button(Yii::t('backend', 'Customer search'), [
                                                                'class' => 'btn common-button-submit btn-ajax-modal',
                                                                'value' => yii\helpers\Url::to('searchlistcustomer'),
                                                                'id' => 'btn-search',
                                                            ])
                                                            ?></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <?php Pjax::end(); ?>
                                </div>
                                
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-lg-3 col-md-3 font_label hi">
                                                    <?= Yii::t('backend', 'Responsible') ?>  (<?= Yii::t('backend', 'Seat') ?>)</div>
                                                <div class="col-lg-6 col-md-9 input_store_ct">
                                                    <select class="form-control" name="staff_id" id="staffId"></select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-lg-3 col-md-3 font_label">
                                                    <?= Yii::t('backend', 'Cashier') ?></div>
                                                <div class="col-lg-6 col-md-9 input_store_ct">
                                                    <select class="form-control" name="reji_staff_id" id="rejiStaffId"></select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <table class="table table-striped table-bordered text-center">
                                                <thead>
                                                    <tr>
                                                        <th></th>
                                                        <th><?= Yii::t('backend', 'Categories') ?></th>
                                                        <th><?= Yii::t('backend', 'Content') ?></th>
                                                        <th><?= Yii::t('backend', 'Individual settings') ?></th>
                                                        <th><?= Yii::t('backend', 'Unit price') ?></th>
                                                        <th><?= Yii::t('backend', 'Quantity') ?></th>
                                                        <th><?= Yii::t('backend', 'Balance Money') ?></th>
                                                        <th><?= Yii::t('backend', 'Register staff') ?></th>
                                                    </tr>
                                                </thead>
                                                <tbody id="detail_products">
                                                <?php 
                                                if(!empty($booking)){
                                                    $listStaff = array();
                                                    $store = MasterStore::find()->where(['id'=>$storeId])->one();
                                                    if(!empty($store)){
                                                        foreach($store->masterStaff as $staff){
                                                            $listStaff[$staff['id']] = $staff['name'];
                                                        }
                                                    }
                                                    $JancodeAdded = array(); $arrayProducts = array();
                                                    foreach($booking->bookingProduct as $booking_product){
                                                        $product = $booking_product->product;
                                                        if(in_array($product['jan_code'], $JancodeAdded)){
                                                            $index = array_search($product['jan_code'], $JancodeAdded, true);
                                                            $arrayProducts[$index]['quantity'] = $arrayProducts[$index]['quantity']+1;
                                                        }else{
                                                            array_push($JancodeAdded, $product['jan_code']);
                                                            array_push($arrayProducts, array('product'=>$product, 'quantity'=>1));
                                                        }
                                                    }
                                                    foreach($arrayProducts as $booking_product){
                                                        $product = $booking_product['product'];
                                                        ?>
                                                        <tr product-id="<?= $product['id'] ?>" jan_code="<?= $product['jan_code'] ?>" unit_price="<?= $product['unit_price'] ?>" tax-method="<?= $product['tax_display_method'] ?>" tax-id="<?= $product['tax_rate_id'] ?>" tax-rate="<?= Util::getTaxProduct($product['tax_rate_id'])?>" tax-money="0" type="1" last-price="0">
                                                            <td class="col_action"><button type="button" class="btn btn-danger btn-add btn-remove btn-remove-product"><i class="fa fa-times-circle" aria-hidden="true"></i></button></td>
                                                            <td class="col_type text-left td-product"><?= Yii::t('backend','Product') ?></td>
                                                            <td class="col_name text-left td-product"><?= $product['name'] ?></td>
                                                            <td class="col_private_setting text-left td-product"></td>
                                                            <td class="col_price td-product unit-price"><span class="price"><?= $product['unit_price'] ?></span></td>
                                                            <td class=""><input name="quantity" min="1" value="<?= $booking_product['quantity'] ?>" type="number"></td>
                                                            <td class="td-product total-price"><span class="price"><?= $product['unit_price'] ?></span></td>
                                                            <td class="">
                                                                <?= Html::dropDownList('staff_product', $booking['staff_id'], ['' => '-----'] + $listStaff) ?>
                                                            </td>
                                                        </tr>
                                                        <?php
                                                    }
                                                    
                                                    $bookingCoupon = $booking->bookingCoupon;
                                                    foreach($bookingCoupon as $bookingCoupon){
                                                        
                                                        $arrayProduct = array(); $taxMethod = ''; $taxId = ''; $taxRate = 0;
                                                        $discount = 0; $price = '0';
                                                        $coupon = $bookingCoupon->coupon;

                                                        $productCoupons = $coupon->productsCoupon;
                                                        $coupon_for = count($coupon->productsCoupon) > 0 ? 'certain_product' : 'all_product';
                                                        
                                                        foreach($productCoupons as $productCoupon){
                                                            array_push($arrayProduct, $productCoupon->product_id);
                                                        }
                                                        $text_array_product = implode(',', $arrayProduct);
                                                        switch ($coupon['benefits_content']){
                                                            case '00': 
                                                                $discount = $coupon['discount_yen'];
                                                                $price = '-'.$discount;
                                                                break;
                                                            case '01': 
                                                                $discount = $coupon['discount_percent'];
                                                                break;
                                                            case '02':
                                                                $discount = $coupon['discount_price_set'];
                                                                $price = $discount;
                                                                $taxMethod = $coupon['discount_price_set_tax_type'];
                                                                $product = $productCoupons[0]->product;
                                                                $taxId = $product[0]['tax_rate_id'];
                                                                $taxRate = Util::getTaxProduct($taxId);
                                                                break;
                                                            case '03':
                                                                $discount = $coupon['discount_drink_eat'];
                                                                $price = $discount;
                                                                $taxMethod = $coupon['discount_drink_eat_tax_type'];
                                                                $product = $productCoupons[0]->product;
                                                                $taxId = $product[0]['tax_rate_id'];
                                                                $taxRate = Util::getTaxProduct($taxId);
                                                                break;
                                                            default : break;
                                                        }
                                                        ?>
                                                        <tr product-id="<?= $coupon['id'] ?>" jan_code="<?= $coupon['coupon_jan_code'] ?>" unit_price="<?= $price ?>" tax-method="<?= $taxMethod ?>" tax-id="<?= $taxId ?>" tax-rate="<?= $taxRate ?>" tax-money="0" type="2" benefits_content="<?= $coupon['benefits_content'] ?>" product_coupons="<?= $text_array_product ?>" discount_coupon="<?= $discount ?>" coupon_for="<?= $coupon_for ?>" last-price="0">
                                                            <td class="col_action"><button type="button" class="btn btn-danger btn-add btn-remove btn-remove-product"><i class="fa fa-times-circle" aria-hidden="true"></i></button></td>
                                                            <td class="col_type text-left td-product"><?= Yii::t('backend','Coupon') ?></td>
                                                            <td class="col_name text-left td-product"><?= $coupon['title'] ?></td>
                                                            <td class="col_private_setting text-left td-product"></td>
                                                            <td class="col_price td-product unit-price"><span class="price"><?= $price ?></span></td>
                                                            <td class=""><input name="quantity" class="quantity" min="1" value="1" readonly="" type="number"></td>
                                                            <td class="td-product total-price"><span class="price"><?= $price ?></span></td>
                                                            <td class=""><?= Html::dropDownList('staff_product', $booking['staff_id'], ['' => '-----'] + $listStaff) ?></td>
                                                        </tr>
                                                        <?php
                                                    }
                                                    
                                                    if($booking['point_use'] > 0){
                                                        ?>
                                                        <tr point_id="pt" jan_code="pt" point_status="0" point_input="<?= $booking['point_use'] ?>">
                                                            <td class="col_action"><button type="button" class="btn btn-danger btn-add btn-remove btn-remove-product"><i class="fa fa-times-circle" aria-hidden="true"></i></button></td>
                                                            <td class="col_type text-left td-product"><?= Yii::t('backend', 'Point_order') ?></td>
                                                            <td class="col_name text-left td-product">使用</td>
                                                            <td class="col_private_setting text-left td-product"></td>
                                                            <td class="col_price td-product unit-price"><span class="price" id="point_input_1"><?= $booking['point_use'] ?> P</span></td>
                                                            <td class=""><input name="quantity" min="1" value="1" readonly="" type="number"></td>
                                                            <td class="td-product total-price"><span class="price" id="point_input_2"><?= $booking['point_use'] ?> P</span></td>
                                                            <td class=""><?= Html::dropDownList('staff_product', '', ['' => '-----'] + $listStaff) ?></td>
                                                        </tr>
                                                        <?php
                                                    }
                                                }
                                                ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <div class="cover-search-jancode">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <label>JAN <?= Yii::t('backend', 'input code') ?></label>
                                                    </div>
                                                    <div class="col-lg-4">
                                                        <input type="text" name="jan_code" value="" class="field form-control convert-haft-size">
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <div class="cover-product" id="inputJanCode">
                                                            
                                                        </div>
                                                    </div>
                                                    
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <?php Pjax::begin(['enablePushState' => false, 'id' => 'pjax_product']) ?>
                                    <script>
                                        round_down_flg = <?= $data['round_down_flg'] ?>;
                                        round_process_flg = <?= $data['round_process_flg'] ?>;
                                    </script>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <div class="row">
                                                <?php 
                                                    
                                                    if(!empty($data['categories']) || !empty($data['ticket'])) : 
                                                        $categories = $data['categories'];
                                                        $tickets = $data['ticket'];
                                                        $index_active = 0;
                                                        if(!empty($data['categories'])){
                                                            $idActive = $categories[$index_active]->company_id.'_'.$categories[$index_active]->id;
                                                        }else{
                                                            if(!empty($data['ticket'])){
                                                                $idActive = 'ticket';
                                                            }
                                                        }
                                                        
                                                ?>
                                                <div class="col-md-12">
                                                    <ul class="nav nav-tabs">
                                                        <!--<li class="active"><a data-toggle="tab" href="#home">Home</a></li>-->
                                                        <?php foreach($categories as $category): ?>
                                                        <?php $currentIdCategory = $category->company_id.'_'.$category->id;?>
                                                        <li <?= $idActive == $currentIdCategory ? 'class="active"':''?>>
                                                            <a data-toggle="tab" href="#<?= $category->company_id.'_'.$category->id ?>">
                                                                <?= $category->name ?>
                                                            </a>
                                                        </li>
                                                        <?php endforeach; ?>
                                                        
                                                        <?php if(!empty($tickets)) : ?>
                                                        <li <?= $idActive == 'ticket' ? 'class="active"':''?>>
                                                            <a data-toggle="tab" href="#ticket">
                                                                <?= Yii::t('backend', 'Ticket') ?>
                                                            </a>
                                                        </li>
                                                        <?php endif; ?>
                                                    </ul>

                                                    <div class="tab-content">
                                                        <?php foreach($categories as $category): ?>
                                                        <?php $currentIdCategory = $category->company_id.'_'.$category->id;?>
                                                        <div id="<?= $currentIdCategory ?>" class="tab-pane fade in <?= $idActive == $currentIdCategory ? 'active':''?>">
                                                            <div id="categories_product" class="cover-table-tab">
                                                                <div class="table-product-tab">
                                                                    <table class="table table-striped table-bordered text-center ">
                                                                        <tbody>
                                                                            <?php 
                                                                            $index = 0;
                                                                            $products = $category->getMstProducts()->where(['del_flg'=>'0'])->andWhere(['<>', 'jan_code', ''])->all();
                                                                            $products_clone = array();
                                                                            foreach($products as $product){
                                                                                if($product['store_id'] == $data['store_id']){
                                                                                    $products_clone[] = $product;
                                                                                }
                                                                            }
                                                                            
                                                                            $count = count($products_clone);
                                                                            foreach($products_clone as $product): 
                                                                            ?>
                                                                                <?php if($index % 2 == 0) : ?>
                                                                                <tr>
                                                                                    <td class="">
                                                                                        
                                                                                        <button type="button" class="text-left btn btn-default btn-add btn-add-product btn-block">
                                                                                            <i class="fa fa-plus-circle" aria-hidden="true"></i>
                                                                                            <span jan_code="<?= $products_clone[$index]->jan_code ?>" id="<?= $products_clone[$index]->id ?>" price="<?= $products_clone[$index]->unit_price ?>" tax-method="<?= $products_clone[$index]->tax_display_method ?>" tax-id="<?= $products_clone[$index]->tax_rate_id ?>">
                                                                                                <?= htmlentities($products_clone[$index]->name) ?>
                                                                                            </span>
                                                                                        </button>
                                                                                    </td>
                                                                                    <?php if(isset($products_clone[$index+1])) : ?>
                                                                                    <td class="">
                                                                                        <button type="button" class=" text-left btn btn-default btn-add btn-add-product btn-block">
                                                                                            <i class="fa fa-plus-circle" aria-hidden="true"></i>
                                                                                            <span jan_code="<?= $products_clone[$index+1]->jan_code ?>" id="<?= $products_clone[$index+1]->id ?>" price="<?= $products_clone[$index+1]->unit_price ?>" tax-method="<?= $products_clone[$index+1]->tax_display_method?>" tax-id="<?= $products_clone[$index+1]->tax_rate_id ?>">
                                                                                                <?= htmlentities($products_clone[$index+1]->name) ?>
                                                                                            </span>
                                                                                        </button>
                                                                                    </td>
                                                                                    <?php else : ?>
                                                                                    <td class="not-active"></td>
                                                                                    <?php endif; ?>
                                                                                </tr>
                                                                                <?php endif; 
                                                                                $index++;
                                                                                ?>
                                                                            <?php endforeach; ?>
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>  
                                                        </div>
                                                        <?php endforeach; ?>
                                                        
                                                        <?php if(!empty($tickets)) : ?>
                                                        <div id="ticket" class="tab-pane fade in <?= $idActive == 'ticket' ? 'active':''?>">
                                                            <div id="categories_product" class="cover-table-tab">
                                                                <div class="table-product-tab">
                                                                    <table class="table table-striped table-bordered text-center ">
                                                                        <tbody>
                                                                            <?php 
                                                                            $index = 0;
                                                                            $count = count($tickets);
                                                                            foreach($tickets as $ticket): ?>
                                                                                <?php if($index % 2 == 0) : ?>
                                                                                <tr>
                                                                                    <td class="">
                                                                                        
                                                                                        <button type="button" class="text-left btn btn-default btn-add btn-add-product btn-block">
                                                                                            <i class="fa fa-plus-circle" aria-hidden="true"></i>
                                                                                            <span jan_code="<?= $tickets[$index]->ticket_jan_code ?>" id="<?= $tickets[$index]->id ?>" price="<?= $tickets[$index]->price ?>" status="buy" set_ticket="<?= $tickets[$index]->set_ticket ?>" date_expiration="<?= $tickets[$index]->date_expiration ?>">
                                                                                                <?= htmlentities($tickets[$index]->name) ?>
                                                                                            </span>
                                                                                        </button>
                                                                                    </td>
                                                                                    <?php if($index+1 < $count - 1) : ?>
                                                                                    <td class="">
                                                                                        <button type="button" class=" text-left btn btn-default btn-add btn-add-product btn-block">
                                                                                            <i class="fa fa-plus-circle" aria-hidden="true"></i>
                                                                                            <span jan_code="<?= $tickets[$index+1]->ticket_jan_code ?>" id="<?= $tickets[$index+1]->id ?>" price="<?= $tickets[$index+1]->price ?>" status="buy" set_ticket="<?= $tickets[$index]->set_ticket ?>" date_expiration="<?= $tickets[$index]->date_expiration ?>">
                                                                                                <?= htmlentities($tickets[$index+1]->name) ?>
                                                                                            </span>
                                                                                        </button>
                                                                                    </td>
                                                                                    <?php else : ?>
                                                                                    <td class="not-active"></td>
                                                                                    <?php endif; ?>
                                                                                </tr>
                                                                                <?php endif; 
                                                                                $index++;
                                                                                ?>
                                                                            <?php endforeach; ?>
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>  
                                                        </div>
                                                        <?php endif; ?>
                                                    </div>
                                                </div>
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                    </div>
                                    <?php Pjax::end(); ?>
                                    
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <button type="button" class="btn btn-default btn-order" id="btn_discount_percent" value="product-setting">
                                                        <i class="fa fa-plus-circle" aria-hidden="true"></i> <?= Yii::t('backend', 'Discount percent') ?>
                                                    </button>
                                                    <button type="button" class="btn btn-default btn-order" id="btn_discount_yen" value="product-setting">
                                                        <i class="fa fa-plus-circle" aria-hidden="true"></i> <?= Yii::t('backend', 'Discount yen') ?>
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="cover-accounting">
                                                    <div class='col-lg-4 col-md-12 box-order-1'>
                                                        <div class="info-box">
                                                            <div class="row">
                                                                <div class="col-xs-4"><label><?= Yii::t('backend', 'Subtotal') ?></label></div>
                                                                <div class="col-xs-8 text-right"><span class="sub_total">0</span> 円</div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-xs-8"><label><?= Yii::t('backend', 'Use charge point') ?></label></div>
                                                                <div class="col-xs-4 text-right"><span class="use_charge">0</span> 円</div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-xs-6"><label><?= Yii::t('backend', 'Consumption tax') ?></label></div>
                                                                <div class="col-xs-6 text-right"><span class="sum_tax">0</span> 円</div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-xs-6"><label><?= Yii::t('backend', 'Adjustment') ?></label></div>
                                                                <div class="col-xs-6 text-right"><span class="adjustment_money">0</span> 円</div>
                                                            </div>
                                                            <div class="row last-row-info-box">
                                                                <div class="col-xs-4"><label><?= Yii::t('backend', 'Total') ?></label></div>
                                                                <div class="col-xs-8 text-right"><span class="end_money_total">0</span> 円</div>
                                                                <div class="col-md-12"><hr></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class='col-lg-4 col-md-12 box-order-2'>
                                                        <div class="info-box">
                                                            <div class="form-group">
                                                            <div class="row">
                                                                <?php $listCheckout = array("blank"=>"", "credit_card"=>Yii::t('backend', 'Credit card') ,"product"=>Yii::t('backend', 'Product Card'), "other"=>Yii::t('backend', 'Other')) ?>
                                                                <div class="col-xs-6"><?= Html::dropDownList('type_checkout_1', '', $listCheckout, ['class'=>'form-control', 'required'=>'required']) ?></div>
                                                                <div class="col-xs-6"><input type="number" min="0" max="999999999999999" name="type_checkout_1" class="form-control" value="" disabled></div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                            <div class="row">
                                                                
                                                                <div class="col-xs-6"><?= Html::dropDownList('type_checkout_2', '', $listCheckout, ['class'=>'form-control', 'required'=>'required']) ?></div>
                                                                <div class="col-xs-6"><input type="number" min="0" max="999999999999999" name="type_checkout_2" class="form-control" value="" disabled></div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                            <div class="row">
                                                                
                                                                <div class="col-xs-6"><?= Html::dropDownList('type_checkout_3', '', $listCheckout, ['class'=>'form-control', 'required'=>'required']) ?></div>
                                                                <div class="col-xs-6"><input type="number" min="0" max="999999999999999" name="type_checkout_3" class="form-control" value="" disabled></div>
                                                                </div>
                                                            </div>
                                                            <div class="row last-row-info-box">
                                                                <div class="col-xs-4"><label><?= Yii::t('backend', 'Subtractor') ?></label></div>
                                                                <div class="col-xs-8 text-right"><span class="total_money_after">0</span> 円</div>
                                                                <div class="col-md-12"><hr></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class='col-lg-4 col-md-12 box-order-3'>
                                                        <div class="info-box">
                                                            <div class="form-group">
                                                            <div class="row last-row-info-box">
                                                                <div class="col-xs-5"><label><?= Yii::t('backend', 'Deposit') ?></label></div>
                                                                <div class="col-xs-7"><input id="real_cash" type="text" class="form-control convert-haft-size" name="real_cash" min="0" maxlength="10"></div>
                                                                <div class="col-md-12"><hr></div>
                                                            </div>
                                                            </div>
                                                            <div class="row last-row-info-box row-bottom">
                                                                <div class="col-xs-5"><label><?= Yii::t('backend', 'Refund') ?></label></div>
                                                                <div class="col-xs-7 text-right"><span class="refund_money">0</span> 円</div>
                                                                <div class="col-md-12"><hr></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="error-msg text-center" id="error_order"><span></span></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-lg-3 col-md-12">
                                                    <button type="button" id="return_goods" class="btn btn-default btn-block"><?= Yii::t('backend', 'Returns_good') ?></button>
                                                </div>
                                                <div class="col-lg-3 col-md-12">
                                                    
                                                </div>
                                                <div class="col-lg-3 col-md-12">
                                                    <?php if($bookingId != '') : ?>
                                                        <a href="/admin/booking/view?id=<?= $bookingId ?>" class="btn btn-default btn-block"><?= Yii::t('backend', 'Close') ?></a>
                                                    <?php endif; ?>
                                                </div>
                                                <div class="col-lg-3 col-md-12">
                                                    <button type="button" id="submit_order" class="btn btn-default btn-block common-button-submit"><?= Yii::t('backend', 'Accounting') ?></button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>                                    
                                </div>
                                <?php ActiveForm::end(); ?>
                            </div>
                        </div>
                        
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
</div>

<?php
Modal::begin([
    'id' => 'userModal',
    'size' => 'SIZE_LARGE',
    'header' => '<div class="box-header with-border common-box-h4 col-md-8"><h4 class="text-title"><b>' . Yii::t('backend', 'Customer search results') . '</b></h4></div>',
    'footer' => Html::button(Yii::t('backend', 'Close'), ['class' => 'btn common-button-default', 'data-dismiss' => "modal", 'id' => 'btn-close'])
    . Html::submitButton(Yii::t('backend', 'Customer choice'), ['class' => 'btn common-button-submit', 'id' => 'btn-custormer']),
]);
?>
<div id="modalContent"></div>
<?php Modal::end(); ?>
<div id="div_modal">
    <div class="row">
        <?php Modal::begin([
            'id' => 'point_modal',
            'size' => 'SIZE_LARGE',
            'header' => '<div class="box-header with-border common-box-h4 col-md-8"><h4 class="text-title"><b>' . Yii::t('backend', 'Poin') . '</b></h4></div>',
            'footer' => Html::button(Yii::t('backend', 'Close'), ['class' => 'btn common-button-default', 'data-dismiss' => "modal", 'id' => 'btn-close'])
            . Html::button(Yii::t('backend', 'Setting'), ['class' => 'btn common-button-submit', 'id' => 'btn_point_modal']),
        ]);
        ?>
        <div id="point_modal_content">  </div>
        <?php Modal::end(); ?>
    </div>
    <div class="row">
        <?php Modal::begin([
            'id' => 'charge_modal',
            'size' => 'SIZE_LARGE',
            'header' => '<div class="box-header with-border common-box-h4 col-md-8"><h4 class="text-title"><b>' . Yii::t('backend', 'Charge') . '</b></h4></div>',
            'footer' => Html::button(Yii::t('backend', 'Close'), ['class' => 'btn common-button-default', 'data-dismiss' => "modal", 'id' => 'btn-close'])
            . Html::button(Yii::t('backend', 'Setting'), ['class' => 'btn common-button-submit', 'id' => 'btn_charge_modal']),
        ]);
        ?>
        <div id="charge_modal_content">  </div>
        <?php Modal::end(); ?>
    </div>
    <div class="row">
        <?php Modal::begin([
            'id' => 'ticket_modal',
            'size' => 'SIZE_LARGE',
            'header' => '<div class="box-header with-border common-box-h4 col-md-8"><h4 class="text-title"><b>' . Yii::t('backend', 'Ticket') . '</b></h4></div>',
            'footer' => Html::button(Yii::t('backend', 'Close'), ['class' => 'btn common-button-default', 'data-dismiss' => "modal", 'id' => 'btn-close'])
            . Html::button(Yii::t('backend', 'Setting'), ['class' => 'btn common-button-submit', 'id' => 'btn_ticket_modal']),
        ]);
        ?>
        <div id="ticket_modal_content">  </div>
        <?php Modal::end(); ?>
    </div>
    <div class="row">
        <?php Modal::begin([
            'id' => 'product_modal',
            'size' => 'SIZE_LARGE',
            'header' => '<div class="box-header with-border common-box-h4 col-md-8"><h4 class="text-title"><b>' . Yii::t('backend', 'Detail Product') . '</b></h4></div>',
            'footer' => Html::button(Yii::t('backend', 'Close'), ['class' => 'btn common-button-default', 'data-dismiss' => "modal", 'id' => 'btn-close'])
            . Html::button(Yii::t('backend', 'Setting'), ['class' => 'btn common-button-submit', 'id' => 'btn_product_modal']),
        ]);
        ?>
        <div id="product_modal_content">  </div>
        <?php Modal::end(); ?>
    </div>
</div>
<script>
    success_class = 'alert-success alert fade in';
    failed_class = 'alert-danger alert fade in';
    return_good_status = 0;
    $rejiStaffId = <?= Yii::$app->user->identity->id ?>;
    <?= empty($booking) ? '$mainId = ""' : '$mainId = '.$booking['staff_id'] ?>    
    $listStaff = [];
    textListStaff = '';
    $listaddedJancode = [];    
    $listTaxRate = [];
    $subtotalMoney = 0;
    $totalInnerTax = 0;
    $totalOutterTax = 0;
    $staff_id_url = "<?= Url::to(['/master-coupon/selectstaffbyidstore']) ?>";
    $order_checkout_url = "<?= Url::to(['/booking/order-checkout']) ?>";
    $urlAccountingManagement = '<?= Url::to(['accounting-management?'.$_SERVER['QUERY_STRING']]) ?>';    
    
    /*--------------------------POINT----------------------*/
    point_allocation = 0;
    point_increase = 0;
    point_reduce = 0;
    point_use = 0;
    money_conversion_from_point = 0;
    grant_yen_per_point = <?= $data['grant_yen_per_point'] ?>;
    point_conversion_ratio = <?= $data['point_conversion_ratio'] ?>;
    
    /*--------------------------CHARGE----------------------*/
    charge_use = 0;
    charge_add = 0;
    charge_add_money = 0;
    
    /*----------------------DISCOUNT BY RICE----------------*/
    money_discount_percent = 0;
    money_discount_yen = 0;
    
    $(document).ready(function(){
        var inputmask_d2b27c14 = {"groupSeparator":",","alias":"integer","autoGroup":false,"removeMaskOnSubmit":true,"allowMinus":false};
        $("input[name=real_cash]").inputmask(inputmask_d2b27c14);
    });
</script>