<?php

    use yii\helpers\Html;
    use yii\grid\GridView;
    use yii\jui\DatePicker;
    use yii\widgets\Pjax;
    use yii\bootstrap\Modal;
    use yii\helpers\Url;
    use common\components\Constants;
    use common\components\Util;

/* @var $this yii\web\View */
    /* @var $searchModel common\models\BookingSearch */
    /* @var $dataProvider yii\data\ActiveDataProvider */
    \backend\assets\BookingAsset::register($this);
    $this->title = Yii::t('backend', 'Reservations edit');
    $this->params['breadcrumbs'][] = ['label' =>  Yii::t('backend', 'Booking List'), 'url' => ['index', 'date' => date("Y/m/d")]];
    $this->params['breadcrumbs'][] = $this->title;
    $this->registerCssFile('@web/css/booking.css');
?>

<?php //Pjax::begin([ 'id' => 'solutionItems','enablePushState' => false]); ?>
<div class="row">
  <div class="col-lg-12 col-md-12">
    <div class="box box-info box-solid">
      <div class="box-header with-border">
        <h4 class="text-title"><b><?= $this->title ?></b></h4>
      </div>
      <div class="box-body content">
        <div class="col-md-12">
          <div class="common-box">
            <div class="box-header with-border common-box-h4 col-md-6">
              <h4 class="text-title"><b><?= Yii::t('backend', 'Date and staff') ?></b></h4>
            </div>
            <div class="box-body content">
                

                <?= Html::beginForm(null, 'post', ['data-pjax' => true, 'class' => 'form-inline', 'id' => "booking-schedule"]); ?>
                <div class="row col-md-12">
                <div class="col-md-02">
                <h3 ><?= Util::dateFormatString($date) ?>  <?= Html::button(Yii::t('backend', 'Select a date'), ['class' => 'btn common-button-submit', 'id' => 'select-date']) ?></h3>
                </div>
                    <?= Html::hiddenInput('bookingId',isset(Yii::$app->request->get()['id'] )? Yii::$app->request->get()['id'] : null ,['id'=>'bookingId']) ?>
                <?=
                    DatePicker::widget([
                        'id' => 'dateBooking',
                        'name' => 'dateBooking',
                        'value' => $date,
                        'language' => 'ja',
                        'dateFormat' => 'yyyy-MM-dd',
                        "clientOptions" => [
                            "changeMonth" => true,
                            "changeYear" => true,
                            "defaultDate" => $date,
                            'onSelect' => new \yii\web\JsExpression('function(dateText, inst) { $("#booking-schedule").submit() }'),
                            

                        ]
                    ])
                ?>
                
              
                <div class="col-md-02">
               
                </div>
                  </div>
                <div class="row">
                <div class="col-md-12">
                <?= Html::dropDownList('start_time', isset($staffBooked['start_time'] )? $staffBooked['start_time'] :null , $hourList, ['class' => 'form-control add-time-off', 'id' => 'btn-select-starttime']) ?>
                <?= Html::dropDownList('staff_select', isset($staffBooked['staff_id'] )? $staffBooked['staff_id'] :null, $staffList, ['class' => 'form-control add-time-off', 'id' => 'btn-select-staff']) ?>
                <?= Html::button(Yii::t('backend', 'Add'), ['class' => 'btn common-button-submit', 'id' => 'btn-add' , 'name' => 'btn-add']) ?>  
                <!--hidden input-->
                <!--Check to know is btn ok-submit-->
                <?= Html::hiddenInput('isDecision',null,['id'=>'btn-is-decision']) ?>
                <!--Set end time-->
                <?= Html::hiddenInput('end_time',null,['id'=>'btn-end-time']) ?>
                <?= Html::endForm() ?>
                 </div>
                </div>
                 <?php if (count($staffList) > 0 ){ ?>  
                <div class="scroll-container" >
                  <div id="schedule-table"  style="min-width: 1000px "></div>
                </div> 
              <?php }else{
                echo Html::tag('h4',Yii::t('backend', Yii::t('backend',"Store don't have any staff")));
              } ?>
                <br>
                <div class="row text-center">
                    
                  <?= Html::a(Yii::t('backend', 'Close'), isset(Yii::$app->request->get()['id'] )?'update?id='. Yii::$app->request->get()['id'] :'create'  , ['class' => 'btn btn-default common-button-default', 'id' => 'btn-back']) ?>
                  
                  <?= Html::button(Yii::t('backend', 'Decision'), ['class' => 'btn common-button-submit', 'id' => 'btn-ok' ,'data-pjax' => '0' ,'name' => 'btn-ok','onClick'=>'submitBookingStaff()']) ?>
                </div>  
                
              </div>
            </div>
          </div>


        </div>
      </div>
    </div>
  </div>
<?php
    Modal::begin([
        'id' => 'bookingModal',
        'size' => 'SIZE_LARGE',
        'header' => '<b>' . Yii::t('backend', 'Booking detail') . '</b>',
        'footer' => Html::button(Yii::t('backend', 'Close'), ['class' => 'btn common-button-default', 'data-dismiss' => "modal"])
        . Html::submitButton(Yii::t('backend', 'Rebooking'), ['class' => 'btn common-button-submit']),
        'closeButton' => [Yii::t('backend', 'Close'), ['class' => 'btn btn-primary']],
    ]);
    echo "<div class='dialogContent'></div>";
    Modal::end();
?>



<script>
 
    data = <?= $data ?>;
    mergeData = <?= $mergeData ?>;
//  console.log(data);
//  console.log(mergeData);
//  var window.gon.mergeData = $("#mergeData").val();


// console.log( window.gon.mergeData);

  
    cellRenderer = function (instance, td, row, col, prop, value, cellProperties) {

      Handsontable.renderers.TextRenderer.apply(this, arguments);
      td.innerHTML = '';
      if (typeof (value) === "object" && value != '' ) {

        td.setAttribute("rowSpan", value.rowSpan);
        //        td.className = 'obj-booking';
        //is busy booking
        if(value.type == 'booking'&& value.isFinish == false){
          td.className = 'obj-booking-disable text-left';
          td.setAttribute("id", value.bookingId);
        }
        //is  booking finish
        if(value.type == 'booking'&& value.isFinish != false) {
          td.className = 'obj-booking-disable text-left';
          td.setAttribute("id", value.bookingId);
        } 
        //is  day off
        if(value.type == 'offday'&& value.workOffDate != null) {
          td.className = 'obj-day-off text-left';
//          td.setAttribute("id", value.bookingId);
        }
        
        

      }
      else {
        td.className = value;

//        td.innerHTML = '';
      }


    };
    templateAddNew = "{custormer} \n {start} ～ {end}";
    megRenderer = function (instance, td, row, col, prop, value, cellProperties) {
      Handsontable.renderers.TextRenderer.apply(this, arguments);
      td.className = 'obj-booking';
//        td.innerHTML = content;

    };

    container = document.getElementById('schedule-table');
    hot = new Handsontable(container, {
      data: data,
      //        width: 500,
      //        height: 500,
      colWidths: 100,
      rowHeights: 80,
      renderAllRows: true,
      viewportColumnRenderingOffset : 30,
//      autoRowSize: true,
//             rowHeaders: true,
//          colHeaders: true,
      fixedRowsTop: 1,
      fixedColumnsLeft: 1,
      contextMenu: false,
     
      mergeCells: mergeData,
      readOnly: true,
      fillHandle: {
        autoInsertRow: false,
      },
      className: "htMiddle",
      //      cells: function (row, col, prop) {
      //        var cellProperties = {};
      //        cellProperties.readOnly = true;
      //        if (col != 0 && row != 0) {
      //          this.renderer = cellRenderer;
      //        }
      //        return cellProperties;
      //      },

      
    });
    hot.updateSettings({
      cells: function (row, col, prop) {
        var cellProperties = {};
        cellProperties.readOnly = true;
        if (col != 0 && row != 0) {
          this.renderer = cellRenderer;
        }
        return cellProperties;
      }

    });

    Date.prototype.addHours = function (h) {
      this.setHours(this.getHours() + h);
      return this;
    }
//    $('.htDimmed').trigger('click');


</script>
<?php //Pjax::end(); ?>
<script>
  
    endTime = bookingstaff= '';
    timeExcute = <?= $sumMinute ?>;
    $(document).on('click', '#select-date', function () {
        $('#dateBooking').datepicker("show");
    })
    //function add back local variable temp booking staff back to grid
    function setSessionBooking(bookingstaff,oldBooking){
     
     
     bookingstaffOld = sessionStorage.getItem('bookingstaffOld');
     bookingstaffOld = JSON.parse(bookingstaffOld);
     console.log(bookingstaffOld);
     
     //remove old data booking
      if(bookingstaffOld != null && bookingstaff.dateBooking == $('#dateBooking').val()){
       
        indexesOld = $.map(mergeData, function(obj, index) {
          if(obj.col == bookingstaffOld.timeIndex && obj.row ==  bookingstaffOld.staffIndex) {
              return index;
          }
        })
        if(jQuery.isEmptyObject(indexesOld) != true){
        
          mergeData.splice(indexesOld, 1);
        }
        hot.setDataAtCell(bookingstaffOld.staffIndex, bookingstaffOld.timeIndex, '1');
        
      }
      
      
     //add new selected booking 
     hot.setDataAtCell(bookingstaff.staffIndex, bookingstaff.timeIndex, bookingstaff.content);
     mergeData.push({row: bookingstaff.staffIndex, col: bookingstaff.timeIndex, rowspan: 1, colspan: bookingstaff.timeSpan});

      hot.updateSettings({
        mergeCells: mergeData,
        cells: function (row, col, prop) {
          var cellProperties = {};
          cellProperties.readOnly = true;

          if (row == bookingstaff.staffIndex && col == bookingstaff.timeIndex) {
            this.renderer = megRenderer;
          } else if (col != 0 && row != 0) {
            this.renderer = cellRenderer;
          }
          return cellProperties;
        }

      });

      mergeData.pop();
   }
    
    //validate if staff can booking if can add to grid
    $(document).on('click', '#btn-add', function () {

      var startTime   = $('#btn-select-starttime').val();
      var staffSelect = $('#btn-select-staff').val();
      var dateBooking = $('#dateBooking').val();
      var bookingId   = $('#bookingId').val();
//      var timeExcute  = 125;
      var pathControler = SITE_ROOT + '/booking/check-valid-booking';
      $.ajax({
        url: pathControler,
        type: 'post',
        data: {
          startTime: startTime,
          dateBooking: dateBooking,
          staffSelect: staffSelect,
          timeExcute: timeExcute ,
          bookingId :bookingId
        },
        success: function (data) {

          if (data === "false") {
//            alert('busy');
            yii.warning(MESSAGE.BUSY,MESSAGE.WARNING);
          } else {
            addSchedule();
            $('#btn-end-time').val(endTime);
          }
        }
      });



    });
    //confirm and submit to save staff booking
    function submitBookingStaff(){
      $("#btn-is-decision").val(1);
      sessionStorage.setItem('bookingstaff',bookingstaff );
      $("#booking-schedule").submit();
      
    }

    function addSchedule() {
      var startTime   = $('#btn-select-starttime').val();
      var staffSelect = $('#btn-select-staff').val();
      var dateBooking = $('#dateBooking').val();
      var staffIndex  = timeIndex = 1;

//      var timeExcute = 70;
      timeExcute = Math.ceil(timeExcute / 30) * 30;
      var timeSpan = Math.ceil(timeExcute / 30);

      var count = 1;

      
      ($("#btn-select-staff").children()).each(
              function () {
                if ($(this).attr('value') == staffSelect)
                  staffIndex = count;
                else
                  count++;
              }
      );
      var count = 1;
      ($("#btn-select-starttime").children()).each(
              function () {
                if ($(this).attr('value') == startTime)
                  timeIndex = count;
                else
                  count++;
              }
      );
      var options = document.getElementById('btn-select-starttime').options;
       endTime = options[timeIndex + timeSpan - 1 ].value;
//      console.log(typeof (bookingstaff) != 'object');
      if(typeof (bookingstaff) == 'string'){
        bookingstaff = JSON.parse(bookingstaff);
      }
      if(bookingstaff != null && bookingstaff.dateBooking == $('#dateBooking').val() ){
       
        indexes = $.map(mergeData, function(obj, index) {
          if(obj.col == bookingstaff.timeIndex && obj.row ==  bookingstaff.staffIndex) {
              return index;
          }
        })
        
        if(jQuery.isEmptyObject(indexes) != true){
        console.log(typeof (indexes));
        mergeData.splice(indexes, 1);
        }
        
      }
      
      
      
//      console.log(mergeData);
      content = templateAddNew.replace('{custormer}', "<?= $custormerName ?>").replace('{start}', startTime).replace('{end}', endTime);
      bookingstaff = JSON.stringify({
        content   : content ,
        staffIndex: staffIndex ,
        timeIndex : timeIndex ,
        timeSpan  : timeSpan ,
        startTime : startTime ,
        endTime   : endTime ,
        dateBooking :  $('#dateBooking').val()
      });
      
      
     
      hot.setDataAtCell(staffIndex, timeIndex, content);
//                        $('#schedule-table').handsontable('setDataAtCell', 2,2, 2);
      mergeData.push({row: staffIndex, col: timeIndex, rowspan: 1, colspan: timeSpan});
      
//    
//      
//    console.log(indexes);
////       console.log(mergeData.findIndex(x => x.col == 2  x.row == 5 ));
//      mergeData.splice(indexes, 1);
      hot.updateSettings({
        mergeCells: mergeData,
        cells: function (row, col, prop) {
          var cellProperties = {};
          cellProperties.readOnly = true;

          if (row == staffIndex && col == timeIndex) {
            this.renderer = megRenderer;
          } else if (col != 0 && row != 0) {
            this.renderer = cellRenderer;
          }
          return cellProperties;
        }

      });
      
      mergeData.pop();
    }
    
    //set back season variable to create
    bookingstaff = sessionStorage.getItem('bookingstaff');
    bookingstaffOld = sessionStorage.getItem('bookingstaffOld');
    if(bookingstaff != null){
      bookingstaff = JSON.parse(bookingstaff);
      if(bookingstaff != null && bookingstaff.dateBooking == $('#dateBooking').val()){
        setSessionBooking(bookingstaff);
        $('#btn-end-time').val(bookingstaff.endTime);
        bookingstaff = JSON.stringify(bookingstaff);
      }
    }

 
</script>