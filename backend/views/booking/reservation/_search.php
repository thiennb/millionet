<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use common\components\Constants;
use yii\jui\DatePicker;
use common\models\MasterStaff;
use common\models\MasterStore;
use common\models\MstCategoriesProduct;
use yii\helpers\Url;
// Drop List
use kartik\depdrop\DepDrop;

/* @var $this yii\web\View */
/* @var $searchModel common\models\BookingSearch */
/* @var $form yii\widgets\ActiveForm */
?>
    <style>
        @media (min-width: 1200px) {
            .col-lg-1_5 {
                width: 10.33333333%;
            }
            .col-lg-0 {
                display: none;
            }
            .col-lg-0_5  {
                width: 3.33333334%;
            }

            .col-lg-1_5_add  {
                width: 7%;
            }
            .col-lg-2_5  {
                width: 13.33333333%;
            }
        }

    </style>
<div class="master-customer-search">
    <?php
    $form = ActiveForm::begin([
                'action' => [Yii::$app->controller->action->id],
                'enableClientValidation' => false,
                'method' => 'get',
                'enableClientValidation' => false,
    ]);
    ?>


    <?php //= $form->field($model, 'register_staff_id',$fieldOptions1)->dropDownList(MasterStaff::getListStaff(),['prompt'=>Yii::t('backend', 'Please Select')]) ?>
    <!-- Datpdt start-->
    <!-- start date from start -->
    <div class="col-md-12"> 
        <div class="col-lg-2 col-md-3 font_label label_booking_ct">  
            <?=
            Yii::t("backend", "Date booking");
            ?>
        </div> 
        <div class="col-lg-2 col-md-4 input_booking_ct">
            <?=
            $form->field($model, 'booking_date_from', [
                'template' => '<div class="clear-padding">{input}{error}</div>'
            ])->widget(DatePicker::classname(), [
                'language' => 'ja',
                'dateFormat' => 'yyyy/MM/dd',
                'clientOptions' => [
                    "changeMonth" => true,
                    "changeYear" => true,
                    "yearRange" => "1900:+0"
                ],
            ])->textInput(['maxlength' => 10])
            ?>
        </div> 
        <div  class="col-md-1 text-center separator">～</div>
        <div class="col-lg-2 col-md-4 input_booking_ct"> 
            <?=
            $form->field($model, 'booking_date_to', [
                'template' => '<div class="clear-padding">{input}{error}</div>'
            ])->widget(DatePicker::classname(), [
                'language' => 'ja',
                'dateFormat' => 'yyyy/MM/dd',
                'clientOptions' => [
                    "changeMonth" => true,
                    "changeYear" => true,
                    "yearRange" => "1900:+0"
                ],
            ])->textInput(['maxlength' => 10])
            ?>
        </div> 
    </div>
    <!-- start date from end -->
    <!-- status start -->
    <div class="col-md-12">
        <div class="col-lg-2 col-md-3 font_label label_booking_ct">  
            <?=
            Yii::t("backend", "Booking Store");
            ?>
        </div>
        <div class="col-lg-2 col-md-4 input_booking_ct">
            <?php 
            if(count(MasterStore::getListStore())>1){
                echo $form->field($model, 'store_id')->dropDownList(MasterStore::getListStore(), ['prompt' => Yii::t('backend', 'Please Select')])->label(false) ;
            }else{
                echo $form->field($model, 'store_id')->dropDownList(MasterStore::getListStore())->label(false);
            }
            ?>
        </div>
        <div class="col-lg-1_5 col-md-2 font_label label_booking_ct">  
            <?=
            Yii::t("backend", "Booking status");
            ?>
        </div>
        <div class="col-lg-2 col-md-3 input_booking_ct">
            <?= $form->field($model, 'status')->dropDownList(Constants::LIST_BOOKING_STATUS_KANRI, ['prompt' => Yii::t('backend', 'Please Select')])->label(false) ?>
        </div>
    </div>
    <!-- status end -->
    <!-- list_category start -->
    <div class="col-md-12">
        <div class="col-lg-2 col-md-3 font_label label_booking_ct">  
            <?=
            Yii::t("backend", "Product Category");
            ?>
        </div>
        <div class="col-lg-8 col-md-9 font_label">
            <?= $form->field($model, 'list_category')->inline(true)->checkboxlist(MstCategoriesProduct::listCategory())->label(false) ?>
        </div>
    </div>
    <!-- list_category end -->
    <!-- product_name start -->
    <div class="col-md-12">
        <div class="col-lg-2 col-md-3 font_label label_booking_ct">  
            <?=
            Yii::t("backend", "Name Product Booking");
            ?>
        </div>
        <div class="col-lg-2 col-md-4 input_booking_ct">
            <?= $form->field($model, 'product_name')->textInput(['maxlength' => true])->label(false) ?>
        </div>
    </div>
    <!-- product_name end -->
    <!-- customer_jan_code and sex start -->
    <div class="col-md-12">
        <div class="col-lg-2 col-md-3 font_label label_booking_ct">  
            <?=
            Yii::t("backend", "Membership card number");
            ?>
        </div>
        <div class="col-lg-2 col-md-4 input_booking_ct">
            <?= $form->field($model, 'customer_jan_code')->widget(\yii\widgets\MaskedInput::className(), [
            'mask' => '9',
            'clientOptions' => ['repeat' => 13, 'greedy' => false]
        ])->textInput(['maxlength' => true])->label(false) ?>
        </div>
        <div class="col-lg-1_5 col-md-2 font_label label_booking_ct" >  
            <?=
            Yii::t("backend", "Sex Coupon");
            ?>
        </div>
        <div class="col-lg-2 col-md-3 input_booking_ct" >
            <?= $form->field($model, 'sex')->dropDownList(Constants::LIST_SEX, ['prompt' => Yii::t('backend', 'Please Select')])->label(false) ?>
        </div>
    </div>
    <!-- customer_jan_code and sex end -->
    <!-- customer_first_name  start -->
    <div class="col-md-12">
        <div class="col-lg-2 col-md-3 font_label label_booking_ct">  
            <?=
            Yii::t("backend", "Name Customer");
            ?>
        </div>
        <div class="col-lg-0_5 col-md-1 font_label label-margin">
            <?=
            Yii::t("backend", "First name");
            ?>
        </div>
        <div class="col-lg-2_5 col-md-3" >
            <?=
            $form->field($model, 'customer_first_name')->textInput(['maxlength' => true])->label(false)
            ?>
        </div>
        <div class="col-lg-1_5 col-md-1 font_label label-margin"></div>
        <div class="col-lg-0_5 col-md-1 font_label label-margin">
            <?=
            Yii::t("backend", "Last Name Cuopon");
            ?>
        </div>
        <div class="col-lg-2_5 col-md-3">
            <?=
            $form->field($model, 'customer_last_name')->textInput(['maxlength' => true])->label(false)
            ?>
        </div>
    </div>
    <!-- customer_first_name end -->
    <!-- rank and mobile start -->
    <div class="col-md-12">
        <div class="col-lg-2 col-md-3 font_label label_booking_ct">  
            <?=
            Yii::t("backend", "Rank Customer");
            ?>
        </div>
        <div class="col-lg-2 col-md-4 input_booking_ct">
            <?= $form->field($model, 'rank')->dropDownList(Constants::RANK, ['prompt' => Yii::t('backend', 'Please Select')])->label(false) ?>
        </div>
        <div class="col-lg-1_5 col-md-2 font_label label_booking_ct" >  
            <?=
            Yii::t("backend", "People Send Tel");
            ?>
        </div>
        <div class="col-lg-2 col-md-3 input_booking_ct">
            <?=
            $form->field($model, 'mobile')->widget(\yii\widgets\MaskedInput::className(), [
                'mask' => '9',
                'clientOptions' => ['repeat' => 17, 'greedy' => false]
            ])->textInput(['maxlength' => 17])->label(false)
            ?>
        </div>
    </div>
    <!-- rank and mobile end -->
    <!-- Booking Times start -->
    <div class="col-md-12">
        <div class="col-lg-2 col-md-3 font_label label_booking_ct">  
            <?=
            Yii::t("backend", "Booking Times");
            ?>
        </div>
        <div class="col-lg-2 col-md-4 input_booking_ct">
            <?=
            $form->field($model, 'booking_time_min', [
                'template' => '<div class=" clear-padding">{input}{error}</div>',
                'inputTemplate' => '<div class="input-group">{input}<span class="input-group-addon"><b>回</b></span></div>',
            ])->widget(\yii\widgets\MaskedInput::className(), [
                'clientOptions' => [
                    'groupSeparator' => ',',
                    'alias' => 'integer',
                    'autoGroup' => true,
                    'removeMaskOnSubmit' => true,
                    'allowMinus' => false,
                ]
            ])->textInput(['maxlength' => true])
            ?>
        </div>
        <div  class="col-md-1 text-center separator">～</div>
        <div class="col-lg-2 col-md-4 input_booking_ct">
            <?=
            $form->field($model, 'booking_time_max', [
                'template' => '<div class="clear-padding">{input}{error}</div>',
                'inputTemplate' => '<div class="input-group">{input}<span class="input-group-addon"><b>回</b></span></div>',
            ])->widget(\yii\widgets\MaskedInput::className(), [
                'clientOptions' => [
                    'groupSeparator' => ',',
                    'alias' => 'integer',
                    'autoGroup' => true,
                    'removeMaskOnSubmit' => true,
                    'allowMinus' => false,
                ]
            ])->textInput(['maxlength' => true])
            ?>
        </div>
    </div>
    <!-- Booking Times end -->
    <!-- birth_date_from and birth_date_to start -->
    <div class="col-md-12"> 
        <div class="col-lg-2 col-md-3 font_label label_booking_ct">  
            <?=
            Yii::t("backend", "Birthday From");
            ?>
        </div> 
        <div class="col-lg-2 col-md-4 input_booking_ct">
            <?=
            $form->field($model, 'birth_date_from', [
                'template' => '<div class="clear-padding">{input}{error}</div>'
            ])->widget(DatePicker::classname(), [
                'language' => 'ja',
                'dateFormat' => 'yyyy/MM/dd',
                'clientOptions' => [
                    "changeMonth" => true,
                    "changeYear" => true,
                    "yearRange" => "1900:+0"
                ],
            ])->textInput(['maxlength' => 10])
            ?>
        </div> 
        <div  class="col-md-1 text-center separator">～</div>
        <div class="col-lg-2 col-md-4 input_booking_ct"> 
            <?=
            $form->field($model, 'birth_date_to', [
                'template' => '<div class="clear-padding">{input}{error}</div>'
            ])->widget(DatePicker::classname(), [
                'language' => 'ja',
                'dateFormat' => 'yyyy/MM/dd',
                'clientOptions' => [
                    "changeMonth" => true,
                    "changeYear" => true,
                    "yearRange" => "1900:+0"
                ],
            ])->textInput(['maxlength' => 10])
            ?>
        </div> 
    </div>
    <!-- birth_date_from and birth_date_to end -->
    <!--last_visit_date_from start -->
    <div class="col-md-12"> 
        <div class="col-lg-2 col-md-3 font_label label_booking_ct">  
            <?=
            Yii::t("backend", "Last Visit Date From");
            ?>
        </div> 
        <div class="col-lg-2 col-md-4 input_booking_ct">
            <?=
            $form->field($model, 'last_visit_date_from', [
                'template' => '<div class="clear-padding">{input}{error}</div>'
            ])->widget(DatePicker::classname(), [
                'language' => 'ja',
                'dateFormat' => 'yyyy/MM/dd',
                'clientOptions' => [
                    "changeMonth" => true,
                    "changeYear" => true,
                    "yearRange" => "1900:+0"
                ],
            ])->textInput(['maxlength' => 10])
            ?>
        </div> 
        <div  class="col-md-1 text-center separator">～</div>
        <div class="col-lg-2 col-md-4 input_booking_ct"> 
            <?=
            $form->field($model, 'last_visit_date_to', [
                'template' => '<div class="clear-padding">{input}{error}</div>'
            ])->widget(DatePicker::classname(), [
                'language' => 'ja',
                'dateFormat' => 'yyyy/MM/dd',
                'clientOptions' => [
                    "changeMonth" => true,
                    "changeYear" => true,
                    "yearRange" => "1900:+0"
                ],
            ])->textInput(['maxlength' => 10])
            ?>
        </div> 
    </div>
    <!-- last_visit_date_from end -->
    <!-- Last time staff start -->
    <div class="col-md-12">
        <div class="col-lg-2 col-md-3 font_label staff-id label_booking_ct">  
            <?=
            Yii::t("backend", "Last time staff");
            ?>
        </div>

        <div class="col-lg-2 col-md-4 staff-id input_booking_ct">
            <?= Html::hiddenInput('staff-id-last', $model->last_staff_id, ['id' => 'staff-id-last']); ?>
            <?=
                    $form->field($model, 'last_staff_id')->dropDownList([], ['prompt' => Yii::t('backend', 'Please Select')])
                    ->widget(DepDrop::classname(), [
                        'options' => ['id' => Html::getInputId($model, 'last_staff_id')],
                        'pluginOptions' => [
                            'depends' => [Html::getInputId($model, 'store_id')],
                            'placeholder' => '',
                            'url' => Url::to(['/master-customer/select-staff-by-id-store']),
                            'initialize' => false,
                            'params' => ['staff-id-last']
                        ]
                    ])->label(false)
            ?>

        </div>
        <div class="col-lg-2 col-md-3 font_label seat-id label_booking_ct">  
            <?=
            Yii::t("backend", "Last Seat");
            ?>
        </div>
        <div class="col-lg-2 col-md-4 seat-id input_booking_ct">
            <?= Html::hiddenInput('seat-id-last', $model->last_seat_id, ['id' => 'seat-id-last']); ?>
            <?=
                    $form->field($model, 'last_seat_id')->dropDownList([], ['prompt' => Yii::t('backend', 'Please Select')])
                    ->widget(DepDrop::classname(), [
                        'options' => ['id' => Html::getInputId($model, 'last_seat_id')],
                        'pluginOptions' => [
                            'depends' => [Html::getInputId($model, 'store_id')],
                            'placeholder' => '',
                            'url' => Url::to(['/master-customer/select-seat-by-id-store']),
                            'initialize' => false,
                            'params' => ['seat-id-last']
                        ]
                    ])->label(false)
            ?>

        </div>


    </div>
    <!-- Last time staff end -->
    <!-- Service staff start -->
    <div class="col-md-12">
        <div class="col-lg-2 col-md-3 label-margin staff-id label_booking_ct">  
            <?=
            Yii::t("backend", "Service staff");
            ?>
        </div>
        <div class="col-lg-2 col-md-4 staff-id input_booking_ct">
            <?= Html::hiddenInput('service-staff', $model->service_staff_id, ['id' => 'service-staff-id']); ?>
            <?=
                    $form->field($model, 'service_staff_id')->dropDownList(MasterStaff::getListStaff(), ['prompt' => Yii::t('backend', 'Please Select')])
                    ->widget(DepDrop::classname(), [
                        'options' => ['id' => Html::getInputId($model, 'service_staff_id')],
                        'pluginOptions' => [
                            'depends' => [Html::getInputId($model, 'store_id')],
                            'placeholder' => '',
                            'url' => Url::to(['/master-customer/select-staff-by-id-store']),
                            'initialize' => false,
                            'params' => ['service-staff-id']
                        ]
                    ])->label(false)
            ?>


        </div>

        <div class="col-lg-2 col-md-3 font_label label-margin seat-id label_booking_ct">  
            <?=
            Yii::t("backend", "Type Seat Name");
            ?>
        </div>
        <div class="col-lg-2 col-md-4 seat-id input_booking_ct">
            <?= Html::hiddenInput('seat-id-last', $model->seat_type, ['id' => 'seat-type-id']); ?>
            <?=
                    $form->field($model, 'seat_type')->dropDownList([], ['prompt' => Yii::t('backend', 'Please Select')])
                    ->widget(DepDrop::classname(), [
                        'options' => ['id' => Html::getInputId($model, 'seat_type')],
                        'pluginOptions' => [
                            'depends' => [Html::getInputId($model, 'store_id')],
                            'placeholder' => '',
                            'url' => Url::to(['/master-customer/select-type-seat-by-id-store']),
                            'initialize' => true,
                            'params' => ['seat-type-id']
                        ]
                    ])->label(false)
            ?>
        </div>
    </div>
    <!-- Service staff end -->
</div>
<!-- Button submit and back-->
<div class="form-group">
    <?= Html::a(Yii::t('backend', 'Return'), ['/'], ['class' => 'btn common-button-default btn-default']) ?>
    <?= Html::submitButton(Yii::t('backend', 'Search'), ['class' => 'btn common-button-submit']) ?>
</div>

<!-- End Button submit and back-->

<!-- Datpdt end -->   
<?php ActiveForm::end(); ?>
<script>
    $('#<?= Html::getInputId($model, 'last_seat_id') ?>').on('depdrop.afterChange', function (event) {
        var $el = $(this);
        var chkOptions = $el.find('option').length;
        if (chkOptions === 0 || $el.find('option[value=""]').length === chkOptions) {
            $('.seat-id').hide();
        } else {
            $('.seat-id').show();
        }
    });

    $('#<?= Html::getInputId($model, 'last_staff_id') ?>').on('depdrop.afterChange', function (event) {
        var $el = $(this);
        var chkOptions = $el.find('option').length;
        if (chkOptions === 0 || $el.find('option[value=""]').length === chkOptions) {
            $('.staff-id').hide();
        } else {
            $('.staff-id').show();
        }
    });
    $('.staff-id').hide();
    $('.seat-id').hide();
</script>