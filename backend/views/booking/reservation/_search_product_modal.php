<?php

    /*
     * To change this license header, choose License Headers in Project Properties.
     * To change this template file, choose Tools | Templates
     * and open the template in the editor.
     */

    use yii\helpers\Html;
    use yii\bootstrap\Collapse;
    use yii\bootstrap\ActiveForm;
?>


<style>

  .panel-heading .collapse-toggle[aria-expanded=true]:after {
    /* symbol for "opening" panels */
    font-family: 'Glyphicons Halflings';  /* essential for enabling glyphicon */
    content: "\e114";    /* adjust as needed, taken from bootstrap.css */
    float: right;        /* adjust as needed */
    color: grey;         /* adjust as needed */
  }
  .panel-heading .collapse-toggle.collapsed:after ,.panel-heading .collapse-toggle:after {
    /* symbol for "collapsed" panels */
    font-family: 'Glyphicons Halflings'; 
     float: right;        /* adjust as needed */
    color: grey;         /* adjust as needed */
    content: "\e080";    /* adjust as needed, taken from bootstrap.css */
  }
  .panel-heading .collapse-toggle {
      width: 100%;
      display: inherit;
  }
</style>
<?php
    $couponselected = Yii::$app->session->get('BookingCoupon'); //session save coupon selected
    $couponselected = $couponselected == null ? [] : $couponselected;
    $arrCoupon = [];
    if(count($listCoupon) >0 ){
    foreach ($listCoupon as $key => $value) {
      
      //$arrCoupon[] = Html::checkbox('BookingCoupon[]', in_array($key, $couponselected) ?true:false ,['value'=>$key]).Html::encode( $value);
        $arrCoupon[] = Html::checkbox('BookingCoupon[]', in_array($key, $couponselected) ?true:false ,['value'=>$key,'data-type-coupon'=>$value['combine_with_other_coupon_flg'], 'class'=>'list-coupon']).Html::encode( $value['name']); 
    }
    }else{
        $arrCoupon[] = Html::tag('h4',Yii::t('backend', "No coupon"));
    }
?>


<?php
     $form = ActiveForm::begin([
         'id' => 'booking-product'
     ]);
    echo Collapse::widget([
        'items' => [
            // equivalent to the above
            [
                'label'   => Yii::t('backend', 'Coupon'),
                'content' => $arrCoupon,
                // open its content by default
               
            ],
        ]
    ]);
?>
<?php
    $productSelected = Yii::$app->session->get('BookingProduct'); //session save product selected
    $productSelected = $productSelected == null ? [] : $productSelected;
    foreach ($listCategory as $key => $value) {
      if(count($products[$key]) > 0){
      $arrCategory = [];
          foreach ($products[$key] as $product) {

            $arrCategory[] = Html::checkbox('BookingProduct[]', in_array($product['id'], $productSelected) ?true:false ,['value'=>$product['id']]).Html::encode($product['name']);
            
          }
      


      echo Collapse::widget([
          'items' => [
              // equivalent to the above
              [
                  'label'   => $value,
                  'content' => $arrCategory,
//                        'contentOptions' => ['class' => 'in']
              ],
          ]
      ]);
      }
    }
    
    ActiveForm::end();
?>

<script>
  
    $(document).on('click', '.list-coupon', function () {
        var typeCoupon = $(this).data('type-coupon');
        var checked    = $(this).prop('checked');
        if(typeCoupon == 0 && checked == true){
           $('.list-coupon').prop('checked',false).prop('disabled', true);
          $(this).prop('checked',true).prop('disabled', false);
          
        }else if(typeCoupon == 0 && checked == false ){
          resetCheckboxCoupon();
        }
        //
        if(typeCoupon == 1 && checked == true){
          $('.list-coupon[data-type-coupon=0]').prop('checked',false).prop('disabled', true);
          
        }else if(typeCoupon == 1 && checked == false ){
 
            if($('input[type="checkbox"][name="BookingCoupon[]"]:checked').length  == 0) {resetCheckboxCoupon()}

        }
        
    })
    
    function resetCheckboxCoupon(){
      $('.list-coupon').prop('checked',false).prop('disabled', false);
    }
      
</script>