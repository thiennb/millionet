<?php

    /* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
 use yii\helpers\Html;
 use yii\bootstrap\ActiveForm;
 use yii\helpers\ArrayHelper;
   
?>
<div>
  <div class="reservation-number row text-bold" style="margin: 10px"><?= Yii::t('backend', 'Reservation number').' : '. $numberPeople ?> </div>
  <?php $form = ActiveForm::begin([
         'id' => 'booking-seat'
        ]);
  foreach ($seats as $seat) { ?>
    
  <div class="row" style="margin: 10px">
        <div class="col-md-1">
          <?php echo Html::checkbox('seatId[]', in_array($seat['id'], $seatId) ?true:false ,['value'=>$seat['id'] ,
                                                        'class' =>'cb-seat-choose' ,
                                                        'data-maxpeople'  =>$seat['capacity_max'],
                                                        'data-seatcode'  =>$seat['seat_code'],
                                                        'data-actualload' => isset($seatChoose[$seat['id']])?$seatChoose[$seat['id']]['actual_load']:0
                                                        ]) ?>
         
        </div>
        <div class="col-md-8 ">
          
          <?= Yii::t('backend', 'Name') ?> &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp : &nbsp <?= $seat['name'] ?>  

        </div> 
        <div class="col-md-2 ">
           <?= Yii::t('backend', 'Maximum capacity') ?>  :  <?= $seat['capacity_max'] ?>
        </div>
      </div>
  <?php }?>
  <?php    ActiveForm::end(); ?>
  
</div>
<script>
  $(function () {
     maxPeople = <?= $numberPeople ?>;
//    $('input[type=checkbox][class=cb-seat-choose]').each(function () {
//        var sThisVal = $(this).checked ? "1" : "0";
//    console.log();
//    });
    $('.cb-seat-choose').trigger('change');
  })
</script>  