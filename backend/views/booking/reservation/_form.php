<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Modal;
use common\widgets\preview\Preview;
use common\components\Constants;
use yii\jui\DatePicker;
use yii\widgets\Pjax;
use yii\bootstrap\Collapse;
use yii\helpers\Url;
use common\models\MasterStore;

/* @var $this yii\web\View */
/* @var $model common\models\MasterCustomer */
/* @var $form yii\widgets\ActiveForm */
$session = yii::$app->session;
$this->registerJsFile('@web/js/booking/form-booking.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
?>

<?php
$template1 = [
    'options' => ['class' => 'form-group '],
    'template' => '<div class="row" ><div class="col-md-3 label-margin label_booking_ct"> {label}</div><div class="col-md-4 input_store_ct">{input} {hint} {error}</div>  </div>'
        ]
?>
<div class="row ">
    <div class="box-header with-border common-box-h4 col-md-8">
        <h4 class="text-title"><b><?= Yii::t('backend', 'Create Master Customer') ?></b></h4>
    </div>
</div>

<div class="master-customer-form">

    <?php
    $form = ActiveForm::begin([
                //'layout' => 'horizontal',
                'id' => 'form-booking',
                'validationUrl' => 'validate',
                'enableClientValidation' => false,
                'enableAjaxValidation' => true,
                'validateOnChange' => false,
                'validateOnBlur' => false,
                'successCssClass' => '',
    ]);
    ?>
    <?php Pjax::begin(['enablePushState' => false, 'id' => 'p1']) ?>
    <?= $form->field($model, 'id')->hiddenInput()->label(false) ?>
    <!-- Datpdt custom layout start -->
    <div class="col-md-12">
        <!-- Membership card number start -->
        <div class="row">
            <div class="col-md-3 label-margin label_booking_ct">  

                <?=
                Yii::t("backend", "Membership card number");
                ?>
            </div>
            <div class="col-md-4 input_store_ct">
                <?=
                $form->field($model, 'customer_jan_code')->widget(\yii\widgets\MaskedInput::className(), [
                    'mask' => '9',
                    'clientOptions' => ['repeat' => 13, 'greedy' => false]
                ])->textInput(['maxlength' => true])->label(false);
                ?> 
            </div> 
            <div class="col-md-3 font_label">
                <?= $form->field($modelCustormerStore, 'black_list_flg')->checkbox() ?>
            </div> 
        </div>
        <!-- Membership card number end -->
        <!--  Name and Name Kana -->    
        <div class="row"> 
            <div class="col-md-3 label-margin label_booking_ct">  
                <?= Yii::t("backend", "Name"); ?>
            </div>
            <div class="col-md-1  label-margin required-star" style="padding-right: 0;">
                <?= Html::activeLabel($model, 'first_name'); ?>
            </div>
            <div class="col-md-2">
                <?=
                $form->field($model, 'first_name', [
                    'template' =>
                    '<div class="col-md-12">{input}{error}</p></div>'
                ])->textInput(['maxlength' => true, 'style' => ''])->label(false)
                ?>
            </div>
            <div class="col-md-1 no-padding-right label-margin required-star" style="padding-right: 0;">
                <?= Yii::t("backend", "Last Name"); ?>
            </div>
            <div class="col-md-2">
                <?=
                $form->field($model, 'last_name', [
                    'template' =>
                    '<div class="col-md-12">{input}{error}</p></div>'
                ])->textInput(['maxlength' => true, 'style' => ''])->label(false)
                ?>
            </div>
        </div>
        <div class="row"> 
            <div class="col-md-3 label-margin label_booking_ct">  
                <?= Yii::t("backend", "Name kana"); ?>
            </div>
            <div class="col-md-1 label-margin required-star" style="padding-right: 0;">
                <?= Yii::t("backend", "First Name Kana"); ?>
            </div>
            <div class="col-md-2">

                <?=
                $form->field($model, 'first_name_kana', [
                    'template' =>
                    '<div class="col-md-12">{input}{error}</p></div>'
                ])->textInput(['maxlength' => true, 'style' => ''])->label(false)
                ?>
            </div>
            <div class="col-md-1 n label-margin required-star" style="padding-right: 0;">
                <?= Yii::t("backend", "Last Name Kana"); ?>
            </div>
            <div class="col-md-2">
                <?=
                $form->field($model, 'last_name_kana', [
                    'template' =>
                    '<div class="col-md-12">{input}{error}</p></div>'
                ])->textInput(['maxlength' => true, 'style' => ''])->label(false)
                ?>
            </div>
            <div class="col-md-1">
                <?=
                Html::button(Yii::t('backend', 'Customer search'), [
                    'class' => 'btn common-button-submit btn-ajax-modal',
                    'value' => yii\helpers\Url::to('searchcustomer'),
                    'id' => 'btn-search',
                ])
                ?>
            </div>
        </div>
        <!--  End and Name Kana -->
        <!-- sex start -->

        <?= $form->field($model, 'sex', $template1)->dropDownList(Constants::LIST_SEX) ?> 


        <!-- sex and-->
        <!-- Birthday start -->

        <?=
        $form->field($model, 'birth_date', $template1)->widget(DatePicker::classname(), [
            'language' => 'ja',
            'dateFormat' => 'yyyy/MM/dd',
            'clientOptions' => [
                "changeMonth" => true,
                "changeYear" => true,
                "yearRange" => "1900:+0"
            ],
            'options' => [
                'maxlength' => 10
            ]
        ])
        ?>


        <!-- Birthday end -->
        <!-- Phone start -->

        <?=
        $form->field($model, 'mobile', $template1)->widget(\yii\widgets\MaskedInput::className(), [
            'mask' => '9',
            'clientOptions' => ['repeat' => 17, 'greedy' => false]
        ])->textInput(['maxlength' => 17])
        ?>


        <!-- Phone end -->
        <!-- Email start -->

        <?= $form->field($model, 'email', $template1)->textInput(['maxlength' => true]) ?>

        <!-- Email end -->
        <!-- PostCode start -->
        <div class="form-group field-mastercustomer-post_code"> 

            <?=
                    $form->field($model, 'post_code', [
                        'template' => '<div class="row" ><div class="col-md-3 label-margin label_booking_ct"> {label}</div><div class="col-md-3">{input}<br>{hint}{error}</div>  </div>',
                        'inputTemplate' => '<div class="col-md-8 no-padding post_code_booking_ct" style="">{input}</div><div class="col-md-4 post_code_btn_booking_ct"><a id="search-code" class="btn common-button-submit"> 住所検索</a> </div>',
                    ])->textInput(['maxlength' => true ])
                    ->widget(\yii\widgets\MaskedInput::className(), [
                        'mask' => '999-9999',
                        "options" => ['class'=>'form-control convert-haft-size'],
                        'clientOptions' => [
                            'removeMaskOnSubmit' => true
                        ]
                            
                        
                    ])
            ?>

        </div>
        <!-- PostCode end -->
        <!-- Address1 start-->

        <?= $form->field($model, 'address', $template1)->textInput(['maxlength' => true]) ?>

        <!-- Address1 end -->
        <!-- Address2 start-->

        <?= $form->field($model, 'address2', $template1)->textInput(['maxlength' => true]) ?>

        <!-- Address2 end -->
        <!-- Memo start -->

        <?= $form->field($model, 'memo', $template1)->textarea(['rows' => 6, 'maxlength' => true]) ?>

        <!-- Memo End -->
        <!-- News transmision start -->
        <div class="form-group field-mastercustomer-post_code"> 
            <div class="row"> 
                <div class="col-md-3 label_transmision_booking_ct">  
                    <label for="mastercustomer-news_transmision_flg" class="control-label">
                        <?= Yii::t("backend", "News transmision"); ?>
                    </label>
                </div>
                <div class="col-md-6 font_label">
                    <?= $form->field($modelCustormerStore, 'news_transmision_flg', $template1)->inline(true)->radioList(Constants::LIST_NEWS_TRANSMISION)->label(false) ?>
                </div> 
            </div>
        </div>
        <!-- News transmision end -->

        <!-- Datpdt custom layout end -->

        <a href="<?= Url::current() ?>" id="refrest-custormer" style="display: none"></a>

        <script>
            $("input[type=radio][name='CustomerStore[news_transmision_flg]'][value='1']").prop("disabled", true);
            $("#search-code").click(function (e) {
                getPostcode('#mastercustomer-post_code', '#mastercustomer-address');
            });
        </script>


        <div class="row ">
            <div class="box-header with-border common-box-h4 col-md-8">
                <h4 class="text-title"><b><?= Yii::t('backend', 'Reservation information') ?></b></h4>
            </div>
        </div>






        <div class="form-group">
            <div class="row">
                <div class="col-md-3" ><label for="booking-booking_code" class="control-label "><?= Yii::t("backend", "Booking Code"); ?></label></div>
                <div class="col-md-3" id="preview-booking_code"> <?= $modelBooking->booking_code != null ? $modelBooking->booking_code : "" ?></div>
                <div class="help-block help-block-error "></div>
            </div>
        </div>


        <!-- store_id start -->

        <?= $form->field($modelBooking, 'store_id', $template1)->dropDownList(common\models\MasterStore::getListStore()) ?>

        <!-- store_id end -->
        <div class="row">
            <div class="col-md-3 label-margin label_booking_ct">  
                <?=
                Yii::t("backend", "Booking Method");
                ?>
            </div>
            <div class="col-md-4 input_store_ct">
                <?= $form->field($modelBooking, 'booking_method')->dropDownList(Constants::LIST_BOOKING_MENTHOD)->label(false) ?>
            </div> 
            <div class="col-md-3 font_label">
                <?= $form->field($modelBooking, 'to_home_delivery_flg')->checkbox() ?>
            </div> 
        </div>
        <!-- booking_method start -->

        <!-- booking_method end -->
        <div class=" ">
            <?php Pjax::begin(['enablePushState' => false, 'id' => 'p2']) ?>
            <div class="row field-masterbooking-product">
                <div class="col-md-3 label_booking_ct">
                    <label class="control-label"> <?= Yii::t('backend', 'Product') ?></label>
                </div>
                <div class="col-md-4 product_select_booking_ct" id="preview-product">

                    <?php
                    foreach ($productBookedName as $productName) {
                        echo $productName . '<br>';
                    }
                    ?>
                    <?= $form->field($modelBooking, 'productSelect')->hiddenInput()->label(false) ?>
                </div>
                <div class="col-md-3">
                    <?=
                    Html::button(Yii::t('backend', 'Product selection'), [
                        'class' => 'btn common-button-submit ',
                        'value' => yii\helpers\Url::to('searchproduct'),
                        'id' => 'btn-search-product',
                        'style' => 'width: 12em;'
                    ])
                    ?>
                </div>

            </div>
            <div class="row">
                <div class="col-md-3 label_booking_ct">
                    <label class="control-label "> <?= Yii::t('backend', 'Coupon') ?></label>
                </div>
                <div class="col-md-4" id="preview-coupon">

                    <?php
                    foreach ($couponsBookedName as $couponName) {
                        echo $couponName . '<br>';
                    }
                    ?>

                </div>
                <a href="<?= Url::current() ?>" id="refrest-coupon" style="display: none">Refresh</a>
                <?= ''//$form->field($modelBooking, 'productselect')->hiddenInput()->label(false)   ?>
            </div>


            <div class="row">
                <div class="col-md-3 label_booking_ct">
                    <label class="control-label "> <?= Yii::t('backend', 'Option') ?></label>
                </div>
                <div class="col-md-4 product_select_booking_ct" id="preview-option">
                    <?php
                    foreach ($optionsBookedName as $optionName) {
                        echo $optionName . '<br>';
                    }
                    ?>
                </div>
                <a href="<?= Url::current() ?>" id="refrest-option" style="display: none">Refresh</a>
                <div class="col-md-3">
                    <?=
                    Html::button(Yii::t('backend', 'Select commodity option'), [
                        'class' => 'btn common-button-submit ',
                        'value' => yii\helpers\Url::to('searchoption'),
                        'id' => 'btn-search-option',
                        'style' => 'width: 12em;',
                        'disabled' => $session->has('BookingCoupon') || $session->has('BookingProduct') ? false : true
                    ])
                    ?>
                </div>

            </div>
            <!--Staff booking-->
            <br>


            <?php if (isset($session->get('BookingStore')['booking_resources']) && $session->get('BookingStore')['booking_resources'] == MasterStore::BOOKING_RESULT_SALON || !$session->has('BookingStore')) {
                ?>
                <!--Staff type--> 
                <div class="row">

                    <div class="col-md-3 label_booking_ct">
                        <label class="control-label "> <?= Yii::t('backend', 'Date time') ?></label>
                    </div>
                    <div class="col-md-4 product_select_booking_ct" id="preview-booking_date">
                        <?php
                        $date = isset($staffBookedName['booking_date']) ? Yii::$app->formatter->asDate($staffBookedName['booking_date']) . " " . $staffBookedName['start_time'] : '';
                        echo $date;
                        ?>

                    </div>
                    <div class="col-md-3">
                        <?=
                        Html::button(Yii::t('backend', 'Date and staff'), [
//               'data-pjax' => false,
                            'class' => 'btn common-button-submit ',
                            'value' => yii\helpers\Url::to('searchstaffschedule'),
                            'id' => 'btn-staff-select',
                            'style' => 'width: 12em;',
                            'disabled' => ($session->has('BookingCoupon') && count($session->get('BookingCoupon')) > 0 ) || ($session->has('BookingProduct') && count($session->get('BookingProduct')) > 0 ) || ($session->has('BookingOption') && count($session->get('BookingOption')) > 0 ) ? false : true
                        ])
                        ?>
                    </div>  
                </div> 
                <div class="row">

                    <div class="col-md-3 label_booking_ct">
                        <label class="control-label "> <?= Yii::t('backend', 'Staff') ?></label>
                    </div>
                    <div class="col-md-4 product_select_booking_ct" id="preview-staff_name">

                        <?php
                        $staffName = isset($staffBookedName['staff_name']) ? $staffBookedName['staff_name'] : '';
                        echo $staffName;
                        ?>
                        <?= $form->field($modelBooking, 'staffSelect')->hiddenInput()->label(false) ?>
                    </div>
                    <div class="col-md-3 font_label">

                        <?=
                        $form->field($modelBooking, 'customer_specify')->checkbox();
                        ?>
                    </div>
                </div>  
            <?php } else { ?>
                <!--Seat type-->
                <div class="row">

                    <div class="col-md-3 label_booking_ct">
                        <label class="control-label "> <?= Yii::t('backend', 'Date time') ?></label>
                    </div>
                    <div class="col-md-4" id="preview-booking_date">
                        <?php
                        $date = isset($staffBookedName['booking_date']) ? Yii::$app->formatter->asDate($staffBookedName['booking_date']) . " " . $staffBookedName['start_time'] : '';
                        echo $date;
                        ?>

                    </div>
                    <div class="col-md-3">
                        <?=
                        Html::button(Yii::t('backend', 'Date and seat type'), [
//               'data-pjax' => false,
                            'class' => 'btn common-button-submit ',
                            'value' => yii\helpers\Url::to('searchseatschedule'),
                            'id' => 'btn-seat-select',
                            'style' => 'width: 12em;',
                            'disabled' => ($session->has('BookingCoupon') && count($session->get('BookingCoupon')) > 0 ) || ($session->has('BookingProduct') && count($session->get('BookingProduct')) > 0 ) || ($session->has('BookingOption') && count($session->get('BookingOption')) > 0 ) ? false : true
                        ])
                        ?>
                    </div>  
                </div>
                <div class="row">

                    <div class="col-md-3 label_booking_ct">
                        <label class="control-label "> <?= Yii::t('backend', 'Type Seat Name') ?></label>
                    </div>
                    <div class="col-md-4" id="preview-staff_name">

                        <?php
                            if(isset($staffBookedName['seat_name']) && count($staffBookedName['seat_name']) >0 ){
                              foreach ($staffBookedName['seat_name'] as $idSeat => $nameSeat) {
                                echo $nameSeat. '<br>';
                              }
                            }
                        
                        ?>
                        <?= $form->field($modelBooking, 'staffSelect')->hiddenInput()->label(false) ?>
                    </div>
                    <div class="col-md-3">

                    </div>
                </div> 

            <?php } ?>




            <?php Pjax::end() ?>
        </div>

        <!-- Demand start -->

        <?= $form->field($modelBooking, 'demand', $template1)->textarea(['rows' => 6, 'maxlength' => true]) ?>

        <!-- Demand end -->
        <!-- memo start -->

        <?= $form->field($modelBooking, 'memo_manager', $template1)->textarea(['rows' => 6, 'maxlength' => true]) ?>

        <!-- memo end -->
        <?php $point = Yii::$app->formatter->asDecimal($modelCustormerStore->total_point, 0); ?>
        <!-- point_use start -->
        <?php Pjax::begin(['enablePushState' => false, 'id' => 'p8']) ?>
        <div class="row">
            <div class="col-md-3 label-margin label_booking_ct">  
                <?=
                Yii::t("backend", "Point use");
                ?>
            </div>
            <div class="col-md-6">
                <?php
                $customerId = $model->id != null ? $model->id : 0;
                $pointModal = common\models\CustomerStore::findOne(['customer_id' => $customerId, 'store_id' => $modelBooking->store_id]);
                $point = count($pointModal) > 0 ? Yii::$app->formatter->asDecimal($pointModal->total_point, 0) : 0;
                ?>
                <?=
                $form->field($modelBooking, 'point_use', [
                    'inputTemplate' => "<div class='col-md-6 no-padding'>{input}</div><div class='col-md-6' style='font-size: 16px; line-height: 36px'> P (現在ポイント) &nbsp $point P</div>",
                ])->widget(\yii\widgets\MaskedInput::className(), [
//                    "options" => ['class'=>'form-control convert-haft-size'],
                    'clientOptions' => [
                        'groupSeparator' => ',',
                        'alias' => 'integer',
                        'autoGroup' => true,
                        'removeMaskOnSubmit' => true,
                        'allowMinus' => false,
            ]])->textInput(['maxlength' => true,'class'=>'form-control convert-haft-size'])->label(false)
                ?>


            </div> 
        </div>
        <a href="<?= Url::current() ?>" id="refrest-point" style="display: none"></a>
        <?php Pjax::end() ?>
    </div>
    <?php Pjax::end(); ?>
    <!-- point_use end -->

    <div class="row text-center"> 
        <?= Html::a(Yii::t('backend', 'Return'), ['index', 'date' => date("Y/m/d")], ['class' => 'btn btn-default common-button-default', 'style' => 'margin-top: 1em;']) ?>
        <?=
        Html::submitButton(Yii::t('backend', 'Confirm'), [
            'class' => 'btn common-button-submit ',
//                        'onclick'=>"window.location.href = '" . \Yii::$app->urlManager->createUrl(['/create','id'=>$model->id]) . "';",
//                        'data-toggle'=>'tooltip',
//                        'title'=>Yii::t('app', 'Create New Record'),
            'value' => "123s",
            'id' => 'btn-searchs',
            'style' => 'margin-top: 1em;'
        ])
        ?>
    </div>


    <div class="row">
        <?php
        Modal::begin([
            'id' => 'submit-modal',
            'size' => 'SIZE_LARGE',
            'header' => '<div class="box-header with-border common-box-h4 col-md-8"><h4 class="text-title"><b>' . Yii::t('backend', 'Confirm Notice') . '</b></h4></div>',
            'footer' => Html::button(Yii::t('backend', 'Close'), ['class' => 'btn common-button-default', 'data-dismiss' => "modal", 'id' => 'btn-close'])
            . Html::submitButton(Yii::t('backend', 'Save'), ['class' => 'btn common-button-submit', 'id' => 'btn-submit']),
            'closeButton' => FALSE,
        ]);
        ?>
        <?=
        \common\widgets\preview\PreviewBooking::widget([
            "data" => [
                'first_name,last_name' => [
                    'type' => 'input_mult',
                    'label' => Yii::t('backend', 'Name Customer')
                ],
                'mobile' => [
                    'type' => 'input',
                    'label' => Yii::t('backend', 'Phone')
                ],
                'booking_code' => [
                    'type' => 'label',
                    'label' => Yii::t('backend', 'Booking Code')
                ],
                'store_id' => [
                    'type' => 'select',
                    'label' => Yii::t('backend', 'Booking Store')
                ],
                'booking_method' => [
                    'type' => 'select',
                    'label' => Yii::t('backend', 'Booking Method')
                ],
                'product' => [
                    'type' => 'label',
                    'label' => Yii::t('backend', 'Product')
                ],
                'option' => [
                    'type' => 'label',
                    'label' => Yii::t('backend', 'Option')
                ],
                'coupon' => [
                    'type' => 'label',
                    'label' => Yii::t('backend', 'Coupon')
                ],
                'booking_date' => [
                    'type' => 'label',
                    'label' => Yii::t('backend', 'Date time')
                ],
                'staff_name' => [
                    'type' => 'label',
                    'label' => Yii::t('backend', 'Staff')
                ],
                'demand' => [
                    'type' => 'text_area',
                    'label' => Yii::t('backend', 'Demand')
                ],
                'memo_manager' => [
                    'type' => 'text_area',
                    'label' => Yii::t('backend', 'Memo manager')
                ],
                'point_use' => [
                    'type' => 'input',
                    'label' => 'ご利用ポイント',
                    'syb' => 'P'
                ],
                'payment' => [
                    'type' => 'input',
                    'label' => Yii::t('backend', 'Money Pay'),
                    'syb' => '円'
                ],
            ],
            "modelName" => $model->formName(),
            "modelName_1" => 'booking',
            'idBtnConfirm' => 'btn-searchs',
            'formId' => 'form-booking',
            'btnClose' => 'btn-close',
            'btnSubmit' => 'btn-submit',
            'modalId' => 'submit-modal'
        ])
        ?>
        <?php
        Modal::end();
        ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php
Modal::begin([
    'id' => 'userModal',
    'size' => 'SIZE_LARGE',
    'header' => '<div class="box-header with-border common-box-h4 col-md-8"><h4 class="text-title"><b>' . Yii::t('backend', 'Customer search results') . '</b></h4></div>',
    'footer' => Html::button(Yii::t('backend', 'Close'), ['class' => 'btn common-button-default', 'data-dismiss' => "modal", 'id' => 'btn-close'])
    . Html::submitButton(Yii::t('backend', 'Customer choice'), ['class' => 'btn common-button-submit', 'id' => 'btn-custormer']),
]);
?>
<div id="modalContent">  </div>
<?php
Modal::end();
?>

<!--modal popup when choose product--> 
<?php
Modal::begin([
    'id' => 'productModal',
    'size' => 'SIZE_LARGE',
    'header' => '<div class="box-header with-border common-box-h4 col-md-8"><h4 class="text-title"><b>' . Yii::t('backend', 'Product selection') . '</b></h4></div>',
    'footer' => Html::button(Yii::t('backend', 'Close'), ['class' => 'btn common-button-default', 'data-dismiss' => "modal", 'id' => 'btn-close'])
    . Html::submitButton(Yii::t('backend', 'Product selection'), ['class' => 'btn common-button-submit', 'id' => 'btn-product', 'onclick' => 'submitProduct()']),
    'closeButton' => FALSE,
    'clientOptions' => ['backdrop' => 'static', 'keyboard' => false]
]);
?>
<div id="modalProductContent"> 

</div>
<?php
Modal::end();
?>    

<!--modal popup when choose option--> 
<?php
Modal::begin([
    'id' => 'optionModal',
    'size' => 'SIZE_LARGE',
    'header' => '<div class="box-header with-border common-box-h4 col-md-8"><h4 class="text-title"><b>' . Yii::t('backend', 'Select commodity option') . '</b></h4></div>',
    'footer' => Html::button(Yii::t('backend', 'Close'), ['class' => 'btn common-button-default', 'data-dismiss' => "modal", 'id' => 'btn-close'])
    . Html::submitButton(Yii::t('backend', 'Select Options'), ['class' => 'btn common-button-submit', 'id' => 'btn-option', 'onclick' => 'submitOption()']),
    'closeButton' => FALSE,
    'clientOptions' => ['backdrop' => 'static', 'keyboard' => false]
]);
?>
<div id="modalOptionContent"> 

</div>
<?php
Modal::end();
?>   

<!--modal popup when choose staff--> 
<?php
Modal::begin([
    'id' => 'staffModal',
    'size' => 'SIZE_LARGE_EX',
    'header' => '<b>' . Yii::t('backend', 'Date and staff') . '</b>',
    'footer' => Html::button(Yii::t('backend', 'Close'), ['class' => 'btn common-button-default', 'data-dismiss' => "modal", 'id' => 'btn-close'])
    . Html::submitButton(Yii::t('backend', 'Select Options'), ['class' => 'btn common-button-submit', 'id' => 'btn-option', 'onclick' => 'submitStaff()']),
]);
?>
<div id="modalStaffContent"> 

</div>
<?php
Modal::end();
?> 
