<?php

    use yii\helpers\Html;
    use yii\bootstrap\ActiveForm;
    use yii\widgets\Pjax;
    use common\components\Constants;
    use common\models\Booking;
    use yii\helpers\Url;

/* @var $this yii\web\View */
    /* @var $model common\models\Booking */
    $this->title = Yii::t('backend', 'Reservation · Account details');

    $this->params['breadcrumbs'][] = ['label' =>  Yii::t('backend', '予約履歴'), 'url' => Yii::$app->request->referrer];
    $this->params['breadcrumbs'][] = $this->title;
    $this->registerJsFile('@web/js/booking/view-booking.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
?>
<style>
  .master-booking-view{
    font-size: 15px;
  }
  .master-booking-view .row{
    margin-bottom: 10px;
  }
  .master-booking-view .btn-action{
    padding: 6px 10px;
    font-size: 16px;
  }
  .info-box{
    padding: 15px;
    background-color: #F9F9F9;
    padding-bottom: 2px;
  }
  #schedule-table{
    font-weight: bold;
  }
  .master-booking-view table thead tr th{
    background-color: #C5E0B4 !important;
  }
  hr{
    border-color:  black;
  }
  .green-box-h4 {
    margin-left: 15px;
    border-left: 10px solid #00AF4F;
    border-bottom:  2px solid #00AF4F !important;
  }
  .text-red-custorm{
    color: #FF0000 !important;
  }
</style>

<div class="row">
  <div class="col-lg-12 col-md-12">
    <div class="box box-info box-solid">
      <div class="box-header with-border">
        <h4 class="text-title"><b><?= $this->title ?></b></h4>
      </div>
      <div class="box-body content">
        <div class="col-md-12">
            <?php if ($order): ?>
                <?php
                echo $this->render('/booking/reservation/_detail_order', [
                    'model' => $model,
                    'order' => $order,
                    //'order_items' => $order_items
                ]);
                ?>
            <?php endif; ?>
          <div class="master-booking-view">
            <div class="row">
              <div class="box-header with-border common-box-h4 green-box-h4 col-md-4">
                <h4 class="text-title"><b><?= Yii::t('backend', 'Booking detail') ?></b></h4>
              </div>
            </div>



            <div class="row">
              <div class="col-md-3 label_preview_booking_ct">
                <b><?= Yii::t('backend', 'Customer name') ?></b>
              </div>
              <div class="col-md-3">
                <?= $model->masterCustomer->first_name . $model->masterCustomer->last_name ?>
              </div>
              <div class="col-md-3">
                <?= $model->imageBlackList; ?>
              </div>
              <div class="col-md-3" style="font-size: 20px; color: blue">
                <?= $model->booking_code ?>
              </div>
            </div>
            <div class="row">
              <div class="col-md-3 label_preview_booking_ct">
                <b><?= Yii::t('backend', 'Contact') ?></b>
              </div>
              <div class="col-md-9 view_booking_ct">
                <?= $model->masterCustomer->mobile ?>
              </div>
            </div>
            <div class="row">
              <div class="col-md-3 label_preview_booking_ct">
                <b><?= Yii::t('backend', 'Booking Store') ?></b>
              </div>
              <div class="col-md-9 view_booking_ct">
                <?= $model->storeMaster->name ?>
              </div>
            </div>
            <div class="row">
              <div class="col-md-3 label_preview_booking_ct">
                <b><?= Yii::t('backend', 'Booking Date Time') ?></b>
              </div>
              <div class="col-md-3">
              <?= Yii::$app->formatter->asDate($model->booking_date) . " " . $model->start_time . " ～ " . Yii::$app->formatter->asDate($model->booking_date) . " " . $model->end_time ?>
              </div>
              <div class="col-md-3 label_preview_booking_ct">
                <b><?= Yii::t('backend', 'Create at') ?></b>
              </div>
              <div class="col-md-3">
            <?= date('Y/m/d H:i', $model->created_at) ?>
              </div>
            </div>
        <?php Pjax::begin(['enablePushState' => false, 'id' => 'p3']) ?>

            <div class="row">
              <div class="col-md-3 label_preview_booking_ct">
                <b><?= Yii::t('backend', 'Booking status') ?></b>
              </div>
              <div class="col-md-9 status_booking_ct">
                <?= Constants::LIST_BOOKING_STATUS_KANRI[$model->status] ?>
                <?php
                    if ($model->status == Booking::BOOKING_STATUS_PENDING) {
                      echo Html::button(Yii::t('backend', 'Approval'), ['class' => 'btn common-button-submit btn-action', 'name' => 'btnAction',
                          'value' => Booking::BOOKING_STATUS_APPROVE,
                      ]);
                      echo Html::button(Yii::t('backend', 'Denial'), ['class' => 'btn common-button-submit btn-action', 'name' => 'btnAction', 'value' => Booking::BOOKING_STATUS_DENY]);
                      echo Html::button(Yii::t('backend', 'Cancel'), ['class' => 'btn common-button-submit btn-action', 'name' => 'btnAction', 'value' => Booking::BOOKING_STATUS_CANCEL]);
                    }
                    if ($model->status == Booking::BOOKING_STATUS_APPROVE && ($model->storeMaster->pos_use == 0 ||  $model->storeMaster->temporary_stop ==1 )) {
                      echo Html::button(Yii::t('backend', 'Finish'), ['class' => 'btn common-button-submit btn-action', 'name' => 'btnAction', 'value' => Booking::BOOKING_STATUS_FINISH]);
                    }
                    if ($model->status == Booking::BOOKING_STATUS_APPROVE) {
                      echo Html::button(Yii::t('backend', 'Cancel'), ['class' => 'btn common-button-submit btn-action', 'name' => 'btnAction', 'value' => Booking::BOOKING_STATUS_CANCEL]);
                    }


                ?>
                <a href="<?= Url::current() ?>" id="refrest-status" style="display: none"></a>
              </div>
            </div>

        <?php Pjax::end() ?>
            <div class="row">
              <div class="col-md-3 label_preview_booking_ct">
                <b><?= Yii::t('backend', 'Responsible(Seat)') ?></b>
              </div>
              <div class="col-md-9 demand_booking_ct">

                <?php

                    if($model->storeMaster->booking_resources == \common\models\MasterStore::BOOKING_RESULT_SALON && isset($model->staff)){
                        echo $model->staff->name;
                    }else if($model->storeMaster->booking_resources == \common\models\MasterStore::BOOKING_RESULT_RESTAURANCE && count($model->listSeat) >0){
                        foreach ($model->listSeat as $seat) {
                            echo   $seat->seat->name.'<br>';
                        }
                    }else{
                        echo Html::tag('span', Yii::t('backend', 'Please specify the person in charge'), ['class' => 'text-red-custorm']);
                    }
                ?>

              </div>
            </div>

            <table class="table table-striped table-bordered text-center">
              <thead>
                <tr>
                  <th><?= Yii::t('backend', 'Category') ?></th>
                  <th style=" width: 400px;"><?= Yii::t('backend', 'Content') ?></th>
                  <th><?= Yii::t('backend', 'Price') ?></th>
                  <th><?= Yii::t('backend', 'Quantity') ?></th>
                  <th><?= Yii::t('backend', 'Balance Money') ?></th>

                </tr>
              </thead>
              <tbody>
                <?php
                    if (isset($resultBooking['coupons'])) {

                      foreach ((array) $resultBooking['coupons'] as $key => $item) {
                        echo Html::beginTag('tr');
                        echo Html::tag('td', Yii::t('backend', 'Coupon'), ['class' => 'text-left', 'style' => 'text-align: left;']);
                        echo Html::tag('td', $item['name'], ['class' => 'text-left', 'style' => 'text-align: left;']);
                        echo Html::tag('td', Yii::$app->formatter->asInteger($item['total_price']), ['class' => 'text-right']);
                        echo Html::tag('td', 1, ['class' => 'text-right']);
                        echo Html::tag('td', Yii::$app->formatter->asInteger($item['total_price']), ['class' => 'text-right']);
                        echo Html::endTag('tr');
                      }
                    }
                    if (isset($resultBooking['products'])) {

                      foreach ((array) $resultBooking['products'] as $key => $item) {
                        echo Html::beginTag('tr');
                        echo Html::tag('td', Yii::t('backend', 'Product'), ['class' => 'text-left', 'style' => 'text-align: left;']);
                        echo Html::tag('td', $item['name'], ['class' => 'text-left', 'style' => 'text-align: left;']);
                        echo Html::tag('td', Yii::$app->formatter->asInteger($item['total_price']), ['class' => 'text-right']);
                        echo Html::tag('td', 1, ['class' => 'text-right']);
                        echo Html::tag('td', Yii::$app->formatter->asInteger($item['total_price']), ['class' => 'text-right']);
                        echo Html::endTag('tr');
                      }
                    }
                    if (isset($resultBooking['options'])) {

                      foreach ($resultBooking['options'] as $key => $item) {
//                  $price = \common\models\BookingSearch::getProductPriceTable($item->product_id, $model->booking_date)[0]['total_price'];
                        echo Html::beginTag('tr');
                        echo Html::tag('td', Yii::t('backend', 'Option'), ['class' => 'text-left', 'style' => 'text-align: left;']);
                        echo Html::tag('td', $item['name'], ['class' => 'text-left', 'style' => 'text-align: left;']);
                        echo Html::tag('td', Yii::$app->formatter->asInteger($item['total_price']), ['class' => 'text-right']);
                        echo Html::tag('td', 1, ['class' => 'text-right']);
                        echo Html::tag('td', Yii::$app->formatter->asInteger($item['total_price']), ['class' => 'text-right']);
                        echo Html::endTag('tr');
                      }
                    }
                ?>

              </tbody>
            </table>
            <div class="row" style="font-weight: bold">
              <div class="col-md-4 box1_booking_ct">
                <div class="info-box ">
                   <?= Yii::t('backend', 'Total fee') ?>
                  <br>
                  <div class="text-right"> <?= Yii::$app->formatter->asInteger($model->booking_price) ?> 円</div>
                  <hr>
                </div>
              </div>
              <div class="col-md-4 box2_booking_ct">

                <div class="info-box <?= $model->point_use > 0 ? 'text-orange' : '' ?> " >

                  <div > <span class="text-black"><?= Yii::t('backend', 'Use point') ?> </span> <span class="text-right" style="float: right"> <?= isset($model->point_use) ? Yii::$app->formatter->asInteger($model->point_use) : 0; ?> P </span> </div>
                  <div class="text-right ">  <?= Yii::$app->formatter->asInteger($model->moneyFromPoint) ?> 円</div>
                  <hr>
                </div>
              </div>
              <div class="col-md-4 box3_booking_ct">
                <div class="info-box ">

                   <?= Yii::t('backend', 'Payment schedule amount') ?>
                  <br>
                  <div class="text-right"> <?= Yii::$app->formatter->asInteger($model->booking_total) ?> 円</div>
                  <hr>
                </div>
              </div>
            </div>


            <div class="row">
              <div class="col-md-3 label_preview_booking_ct">
                    <b><?= Yii::t('backend', 'Demand') ?></b>
              </div>
              <div class="col-md-9 demand_booking_ct">
                    <?= $model->demand ?>
              </div>
            </div>
            <div class="row">
              <div class="col-md-3 label_preview_booking_ct">
                <b><?= Yii::t('backend', 'Memo manager') ?></b>
              </div>
              <div class="col-md-9 demand_booking_ct">
                    <?= $model->memo_manager ?>
              </div>
            </div>
            <?php if (!empty($check_list) && count($check_list) > 0) { ?>
                    <div class="row row-inline col-md-12">
                      <div class="col-md-3 div-header clear-padding div-border">
                        <div class="col-md-12"><b><?= Yii::t('backend', 'Question matters') ?></b></div>
                      </div>
                      <div class="col-md-9 clear-padding demand_booking_ct">
                            <?php foreach ($check_list as $key => $val) { ?>
                          <div class="col-md-12  clear-padding div-border">
                            <div class="col-md-12 div-border-bot"><?= ((int) $val->type == 2) ? Html::encode($val->question_content) : Html::encode($val->notice_content) ?>
                              <?php if (((int) $val->type == 2) && ((int) $val->option == 1)) { ?>
                                <?= Yii::t('backend', '(Select one only)') ?>
                              <?php } else if (((int) $val->type == 2) && ((int) $val->option == 2)) { ?>
                                <?= Yii::t('backend', "(Multiple choice) (maximum answer number {val})", ['val' => $val->max_choice]) ?>
                              <?php } else if (((int) $val->type == 2) && ((int) $val->option == 3)) { ?>
                              <?= Yii::t('backend', '(Free input)') ?>
                            <?php } ?></div>
                            <?php foreach ($val->answer_content as $key1 => $val1) { ?>
                              <div class="col-md-12"><div class="col-md-1"></div><div class="col-md-11"><?= Html::encode($val1) ?></div></div>
                          <?php } ?>
                          </div>
                    <?php } ?>
                      </div>
                    </div>
                <?php } ?>
            <div class="row">
              <div class="col-md-3 label_preview_booking_ct">
                <b><?= Yii::t('backend', 'Update at') ?></b>
              </div>
              <div class="col-md-9 demand_booking_ct">
              <?= Yii::$app->formatter->asTime($model->updated_at, 'yyyy/MM/dd H:i') ?>
              </div>
            </div>
            <div class="row">
              <div class="pull-right">
                <?= Html::a(Yii::t('backend', 'Return'), Yii::$app->request->referrer, ['class' => 'btn btn-default common-button-default', 'data-dismiss' => "modal", 'id' => 'btn-close']) ?>

                <?php if ($model->status != Booking::BOOKING_STATUS_FINISH && !$order) {
                  echo Html::a(Yii::t('backend', 'Reservations edit'), Url::to(['/booking/update','id'=>$model->id]), ['class' => 'btn common-button-submit ', 'id' => 'btn-edit-booking']);
                }  ?>

                <?php  if ($order) {
                    echo  Html::a(Yii::t('backend', 'Responding staff edit'), Url::to(['/booking/edit-responsible-staff','id'=>$model->id]), ['class' => 'btn common-button-submit ', 'id' => 'btn-staff-response']);
                } ?>
              </div>
            </div>



          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script>

  $(function () {



    $(document).on('click', '.btn-action', function () {
      // var modelId use in view _view
      modelId = <?= $model->id ?>
    })

  });




</script>
