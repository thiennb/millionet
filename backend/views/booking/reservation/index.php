<?php

    use yii\helpers\Html;
    use yii\grid\GridView;
    use yii\widgets\Pjax;
    use yii\bootstrap\Modal;
    use yii\helpers\Url;
    use common\models\MasterStaff;

/* @var $this yii\web\View */
    /* @var $searchModel common\models\BookingSearch */
    /* @var $dataProvider yii\data\ActiveDataProvider */

    $this->title = Yii::t('backend', 'Booking List');
    $this->params['breadcrumbs'][] = $this->title;

?>

<style>
  .btn-view {
    background:none!important;
    border:none;
    padding:0!important;
    font: inherit;
    /*border is optional*/
    border-bottom:1px solid #444;
    cursor: pointer;
    color: blue;
  }
</style>
<div class="row">
  <div class="col-lg-12 col-md-12">
    <div class="box box-info box-solid">
      <div class="box-header with-border">
        <h4 class="text-title"><b><?= $this->title ?></b></h4>
      </div>
      <div class="box-body content">
        <div class="col-md-12">
          <div class="common-box">
            <div class="row">
              <div class="box-header with-border common-box-h4 col-lg-8 col-md-12">
                <h4 class="text-title"><b><?= Yii::t('backend', 'Booking Search') ?></b></h4>
              </div>
            </div>

            <div class="box-body content">
              <div class="col-md-10">
                <?php echo $this->render('/booking/reservation/_search', ['model' => $searchModel]); ?>
              </div>
            </div>
          </div>
          <?php
              Pjax::begin();
          ?>
          <div class="common-box">
            <div class="row">
              <div class="box-header with-border common-box-h4 col-lg-8 col-md-12" style="margin-left: 1em;">
                <h4 class="text-title"><b><?= Yii::t('backend', 'Booking list') ?></b></h4>
              </div>
            </div>
            <div class="box-body content">
              <div class="col-md-12">

                <?= Html::a(Yii::t('backend', 'Create Master Booking'), ['/booking/create'], ['class' => ' pagination btn common-button-submit common-float-right'])?>
                <?php Pjax::begin(); ?>
                <?=
                    GridView::widget([
                        'dataProvider' => $dataProvider,
                        'tableOptions' => [
                            'class' => 'table table-striped table-bordered text-center',
                        ],
                        'layout' => "{pager}\n{summary}\n{items}",
                        'summaryOptions' => ['class' => 'common-clr-fl-right'],
                        'summary' => false,
                        'pager' => [
                            'class' => 'common\components\LinkPagerMillionet',
                            'options' => ['class' => 'pagination common-float-right'],
                        ],
                        'columns' => [
                            ['class' => 'yii\grid\SerialColumn', 'header'=>'No','options' => ['class' => 'with-table-no']],
                            [
                                'attribute' => 'store_name',
                                'header' => Yii::t("backend", "Booking Store"),
                            ],
                            [
                                'attribute' => 'booking_date',
                                'header' => Yii::t('backend', 'Booking Date Time'),
                                'value'  => function ($model){
                                      return Yii::$app->formatter->asDate($model->booking_date) . ' ' . $model->start_time ;
                                }
                            ],
                            [
                                'attribute' => 'customer_name',
                                'value' => function ($model) {
                                    return $model->first_name . $model->last_name . "<br />"
                                    . Html::a($model->booking_code, yii\helpers\Url::to(['/booking/view', 'id' => $model->id]) , ['class'=>'btn-view','data-pjax' => '0']);
                                },
                                'format' => 'raw',
                                'header' => Yii::t('backend', 'Customer Name <br />And Booking Code'),
                            ],
                            [
                                'attribute' => 'product_name',
                                'value' => function ($model) {
                                  $result = [];
                                  if (!empty($model->bookingProduct)) {
                                    foreach ($model->bookingProduct as $product) {
                                      if (!empty($product->product)) {
                                        array_push($result, htmlentities($product->product->name));
                                      }
                                    }
                                  }
                                  if (!empty($model->bookingCoupon)) {
                                    foreach ($model->bookingCoupon as $coupon) {
                                      if (!empty($coupon->coupon)) {
                                        array_push($result, htmlentities($coupon->coupon->title));
                                      }
                                    }
                                  }
                                  return implode("<br /> ", $result);
                                },
                                    'header' => Yii::t('backend', 'Product'),
                                    'format' => 'html',
                                ],
                                [
                                    'attribute' => 'booking_method',
                                    'value' => function ($model) {
                                      if (!empty($model->booking_method)) {
                                        $sts = \common\components\Constants::LIST_BOOKING_METHOD;
                                        return isset($sts[$model->booking_method]) ? $sts[$model->booking_method] : "";
                                      }
                                    }
                                ],
                                [
                                    'attribute' => 'staff_id',
                                    'header' => Yii::t('backend', 'Responsible'),
                                    'format' => 'html',
                                    'value' => function ($model) {
                                        if ($model->booking_resources === \common\models\MasterStore::BOOKING_RESULT_RESTAURANCE) {
                                          $seatName = [];
                                          if(!empty($model->listSeat)){
                                            foreach ($model->listSeat as $index => $seat) {
                                              $seatName[] =  $seat->seat->name;
                                            }
                                          }
                                          return implode("<br /> ", $seatName);
                                        } else if ($model->booking_resources === \common\models\MasterStore::BOOKING_RESULT_SALON) {
                                          return empty($model->staff) ? "" : $model->staff->name;
                                        }
                                    }
                                ],
                                [
                                    'attribute' => 'point_use',
                                    'header' => Yii::t('backend', 'Point Use'),
                                    'value' => function($model) {
                                      if (!empty($model->point_use))
                                        return $model->point_use . "P";
                                      else
                                        return "0P";
                                    }
                                ],
                                [
                                    'attribute' => 'booking_total',
                                    'header' => Yii::t('backend', 'Booking Total'),
                                    'value' => function($model) {
                                      if (!empty($model->booking_total)) {
                                        return number_format($model->booking_total, 0, ".", ",") . "円";
                                      } else
                                        return "0円";
                                    }
                                ],
                                [
                                    'attribute' => 'status',
                                    'value' => function($model) {
                                      if (!empty($model->status)) {
                                        $sts = \common\components\Constants::LIST_BOOKING_STATUS_KANRI;
                                        return isset($sts[strval($model->status)]) ? $sts[strval($model->status)] : "";
                                      }
                                    },
                                ],
                            ],
                            ]);
                            ?>
                          </div>
                        </div>
                      </div>
                      <?php Pjax::end(); ?>
                    </div>
                  </div>
                </div>
              </div>
            </div>



            <!--modal view simplified booking-->
            <?php
            Modal::begin([
                'id' => 'bookingModal',
                'size' => 'SIZE_LARGE',
                'header' => '<div class="box-header with-border common-box-h4 col-md-8"><h4 class="text-title"><b>' . Yii::t('backend', 'Reservations and accounting details') . '</b></h4></div>',
                'footer' =>  Html::button(Yii::t('backend', 'Close'), ['class' => 'btn common-button-default', 'data-dismiss' => "modal", 'id' => 'btn-close'])
                . Html::a(Yii::t('backend', 'Reservations edit'),null ,['class' => 'btn common-button-submit ', 'id' => 'btn-edit-booking'])
                . Html::a(Yii::t('backend', 'Responding staff edit'),null ,['class' => 'btn common-button-submit ', 'id' => 'btn-staff-response']),
            ]);
            ?>
            <div id="modalContent">  </div>
            <?php
            Modal::end();
            ?>
<script>
    
    sessionStorage.removeItem('bookingstaff');
    var modelId = 0;
    $(document).on('click', '.btn-view', function () {
//        $('#modalContent').html('');
//        $('#bookingModal').modal('show')
//                          .find('#modalContent')
//                          .load($(this).attr('value'));
            
    })




</script>
