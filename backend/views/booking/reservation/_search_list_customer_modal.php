<?php
    /*
     * To change this license header, choose License Headers in Project Properties.
     * To change this template file, choose Tools | Templates
     * and open the template in the editor.
     */
?>
<style>
    .list-customer{
        max-height:550px;
        overflow-y:scroll;
    }
    .box-search-customer{
        border-bottom: 1px dashed #ccc;
    }
</style>
<div class="box-search-customer">
    <div class="row"> 
        <div class="col-md-2 label-margin label_booking_ct">  
            <?= Yii::t("backend", "Name"); ?>
        </div>
        <div class="col-md-2  label-margin required-star" style="padding-right: 0;">
            <label for="mastercustomer-first_name"><?= Yii::t("backend", "First Name"); ?></label>            
        </div>
        <div class="col-md-2">
            <div class="form-group field-mastercustomer-first_name required">
                <input id="mastercustomer-first_name" class="form-control" name="MasterCustomer[first_name]" maxlength="40" style="" type="text">
                    <p class="help-block help-block-error"></p><p></p>
                
            </div>            
        </div>
        <div class="col-md-2 no-padding-right label-margin required-star" style="padding-right: 0;">
            <?= Yii::t("backend", "Last Name"); ?>
        </div>
        <div class="col-md-2">
            <div class="form-group field-mastercustomer-last_name required">
                <input id="mastercustomer-last_name" class="form-control" name="MasterCustomer[last_name]" maxlength="40" style="" type="text">
                    <p class="help-block help-block-error"></p><p></p>              
            </div>            
        </div>
    </div>
    <div class="row"> 
        <div class="col-md-2 label-margin label_booking_ct">  
            <?= Yii::t("backend", "Name kana"); ?>
        </div>
        <div class="col-md-2 label-margin required-star" style="padding-right: 0;">
            <?= Yii::t("backend", "First Name Kana"); ?>
        </div>
        <div class="col-md-2">

            <div class="form-group field-mastercustomer-first_name_kana required">
                    <input id="mastercustomer-first_name_kana" class="form-control" name="MasterCustomer[first_name_kana]" maxlength="40" style="" type="text">
                    <p class="help-block help-block-error"></p><p></p>
            </div>            
        </div>
        <div class="col-md-2 n label-margin required-star" style="padding-right: 0;">
            <?= Yii::t("backend", "Last Name Kana"); ?>
        </div>
        <div class="col-md-2">
            <div class="form-group field-mastercustomer-last_name_kana required">
                    <input id="mastercustomer-last_name_kana" class="form-control" name="MasterCustomer[last_name_kana]" maxlength="40" style="" type="text">
                    <p class="help-block help-block-error"></p><p></p>
            </div>            
        </div>

    </div>
    <div class="row">
        <div class="col-md-2 label-margin label_booking_ct"> 
            <label class="control-label" for="mastercustomer-mobile"><?= Yii::t("backend", "Phone"); ?></label>
        </div>
        <div class="col-md-5 input_store_ct">
            <input id="mastercustomer-mobile" class="form-control" name="MasterCustomer[mobile]" maxlength="17" type="text">  
            <p class="help-block help-block-error"></p>
        </div> 
        <div class="col-md-2">
            <button type="button" id="btn-search" class="btn common-button-submit btn-ajax-modal" value="searchlistcustomer"><?= Yii::t('backend', 'Customer search') ?></button>            
        </div>
    </div>
</div>

<div class="list-customer">
<?php foreach ($models as $model) { ?>
<div class="row" style="margin: 10px">
    <div class="col-md-1">
      <input type="radio" name="customerId" value="<?= $model->id ?>"> 
    </div>
    <div class="col-md-11 ">

      <?= Yii::t('backend', 'Name') ?> &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp : &nbsp <?= $model->first_name . $model->last_name ?>

        <br>
        <?= Yii::t('backend', 'Phone') ?> : &nbsp <?= $model->mobile ?>
      <?= $model->ImageBlackList ?>

    </div> 
  </div>
<?php } ?> 
</div>
