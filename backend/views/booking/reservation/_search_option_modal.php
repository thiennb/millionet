<?php
    /*
     * To change this license header, choose License Headers in Project Properties.
     * To change this template file, choose Tools | Templates
     * and open the template in the editor.
     */
use common\models\ProductOption;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
?>
<style>
    .row {
        margin-left: 0px;
        margin-right: 0px;
    }
    .green-box-h4 {
    border-bottom: 2px solid #00af4f !important;
    border-left: 0px ;
    
}
</style>
<?php
      $form = ActiveForm::begin([
               'id' => 'booking-option'
           ]);
?>
<?php 
    $productSelected = Yii::$app->session->get('BookingOption'); //session save Option selected . format 'option_45_120' => '110'
    $productSelected = $productSelected != null ? $productSelected : [];
    
    if(count($options)>0){
    foreach ($options as $optionId => $productSelect ) {
   
      ?>

        
      <div class="row">
        <div class="common-box">
            <div class="box-header with-border common-box-h4 col-md-12 ">
                
              
              <h4 class="text-title"><b><?= common\models\MasterOption::findOne($optionId)->name  ?></b></h4>
              
            </div>
            <?php foreach ($productSelect as $productSelectId => $productInOption) { ?>
          <div class="row">
            <?php if($productSelectId != 0) {?>
            <div class="box-header with-border common-box-h4 green-box-h4 col-md-6">
            <h4> <?=  common\models\MstProduct::findOne($productSelectId)->name  ?></h4>
            </div>
            <?php } ?>
          
            <table class="table table-bordered col-md-12">
                <thead>
                  <tr>
                    <th><?= Yii::t('backend', 'Name')?></th>
                    <th><?= Yii::t('backend', 'Price')?></th>
                    <th><?= Yii::t('backend', 'The time required')?> </th>
                  </tr>
                </thead>
                <tbody>
                    <tr>
                      <td class="text-left" colspan="3"><?= Html::radio('option_'.$optionId.'_'.$productSelectId , true 
                            ,['value'=>  -1]) . Yii::t('backend', 'No product selected')?>
                        </td>
                        
                    </tr>
                  <?php  
                        foreach ($productInOption as  $productOptionValue){ ?>
                        <?php  if(isset($productOptionValue['product'])){
                          $name = 'option_'.$optionId.'_'.$productSelectId ;
                          $value = $productOptionValue['product']['id'];
                          ?>
                        <tr>
                          <td class="text-left"><?= Html::radio($name, 
                              in_array($name,array_keys($productSelected) ) && ($productSelected[$name]== $value)  ?true :false
                              ,['value'=>  $value]) . Html::encode($productOptionValue['product']['name'] )?></td>
                          <td><?= '¥'. Yii::$app->formatter->asInteger($productOptionValue['product']['unit_price'] ) ?></td>
                          <td><?=  $productOptionValue['product']['time_require'] .'分'?></td>
                        </tr>
                        <?php } else{
//                          echo $productOption->id;
                          }?>
                   <?php } ?>
                  
                  
                </tbody>
              </table>
              </div>
            <?php    } ?>
        </div>
        
      </div>
    <?php 
    
            } 
    
                        }else{
                          echo Html::tag('h4',Yii::t('backend', 'No option'));
                        } 
                        ?> 
<?php ActiveForm::end(); ?>