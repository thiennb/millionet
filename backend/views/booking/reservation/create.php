<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\MasterCustomer */

$this->title = Yii::t('backend', 'Reservations edit');
$this->params['breadcrumbs'][] = ['label' =>  Yii::t('backend', '予約履歴'), 'url' => ['index', 'date' => date("Y/m/d")]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row text-setting company">
    <div class="col-lg-12 col-md-12">
        <div class="box box-info box-solid add">
            <div class="box-header with-border">
                <h4 class="text-title"><b><?= $this->title ?></b></h4>
            </div>
            <div class="box-body content">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-8">
                            <?=
                            $this->render('_form', [
                                'model' => $model,
                                'modelBooking' => $modelBooking ,
                                'modelCustormerStore' => $modelCustormerStore ,
                                'couponsBookedName' => $couponsBookedName ,
                                'productBookedName' => $productBookedName ,
                                'optionsBookedName' => $optionsBookedName ,
                                'staffBookedName'   => $staffBookedName ,
                                'storeBooked'       => $storeBooked
                            ])
                            ?>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
</div>
