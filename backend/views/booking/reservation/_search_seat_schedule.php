<?php

    use yii\helpers\Html;
    use yii\grid\GridView;
    use yii\jui\DatePicker;
    use yii\widgets\Pjax;
    use yii\bootstrap\Modal;
    use yii\helpers\Url;
    use common\components\Constants;
    use common\components\Util;

/* @var $this yii\web\View */
    /* @var $searchModel common\models\BookingSearch */
    /* @var $dataProvider yii\data\ActiveDataProvider */
    \backend\assets\BookingAsset::register($this);
    $this->title = Yii::t('backend', 'Reservations edit');
    $this->params['breadcrumbs'][] = ['label' =>  Yii::t('backend', 'Booking List'), 'url' => ['index', 'date' => date("Y/m/d")]];
    $this->params['breadcrumbs'][] = $this->title;
    $this->registerCssFile('@web/css/booking.css');
    
    
?>

<style>
  .border-red{
    border-color: #DD4B39;
  } 
  .number-load{
    float: right;
    font-size: 20px;
    line-height: 0;
  }
   @media only screen and (min-width: 1040px) and (max-width: 1280px) {
    .col-md-1 {
        width: 12.333%;
    }
    .col-md-8 {
        width: 60%;
    }
    .col-md-2 {
        width: 25% !important;
    }
   }
/*  .number-people{
    display: inline;
    width: 95px;
  }*/
</style>
<?php //Pjax::begin([ 'id' => 'solutionItems','enablePushState' => false]); ?>
<div class="row">
  <div class="col-lg-12 col-md-12">
    <div class="box box-info box-solid">
      <div class="box-header with-border">
        <h4 class="text-title"><b><?= $this->title ?></b></h4>
      </div>
      <div class="box-body content">
        <div class="col-md-12">
          <div class="common-box">
            <div class="box-header with-border common-box-h4 col-md-6">
              <h4 class="text-title"><b><?= Yii::t('backend', 'Booking') ?></b></h4>
            </div>
            <div class="box-body content">
                

                <?= Html::beginForm(null, 'post', ['data-pjax' => true, 'class' => '', 'id' => "booking-schedule"]); ?>
                <div class="row col-md-12">
                <div class="col-md-02">
                    <h3 ><?= Util::dateFormatString($date) ?>  <?= Html::button(Yii::t('backend', 'Select a date'), ['class' => 'btn common-button-submit', 'id' => 'select-date']) ?></h3>
                </div>
                    <?= Html::hiddenInput('bookingId',isset(Yii::$app->request->get()['id'] )? Yii::$app->request->get()['id'] : null ,['id'=>'bookingId']) ?>
                <?=
                    DatePicker::widget([
                        'id' => 'dateBooking',
                        'name' => 'dateBooking',
                        'value' => $date,
                        'language' => 'ja',
                        'dateFormat' => 'yyyy-MM-dd',
                        "clientOptions" => [
                            "changeMonth" => true,
                            "changeYear" => true,
                            "defaultDate" => $date,
                            'onSelect' => new \yii\web\JsExpression('function(dateText, inst) { $("#booking-schedule").submit() }'),
                            

                        ]
                    ])
                ?>
                
              
                <div class="col-md-02">
               
                </div>
                  </div>
                <div class="row">
                    <div class="row col-md-12">
                        <div class="col-md-1">
                          <?= Yii::t("backend", "Start time") ?>
                        </div>
                        <div class="col-md-1">
                          <?= Html::dropDownList('start_time', isset($staffBooked['start_time'] )? $staffBooked['start_time'] :null , $hourList, ['class' => 'form-control add-time-off', 'id' => 'btn-select-starttime']) ?>
                        </div>
                    </div>
                  
                    <div class="row col-md-12">
                        <div class="col-md-1">
                           <?= Yii::t('backend', "Number people")?>
                        </div>
                        <div class="col-md-2">
                           <?= Html::textInput('number_people',$numberPeople,['style'=>'display: inline;width: 95px;','class' => 'form-control number-people', 'id' => 'txt-number-people','placeHolder' => Yii::t('backend', 'Input population')]) ?> 人

                        </div>
                        <div class="col-md-2">
                            <div class="status-people inline " style=""> 残り <span class="number-people" style="font-size: 30px">0 </span> 人未設定 </div>
                        </div>
                      
                    </div>
                    <div class="row col-md-12">
                        <div class="col-md-1">
                          <?= Yii::t('backend', "Type Seat Name")?>
                        </div>
                        <div class="col-md-2">
                          <?= Html::dropDownList('seat_type_select', isset($seatType ) ? $seatType :null, $seatTypeList, ['class' => 'form-control add-time-off', 'id' => 'btn-select-type-seat']) ?>
                
                        </div>
                        <div class="col-md-6">
                           
                            <?= Html::checkbox('no_smoking',$noSmoking,['id'=>'btn-no-smoking']) ?> <?= Yii::t('backend', 'Non smoking seat') ?>   
                            <?= Html::button(Yii::t('backend', 'Add'), ['class' => 'btn common-button-submit', 'id' => 'btn-add' , 'name' => 'btn-add']) ?>
                        </div>
                        
                      
                    </div>
                  
                  
                <div class="col-md-11">
                
               
                <br>
                  
                
                
                
                <!--hidden input-->
                <!--Check to know is btn ok-submit-->
                <?= Html::hiddenInput('isDecision',null,['id'=>'btn-is-decision']) ?>
                <!--Set end time-->
                <?= Html::hiddenInput('end_time',null,['id'=>'btn-end-time']) ?>
                
                <?= Html::dropDownList('seat_id', null, common\models\MasterSeat::listSeatByType($storeId, $seatType ,$noSmoking), ['class' => 'form-control hidden', 'id' => 'seat_id']) ?>
                <?= Html::textInput('seat_suit',$seatBooking,['id'=>'seat_suit','class'=>'hide']) ?>
               
                <?= Html::endForm() ?>
                </div>
                </div>
                 <?php if (count(common\models\MasterSeat::listSeatByType($storeId, $seatType ,$noSmoking)) > 0 ){ ?>  
                <div class="scroll-container" >
                  <div id="schedule-table"  style="min-width: 1000px "></div>
                </div> 
              <?php }else{
                echo Html::tag('h4',Yii::t('backend', Yii::t('backend',"Can't find seat with input conditions")));
              } ?>
                <br>
                <div class="row text-center">
                    
                  <?= Html::a(Yii::t('backend', 'Close'), isset(Yii::$app->request->get()['id'] )?'update?id='. Yii::$app->request->get()['id'] :'create'  , ['class' => 'btn btn-default common-button-default', 'id' => 'btn-back']) ?>
                  
                  <?= Html::button(Yii::t('backend', 'Decision'), ['class' => 'btn common-button-submit', 'id' => 'btn-ok' ,'data-pjax' => '0' ,'name' => 'btn-ok','onClick'=>'submitBookingStaff()']) ?>
                </div>  
                
              </div>
            </div>
          </div>


        </div>
      </div>
    </div>
  </div>
<?php
    Modal::begin([
        'id' => 'bookingModal',
        'size' => 'SIZE_LARGE',
        'header' => '<b>' . Yii::t('backend', 'Booking detail') . '</b>',
        'footer' => Html::button(Yii::t('backend', 'Close'), ['class' => 'btn common-button-default', 'data-dismiss' => "modal"])
        . Html::submitButton(Yii::t('backend', 'Rebooking'), ['class' => 'btn common-button-submit','id'=>'btn-choose-seat']),
        'closeButton' => [Yii::t('backend', 'Close'), ['class' => 'btn btn-primary']],
    ]);
    echo "<div id='modalContent'></div>";
    Modal::end();
?>

 
<script>

    Array.prototype.removeValue = function(name, value){
        var array = $.map(this, function(v,i){
           return v[name] == value ? null : v;
        });
        this.length = 0; //clear original array
        this.push.apply(this, array); //push all elements except the one we want to delete
     }
    data = <?= $data ?>;
    mergeData = <?= $mergeData ?>;
    
    //remove merge array 
    if($('#bookingId').val()!= null){
        mergeData.removeValue('bookingId', $('#bookingId').val());
    }
//  console.log(data);
//  console.log(mergeData);
//  var window.gon.mergeData = $("#mergeData").val();


// console.log( window.gon.mergeData);

  
    cellRenderer = function (instance, td, row, col, prop, value, cellProperties) {
 
      Handsontable.renderers.TextRenderer.apply(this, arguments);
      td.innerHTML = '';
      if (typeof (value) === "object" && value != '' ) {

//        td.setAttribute("rowSpan", value.rowSpan);
        //        td.className = 'obj-booking';
        //is busy booking
        if(value.type == 'booking'&& value.isFinish == false && (value.bookingId != $('#bookingId').val())){
          td.className = 'obj-booking-disable text-left';
          td.setAttribute("id", value.bookingId);
        }
        //is  booking finish
        if(value.type == 'booking'&& value.isFinish != false) {
          td.className = 'obj-booking-disable text-left';
          td.setAttribute("id", value.bookingId);
        } 
        //is  day off
        if(value.type == 'offday'&& value.workOffDate != null) {
          td.className = 'obj-day-off text-left';
//          td.setAttribute("id", value.bookingId);
        }
        
        

      }
      else {
        
        if(td.className !='obj-booking'){
//          td.className = value;
        }

        
      }


    };
    templateAddNew = "{custormer} \n {start} ～ {end} <div class='number-load'> {actual} 人  </div>";
    megRenderer = function (instance, td, row, col, prop, value, cellProperties) {
//      console.log(value);
      Handsontable.renderers.TextRenderer.apply(this, arguments);
       var escaped = Handsontable.helper.stringify(value);
       if (typeof (value) === "object" && value != '' && value.bookingId != '') {
         //is booking busy
          td.innerHTML = '';
          td.className = 'obj-booking-disable';
       }else{

         //choose time from add
         td.innerHTML = escaped;
         td.className = 'obj-booking';
       }

    };

    container = document.getElementById('schedule-table');
    hot = new Handsontable(container, {
      data: data,
      //        width: 500,
      //        height: 500,
      colWidths: 100,
      rowHeights: 80,
      renderAllRows: true,
      viewportColumnRenderingOffset : 30,
//      autoRowSize: true,
//             rowHeaders: true,
//          colHeaders: true,
      fixedRowsTop: 1,
      fixedColumnsLeft: 1,
      contextMenu: false,
     
      mergeCells: mergeData,
      readOnly: true,
      fillHandle: {
        autoInsertRow: false,
      },
      className: "htMiddle",
      //      cells: function (row, col, prop) {
      //        var cellProperties = {};
      //        cellProperties.readOnly = true;
      //        if (col != 0 && row != 0) {
      //          this.renderer = cellRenderer;
      //        }
      //        return cellProperties;
      //      },

      
    });
    hot.updateSettings({
      cells: function (row, col, prop) {
        var cellProperties = {};
        cellProperties.readOnly = true;
        if (col != 0 && row != 0) {
          this.renderer = cellRenderer;
        }
        return cellProperties;
      }

    });

    Date.prototype.addHours = function (h) {
      this.setHours(this.getHours() + h);
      return this;
    }
//    $('.htDimmed').trigger('click');


</script>
<?php //Pjax::end(); ?>
<script>
  
    endTime = bookingstaff= '';
    timeExcute = <?= $sumMinute ?>;
    $(document).on('click', '#select-date', function () {
        $('#dateBooking').datepicker("show");
    });
    $(document).on('change', '#btn-select-type-seat ,#btn-no-smoking', function () {
        $("#booking-schedule").submit();
    });
    

    
    //validate if staff can booking if can add to grid
    $(document).on('click', '#btn-add', function () {
        //open modal with seat auto choose
        openModalChooseSeat();

                });
    function openModalChooseSeat( isCreate ){
        if (isCreate === undefined) isCreate = true;

        var startTime   = $('#btn-select-starttime').val();
        var staffSelect = $('#btn-select-type-seat').val();
        var dateBooking = $('#dateBooking').val();
        var bookingId   = $('#bookingId').val();
        var numberPeople= $('#txt-number-people').val();
        var noSmoking   = $('#btn-no-smoking').is(":checked") ? 1 : null;

        //validate number people 
        $('#txt-number-people').removeClass('border-red');
        if(numberPeople <= 0 || numberPeople == '' || (!$.isNumeric( numberPeople) || Math.floor(numberPeople) != numberPeople ) ){
          $('#txt-number-people').addClass('border-red');
          return false;
        }
  //      var timeExcute  = 125;
        var pathControler = SITE_ROOT + '/booking/check-valid-seat-booking';
        $.ajax({
          url: pathControler,
          type: 'post',
          dataType: "json",
          data: {
              startTime   : startTime,
              dateBooking : dateBooking,
              staffSelect : staffSelect,
              timeExcute  : timeExcute ,
              bookingId   : bookingId,
              numberPeople: numberPeople,
              noSmoking   : noSmoking
          },
          success: function (data) {

            if (data.seatChoose  == null || data.seatChoose.toString() == 'false') {
  //            alert('busy');
              yii.warning(MESSAGE.SEAT_NOT_ENOUGHT,MESSAGE.WARNING);
            } else {
              var seatChoose = isCreate == true ? data.seatChoose : JSON.parse($('#seat_suit').val()) ;
              var seatFree = data.seatFree;
//              $('#seat_suit').val(JSON.stringify(data.seatChoose));
              $('#bookingModal').modal('show')
                  .find('#modalContent')
                  .load(SITE_ROOT + '/booking/choose-seat', {
                    seatChoose  : seatChoose,
                    storeId   : <?= $storeId ?>,
                    typeSelect: staffSelect,
                    numberPeople: numberPeople,
                    noSmoking : noSmoking,
                    seatFree  : seatFree
                  });

  //            addSchedule(data.seatChoose);
              $('#btn-end-time').val(endTime);
            }
          }
        });
    }
    
    //confirm and submit to save staff booking
    function submitBookingStaff(){
      $("#btn-is-decision").val(1);
      sessionStorage.setItem('bookingstaff',bookingstaff );
      $("#booking-schedule").submit();
      
    }

    function addSchedule(data) {
      var startTime   = $('#btn-select-starttime').val();
      var staffSelect = $('#btn-select-type-seat').val();
      var dateBooking = $('#dateBooking').val();
      var staffIndex  = timeIndex = 0;
    
      
//      var timeExcute = 70;
      timeExcute = Math.ceil(timeExcute / 30) * 30;
      var timeSpan = Math.ceil(timeExcute / 30);

//      console.log(mergeData);
      $.each(data, function (index, value) {
//        console.log(index );
//        console.log(value.id );
//        console.log('----' );
        var count = 1;
            ($("#seat_id").children()).each(
                function () {
                  if ($(this).attr('value') == value.id)
                    staffIndex = count;
                  else
                    count++;
                }
            );
    
        if(staffIndex == 0) return  false;
        var count = 1;
            ($("#btn-select-starttime").children()).each(
                function () {
                  if ($(this).attr('value') == startTime)
                    timeIndex = count;
                  else
                    count++;
                }
            );
        var options = document.getElementById('btn-select-starttime').options;
               endTime = options[timeIndex + timeSpan - 1 ].value;
//              console.log(bookingstaff);
//              if(typeof (bookingstaff) == 'string'){
//                bookingstaff = JSON.parse(bookingstaff);
//              }
              if( bookingstaff.dateBooking == $('#dateBooking').val() ){

                indexes = $.map(mergeData, function(obj, index) {
                  if(obj.col == bookingstaff.timeIndex && obj.row ==  bookingstaff.staffIndex) {
                      return index;
                  }
                })
                  //TODO
//                if(jQuery.isEmptyObject(indexes) != true){
//                console.log(typeof (indexes));
//                mergeData.splice(indexes, 1);
//                }
        
      }
      //add end time
      $('#btn-end-time').val(endTime);
      content = templateAddNew.replace('{custormer}', "<?= $custormerName ?>").replace('{start}', startTime).replace('{end}', endTime).replace('{actual}', value.actual_load);
      bookingstaff = JSON.stringify({
        content   : content ,
        staffIndex: staffIndex ,
        timeIndex : timeIndex ,
        timeSpan  : timeSpan ,
        startTime : startTime ,
        endTime   : endTime ,
        dateBooking :  $('#dateBooking').val()
      });
      
      
//     console.log(mergeData);
      hot.setDataAtCell(staffIndex, timeIndex, content);
//      hot.setCellMeta(rowIndex, columnIndex, 'className', cellMeta.className + ' demo');
//                        $('#schedule-table').handsontable('setDataAtCell', 2,2, 2);
   
      mergeData.push({row: staffIndex, col: timeIndex, rowspan: 1, colspan: timeSpan});
   
//    console.log(indexes);
////       console.log(mergeData.findIndex(x => x.col == 2  x.row == 5 ));
//      mergeData.splice(indexes, 1);
      hot.updateSettings({
        mergeCells: mergeData,
        cells: function (row, col, prop) {
          var cellProperties = {};
          cellProperties.readOnly = true;
          var inRow = false;
          var inCol = false;
          $.each(mergeData, function(i,obj) {
             
            if (obj.row === row && obj.col === col ) { inRow = true; inCol = true; return false;}
            
          });  
          
          if ((row == staffIndex && col == timeIndex) || (inRow == true && inCol == true )  ) {
            
            this.renderer = megRenderer;
          } else if (col != 0 && row != 0 ) {
            this.renderer = cellRenderer;
          }
          return cellProperties;
        }

      });
      });
  
      mergeData.splice(mergeData.length - data.length,data.length );
  
    }
    
    //set back season variable to create
    //************************* UPDATE FROM SEESION **********************************
    //function when update
//    bookingstaff = sessionStorage.getItem('bookingstaff');
    maxPeople = 0;
    chooseNumber= 0;
    if($('#seat_suit').val() != ''){
//      bookingstaff = JSON.parse(bookingstaff);
//      if(bookingstaff != null && bookingstaff.dateBooking == $('#dateBooking').val()){
//        setSessionBooking(bookingstaff);
//        $('#btn-end-time').val(bookingstaff.endTime);
//        bookingstaff = JSON.stringify(bookingstaff);
//      }
        var seat = jQuery.parseJSON($('#seat_suit').val());
        addSchedule(seat);
        var freeNumber = <?= $numberPeople !=null ? $numberPeople :0 ?>;
        $.each(seat,function (index,element) {
            freeNumber -= element.actual_load;        
        })
        $('.number-people').text(freeNumber);
        if(freeNumber>0){
            $('.number-people').addClass('text-danger');}
        else{
            $('.number-people').removeClass('text-danger');
        }
       
    }
    
    //script for choose seat modal
    
    $(document).on('change', '.cb-seat-choose', function () {
        $('.cb-seat-choose:checkbox').removeAttr('disabled');
        
        $('#btn-choose-seat').attr('disabled', 'disabled');
         chooseNumber = maxPeople;
        var checkBoxSelect = $(this);
        $('.cb-seat-choose:checkbox:checked').each(function (index,element) {
            $('#btn-choose-seat').removeAttr('disabled');
            if(chooseNumber >= 0 && chooseNumber - $(element).data('maxpeople') >0 ){
                var temp =  $(element).data('maxpeople');
            }else{
                var temp =  chooseNumber;
            }
            chooseNumber -= temp;
            
          $(element).attr('data-actualload',temp)  ;

        })
        
//        alert(chooseNumber + '/'+ maxPeople);
        if(chooseNumber == 0  ) {
            $('.cb-seat-choose:checkbox:not(:checked)').attr('disabled', 'disabled');
//            alert(MESSAGE.SEATS_ARE_ALREADY_ENOUGH); 
//            alert(chooseNumber + '/'+ maxPeople);
//            checkBoxSelect.prop('checked', false);
            return false;
        }
          
    
    });
    
    
    //accepp seat
    $(document).on('click', '#btn-choose-seat', function () {
        var seatChoose = [];
        $('.cb-seat-choose:checkbox:checked').each(function (index,element) {
            seatChoose.push({
              id : $(element).val(),
              seat_code : $(element).data('seatcode'),
              capacity : $(element).data('maxpeople'),
              near_max:0,
              near_level:0,
              actual_load:$(element).data('actualload')
            })
            
            $('#seat_suit').val(JSON.stringify(seatChoose));

            mergeData.removeValue('bookingId', $('#bookingId').val());
            addSchedule(jQuery.parseJSON($('#seat_suit').val()));
            //add number people
            $('.number-people').text(chooseNumber);
            if(chooseNumber>0){
              $('.number-people').addClass('text-danger');}
            else{
              $('.number-people').removeClass('text-danger');
            }
            $('#bookingModal').modal('hide');
        })
        
    });
    $(document).on('click','.obj-booking',function (){
        openModalChooseSeat(false);
    });
        
</script>