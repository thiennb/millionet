<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\MasterCustomer */

$this->title = Yii::t('backend', 'Reservations edit');
$this->params['breadcrumbs'][] = ['label' =>  Yii::t('backend', 'Booking List'), 'url' => ['index', 'date' => date("Y/m/d")]];

$this->params['breadcrumbs'][] = $this->title ;
?>
<div class="row text-setting company">
    <div class="col-lg-12 col-md-12">
        <div class="box box-info box-solid add">
            <div class="box-header with-border">
                <h4 class="text-title"><b><?= $this->title ?></b></h4>
            </div>
            <div class="box-body content">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-8">
                            <?=
                            $this->render('_form', [
                                'model' => $model,
                                'modelBooking' => $modelBooking ,
                                'modelCustormerStore' => $modelCustormerStore ,
                                'couponsBookedName' => $couponsBookedName ,
                                'productBookedName' => $productBookedName ,
                                'optionsBookedName' => $optionsBookedName ,
                                'staffBookedName'   => $staffBookedName ,
                                'storeBooked'       => $storeBooked
                            ])
                            ?>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
</div>

<script>
//    variable use for update
 <?php if($modelBooking->storeMaster->booking_resources == \common\models\MasterStore::BOOKING_RESULT_SALON ){ ?>
    var staff_id = <?= $modelBooking->staff_id?> 
    
   
    if(sessionStorage.getItem('bookingstaff') === null && staff_id != 0){
        bookingstaff = JSON.stringify({
          content   :   '<?= $jsStaffBooking['content'] ?>' ,
          staffIndex:   <?= $jsStaffBooking['staffIndex'] ?> ,
          timeIndex :   <?= $jsStaffBooking['timeIndex'] ?> ,
          timeSpan  :   <?= $jsStaffBooking['timeSpan'] ?> ,
          startTime :   '<?= $jsStaffBooking['startTime'] ?>' ,
          endTime   :   '<?= $jsStaffBooking['endTime'] ?>' ,
          dateBooking : '<?= $jsStaffBooking['dateBooking'] ?>' 
        });
        
        sessionStorage.setItem('bookingstaff',bookingstaff );
        
    }
    
//    template use for old booking
     bookingstaffOld = JSON.stringify({
          content   : '<?= $jsStaffBooking['content'] ?>' ,
          staffIndex: <?= $jsStaffBooking['staffIndex'] ?> ,
          timeIndex : <?= $jsStaffBooking['timeIndex'] ?> ,
          timeSpan  : <?= $jsStaffBooking['timeSpan'] ?> ,
          startTime : '<?= $jsStaffBooking['startTime'] ?>' ,
          endTime   : '<?= $jsStaffBooking['endTime'] ?>' ,
          dateBooking :  '<?= $jsStaffBooking['dateBooking'] ?>' 
        });
        
       
    sessionStorage.setItem('bookingstaffOld',bookingstaffOld );
  
       <?php } ?>
</script>