<?php

use yii\helpers\Html;
use Carbon\Carbon;
use common\models\BookingBusiness;

/* @var $this yii\web\View */
/* @var $model common\models\Booking */
?>
<style>

</style>

<div class="master-booking-view">
    <div class="row">
        <div class="box-header with-border common-box-h4 green-box-h4 col-md-4">
            <h4 class="text-title"><b><?php echo Yii::t('backend', '会計詳細') ?></b></h4>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12" style="font-size: 20px; color: blue">
          <?php echo $order->order_code ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3 label_preview_booking_ct">
            <b><?php echo Yii::t('backend', 'Stores') ?></b>
        </div>
        <div class="col-md-3">
            <?php echo Html::encode($model->storeMaster->name) ?>
        </div>
        <div class="col-md-3 label_preview_booking_ct">
            <b><?php echo Yii::t('backend', 'Name Customer') ?></b>
        </div>
        <div class="col-md-3">
            <?php if ($model->masterCustomer): ?>
                <?php echo Html::encode($model->masterCustomer->first_name . $model->masterCustomer->last_name) ?>
            <?php endif; ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3 label_preview_booking_ct">
            <b><?php echo Yii::t('backend', 'Responsible') ?></b>
        </div>
        <div class="col-md-3">
            <?php $management = $order->management; ?>
            <?php if ($management): $staff = $management->staff; ?>
                <?php if ($staff): ?>
                    <?php echo Html::encode($staff->name) ?>
                <?php endif; ?>
            <?php endif; ?>
        </div>
        <div class="col-md-3 label_preview_booking_ct">
            <b><?php echo Yii::t('backend', 'Order date time') ?></b>
        </div>
        <div class="col-md-3">
            <?php echo strtr($order->process_date, '-', '/') ?> <?php echo $order->process_time ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3 label_preview_booking_ct">
            <b><?php echo Yii::t('backend', 'Cashier') ?></b>
        </div>
        <div class="col-md-3">
            <?php $management = $order->rejiManagement; ?>
            <?php if ($management): $staff = $management->staff; ?>
                <?php if ($staff): ?>
                    <?php echo Html::encode($staff->name) ?>
                <?php endif; ?>
            <?php endif; ?>
        </div>
        <div class="col-md-3 label_preview_booking_ct">
            <b><?php echo Yii::t('backend', 'Update at') ?></b>
        </div>
        <div class="col-md-3">
            <?php echo Carbon::createFromTimestamp($order->updated_at)->format('Y/m/d H:i') ?>
        </div>
    </div>

    <table class="table table-striped table-bordered text-center">
        <thead>
            <tr>
                <th><?php echo Yii::t('backend', 'Category') ?></th>
                <th style=" width: 250px;"><?php echo Yii::t('backend', 'Content') ?></th>
                <th>
                    <?php echo Yii::t('backend', 'Individual settings') ?>
                </th>
                <th><?php echo Yii::t('backend', 'Price') ?></th>
                <th><?php echo Yii::t('backend', 'Quantity') ?></th>
                <th><?php echo Yii::t('backend', 'Balance Money') ?></th>
                <th>
                    <?php echo Yii::t('backend', 'People Staff') ?>
                </th>
            </tr>
        </thead>
        <tbody>
            <?php $order_detail = $order->masterOrderDetail; $pay_coupons = $order->payCoupons ?>
            <?php
                $mode_count = [];
                $mode_count['Coupon'] = count($pay_coupons);
                $mode_count['Product'] = 0 ;
                foreach ($order_detail as $id => $detail) {
                    if (substr($detail->jan_code, 0, 2) === '22') {
                        $mode_count['Coupon']++;
                    }
                    else {
                        $mode_count['Product']++;
                    }
                }
            ?>
            <?php $current_mode = '' ?>
            <?php foreach ($pay_coupons as $id => $detail) : $mode = 'Coupon'?>
                <tr>
                    <?php if ($mode !== $current_mode) : $current_mode = $mode ?>
                        <td class="text-left" rowspan="<?php echo $mode_count[$mode] ?>">
                            <?php echo Yii::t('backend', $mode) ?>
                        </td>
                    <?php endif ?>
                    <td class="text-left">
                        <?php $coupon = $detail->coupon ?>
                        <?php if ($coupon): ?>
                            <?php echo Html::encode($coupon->title) ?>
                        <?php endif; ?>
                    </td>
                    <td class="text-left">
                        <?php if ($detail->price_down_type == '1'): ?>
                            <?php echo '割引' ?>
                        <?php elseif ($detail->price_down_type == '2'): ?>
                            <?php echo '値引' ?>
                        <?php endif; ?>
                        <br/>
                    </td>
                    <td class="text-right">
                        <?php echo BookingBusiness::formatMoney($detail->price_down_money) ?>
                    </td>
                    <td class="text-right">
                        <?php echo $detail->quantity ?>
                    </td>
                    <td class="text-right">
                        <?php echo BookingBusiness::formatMoney($detail->price_down_money) ?>
                    </td>
                    <td>

                    </td>
                </tr>
            <?php endforeach ?>
            <?php foreach ($order_detail as $id => $detail): $mode = substr($detail->jan_code, 0, 2) === '22' ? 'Coupon' : 'Product'; ?>
                <tr>
                    <?php if ($mode !== $current_mode) : $current_mode = $mode ?>
                        <td class="text-left" rowspan="<?php echo $mode_count[$mode] ?>">
                            <?php echo Yii::t('backend', $mode) ?>
                        </td>
                    <?php endif ?>
                    <td class="text-left">
                        <?php echo Html::encode($detail->product_display_name) ?>
                    </td>
                    <td class="text-left">
                        <?php if ($detail->price_down_type == '1'): ?>
                            <?php echo '割引' ?>
                        <?php elseif ($detail->price_down_type == '2'): ?>
                            <?php echo '値引' ?>
                        <?php endif; ?>
                        <br/>
                        <?php if ($detail->gift_flg !== '0'): ?>
                            <?php echo 'ギフト' ?>
                        <?php endif; ?>
                    </td>
                    <td class="text-right">
                        <?php echo BookingBusiness::formatMoney($detail->price) ?>
                    </td>
                    <td class="text-right">
                        <?php echo $detail->quantity ?>
                    </td>
                    <td class="text-right">
                        <?php echo BookingBusiness::formatMoney($detail->price) ?>
                    </td>
                    <td class="text-left">
                        <?php $management = $detail->management; ?>
                        <?php if ($management): $staff = $management->staff; ?>
                            <?php if ($staff): ?>
                                <?php echo Html::encode($staff->name) ?>
                            <?php endif; ?>
                        <?php endif; ?>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="row">
        <div class="col-md-4 box1_booking_ct">
            <div class="info-box">
                <div class="row">
                    <div class="col-xs-12">
                        <?php echo Yii::t('backend', 'Subtotal') ?>
                        <span class="text-right pull-right"> <?php echo BookingBusiness::formatMoney($order->subtotal) ?> 円</span>
                        <div class="clearfix"></div>
                        <?php echo Yii::t('backend', 'Consumption tax') ?>
                        <span class="text-right pull-right"> <?php echo BookingBusiness::formatMoney($order->tax) ?> 円</span>
                        <div class="clearfix"></div>
                        <?php echo Yii::t('backend', 'Adjustment') ?>
                        <span class="text-right pull-right"> <?php echo BookingBusiness::formatMoney($order->money_adjust) ?> 円</span>
                        <div class="clearfix"></div>
                        <div style="font-weight: bold">
                            <?php echo Yii::t('backend', 'Total') ?>
                            <span class="text-right pull-right"> <?php echo BookingBusiness::formatMoney($order->total) ?> 円</span>
                        </div>
                    </div>
                </div>
                <hr>
            </div>
        </div>
        <div class="col-md-4 box1_booking_ct">
            <div class="info-box">
               <div class="row">
                   <div class="col-xs-12">
                       <?php echo Yii::t('backend', 'Money ticket') ?>
                       <span class="text-right pull-right"> <?php echo BookingBusiness::formatMoney($order->money_ticket) ?> 円</span>
                       <div class="clearfix"></div>
                       <?php echo Yii::t('backend', 'Money credit') ?>
                       <span class="text-right pull-right"> <?php echo BookingBusiness::formatMoney($order->money_credit) ?> 円</span>
                       <div class="clearfix"></div>
                       <?php echo Yii::t('backend', 'Money other') ?>
                       <span class="text-right pull-right"> <?php echo BookingBusiness::formatMoney($order->money_other) ?> 円</span>
                       <div class="clearfix"></div>
                       <div style="font-weight: bold">
                           <?php echo Yii::t('backend', 'Subtractor') ?>
                           <span class="text-right pull-right"> <?php echo BookingBusiness::formatMoney($order->money_cash) ?> 円</span>
                       </div>
                   </div>
               </div>
                <hr>
            </div>
        </div>

        <div class="col-md-4 box1_booking_ct">
            <div class="info-box">
                <div class="row">
                    <div class="col-xs-12">
                        <div style="font-weight: bold">
                            <?php echo Yii::t('backend', 'Deposit') ?>
                            <span class="text-right pull-right"> <?php echo BookingBusiness::formatMoney($order->money_receive) ?> 円</span>
                        </div>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-xs-12">
                        <div style="font-weight: bold">
                            <?php echo Yii::t('backend', 'Change_accouting') ?>
                            <span class="text-right pull-right"> <?php echo BookingBusiness::formatMoney($order->money_refund) ?> 円</span>
                        </div>
                    </div>
                </div>
                <hr>
            </div>
        </div>
    </div>
</div>
<script>
    (function ($) {
        $('#btn-staff-response').show();
    })(jQuery);
</script>
