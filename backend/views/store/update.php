<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\MasterStore */

$this->title = Yii::t('app', Yii::t('backend', 'Create Store'));
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Store List'), 'url' => ['index']];
//$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('backend', 'Create Store');
?>
<div class="row text-setting company">
    <div class="col-lg-12 col-md-12">
        <div class="box box-info box-solid add">
            <div class="box-header with-border">
                <h4 class="text-title"><b><?= $this->title ?></b></h4>
            </div>
            <div class="box-body content">
                <div class="col-md-12">
                    <div class="row">
                        <?=
                        $this->render('_form', [
                            'model' => $model,
                            'check_list' => $check_list
                        ])
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
