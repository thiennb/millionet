<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;
use common\components\Util;
use yii\bootstrap\Modal;
use common\widgets\preview\PreviewStore;
use common\components\Constants;
use common\models\MasterStore;
use common\widgets\preview\PreviewStoreGift;

$this->registerJsFile('@web/js/confirm_booking.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile('@web/js/get_location.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
?>

<!-- ================================ -->
<!-- Start Form Confirm -->
<!-- ================================ -->
<!-- Datpdt 21/09/2016 Start-->  
<!-- ============================== -->
<!-- ======== Create Store ===      -->
<!-- ============================== -->
<?php
$form = ActiveForm::begin([
            'options' => [
                'enctype' => 'multipart/form-data',
                'id' => strtolower($model->formName()) . '-id',
            ],
            // important
            'validationUrl' => 'validate',
            'enableClientValidation' => false,
            'enableAjaxValidation' => true,
            'validateOnChange' => false,
            'validateOnBlur' => false,
            'successCssClass' => '',
        ]);
?>
<div class="wizard">
    <div class="wizard-inner" style="display: none">
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active">
                <a href="#step1" data-toggle="tab" aria-controls="step1" role="tab" title="Step 1">

                </a>
            </li>
            <li role="presentation" class="disabled">
                <a href="#step2" data-toggle="tab" aria-controls="step2" role="tab" title="Step 2">

                </a>
            </li>
        </ul>
    </div>    
    <div class="tab-content">
        <!-- Table Confirm Booking Start  -->
        <form id='_form_confirm_booking_et'>
            <?=
            $this->render('_form_confirm_booking', ['check_list' => $check_list])
            ?>
        </form>
        <input id="value_textarea_qs" type="hidden" name="Question_Answer" />

        <div class="col-md-8 tab-pane active" role="tabpanel" id="step1">
            <div class="box-header with-border common-box-h4 col-md-6 ">
                <h4 class="text-title"><b><?= Yii::t('backend', 'Create Store') ?></b></h4>
            </div>
            <div class="col-md-12">
                <div class="store-form">
                    <!-- Name Store -->
                    <div class="col-md-12"> 
                        <div class="col-md-3 label-margin required-star label_store_ct">  
                            <?=
                            Yii::t("backend", "Name Store");
                            ?>
                        </div> 
                        <div class="col-md-4 input_store_ct">
                            <?= $form->field($model, 'name')->textInput(['maxlength' => true])->label(false) ?>
                        </div>
                        <div class="col-md-2 pos_use_store_ct">
                            <?= $form->field($model, 'pos_use')->label(false)->dropDownList(Constants::POS) ?>
                        </div>
                        <div class="col-md-2 label-margin">
                            <?=
                            $form->field($model, 'temporary_stop', [
                                'options' => ['tag' => false, 'class' => 'checkbox icheck']])->checkbox()
                            ?>
                        </div>
                    </div>
                    <!-- End Name Store Date -->
                    <!-- Name Store -->
                    <div class="col-md-12"> 
                        <div class="col-md-3 label-margin label_store_ct">  
                            <?=
                            Yii::t("backend", "Name Kana");
                            ?>
                        </div> 
                        <div class="col-md-4 input_store_ct">
                            <?= $form->field($model, 'name_kana')->textInput(['maxlength' => true])->label(false) ?>
                        </div>
                    </div>
                    <!-- End Name Store Date -->
                    <!-- Post Code -->
                    <div class="col-md-12"> 
                        <div class="col-md-3 label-margin label_store_ct">  
                            <?=
                            Yii::t("backend", "PostCode");
                            ?>
                        </div> 
                        <div class="col-md-2 post_code_store_ct">
                            <?=
                            $form->field($model, 'post_code')->label(false)->textInput(['maxlength' => true])->widget(\yii\widgets\MaskedInput::className(), [
                                'mask' => '999-9999',
                                'clientOptions' => [

                                    'removeMaskOnSubmit' => true
                                ]
                            ])
                            ?>
                        </div>
                        <div class="col-md-offset-0">
                            <?=
                            Html::button(Yii::t('backend', 'Search Adress'), ['class' => 'btn', 'style' => 'background: #FF69B4;color: #fff;font-weight: bold;border-radius: 5px', 'id' => 'search-code'])
                            ?>
                        </div>
                    </div>
                    <!-- End Post Code -->
                    <!-- Address -->
                    <div class="col-md-12"> 
                        <div class="col-md-3 label-margin label_store_ct">  
                            <?=
                            Yii::t("backend", "Address Store");
                            ?>
                        </div> 
                        <div class="col-md-4 input_store_ct">
                            <?= $form->field($model, 'address')->textInput(['maxlength' => true])->label(false) ?>
                        </div>
                        <!-- Set latitude and longitude by Address -->
                        <div class="col-md-1">
                            <?= $form->field($model, 'latitude')->hiddenInput(['class' => 'hidden_image'])->label(FALSE) ?>
                        </div>
                        <div class="col-md-1">
                            <?= $form->field($model, 'longitude')->hiddenInput(['class' => 'hidden_image'])->label(FALSE) ?>
                        </div>
                        <!-- End Set latitude and longitude by Address -->
                    </div>
                    <!-- End Address -->
                    <!-- Directions -->
                    <div class="col-md-12"> 
                        <div class="col-md-3 label-margin label_store_ct">  
                            <?= Yii::t("backend", "Directions Address"); ?>
                        </div> 
                        <div class="col-md-4 input_store_ct">
                            <?= $form->field($model, 'directions')->textarea(['rows' => 5, 'maxlength' => true])->label(false) ?>
                        </div>
                    </div>            
                    <!-- End Directions -->
                    <!-- Home Page -->
                    <div class="col-md-12"> 
                        <div class="col-md-3 label-margin label_store_ct">  
                            <?=
                            Yii::t("backend", "Website");
                            ?>
                        </div> 
                        <div class="col-md-4 input_store_ct">
                            <?= $form->field($model, 'website')->textInput(['maxlength' => true])->label(false) ?>
                        </div>
                    </div>
                    <!-- End Home Page -->
                    <!-- Time_open and Time_close -->
                    <div class="col-md-12"> 
                        <div class="col-md-3 label-margin label_store_ct">  
                            <?= Yii::t("backend", "Time From"); ?>
                        </div> 
                        <div class="col-md-1" style="width: 10em;">
                            <?= $form->field($model, 'time_open')->label(false)->dropDownList(Constants::LIST_OPTION_SELECT_TIME) ?>
                        </div> 
                        <div class="col-md-1 label-margin label-center" style="width: 3.5em;">～</div> 
                        <div class="col-md-1" style="width: 10em;"> 
                            <?= $form->field($model, 'time_close')->label(false)->dropDownList(Constants::LIST_OPTION_SELECT_TIME) ?>
                        </div> 
                    </div>
                    <!-- End Time_open and Time_close -->
                    <!-- Regular Holiday -->
                    <div class="col-md-12"> 
                        <div class="col-md-3 label-margin label_store_ct">  
                            <?= Yii::t("backend", "Regular Holiday"); ?>
                        </div> 
                        <div class="col-md-4 input_store_ct">
                            <?= $form->field($model, 'regular_holiday')->textInput(['maxlength' => true])->label(false) ?>
                        </div>
                    </div>
                    <!-- End Regular Holiday-->
                    <!-- Tel -->
                    <div class="col-md-12"> 
                        <div class="col-md-3 label-margin label_store_ct">  
                            <?= Yii::t("backend", "Phone"); ?>
                        </div> 
                        <div class="col-md-4 input_store_ct">
                            <?=
                            $form->field($model, 'tel')->label(false)->widget(\yii\widgets\MaskedInput::className(), [
                                'mask' => '9',
                                'clientOptions' => ['repeat' => 17, 'greedy' => false]
                            ])->textInput(['maxlength' => true])
                            ?>
                        </div>
                    </div>
                    <!-- End Tel -->
                    <!-- Introduction Store -->
                    <div class="col-md-12"> 
                        <div class="col-md-3 label-margin label_store_ct">  
                            <?= Yii::t("backend", "Introduction Store"); ?>
                        </div> 
                        <div class="col-md-4 input_store_ct">
                            <?= $form->field($model, 'introduce')->textarea(['rows' => 6, 'maxlength' => true])->label(false) ?>
                        </div>
                    </div>
                    <!-- -->
                    <div class="col-md-12"> 
                        <div class="col-md-3 label-margin label_show_flg_store_ct">  
                            <?= Yii::t("backend", "Reservation site member app display"); ?>
                        </div> 
                        <div class="col-md-4 label-margin radio_show_flg_store_ct">
                            <?= $form->field($model, 'show_flg')->radioList(Constants::LIST_SHOW)->label(false); ?>
                        </div>
                    </div>
                    <!-- -->
                    <!-- -->
                    <div class="col-md-12"> 
                        <div class="col-md-3 label-margin label_store_ct">  
                            <?= Yii::t("backend", "Rounded down"); ?>
                        </div> 
                        <div class="col-md-2">
                            <?= $form->field($model, 'round_down_flg')->label(false)->dropDownList(Constants::ROUND_DOW) ?>
                        </div>
                        <div class="col-md-1 label-margin" style="margin-left: 1em;width: 7em;">  
                            <?= Yii::t("backend", "Rounding"); ?>
                        </div> 
                        <div class="col-md-2">
                            <?= $form->field($model, 'round_process_flg')->label(false)->dropDownList(Constants::ROUND_PROCESS) ?>
                        </div>
                        
                    </div>
                    <!-- -->

                    <!-- End Introduction Store -->
                    <!-- Image 1,Image 2,Image 3,Image 4,Image 5 -->
                    <div class="col-md-12"> 
                        <div class="col-md-3 label-margin label_store_ct">  
                            <?= Yii::t("backend", "Image1"); ?>
                        </div> 
                        <div class="col-md-4 image_store_ct">
                            <?=
                            $form->field($model, 'tmp_image1')->label(false)->widget(
                                    FileInput::classname(), [
                                'options' => ['accept' => 'image/*'],
                                'pluginOptions' => [
                                    'allowedFileExtensions' => ['jpg', 'gif', 'png', 'jpeg'],
                                    'maxFileSize' => 2048,
                                    'initialPreview' => [
                                        Html::img(Util::getUrlImage($model->image1))
                                    ],
                                    'overwriteInitial' => true,
                                    'showUpload' => false,
                                    'showCaption' => false,
                                    'browseLabel' => Yii::t('backend', 'Select an image'),
                                ],
                                'pluginEvents' => [
                                    "fileclear" => 'function() { $(this).parent().parent().parent().parent().find(".hidden_image").val("") }',
                                ],
                            ]);
                            ?>
                            <?= $form->field($model, 'hidden_image1')->hiddenInput(['class' => 'hidden_image'])->label(FALSE) ?>
                        </div>
                    </div>
                    <div class="col-md-12"> 
                        <div class="col-md-3 label-margin label_store_ct">  
                            <?= Yii::t("backend", "Image2"); ?>
                        </div> 
                        <div class="col-md-4 image_store_ct">
                            <?=
                            $form->field($model, 'tmp_image2')->label(false)->widget(
                                    FileInput::classname(), [
                                'options' => ['accept' => 'image/*'],
                                'pluginOptions' => [
                                    'allowedFileExtensions' => ['jpg', 'gif', 'png', 'jpeg'],
                                    'maxFileSize' => 2048,
                                    'initialPreview' => [
                                        Html::img(Util::getUrlImage($model->image2))
                                    ],
                                    'overwriteInitial' => true,
                                    'showUpload' => false,
                                    'showCaption' => false,
                                    'browseLabel' => Yii::t('backend', 'Select an image'),
                                ],
                                'pluginEvents' => [
                                    "fileclear" => 'function() { $(this).parent().parent().parent().parent().find(".hidden_image").val("") }',
                                ],
                            ]);
                            ?>
                            <?= $form->field($model, 'hidden_image2')->hiddenInput(['class' => 'hidden_image'])->label(FALSE) ?>
                        </div>
                    </div>
                    <div class="col-md-12"> 
                        <div class="col-md-3 label-margin label_store_ct">  
                            <?= Yii::t("backend", "Image3"); ?>
                        </div> 
                        <div class="col-md-4 image_store_ct">
                            <?=
                            $form->field($model, 'tmp_image3')->label(false)->widget(
                                    FileInput::classname(), [
                                'options' => ['accept' => 'image/*'],
                                'pluginOptions' => [
                                    'allowedFileExtensions' => ['jpg', 'gif', 'png', 'jpeg'],
                                    'maxFileSize' => 2048,
                                    'initialPreview' => [
                                        Html::img(Util::getUrlImage($model->image3))
                                    ],
                                    'overwriteInitial' => true,
                                    'showUpload' => false,
                                    'showCaption' => false,
                                    'browseLabel' => Yii::t('backend', 'Select an image'),
                                ],
                                'pluginEvents' => [
                                    "fileclear" => 'function() { $(this).parent().parent().parent().parent().find(".hidden_image").val("") }',
                                ],
                            ]);
                            ?>
                            <?= $form->field($model, 'hidden_image3')->hiddenInput(['class' => 'hidden_image'])->label(FALSE) ?>
                        </div>
                    </div>
                    <div class="col-md-12"> 
                        <div class="col-md-3 label-margin label_store_ct">  
                            <?= Yii::t("backend", "Image4"); ?>
                        </div> 
                        <div class="col-md-4 image_store_ct">
                            <?=
                            $form->field($model, 'tmp_image4')->label(false)->widget(
                                    FileInput::classname(), [
                                'options' => ['accept' => 'image/*'],
                                'pluginOptions' => [
                                    'allowedFileExtensions' => ['jpg', 'gif', 'png', 'jpeg'],
                                    'maxFileSize' => 2048,
                                    'initialPreview' => [
                                        Html::img(Util::getUrlImage($model->image4))
                                    ],
                                    'overwriteInitial' => true,
                                    'showUpload' => false,
                                    'showCaption' => false,
                                    'browseLabel' => Yii::t('backend', 'Select an image'),
                                ],
                                'pluginEvents' => [
                                    "fileclear" => 'function() { $(this).parent().parent().parent().parent().find(".hidden_image").val("") }',
                                ],
                            ]);
                            ?>
                            <?= $form->field($model, 'hidden_image4')->hiddenInput(['class' => 'hidden_image'])->label(FALSE) ?>
                        </div> 
                    </div>
                    <div class="col-md-12"> 
                        <div class="col-md-3 label-margin label_store_ct">  
                            <?= Yii::t("backend", "Image5"); ?>
                        </div> 
                        <div class="col-md-4 image_store_ct">
                            <?=
                            $form->field($model, 'tmp_image5')->label(false)->widget(
                                    FileInput::classname(), [
                                'options' => ['accept' => 'image/*'],
                                'pluginOptions' => [
                                    'allowedFileExtensions' => ['jpg', 'gif', 'png', 'jpeg'],
                                    'maxFileSize' => 2048,
                                    'initialPreview' => [
                                        Html::img(Util::getUrlImage($model->image5))
                                    ],
                                    'overwriteInitial' => true,
                                    'showUpload' => false,
                                    'showCaption' => false,
                                    'browseLabel' => Yii::t('backend', 'Select an image'),
                                ],
                                'pluginEvents' => [
                                    "fileclear" => 'function() { $(this).parent().parent().parent().parent().find(".hidden_image").val("") }',
                                ],
                            ]);
                            ?>
                            <?= $form->field($model, 'hidden_image5')->hiddenInput(['class' => 'hidden_image'])->label(FALSE) ?>
                        </div> 
                    </div>
                    <!-- End Image 1,Image 2,Image 3,Image 4,Image 5 -->
                    <div class="col-md-12"> 
                        <div class="col-md-3 label-margin label_store_ct">  
                            <?= Yii::t("backend", "App Logo Img"); ?>
                        </div> 
                        <div class="col-md-4 image_store_ct">
                            <?=
                            $form->field($model, 'tmp_app_logo_img')->label(false)->widget(
                                    FileInput::classname(), [
                                'options' => ['accept' => 'image/*'],
                                'pluginOptions' => [
                                    'allowedFileExtensions' => ['jpg', 'gif', 'png', 'jpeg'],
                                    'maxFileSize' => 2048,
                                    'initialPreview' => [
                                        Html::img(Util::getUrlImage($model->app_logo_img))
                                    ],
                                    'overwriteInitial' => true,
                                    'showUpload' => false,
                                    'showCaption' => false,
                                    'browseLabel' => Yii::t('backend', 'Select an image'),
                                ],
                                'pluginEvents' => [
                                    "fileclear" => 'function() { $(this).parent().parent().parent().parent().find(".hidden_image").val("") }',
                                ],
                            ]);
                            ?>
                            <?= $form->field($model, 'hidden_app_logo_img')->hiddenInput(['class' => 'hidden_image'])->label(FALSE) ?>
                        </div> 
                    </div>
                    <div class="col-md-12"> 
                        <div class="col-md-3 label-margin label_store_ct">  
                            <?= Yii::t("backend", "Receipt Logo Img"); ?>
                        </div> 
                        <div class="col-md-4 image_store_ct">
                            <?=
                            $form->field($model, 'tmp_receipt_logo_img')->label(false)->widget(
                                    FileInput::classname(), [
                                'options' => ['accept' => 'image/*'],
                                'pluginOptions' => [
                                    'allowedFileExtensions' => ['jpg', 'gif', 'png', 'jpeg'],
                                    'maxFileSize' => 2048,
                                    'initialPreview' => [
                                        Html::img(Util::getUrlImage($model->receipt_logo_img))
                                    ],
                                    'overwriteInitial' => true,
                                    'showUpload' => false,
                                    'showCaption' => false,
                                    'browseLabel' => Yii::t('backend', 'Select an image'),
                                ],
                                'pluginEvents' => [
                                    "fileclear" => 'function() { $(this).parent().parent().parent().parent().find(".hidden_image").val("") }',
                                ],
                            ]);
                            ?>
                            <?= $form->field($model, 'hidden_receipt_logo_img')->hiddenInput(['class' => 'hidden_image'])->label(FALSE) ?>
                        </div> 
                    </div>
                </div>
            </div>
            <!-- ============================== -->
            <!-- ======== End Create Store ===  -->
            <!-- ============================== -->
            <!-- ============================= -->
            <!-- === Setting Booking Store ==== -->
            <!-- ============================== -->
            <div class="box-header with-border common-box-h4 col-md-6 label_store_ct">
                <h4 class="text-title"><b><?= Yii::t('backend', 'Gold certificate setting') ?></b></h4>
            </div>
            <div class="col-md-12">
                <div class="store-form">
                    <div class="col-md-12"> 
                        <div class="col-md-3 label-margin">  
                            <?= Yii::t("backend", "Gold certificate issuance"); ?>
                        </div>
                        <div class="col-md-1 label-margin" style="width: 7em;">
                            <?= $form->field($model, 'gold_ticket_supply_flg')->radioList(Constants::GOLD_TICKET_SUPPLY_FLG)->label(false); ?>
                        </div>
                        <div class="col-md-2" style="margin-top: 2em;">  
                            <?= $form->field($model, 'gold_ticket_supply_method')->label(false)->dropDownList(Constants::GOLD_TICKET_SUPPLY_METHOD) ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="store-form">
                    <div class="col-md-12"> 
                        <div class="col-md-3 label-margin label_store_ct">  
                            <?= Yii::t("backend", "Issuance conditions"); ?>
                        </div>
                        <div class="col-md-2">
                            <?=
                            $form->field($model, 'supply_condition_point')->widget(\yii\widgets\MaskedInput::className(), [
                                'clientOptions' => [
                                    'groupSeparator' => ',',
                                    'alias' => 'integer',
                                    'autoGroup' => true,
                                    'removeMaskOnSubmit' => true,
                                    'allowMinus' => false,
                                ]
                            ])->textInput(['maxlength' => 9])->label(false)
                            ?>
                        </div>
                        <div class="col-md-1 label-margin equals_label_store_ct">
                            <?= Yii::t("backend", "Equals"); ?>
                        </div>
                        <div class="col-md-2">
                            <?=
                            $form->field($model, 'supply_condition_yen')->widget(\yii\widgets\MaskedInput::className(), [
                                'clientOptions' => [
                                    'groupSeparator' => ',',
                                    'alias' => 'integer',
                                    'autoGroup' => true,
                                    'removeMaskOnSubmit' => true,
                                    'allowMinus' => false,
                                ]
                            ])->textInput(['maxlength' => 13])->label(false)
                            ?>
                        </div>
                        <div class="col-md-2 label-margin label_store_ct">
                            <?= Yii::t("backend", "Issue a circle of gold note"); ?>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-3 label-margin label_store_ct">  
                            <?= Yii::t("backend", "Date Expiration Ticket"); ?>
                        </div>
                        <div class="col-md-1 label-margin" style="width: 7.5em;">
                            <?= $form->field($model, 'expiration_date_flg')->radioList(Constants::EXPIRATION_DATE_FLG)->label(false); ?>
                        </div>
                        <div class="col-md-2 label-margin label_store_ct" style="margin-top: 3em;width: 10.2em;">  
                            <?= Yii::t("backend", "From the date of issue"); ?>
                        </div>
                        <div class="col-md-2" style="margin-top: 2em;">  
                            <?=
                            $form->field($model, 'supply_year')->widget(\yii\widgets\MaskedInput::className(), [
                                'clientOptions' => [
                                    'alias' => 'integer',
                                    'autoGroup' => true,
                                    'removeMaskOnSubmit' => true,
                                    'allowMinus' => false,
                                ]
                            ])->textInput(['maxlength' => 2])->label(false)
                            ?>
                        </div>
                        <div class="col-md-1 label-margin label_store_ct" style="margin-top: 3em; width: 20%;">  
                            <?= Yii::t("backend", "Year"); ?>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-3 label-margin label_store_ct">  
                            <?= Yii::t("backend", "Gold certificate image"); ?>
                        </div> 
                        <div class="col-md-4 image_store_ct">
                            <?=
                            $form->field($model, 'tmp_gold_ticket_image')->label(false)->widget(
                                    FileInput::classname(), [
                                'options' => ['accept' => 'image/*'],
                                'pluginOptions' => [
                                    'allowedFileExtensions' => ['jpg', 'gif', 'png', 'jpeg'],
                                    'maxFileSize' => 2048,
                                    'initialPreview' => [
                                        Html::img(Util::getUrlImage($model->gold_ticket_image))
                                    ],
                                    'overwriteInitial' => true,
                                    'showUpload' => false,
                                    'showCaption' => false,
                                    'browseLabel' => Yii::t('backend', 'Select an image'),
                                ],
                                'pluginEvents' => [
                                    "fileclear" => 'function() { $(this).parent().parent().parent().parent().find(".hidden_image").val("") }',
                                ],
                            ]);
                            ?>
                            <?= $form->field($model, 'hidden_gold_ticket_image')->hiddenInput(['class' => 'hidden_image'])->label(FALSE) ?>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-3 label-margin label_store_ct">  
                            <?= Yii::t("backend", "Description 1"); ?>
                        </div>
                        <div class="col-md-4 input_store_ct">
                            <?=
                            $form->field($model, 'gold_ticket_description1')->textInput(['maxlength' => true])->label(false)
                            ?>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-3 label-margin label_store_ct">  
                            <?= Yii::t("backend", "Description 2"); ?>
                        </div>
                        <div class="col-md-4 input_store_ct">
                            <?=
                            $form->field($model, 'gold_ticket_description2')->textInput(['maxlength' => true])->label(false)
                            ?>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-3 label-margin" style="margin-left: 1em;">  
                            <?=
                            Html::button(Yii::t('backend', 'Preview Coupon'), [
                                'class' => 'btn common-button-submit',
                                'id' => 'btn-confirm-preview-md',
                                'data' => [
                                    'toggle' => 'modal',
                                    'target' => '#previewModalConfirm',
                                ],
                            ])
                            ?>
                        </div>
                    </div>
                </div>
                <div class="box-header with-border common-box-h4 col-md-6">
                    <h4 class="text-title"><b><?= Yii::t('backend', 'Setting Booking Store') ?></b></h4>
                </div>
                <div class="col-md-12">
                    <div class="store-form">
                        <!-- Reservations Possible Time-->
                        <div class="col-md-12"> 
                            <div class="col-md-3 label-margin label_store_ct">  
                                <?= Yii::t("backend", "Reservations Possible Time"); ?>
                            </div>
                            <div class="col-md-3">
                                <?=
                                $form->field($model, 'reservations_possible_time')->label(false)->widget(\yii\widgets\MaskedInput::className(), [
                                    'clientOptions' => [
                                        'alias' => 'integer',
                                        'autoGroup' => true,
                                        'removeMaskOnSubmit' => true,
                                        'allowMinus' => false,
                                    ]
                                ])->textInput(['maxlength' => 3])
                                ?>
                            </div>
                            <div class="col-md-3 label-margin">
                                <?= Yii::t("backend", "Can book before ___ hours"); ?>
                            </div>
                        </div>
                        <!-- End Reservations Possible Time -->
                        <!-- Cancel Possible Time-->
                        <div class="col-md-12"> 
                            <div class="col-md-3 label-margin label_store_ct">  
                                <?= Yii::t("backend", "Cancel Possible Time"); ?>
                            </div>
                            <div class="col-md-3">
                                <?=
                                $form->field($model, 'cancel_possible_time')->label(false)->widget(\yii\widgets\MaskedInput::className(), [
                                    'clientOptions' => [
                                        'alias' => 'integer',
                                        'autoGroup' => true,
                                        'removeMaskOnSubmit' => true,
                                        'allowMinus' => false,
                                    ]
                                ])->textInput(['maxlength' => 3])
                                ?>
                            </div>
                            <div class="col-md-3 label-margin">
                                <?= Yii::t("backend", "It can be canceled up to the time"); ?>
                            </div>
                        </div>
                        <!-- End Cancel Possible Time -->
                        <!-- Booking Resources -->
                        <div class="col-md-12"> 
                            <div class="col-md-3 label-margin label_store_ct">  
                                <?= Yii::t("backend", "Booking Resources"); ?>
                            </div>
                            <div class="col-md-8  booking_resources_store_ct">
                                <?php // Util::dd($model);     ?>
                                <?= $form->field($model, 'booking_resources')->radioList(Constants::LIST_SHOW_BOOKING)->label(false); ?>

                            </div>
                        </div>
                        <!-- End Booking Resources -->  
                        <!-- Confirm Booking -->
                        <div class="col-md-12"> 
                            <div class="col-md-3">  
                                <?= Html::button(Yii::t('backend', 'Confirm Booking'), ['class' => 'btn btn-default common-button-default next-step', 'name' => 'bt_confirm_question', 'value' => 'bt_confirm_question', 'id' => 'bt_confirm_question']) ?>
                            </div>
                            <div class="col-md-3 font_label" style="line-height: 3;" id="check_have_question">

                            </div>
                        </div>
                        <!-- End Confirm Booking -->
                    </div>
                </div>
                <!-- ============================== -->
                <!-- ==== End Setting Booking Store -->
                <!-- ============================== -->
                <!-- ============================== -->
                <!-- ==== Set Store SNS Link        -->
                <!-- ============================== -->
                <div class="box-header with-border common-box-h4 col-md-6 ">
                    <h4 class="text-title"><b><?= Yii::t('backend', 'Set Store SNS Link') ?></b></h4>
                </div>
                <div class="col-md-12">
                    <!-- Link Title 1 and Url Link 1 , Link 1 icon-->
                    <div class="store-form">
                        <!-- Link Title 1-->
                        <div class="col-md-12"> 
                            <div class="col-md-3 label-margin label_store_ct">  
                                <?= Yii::t("backend", "Link Title 1"); ?>
                            </div>
                            <div class="col-md-4 input_store_ct">
                                <?= $form->field($model, 'link_title_1')->textInput(['maxlength' => true])->label(false) ?>
                            </div>
                        </div>
                        <!-- End Link Title 1 -->
                        <!-- Url Link 1 -->
                        <div class="col-md-12"> 
                            <div class="col-md-3 label-margin label_store_ct">  
                                <?= Yii::t("backend", "Link Url 1"); ?>
                            </div>
                            <div class="col-md-4 input_store_ct">
                                <?= $form->field($model, 'link_url_1')->textInput(['maxlength' => true])->label(false) ?>
                            </div>
                        </div>
                        <!-- End Url Link 1 -->
                        <!-- Link 1 icon -->
                        <div class="col-md-12"> 
                            <div class="col-md-3 label-margin label_store_ct">  
                                <?= Yii::t("backend", "Link 1 icon"); ?>
                            </div>
                            <div class="col-md-4 image_store_ct">
                                <?=
                                $form->field($model, 'tmp_icon_1')->label(false)->widget(
                                        FileInput::classname(), [
                                    'options' => ['accept' => 'image/*'],
                                    'pluginOptions' => [
                                        'allowedFileExtensions' => ['jpg', 'gif', 'png', 'jpeg'],
                                        'maxFileSize' => 2048,
                                        'initialPreview' => [
                                            Html::img(Util::getUrlImage($model->link_icon_1))
                                        ],
                                        'overwriteInitial' => true,
                                        'showUpload' => false,
                                        'showCaption' => false,
                                        'browseLabel' => Yii::t('backend', 'Select an image'),
                                    ],
                                    'pluginEvents' => [
                                        "fileclear" => 'function() { $(this).parent().parent().parent().parent().find(".hidden_image").val("") }',
                                    ],
                                ]);
                                ?>
                                <?= $form->field($model, 'hidden_icon_1')->hiddenInput(['class' => 'hidden_image'])->label(FALSE) ?>
                            </div>
                        </div>
                        <!-- End Link 1 icon -->
                    </div>
                    <!-- End Link Title 1 and Url Link 1 , Link 1 icon-->
                    <!-- Link Title 2 and Url Link 2 , Link 2 icon-->
                    <div class="store-form">
                        <!-- Link Title 2-->
                        <div class="col-md-12"> 
                            <div class="col-md-3 label-margin label_store_ct">  
                                <?= Yii::t("backend", "Link Title 2"); ?>
                            </div>
                            <div class="col-md-4 input_store_ct">
                                <?= $form->field($model, 'link_title_2')->textInput(['maxlength' => true])->label(false) ?>
                            </div>
                        </div>
                        <!-- End Link Title 2 -->
                        <!-- Url Link 2 -->
                        <div class="col-md-12"> 
                            <div class="col-md-3 label-margin label_store_ct">  
                                <?= Yii::t("backend", "Link Url 2"); ?>
                            </div>
                            <div class="col-md-4 input_store_ct">
                                <?= $form->field($model, 'link_url_2')->textInput(['maxlength' => true])->label(false) ?>
                            </div>
                        </div>
                        <!-- End Url Link 2 -->
                        <!-- Link 2 icon -->
                        <div class="col-md-12"> 
                            <div class="col-md-3 label-margin label_store_ct">  
                                <?= Yii::t("backend", "Link 2 icon"); ?>
                            </div>
                            <div class="col-md-4 image_store_ct">
                                <?=
                                $form->field($model, 'tmp_icon_2')->label(false)->widget(
                                        FileInput::classname(), [
                                    'options' => ['accept' => 'image/*'],
                                    'pluginOptions' => [
                                        'allowedFileExtensions' => ['jpg', 'gif', 'png', 'jpeg'],
                                        'maxFileSize' => 2048,
                                        'initialPreview' => [
                                            Html::img(Util::getUrlImage($model->link_icon_2))
                                        ],
                                        'overwriteInitial' => true,
                                        'showUpload' => false,
                                        'showCaption' => false,
                                        'browseLabel' => Yii::t('backend', 'Select an image'),
                                    ],
                                    'pluginEvents' => [
                                        "fileclear" => 'function() { $(this).parent().parent().parent().parent().find(".hidden_image").val("") }',
                                    ],
                                ]);
                                ?>
                                <?= $form->field($model, 'hidden_icon_2')->hiddenInput(['class' => 'hidden_image'])->label(FALSE) ?>
                            </div>
                        </div>
                        <!-- End Link 2 icon -->
                    </div>
                    <!-- End Link Title 2 and Url Link 2 , Link 2 icon-->
                    <!-- Link Title 3 and Url Link 3 , Link 3 icon-->
                    <div class="store-form">
                        <!-- Link Title 3-->
                        <div class="col-md-12"> 
                            <div class="col-md-3 label-margin label_store_ct">  
                                <?= Yii::t("backend", "Link Title 3"); ?>
                            </div>
                            <div class="col-md-4 input_store_ct">
                                <?= $form->field($model, 'link_title_3')->textInput(['maxlength' => true])->label(false) ?>
                            </div>
                        </div>
                        <!-- End Link Title 3 -->
                        <!-- Url Link 3 -->
                        <div class="col-md-12"> 
                            <div class="col-md-3 label-margin label_store_ct">  
                                <?= Yii::t("backend", "Link Url 3"); ?>
                            </div>
                            <div class="col-md-4 input_store_ct">
                                <?= $form->field($model, 'link_url_3')->textInput(['maxlength' => true])->label(false) ?>
                            </div>
                        </div>
                        <!-- End Url Link 3 -->
                        <!-- Link 3 icon -->
                        <div class="col-md-12"> 
                            <div class="col-md-3 label-margin label_store_ct">  
                                <?= Yii::t("backend", "Link 3 icon"); ?>
                            </div>
                            <div class="col-md-4 image_store_ct">
                                <?=
                                $form->field($model, 'tmp_icon_3')->label(false)->widget(
                                        FileInput::classname(), [
                                    'options' => ['accept' => 'image/*'],
                                    'pluginOptions' => [
                                        'allowedFileExtensions' => ['jpg', 'gif', 'png', 'jpeg'],
                                        'maxFileSize' => 2048,
                                        'initialPreview' => [
                                            Html::img(Util::getUrlImage($model->link_icon_3))
                                        ],
                                        'overwriteInitial' => true,
                                        'showUpload' => false,
                                        'showCaption' => false,
                                        'browseLabel' => Yii::t('backend', 'Select an image'),
                                    ],
                                    'pluginEvents' => [
                                        "fileclear" => 'function() { $(this).parent().parent().parent().parent().find(".hidden_image").val("") }',
                                    ],
                                ]);
                                ?>
                                <?= $form->field($model, 'hidden_icon_3')->hiddenInput(['class' => 'hidden_image'])->label(FALSE) ?>
                            </div>
                        </div>
                        <!-- End Link 3 icon -->
                    </div>
                    <!-- End Link Title 3 and Url Link 3 , Link 3 icon-->
                </div>
                <!-- ============================== -->
                <!-- ==== End Set Store SNS Link    -->
                <!-- ============================== -->
                <!-- ============================== -->
                <!-- ====  Name Setting Store       -->
                <!-- ============================== -->
                <div class="box-header with-border common-box-h4 col-md-6 ">
                    <h4 class="text-title"><b><?= Yii::t('backend', 'Name Setting Store') ?></b></h4>
                </div>
                <div class="col-md-12">
                    <!-- Store Item_1 -->
                    <div class="store-form">
                        <div class="col-md-12">
                            <div class="col-md-3 label-margin label_store_ct">  
                                <?= Yii::t("backend", "Name Item 1"); ?>
                            </div>
                            <div class="col-md-3">
                                <?= $form->field($model, 'store_item_1_title')->textInput(['maxlength' => true])->label(false) ?>
                            </div>
                        </div>
                        <div class="col-md-12 store_item_ct">
                            <div class="col-md-3 label-margin">  
                            </div>
                            <div class="col-md-4">
                                <?= $form->field($model, 'store_item_1')->textInput(['maxlength' => true])->label(false) ?>
                            </div>
                        </div>
                    </div>
                    <!-- End Store Item_1 -->
                    <!-- Store Item_2 -->
                    <div class="store-form">
                        <div class="col-md-12">
                            <div class="col-md-3 label-margin label_store_ct">  
                                <?= Yii::t("backend", "Name Item 2"); ?>
                            </div>
                            <div class="col-md-3">
                                <?= $form->field($model, 'store_item_2_title')->textInput(['maxlength' => true])->label(false) ?>
                            </div>
                        </div>
                        <div class="col-md-12 store_item_ct">
                            <div class="col-md-3 label-margin">  
                            </div>
                            <div class="col-md-4">
                                <?= $form->field($model, 'store_item_2')->textInput(['maxlength' => true])->label(false) ?>
                            </div>
                        </div>
                    </div>
                    <!-- End Store Item_2 -->
                    <!-- Store Item_3 -->
                    <div class="store-form">
                        <div class="col-md-12">
                            <div class="col-md-3 label-margin label_store_ct">  
                                <?= Yii::t("backend", "Name Item 3"); ?>
                            </div>
                            <div class="col-md-3">
                                <?= $form->field($model, 'store_item_3_title')->textInput(['maxlength' => true])->label(false) ?>
                            </div>
                        </div>
                        <div class="col-md-12 store_item_ct">
                            <div class="col-md-3 label-margin">  
                            </div>
                            <div class="col-md-4">
                                <?= $form->field($model, 'store_item_3')->textInput(['maxlength' => true])->label(false) ?>
                            </div>
                        </div>
                    </div>
                    <!-- End Store Item_3 -->
                    <!-- Store Item_4 -->
                    <div class="store-form">
                        <div class="col-md-12">
                            <div class="col-md-3 label-margin label_store_ct">  
                                <?= Yii::t("backend", "Name Item 4"); ?>
                            </div>
                            <div class="col-md-3">
                                <?= $form->field($model, 'store_item_4_title')->textInput(['maxlength' => true])->label(false) ?>
                            </div>
                        </div>
                        <div class="col-md-12 store_item_ct">
                            <div class="col-md-3 label-margin">  
                            </div>
                            <div class="col-md-4">
                                <?= $form->field($model, 'store_item_4')->textInput(['maxlength' => true])->label(false) ?>
                            </div>
                        </div>
                    </div>
                    <!-- End Store Item_4 -->
                    <!-- Store Item 5 -->
                    <div class="store-form">
                        <div class="col-md-12">
                            <div class="col-md-3 label-margin label_store_ct">  
                                <?= Yii::t("backend", "Name Item 5"); ?>
                            </div>
                            <div class="col-md-3">
                                <?= $form->field($model, 'store_item_5_title')->textInput(['maxlength' => true])->label(false) ?>
                            </div>
                        </div>
                        <div class="col-md-12 store_item_ct">
                            <div class="col-md-3 label-margin">  
                            </div>
                            <div class="col-md-4">
                                <?= $form->field($model, 'store_item_5')->textInput(['maxlength' => true])->label(false) ?>
                            </div>
                        </div>
                    </div>
                    <!-- End Store Item 5 -->
                    <!-- Store Item 6 -->
                    <div class="store-form">
                        <div class="col-md-12">
                            <div class="col-md-3 label-margin label_store_ct">  
                                <?= Yii::t("backend", "Name Item 6"); ?>
                            </div>
                            <div class="col-md-3">
                                <?= $form->field($model, 'store_item_6_title')->textInput(['maxlength' => true])->label(false) ?>
                            </div>
                        </div>
                        <div class="col-md-12 store_item_ct">
                            <div class="col-md-3 label-margin">  
                            </div>
                            <div class="col-md-4">
                                <?= $form->field($model, 'store_item_6')->textInput(['maxlength' => true])->label(false) ?>
                            </div>
                        </div>
                    </div>
                    <!-- End Store Item 6 -->
                    <!-- Store Item 7 -->
                    <div class="store-form">
                        <div class="col-md-12">
                            <div class="col-md-3 label-margin label_store_ct">  
                                <?= Yii::t("backend", "Name Item 7"); ?>
                            </div>
                            <div class="col-md-3">
                                <?= $form->field($model, 'store_item_7_title')->textInput(['maxlength' => true])->label(false) ?>
                            </div>
                        </div>
                        <div class="col-md-12 store_item_ct">
                            <div class="col-md-3 label-margin">  
                            </div>
                            <div class="col-md-4">
                                <?= $form->field($model, 'store_item_7')->textInput(['maxlength' => true])->label(false) ?>
                            </div>
                        </div>
                    </div>
                    <!-- End Store Item 7 -->
                    <!-- Store Item 8 -->
                    <div class="store-form">
                        <div class="col-md-12">
                            <div class="col-md-3 label-margin label_store_ct">  
                                <?= Yii::t("backend", "Name Item 8"); ?>
                            </div>
                            <div class="col-md-3">
                                <?= $form->field($model, 'store_item_8_title')->textInput(['maxlength' => true])->label(false) ?>
                            </div>
                        </div>
                        <div class="col-md-12 store_item_ct">
                            <div class="col-md-3 label-margin">  
                            </div>
                            <div class="col-md-4">
                                <?= $form->field($model, 'store_item_8')->textInput(['maxlength' => true])->label(false) ?>
                            </div>
                        </div>
                    </div>
                    <!-- End Store Item 8 -->
                    <!-- Store Item 9 -->
                    <div class="store-form">
                        <div class="col-md-12">
                            <div class="col-md-3 label-margin label_store_ct">  
                                <?= Yii::t("backend", "Name Item 9"); ?>
                            </div>
                            <div class="col-md-3">
                                <?= $form->field($model, 'store_item_9_title')->textInput(['maxlength' => true])->label(false) ?>
                            </div>
                        </div>
                        <div class="col-md-12 store_item_ct">
                            <div class="col-md-3 label-margin">  
                            </div>
                            <div class="col-md-4">
                                <?= $form->field($model, 'store_item_9')->textInput(['maxlength' => true])->label(false) ?>
                            </div>
                        </div>
                    </div>
                    <!-- End Store Item 9 -->
                    <!-- Store Item 10 -->
                    <div class="store-form">
                        <div class="col-md-12">
                            <div class="col-md-3 label-margin label_store_ct">  
                                <?= Yii::t("backend", "Name Item 10"); ?>
                            </div>
                            <div class="col-md-3">
                                <?= $form->field($model, 'store_item_10_title')->textInput(['maxlength' => true])->label(false) ?>
                            </div>
                        </div>
                        <div class="col-md-12 store_item_ct">
                            <div class="col-md-3 label-margin">  
                            </div>
                            <div class="col-md-4">
                                <?= $form->field($model, 'store_item_10')->textInput(['maxlength' => true])->label(false) ?>
                            </div>
                        </div>
                    </div>
                    <!-- End Store Item 10 -->  
                </div>
                <!-- ============================== -->
                <!-- ==== End Name Setting Store    -->
                <!-- ============================== -->
                <!-- ============================== -->
                <!-- ==== Staff name setting        -->
                <!-- ============================== -->
                <div class="box-header with-border common-box-h4 col-md-6 ">
                    <h4 class="text-title"><b><?= Yii::t('backend', 'Staff name setting') ?></b></h4>
                </div>
                <div class="col-md-12">
                    <!-- Staff Item 1 -->
                    <div class="store-form">
                        <div class="col-md-12"> 
                            <div class="col-md-3 label-margin label_store_ct">  
                                <?= Yii::t("backend", "Staff Item 1"); ?>
                            </div>
                            <div class="col-md-4 input_store_ct">
                                <?= $form->field($model, 'staff_item_1')->textInput(['maxlength' => true])->label(false) ?>
                            </div>
                        </div>
                    </div>
                    <!-- End Staff Item 1 -->
                    <!-- Staff Item 2 -->
                    <div class="store-form">
                        <div class="col-md-12">
                            <div class="col-md-3 label-margin label_store_ct">  
                                <?= Yii::t("backend", "Staff Item 2"); ?>
                            </div>
                            <div class="col-md-4 input_store_ct">
                                <?= $form->field($model, 'staff_item_2')->textInput(['maxlength' => true])->label(false) ?>
                            </div>
                        </div>
                    </div>
                    <!-- End Staff Item 2 -->
                    <!-- Staff Item 3 -->
                    <div class="store-form">
                        <div class="col-md-12">
                            <div class="col-md-3 label-margin label_store_ct">  
                                <?= Yii::t("backend", "Staff Item 3"); ?>
                            </div>
                            <div class="col-md-4 input_store_ct">
                                <?= $form->field($model, 'staff_item_3')->textInput(['maxlength' => true])->label(false) ?>
                            </div>
                        </div>
                    </div>
                    <!-- End Staff Item 3 -->
                    <!-- Staff Item 4 -->
                    <div class="store-form">
                        <div class="col-md-12">
                            <div class="col-md-3 label-margin label_store_ct">  
                                <?= Yii::t("backend", "Staff Item 4"); ?>
                            </div>
                            <div class="col-md-4 input_store_ct">
                                <?= $form->field($model, 'staff_item_4')->textInput(['maxlength' => true])->label(false) ?>
                            </div>
                        </div>
                    </div>
                    <!-- End Staff Item 4 -->
                    <!-- Staff Item 5 -->
                    <div class="store-form">
                        <div class="col-md-12">
                            <div class="col-md-3 label-margin label_store_ct">  
                                <?= Yii::t("backend", "Staff Item 5"); ?>
                            </div>
                            <div class="col-md-4 input_store_ct">
                                <?= $form->field($model, 'staff_item_5')->textInput(['maxlength' => true])->label(false) ?>
                            </div>
                        </div>
                    </div>
                    <!-- End Staff Item 5 -->
                </div>
                <!-- ============================== -->
                <!-- ==== End Staff name setting    -->
                <!-- ============================== -->
                <!-- ============================== -->
                <!-- Set Function Display On Member App  -->
                <!-- ============================== -->
                <div class="box-header with-border common-box-h4 col-md-6 ">
                    <h4 class="text-title"><b><?= Yii::t('backend', 'Set Function Display On Member App') ?></b></h4>
                </div>
                <!-- Booking -->
                <div class="col-md-12">
                    <div class="col-md-3 label-margin label_store_ct">  
                        <?= Yii::t("backend", "Booking"); ?>
                    </div>
                    <div class="col-md-5">
                        <?= $form->field($model, 'show_in_booking_flg')->radioList(Constants::LIST_SHOW_STORE, ['stype' => 'margin-right: 4em;'])->label(false); ?>
                    </div>
                </div>
                <!-- End Booking -->
                <!-- Notice -->
                <div class="col-md-12">
                    <div class="col-md-3 label-margin label_store_ct">  
                        <?= Yii::t("backend", "Notice"); ?>
                    </div>
                    <div class="col-md-5">
                        <?= $form->field($model, 'show_in_notice_flg')->radioList(Constants::LIST_SHOW_STORE)->label(false); ?>
                    </div>
                </div>
                <!-- End Notice -->
                <!-- Coupon -->
                <div class="col-md-12">
                    <div class="col-md-3 label-margin label_store_ct">  
                        <?= Yii::t("backend", "Coupon"); ?>
                    </div>
                    <div class="col-md-5">
                        <?= $form->field($model, 'show_in_coupon_flg')->radioList(Constants::LIST_SHOW_STORE)->label(false); ?>
                    </div>
                </div>
                <!-- End Coupon -->
                <!-- Product Catalog -->
                <div class="col-md-12">
                    <div class="col-md-3 label-margin label_store_ct">  
                        <?= Yii::t("backend", "Product Catalog"); ?>
                    </div>
                    <div class="col-md-5">
                        <?= $form->field($model, 'show_in_product_category_flg')->radioList(Constants::LIST_SHOW_STORE)->label(false); ?>
                    </div>
                </div>
                <!-- End Product Catalog -->
                <!-- My Page -->
                <div class="col-md-12">
                    <div class="col-md-3 label-margin label_store_ct">  
                        <?= Yii::t("backend", "My Page"); ?>
                    </div>
                    <div class="col-md-5">
                        <?= $form->field($model, 'show_in_my_page_flg')->radioList(Constants::LIST_SHOW_STORE)->label(false); ?>
                    </div>
                </div>
                <!-- End My Page -->
                <!-- Censorship Information Staff -->
                <div class="col-md-12">
                    <div class="col-md-3 label-margin label_store_ct">  
                        <?= Yii::t("backend", "Censorship Information Staff"); ?>
                    </div>
                    <div class="col-md-5">
                        <?= $form->field($model, 'show_in_staff_flg')->radioList(Constants::LIST_SHOW_STORE)->label(false); ?>
                    </div>
                </div>
                <!-- End Censorship Information Staff -->
                <!-- ============================== -->
                <!-- End Set Function Display On Member App  -->
                <!-- ============================== -->
                <!-- Button -->
                <div class="col-md-12">
                    <?= Html::a(Yii::t('backend', 'Return'), ['index'], ['class' => 'btn btn-default common-button-default']) ?>
                    <?=
                    Html::submitButton(Yii::t('backend', 'Confirm'), [
                        'class' => 'btn common-button-submit',
                        'id' => 'btn-confirm',
                        'onclick' => 'init_size_image(event)'
                    ])
                    ?>
                </div>
                <!-- End Button -->
                <!-- Pop Up -->
                <!-- Preview -->
                <?php
                Modal::begin([
                    'id' => 'previewModalConfirm',
                    'size' => 'SIZE_LARGE',
                    'header' => '<div class="box-header with-border common-box-h4 col-md-8"><h4 class="text-title"><b>' . Yii::t('backend', 'Cash voucher preview') . '</b></h4></div>',
                    'footer' => Html::button(Yii::t('backend', 'Close'), ['class' => 'btn common-button-default', 'data-dismiss' => "modal", 'id' => 'btn-close2']),
                    'footerOptions' => ['class' => 'modal-footer text-center'],
                ]);
                ?>

                <?=
                PreviewStoreGift::widget([
                    "data" => [
                        'date_time' => [
                            'type' => 'date_time',
                            'label' => Yii::t('backend', ' ')
                        ],
                        'tmp_gold_ticket_image' => [
                            'type' => 'image',
                            'label' => Yii::t('backend', ' ')
                        ],
                        'gold_ticket_description1' => [
                            'type' => 'input_description1',
                            'label' => Yii::t('backend', ' ')
                        ],
                        'gold_ticket_description2' => [
                            'type' => 'input_description2',
                            'label' => Yii::t('backend', ' ')
                        ],
                        'expiration_date_flg' => [
                            'type' => 'radio_expiration_date_flg',
                            'label' => Yii::t('backend', ' ')
                        ],
                        'member_no' => [
                            'type' => 'radio_member_no',
                            'label' => Yii::t('backend', ' ')
                        ],
                    ],
                    "modelName" => $model->formName(),
                    'idBtnConfirm' => 'btn-confirm-preview-md',
                    'btnClose' => 'btn-close',
                    'btnSubmit' => 'btn-submit',
                    'modalId' => 'previewModalConfirm'
                ])
                ?>
                <?php
                Modal::end();
                ?> 
                <!-- End Preview -->
                <?php
                Modal::begin([
                    'id' => 'userModal',
                    'size' => 'SIZE_LARGE',
                    'header' => '<div class="box-header with-border common-box-h4 col-md-8"><h4 class="text-title"><b>' . Yii::t('backend', 'Confirm input') . '</b></h4></div>',
                    'footer' => Html::button(Yii::t('backend', 'Close'), ['class' => 'btn common-button-default', 'data-dismiss' => "modal", 'id' => 'btn-close'])
                    . Html::submitButton(Yii::t('backend', 'Save'), ['class' => 'btn common-button-submit', 'id' => 'btn-submit']),
                    'closeButton' => FALSE,
                ]);
                ?>

                <?=
                PreviewStore::widget([
                    "data" => [
                        // Name Store
                        'name' => [
                            'type' => 'input',
                            'label' => Yii::t('backend', 'Name Store')
                        ],
                        // End Name Store            
                        // Post code
                        'post_code' => [
                            'type' => 'input',
                            'label' => Yii::t('backend', 'PostCode')
                        ],
                        // End Post code
                        // Address
                        'address' => [
                            'type' => 'input',
                            'label' => Yii::t('backend', 'Address Store')
                        ],
                        // End Address
                        // Directions
                        'directions' => [
                            'type' => 'input',
                            'label' => Yii::t('backend', 'Directions Address')
                        ],
                        // End Directions
                        // Home Page
                        'website' => [
                            'type' => 'input',
                            'label' => Yii::t('backend', 'Website')
                        ],
                        // End Home Page
                        // Time Open
                        'time_open' => [
                            'type' => 'input',
                            'groupWithSeat' => 'time_close',
                            'label' => Yii::t('backend', 'Time From')
                        ],
                        // End Time Open
                        // Regular Holiday
                        'regular_holiday' => [
                            'type' => 'input',
                            'label' => Yii::t('backend', 'Regular Holiday')
                        ],
                        // End Regular Holiday
                        // Tel
                        'tel' => [
                            'type' => 'input',
                            'label' => Yii::t('backend', 'Phone')
                        ],
                        // End Tel            
                        // Introduce Store
                        'introduce' => [
                            'type' => 'input',
                            'label' => Yii::t('backend', 'Introduction Store')
                        ],
                        // End Introduce Store
                        // Reservation site member app display
                        'show_flg' => [
                            'type' => 'radio',
                            'label' => Yii::t('backend', 'Reservation site member app display')
                        ],
                        // End Reservation site member app display    
                        // Image Store
                        'tmp_image1' => [
                            'type' => 'image',
                            'label' => Yii::t('backend', 'Image1')
                        ],
                        'tmp_image2' => [
                            'type' => 'image',
                            'label' => Yii::t('backend', 'Image2')
                        ],
                        'tmp_image3' => [
                            'type' => 'image',
                            'label' => Yii::t('backend', 'Image3')
                        ],
                        // End Image Store
                        // Reservations Possible Time
                        'reservations_possible_time' => [
                            'type' => 'input',
                            'label' => Yii::t('backend', 'Reservations Possible Time')
                        ],
                        'cancel_possible_time' => [
                            'type' => 'input',
                            'label' => Yii::t('backend', 'Cancel Possible Time')
                        ],
                        // End Reservations Possible Time
                        // Booking Resources
                        'booking_resources' => [
                            'type' => 'radio',
                            'label' => Yii::t('backend', 'Booking Resources')
                        ],
                        'confirm_booking' => [
                            'type' => 'text_confirm_booking',
                            'label' => Yii::t('backend', 'Confirm Booking')
                        ],
                        // Check List
                        'question_priview' => [
                            'type' => 'textarea',
                        ],
                        // End Booking Resources 
                        // Link SNS 1 Store
                        'link_title_1' => [
                            'type' => 'input',
                            'label' => Yii::t('backend', 'Link Title 1')
                        ],
                        'link_url_1' => [
                            'type' => 'input',
                            'label' => Yii::t('backend', 'Link Url 1')
                        ],
                        'tmp_icon_1' => [
                            'type' => 'image',
                            'label' => Yii::t('backend', 'Link 1 icon')
                        ],
                        // End Link SNS 1 Store
                        // Link SNS 2 Store
                        'link_title_2' => [
                            'type' => 'input',
                            'label' => Yii::t('backend', 'Link Title 2')
                        ],
                        'link_url_2' => [
                            'type' => 'input',
                            'label' => Yii::t('backend', 'Link Url 2')
                        ],
                        'tmp_icon_2' => [
                            'type' => 'image',
                            'label' => Yii::t('backend', 'Link 2 icon')
                        ],
                        // End Link SNS 2 Store
                        // Link SNS 3 Store
                        'link_title_3' => [
                            'type' => 'input',
                            'label' => Yii::t('backend', 'Link Title 3')
                        ],
                        'link_url_3' => [
                            'type' => 'input',
                            'label' => Yii::t('backend', 'Link Url 3')
                        ],
                        'tmp_icon_3' => [
                            'type' => 'image',
                            'label' => Yii::t('backend', 'Link 3 icon')
                        ],
                        // End Link SNS 3 Store
                        // Name Setting Store 
                        'store_item_1_title' => [
                            'type' => 'input',
                            'label' => Yii::t('backend', 'Name Item 1')
                        ],
                        'store_item_1' => [
                            'type' => 'input',
                            'label' => Yii::t('backend', ' ')
                        ],
                        'store_item_2_title' => [
                            'type' => 'input',
                            'label' => Yii::t('backend', 'Name Item 2')
                        ],
                        'store_item_2' => [
                            'type' => 'input',
                            'label' => Yii::t('backend', ' ')
                        ],
                        'store_item_3_title' => [
                            'type' => 'input',
                            'label' => Yii::t('backend', 'Name Item 3')
                        ],
                        'store_item_3' => [
                            'type' => 'input',
                            'label' => Yii::t('backend', ' ')
                        ],
                        'store_item_4_title' => [
                            'type' => 'input',
                            'label' => Yii::t('backend', 'Name Item 4')
                        ],
                        'store_item_4' => [
                            'type' => 'input',
                            'label' => Yii::t('backend', ' ')
                        ],
                        'store_item_5_title' => [
                            'type' => 'input',
                            'label' => Yii::t('backend', 'Name Item 5')
                        ],
                        'store_item_5' => [
                            'type' => 'input',
                            'label' => Yii::t('backend', ' ')
                        ],
                        'store_item_6_title' => [
                            'type' => 'input',
                            'label' => Yii::t('backend', 'Name Item 6')
                        ],
                        'store_item_6' => [
                            'type' => 'input',
                            'label' => Yii::t('backend', ' ')
                        ],
                        'store_item_7_title' => [
                            'type' => 'input',
                            'label' => Yii::t('backend', 'Name Item 7')
                        ],
                        'store_item_7' => [
                            'type' => 'input',
                            'label' => Yii::t('backend', ' ')
                        ],
                        'store_item_8_title' => [
                            'type' => 'input',
                            'label' => Yii::t('backend', 'Name Item 8')
                        ],
                        'store_item_8' => [
                            'type' => 'input',
                            'label' => Yii::t('backend', ' ')
                        ],
                        'store_item_9_title' => [
                            'type' => 'input',
                            'label' => Yii::t('backend', 'Name Item 9')
                        ],
                        'store_item_9' => [
                            'type' => 'input',
                            'label' => Yii::t('backend', ' ')
                        ],
                        'store_item_10_title' => [
                            'type' => 'input',
                            'label' => Yii::t('backend', 'Name Item 10')
                        ],
                        'store_item_10' => [
                            'type' => 'input',
                            'label' => Yii::t('backend', ' ')
                        ],
                        // End Name Setting Store
                        // Staff name setting
                        'staff_item_1' => [
                            'type' => 'input',
                            'label' => Yii::t('backend', 'Staff Item 1')
                        ],
                        'staff_item_2' => [
                            'type' => 'input',
                            'label' => Yii::t('backend', 'Staff Item 2')
                        ],
                        'staff_item_3' => [
                            'type' => 'input',
                            'label' => Yii::t('backend', 'Staff Item 3')
                        ],
                        'staff_item_4' => [
                            'type' => 'input',
                            'label' => Yii::t('backend', 'Staff Item 4')
                        ],
                        'staff_item_5' => [
                            'type' => 'input',
                            'label' => Yii::t('backend', 'Staff Item 5')
                        ],
                        // End Staff name setting
                        // Start show
                        'show_in_booking_flg' => [
                            'type' => 'checkbox_store',
                            'show_in_booking_flg_check' => true,
                            'label' => Yii::t('backend', 'Booking')
                        ],
                        'show_in_notice_flg' => [
                            'type' => 'checkbox_store',
                            'show_in_notice_flg_check' => true,
                            'label' => Yii::t('backend', 'Notice')
                        ],
                        'show_in_coupon_flg' => [
                            'type' => 'checkbox_store',
                            'show_in_coupon_flg_check' => true,
                            'label' => Yii::t('backend', 'Coupon')
                        ],
                        'show_in_product_category_flg' => [
                            'type' => 'checkbox_store',
                            'show_in_product_category_flg_check' => true,
                            'label' => Yii::t('backend', 'Product Catalog')
                        ],
                        'show_in_my_page_flg' => [
                            'type' => 'checkbox_store',
                            'show_in_my_page_flg_check' => true,
                            'label' => Yii::t('backend', 'My Page')
                        ],
                        'show_in_staff_flg' => [
                            'type' => 'checkbox_store',
                            'show_in_staff_flg_check' => true,
                            'label' => Yii::t('backend', 'Censorship Information Staff')
                        ],
                    // End show
                       
                    ],
                    "modelName" => $model->formName(),
                    'idBtnConfirm' => 'btn-confirm',
                    'formId' => strtolower($model->formName()) . '-id',
                    'btnClose' => 'btn-close',
                    'btnSubmit' => 'btn-submit',
                    'modalId' => 'userModal'
                ])
                ?>
                <?php
                Modal::end();
                ?>
                <!-- End Pop Up -->

            </div>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
    <!-- Datpdt 21/09/2016 End-->  
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBuVBIXdrZ97l5-wliywj9HRsp2ZGDmmgI" type="text/javascript"></script>
    <script>
        jQuery.fn.preventDoubleSubmission = function () {

            var last_clicked, time_since_clicked;

            $("#btn-submit").bind('click', function (event) {

                if (last_clicked)
                    time_since_clicked = event.timeStamp - last_clicked;

                last_clicked = event.timeStamp;

                if (time_since_clicked < 2000)
                    return false;

                return true;
            });
        };
        $('#masterstore-id').preventDoubleSubmission();

        $("input[type=radio][name='CustomerStore[news_transmision_flg]'][value='1']").prop("disabled", true);
        $("#search-code").click(function (e) {
            getPostcode('#masterstore-post_code', '#masterstore-address');
        });
        function init_size_image(e) {
            $(".file-preview-image").removeAttr("style");
        }
        // =========================
        // Form Confirm
        // =========================


        function clone() {

            var cloneIndex = $(".clonedInput").length - 1;

            var $clone = $("#clonedInput").show().clone();
            $clone.attr('key', cloneIndex);
            $clone.find('.display_flg input:radio').attr('name', "Question[" + cloneIndex + '][display_flg]');
            $clone.find('[name*="Question[type]"]').attr('name', "Question[" + cloneIndex + '][type]').attr('key', cloneIndex);
            $clone.find('[name*="Checklist[content_table]"]').attr('name', "Checklist[content_table][" + cloneIndex + ']').attr('key', cloneIndex);
            $clone.attr("name", "clonedInput" + cloneIndex);
            $clone.find('[name*="Question[id_checklist]"]').attr('name', "Question[id_checklist][" + cloneIndex + ']').attr('key', cloneIndex);
            $clone.find('[name*="Question[id_question]"]').attr('name', "Question[id_question][" + cloneIndex + ']').attr('key', cloneIndex);
            $clone.find('[name*="Question[id_answer]"]').attr('name', "Question[id_answer][" + cloneIndex + ']').attr('key', cloneIndex);
            $clone.appendTo("#appen_table");
            addAttentionClickAdd(cloneIndex);

            $("#clonedInput").hide();

            cloneIndex++;

        }

        $("#clone").on("click", function () {
            clone();
        });

        $(document).ready(function () {
            // Load _attention (注意事項)
            var idStore = GetURLParameter('id');

            if (typeof (idStore) !== "undefined") {
                $('#btn-confirm-question').trigger('click');
            } else {
                addAttention(0);
            }

            // Select your input element.
            var numInput = document.querySelector('input');

            // Listen for input event on numInput.
            numInput.addEventListener('input', function () {
                // Let's match only digits.
                var num = this.value.match(/^\d+$/);
                if (num === null) {
                    // If we have no match, value will be empty.
                    this.value = "";
                }
            }, false);
        });

        // **********************
        // Get parameter url
        // **********************
        function GetURLParameter(sParam)
        {
            var sPageURL = window.location.search.substring(1);
            var sURLVariables = sPageURL.split('&');
            for (var i = 0; i < sURLVariables.length; i++)
            {
                var sParameterName = sURLVariables[i].split('=');
                if (sParameterName[0] === sParam)
                {
                    return sParameterName[1];
                }
            }
        }


    </script>