<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\LinkPager;
use yii\widgets\Pjax;
use common\models\MasterNotice;
use common\components\Constants;

/* @var $this yii\web\View */
/* @var $searchModel common\models\MstProductSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$role = \common\components\FindPermission::getRole();
$this->title = Yii::t('backend', 'Store List');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <div class="col-lg-12 col-md-12">
        <div class="box box-info box-solid">
            <div class="box-header with-border">
                <h4 class="text-title"><b><?= $this->title ?></b></h4>
            </div>
            <div class="box-body content">
                <div class="col-md-12">
                    <div class="common-box">
                        <div class="box-header with-border common-box-h4 col-md-6">
                            <h4 class="text-title"><b><?= Yii::t('backend', 'Store Finder') ?></b></h4>
                        </div>
                        <div class="box-body content">
                            <div class="col-md-8">
                                <?php echo $this->render('_search', ['model' => $searchModel]); ?>                                
                            </div>
                        </div>
                    </div>
                    <?php
                    Pjax::begin();
                    ?>    
                    <div class="common-box">
                        <div class="box-header with-border common-box-h4 col-md-6">
                            <h4 class="text-title"><b><?= Yii::t('backend', 'Store List') ?></b></h4>
                        </div>
                        <div class="box-body content">
                            <div class="col-md-10">
                                <?php
                                if ($role->permission_id == Constants::APP_MANAGER_ROLE || $role->permission_id == Constants::COMPANY_MANAGER_ROLE)
                                    echo Html::a(Yii::t('backend', 'Create'), ['create'], ['class' => 'btn common-button-submit common-float-right ct_store_bt_create', 'style' => 'margin-bottom: 0.5em;'])
                                    ?>

                                <?=
                                GridView::widget([
                                    'dataProvider' => $dataProvider,
                                    'layout' => "{pager}\n{summary}\n{items}",
                                    'summaryOptions' => ['class' => 'common-clr-fl-right'],
                                    'summary' => false,
                                    'pager' => [
                                        'class' => 'common\components\LinkPagerMillionet',
                                        'options' => ['class' => 'pagination common-float-right'],
                                    ],
                                    'tableOptions' => [
                                        'class' => 'table table-striped table-bordered text-center',
                                    ],
                                    'columns' => [
                                        ['class' => 'yii\grid\SerialColumn', 'header' => 'No', 'options' => ['class' => 'with-table-no']],
                                        [
                                            'attribute' => 'name',
                                            'label' => Yii::t('backend', 'Name Store'),
                                        ],
                                        [
                                            'attribute' => 'tel',
                                            'label' => Yii::t('backend', 'Phone'),
                                        ],
                                        [
                                            'attribute' => 'time_open',
                                            'label' => Yii::t('backend', 'Time From'),
                                            'value' => function ($model) {
                                                if($model->time_close != null){
                                                    return $model->time_open.' ～ '.$model->time_close;
                                                }else{
                                                    return $model->time_open;
                                                }
                                                
                                            }
                                        ],
                                        [
                                            'class' => 'yii\grid\ActionColumn', 'template' => "{delete} {update}",
                                            'options' => ['class' => 'with-table-button'],
                                            'buttons' => [
                                                //view button
                                                // Delete
                                                'delete' => function ($url, $model) {
                                                    return Html::a('<span class="fa fa-delete"></span>' . Yii::t('backend', 'Delete'), $url, [
                                                                'title' => Yii::t('backend', 'Delete'),
                                                                'class' => 'btn common-button-action',
                                                                'aria-label' => "Delete",
                                                                'data-confirm' => Yii::t('backend', "Delete Store Error Message"),
                                                                'data-method' => "post"
                                                    ]);
                                                },
                                                        // Update                                                        
                                                        'update' => function ($url, $model) {
                                                    return Html::a('<span class="fa fa-delete"></span>' . Yii::t('backend', 'Update'), $url, [
                                                                'title' => Yii::t('backend', 'Update'),
                                                                'class' => 'btn common-button-action',
                                                    ]);
                                                },
                                                    ],
                                                ],
                                            ],
                                        ]);
                                        ?>
                                    </div>
                                </div>
                            </div>
        <?php
        Pjax::end();
        ?>
                </div>
            </div>
        </div>
    </div>
</div>