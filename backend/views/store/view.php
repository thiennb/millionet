<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\MasterStore */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Stores'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="store-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name_store',
            'address_store:ntext',
            'directions_address',
            'website',
            'time_open',
            'time_close',
            'regular_holiday',
            'phone',
            'introduction_store:ntext',
            'image1',
            'image2',
            'image3',
            'show:boolean',
            'point_conversion',
            'reservations_possible_time',
            'booking_resources',
            'name_item_1',
            'name_item_2',
            'name_item_3',
            'name_item_4',
            'name_item_5',
            'name_item_6',
            'name_item_7',
            'name_item_8',
            'name_item_9',
            'name_item_10',
            'created_at',
            'updated_at',
            'people_created',
            'people_updated',
        ],
    ]) ?>

</div>
