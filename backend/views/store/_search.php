<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
?>
<div class="store-search">
    <?php
    $form = ActiveForm::begin([
                'action' => ['index'],
                'method' => 'get',
                'enableClientValidation' => false,
    ]);
    ?>
    <div class="col-md-12">
        <div class="store-form">
            <div class="col-md-2 label-margin">  
                <?= Yii::t("backend", "Name Store"); ?>
            </div> 
            <div class="col-md-4 store_ct">
                <?= $form->field($model, 'name')->textInput(['maxlength' => '100'])->label(false) ?>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="store-form">
            <div class="col-md-2 label-margin">  
                <?= Yii::t("backend", "Phone"); ?>
            </div> 
            <div class="col-md-4 store_ct">
                <?= $form->field($model, 'tel')->label(false)->widget(\yii\widgets\MaskedInput::className(), [
                      'mask' => '9',
                      'clientOptions' => ['repeat' => 17, 'greedy' => false]
                    ])->textInput(['maxlength' => 17]) ?>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="store-form">
            <div class="col-md-2 label-margin">  
                <?= Yii::t("backend", "PostCode"); ?>
            </div> 
            <div class="col-md-4 store_ct">

                <?=
                $form->field($model, 'post_code')->label(false)->textInput(['maxlength' => '8'])->widget(\yii\widgets\MaskedInput::className(), [
                    'mask' => '999-9999',
                    'clientOptions' => [
                        'removeMaskOnSubmit' => true
            ]])
                ?>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="store-form">
            <div class="col-md-2 label-margin">  
                <?= Yii::t("backend", "Address Store"); ?>
            </div> 
            <div class="col-md-4 store_ct">
                <?= $form->field($model, 'address')->textInput(['maxlength' => '240'])->label(false) ?>
            </div>
        </div>
    </div>
    <div class="form-group" style="margin-left: 2em;">
        <button type="button" class="btn btn-default common-button-default"><a style="color: black;" href="<?php echo Url::to('/admin') ?>"><?php echo Yii::t('backend', 'Return') ?></a></button>
    <?= Html::submitButton(Yii::t('backend', 'Search'), ['class' => 'btn common-button-submit']) ?>
    </div>
<?php ActiveForm::end(); ?>
</div>
