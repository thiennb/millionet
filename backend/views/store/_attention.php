<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->registerJsFile('@web/js/confirm_booking.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
?>
<?php
$form = ActiveForm::begin();
?>
<div class="col-md-12">
    <div class="col-md-1 font_label">
        Q
    </div>
    <div class="col-md-10">
        <textarea class="form-control area_question" name="Question[0][notice_content]" maxlength="300" rows="4" key="0"></textarea>
    </div>
    <div class="col-md-1 font_label">
        <p id="count_text" name="count_text[0]">0/300<p>
    </div>
</div>
<?php ActiveForm::end(); ?>