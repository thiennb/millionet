<?php
use yii\helpers\Html;
?>
<div class="col-md-8 tab-pane"  role="tabpanel" id="step2">
    <div class="box-header with-border common-box-h4 col-md-6 ">
        <h4 class="text-title"><b><?= Yii::t('backend', '予約時確認事項') ?></b></h4>
    </div>
    <div class="store-form">
        <div class="col-md-9">
            <div id="clonedInput" key="0" class="clonedInput" style="display: none">
                <input type="hidden" class="id_checklist" name="Question[id_checklist][0]" value="0" key="0">
                <input type="hidden" class="id_question" name="Question[id_question][0]" value="0" key="0">
                <input type="hidden" class="id_answer" name="Question[id_answer][0]" value="0" key="0">
                <table class="table table-bordered tb">
                    <thead>
                        <tr>
                            <th class="bg_table_confirm_booking" style="width: 8em;padding-bottom: 1em">
                                タイプ
                            </th>
                            <th style="width: 20em;background : #FFFFFF">

                                <select id="checklist-type-0" class="form-control type_question" name="Question[type][0]" key="0">
                                    <option value="1">注意事項</option>
                                    <option value="2">質問事項</option>
                                </select>
                            </th>
                            <th class="bg_table_confirm_booking" style="width: 5em;padding-bottom: 1em;">
                                表示
                            </th>
                            <th style="background : #FFFFFF">

                                <div id="checklist-display_flg" class="display_flg"  style="float: left;">
                                    <label><input type="radio" name="Question[display_flg][0]" value="0" checked=""> 表示する</label>
                                    <label><input type="radio" name="Question[display_flg][0]" value="1"> 表示しない</label>
                                </div>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class="bg_table_confirm_booking font_label">内容</td>
                            <td colspan="3" style="text-align: left" name="Checklist[content_table]" >
                              
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div id="appen_table">
    <?php
    if ($check_list == null) {
        echo '<div class="store-form">
                                <div class="col-md-9 table_question" id="clone_table">
                                    <div id="clonedInput" class="clonedInput" name="clonedInput0" key="0">
                                        <input type="hidden" class="id_checklist" name="Question[0][id_checklist]" value="0" key="0">
                                        <input type="hidden" class="id_question" name="Question[0][id_question]" value="0" key="0">
                                        <input type="hidden" class="id_answer" name="Question[id_answer][0]" value="0" key="0">
                                        <input type="hidden" class="id_type_db" name="Question[0][id_type_db]" value="0" key="0">
                                        <table class="table table-bordered">
                                            <thead>
                                                <tr>
                                                    <th class="bg_table_confirm_booking" style="width: 8em;padding-bottom: 1em">
                                                        タイプ
                                                    </th>
                                                    <th style="width: 20em;background : #FFFFFF">
                                                        <select id="checklist-type-0" class="form-control type_question" name="Question[0][type]" key="0">
                                                            <option value="1">注意事項</option>
                                                            <option value="2">質問事項</option>
                                                        </select>

                                                    </th>
                                                    <th class="bg_table_confirm_booking" style="width: 5em;padding-bottom: 1em;">
                                                        表示
                                                    </th>
                                                    <th style="background : #FFFFFF">

                                                        <div id="checklist-display_flg" class="display_flg" style="float: left;">
                                                            <label><input type="radio" name="Question[0][display_flg]" value="0" checked=""> 表示する</label>
                                                            <label><input type="radio" name="Question[0][display_flg]" value="1"> 表示しない</label>
                                                        </div>
                                                    </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td class="bg_table_confirm_booking font_label">内容</td>
                                                    <td colspan="3" style="text-align: left" name="Checklist[content_table][0]" >
                                                        <div class="col-md-12"><div class="col-md-1 font_label">Q</div>
                                                        <div class="col-md-10 notice_content_store_ct">
                                                            <textarea class="form-control area_question" name="Question[0][notice_content]" maxlength="300" rows="4" key="0"></textarea>
                                                        </div>
                                                        <div class="col-md-1 font_label"><p id="count_text" name="count_text[0]">0/300</p><p></p></div></div>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>

                          ';
    } else {
        for ($i = 0; $i < count($check_list); $i++) {
            if ($check_list[$i]['type'] == '1') {
                echo '<div class="store-form">
                                <div class="col-md-9 table_question" id="clone_table">
                                    <div id="clonedInput" class="clonedInput" name="clonedInput' . $i . '" key="' . $i . '">
                                        <input type="hidden" class="id_checklist" name="Question[' . $i . '][id_checklist]" value="' . $check_list[$i]['id_checklist'] . '" key="'.$i.'">
                                        <input type="hidden" class="id_question" name="Question[' . $i . '][id_question]" value="' . $check_list[$i]['id_question'] . '" key="'.$i.'">
                                        <input type="hidden" class="id_answer" name="Question[' . $i . '][id_answer]" value="' . $check_list[$i]['id_answer'] . '" key="'.$i.'">
                                        <input type="hidden" class="id_type_db" name="Question[' . $i . '][id_type_db]" value="' . $check_list[$i]['type'] . '" key="'.$i.'">
                                        <input type="hidden" class="id_option_db" name="Question[' . $i . '][option]" value="' . $check_list[$i]['option'] . '" key="'.$i.'">
                                        <table class="table table-bordered">
                                            <thead>
                                                <tr>
                                                    <th class="bg_table_confirm_booking" style="width: 8em;padding-bottom: 1em">
                                                        タイプ
                                                    </th>' .
                                                    '<th style="width: 20em;background : #FFFFFF">
                                                        <select id="checklist-type-0" class="form-control type_question" name="Question[' . $i . '][type]" key="' . $i . '">
                                                            <option value="1" selected>注意事項</option>
                                                            <option value="2">質問事項</option>
                                                        </select>
                                                    </th>
                                                    <th class="bg_table_confirm_booking" style="width: 5em;padding-bottom: 1em">
                                                        表示
                                                    </th>
                                                    <th style="background : #FFFFFF">
                                                        <div id="checklist-display_flg" class="display_flg" style="float: left;">';

                                                    if ($check_list[$i]['display_flg'] == '0') {
                                                        echo '
                                                            <label><input type="radio" name="Question[' . $i . '][display_flg]" value="0" checked> 表示する</label>
                                                            <label><input type="radio" name="Question[' . $i . '][display_flg]" value="1"> 表示しない</label>
                                                            ';
                                                    } else {
                                                        echo '
                                                            <label><input type="radio" name="Question[' . $i . '][display_flg]" value="0"> 表示する</label>
                                                            <label><input type="radio" name="Question[' . $i . '][display_flg]" value="1" checked> 表示しない</label>
                                                            ';
                                                    }
                                                    echo '</div>
                                                    </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td class="bg_table_confirm_booking font_label">内容</td>
                                                    <td colspan="3" style="text-align: left" name="Checklist[content_table][' . $i . ']" >
                                                        <div class="col-md-12">
                                                        <div class="col-md-1 font_label">
                                                            Q
                                                        </div>
                                                        <div class="col-md-10 notice_content_store_ct">
                                                            <textarea class="form-control area_question" name="Question[' . $i . '][notice_content]" maxlength="300" rows="4" key="' . $i . '">' . $check_list[$i]['notice_content'] . '</textarea>
                                                        </div>
                                                        <div class="col-md-1 font_label">
                                                            <p id="count_text" name="count_text[' . $i . ']">' . strlen($check_list[$i]['notice_content']) . '/300<p>
                                                        </div>
                                                    </div>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                       </div>';
            } else {
                if ($check_list[$i]['option'] == '1') {
                    echo '<div class="store-form">
                                <div class="col-md-9 table_question" id="clone_table">
                                    <div id="clonedInput" class="clonedInput" name="clonedInput' . $i . '" key="' . $i . '">
                                        <input type="hidden" class="id_checklist" name="Question[' . $i . '][id_checklist]" value="' . $check_list[$i]['id_checklist'] . '" key="'.$i.'">
                                        <input type="hidden" class="id_question" name="Question[' . $i . '][id_question]" value="' . $check_list[$i]['id_question'] . '" key="'.$i.'">
                                        <input type="hidden" class="id_answer" name="Question[' . $i . '][id_answer]" value="' . $check_list[$i]['id_answer'] . '" key="'.$i.'">
                                        <input type="hidden" class="id_type_db" name="Question[' . $i . '][id_type_db]" value="' . $check_list[$i]['type'] . '" key="'.$i.'">
                                        <input type="hidden" class="id_option_db" name="Question[' . $i . '][option]" value="' . $check_list[$i]['option'] . '" key="'.$i.'">
                                        <table class="table table-bordered">
                                            <thead>
                                                <tr>
                                                    <th class="bg_table_confirm_booking" style="width: 8em;padding-bottom: 1em">
                                                        タイプ
                                                    </th>' .
                                                    '<th style="width: 20em;background : #FFFFFF">
                                                        <select id="checklist-type-0" class="form-control type_question" name="Question[' . $i . '][type]" key="' . $i . '">
                                                            <option value="1" >注意事項</option>
                                                            <option value="2" selected>質問事項</option>
                                                        </select>
                                                    </th>
                                                    <th class="bg_table_confirm_booking" style="width: 5em;padding-bottom: 1em">
                                                        表示
                                                    </th>
                                                    <th style="background : #FFFFFF">
                                                        <div id="checklist-display_flg" class="display_flg" style="float: left;">';

                                                    if ($check_list[$i]['display_flg'] == '0') {
                                                        echo '
                                                            <label><input type="radio" name="Question[' . $i . '][display_flg]" value="0" checked> 表示する</label>
                                                            <label><input type="radio" name="Question[' . $i . '][display_flg]" value="1"> 表示しない</label>
                                                            ';
                                                        } else {
                                                            echo '
                                                            <label><input type="radio" name="Question[' . $i . '][display_flg]" value="0"> 表示する</label>
                                                            <label><input type="radio" name="Question[' . $i . '][display_flg]" value="1" checked> 表示しない</label>
                                                            ';
                                                    }
                                                    echo '</div>
                                                    </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td class="bg_table_confirm_booking font_label">内容</td>
                                                    <td colspan="3" style="text-align: left" name="Checklist[content_table][' . $i . ']" >';
                                                    echo '<div class="col-md-12"> <div class="col-md-12"><div class="col-md-1"></div><div class="msg_error col-md-8" style="color: #dd4b39;margin-bottom: 1em;" name="Error[' . $i . ']" ></div></div></div>';
                                                    echo '<div class="col-md-12">
                                                            <div class="col-md-1">
                                                            </div>
                                                            <div class="col-md-4 option_store_st">
                                                                <select class="form-control optiont_select" name="Question[' . $i . '][option]" key="' . $i . '">
                                                                    <option value="1" selected>一つのみ選択</option>
                                                                    <option value="2">複数選択</option>
                                                                    <option value="3">フリー入力</option>
                                                                </select>
                                                            </div>
                                                            <div name="CountAnswer[' . $i . ']">

                                                            </div>

                                                        </div>
                                                        <div class="col-md-12">
                                                            <div class="col-md-1 font_label" style="padding-left: 2.2em;margin-top: 1em;">
                                                                Q
                                                            </div>
                                                            <div class="col-md-11 question_content_store_ct" style="margin-bottom: 0.5em;">
                                                               <input type="text" class="form-control ip_question" name="Question[' . $i . '][question_content]" maxlength="200" key="' . $i . '" style="margin-top: 0.5em;" value="' . $check_list[$i]['question_content'] . '"> 
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12 list_answer" name="ListAnswer[' . $i . ']">';
                                                                $value_answer_content = $check_list[$i]['answer_content'];                                                               
                                                                $count_answer_sub = count($value_answer_content);
                                                                
                                                                if(($count_answer_sub % 2) != 0){
                                                                    for ($j = 0; $j < $count_answer_sub + 1; $j++) {
                                                                            if($j < $count_answer_sub){
                                                                                    echo '<div class="col-md-6" style="margin-bottom: 1em;">
                                                                                          <div class="col-md-1 label-margin">A' . ($j + 1) . '</div>
                                                                                          <div class="col-md-10 answer_store_ct_10">
                                                                                             <input maxlength="100"  type="text" class="form-control ip_answer" name="Question[' . $i . '][answer][' . $j . ']" key = "' . $i . '" data_bin= "' . $i . '" data-position=' . ($j + 1) . ' value="' . $value_answer_content[$j] . '">
                                                                                          </div>
                                                                                        </div>';
                                                                            }else {
                                                                                echo '<div class="col-md-6" style="margin-bottom: 1em;">
                                                                                            <div class="col-md-1 label-margin">A' . ($j + 1)  . '</div>
                                                                                            <div class="col-md-10 answer_store_ct_10">
                                                                                               <input maxlength="100"  type="text" class="form-control ip_answer" name="Question[' . $i . '][answer][' . $j . ']" key = "' . $i . '" data_bin= "' . $i . '" data-position=' . ($j + 1) . ' ">
                                                                                            </div>
                                                                                          </div>';
                                                                            }  
                                                                        }
                                                                } else {
                                                                    for ($j = 0; $j < $count_answer_sub ; $j++) {
                                                                        echo '<div class="col-md-6" style="margin-bottom: 1em;">
                                                                                    <div class="col-md-1 label-margin">A' . ($j + 1) . '</div>
                                                                                    <div class="col-md-10 answer_store_ct_10">
                                                                                       <input maxlength="100"  type="text" class="form-control ip_answer" name="Question[' . $i . '][answer][' . $j . ']" key = "' . $i . '" data_bin= "' . $i . '" data-position=' . ($j + 1) . ' value="' . $value_answer_content[$j] . '">
                                                                                    </div>
                                                                               </div>';
                                                                    }
                                                                }
                                                                echo '</div>';
                                                                echo '</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                    </div>';
                } else if ($check_list[$i]['option'] == '2') {

                    echo '<div class="store-form">
                                <div class="col-md-9 table_question" id="clone_table">
                                    <div id="clonedInput" class="clonedInput" name="clonedInput' . $i . '" key="' . $i . '">
                                        <input type="hidden" class="id_checklist" name="Question[' . $i . '][id_checklist]" value="' . $check_list[$i]['id_checklist'] . '" key="'.$i.'">
                                        <input type="hidden" class="id_question" name="Question[' . $i . '][id_question]" value="' . $check_list[$i]['id_question'] . '" key="'.$i.'">
                                        <input type="hidden" class="id_answer" name="Question[' . $i . '][id_answer]" value="' . $check_list[$i]['id_answer'] . '" key="'.$i.'">
                                        <input type="hidden" class="id_type_db" name="Question[' . $i . '][id_type_db]" value="' . $check_list[$i]['type'] . '" key="'.$i.'">
                                        <input type="hidden" class="id_option_db" name="Question[' . $i . '][option]" value="' . $check_list[$i]['option'] . '" key="'.$i.'">
                                        <table class="table table-bordered">
                                            <thead>
                                                <tr>
                                                    <th class="bg_table_confirm_booking" style="width: 8em;padding-bottom: 1em">
                                                        タイプ
                                                    </th>' .
                                                    '<th style="width: 20em;background : #FFFFFF">
                                                        <select id="checklist-type-0" class="form-control type_question" name="Question[' . $i . '][type]" key="' . $i . '">
                                                            <option value="1" >注意事項</option>
                                                            <option value="2" selected>質問事項</option>
                                                        </select>
                                                    </th>
                                                    <th class="bg_table_confirm_booking" style="width: 5em;padding-bottom: 1em">
                                                        表示
                                                    </th>
                                                    <th style="background : #FFFFFF">
                                                        <div id="checklist-display_flg" class="display_flg" style="float: left;">';

                                                    if ($check_list[$i]['display_flg'] == '0') {
                                                        echo '
                                                            <label><input type="radio" name="Question[' . $i . '][display_flg]" value="0" checked> 表示する</label>
                                                            <label><input type="radio" name="Question[' . $i . '][display_flg]" value="1"> 表示しない</label>
                                                            ';
                                                    } else {
                                                        echo '
                                                            <label><input type="radio" name="Question[' . $i . '][display_flg]" value="0"> 表示する</label>
                                                            <label><input type="radio" name="Question[' . $i . '][display_flg]" value="1" checked> 表示しない</label>
                                                            ';
                                                    }
                                                    echo '</div>
                                                    </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td class="bg_table_confirm_booking font_label">内容</td>
                                                    <td colspan="3" style="text-align: left" name="Checklist[content_table][' . $i . ']" >';
                                                    echo '<div class="col-md-12"> <div class="col-md-12"><div class="col-md-1"></div><div class="msg_error col-md-8" style="color: #dd4b39;margin-bottom: 1em;" name="Error[' . $i . ']" ></div></div></div>';
                                                    echo '<div class="col-md-12">
                                                            <div class="col-md-1">
                                                            </div>
                                                            <div class="col-md-4 option_store_ct_sl2">
                                                                <select class="form-control optiont_select" name="Question[' . $i . '][option]" key="' . $i . '">
                                                                    <option value="1">一つのみ選択</option>
                                                                    <option value="2" selected>複数選択</option>
                                                                    <option value="3">フリー入力</option>
                                                                </select>
                                                            </div>
                                                            <div name="CountAnswer[' . $i . ']">';
                                                    echo '<div class="col-md-3 label-margin" style="padding-left: 4.0em;">最大回答数</div>
                                                                        <div class="col-md-3">
                                                                        <input style="text-align: right;" onkeypress="return isNumeric(event)" oninput="maxLengthCheck(this)" type = "number" maxlength = "2" min = "1" max = "99" class="form-control" id="count_answer" name="Question[' . $i . '][CountAsw]" key = "' . $i . '" value="' . $check_list[$i]['max_choice'] . '">
                                                                    </div>';
                                                    echo '</div>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <div class="col-md-1 font_label" style="padding-left: 2.2em;margin-top: 1em;">
                                                                Q
                                                            </div>
                                                            <div class="col-md-11 question_content_store_ct" style="margin-bottom: 0.5em;">
                                                               <input type="text" class="form-control ip_question" name="Question[' . $i . '][question_content]" maxlength="200" key="' . $i . '" style="margin-top: 0.5em;" value="' . $check_list[$i]['question_content'] . '"> 
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12 list_answer" name="ListAnswer[' . $i . ']">';
                                                                $value_answer_content = $check_list[$i]['answer_content'];                                                               
                                                                $count_answer_sub = count($value_answer_content);
                                                                
                                                                if(($count_answer_sub % 2) != 0){
                                                                    for ($j = 0; $j < $count_answer_sub + 1; $j++) {
                                                                            if($j < $count_answer_sub){
                                                                                    echo '<div class="col-md-6" style="margin-bottom: 1em;">
                                                                                          <div class="col-md-1 label-margin">A' . ($j + 1) . '</div>
                                                                                          <div class="col-md-10 answer_store_ct_10">
                                                                                             <input maxlength="100"  type="text" class="form-control ip_answer" name="Question[' . $i . '][answer][' . $j . ']" key = "' . $i . '" data_bin= "' . $i . '" data-position=' . ($j + 1) . ' value="' . $value_answer_content[$j] . '">
                                                                                          </div>
                                                                                        </div>';
                                                                            }else {
                                                                                echo '<div class="col-md-6" style="margin-bottom: 1em;">
                                                                                            <div class="col-md-1 label-margin">A' . ($j + 1)  . '</div>
                                                                                            <div class="col-md-10 answer_store_ct_10">
                                                                                               <input maxlength="100"  type="text" class="form-control ip_answer" name="Question[' . $i . '][answer][' . $j . ']" key = "' . $i . '" data_bin= "' . $i . '" data-position=' . ($j + 1) . ' ">
                                                                                            </div>
                                                                                          </div>';
                                                                            }  
                                                                        }
                                                                } else {
                                                                    for ($j = 0; $j < $count_answer_sub ; $j++) {
                                                                        echo '<div class="col-md-6" style="margin-bottom: 1em;">
                                                                                    <div class="col-md-1 label-margin">A' . ($j + 1) . '</div>
                                                                                    <div class="col-md-10 answer_store_ct_10">
                                                                                       <input maxlength="100"  type="text" class="form-control ip_answer" name="Question[' . $i . '][answer][' . $j . ']" key = "' . $i . '" data_bin= "' . $i . '" data-position=' . ($j + 1) . ' value="' . $value_answer_content[$j] . '">
                                                                                    </div>
                                                                               </div>';
                                                                    }
                                                                }
                                                                echo '</div>';
                                                                echo '</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                    </div>';
                } else if ($check_list[$i]['option'] == '3') {
                    echo '<div class="store-form">
                                <div class="col-md-9 table_question" id="clone_table">
                                    <div id="clonedInput" class="clonedInput" name="clonedInput' . $i . '" key="' . $i . '">
                                        <input type="hidden" class="id_checklist" name="Question[' . $i . '][id_checklist]" value="' . $check_list[$i]['id_checklist'] . '" key="'.$i.'">
                                        <input type="hidden" class="id_question" name="Question[' . $i . '][id_question]" value="' . $check_list[$i]['id_question'] . '" key="'.$i.'">
                                        <input type="hidden" class="id_answer" name="Question[' . $i . '][id_answer]" value="' . $check_list[$i]['id_answer'] . '" key="'.$i.'">
                                        <input type="hidden" class="id_type_db" name="Question[' . $i . '][id_type_db]" value="' . $check_list[$i]['type'] . '" key="'.$i.'">
                                        <input type="hidden" class="id_option_db" name="Question[' . $i . '][option]" value="' . $check_list[$i]['option'] . '" key="'.$i.'">
                                        <table class="table table-bordered">
                                            <thead>
                                                <tr>
                                                    <th class="bg_table_confirm_booking" style="width: 8em;padding-bottom: 1em">
                                                        タイプ
                                                    </th>' .
                                                    '<th style="width: 20em;background : #FFFFFF">
                                                        <select id="checklist-type-0" class="form-control type_question" name="Question[' . $i . '][type]" key="' . $i . '">
                                                            <option value="1" >注意事項</option>
                                                            <option value="2" selected>質問事項</option>
                                                        </select>
                                                    </th>
                                                    <th class="bg_table_confirm_booking" style="width: 5em;padding-bottom: 1em">
                                                        表示
                                                    </th>
                                                    <th style="background : #FFFFFF">
                                                        <div id="checklist-display_flg" class="display_flg" style="float: left;">';

                                                    if ($check_list[$i]['display_flg'] == '0') {
                                                        echo '
                                                            <label><input type="radio" name="Question[' . $i . '][display_flg]" value="0" checked> 表示する</label>
                                                            <label><input type="radio" name="Question[' . $i . '][display_flg]" value="1"> 表示しない</label>
                                                            ';
                                                    } else {
                                                        echo '
                                                            <label><input type="radio" name="Question[' . $i . '][display_flg]" value="0"> 表示する</label>
                                                            <label><input type="radio" name="Question[' . $i . '][display_flg]" value="1" checked> 表示しない</label>
                                                            ';
                                                    }
                                                    echo '</div>
                                                    </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td class="bg_table_confirm_booking font_label">内容</td>
                                                    <td colspan="3" style="text-align: left" name="Checklist[content_table][' . $i . ']" >';
                                                    echo '<div class="col-md-12">
                                                            <div class="col-md-1">
                                                            </div>
                                                            <div class="col-md-4 option_store_st">
                                                                <select class="form-control optiont_select" name="Question[' . $i . '][option]" key="' . $i . '">
                                                                    <option value="1">一つのみ選択</option>
                                                                    <option value="2">複数選択</option>
                                                                    <option value="3" selected>フリー入力</option>
                                                                </select>
                                                            </div>
                                                            <div name="CountAnswer[' . $i . ']">

                                                            </div>

                                                        </div>
                                                        <div class="col-md-12">
                                                            <div class="col-md-1 font_label" style="padding-left: 2.2em;margin-top: 1em;">
                                                                Q
                                                            </div>
                                                            <div class="col-md-11 question_content_store_ct" style="margin-bottom: 0.5em;">
                                                               <input type="text" class="form-control ip_question" name="Question[' . $i . '][question_content]" maxlength="200" key="' . $i . '" style="margin-top: 0.5em;" value="' . $check_list[$i]['question_content'] . '"> 
                                                            </div>
                                                        </div>';
                                                    echo '</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                    </div>';
                }
            }
        }
       
    }
    ?>
    </div>
    <?php
        echo '<div class="col-md-12">
                                <div class="col-md-8">
                                </div>
                                <div class="col-md-2 add_">
                                   <button type="button" id="clone" class="bt-clone">+追加</button>
                                </div>
                            </div>
                            <!-- Table Confirm Booking End  -->

                            <!-- Button -->
                            <div class="col-md-12" style="margin-left: 15em;">
                               <button type="button" id="back_confirm" class="btn btn-default common-button-default prev-step-back">戻る</button>
                               <button type="button" id="btn-confirm-question" class="btn common-button-submit prev-step">決定</button>
                            </div>
                            <!-- End Button -->';
    ?>
</div>