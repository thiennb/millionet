<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\components\Constants;

$this->registerJsFile('@web/js/confirm_booking.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
?>
<?php
$form = ActiveForm::begin();
?>
<div class="col-md-12">
    <div class="col-md-1">
        
    </div>
    <div class="col-md-4">
        <select class="form-control optiont_select" name="Question[0][option]" key="0">
            <option value="1">一つのみ選択</option>
            <option value="2">複数選択</option>
            <option value="3">フリー入力</option>
        </select>
    </div>
    <div name="CountAnswer[0]">
        
    </div>
   
</div>
<div class="col-md-12">
    <div class="col-md-1 font_label" style="padding-left: 2.2em;    margin-top: 1em;">
        Q
    </div>
    <div class="col-md-11" style="margin-bottom: 0.5em;">
       <input type="text" class="form-control ip_question" name="Question[question_content][0]" maxlength="200" key="0" style="margin-top: 0.5em;"> 
    </div>
</div>
<div class="col-md-12 list_answer" name="ListAnswer[0]">
    
</div>
<?php ActiveForm::end(); ?>