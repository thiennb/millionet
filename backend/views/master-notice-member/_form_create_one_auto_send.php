<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\bootstrap\Modal;
use common\widgets\preview\Preview;
use yii\jui\DatePicker;
use common\components\Constants;

/* @var $this yii\web\View */
/* @var $model common\models\MasterStaff */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="master_notice_form">

    <?php
    $form = ActiveForm::begin([
                'options' => [
                    'enctype' => 'multipart/form-data',
                    'id' => strtolower($model->formName()) . '-id'
                ], // important
                'validationUrl' => 'validate',
                'enableClientValidation' => false,
                'enableAjaxValidation' => true,
                'validateOnChange' => false,
                'validateOnBlur' => false,
                'successCssClass' => '',
                'action' => ['create-auto-send'],
    ]);
    ?>
    <div class="col-md-12" id="list-type_send">
        <div class="col-md-2 label-margin label_preview_one_notice_ct">
            <?= Yii::t('backend', 'Delivery start date time') ?>
        </div>
        <div class="col-md-7">
            <?php $model->type_send = isset($model->type_send) ? $model->type_send : 0 ?>
            <div class="row radio-list">
                <div class="col-md-1">
                    <?= $form->field($model, 'type_send')->radio(['label' => '', 'value' => 0]) ?>
                </div>
                <div class="col-md-11" id="radio_customer_value_0">
                    <div class="col-md-7">
                        <?= Yii::t('backend', 'Immediately') ?>
                    </div>
                </div>
            </div>     
            <div class="row radio-list">
                <div class="col-md-1">
                    <?= $form->field($model, 'type_send')->radio(['label' => '', 'value' => 1]) ?>
                </div>
                <div class="col-md-11" id="radio_customer_value_1">
                    <div class="col-md-7">
                        <?=
                        $form->field($model, 'send_date', [
                            'template' => '{input}{error}'
                        ])->widget(DatePicker::classname(), [
                            'language' => 'ja',
                            'dateFormat' => 'yyyy/MM/dd',
                            'clientOptions' => [
                                "changeMonth" => true,
                                "changeYear" => true,
                                "yearRange" => "1900:+0"
                            ],
                        ])->textInput(['maxLength' => true])
                        ?>
                    </div>
                    <div class="col-md-5">
                        <?= $form->field($model, 'send_time', ['template' => '{input}{error}'])->dropDownList(Constants::LIST_OPTION_SELECT_TIME, ['maxlength' => true, 'class' => 'form-control add-time-off'])->label(FALSE) ?>
                    </div>
                </div>
            </div>            
        </div>
    </div>

    <div class="col-md-12">
        <div class="col-md-2 label-margin label_preview_one_notice_ct">
            <?= Yii::t('backend', 'Title') ?>
        </div>
        <div class="col-md-7">
            <?= $form->field($model, 'title', ['template' => '{input}{error}'])->textInput(['maxlength' => true])->label(FALSE) ?>
        </div>
    </div>

    <div class="col-md-12">
        <div class="col-md-2 label-margin label_preview_one_notice_ct">
            <?= Yii::t('backend', 'Note') ?>
        </div>
        <div class="col-md-7">
            <?= $form->field($model, 'content', ['template' => '{input}{error}'])->textarea(['maxlength' => true, 'rows' => '6'])->label(FALSE) ?>
        </div>

    </div>
    <div class="form-group"></div>

    <div class="col-md-12">
        <div class="col-md-2 label-margin"></div>
        <div class="col-md-7 clear-padding setting_auto_notice_ct">
            <?= Html::a(Yii::t('backend', Yii::t('backend', 'Return')), ['setting-auto-send'], ['class' => 'btn btn-default common-button-default']) ?>
            <?=
            Html::submitButton(Yii::t('backend', 'Confirm'), [
                'class' => 'btn common-button-submit',
                'id' => 'btn-confirm',
            ])
            ?>
        </div>
    </div>
    <?= $form->field($setting_auto_send, 'hour_after_register_member')->hiddenInput()->label(false); ?>
    <?= $form->field($setting_auto_send, 'hour_after_accounting')->hiddenInput()->label(false); ?>
    <?= $form->field($setting_auto_send, 'distance_to_shop')->hiddenInput()->label(false); ?>
    <?= $form->field($setting_auto_send, 'push_timming')->hiddenInput()->label(false); ?>
    <?= $form->field($setting_auto_send, 'hour_before_booking')->hiddenInput()->label(false); ?>
    <?= $form->field($setting_auto_send, 'customer_jan_code')->hiddenInput()->label(false) ?>
    <?= $form->field($setting_auto_send, 'first_name')->hiddenInput()->label(false) ?>
    <?= $form->field($setting_auto_send, 'last_name')->hiddenInput()->label(false) ?>
    <?= $form->field($setting_auto_send, 'first_name_kana')->hiddenInput()->label(false) ?>
    <?= $form->field($setting_auto_send, 'last_name_kana')->hiddenInput()->label(false) ?>
    <?= $form->field($setting_auto_send, 'sex')->hiddenInput()->label(false) ?>
    <?php
    echo $form->field($setting_auto_send, 'start_birth_date')->hiddenInput()->label(false);
    echo $form->field($setting_auto_send, 'end_birth_date')->hiddenInput()->label(false);
    ?>
    <?php
    echo $form->field($setting_auto_send, 'start_visit_date')->hiddenInput()->label(false);
    echo $form->field($setting_auto_send, 'end_visit_date')->hiddenInput()->label(false);
    ?>
    <?php
    echo $form->field($setting_auto_send, 'min_visit_number')->hiddenInput()->label(false);
    echo $form->field($setting_auto_send, 'max_visit_number')->hiddenInput()->label(false);
    ?>
    <?= $form->field($setting_auto_send, 'store_id')->hiddenInput()->label(false) ?>
    <?= $form->field($setting_auto_send, 'rank_id')->hiddenInput()->label(false) ?>
    <?= $form->field($setting_auto_send, 'last_staff_id')->hiddenInput()->label(false) ?>
    <?= $form->field($setting_auto_send, 'black_list_flg')->hiddenInput()->label(false) ?>
    <div class="col-md-12">
        <?php
        Modal::begin([
            'id' => 'userModal',
            'size' => 'SIZE_LARGE',
            'header' => '<div class="box-header with-border common-box-h4 col-md-8"><h4 class="text-title"><b>' . Yii::t('backend', 'Confirm Notice') . '</b></h4></div>',
            'footer' => Html::button(Yii::t('backend', 'Close'), ['class' => 'btn common-button-default', 'data-dismiss' => "modal", 'id' => 'btn-close'])
            . Html::submitButton(Yii::t('backend', 'Save'), ['class' => 'btn common-button-submit', 'id' => 'btn-submit']),
            'closeButton' => FALSE,
        ]);
        ?>
        <div class="form-horizontal">
            <div class="form-group" > 
                <label class="col-sm-4"> 
                    <?= Yii::t('backend', Yii::t('backend', 'Deliver Time')) ?>
                </label>
                <div class="col-sm-8" > 
                    <span>
                        <?php
                        switch ($setting_auto_send->push_timming) {
                            case Constants::PUSH_TIMMING_AT_BOOKING:
                                echo Yii::t('backend', 'When booking');
                                break;
                            case Constants::PUSH_TIMMING_BEFORE_BOOKING:
                                echo Yii::t('backend', 'Booking') . $setting_auto_send->hour_before_booking . Yii::t('backend', 'Time ago');
                                break;
                            case Constants::PUSH_TIMMING_AT_REGISTER:
                                echo Yii::t('backend', 'When register member');
                                break;
                            case Constants::PUSH_TIMMING_AFTER_REGISTER:
                                echo Yii::t('backend', 'From member registration') . $setting_auto_send->hour_after_register_member . Yii::t('backend', 'Time');
                                break;
                            case Constants::PUSH_TIMMING_AFTER_CHARGE:
                                echo Yii::t('backend', 'From accounting') . $setting_auto_send->hour_after_accounting . Yii::t('backend', 'Time');
                                break;
                            case Constants::PUSH_TIMMING_NEAR_STORE:
                                echo Yii::t('backend', 'From the store') . $setting_auto_send->distance_to_shop . Yii::t('backend', 'When approach within meters');
                                break;
                        }
                        ?>

                    </span>

                </div>
            </div>
            <div class="form-group" > 
                <label class="col-sm-4"> 
                    <?= Yii::t('backend', Yii::t('backend', "Membership card number")) ?>
                </label>
                <div class="col-sm-8" > 
                    <span>
                        <?= $setting_auto_send->customer_jan_code ?>
                    </span>

                </div>
            </div>

            <div class="form-group" > 
                <label class="col-sm-4"> 
                    <?= Yii::t('backend', Yii::t('backend', 'Name')) ?>
                </label>
                <div class="col-sm-8" > 
                    <span>
                        <?= $setting_auto_send->first_name . $setting_auto_send->last_name ?>
                    </span>

                </div>
            </div>

            <div class="form-group" > 
                <label class="col-sm-4"> 
                    <?= Yii::t('backend', Yii::t('backend', 'Name kana')) ?>
                </label>
                <div class="col-sm-8" > 
                    <span>
                        <?= $setting_auto_send->first_name_kana . $setting_auto_send->last_name_kana ?>
                    </span>

                </div>
            </div>
            <div class="form-group" > 
                <label class="col-sm-4"> 
                    <?= Yii::t('backend', Yii::t('backend', 'Sex')) ?>
                </label>
                <div class="col-sm-8" > 
                    <span>
                        <?php
                        $sex = common\components\Constants::LIST_SEX;
                        echo isset($sex[$setting_auto_send->sex]) ? $sex[$setting_auto_send->sex] : "";
                        ?>
                    </span>

                </div>
            </div>
            <div class="form-group" > 
                <label class="col-sm-4"> 
                    <?= Yii::t('backend', Yii::t('backend', 'Birthday')) ?>
                </label>
                <div class="col-sm-8" > 
                    <span>
                        <?php
                        if (!empty($setting_auto_send->start_birth_date) || !empty($setting_auto_send->end_birth_date)) {
                            echo empty($setting_auto_send->start_birth_date) ? "" : $setting_auto_send->start_birth_date;
                            echo "〜";
                            echo empty($setting_auto_send->end_birth_date) ? "" : $setting_auto_send->end_birth_date;
                        }
                        ?>
                    </span>

                </div>
            </div>
            <div class="form-group" > 
                <label class="col-sm-4"> 
                    <?= Yii::t('backend', Yii::t('backend', 'Last visit time')) ?>
                </label>
                <div class="col-sm-8" > 
                    <span>
                        <?php
                        if (!empty($setting_auto_send->start_visit_date) || !empty($setting_auto_send->end_visit_date)) {
                            echo empty($setting_auto_send->start_visit_date) ? "" : $setting_auto_send->start_visit_date;
                            echo "〜";
                            echo empty($setting_auto_send->end_visit_date) ? "" : $setting_auto_send->end_visit_date;
                        }
                        ?>
                    </span>

                </div>
            </div>
            <div class="form-group" > 
                <label class="col-sm-4"> 
                    <?= Yii::t('backend', Yii::t('backend', 'Visit number')) ?>
                </label>
                <div class="col-sm-8" > 
                    <span>
                        <?php
                        if (!empty($setting_auto_send->min_visit_number) || !empty($setting_auto_send->max_visit_number)) {
                            echo empty($setting_auto_send->min_visit_number) ? "" : $setting_auto_send->min_visit_number;
                            echo "〜";
                            echo empty($setting_auto_send->max_visit_number) ? "" : $setting_auto_send->max_visit_number;
                        }
                        ?>
                    </span>

                </div>
            </div>
            <div class="form-group" > 
                <label class="col-sm-4"> 
                    <?= Yii::t('backend', Yii::t('backend', 'Distributing Store')) ?>
                </label>
                <div class="col-sm-8" > 
                    <span>
                        <?= isset($setting_auto_send->store) ? $setting_auto_send->store->name : "" ?>
                    </span>

                </div>
            </div>
            <div class="form-group" > 
                <label class="col-sm-4"> 
                    <?= Yii::t('backend', Yii::t('backend', 'Customers rank')) ?>
                </label>
                <div class="col-sm-8" > 
                    <span>
                        <?php
                        $rank = common\components\Constants::RANK;
                        echo isset($rank[$setting_auto_send->rank_id]) ? $rank[$setting_auto_send->rank_id] : "";
                        ?>
                    </span>

                </div>
            </div>
            <div class="form-group" > 
                <label class="col-sm-4"> 
                    <?= Yii::t('backend', Yii::t('backend', 'Last time staff')) ?>
                </label>
                <div class="col-sm-8" > 
                    <span>
                        <?= isset($setting_auto_send->staff) ? $setting_auto_send->staff->name : "" ?>
                    </span>

                </div>
            </div>
            <?php if ($setting_auto_send->black_list_flg) {
                ?>
                <div class="form-group" > 
                    <label> 
                        <?= Yii::t('backend', Yii::t('backend', 'black_list')) ?>
                    </label>
                </div>
            <?php } ?>

        </div>

        <?=
            Preview::widget([
            "data" => [
                'type_send' => [
                    'type' => 'radio_customer',
                    'label' => Yii::t('backend', Yii::t('backend', 'Delivery start date time'))
                ],
                'title' => [
                    'type' => 'input',
                    'label' => Yii::t('backend', Yii::t('backend', 'Title'))
                ],
                'content' => [
                    'type' => 'input',
                    'area' => 'area',
                    'label' => Yii::t('backend', Yii::t('backend', 'Note'))
                ],
            ],
            "modelName" => $model->formName(),
            'idBtnConfirm' => 'btn-confirm',
            'formId' => strtolower($model->formName()) . '-id',
            'btnClose' => 'btn-close',
            'btnSubmit' => 'btn-submit',
            'modalId' => 'userModal'
        ])
        ?>
        <?php
        Modal::end();
        ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<script>
    $("input[name='<?= Html::getInputName($model, 'type_send') ?>']").on("change", function () {
        $("#list-type_send").find(':input:not([type="radio"])').prop('disabled', true).val('');
        $(this).parents(".row.radio-list").find(':input:not([type="radio"])').prop('disabled', false);
    })
    $(document).ready(function () {
        $("#list-type_send").find(':input:not([type="radio"])').prop('disabled', true);
        $(':input[type="radio"]:checked').parents(".row.radio-list").find(':input:not([type="radio"])').prop('disabled', false);
    });
</script>