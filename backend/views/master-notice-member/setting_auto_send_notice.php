<?php

use dmstr\widgets\Alert;
/* @var $this yii\web\View */
/* @var $searchModel common\models\MasterNoticeMemberSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('backend', 'Notice');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <div class="col-lg-12 col-md-12">
        <div class="box box-info box-solid">
            <div class="box-header with-border">
                <h4 class="text-title"><b><?= $this->title ?></b></h4>
            </div>
            <div class="box-body content">
                <div class="col-md-12">
                    <div class="common-box">
                        <div class="row">
                            <div class="box-header with-border common-box-h4 col-lg-8 col-md-12">
                                <h4 class="text-title"><b><?= Yii::t('backend', 'Condition auto send notice') ?></b></h4>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group"><?= Alert::widget() ?></div>
                        </div>
                        <div class="box-body content">
                            <div class="col-md-12">
                                <?php echo $this->render('_form_setting_auto_send_notice', ['model' => $model]); ?>                                
                            </div>  
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>