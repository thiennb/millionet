<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\bootstrap\Modal;
use common\widgets\preview\Preview;
use yii\jui\DatePicker;
use common\components\Constants;

/* @var $this yii\web\View */
/* @var $model common\models\MasterStaff */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="master_notice_form">

    <?php
    $form = ActiveForm::begin([
                'options' => [
                    'enctype' => 'multipart/form-data',
                    'id' => strtolower($model->formName()) . '-id'
                ], // important
                'validationUrl' => 'validate',
                'enableClientValidation' => false,
                'enableAjaxValidation' => true,
                'validateOnChange' => false,
                'validateOnBlur' => false,
                'successCssClass' => '',
    ]);
    ?>
    <div class="col-md-12" id="list-type_send">
        <div class="col-md-2 label-margin label_preview_one_notice_ct">
            <?= Yii::t('backend', 'Delivery start date time') ?>
        </div>
        <div class="col-md-7">
            <?php $model->type_send = !empty($model->send_date) ? 1 : 0 ?>
            <div class="row radio-list">
                <div class="col-md-1">
                    <?= $form->field($model, 'type_send')->radio(['label' => '', 'value' => 0]) ?>
                </div>
                <div class="col-md-11" id="radio_customer_value_0">
                    <div class="col-md-7">
                        <?= Yii::t('backend', 'Immediately') ?>
                    </div>
                </div>
            </div>     
            <div class="row radio-list">
                <div class="col-md-1">
                    <?= $form->field($model, 'type_send')->radio(['label' => '', 'value' => 1]) ?>
                </div>
                <div class="col-md-11" id="radio_customer_value_1">
                    <div class="col-md-7">
                        <?=
                        $form->field($model, 'send_date', [
                            'template' => '{input}{error}'
                        ])->widget(DatePicker::classname(), [
                            'language' => 'ja',
                            'dateFormat' => 'yyyy/MM/dd',
                            'clientOptions' => [
                                "changeMonth" => true,
                                "changeYear" => true,
                                "yearRange" => "1900:+0"
                            ],
                        ])->textInput(['maxLength' => true])
                        ?>
                    </div>
                    <div class="col-md-5">
                        <?= $form->field($model, 'send_time', ['template' => '{input}{error}'])->dropDownList(Constants::LIST_OPTION_SELECT_TIME, ['maxlength' => true, 'class' => 'form-control add-time-off'])->label(FALSE) ?>
                    </div>
                </div>
            </div>            
        </div>
    </div>

    <div class="col-md-12">
        <div class="col-md-2 label-margin label_preview_one_notice_ct">
            <?= Yii::t('backend', 'Title') ?>
        </div>
        <div class="col-md-7">
            <?= $form->field($model, 'title', ['template' => '{input}{error}'])->textInput(['maxlength' => true])->label(FALSE) ?>
        </div>
    </div>

    <div class="col-md-12">
        <div class="col-md-2 label-margin label_preview_one_notice_ct">
            <?= Yii::t('backend', 'Note') ?>
        </div>
        <div class="col-md-7">
            <?= $form->field($model, 'content', ['template' => '{input}{error}'])->textarea(['maxlength' => true, 'rows' => '6', 'style' => 'overflow-y: auto;'])->label(FALSE) ?>
        </div>
    </div>

    <div class="form-group"></div>

    <div class="col-md-12">
        <div class="col-md-2 label-margin "></div>
        <div class="col-md-7 setting_auto_notice_ct">
            <?= Html::a(Yii::t('backend', Yii::t('backend', 'Return')), ['list-plan'], ['class' => 'btn btn-default common-button-default']) ?>
            <?=
            Html::submitButton(Yii::t('backend', 'Confirm'), [
                'class' => 'btn common-button-submit',
                'id' => 'btn-confirm',
            ])
            ?>
        </div>
    </div>
    <div class="col-md-12">
        <?php
        Modal::begin([
            'id' => 'userModal',
            'size' => 'SIZE_LARGE',
            'header' => '<div class="box-header with-border common-box-h4 col-md-8"><h4 class="text-title"><b>' . Yii::t('backend', 'Confirm Notice') . '</b></h4></div>',
            'footer' => Html::button(Yii::t('backend', 'Close'), ['class' => 'btn common-button-default', 'data-dismiss' => "modal", 'id' => 'btn-close'])
            . Html::submitButton(Yii::t('backend', 'Save'), ['class' => 'btn common-button-submit', 'id' => 'btn-submit']),
            'closeButton' => FALSE,
        ]);
        ?>
        <?=
        Preview::widget([
            "data" => [
                'type_send' => [
                    'type' => 'radio_customer',
                    'label' => Yii::t('backend', Yii::t('backend', 'Delivery start date time'))
                ],
                'title' => [
                    'type' => 'input',
                    'label' => Yii::t('backend', Yii::t('backend', 'Title'))
                ],
                'content' => [
                    'type' => 'input',
                    'area' => 'area',
                    'label' => Yii::t('backend', Yii::t('backend', 'Note'))
                ],
            ],
            "modelName" => $model->formName(),
            'idBtnConfirm' => 'btn-confirm',
            'formId' => strtolower($model->formName()) . '-id',
            'btnClose' => 'btn-close',
            'btnSubmit' => 'btn-submit',
            'modalId' => 'userModal'
        ])
        ?>
        <?php
        Modal::end();
        ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<script>
    $("input[name='<?= Html::getInputName($model, 'type_send') ?>']").on("change", function () {
        $("#list-type_send").find(':input:not([type="radio"])').prop('disabled', true).val('');
        $(this).parents(".row.radio-list").find(':input:not([type="radio"])').prop('disabled', false);
        $('#<?=strtolower($model->formName()) . '-id'?>').yiiActiveForm('updateAttribute', '<?= Html::getInputId($model, 'send_date')?>', '');
        $('#<?=strtolower($model->formName()) . '-id'?>').yiiActiveForm('updateAttribute', '<?= Html::getInputId($model, 'send_time')?>', '');
    })
    $(document).ready(function () {
        $("#list-type_send").find(':input:not([type="radio"])').prop('disabled', true);
        
        $(':input[type="radio"]:checked').parents(".row.radio-list").find(':input:not([type="radio"])').prop('disabled', false);
    });
</script>