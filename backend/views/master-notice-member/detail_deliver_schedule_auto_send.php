<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use dmstr\widgets\Alert;
use common\components\Constants;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $searchModel common\models\MasterNoticeMemberSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('backend', 'Notice Detail');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <div class="col-lg-12 col-md-12">
        <div class="box box-info box-solid">
            <div class="box-header with-border">
                <h4 class="text-title"><b><?= $this->title ?></b></h4>
            </div>
            <div class="box-body">
                <div class="col-md-12">
                    <div class="common-box">
                        <div class="row">
                            <div class="box-header with-border common-box-h4 col-lg-8 col-md-10">
                                <h4 class="text-title"><b><?= Yii::t('backend', 'Notice Detail') ?></b></h4>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group"><?= Alert::widget() ?></div>
                        </div>
                        <div class="row">
                            <div class="box-body">
                                <div class="col-md-6">
                                    <div class="row form-group">
                                        <div class="col-md-4 label_preview_one_notice_ct"><label><?= Yii::t('backend', 'Delivery start date') ?></label></div>
                                        <div class="col-md-8 input_notice_ct"><?= empty($model->send_date) ? "" : date('Y/m/d', strtotime($model->send_date)) ?></div>
                                    </div>
                                    <div class="row form-group">
                                        <div class="col-md-4 label_preview_one_notice_ct"><label><?= Yii::t('backend', 'Title') ?></label></div>
                                        <div class="col-md-8 input_notice_ct"><?= $model->title ?></div>
                                    </div>
                                    <div class="row form-group">
                                        <div class="col-md-4 label_preview_one_notice_ct"><label><?= Yii::t('backend', 'Note') ?></label></div>
                                        <div class="col-md-8 input_notice_ct">
                                            <p style="white-space:pre-wrap;"> <?= Html::encode(trim($model->content) ) ?></p>
                                        </div>
                                    </div>
                                </div>  
                            </div>
                        </div>
                    </div>
                    <div class="common-box">
                        <div class="row">
                            <div class="box-header with-border common-box-h4 col-lg-8 col-md-10">
                                <h4 class="text-title"><b><?= Yii::t('backend', 'Conditions of distribution') ?></b></h4>
                            </div>
                        </div>
                        <div class="row">
                            <div class="box-body content">
                                <div class="col-md-6">

                                    <div class="row form-group">
                                        <div class="col-md-4 label_preview_one_notice_ct"><label><?= Yii::t('backend', 'Deliver Time') ?></label></div>
                                        <div class="col-md-8 input_notice_ct">
                                            <?php if (!empty($setting)) { ?> 
                                                <?php if ($setting->push_timming == Constants::PUSH_TIMMING_AT_BOOKING) { ?>
                                                    <?= Yii::t('backend', 'When booking') ?>
                                                <?php } else if ($setting->push_timming == Constants::PUSH_TIMMING_BEFORE_BOOKING) { ?>
                                                    <?= Yii::t('backend', 'Booking') ?><?= $setting->hour_before_booking ?><?= Yii::t('backend', 'Time ago') ?>
                                                <?php } else if ($setting->push_timming == Constants::PUSH_TIMMING_AT_REGISTER) { ?>
                                                    <?= Yii::t('backend', 'When register member') ?>
                                                <?php } else if ($setting->push_timming == Constants::PUSH_TIMMING_AFTER_REGISTER) { ?>
                                                    <?= Yii::t('backend', 'From member registration') ?><?= $setting->hour_after_register_member ?><?= Yii::t('backend', 'Time') ?>
                                                <?php } else if ($setting->push_timming == Constants::PUSH_TIMMING_AFTER_CHARGE) { ?>
                                                    <?= Yii::t('backend', 'From accounting') ?><?= $setting->hour_after_accounting ?><?= Yii::t('backend', 'Time') ?>
                                                <?php } else if ($setting->push_timming == Constants::PUSH_TIMMING_NEAR_STORE) { ?>
                                                    <?= Yii::t('backend', 'From the store') ?><?= empty($model->distance_to_shop)?'':Constants::DISTANCE_TO_SHOP[$setting->distance_to_shop] ?><?= Yii::t('backend', 'When approach within meters') ?>
                                                    <?php                                                    
                                                }
                                            }
                                            ?>

                                        </div>
                                    </div>
                                    <div class="row form-group">
                                        <div class="col-md-4 label_preview_one_notice_ct"><label><?= Yii::t('backend', 'Membership card number') ?></label></div>
                                        <div class="col-md-8 input_notice_ct"><?= Html::encode($model->customer_jan_code) ?></div>
                                    </div>
                                    <div class="row form-group">
                                        <div class="col-md-4 label_preview_one_notice_ct"><label><?= Yii::t('backend', 'Name Coupon') ?></label></div>
                                        <div class="col-md-8 input_notice_ct"><?= Html::encode($model->first_name . $model->last_name) ?></div>
                                    </div>
                                    <div class="row form-group">
                                        <div class="col-md-4 label_preview_one_notice_ct"><label><?= Yii::t('backend', 'Sex') ?></label></div>
                                        <div class="col-md-8 input_notice_ct"><?= Html::encode(empty($model->sex)?'':Constants::LIST_SEX[$model->sex]) ?></div>
                                    </div>
                                    <div class="row form-group">
                                        <div class="col-md-4 label_preview_one_notice_ct"><label><?= Yii::t('backend', 'Birthday From') ?></label></div>
                                        <div class="col-md-8 input_notice_ct"><?= Html::encode(date('Y/m/d', strtotime($model->birth_date))) ?></div>
                                    </div>
                                    <div class="row form-group">
                                        <div class="col-md-4 label_preview_one_notice_ct"><label><?= Yii::t('backend', 'Last Visit Date From') ?></label></div>
                                        <div class="col-md-8 input_notice_ct"><?= Html::encode(empty($model->last_visit_date) ? "" : date('Y/m/d', strtotime($model->last_visit_date))) ?></div>
                                    </div>
                                    <div class="row form-group">
                                        <div class="col-md-4 label_preview_one_notice_ct"><label><?= Yii::t('backend', 'Visit number') ?></label></div>
                                        <div class="col-md-8 input_notice_ct"><?= Html::encode($model->number_visit) ?></div>
                                    </div>
                                    <div class="row form-group">
                                        <div class="col-md-4 label_preview_one_notice_ct"><label><?= Yii::t('backend', 'Distributing Store') ?></label></div>
                                        <div class="col-md-8 input_notice_ct"><?= Html::encode($model->store_name) ?></div>
                                    </div>
                                    <div class="row form-group">
                                        <div class="col-md-4 label_preview_one_notice_ct"><label><?= Yii::t('backend', 'Rank Customer') ?></label></div>
                                        <div class="col-md-8 input_notice_ct"><?= Html::encode(empty($model->rank)?'':Constants::RANK[$model->rank]) ?></div>
                                    </div>
                                    <div class="row form-group">
                                        <div class="col-md-4 label_preview_one_notice_ct"><label><?= Yii::t('backend', 'Last time staff') ?></label></div>
                                        <div class="col-md-8 input_notice_ct"><?= Html::encode($model->last_staff_name) ?></div>
                                    </div>
                                    <?php
                                    if (!empty($setting)) {
                                        if ($setting->black_list_flg == '1') {
                                            ?> 
                                            <div class="row">
                                                <div class="col-md-12"><label><?= Yii::t('backend', 'Black List Cuopon') ?></label></div>
                                            </div>
                                            <?php
                                        }
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row text-center">
                        <div class="col-md-6">
                            <?php $form = ActiveForm::begin(); ?>
                            <?= Html::a(Yii::t('backend', 'Return'), ['list-auto-send'], ['class' => 'btn btn-default common-button-default']) ?>
                            <?php
                            if ($status == \common\models\MasterNotice::STATUS_SEND_DOING) {
                                echo Html::submitButton(Yii::t('backend', Yii::t('backend', 'Stop Delivery')), [
                                    'class' => 'btn common-button-submit',
                                    'id' => 'btn-confirm',
                                ]);
                            }
                            ?>
                            <?php ActiveForm::end(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>