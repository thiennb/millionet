<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use common\components\Constants;
use yii\jui\DatePicker;
use common\models\MasterStore;
use kartik\depdrop\DepDrop;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model common\models\MasterNoticeMemberSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<?php
    $form = ActiveForm::begin([
                'enableClientValidation' => FALSE,
                'action' => ['setting-plan'],
                'method' => 'get',
    ]);
?>
<div class="master-notice-member-search col-md-8">    
    <div class="col-md-12">
        <div class="col-lg-3 col-md-4 label-margin label_preview_booking_ct">
            <?= Yii::t('backend', 'Membership card number') ?>
        </div>
        <div class="col-md-3 input_store_ct">
            <?=
            $form->field($model, 'customer_jan_code', [
                'template' => "{input}{error}"
            ])->widget(\yii\widgets\MaskedInput::className(), [
            'mask' => '9',
            'clientOptions' => ['repeat' => 13, 'greedy' => false]])->textInput(['maxlength' => true])->label(FALSE)
            ?>
        </div>
    </div>

    <div class="col-md-12">
        <div class="col-lg-3 col-md-4 label-margin label_preview_booking_ct">
            <?= Yii::t('backend', 'Name') ?>
        </div>
        <div class="col-md-1 label-margin">
            <?= Yii::t('backend', 'First Name') ?>
        </div>
        <div class="col-md-2">
            <?= $form->field($model, 'first_name')->textInput(['maxlength' => true])->label(false) ?>
        </div>
        <div class="col-md-1 label-margin">
            <?= Yii::t('backend', 'Last Name') ?>
        </div>
        <div class="col-md-2">
            <?= $form->field($model, 'last_name')->textInput(['maxlength' => true])->label(false) ?>
        </div>
    </div>
    <div class="col-md-12">
        <div class="col-lg-3 col-md-4 label-margin first_name_notice_ct">
        </div>
        <div class="col-md-1 label-margin">
            <?= Yii::t('backend', 'First Name Kana') ?>
        </div>
        <div class="col-md-2">
            <?= $form->field($model, 'first_name_kana')->textInput(['maxlength' => true])->label(false) ?>
        </div>
        <div class="col-md-1 label-margin">
            <?= Yii::t('backend', 'Last Name Kana') ?>
        </div>
        <div class="col-md-2">
            <?= $form->field($model, 'last_name_kana')->textInput(['maxlength' => true])->label(false) ?>
        </div>
    </div>

    <div class="col-md-12">
        <div class="col-lg-3 col-md-4 label-margin label_preview_booking_ct">
            <?= Yii::t('backend', 'Sex') ?>
        </div>
        <div class="col-md-3 input_store_ct">
            <?=
            $form->field($model, 'sex')->dropDownList(Constants::LIST_SEX, ['prompt' => Yii::t('backend', 'Please Select')])->label(FALSE)
            ?>
        </div>
    </div>

    <div class="col-md-12">
        <div class="col-lg-3 col-md-4 label-margin label_preview_booking_ct">
            <?= Yii::t('backend', 'Birthday') ?>
        </div>
        <div class="col-md-3 input_store_ct">
            <?=
            $form->field($model, 'start_birth_date')->widget(DatePicker::classname(), [
                'language' => 'ja',
                'dateFormat' => 'yyyy/MM/dd',
                'clientOptions' => [
                    "changeMonth" => true,
                    "changeYear" => true,
                    "yearRange" => "1900:+0"
                ],
            ])->textInput(['maxlength' => 10])->label(false)
            ?>
        </div>
        <div class="col-md-1 label-margin label-center date_ separator">～</div>
        <div class="col-md-3 input_store_ct">
            <?=
            $form->field($model, 'end_birth_date')->widget(DatePicker::classname(), [
                'language' => 'ja',
                'dateFormat' => 'yyyy/MM/dd',
                'clientOptions' => [
                    "changeMonth" => true,
                    "changeYear" => true,
                    "yearRange" => "1900:+0"
                ],
            ])->textInput(['maxlength' => 10])->label(false)
            ?>
        </div>
    </div>

    <div class="col-md-12">
        <div class="col-lg-3 col-md-4 label-margin label_preview_booking_ct">
            <?= Yii::t('backend', 'Last visit time') ?>
        </div>
        <div class="col-md-3 input_store_ct">
            <?=
            $form->field($model, 'start_visit_date')->widget(DatePicker::classname(), [
                'language' => 'ja',
                'dateFormat' => 'yyyy/MM/dd',
                'clientOptions' => [
                    "changeMonth" => true,
                    "changeYear" => true,
                    "yearRange" => "1900:+0"
                ],
            ])->textInput(['maxlength' => 10])->label(false)
            ?>
        </div>
        <div class="col-md-1 label-margin label-center date_ separator">～</div>
        <div class="col-md-3 input_store_ct">
            <?=
            $form->field($model, 'end_visit_date')->widget(DatePicker::classname(), [
                'language' => 'ja',
                'dateFormat' => 'yyyy/MM/dd',
                'clientOptions' => [
                    "changeMonth" => true,
                    "changeYear" => true,
                    "yearRange" => "1900:+0"
                ],
            ])->textInput(['maxlength' => 10])->label(false)
            ?>
        </div>
    </div>

    <div class="col-md-12">
        <div class="col-lg-3 col-md-4 label-margin label_preview_booking_ct">
            <?= Yii::t('backend', 'Visit number') ?>
        </div>
        <div class="col-md-3 input_store_ct">
            <?=
            $form->field($model, 'min_visit_number', [
                'inputTemplate' => '<div class="input-group">{input}<span class="input-group-addon"><b>' . Yii::t("backend", "Or more times") . '</b></span></div>',
            ])->widget(\yii\widgets\MaskedInput::className(), [
                'clientOptions' => [
                    'groupSeparator' => ',',
                    'alias' => 'integer',
                    'autoGroup' => true,
                    'removeMaskOnSubmit' => true,
                    'allowMinus' => false,
                ]
            ])->textInput(['maxlength' => true])->label(false)
            ?>
        </div>
        <div class="col-md-1 label-margin label-center date_ separator"></div>
        <div class="col-md-3 input_store_ct">
            <?=
            $form->field($model, 'max_visit_number', [
                'inputTemplate' => '<div class="input-group">{input}<span class="input-group-addon"><b>' . Yii::t("backend", 'Following times') . '</b></span></div>',
            ])->widget(\yii\widgets\MaskedInput::className(), [
                'clientOptions' => [
                    'groupSeparator' => ',',
                    'alias' => 'integer',
                    'autoGroup' => true,
                    'removeMaskOnSubmit' => true,
                    'allowMinus' => false,
                ]
            ])->textInput(['maxlength' => true])->label(false)
            ?>
        </div>
    </div>

    <div class="col-md-12">
        <div class="col-lg-3 col-md-4 label-margin label_preview_booking_ct">
            <?= Yii::t('backend', 'Distributing Store') ?>
        </div>
        <div class="col-md-3 input_store_ct">
            <?=
            $form->field($model, 'store_id')->dropDownList(MasterStore::getListStore(), ['prompt' => Yii::t('backend', 'Please Select')])->label(FALSE)
            ?>
        </div>
    </div>

    <div class="col-md-12">
        <div class="col-lg-3 col-md-4 label-margin label_preview_booking_ct">
            <?= Yii::t('backend', 'Customers rank') ?>
        </div>
        <div class="col-md-3 input_store_ct">
            <?=
            $form->field($model, 'rank_id')->dropDownList(Constants::RANK, ['prompt' => Yii::t('backend', 'Please Select')])->label(FALSE)
            ?>
        </div>
    </div>

    <div class="col-md-12">
        <div class="col-lg-3 col-md-4 label-margin label_preview_booking_ct">
            <?= Yii::t('backend', 'Last time staff') ?>
        </div>
        <div class="col-md-3 input_store_ct">
            <?= Html::hiddenInput('staff-id', $model->last_staff, ['id' => 'staff-id']); ?>
            <?=
                    $form->field($model, 'last_staff')->dropDownList([], ['prompt' => Yii::t('backend', 'Please Select')])->label(FALSE)
                    ->widget(DepDrop::classname(), [
                        'options' => ['id' => Html::getInputId($model, 'last_staff')],
                        'pluginOptions' => [
                            //'init' => true,
                            'depends' => [Html::getInputId($model, 'store_id'),],
                            'placeholder' => ' ',
                            'url' => Url::to(['/master-customer/select-staff-by-id-store']),
                            'initialize' => true,
                            'params' => ['staff-id']
                        ]
            ]);
            ?>
        </div>
    </div>

    <div class="col-md-12">
        <div class="col-md-5 label-margin font_label">
            <?= $form->field($model, 'black_list')->checkbox(['label' => '&nbsp;' . Yii::t('backend', 'black_list')], true) ?>
        </div>
    </div>

    <div class="form-group">
        <?= Html::a(Yii::t('backend', 'Return'), ['/'], ['class' => 'btn common-button-default btn-default']) ?>
        <?= Html::submitButton(Yii::t('backend', 'Customer search'), ['class' => 'btn common-button-submit']) ?>
    </div>
</div>
<?php ActiveForm::end(); ?>