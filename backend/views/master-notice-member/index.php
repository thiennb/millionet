<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use dmstr\widgets\Alert;
use common\components\Constants;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $searchModel common\models\MasterNoticeMemberSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('backend', 'Notice');
$this->params['breadcrumbs'][] = $this->title;
$this->registerJsFile('@web/js/manual_notice.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
?>
<div class="row">
    <div class="col-lg-12 col-md-12">
        <div class="box box-info box-solid">
            <div class="box-header with-border">
                <h4 class="text-title"><b><?= $this->title ?></b></h4>
            </div>
            <div class="box-body content">
                <div class="col-md-12">
                    <div class="common-box">
                        <div class="row">
                            <div class="box-header with-border common-box-h4 col-md-4">
                                <h4 class="text-title"><b><?= Yii::t('backend', 'News delivery target designation') ?></b></h4>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group"><?= Alert::widget() ?></div>
                        </div>
                        <div class="box-body content">
                            <div class="col-md-12">
                                <?php echo $this->render('_search', ['model' => $searchModel]); ?>                                
                            </div>  
                        </div>
                    </div>
                    <?php
                    /* Pjax::begin(); */
                    if ($dataProvider !== null) {
                        ?>

                        <?php
                        $form = ActiveForm::begin([
                                    'enableClientValidation' => FALSE,
                                    'action' => ['#'],
                                    'method' => 'post',
                                    'id' => 'search_setting_plan'
                        ]);
                        ?>

                        <div class="common-box">
                            <div class="row">
                                <div class="box-header with-border common-box-h4 col-lg-8 col-md-12">
                                    <h4 class="text-title"><b><?= Yii::t('backend', 'Customer list') ?></b></h4>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="box-body content">
                                    <div class="col-md-12">
                                        <?=
                                        GridView::widget([
                                            'dataProvider' => $dataProvider,
                                            'layout' => "{pager}\n{items}",
                                            'summary' => false,
                                            'columns' => [
                                                ['class' => 'yii\grid\SerialColumn', 'header' => 'No', 'options' => ['class' => 'with-table-no']],
                                                [
                                                    'attribute' => 'name',
                                                    'label' => Yii::t('backend', 'Customer name 1'),
                                                    'value' => function ($model) {
                                                        return empty($model->customer) ? "" : $model->customer->first_name . $model->customer->last_name;
                                                    },
                                                ],
                                                [
                                                    'attribute' => 'store_name',
                                                    'value' => function ($model) {

                                                        return empty($model->store) ? "" : $model->store->name;
                                                    },
                                                    'label' => Yii::t('backend', 'Store Id'),
                                                ],
                                                [
                                                    'attribute' => 'sex',
                                                    'label' => Yii::t('backend', 'Sex'),
                                                    'value' => function ($model) {
                                                        $sex_list = Constants::LIST_SEX;
                                                        $sex_id = empty($model->customer) ? null : $model->customer->sex;
                                                        return isset($sex_list[$sex_id]) ? $sex_list[$sex_id] : "";
                                                    },
                                                ],
                                                [
                                                    'attribute' => 'number_visit',
                                                    'label' => Yii::t('backend', 'Visit number'),
                                                    'value' => function ($model) {
                                                        return $model->number_visit . ' ' . Yii::t('backend', 'The time');
                                                    },
                                                ],
                                                [
                                                    'attribute' => 'last_visit_date',
                                                    'label' => Yii::t('backend', 'Last visit time'),
                                                    'value' => function ($model) {
                                                        return empty($model->last_visit_date) ? "" : date('Y/m/d', strtotime($model->last_visit_date));
                                                    },
                                                ],
                                                [
                                                    'attribute' => 'rank_id',
                                                    'label' => Yii::t('backend', 'Rank'),
                                                    'value' => function ($model) {
                                                        $rank = Constants::RANK;
                                                        return isset($rank[(string) $model->rank_id]) ? $rank[(string) $model->rank_id] : Yii::t('backend', 'No Setting');
                                                    },
                                                ],
                                                [
                                                    'class' => 'yii\grid\ActionColumn', 'template' => "{create-one}",
                                                    'buttons' => [
                                                        'create-one' => function ($url, $model) {
                                                            return Html::button('<span class="fa fa-delete"></span>' . Yii::t('backend', 'Create 1'), [
                                                                        'class' => 'btn common-button-action create_one', 'customer_id' => $model->customer_id, 'store_id' => $model->store_id
                                                                    ]) . Html::hiddenInput('Customer[]', $model->customer_id) . Html::hiddenInput('Store[]', $model->store_id);
                                                        },
                                                            ],
                                                        ],
                                                    ],
                                                ]);
                                                ?>
                                                <div class="col-md-12">
                                                    <div class="text-center form-group">
                                                        <?= Html::button(Yii::t('backend', 'Create Multi'), ['class' => 'btn common-button-submit', 'name' => 'btn-create-muti', 'value' => 'create mutilple notice', 'disabled' => $dataProvider->count == 0, 'id' => 'create_muti']) ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php foreach ($hidden_form->attributes as $key => $value){ ?>
                                    <?= $form->field($hidden_form, $key, ['template' => "{input}"])->hiddenInput(['value' => $value])->label(FALSE)
                                    ?>
                                <?php } ?>
                                    <input type="hidden" value="" id="customer_id_create_one" name="customer_id_create_one"/>
                                    <input type="hidden" value="" id="store_id_create_one" name="store_id_create_one" />
                                <?php
                                ActiveForm::end();
                            }
                            /* Pjax::end(); */
                            ?>
                </div>
            </div>
        </div>
    </div>
</div>