<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use dmstr\widgets\Alert;
use common\components\Constants;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $searchModel common\models\MasterNoticeMemberSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('backend', 'Notice Detail');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <div class="col-lg-12 col-md-12">
        <div class="box box-info box-solid">
            <div class="box-header with-border">
                <h4 class="text-title"><b><?= $this->title ?></b></h4>
            </div>
            <div class="box-body">
                <div class="col-md-10 clear-padding">
                    <div class="common-box">
                        <div class="row form-group">
                            <div class="box-header with-border common-box-h4 col-lg-6 col-md-6">
                                <h4 class="text-title"><b><?= Yii::t('backend', 'Notice Detail') ?></b></h4>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12 form-group clear-padding">
                                <div class="col-md-3 label_preview_one_notice_ct"><label><?= Yii::t('backend', 'Delivery start date') ?></label></div>
                                <div class="col-md-9 input_notice_ct"><?= empty($notice->send_date) ? "" : date('Y/m/d', strtotime($notice->send_date)) ?></div>
                            </div>
                            <div class="col-md-12 form-group clear-padding">
                                <div class="col-md-3 label_preview_one_notice_ct"><label><?= Yii::t('backend', 'Title') ?></label></div>
                                <div class="col-md-9 input_notice_ct"><?= $notice->title ?></div>
                            </div>
                            <div class="col-md-12 form-group clear-padding">
                                <div class="col-md-3 label_preview_one_notice_ct"><label><?= Yii::t('backend', 'Note') ?></label></div>
                                <div class="col-md-9 input_notice_ct" ><?= Html::encode(trim($notice->content) ) ?></div>
                            </div>
                        </div>
                    </div>
                    <?php Pjax::begin(); ?>
                    <div class="common-box">
                        <div class="row form-group">
                            <div class="box-header with-border common-box-h4 col-lg-6 col-md-6">
                                <h4 class="text-title"><b><?= Yii::t('backend', 'Distribution target list') ?></b></h4>
                            </div>
                        </div>
                        <div class="row">
                            <div class="box-body content">
                                <div class="col-md-10 clear-padding">
                                    <?=
                                    GridView::widget([
                                        'dataProvider' => $model,
                                        'layout' => "{pager}\n{items}",
                                        'summary' => false,
                                        'pager' => [
                                            'class' => 'common\components\LinkPagerMillionet',
                                            'options' => ['class' => 'pagination common-float-right'],
                                        ],
                                        'columns' => [
                                            ['class' => 'yii\grid\SerialColumn', 'header' => 'No', 'options' => ['class' => 'with-table-no']],
                                            [
                                                'attribute' => 'name',
                                                'label' => Yii::t('backend', 'Customer name 1'),
                                                'value' => function ($model) {
                                                    return $model->first_name . $model->last_name;
                                                },
                                            ],
                                            [
                                                'attribute' => 'sex',
                                                'value' => function ($model) {
                                                    $sex = Constants::LIST_SEX;
                                                    return isset($sex[$model->sex]) ? $sex[$model->sex] : "";
                                                },
                                            ],
                                            [
                                                'attribute' => 'number_visit',
                                                'value' => function ($model) {
                                                    return $model->number_visit . ' ' . Yii::t('backend', 'The time');
                                                },
                                            ],
                                            [
                                                'attribute' => 'last_visit_date',
                                                'label' => Yii::t('backend', 'Last visit time'),
                                                'value' => function ($model) {
                                                    return empty($model->last_visit_date) ? "" : date('Y/m/d', strtotime($model->last_visit_date));
                                                },
                                            ],
                                            [
                                                'attribute' => 'rank',
                                                'label' => Yii::t('backend', 'Rank'),
                                                'value' => function ($model) {
                                                    $rank = Constants::RANK;
                                                    return isset($rank[$model->rank]) ? $rank[$model->rank] : "";
                                                },
                                            ],
                                        ],
                                    ]);
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php Pjax::end(); ?>
                    <div class="row text-center">
                        <div class="col-md-6">
                            <?php $form = ActiveForm::begin(); ?>
                            <?= Html::a(Yii::t('backend', 'Return'), ['list-plan'], ['class' => 'btn btn-default common-button-default']) ?>
                            <?php
                            if ($status == \common\models\MasterNotice::STATUS_SEND_DOING) {
                                echo Html::submitButton(Yii::t('backend', Yii::t('backend', 'Stop Delivery')), [
                                    'class' => 'btn common-button-submit',
                                    'id' => 'btn-confirm',
                                ]);
                            }
                            ?>
                            <?php ActiveForm::end(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>