<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use common\components\Constants;
use yii\jui\DatePicker;
use common\models\MasterStore;
use kartik\depdrop\DepDrop;

/* @var $this yii\web\View */
/* @var $model common\models\MasterNoticeMemberSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="master-notice-member-search col-md-8">
    <?php
    $form = ActiveForm::begin([
                // important
                'enableClientValidation' => false,
    ]);
    ?>
    <div class="col-md-12">
        <div class="col-md-2 label-margin deliver_time_notice_ct">
            <label class='mws-form-label'>
                <?= Yii::t('backend', 'Deliver Time') ?>
            </label>
        </div>
        <?php $model->push_timming = isset($model->push_timming) ? $model->push_timming : "01" ?>
        <div class="col-md-10" id="list-push-timming">
            <div class="row">
                <div class="col-md-1">
                    <?= $form->field($model, 'push_timming')->radio(['label' => '', 'value' => Constants::PUSH_TIMMING_AT_BOOKING, 'uncheck' => null]) ?>
                </div>
                <div class="col-md-11">
                    <div class="col-md-3 label-margin">
                        <?= Yii::t('backend', 'When booking') ?>
                    </div>
                </div>
            </div>     
            <div class="row radio-list">
                <div class="col-md-1">
                    <?= $form->field($model, 'push_timming')->radio(['label' => '', 'value' => Constants::PUSH_TIMMING_BEFORE_BOOKING, 'uncheck' => null]) ?>
                </div>
                <div class="col-md-11">
                    <div class="col-md-3 label-margin">
                        <?= Yii::t('backend', 'Booking') ?>
                    </div>
                    <div class="col-md-2 clear-form-group-2">
                        <?=
                        $form->field($model, 'hour_before_booking', ['template' => "{input}{error}"])->widget(\yii\widgets\MaskedInput::className(), [
                            'clientOptions' => [
                                'alias' => 'decimal',
                                'autoGroup' => true,
                                'removeMaskOnSubmit' => true,
                                'allowMinus' => false,
                                'allowPlus' => false,
                            ]
                        ])->textInput(['maxlength' => true])->label(FALSE)
                        ?> 
                    </div>
                    <div class="col-md-6 label-margin">
                        <?= Yii::t('backend', 'Time ago') ?>
                    </div>
                </div>
            </div>
            <div class="row radio-list">
                <div class="col-md-1">
                    <?= $form->field($model, 'push_timming')->radio(['label' => '', 'value' => Constants::PUSH_TIMMING_AT_REGISTER, 'uncheck' => null]) ?>
                </div>
                <div class="col-md-11">
                    <div class="col-md-12 label-margin">
                        <?= Yii::t('backend', 'When register member') ?>
                    </div>
                </div>
            </div>                
            <div class="row radio-list">
                <div class="col-md-1">
                    <?= $form->field($model, 'push_timming')->radio(['label' => '', 'value' => Constants::PUSH_TIMMING_AFTER_REGISTER, 'uncheck' => null]) ?>
                </div>
                <div class="col-md-11">
                    <div class="col-md-3 label-margin">
                        <?= Yii::t('backend', 'From member registration') ?>
                    </div>
                    <div class="col-md-2 clear-form-group-2">
                        <?=
                        $form->field($model, 'hour_after_register_member', ['template' => "{input}{error}"])->widget(\yii\widgets\MaskedInput::className(), [
                            'clientOptions' => [
                                'alias' => 'decimal',
                                'autoGroup' => true,
                                'removeMaskOnSubmit' => true,
                                'allowMinus' => false,
                                'allowPlus' => false,
                            ]
                        ])->textInput(['maxlength' => true])->label(FALSE)
                        ?> 
                    </div>
                    <div class="col-md-6 label-margin">
                        <?= Yii::t('backend', 'Time') ?>
                    </div>
                </div>
            </div>
            <div class="row radio-list">
                <div class="col-md-1">
                    <?= $form->field($model, 'push_timming')->radio(['label' => '', 'value' => Constants::PUSH_TIMMING_AFTER_CHARGE, 'uncheck' => null]) ?>
                </div>
                <div class="col-md-11">
                    <div class="col-md-3 label-margin">
                        <?= Yii::t('backend', 'From accounting') ?>
                    </div>
                    <div class="col-md-2 clear-form-group-2">
                        <?=
                        $form->field($model, 'hour_after_accounting', ['template' => "{input}{error}"])->widget(\yii\widgets\MaskedInput::className(), [
                            'clientOptions' => [
                                'alias' => 'decimal',
                                'autoGroup' => true,
                                'removeMaskOnSubmit' => true,
                                'allowMinus' => false,
                                'allowPlus' => false,
                            ]
                        ])->textInput(['maxlength' => true])->label(FALSE)
                        ?> 
                    </div>
                    <div class="col-md-6 label-margin">
                        <?= Yii::t('backend', 'Time') ?>
                    </div>
                </div>
            </div>
            <div class="row radio-list">
                <div class="col-md-1">
                    <?= $form->field($model, 'push_timming')->radio(['label' => '', 'value' => Constants::PUSH_TIMMING_NEAR_STORE, 'uncheck' => null]) ?>
                </div>
                <div class="col-md-11">
                    <div class="col-md-3 label-margin">
                        <?= Yii::t('backend', 'From the store') ?>
                    </div>
                    <div class="col-md-2 clear-form-group-2">
                        <?= $form->field($model, 'distance_to_shop', ['template' => "{input}{error}"])->dropDownList(Constants::DISTANCE_TO_SHOP, ['prompt' => Yii::t('backend', 'Please Select')])->label(FALSE) ?> 
                    </div>
                    <div class="col-md-6 label-margin">
                        <?= Yii::t('backend', 'When approach within meters') ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-12">
        <div class="col-md-2 label-margin">
            <?= Yii::t('backend', 'Membership card number') ?>
        </div>
        <div class="col-md-3">
            <?=
            $form->field($model, 'customer_jan_code')->widget(\yii\widgets\MaskedInput::className(), [
                'mask' => '9',
                'clientOptions' => ['repeat' => 13, 'greedy' => false]])->textInput(['maxlength' => true])->label(FALSE)
            ?>
        </div>
    </div>

    <div class="col-md-12">
        <div class="col-md-2 label-margin">
            <?= Yii::t('backend', 'Name') ?>
        </div>
        <div class="col-md-1 label-margin">
            <?= Yii::t('backend', 'First Name') ?>
        </div>
        <div class="col-md-2">
            <?=
            $form->field($model, 'first_name')->textInput(['maxlength' => true])->label(false)
            ?>
        </div>
        <div class="col-md-1 label-margin">
            <?= Yii::t('backend', 'Last Name') ?>
        </div>
        <div class="col-md-2">
            <?=
            $form->field($model, 'last_name')->textInput(['maxlength' => true])->label(false)
            ?>
        </div>
    </div>

    <div class="col-md-12">
        <div class="col-md-2 label-margin">
        </div>
        <div class="col-md-1 label-margin">
            <?= Yii::t('backend', 'First Name Kana') ?>
        </div>
        <div class="col-md-2">
            <?=
            $form->field($model, 'first_name_kana')->textInput(['maxlength' => true])->label(false)
            ?>
        </div>
        <div class="col-md-1 label-margin">
            <?= Yii::t('backend', 'Last Name Kana') ?>
        </div>
        <div class="col-md-2">
            <?=
            $form->field($model, 'last_name_kana')->textInput(['maxlength' => true])->label(false)
            ?>
        </div>
    </div>

    <div class="col-md-12">
        <div class="col-md-2 label-margin">
            <?= Yii::t('backend', 'Sex') ?>
        </div>
        <div class="col-md-3">
            <?=
            $form->field($model, 'sex')->dropDownList(Constants::LIST_SEX, ['prompt' => Yii::t('backend', 'Please Select')])->label(FALSE)
            ?>
        </div>
    </div>
    <div class="col-md-12">
        <div class="col-md-2 label-margin">
            <?= Yii::t('backend', 'Birthday') ?>
        </div>
        <div class="col-md-3">
            <?=
            $form->field($model, 'start_birth_date')->widget(DatePicker::classname(), [
                'language' => 'ja',
                'dateFormat' => 'yyyy/MM/dd',
                'clientOptions' => [
                    "changeMonth" => true,
                    "changeYear" => true,
                    "yearRange" => "1900:+0"
                ],
            ])->textInput(['maxlength' => true])->label(false)
            ?>
        </div>
        <div class="col-md-1 label-margin label-center date_ separator">～</div>
        <div class="col-md-3">
            <?=
            $form->field($model, 'end_birth_date')->widget(DatePicker::classname(), [
                'language' => 'ja',
                'dateFormat' => 'yyyy/MM/dd',
                'clientOptions' => [
                    "changeMonth" => true,
                    "changeYear" => true,
                    "yearRange" => "1900:+0"
                ],
            ])->textInput(['maxlength' => true])->label(false)
            ?>
        </div>
    </div>
    <div class="col-md-12">
        <div class="col-md-2 label-margin">
            <?= Yii::t('backend', 'Last visit time') ?>
        </div>
        <div class="col-md-3">
            <?=
            $form->field($model, 'start_visit_date')->widget(DatePicker::classname(), [
                'language' => 'ja',
                'dateFormat' => 'yyyy/MM/dd',
                'clientOptions' => [
                    "changeMonth" => true,
                    "changeYear" => true,
                    "yearRange" => "1900:+0"
                ],
            ])->textInput(['maxlength' => true])->label(false)
            ?>
        </div>
        <div class="col-md-1 label-margin label-center date_ separator">～</div>
        <div class="col-md-3">
            <?=
            $form->field($model, 'end_visit_date')->widget(DatePicker::classname(), [
                'language' => 'ja',
                'dateFormat' => 'yyyy/MM/dd',
                'clientOptions' => [
                    "changeMonth" => true,
                    "changeYear" => true,
                    "yearRange" => "1900:+0"
                ],
            ])->textInput(['maxlength' => true])->label(false)
            ?>
        </div>
    </div>
    <div class="col-md-12">
        <div class="col-md-2 label-margin">
            <?= Yii::t('backend', 'Visit number') ?>
        </div>
        <div class="col-md-3">
            <?=
            $form->field($model, 'min_visit_number', [
                'inputTemplate' => '<div class="input-group">{input}<span class="input-group-addon"><b>' . Yii::t("backend", "Or more times") . '</b></span></div>',
            ])->widget(\yii\widgets\MaskedInput::className(), [
                'clientOptions' => [
                    'groupSeparator' => ',',
                    'alias' => 'integer',
                    'autoGroup' => true,
                    'removeMaskOnSubmit' => true,
                    'allowMinus' => false,
                ]
            ])->textInput(['maxlength' => true])->label(false)
            ?>
        </div>
        <div class="col-md-1 label-margin label-center date_ separator"></div>
        <div class="col-md-3">
            <?=
            $form->field($model, 'max_visit_number', [
                'inputTemplate' => '<div class="input-group">{input}<span class="input-group-addon"><b>' . Yii::t("backend", "Following times") . '</b></span></div>',
            ])->widget(\yii\widgets\MaskedInput::className(), [
                'clientOptions' => [
                    'groupSeparator' => ',',
                    'alias' => 'integer',
                    'autoGroup' => true,
                    'removeMaskOnSubmit' => true,
                    'allowMinus' => false,
                ]
            ])->textInput(['maxlength' => true])->label(false)
            ?>
        </div>
    </div>

    <div class="col-md-12">
        <div class="col-md-2 label-margin">
            <?= Yii::t('backend', 'Distributing Store') ?>
        </div>
        <div class="col-md-3">
<?= $form->field($model, 'store_id')->dropDownList(MasterStore::getListStore(), ['prompt' => Yii::t('backend', 'Please Select')])->label(FALSE) ?>
        </div>
    </div>

    <div class="col-md-12">
        <div class="col-md-2 label-margin">
            <?= Yii::t('backend', 'Customers rank') ?>
        </div>
        <div class="col-md-3">
            <?=
            $form->field($model, 'rank_id')->dropDownList(Constants::RANK, ['prompt' => Yii::t('backend', 'Please Select')])->label(FALSE)
            ?>
        </div>
    </div>

    <div class="col-md-12">
        <div class="col-md-2 label-margin">
            <?= Yii::t('backend', 'Last time staff') ?>
        </div>
        <div class="col-md-3">
            <?= Html::hiddenInput('staff-id', $model->last_staff_id, ['id' => 'staff-id']); ?>
            <?=
                    $form->field($model, 'last_staff_id')->dropDownList([], ['prompt' => Yii::t('backend', 'Please Select')])->label(FALSE)
                    ->widget(DepDrop::classname(), [
                        'options' => ['id' => Html::getInputId($model, 'last_staff_id')],
                        'pluginOptions' => [
                            //'init' => true,
                            'depends' => [Html::getInputId($model, 'store_id'),],
                            'placeholder' => ' ',
                            'url' => yii\helpers\Url::to(['/master-customer/select-staff-by-id-store']),
                            'initialize' => true,
                            'params' => ['staff-id']
                        ]
            ]);
            ?>
        </div>
    </div>

    <div class="col-md-12">
        <div class="col-md-5 label-margin font_label">
<?= $form->field($model, 'black_list_flg')->checkbox(['label' => '&nbsp;' . Yii::t('backend', 'black_list')], true) ?>
        </div>
    </div>

    <div class="form-group text-center">
<?= Html::a(Yii::t('backend', 'Return'), ['/'], ['class' => 'btn common-button-default btn-default']) ?>
    <?= Html::submitButton(Yii::t('backend', 'Create Notice'), ['class' => 'btn common-button-submit']) ?>
    </div>

<?php ActiveForm::end(); ?>
</div>

<script>
    $("input[name='<?= Html::getInputName($model, 'push_timming') ?>']").on("change", function () {
        $("#list-push-timming").find(':input:not([type="radio"])').prop('disabled', true).val('');
        $(this).parents(".row.radio-list").find(':input:not([type="radio"])').prop('disabled', false);
    })
    $(document).ready(function () {
        $("#list-push-timming").find(':input:not([type="radio"])').prop('disabled', true);
        $(':input[type="radio"]:checked').parents(".row.radio-list").find(':input:not([type="radio"])').prop('disabled', false);
    });
</script>
