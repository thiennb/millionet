<?php

use common\components\Constants;
use common\models\MasterStore;
//use common\widgets\search;

/* @var $this yii\web\View */
/* @var $model common\models\MasterNoticeMemberSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<?=
\common\widgets\preview\SearchWidget::widget([
    "data" => [
        'deliver_status' => [
                'type' => 'dropdownlist',
                'label' => Yii::t('backend', 'Delivery Status'),
                'drop-data' => Constants::DELIVERY_STATUS,
                'prompt' => Yii::t('backend', 'Please Select'),
                'show-condition' => Yii::$app->controller->action->id === "list-history",
            ],
        'store_id' => [
                'type' => 'dropdownlist',
                'label' => Yii::t('backend', 'Distributing Store'),
                'drop-data' => MasterStore::getListStore(),
                'prompt' => Yii::t('backend', 'Please Select'),
                //'show-condition' => Yii::$app->controller->action->id === "list-history",
            ],
        'send_date' => [
                'type' => 'date-to-date',
                'double-input' => [
                        'input-1' => 'start_send_date',
                        'input-2' => 'end_send_date',
                    ],
                'label' => Yii::t('backend', 'Date of delivery'),
                
                
            ],
    ],
    'formAction' => Yii::$app->controller->action->id,
    'btnBack' => ['action' => '/'],
    'searchName' => 'master-notice',
    'model' => $model,
])   ?>