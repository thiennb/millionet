<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use dmstr\widgets\Alert;
use common\components\Constants;

/* @var $this yii\web\View */
/* @var $searchModel common\models\MasterNoticeMemberSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$role = \common\components\FindPermission::getRole();
$this->title = Yii::t('backend', 'Delivery schedule List');
$this->params['breadcrumbs'][] = $this->title;
?>
<style>
    .pagination{ 
        display: table; 
        margin: 0 auto; 
        padding: 10px;
    }
    .pagination a{ 
        margin: 0 2px; 
    }
</style>
<div class="row">
    <div class="col-lg-12 col-md-12">
        <div class="box box-info box-solid">
            <div class="box-header with-border">
                <h4 class="text-title"><b><?= $this->title ?></b></h4>
            </div>
            <div class="box-body">
                <div class="col-md-12">
                    <div class="common-box">
                        <div class="box-body">
                            <div class="row">
                                <div class="box-header with-border common-box-h4 col-lg-8 col-md-10">
                                    <h4 class="text-title"><b><?= Yii::t('backend', 'Search') ?></b></h4>
                                </div>
                            </div>
                            <div class="box-body">
                                <div class="col-md-8">
                                    <?php echo $this->render('_search_deliver_schedule', ['model' => $searchModel]); ?>                                
                                </div>  
                            </div>
                            <div class="row">
                                <div class="box-header with-border common-box-h4 col-lg-8 col-md-10">
                                    <h4 class="text-title"><b><?= Yii::t('backend', 'Delivery schedule List') ?></b></h4>
                                </div>
                            </div>
                            <div class="row">
                                <div class="box-body">
                                    <div class="col-md-10">
                                        <?php if ($role->permission_id != Constants::STAFF_ROLE)
                                            echo Html::a(Yii::t('backend', 'Create New'), ['/master-notice-member/setting-plan'], ['class' => 'pagination btn common-button-submit common-float-right margin-bottom-10']); ?>
                                        <?php Pjax::begin(); ?>

                                        <?php Pjax::begin(); ?>
                                        <?=
                                        GridView::widget([
                                            'dataProvider' => $dataProvider,
                                            'layout' => "{pager}\n{items}",
                                            'summary' => false,
                                            'pager' => [
                                                'class' => 'common\components\LinkPagerMillionet',
                                                'options' => ['class' => 'pagination common-float-right'],
                                            ],
                                            'columns' => [
                                                ['class' => 'yii\grid\SerialColumn', 'header' => 'No', 'options' => ['class' => 'with-table-no']],
                                                [
                                                    'attribute' => 'push_date',
                                                    'label' => Yii::t('backend', 'Date of delivery'),
                                                    'value' => function ($model) {
                                                        return empty($model->push_date) ? "" : date('Y/m/d H:s', $model->push_date);
                                                    }
                                                ],
                                                [
                                                    'attribute' => 'store_name',
                                                    'value' => function ($model) {
                                                        return $model->store_name;
                                                    },
                                                    'label' => Yii::t('backend', 'Distributing Store'),
                                                ],
                                                [
                                                    'attribute' => 'name',
                                                    'label' => Yii::t('backend', 'Name Customer'),
                                                    'value' => function ($model) {
                                                        return $model->first_name . $model->last_name;
                                                    },
                                                ],
                                                [
                                                    'attribute' => 'title',
                                                    'label' => Yii::t('backend', 'Title'),
                                                ],
                                                [
                                                    'class' => 'yii\grid\ActionColumn', 'template' => "{detail-deliver-schedule}",
                                                    'buttons' => [
                                                        'detail-deliver-schedule' => function ($url, $model) {
                                                            return Html::a(Yii::t('backend', 'Detail'), $url, [
                                                                        'class' => 'btn common-button-action',
                                                            ]);
                                                        },
                                                            ],
                                                        ],
                                                    ],
                                                ]);
                                                ?>
                                                <?php Pjax::end(); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>