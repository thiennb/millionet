<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use dmstr\widgets\Alert;
use common\components\Constants;

/* @var $this yii\web\View */
/* @var $searchModel common\models\MstCategoriesProductSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->registerJsFile('@web/js/product_category.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$role = \common\components\FindPermission::getRole();

$this->title = Yii::t('backend', 'Product Category List');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row text-setting company">
    <div class="col-lg-12 col-md-12">
        <div class="box box-info box-solid add">
            <div class="box-header with-border">
                <h4 class="text-title"><b><?= $this->title ?></b></h4>
            </div>
            <div class="box-body content">
                <div class="col-md-12">
                    <?php Pjax::begin(['enablePushState' => false]); ?>
                    <div class="common-box">
                        <div class="row">
                            <div class="box-header with-border common-box-h4 col-md-4">
                                <h4 class="text-title"><b><?= Yii::t('backend', 'Product Category List') ?></b></h4>
                            </div>
                        </div>
                        <div class="box-body">
                            <div class="col-md-7">
                                <div class="row text-center">
                                    <?= Html::a(Yii::t('backend', 'Return'), ['/'], ['class' => 'btn btn-default common-button-default margin-bottom-10']) ?>
                                    <?php
                                    if (!($role->permission_id == Constants::STORE_MANAGER_ROLE || $role->permission_id == Constants::STAFF_ROLE)) {
                                        echo Html::button(Yii::t('backend', 'Save'), ['class' => 'btn common-button-submit margin-bottom-10', 'id' => "bt_register_product_category"]);
                                    }
                                    ?>
                                </div>
                                <?php
                                if (!($role->permission_id == Constants::STORE_MANAGER_ROLE || $role->permission_id == Constants::STAFF_ROLE)) {
                                    echo GridView::widget([
                                        'dataProvider' => $dataProvider,
                                        'layout' => "{summary}\n{items}",
                                        'summaryOptions' => ['class' => 'common-clr-fl-right'],
                                        'summary' => false,
                                        'tableOptions' => [
                                            'class' => 'table table-striped table-bordered text-center master-product-category',
                                        ],
                                        'columns' => [
                                            //['class' => 'yii\grid\SerialColumn', 'header'=>'No','options' => ['class' => 'with-table-no']],
                                            [
                                                'attribute' => 'name',
                                                'headerOptions' => [
                                                    'class' => ['text-center col-md-11']
                                                ]
                                            ],
                                            [
                                                'class' => 'yii\grid\ActionColumn', 'template' => "{delete}",
                                                'buttons' => [
                                                    //delete button
                                                    'delete' => function ($url, $model) {
                                                        return Html::a('<span class="fa fa-delete"></span>' . Yii::t('backend', 'Delete'), $url, [
                                                                    'class' => 'btn common-button-action',
                                                                    'aria-label' => "Delete",
                                                                    'data-confirm' => Yii::t('backend', "Delete Category Product Error Message"),
                                                                    'data-method' => "post"
                                                        ]);
                                                    },
                                                        ],
                                                        'headerOptions' => [
                                                            'class' => ['text-center col-md-1']
                                                        ]
                                                    ]
                                                ],
                                            ]);
                                        } else {
                                            echo GridView::widget([
                                                'dataProvider' => $dataProvider,
                                                'layout' => "{summary}\n{items}",
                                                'summaryOptions' => ['class' => 'common-clr-fl-right'],
                                                'summary' => false,
                                                'columns' => [
                                                    //['class' => 'yii\grid\SerialColumn', 'header'=>'No','options' => ['class' => 'with-table-no']],
                                                    [
                                                        'attribute' => 'name',
                                                        'headerOptions' => [
                                                            'class' => ['text-center col-md-11']
                                                        ]
                                                    ],
                                                ]
                                            ]);
                                        }
                                        ?>
                                    </div>
                                </div>
                            </div>
                            <?php Pjax::end(); ?>

                </div>
            </div>
        </div>
    </div>
</div>