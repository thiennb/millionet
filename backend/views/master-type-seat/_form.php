<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use kartik\file\FileInput;
use common\components\Util;
use yii\bootstrap\Modal;
use common\widgets\preview\Preview;
use common\models\MasterStore;

/* @var $this yii\web\View */
/* @var $model common\models\MasterTypeSeat */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="master-type-seat-form col-md-8">
    <?php
    $form = ActiveForm::begin(
                    [
                        'options' => [
                            'enctype' => 'multipart/form-data',
                            'id' => strtolower($model->formName()) . '-id',
                        ],
                        'layout' => 'horizontal',
                        'fieldConfig' => [
                            'template' => "{beginWrapper}<div class='col-md-3'>{label}</div><div class='col-md-4'>{input}\n{hint}\n{error}</div>\n{endWrapper}",
                            'horizontalCssClasses' => [
                                'label' => '',
                                'wrapper' => 'row',
                                'error' => '',
                                'hint' => '',
                            ],
                            'horizontalCheckboxTemplate' => "{input}{label}\n{error}\n{hint}"
                        ],
                        // important
                        'validationUrl' => 'validate',
                        'enableClientValidation' => false,
                        'enableAjaxValidation' => true,
                        'validateOnChange' => false,
                        'validateOnBlur' => false,
                        'successCssClass' => '',
    ]);
    ?>
    <div class="col-md-12">
        <div class="col-md-3 no-padding">
            <label class="control-label required-star label_type_seat" >
                <?= Yii::t('backend', 'Store Id') ?>
            </label>
        </div>
        <div class="col-md-4 input_type_seat">
            <?=
            $form->field($model, 'store_id', [
                'template' => '{input}{error}'
            ])->dropDownList(MasterStore::getListStoreTypeSeat(), ['prompt' => Yii::t('backend', 'Please Select')])->label(FALSE);
            ?>
        </div>
    </div>

    <div class="col-md-12">
        <div class="col-md-3 no-padding">
            <label class="control-label label_type_seat" >
                <?= Yii::t('backend', 'Name Type Seat') ?>
            </label>
            <span style="color:red">*</span>
        </div>

        <div class="col-md-4 input_type_seat">
            <?=
            $form->field($model, 'name', [
                'template' => '{input}{error}'
            ])->textInput(['maxlength' => true])->label(FALSE)
            ?>
        </div>
    </div>

    <div class="col-md-12">
        <div class="col-md-3 no-padding">
            <label class="control-label label_type_seat" >
                <?= Yii::t('backend', 'Introduce') ?>
            </label>
        </div>

        <div class="col-md-4 input_type_seat">
            <?=
            $form->field($model, 'introduce', [
                'template' => '{input}{error}'
            ])->textarea(['rows' => 6, 'maxlength' => 500])->label(FALSE)
            ?>
        </div>
    </div>

    <div class="col-md-12">
        <div class="col-md-3 no-padding">
            <label class="control-label label_type_seat" >
                <?= Yii::t('backend', 'Type Seat Image1') ?>
            </label>
        </div>
        <div class="col-md-4 image_type_seat">
            <?=
            $form->field($model, 'tmp_image1', [
                'template' => '{input}{error}'
            ])->widget(FileInput::classname(), [
                'options' => ['accept' => 'image/*'],
                'pluginOptions' => [
                    'allowedFileExtensions' => ['jpg', 'gif', 'png', 'jpeg'],
                    'maxFileSize' => 2048,
                    'initialPreview' => [
                        Html::img(Util::getUrlImage($model->image1))
                    ],
                    'overwriteInitial' => true,
                    'showUpload' => false,
                    'showCaption' => false,
                    'browseLabel' => Yii::t('backend', 'Select an image'),
                ],
                'pluginEvents' => [
                    "fileclear" => 'function() { $(this).parent().parent().parent().parent().find(".hidden_image").val("") }',
                ],
            ]);
            ?>
            <?= $form->field($model, 'hidden_image1')->hiddenInput(['class' => 'hidden_image'])->label(FALSE) ?>
        </div>  
    </div>
    <div class="col-md-12">
        <div class="col-md-3 no-padding">
            <label class="control-label label_type_seat" >
                <?= Yii::t('backend', 'Type Seat Image2') ?>
            </label>
        </div>
        <div class="col-md-4 image_type_seat">
            <?=
            $form->field($model, 'tmp_image2', [
                'template' => '{input}{error}'
            ])->widget(FileInput::classname(), [
                'options' => ['accept' => 'image/*'],
                'pluginOptions' => [
                    'allowedFileExtensions' => ['jpg', 'gif', 'png', 'jpeg'],
                    'maxFileSize' => 2048,
                    'initialPreview' => [
                        Html::img(Util::getUrlImage($model->image2))
                    ],
                    'overwriteInitial' => true,
                    'showUpload' => false,
                    'showCaption' => false,
                    'browseLabel' => Yii::t('backend', 'Select an image'),
                ],
                'pluginEvents' => [
                    "fileclear" => 'function() { $(this).parent().parent().parent().parent().find(".hidden_image").val("") }',
                ],
            ]);
            ?>
            <?= $form->field($model, 'hidden_image2')->hiddenInput(['class' => 'hidden_image'])->label(FALSE) ?>

        </div>
    </div>
    <div class="col-md-12">
        <div class="col-md-3 no-padding">
            <label class="control-label label_type_seat" >
                <?= Yii::t('backend', 'Type Seat Image3') ?>
            </label>
        </div>

        <div class="col-md-4 image_type_seat">

            <?=
            $form->field($model, 'tmp_image3', [
                'template' => '{input}{error}'
            ])->widget(FileInput::classname(), [
                'options' => ['accept' => 'image/*'],
                'pluginOptions' => [
                    'allowedFileExtensions' => ['jpg', 'gif', 'png', 'jpeg'],
                    'maxFileSize' => 2048,
                    'initialPreview' => [
                        Html::img(Util::getUrlImage($model->image3))
                    ],
                    'overwriteInitial' => true,
                    'showUpload' => false,
                    'showCaption' => false,
                    'browseLabel' => Yii::t('backend', 'Select an image'),
                ],
                'pluginEvents' => [
                    "fileclear" => 'function() { $(this).parent().parent().parent().parent().find(".hidden_image").val("") }',
                ],
            ]);
            ?>
             <?= $form->field($model, 'hidden_image3')->hiddenInput(['class' => 'hidden_image'])->label(FALSE) ?>
        </div>
    </div>

    <div class="col-md-12">
        <div class="col-md-4 no-padding label_type_seat">
            <label class="control-label" >
                <?= Yii::t('backend', 'Show Type Seat') ?>
            </label>
        </div>

        <div class="col-md-6 font_label">

            <?php
            if ($model->show_flg == null) {
                $model->show_flg = 0;
            }
            ?> 

            <?=
            $form->field($model, 'show_flg', [
                'template' => '{input}{error}'
            ])->radioList($model->listShow())->inline()->label(FALSE);
            ?>

        </div>
    </div>

    <div class="col-md-12">
        <div class="row">
            <?= Html::a(Yii::t('backend', 'Return'), ['index'], ['class' => 'btn btn-default common-button-default']) ?>
            <?=
            Html::submitButton(Yii::t('backend', 'Create Type Seat'), [
                'class' => 'btn common-button-submit',
                'id' => 'btn-confirm',
            ])
            ?>
        </div>
    </div>

    <?php
    Modal::begin([
        'id' => 'userModal',
        'size' => 'SIZE_LARGE',
        'header' => '<div class="box-header with-border common-box-h4 col-md-8"><h4 class="text-title"><b>' . Yii::t('backend', 'Confirm input') . '</b></h4></div>',
        'footer' => Html::button(Yii::t('backend', 'Close'), ['class' => 'btn common-button-default', 'data-dismiss' => "modal", 'id' => 'btn-close'])
        . Html::submitButton(Yii::t('backend', 'Save'), ['class' => 'btn common-button-submit', 'id' => 'btn-submit']),
        'footerOptions' => ['class' => 'modal-footer text-center'],
        'closeButton' => FALSE,
    ]);
    ?>

    <?=
    Preview::widget([
        "data" => [
            'store_id' => [
                'type' => 'select',
                'label' => Yii::t('backend', 'Store Id')
            ],
            'name' => [
                'type' => 'input',
                'label' => Yii::t('backend', 'Name Type Seat')
            ],
            'introduce' => [
                'type' => 'input',
                'label' => Yii::t('backend', 'Introduce')
            ],
            'tmp_image1' => [
                'type' => 'image',
                'label' => Yii::t('backend', 'Type Seat Image1')
            ],
            'tmp_image2' => [
                'type' => 'image',
                'label' => Yii::t('backend', 'Type Seat Image2')
            ],
            'tmp_image3' => [
                'type' => 'image',
                'label' => Yii::t('backend', 'Type Seat Image3')
            ],
            'show_flg' => [
                'type' => 'radio',
                'label' => Yii::t('backend', 'Show')
            ],
        ],
        "modelName" => $model->formName(),
        'idBtnConfirm' => 'btn-confirm',
        'formId' => strtolower($model->formName()) . '-id',
        'btnClose' => 'btn-close',
        'btnSubmit' => 'btn-submit',
        'modalId' => 'userModal'
    ])
    ?>
    <?php
    Modal::end();
    ?>
    <?php ActiveForm::end(); ?>    
</div>
</div>
