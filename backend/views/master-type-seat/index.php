<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\LinkPager;
use yii\widgets\Pjax;
use common\models\MasterNotice;
use common\components\Constants;

/* @var $this yii\web\View */
/* @var $searchModel common\models\MstProductSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('backend', 'Type Seat List');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <div class="col-lg-12 col-md-12">
        <div class="box box-info box-solid">
            <div class="box-header with-border">
                <h4 class="text-title"><b><?= $this->title ?></b></h4>
            </div>
            <div class="box-body content">
                <div class="col-md-8">

                    <?php
//    if(count($dataProvider)>1){
                    Pjax::begin();
                    ?>    
                    <div class="common-box">
                        <div class="box-header with-border common-box-h4 col-md-6">
                            <h4 class="text-title"><b><?= Yii::t('backend', 'Type Seat Box') ?></b></h4>
                        </div>

                        <div class="box-body content">
                            <div class="col-md-8">
                                <?php echo $this->render('_search', ['model' => $searchModel]); ?>                                
                            </div>
                        </div>

                        <div class="common-box">
                            <div class="box-header with-border common-box-h4 col-md-6">
                                <h4 class="text-title"><b><?= Yii::t('backend', 'Type Seat List') ?></b></h4>
                            </div>
                            <div class="box-body content">
                                <div class="col-md-10">
                                    <?= Html::a(Yii::t('backend', 'Crete New Type Seat'), ['create'], ['class' => 'btn common-button-submit common-float-right', 'style' => 'margin-bottom: 0.5em;']) ?>
                                    <?=
                                    GridView::widget([
                                        'dataProvider' => $dataProvider,
                                        'layout' => "{pager}\n{summary}\n{items}",
                                        'summaryOptions' => ['class' => 'common-clr-fl-right'],
                                        'summary' => false,
                                        'pager' => [
                                            'class' => 'common\components\LinkPagerMillionet',
                                            'options' => ['class' => 'pagination common-float-right'],
                                        ],
                                        'tableOptions' => [
                                            'class' => 'table table-striped table-bordered text-center',
                                        ],
                                        'columns' => [
                                            ['class' => 'yii\grid\SerialColumn', 'header'=>'No','options' => ['class' => 'with-table-no']],
                                            [
                                                'attribute' => 'name',
                                            ],
                                            [
                                                'class' => 'yii\grid\ActionColumn', 'template' => "{delete} {update}",
                                                'options' => ['class' => 'with-table-button'],
                                                'buttons' => [
                                                    //view button
                                                    'delete' => function ($url, $model) {
                                                        return Html::a('<span class="fa fa-delete"></span>' . Yii::t('backend', 'Delete'), $url, [
                                                                    'class' => 'btn common-button-action',
                                                                    'aria-label' => "Delete",
                                                                    'data-confirm' => Yii::t('backend', "Delete Coupon Error Message"),
                                                                    'data-method' => "post"
                                                        ]);
                                                    },
                                                    'update' => function ($url, $model) {
                                                        return Html::a('<span class="fa fa-delete"></span>' . Yii::t('backend', 'Update'), $url, [
                                                                    'title' => Yii::t('backend', 'Update'),
                                                                    'class' => 'btn common-button-action',
                                                        ]);
                                                    },
                                                        ],
                                                    ],
                                                ],
                                            ]);
                                            ?>
                                        </div>
                                    </div>
                                </div>
                                <?php
                                Pjax::end();
                                ?>
                    </div>
                </div>
            </div>
        </div>
    </div>