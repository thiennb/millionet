<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use common\models\MasterStore;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model common\models\MasterTypeSeatSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="master-type-seat-search">

     <?php
        $form = ActiveForm::begin([
                'action' => ['index'],
                'enableClientValidation' => false,
                'method' => 'get',
                'layout' => 'horizontal',
                'enableClientValidation' => false,
                'fieldConfig' => [
                    'template' => "{beginWrapper}<div class='col-md-3 label_master_ticket_ct'>{label}</div><div class='col-md-4 ip_ticket_store'>{input}\n{hint}\n{error}</div>\n{endWrapper}",
                    'horizontalCssClasses' => [
                        'label' => '',
                        'wrapper' => 'row',
                        'error' => '',
                        'hint' => '',
                    ],
                ],
        ]);
    ?>
    
    <?= $form->field($model, 'store_id')->dropDownList(MasterStore::getListStoreTypeSeat(),['prompt'=>Yii::t('backend', 'Please Select')]) ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => 20])?>

    <div class="form-group">
       <a href="<?php echo Url::to('/admin') ?>" class="btn common-button-default btn-default">
       <?php echo Yii::t('backend','Return') ?></a></button>
       <?= Html::submitButton(Yii::t('backend', 'Search'), ['class' => 'btn common-button-submit']) ?>
        
    </div>

    <?php ActiveForm::end(); ?>

</div>
