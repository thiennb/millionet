<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\components\Constants;
use yii\jui\DatePicker;
use common\models\MasterStore;
use kartik\depdrop\DepDrop;
use yii\helpers\Url;
use yii\bootstrap\Modal;
use common\widgets\preview\Preview;
use yii\widgets\Pjax;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $model common\models\MasterEnjoyLottery */
/* @var $form yii\widgets\ActiveForm */
$fieldOptions = [
    'options' => ['class' => 'col-md-4 no-padding input_date_form_enjoy_lottery'],
    'template' => '<div class="clear-padding">{input}{error}</div>'
];
$fieldOptions1 = [
    'options' => ['class' => 'col-md-2 no-padding'],
    'template' => '<div class="clear-padding">{input}{error}</div>'
];
$fieldOptions2 = [
    'options' => ['class' => 'col-md-3 no-padding'],
    'template' => '<div class="clear-padding">{input}{error}</div>'
];
$fieldOptions3 = [
    'options' => ['class' => 'col-md-12 no-padding'],
    'template' => '<div class="clear-padding">{input}{error}</div>'
];
$this->registerJsFile('@web/js/enjoy_lottery.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
?>

<div class="master-enjoy-lottery-form">
    <div class="col-md-12 no-padding">
        <div class="col-md-10">
            <?php
            $form = ActiveForm::begin([
                        'options' => [
                            'enctype' => 'multipart/form-data',
                            'id' => strtolower($model->formName()) . '-id',
                        ], // important
                        'validationUrl' => 'validate',
                        'enableClientValidation' => false,
                        'enableAjaxValidation' => true,
                        'validateOnChange' => false,
                        'validateOnBlur' => false,
                        'successCssClass' => '',
            ]);
            ?>
            <?= $form->field($model, 'customer_id_hd')->hiddenInput(['id' => 'customer_id_hd'])->label(FALSE) ?>
            <div class="row col-md-12 row-inline form-group">
                <div class="box-header with-border common-box-h4 col-md-6 ">
                    <h4 class="text-title"><b><?= Html::encode(Yii::t('backend','Input Winning information')) ?></b></h4>
                </div>
            </div>
            <div class="row col-md-12 row-inline">
                <div class="col-md-3 label_form_enjoy_lottery">
                    <label class='mws-form-label required-star'>
                        <?= Yii::t('backend', 'Winner Announcement Date') ?>
                    </label>
                </div>
                <div class="col-md-9 ">
                    <?=
                            $form->field($model, 'winner_announcement_date', $fieldOptions)
                            ->widget(DatePicker::classname(), [
                                'language' => 'ja',
                                'dateFormat' => 'yyyy/MM/dd',
                                'clientOptions' => [
                                    "changeMonth" => true,
                                    "changeYear" => true,
                                    "yearRange" => "1900:+0"
                                ],
                            ])
                    ?>
                    <div class="text-center col-md-2 wt_ct"></div>
                    <?=
                            $form->field($model, 'winner_announcement_time', $fieldOptions2)
                            ->dropDownList(Constants::LIST_OPTION_SELECT_TIME, ['maxlength' => true, 'class' => 'form-control add-time-off'])
                            ->label(FALSE)
                    ?>
                </div>
            </div>
            <div class="row col-md-12 row-inline form-group">
                <div class="box-header with-border common-box-h4 col-md-6 ">
                    <h4 class="text-title"><b><?= Html::encode(Yii::t('backend','Winning condition')) ?></b></h4>
                </div>
            </div>
            <div class="row col-md-12 row-inline">
                <div class="col-md-3 label_form_enjoy_lottery">
                    <label class='mws-form-label'>
                        <?= Yii::t('backend', 'Winning Numbers') ?>
                    </label>
                </div>
                <div class="col-md-9 ">
                    <?=
                            $form->field($model, 'winning_numbers', $fieldOptions1)
                            ->widget(\yii\widgets\MaskedInput::className(), [
                                'clientOptions' => [
                                    'alias' => 'integer',
                                    'autoGroup' => true,
                                    'removeMaskOnSubmit' => true,
                                    'allowMinus' => false,
                                    'groupSeparator' => ',',
                                ]
                            ])->textInput(['maxlength' => 5])
                    ?>
                </div>
            </div>
            <div class="row col-md-12 row-inline">
                <div class="col-md-3 label_form_enjoy_lottery">
                    <label class='mws-form-label'>
                        <?= Yii::t('backend', 'Store Ticket') ?>
                    </label>
                </div>
                <div class="col-md-9 ">
                    <?=
                            $form->field($model, 'store_id', $fieldOptions)
                            ->dropDownList(MasterStore::getListStore(), ['prompt' => Yii::t('backend', 'Please Select')])
                    ?>
                </div>
            </div>
            <div class="row col-md-12 row-inline">
                <div class="col-md-3 label_form_enjoy_lottery">
                    <label class='mws-form-label'>
                        <?= Yii::t('backend', 'Period Start List Search') ?>
                    </label>
                </div>
                <div class="col-md-9 ">
                    <?=
                            $form->field($model, 'start_winner_period_date', $fieldOptions)
                            ->widget(DatePicker::classname(), [
                                'language' => 'ja',
                                'dateFormat' => 'yyyy/MM/dd',
                                'clientOptions' => [
                                    "changeMonth" => true,
                                    "changeYear" => true,
                                    "yearRange" => "1900:+0"
                                ],
                            ])
                    ?>
                    <div class="text-center col-md-1 separator">～</div>
                    <?=
                            $form->field($model, 'end_winner_period_date', $fieldOptions)
                            ->widget(DatePicker::classname(), [
                                'language' => 'ja',
                                'dateFormat' => 'yyyy/MM/dd',
                                'clientOptions' => [
                                    "changeMonth" => true,
                                    "changeYear" => true,
                                    "yearRange" => "1900:+0"
                                ],
                            ])
                    ?>
                </div>
            </div>

            <div class="enjoy-lottery-toggle row col-md-12 form-group" data-toggle="collapse" data-target="#togetther-collapse">
                <?php echo Yii::t('backend', 'List Details') ?>
            </div>

            <div id="togetther-collapse" class="collapse col-md-12 no-padding">

                <div class="row col-md-12 row-inline">
                    <div class="col-md-3 label_form_enjoy_lottery">
                        <label class='mws-form-label'>
                            <?= Yii::t('backend', 'Earn Period From') ?>
                        </label>
                    </div>
                    <div class="col-md-9 ">
                        <?php $model->earn_period_from_min = empty($model->earn_period_from_min)?0:$model->earn_period_from_min; ?>
                        <?=
                                $form->field($model, 'earn_period_from_min', $fieldOptions)
                                ->widget(\yii\widgets\MaskedInput::className(), [
                                    'clientOptions' => [
                                        'alias' => 'integer',
                                        'autoGroup' => true,
                                        'removeMaskOnSubmit' => true,
                                        'allowMinus' => false,
                                        'groupSeparator' => ',',
                                    ]
                                ])->textInput(['maxlength' => 13])
                        ?>
                        <div class="text-center col-md-1 separator">～</div>
                        <?=
                                $form->field($model, 'earn_period_from_max', $fieldOptions)
                                ->widget(\yii\widgets\MaskedInput::className(), [
                                    'clientOptions' => [
                                        'alias' => 'integer',
                                        'autoGroup' => true,
                                        'removeMaskOnSubmit' => true,
                                        'allowMinus' => false,
                                        'groupSeparator' => ',',
                                    ]
                                ])->textInput(['maxlength' => 13])
                        ?>
                    </div>
                </div>
                <div class="row col-md-12 row-inline">
                    <div class="col-md-3 label_form_enjoy_lottery">
                        <label class='mws-form-label'>
                            <?= Yii::t('backend', 'Period Coming Times From') ?>
                        </label>
                    </div>
                    <div class="col-md-9 ">
                        <?php $model->visit_store_min = empty($model->visit_store_min)?0:$model->visit_store_min; ?>
                        <?=
                                $form->field($model, 'visit_store_min', $fieldOptions)
                                ->widget(\yii\widgets\MaskedInput::className(), [
                                    'clientOptions' => [
                                        'alias' => 'integer',
                                        'autoGroup' => true,
                                        'removeMaskOnSubmit' => true,
                                        'allowMinus' => false,
                                        'groupSeparator' => ',',
                                    ]
                                ])->textInput(['maxlength' => 5])
                        ?>
                        <div class="text-center col-md-1 separator">～</div>
                        <?php $model->visit_store_max = empty($model->visit_store_max)?100:$model->visit_store_max; ?>
                        <?=
                                $form->field($model, 'visit_store_max', $fieldOptions)
                                ->widget(\yii\widgets\MaskedInput::className(), [
                                    'clientOptions' => [
                                        'alias' => 'integer',
                                        'autoGroup' => true,
                                        'removeMaskOnSubmit' => true,
                                        'allowMinus' => false,
                                        'groupSeparator' => ',',
                                    ]
                                ])->textInput(['maxlength' => 5])
                        ?>
                    </div>
                </div>
                <div class="row col-md-12 row-inline">
                    <div class="col-md-3 label_form_enjoy_lottery">
                        <label class='mws-form-label'>
                            <?= Yii::t('backend', 'Rank') ?>
                        </label>
                    </div>
                    <div class="col-md-9 ">
                        <?=
                                $form->field($model, 'rank_id', $fieldOptions3)
                                ->checkboxList(Constants::RANK, ['prompt' => Yii::t('backend', 'Please Select')])
                        ?>
                    </div>
                </div>

                <div class="row col-md-12 row-inline">
                    <div class="col-md-3 label_form_enjoy_lottery">
                        <label class='mws-form-label'>
                            <?= Yii::t('backend', 'Sex') ?>
                        </label>
                    </div>
                    <div class="col-md-9 ">
                        <?=
                                $form->field($model, 'sex', $fieldOptions)
                                ->dropDownList(Constants::LIST_SEX, ['prompt' => Yii::t('backend', 'Please Select')])
                        ?>
                    </div>
                </div>

                <div class="row col-md-12 row-inline">
                    <div class="col-md-3 label_form_enjoy_lottery">
                        <label class='mws-form-label'>
                            <?= Yii::t('backend', 'Birth Date') ?>
                        </label>
                    </div>
                    <div class="col-md-9 ">
                        <?=
                                $form->field($model, 'birth_date', $fieldOptions)
                                ->widget(DatePicker::classname(), [
                                    'language' => 'ja',
                                    'dateFormat' => 'yyyy/MM/dd',
                                    'clientOptions' => [
                                        "changeMonth" => true,
                                        "changeYear" => true,
                                        "yearRange" => "1900:+0"
                                    ],
                                ])
                        ?>
                    </div>
                </div>
                <div class="row col-md-12 row-inline">
                    <div class="col-md-3 label_form_enjoy_lottery">
                        <label class='mws-form-label'>
                            <?= Yii::t('backend', 'Careful Check Index') ?>
                        </label>
                    </div>
                    <div class="col-md-9 ">
                        <?=
                                $form->field($model, 'black_list_check', $fieldOptions)
                                ->dropDownList(Constants::CAREFUL_CHECK, ['prompt' => Yii::t('backend', 'Please Select')])
                        ?>
                    </div>
                </div>
                <div class="row col-md-12 row-inline">
                    <div class="col-md-3 label_form_enjoy_lottery">
                        <label class='mws-form-label'>
                            <?= Yii::t('backend', 'Member Specified') ?>
                        </label>
                    </div>
                    <div class="col-md-9 ">
                        <div class="col-md-12 no-padding">
                            <div class="col-md-3 no-padding">
                                <?php $model->member_specified = !empty($customer) ? true : false; ?>
                                <?= $form->field($model, 'member_specified')->checkbox() ?>
                            </div>
                            <div class="col-md-9" id='name_member'>
                                <?= (!empty($customer->first_name) && !empty($customer->last_name)) ? $customer->first_name . $customer->last_name : '' ?>
                            </div>
                        </div>
                        <div class="col-md-12 no-padding form-group" id='search_member'>
                            <div class="col-md-3 no-padding member_code_enjoy_lottery_ct">
                                <input type="text" class="form-control" id='member_code' placeholder="<?= Yii::t('backend', 'Member Specified Label') ?>" maxlength="13" value="<?= Html::encode((!empty($customer->customer_jan_code) ? $customer->customer_jan_code : '')) ?>"/>
                            </div>
                            <div class="col-md-1 with_cl1_ct"></div>
                            <div class="col-md-2 no-padding">
                                <input type="text" class="form-control" id='member_first_name' placeholder="<?= Yii::t('backend', 'Surname') ?>" maxlength="40" value="<?= Html::encode((!empty($customer->first_name) ? $customer->first_name : '')) ?>"/>
                            </div>
                            <div class="col-md-1 with_cl1_ct"></div>
                            <div class="col-md-2 no-padding">
                                <input type="text" class="form-control" id='member_last_name' placeholder="<?= Yii::t('backend', 'Last name kana') ?>"maxlength="40" value="<?= Html::encode((!empty($customer->last_name) ? $customer->last_name : '')) ?>"/>
                            </div>
                            <div class="col-md-3">
                                <input type="button" value="<?= Yii::t('backend', 'Search') ?>" class="btn common-button-action" onclick="search_member_name(this)"/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row col-md-12 row-inline">
                    <div class="col-md-3 label_form_enjoy_lottery">
                        <label class='mws-form-label'>
                            <?= Yii::t('backend', 'Register staff') ?>
                        </label>
                    </div>
                    <div class="col-md-9 ">
                        <?=
                                $form->field($model, 'staff_id_1', $fieldOptions2)
                                ->widget(DepDrop::classname(), [
                                    'options' => ['id' => 'masterenjoylottery-staff_id_1'],
                                    'pluginOptions' => [
                                        //'init' => true,
                                        'depends' => ['masterenjoylottery-store_id'],
                                        'placeholder' => ' ',
                                        'url' => Url::to(['/master-customer/select-staff-by-id-store'])
                                    ]
                        ]);
                        ?>
                        <div class="text-center col-md-1 with_cl1_ct"></div>
                        <?=
                                $form->field($model, 'staff_id_2', $fieldOptions2)
                                ->widget(DepDrop::classname(), [
                                    'options' => ['id' => 'masterenjoylottery-staff_id_2'],
                                    'pluginOptions' => [
                                        //'init' => true,
                                        'depends' => ['masterenjoylottery-store_id'],
                                        'placeholder' => ' ',
                                        'url' => Url::to(['/master-customer/select-staff-by-id-store'])
                                    ]
                        ]);
                        ?>
                        <div class="text-center col-md-1 with_cl1_ct"></div>
                        <?=
                                $form->field($model, 'staff_id_3', $fieldOptions2)
                                ->widget(DepDrop::classname(), [
                                    'options' => ['id' => 'masterenjoylottery-staff_id_3'],
                                    'pluginOptions' => [
                                        //'init' => true,
                                        'depends' => ['masterenjoylottery-store_id'],
                                        'placeholder' => ' ',
                                        'url' => Url::to(['/master-customer/select-staff-by-id-store'])
                                    ]
                        ]);
                        ?>
                    </div>
                </div>
            </div>    
            <div class="form-group col-md-12">
                <?=
                Html::button(Yii::t('backend', 'Display the winning numbers'), [
                    'class' => 'btn common-button-submit',
                    'id' => 'btn-search-customer',
                ])
                ?>                
            </div>
            <div class="row col-md-12 form-group">
                <div class="box-header with-border common-box-h4 col-md-6 ">
                    <h4 class="text-title"><b><?= Html::encode(Yii::t('backend','Winner number selection')) ?></b></h4>
                </div>
                <div class="row col-md-12 form-group">
                    <?= $form->field($model, 'count_winner', ['template' => "{input}{error}"])->hiddenInput(['value' => (int)$count_winner])->label(FALSE) ?>
                </div>
            </div>
        </div>
    </div>
    <?php Pjax::begin(['enablePushState' => false]); ?>
    <div class="col-md-12">
        <div class="col-md-12" id="lottery_customer">
            <?php if(count($list_member_search->models) > 0){  ?>
            <?=            
            GridView::widget([
                'dataProvider' => $list_member_search,
                'layout' => "{pager}\n{items}",
                'summary' => false,
                'pager' => [
                    'firstPageLabel' => '<<',
                    'lastPageLabel' => '>>',
                    'prevPageLabel' => '<',
                    'nextPageLabel' => '>',
                    'options' => ['class' => 'pagination common-float-right'],
                ],
                'tableOptions' => [
                    'class' => 'table table-striped table-bordered text-center',
                    'id' => 'enjoy_lottery_table',
                ],
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn', 'header' => 'No', 'options' => ['class' => 'with-table-no']],
                    [
                        'attribute' => 'lotery',
                        'label' => Yii::t('backend', 'Enjoy number'),
                    ],
                    [
                        'attribute' => 'order_code',
                        'label' => Yii::t('backend', 'Order Code'),
                    ],
                    [
                        'attribute' => 'process_date',
                        'label' => Yii::t('backend', 'Sales date'),
                    ],
                    [
                        'attribute' => 'customer_first_name',
                        'label' => Yii::t('backend', 'Member name'),
                        'value' => function ($model) {
                            return $model->customer_first_name . $model->customer_last_name;
                        }
                    ],
                    [
                        'attribute' => 'customer_visit_number',
                        'label' => Yii::t('backend', 'Visit number'),
                    ],
                    [
                        'attribute' => 'total',
                        'label' => Yii::t('backend', 'Sales'),
                    ],
                ],
            ]);
            ?>
            <?php }?>
            <?= Html::a(Yii::t('backend', 'Refrest'), Url::current(), ['style' => 'display: none', 'id' => 'refresh-data-table']) ?>
        </div>
    </div>
    <?php Pjax::end(); ?>
    <?php
    $date_time = $model->winner_announcement_date . ' ' . $model->winner_announcement_time;
    if (!empty($date_time) && $date_time != ' ') {
        $date_time = (string) date('Y年 m月 d日  H時 i分', strtotime($date_time));
    }
    ?>
    <input type="hidden" id="masterenjoylottery-winner_announcement_date_time" value="<?= $date_time ?>"/>
    <div class="form-group col-md-12">
        <?= Html::a(Yii::t('backend', 'Return'), ['index'], ['class' => 'btn common-button-default btn-default']) ?>
        <?=
        Html::submitButton(Yii::t('backend', 'Confirm'), [
            'class' => 'btn common-button-submit',
            'id' => 'btn-confirm',
        ])
        ?>
    </div>
    <?php
    Modal::begin([
        'id' => 'userModal',
        'size' => 'SIZE_LARGE',
        'header' => '<div class="box-header with-border common-box-h4 col-md-8"><h4 class="text-title"><b>' . Yii::t('backend', 'Confirm Notice') . '</b></h4></div>',
        'footer' => Html::button(Yii::t('backend', 'Return'), ['class' => 'btn common-button-default', 'data-dismiss' => "modal", 'id' => 'btn-close'])
        . Html::submitButton(Yii::t('backend', 'Save'), ['class' => 'btn common-button-submit', 'id' => 'btn-submit']),
        'closeButton' => FALSE,
    ]);
    ?>
    <?=
    Preview::widget([
        "data" => [
            'winner_announcement_date_time' => [
                'type' => 'input_hidden',
                'label' => Yii::t('backend', Yii::t('backend', 'Winner Announcement Date'))
            ],
            'winning_numbers' => [
                'type' => 'input',
                'label' => Yii::t('backend', Yii::t('backend', 'Winning Numbers')),
                'syb' => ' 件'
            ],
            'enjoy_lottery_table' => [
                'type' => 'table-normal',
                'column' => '"1,2,3,4,5,6"',
                'table' => 'enjoy_lottery_table'
            ],
        ],
        "modelName" => $model->formName(),
        'idBtnConfirm' => 'btn-confirm',
        'formId' => strtolower($model->formName()) . '-id',
        'btnClose' => 'btn-close',
        'btnSubmit' => 'btn-submit',
        'modalId' => 'userModal'
    ])
    ?>
    <?php
    Modal::end();
    ?>
    <?php ActiveForm::end(); ?>

</div>
