<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel common\models\MstProductSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('backend', 'Master Enjoy Lotteries');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <div class="col-lg-12 col-md-12">
        <div class="box box-info box-solid">
            <div class="box-header with-border">
                <h4 class="text-title"><b><?= $this->title ?></b></h4>
            </div>
            <div class="box-body content">
                <div class="col-md-12 clear-padding">
                    <div class="common-box">
                        <div class="col-md-12">
                            <div class="box-header with-border common-box-h4 col-md-3">
                                <h4 class="text-title"><b><?= Yii::t('backend', 'Enjoy Lotteries Search') ?></b></h4>
                            </div>
                        </div>                       
                        
                        <div class="box-body content col-md-12">
                            <div class="col-md-5">
                                <?php echo $this->render('_search', ['model' => $searchModel]); ?>                                
                            </div>
                        </div>
                    
                        <div class="box-body content row col-md-12">
                            <div class="row">
                                <div class="box-header with-border common-box-h4 col-md-3">
                                    <h4 class="text-title"><b><?= Yii::t('backend', 'Enjoy Lotteries List') ?></b></h4>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-10">
                                    <?= Html::a(Yii::t('backend', 'New registration'), ['create'], ['class' => 'btn common-button-submit common-float-right margin-bottom-10']) ?>
                                    <?php Pjax::begin(); ?>   
                                    <?=
                                    GridView::widget([
                                        'dataProvider' => $dataProvider,
                                        'tableOptions' => [
                                            'class' => 'table table-striped table-bordered text-center',
                                        ],
                                        'layout' => "{pager}\n{summary}\n{items}",
                                                    'summaryOptions' => ['class' => 'common-clr-fl-right'],
                                                    'summary'=>false,
                                                    'pager' => [
                                                          'class' => 'common\components\LinkPagerMillionet',
                                                          'options' => ['class' => 'pagination common-float-right'],

                                                      ],
                                        'columns' => [
                                            ['class' => 'yii\grid\SerialColumn', 'header' => 'No', 'options' => ['class' => 'with-table-no col-xs-1']],

                                            [
                                                'attribute' => 'register_date',
                                                'label'=>Yii::t('backend','Register date from List Search'),
                                                'value' => function ($model) {
                                                    return date('Y/m/d',  strtotime($model->register_date));
                                                },
                                                'headerOptions' => [
                                                    'class' => ['col-md-2']
                                                ]
                                            ],

                                            [
                                                'attribute' => 'start_winner_period_date',
                                                'label'=>Yii::t('backend','Period Start'),
                                                'value' => function ($model) {
                                                    $date_from = !empty($model->start_winner_period_date)?date('Y/m/d',  strtotime($model->start_winner_period_date)):'';
                                                    $date_to = !empty($model->end_winner_period_date)?date('Y/m/d',  strtotime($model->end_winner_period_date)):'';
                                                    $tmp =  (!empty($model->start_winner_period_date) || !empty($model->end_winner_period_date))?'～':'';
                                                    return $date_from.$tmp.$date_to;
                                                },
                                                'headerOptions' => [
                                                    'class' => ['col-md-2']
                                                ]

                                            ],

                                            [
                                                'attribute' => 'winning_numbers',
                                                'label'=>Yii::t('backend','Winning Numbers List Search'),    
                                                'headerOptions' => [
                                                    'class' => ['col-md-1']
                                                ]

                                            ],

                                             [
                                                'attribute' => 'status_winer',
                                                'label'=>Yii::t('backend','Status List Search'),
                                                'value' => function ($model) {
                                                    return (date('Y/m/d')>date('Y/m/d',  strtotime($model->winner_announcement_date)) || $model->status_winer == \common\models\MasterEnjoyLottery::AFTER_PUBLISH)
                                                            ?Yii::t('backend','After the presentation'):Yii::t('backend','Before the presentation');
                                                },       
                                                'headerOptions' => [
                                                    'class' => ['col-md-2']
                                                ]

                                            ],
                                            [
                                                'attribute' => 'winner_announcement_date',
                                                'label'=>Yii::t('backend','Presentation Date Lottery'),
                                                'value' => function ($model) {
                                                    if(!empty($model->winner_announcement_date)){ return date('Y/m/d',  strtotime($model->winner_announcement_date)).' '.$model->winner_announcement_time;}else{return '';}
                                                },       
                                                'headerOptions' => [
                                                    'class' => ['col-md-2']
                                                ]

                                            ],


                                            [
                                                'class' => 'yii\grid\ActionColumn', 'template' => "<div class='text-right'>{delete} {update}</div>",
                                                'buttons' => [
                                                    //view button
                                                    'delete' => function ($url, $model) {
                                                        if(date('Y/m/d')>date('Y/m/d',  strtotime($model->winner_announcement_date)) || $model->status_winer == \common\models\MasterEnjoyLottery::AFTER_PUBLISH){
                                                            return;
                                                        }else{
                                                            return Html::a('<span class="fa fa-delete"></span>'.Yii::t('backend', 'Delete'), $url, [
                                                                    'class' =>'btn common-button-action',
                                                                    'aria-label'=>"Delete",
                                                                    'data-confirm'=>Yii::t('app',"Are you sure you want to delete?"),
                                                                    'data-method'=>"post"
                                                                  ]);
                                                        }
                                                    },
                                                    'update' => function ($url, $model) {
                                                        return Html::a('<span class="fa fa-delete"></span>'.Yii::t('backend', 'Update'), $url, [
                                                                'class' =>'btn common-button-action' , 
                                                              ]);
                                                    },
                                                ],
                                                'headerOptions' => [
                                                    'class' => ['col-md-2']
                                                ]
                                            ],
                                        ],
                                    ]);
                                    ?>                                   
                                    <?php
                                    Pjax::end();

                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

