<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\jui\DatePicker;
use common\components\Constants;
use common\models\MasterStore;

/* @var $this yii\web\View */
/* @var $model common\models\MasterEnjoyLotterySearch */
/* @var $form yii\widgets\ActiveForm */
?>
<?php
$fieldOptions = [
    'options' => ['class' => 'col-md-4 no-padding input_date_enjoy_lottery'],
    'template' => '<div class="clear-padding">{input}{error}</div>'
];
$fieldOptions1 = [
    'options' => ['class' => 'col-md-8 no-padding'],
    'template' => '<div class="clear-padding">{input}{error}</div>'
];
$fieldOptions2 = [
    'options' => ['class' => 'col-md-2 no-padding input_date_enjoy_lottery'],
    'template' => '<div class="clear-padding">{input}{error}</div>'
];
?>
<div class="master-enjoy-lottery-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'enableClientValidation'=>false,
    ]); ?>
    <div class="row row-inline">
        <div class="col-md-3 label_enjoy_lottery">
            <label class='mws-form-label'>
                <?= Yii::t('backend', 'Store Id') ?>
            </label>
        </div>
        <div class="col-md-9 ">
            <?php $list_store = MasterStore::getListStore();
            if(count($list_store) == 1){
                echo $form->field($model, 'store_id', $fieldOptions1)
                ->dropDownList(MasterStore::getListStore());
            } else{
                echo $form->field($model, 'store_id', $fieldOptions1)
                ->dropDownList(MasterStore::getListStore(), ['prompt' => Yii::t('backend', 'Please Select')]);
            }
            ?>
        </div>
    </div>
    
    <div class="row row-inline">
        <div class="col-md-3 label_enjoy_lottery">
            <label class='mws-form-label '>
                <?= Yii::t('backend', 'Period Start Index') ?>
            </label>
        </div>
        <div class="col-md-9 ">
            <?=
            $form->field($model, 'start_winner_period_date', $fieldOptions)
            ->widget(DatePicker::classname(), [
                'language' => 'ja',
                'dateFormat' => 'yyyy/MM/dd',
                'clientOptions' => [
                    "changeMonth" => true,
                    "changeYear" => true,
                    "yearRange" => "1900:+0"
                ],
            ])
            ?>
            <div class="text-center col-md-1 separator">～</div>
            <?=
            $form->field($model, 'end_winner_period_date', $fieldOptions)
            ->widget(DatePicker::classname(), [
                'language' => 'ja',
                'dateFormat' => 'yyyy/MM/dd',
                'clientOptions' => [
                    "changeMonth" => true,
                    "changeYear" => true,
                    "yearRange" => "1900:+0"
                ],
            ])
            ?>
        </div>
    </div>
    
    <div class="row row-inline">
        <div class="col-md-3 label_enjoy_lottery">
            <label class='mws-form-label'>
                <?= Yii::t('backend','Register date from') ?>
            </label>
        </div>
        <div class="col-md-9 ">
            <?=
            $form->field($model, 'start_register_period_date', $fieldOptions)
            ->widget(DatePicker::classname(), [
                'language' => 'ja',
                'dateFormat' => 'yyyy/MM/dd',
                'clientOptions' => [
                    "changeMonth" => true,
                    "changeYear" => true,
                    "yearRange" => "1900:+0"
                ],
            ])
            ?>
            <div class="text-center col-md-1 separator">～</div>
            <?=
            $form->field($model, 'end_register_period_date', $fieldOptions)
            ->widget(DatePicker::classname(), [
                'language' => 'ja',
                'dateFormat' => 'yyyy/MM/dd',
                'clientOptions' => [
                    "changeMonth" => true,
                    "changeYear" => true,
                    "yearRange" => "1900:+0"
                ],
            ])
            ?>
        </div>
    </div>
    
    <div class="row row-inline">
        <div class="col-md-3 label_enjoy_lottery">
            <label class='mws-form-label'>
                <?= Yii::t('backend','Winning Numbers Index') ?>
            </label>
        </div>
        <div class="col-md-9 ">
            <?=
            $form->field($model, 'number_bill_min', $fieldOptions2)
            ->widget(\yii\widgets\MaskedInput::className(), [
                'clientOptions' => [
                    'alias' => 'integer',
                    'autoGroup' => true,
                    'removeMaskOnSubmit' => true,
                    'allowMinus' => false,
                    'groupSeparator' => ',',
                ]
            ])->textInput(['maxlength' => 5])
            ?>
            <div class="text-center col-md-1 separator">～</div>
            <?=
            $form->field($model, 'number_bill_max', $fieldOptions2)
            ->widget(\yii\widgets\MaskedInput::className(), [
                'clientOptions' => [
                    'alias' => 'integer',
                    'autoGroup' => true,
                    'removeMaskOnSubmit' => true,
                    'allowMinus' => false,
                    'groupSeparator' => ',',
                ]
            ])->textInput(['maxlength' => 5])
            ?>
        </div>
    </div>
    <div class="row row-inline">
        <div class="col-md-3 label_enjoy_lottery">
            <label class='mws-form-label '>
                <?= Yii::t('backend','Careful Check Index') ?>
            </label>
        </div>
        <div class="col-md-9 ">
            <?= $form->field($model, 'black_list_check', $fieldOptions1)
            ->dropDownList(Constants::CAREFUL_CHECK,['prompt'=>Yii::t('backend', 'Please Select')]) ?>
        </div>
    </div>
    <div class="row row-inline">
        <div class="col-md-3 label_enjoy_lottery">
            <label class='mws-form-label'>
                <?= Yii::t('backend','Status lottery') ?>
            </label>
        </div>
        <div class="col-md-9 ">
            <?= $form->field($model, 'status_winer', $fieldOptions1)
            ->dropDownList(Constants::LIST_STATUS_ENJOY_LOTTERY,['prompt'=>Yii::t('backend', 'Please Select')]) ?>
        </div>
    </div>
    
    <div class="form-group">
        <?= Html::a(Yii::t('backend', 'Return'), ['/'], ['class' => 'btn common-button-default btn-default']) ?>
        <?= Html::submitButton(Yii::t('backend', 'Search'), ['class' => 'btn common-button-submit']) ?>
        
    </div>

    <?php ActiveForm::end(); ?>

</div>
