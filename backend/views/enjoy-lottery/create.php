<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\MasterEnjoyLottery */

$this->title = Yii::t('backend', 'Create Master Enjoy Lottery');
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Master Enjoy Lotteries'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row text-setting company">
    <div class="col-lg-12 col-md-12">
        <div class="box box-info box-solid add">
            <div class="box-header with-border">
                <h4 class="text-title"><b><?= $this->title ?></b></h4>
            </div>
            <div class="box-body content">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-8">
                            <?= $this->render('_form', [
                                'model' => $model,
                                'list_member_search' => $list_member_search,
                                'customer'  =>  $customer,
                                'count_winner'  =>  $count_winner,
                             ]) ?>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
</div>
