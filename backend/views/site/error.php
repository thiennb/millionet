<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

$this->title = $name;
?>
<section class="content">

    <div class="error-page">
        <h2 class="headline text-info"><i class="fa fa-warning text-yellow"></i></h2>

        <div class="error-content">
            <h3><?= 'システムエラー' ?></h3>

            <p>
                <?= nl2br(Html::encode($message)) ?>
            </p>

            <p>
              申し訳ございません。システムエラーが発生しました。<br>
                恐れ入りますが、一度画面を閉じて再度アクセスしていただくか、<br>
                しばらく経ってからご利用ください。<br>
            </p>

            
        </div>
    </div>

</section>
