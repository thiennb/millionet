<?php
use Carbon\Carbon;
use yii\helpers\Html;
$this->title = Yii::$app->name;

$yen = Yii::t('backend', 'Yen');
$people = Yii::t('backend', 'People');
?>
<div>
    <div class="row">
        <div class="col-md-12">
            <h3 class="top-date-time pull-left">
                <b><?= date('Y') . Yii::t('backend','Year'). date('m') . Yii::t('backend','Month') . date('d') .Yii::t('backend','Day'). "(".Yii::t('backend',date('l')).")" ?>
                <?= date('H:i') .' '.Yii::t('backend','Time') ?>
                </b>
            </h3>
            <?php if(!empty($stores)) : ?>
            <div class="form-group pull-right list-store-backend">
                <label for="list_store"><?= Yii::t('backend', 'List Stores') ?></label>
                <select class="form-control" id="list_store">
                    <option value="all"><?= Yii::t('backend', 'All Stores') ?></option>
                    <?php foreach($stores as $store) : ?>
                    <option value="<?= $store['store_code']?>"><?= $store['name']?></option>
                    <?php endforeach; ?>
                </select>
            </div>
            <?php endif; ?>
        </div>
    </div>
    <div class="row">
        <div class="index-info">
            <div class="col-md-10">
                <div class="row">
                    <div class="col-md-4 font_label font_size_header_top text-center label_top_admin_ct">
                        <p class="info-header-label"><?= Yii::t('backend', 'Sum Sales') ?></p>
                        <div class="font_label color_price_top text-center">
                            <span id="sum_sale_date"><?= Yii::$app->formatter->asDecimal($total_money_date, 0) ?></span> <?= $yen ?>
                        </div>
                    </div>
                    <div class="col-md-4 font_label font_size_header_top text-center label_top_admin_ct">
                        <p class="info-header-label"><?= Yii::t('backend', 'Number of customers') ?></p>
                        <div class="font_label color_price_top text-center">
                            <span id="total_people_date"><?=  $total_people_date ?></span> <?= $people?>
                            <div class="text-below">
                                <span>（ <?= Yii::t('backend', 'Creates') ?> 0 <?= $people ?> ）</span>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4 font_label font_size_header_top text-center label_top_admin_ct">
                       <p class="info-header-label"><?= Yii::t('backend', 'Average spending per customer') ?></p>
                       <div class="font_label color_price_top text-center">
                            <span id="average_money_date"><?= Yii::$app->formatter->asDecimal((float)$average_money_date,0) ?></span> <?= $yen ?>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4 font_label font_size_header_top text-center label_top_admin_ct">
                        <p class="info-header-label"><?= Yii::t('backend', 'Numbers of booking') ?></p>
                        <div class="font_label color_price_top text-center">
                            <span id="count_booking_date"><?=  $count_booking_date ?></span> <?= Yii::t('backend', 'Item') ?>
                            <div class="text-below">
                                <span>（ <?= Yii::t('backend', 'Delivery') ?> <span id="item_delivery_date"><?= $item_delivery_date ?></span> <?= Yii::t('backend', 'Item') ?> ）</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 font_label font_size_header_top text-center label_top_admin_ct">
                       <p class="info-header-label"><?= Yii::t('backend', 'Total number of customers away') ?></p>
                       <div class="font_label color_price_top text-center">
                            <span id="total_away_people"><?= $total_away_people ?></span> <?= $people ?>
                        </div>
                    </div>

                    <div class="col-md-4 font_label font_size_header_top text-center label_top_admin_ct">
                       <p class="info-header-label"><?= Yii::t('backend', 'Away gimmick number of customers') ?></p>
                       <div class="font_label color_price_top text-center">
                            <span id="total_away_gimmick_people"><?= $total_away_gimmick_people ?></span> <?= $people ?>
                       </div>
                    </div>
                </div>

                <div class="row" style="margin-top: 2em;">
                    <div class="col-md-4 font_label font_size_header_top text-center label_top_admin_ct">
                        <p class="info-header-label"><?= Yii::t('backend', 'This month') ?> <?= Yii::t('backend', 'Total') ?> <?= Yii::t('backend', 'Sales') ?></p>
                        <div class="font_label color_price_top text-center">
                            <span id="total_money_month"><?= Yii::$app->formatter->asDecimal($total_money_month,0) ?></span> <?= $yen ?>
                        </div>
                    </div>

                    <div class="col-md-4 font_label font_size_header_top text-center label_top_admin_ct">
                        <p class="info-header-label"><?= Yii::t('backend', 'This month') ?> <?= Yii::t('backend', 'Total') ?> <?= Yii::t('backend', 'Customer') ?></p>
                        <div class="font_label color_price_top text-center">
                            <span id="total_people_month"><?= $total_people_month ?></span> <?= $people ?>
                         </div>
                    </div>
                    <div class="col-md-4 font_label font_size_header_top text-center label_top_admin_ct">

                    </div>
                </div>
            </div>
            <div class="col-md-10">
                <div class="graph-sale-custom">
                    <div class="row">
                        <div class="col-md-1"><span class="left-graph"><?= Yii::t('backend','Number of customers') ?></div>
                        <div class="col-md-10 chart_top_admin_ct"><canvas id="canvas"></canvas></div>
                        <div class="col-md-1"><span class="right-graph"><?= Yii::t('backend', 'Sales') ?></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php if (count($system_messages)) : ?>
        <div class="row" style="margin: 30px 0">
            <div class="col-xs-10">
                <h4 style="font-weight: bold; border-bottom: solid 1px #444;padding-bottom: 5px; margin: 0">お知らせ</h4>
                <div class="panel-group" id="system_messages">
                    <?php foreach ($system_messages as $msg): ?>
                        <div class="panel panel-default system-message">
                          <div class="panel-heading" data-toggle="collapse" data-parent="#system_messages" href="#system_messages_<?php echo $msg->id ?>">
                              <div class="row">
                                  <div class="col-xs-3">
                                      <?php echo Carbon::createFromFormat('Y-m-d', $msg->public_date)->format('d/m/Y') ?>
                                  </div>
                                  <div class="col-xs-9">
                                      <?php echo Html::encode($msg->title) ?>
                                  </div>
                              </div>
                          </div>
                          <div id="system_messages_<?php echo $msg->id ?>" class="panel-collapse collapse">
                              <div class="panel-body" id="system_messages_<?php echo $msg->id ?>">
                                  <?php echo Html::encode($msg->message) ?>
                              </div>
                          </div>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    <?php endif ?>
</div>
<script>
    dataTime= []; dataMoney = []; dataPeople= []; dataOriginal = [];
    <?php if($graph_customer_sale['time'] != null) :?>
    dataTime = [<?= '"'.implode('","', $graph_customer_sale['time']).'"' ?>];
    <?php endif; ?>
    <?php if($graph_customer_sale['money'] != null) :?>
    dataMoney = [<?= '"'.implode('","', $graph_customer_sale['money']).'"' ?>];
    <?php endif; ?>
    <?php if($graph_customer_sale['people'] != null) :?>
    dataPeople = [<?= '"'.implode('","', $graph_customer_sale['people']).'"' ?>];
    <?php endif; ?>

    dataOriginal = <?= \GuzzleHttp\json_encode($dataOriginal) ?>;
    maxPeople = 10;
    maxMoney = 100;
    minMoney = 0;
    for(i = 0; i < dataPeople.length; i++){
        if(maxPeople< dataPeople[i]){
            maxPeople = Number(dataPeople[i]);
        }
    }

    for(i = 0; i < dataMoney.length; i++){
        if(maxMoney< Number(dataMoney[i])){
            maxMoney = Number(dataMoney[i]);
        }
        if(minMoney > Number(dataMoney[i])){
            minMoney = Number(dataMoney[i]);
            if(maxMoney > 10000 && maxMoney <= 100000){
                if( minMoney > -10000){
                    minMoney = -10000;
                }
            }
            
            if(maxMoney > 100000 && maxMoney <= 1000000){
                if(minMoney > -100000){
                    minMoney = -100000;
                }
            }
            if(maxMoney > 1000000){
                if(minMoney > -1000000){
                    minMoney = -1000000;
                }
            }
            
            if(maxMoney > 100000000){
                if(minMoney > -100000000){
                    minMoney = -100000000;
                }
            }
            
            if(maxMoney > 1000000000){
                if(minMoney > -1000000000){
                    minMoney = -1000000000;
                }
            }
        }
    }
    var barChartData = {
        labels : dataTime,
        datasets: [{
            type: 'bar',
            label: "<?= Yii::t('backend','Number of customers') ?>",
            data: dataPeople,
            fill: false,
            backgroundColor: '#5b9bd5',
            borderColor: '#5b9bd5',
            hoverBackgroundColor: '#71B37C',
            hoverBorderColor: '#71B37C',
            yAxisID: 'y-axis-1'
        }, {
            label: "<?= Yii::t('backend', 'Sales') ?>",
            type: 'line',
            data: dataMoney,
            fill: false,
            borderColor: '#EC932F',
            backgroundColor: '#EC932F',
            pointBorderColor: '#EC932F',
            pointBackgroundColor: '#EC932F',
            pointHoverBackgroundColor: '#EC932F',
            pointHoverBorderColor: '#EC932F',
            yAxisID: 'y-axis-2',
            bezierCurve: false,
            tension: 0
        }]
    };

    var options_graph = {

        legend: {
            display: false
        },
        responsive: true,
        tooltips: {
            mode: 'label',
        },
        elements: {
            line: {
                fill: false
            }
        },
        scales: {
            xAxes: [{
                display: true,
                gridLines: {
                    display: false
                },
                labels: {
                    show: true,
                },
                barPercentage: 0.3,
            }],
            yAxes: [{
                type: "linear",
                display: true,
                position: "left",
                id: "y-axis-1",
                gridLines: {
                    display: false
                },
                labels: {
                    show: true,
                },
                ticks:{max:maxPeople},
            }, {
                type: "linear",
                display: true,
                position: "right",
                id: "y-axis-2",
                gridLines: {
                    display: false
                },
                labels: {
                    show: true,
                },
                ticks:{max:maxMoney, min: minMoney},
            }]
        }
    };

    window.onload = function() {
        var ctx = document.getElementById("canvas").getContext("2d");
        window.myBar = new Chart(ctx, {
            type: 'bar',
            data: barChartData,
            options: options_graph,
        });
    };

    function updateGraph(dataMoney, dataPeople){
        barChartData['datasets'][0]['data'] = dataPeople;
        barChartData['datasets'][1]['data'] = dataMoney;

        var ctx = document.getElementById("canvas").getContext("2d");
        window.myBar = new Chart(ctx, {
            type: 'bar',
            data: barChartData,
            options: options_graph,
        });
    }

    function formatThousandsWithRounding(n_input, dp){
        n = Number(n_input);
        var w = n.toFixed(dp), k = w|0, b = n < 0 ? 1 : 0,
            u = Math.abs(w-k), d = (''+u.toFixed(dp)).substr(2, dp),
            s = ''+k, i = s.length, r = '';
        while ( (i-=3) > b ) { r = ',' + s.substr(i, 3) + r; }
        return s.substr(0, i + 3) + r + (d ? '.'+d: '');
    };
    
    Number.prototype.format = function(n, x, s, c) {
        var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\D' : '$') + ')',
            num = this.toFixed(Math.max(0, ~~n));

        return (c ? num.replace('.', c) : num).replace(new RegExp(re, 'g'), '$&' + (s || ','));
    };

    $(document).ready(function(){
       $('#list_store').change(function(){
          if($(this).val() == 'all'){
              $('#sum_sale_date').text((dataOriginal['totalMoneyDate']).format(0, 3, ',', '.'));
              $('#total_people_date').text(formatThousandsWithRounding(dataOriginal['totalPeopleDate'],0));
              $('#average_money_date').text((dataOriginal['averageMoney']).format(0, 3, ',', '.'));
              $('#count_booking_date').text((dataOriginal['countBooking']).format(0, 3, ',', '.'));
              $('#item_delivery_date').text((dataOriginal['countItemDelivery']).format(0, 3, ',', '.'));
              $('#total_away_people').text(formatThousandsWithRounding(dataOriginal['awayPeople'],0));
              $('#total_away_gimmick_people').text(formatThousandsWithRounding(dataOriginal['awayGimmickPeople'],0));
              $('#total_money_month').text((dataOriginal['totalMoneyMonth']).format(0, 3, ',', '.'));
              $('#total_people_month').text(formatThousandsWithRounding(dataOriginal['totalPeopleMonth'],0));
              updateGraph(dataOriginal['graphInfo']['money'], dataOriginal['graphInfo']['people']);
          }else{
              for(i = 0; i <  dataOriginal['dataMaster'].length; i++){
                  if($(this).val() == dataOriginal['dataMaster'][i]['store_code']){
                        
                        data = dataOriginal['dataMaster'][i];
                        $('#sum_sale_date').text((data['totalMoneyDate']).format(0, 3, ',', '.'));
                        $('#total_people_date').text(formatThousandsWithRounding(data['totalPeopleDate'],0));
                        $('#average_money_date').text((data['averageMoney']).format(0, 3, ',', '.'));
                        $('#count_booking_date').text((data['countBooking']).format(0, 3, ',', '.'));
                        $('#item_delivery_date').text((data['countItemDelivery']).format(0, 3, ',', '.'));
                        $('#total_away_people').text(formatThousandsWithRounding(data['awayPeople'],0));
                        $('#total_away_gimmick_people').text(formatThousandsWithRounding(data['awayGimmickPeople'],0));
                        $('#total_money_month').text((data['totalMoneyMonth']).format(0, 3, ',', '.'));
                        $('#total_people_month').text(formatThousandsWithRounding(data['totalPeopleMonth'],0));
                        updateGraph(data['graphInfo_money'], data['graphInfo_people']);
                  }
              }
          }
       });
    });
</script>
