<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use backend\assets\LoginAsset;
//use yii;

LoginAsset::register($this);

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\CompanyLoginForm */

$this->title = 'Sign In';
?>

<div class="login-box login-box-custom">
    <div class="login-box-body">
		<div class="login-logo">
	        <a href="#"> <?= Yii::t('backend', $this->title)  ?></a>
	    </div>
	    <!-- /.login-logo -->

        <?php $form = ActiveForm::begin(['id' => 'login-form',
			'enableClientValidation' => false,
			'fieldClass' => 'justinvoelker\awesomebootstrapcheckbox\ActiveField']); ?>

		<?= $form
            ->field($model, 'company_code')
            ->textInput(['maxlength' => true]) ?>

        <?= $form
            ->field($model, 'management_id')
            ->textInput(['maxlength' => true]) ?>

        <?= $form
            ->field($model, 'password')
            ->passwordInput(['maxlength' => true]) ?>

		<?= $form->field($model, 'rememberMe')
			->checkbox() ?>

        <?= Html::submitButton(Yii::t('backend', 'Sign In'), ['class' => 'btn common-button-submit', 'name' => 'login-button']) ?>

        <?php ActiveForm::end(); ?>



    </div>
    <!-- /.login-box-body -->
</div><!-- /.login-box -->
