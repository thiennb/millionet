<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\bootstrap\Modal;

/* @var $this yii\web\View */
/* @var $model common\models\PointSetting */
/* @var $form yii\widgets\ActiveForm */
?>
<style>
    label.control-label:after {
        content: " *";
        color: red;
    }
</style>
<?php
$form = ActiveForm::begin([
            'action' => ['update'],
            'id' => 'company-setting-form',
            'enableClientValidation' => false,
            'options' => ['class' => 'form-horizontal'],
        ])
?>
<section>
    <div class="row">
        <div class="box-header with-border common-box-h4 col-md-4">
            <h4 class="text-title"><b><?= Yii::t('backend', 'Company Setting') ?></b></h4>
        </div>
    </div>
    <div class="row">
        <div class="box-body content">
            <div class="col-md-10">
                <div class="point-setting-form">
                    <div class="col-md-12">
                        <div class="col-md-2 label-margin">
                            <label class="control-label "
                                   for="mastercustomer-firstname"> <?= Yii::t("backend", "Company Code"); ?></label>
                        </div>
                        <div class="col-md-4 input_store_ct">
                            <?= $form->field($model, 'company_code')->label(false)->textInput(['maxlength' => true]); ?>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="col-md-2 label-margin">
                            <label class="control-label "
                                   for="mastercustomer-firstname"> <?= Yii::t("backend", "Company Name"); ?></label>
                        </div>
                        <div class="col-md-4 input_store_ct">
                            <?= $form->field($model, 'name')->label(false)->textInput(['maxlength' => true]) ?>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="col-md-2 label-margin">
                            <?= Yii::t("backend", "Company Name Kana"); ?>
                        </div>
                        <div class="col-md-4 input_store_ct">
                            <?= $form->field($model, 'name_kana')->label(false)->textInput(['maxlength' => true]) ?>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="col-md-2 label-margin">
                            <?= Yii::t("backend", "Phone"); ?>
                        </div>
                        <div class="col-md-4 input_store_ct">
                            <?=
                            $form->field($model, 'tel', [
                            ])->widget(\yii\widgets\MaskedInput::className(), [
                                'mask' => '9',
                                'clientOptions' => ['repeat' => 17, 'greedy' => false]
                            ])->label(false)->textInput(['maxlength' => true])
                            ?>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="col-md-2 label-margin">
                            <?= Yii::t("backend", "FAX"); ?>
                        </div>
                        <div class="col-md-4 input_store_ct">
                            <?=
                            $form->field($model, 'fax', [
                            ])->widget(\yii\widgets\MaskedInput::className(), [
                                'mask' => '9',
                                'clientOptions' => ['repeat' => 17, 'greedy' => false]
                            ])->label(false)->textInput(['maxlength' => true])
                            ?>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="col-md-2 label-margin">
                            <?= Yii::t("backend", "Email"); ?>
                        </div>
                        <div class="col-md-4 input_store_ct">
                            <?= $form->field($model, 'email')->label(false)->textInput(['maxlength' => true]) ?>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="col-md-2 label-margin">
                            <?= Yii::t("backend", "Homepage URL"); ?>
                        </div>
                        <div class="col-md-4 input_store_ct">
                            <?= $form->field($model, 'home_page')->label(false)->textInput(['maxlength' => true]) ?>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="col-md-2 label-margin">
                            <?= Yii::t("backend", "Postal Code"); ?>
                        </div>
                        <div class="col-md-2 post_code_company_ct">
                            <?=
                            $form->field($model, 'post_code')->label(false)->textInput([
                                'maxlength' => true])->widget(\yii\widgets\MaskedInput::className(), [
                                'mask' => '999-9999',
                                'clientOptions' => [
                                    'removeMaskOnSubmit' => true
                        ]])
                            ?>
                        </div>
                        <div class="col-md-4 btn_search_adress_company_ct">
                            <?= Html::button(Yii::t('backend', 'Search Adress'), ['class' => 'btn common-button-submit', 'id' => 'searchAddress']) ?>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="col-md-2 label-margin">
                            <?= Yii::t("backend", "Address1"); ?>
                        </div>
                        <div class="col-md-4 input_store_ct">
                            <?= $form->field($model, 'address')->label(false)->textInput(['maxlength' => true]) ?>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-2 label-margin">
                            <?= Yii::t("backend", "Address2"); ?>
                        </div>
                        <div class="col-md-4 input_store_ct">
                            <?= $form->field($model, 'address2')->label(false)->textInput(['maxlength' => true]) ?>
                        </div>
                    </div>

                </div>

            </div>
        </div>
    </div>

    <div class="col-md-12">
        <div class="box-header with-border common-box-h4 col-md-4">
            <h4 class="text-title"><b><?= Yii::t('backend', 'Setting color') ?></b></h4>
        </div>
    </div>
    <div class="row">
        <div class="box-body content">
            <div class="col-md-10">
                <div class="point-setting-form">
                    <div class="col-md-12">
                        <div class="col-md-2 col-md-offset-2 background_giff_ct ct_company_admin">
                            <?= Yii::t('backend', 'Background color') ?>
                        </div>
                        <div class="col-md-2 character_giff_ct ct_company_admin1">
                            <?= Yii::t('backend', 'Character color') ?>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-2 label-margin">
                            <?= Yii::t("backend", "Header section"); ?>
                        </div>
                        <div class="col-md-2">
                            <?= $form->field($model, 'app_header_background_color')->label(false)->textInput(['class' => 'jscolor form-control form-company-custom company_app_header_background_color']) ?>
                        </div>
                        <div class="col-md-2">
                            <?= $form->field($model, 'app_header_character_color')->label(false)->textInput(['class' => 'jscolor form-control company_app_header_character_color']) ?>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-2 label-margin">
                            <?= Yii::t("backend", "Content section"); ?>
                        </div>
                        <div class="col-md-2">
                            <?= $form->field($model, 'app_content_background_color')->label(false)->textInput(['class' => 'jscolor form-control form-company-custom company_app_content_background_color']) ?>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-2 label-margin">
                            <?= Yii::t("backend", "Footer section"); ?>
                        </div>
                        <div class="col-md-2">
                            <?= $form->field($model, 'app_footer_background_color')->label(false)->textInput(['class' => 'jscolor form-control form-company-custom company_app_footer_background_color']) ?>
                        </div>
                        <div class="col-md-2">
                            <?= $form->field($model, 'app_footer_character_color')->label(false)->textInput(['class' => 'jscolor form-control company_app_footer_character_color']) ?>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-3">
                            <div class="form-group">
                                <?= Html::button(Yii::t('backend', 'Detailed Preview'), ['class' => 'btn common-button-submit', 'id' => 'previewBtnCompanySetting']) ?>
                            </div>
                        </div>
                    </div>
                    <!-- Button submit and back-->
                    <div class="col-md-12">
                        <div class="col-md-3 magin_button text-right">
                            <div class="form-group">
                                <?= Html::a(Yii::t('backend', 'Return'), ['/'], ['class' => 'btn btn-default common-button-default']) ?>
                            </div>
                        </div>
                        <div class="col-md-4 magin_button btn_sumit_giff_ct">
                            <div class="form-group">
                                <?= Html::submitButton(Yii::t('backend', 'Save'), ['id' => 'btn-add', 'class' => 'btn common-button-submit']) ?>
                            </div>
                        </div>
                    </div>
                    <!-- End Button submit and back-->
                </div>
            </div>
        </div>
    </div>
    <?php $form = ActiveForm::end() ?>
</section>
<?php
Modal::begin([
    'id' => 'companySettingPreview',
    'size' => 'SIZE_LARGE',
    'header' => '<div class="box-header with-border common-box-h4 col-md-8"><h4 class="text-title"><b>' . Yii::t('backend', 'Setting color preview') . '</b></h4></div>',
    'footer' => '<button id="btn-close-modal-companySettingPreview" class="btn btn-default common-button-default">' . Yii::t('backend', 'Close') . ' </button><button id="btn-submit-modal-companySettingPreview" class="btn btn-default common-button-submit">決定</button>',
    'closeButton' => [Yii::t('backend', 'Close'), ['class' => 'btn btn-primary']],
]);
echo "<div id='dialogContent'>
<div class='row'>
<div class='col-md-5'>
    <div class='box-preview-setting-color'>
        <div class='box-preview-setting-color-header'><div class='col-md-5'><span><i class=\"fa fa-long-arrow-left\" aria-hidden=\"true\"></i>" . Yii::t("backend", "Return") . "</span></div><div class='col-md-7'>" . Yii::t("backend", "Header") . "</div></div>
        <div class='box-preview-setting-color-content'></div>
        <div class='box-preview-setting-color-footer'><div class='col-md-5'><i class=\"fa fa-home\" aria-hidden=\"true\"></i></div><div class='col-md-7'>" . Yii::t("backend", "Footer") . "</div></div>
    </div>
</div>
<div class='col-md-7'>
    <div class='col-md-12 box_preview_ct_mn'>
        <div class='col-md-2 col-md-offset-2 background_giff_ct_preview'>
            " . Yii::t('backend', 'Background color') . "
        </div>
        <div class='col-md-2 character_giff_ct_preview'>
            " . Yii::t('backend', 'Character color') . "
        </div>
    </div>
    <div class=\"col-md-12\">
        <div class=\"col-md-4 label-margin label_color_ct\">
            " . Yii::t("backend", "Header section") . "
        </div>
        <div class=\"col-md-4 input_color_ct\">
           <input id='popup_company_app_header_background_color' class='form-control jscolor'/>
        </div>
        <div class=\"col-md-4 input_color_ct\">
            <input id='popup_company_app_header_character_color' class='form-control jscolor'  />
        </div>
    </div>
    <div class=\"col-md-12\" style='margin-top:10px;'>
        <div class=\"col-md-4 label-margin label_color_ct\">
            " . Yii::t("backend", "Content section") . "
        </div>
        <div class=\"col-md-4 input_color_ct\">
           <input class='form-control jscolor' id='popup_company_app_content_background_color' />
        </div>
    </div>
    <div class=\"col-md-12\" style='margin-top:10px;'>
        <div class=\"col-md-4 label-margin label_color_ct\">
            " . Yii::t("backend", "Footer section") . "
        </div>
        <div class=\"col-md-4 input_color_ct\">
           <input id='popup_company_app_footer_background_color' class='form-control jscolor'/>
        </div>
        <div class=\"col-md-4 input_color_ct\">
            <input id='popup_company_app_footer_character_color' class='form-control jscolor'  />
        </div>
    </div>
</div>
</div>
</div>
</div>";
Modal::end();
?>
<style>
    @media (min-width: 769px) {
        .form-company-custom {
            width: 98%;
        }
    }

    .box-preview-setting-color {
        max-width: 300px;
        height: 320px;
        border: 2px solid #ccc;
    }

    .box-preview-setting-color .box-preview-setting-color-header, .box-preview-setting-color-content, .box-preview-setting-color-footer {
        width: 100%;
    }

    .box-preview-setting-color .box-preview-setting-color-header {
        height: 40px;
        line-height: 40px;
        border-bottom: 2px solid #ccc;
       /* background-color: #FF69B4;
        color: #FFFFFF; */
    }

    .box-preview-setting-color .box-preview-setting-color-content {
        height: 236px;
        border-bottom: 2px solid #ccc;
    }

    .box-preview-setting-color .box-preview-setting-color-footer {
        height: 40px;
        line-height: 40px;
         /* background-color: #00B0F0;
        color: #FFFFFF;  */
    }
    .box-preview-setting-color .box-preview-setting-color-footer .fa-home{
        font-size:37px;
    }

    /*modal*/
    #companySettingPreview .modal-footer{
        background: #ffffff;
        text-align: center;
    }
</style>
<script>
    $(document).ready(function () {
        $("#searchAddress").click(function (e) {
            getPostcode('#company-post_code', '#company-address');
        });

        var $pop_up = $('.modal-content');
        $pop_up.addClass('customer_modal_content');
        $('.modal-footer').addClass('modal-footer_ct');

    });
    $(document).ready(function () {
//        event change form
        $(".company_app_header_background_color").change(function () {
            $('.box-preview-setting-color-header').css({'background-color': '#' + $(".company_app_header_background_color").val()});
            $('#popup_company_app_header_background_color').val($(".company_app_header_background_color").val());
            $('#popup_company_app_header_background_color').css({'background-color': '#' + $(".company_app_header_background_color").val()})
        });
        $(".company_app_header_character_color").change(function () {
            $('.box-preview-setting-color-header').css({'color': '#' + $(".company_app_header_character_color").val()})
            $('#popup_company_app_header_character_color').val($(".company_app_header_character_color").val());
            $('#popup_company_app_header_character_color').css({'background-color': '#' + $(".company_app_header_character_color").val()})
        });
        $(".company_app_content_background_color").change(function () {
            $('.box-preview-setting-color-content').css({'background-color': '#' + $(".company_app_content_background_color").val()})
            $('#popup_company_app_content_background_color').val($(".company_app_content_background_color").val());
            $('#popup_company_app_content_background_color').css({'background-color': '#' + $(".company_app_content_background_color").val()})
        });
        $(".company_app_footer_background_color").change(function () {
            $('.box-preview-setting-color-footer').css({'background-color': '#' + $(".company_app_footer_background_color").val()})
            $('#popup_company_app_footer_background_color').val($(".company_app_footer_background_color").val());
            $('#popup_company_app_footer_background_color').css({'background-color': '#' + $(".company_app_footer_background_color").val()})
        });
        $(".company_app_footer_character_color").change(function () {
            $('.box-preview-setting-color-footer').css({'color': '#' + $(".company_app_footer_character_color").val()})
            $('#popup_company_app_footer_character_color').val($(".company_app_footer_character_color").val());
            $('#popup_company_app_footer_character_color').css({'background-color': '#' + $(".company_app_footer_character_color").val()})
        });
        $('#previewBtnCompanySetting').click(function (e) {
            
            $('#popup_company_app_header_background_color').val($(".company_app_header_background_color").val());
            $('#popup_company_app_header_background_color').css({'background-color': '#' + $(".company_app_header_background_color").val()});
            $('#popup_company_app_header_character_color').val($(".company_app_header_character_color").val());
            $('#popup_company_app_header_character_color').css({'background-color': '#' + $(".company_app_header_character_color").val()});
            $('#popup_company_app_content_background_color').val($(".company_app_content_background_color").val());
            $('#popup_company_app_content_background_color').css({'background-color': '#' + $(".company_app_content_background_color").val()});
            $('#popup_company_app_footer_background_color').val($(".company_app_footer_background_color").val());
            $('#popup_company_app_footer_background_color').css({'background-color': '#' + $(".company_app_footer_background_color").val()});
            $('#popup_company_app_footer_character_color').val($(".company_app_footer_character_color").val());
            $('#popup_company_app_footer_character_color').css({'background-color': '#' + $(".company_app_footer_character_color").val()});
            // Content
            $('.box-preview-setting-color-header').css({'color': '#' + $("#company-app_header_character_color").val()});
            $('.box-preview-setting-color-footer').css({'color': '#' + $("#company-app_footer_character_color").val()});
            $('.box-preview-setting-color-content').css({'background-color': '#' + $("#company-app_content_background_color").val()});
            $('.box-preview-setting-color-header').css({'background-color': '#' + $(".company_app_header_background_color").val()});
            $('.box-preview-setting-color-footer').css({'background-color': '#' + $(".company_app_footer_background_color").val()});
            $('#companySettingPreview').modal('show')
                    .find('#modalContent')
                    .load($(this).attr('value'));
        });
//        event on modal
        $("#popup_company_app_header_background_color").change(function () {
            $('.box-preview-setting-color-header').css({'background-color': '#' + $("#popup_company_app_header_background_color").val()})
        });
        $("#popup_company_app_header_character_color").change(function () {
            $('.box-preview-setting-color-header').css({'color': '#' + $("#popup_company_app_header_character_color").val()})
        });
        $("#popup_company_app_content_background_color").change(function () {
            $('.box-preview-setting-color-content').css({'background-color': '#' + $("#popup_company_app_content_background_color").val()})
        });
        $("#popup_company_app_footer_background_color").change(function () {
            $('.box-preview-setting-color-footer').css({'background-color': '#' + $("#popup_company_app_footer_background_color").val()})
        });
        $("#popup_company_app_footer_character_color").change(function () {
            $('.box-preview-setting-color-footer').css({'color': '#' + $("#popup_company_app_footer_character_color").val()})
        });


        $('#btn-close-modal-companySettingPreview').click(function (e) {
            $('#companySettingPreview').modal('hide');
        });

        $('#btn-submit-modal-companySettingPreview').click(function (e) {

            $('#companySettingPreview').modal('hide');
//            reset data modal
            $('.company_app_header_background_color').val($("#popup_company_app_header_background_color").val());
            $('.company_app_header_background_color').css({'background-color': '#' + $("#popup_company_app_header_background_color").val()})
            $('.company_app_header_character_color').val($("#popup_company_app_header_character_color").val());
            $('.company_app_header_character_color').css({'background-color': '#' + $("#popup_company_app_header_character_color").val()})
            $('.company_app_content_background_color').val($("#popup_company_app_content_background_color").val());
            $('.company_app_content_background_color').css({'background-color': '#' + $("#popup_company_app_content_background_color").val()})
            $('.company_app_footer_background_color').val($("#popup_company_app_footer_background_color").val());
            $('.company_app_footer_background_color').css({'background-color': '#' + $("#popup_company_app_footer_background_color").val()})
            $('.company_app_footer_character_color').val($("#popup_company_app_footer_character_color").val());
            $('.company_app_footer_character_color').css({'background-color': '#' + $("#popup_company_app_footer_character_color").val()})
        });

    })

</script>

