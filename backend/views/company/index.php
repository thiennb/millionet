<?php

use yii\helpers\Html;
use common\components\Constants;

$this->title = Yii::t('backend', 'Company Setting');
\backend\assets\CompanySettingAsset::register($this);
#$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Master Taxes'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$role = \common\components\FindPermission::getRole();
?>
<div class="row text-setting company">
    <div class="col-lg-12 col-md-12">
        <div class="box box-info box-solid add">
            <div class="box-header with-border">
                <h4 class="text-title"><b><?= $this->title ?></b></h4>
            </div>
            <div class="box-body content">
                <div class="col-md-12">
                    <div class="common-box">
                        <!-- Operating Admin  -->
                        <?php
                        if ($role->permission_id == Constants::APP_MANAGER_ROLE) {
                            echo $this->render('_form', [
                                'model' => $model,
                            ]);
                        }
                        ?>
                        <!-- Company Admin  -->
                        <?php
                        if ($role->permission_id == Constants::COMPANY_MANAGER_ROLE) {
                            echo $this->render('_form_company_admin', [
                                'model' => $model,
                            ]);
                        }
                        ?>
                        <!-- Manager Store Or Staff  -->
                        <?php
                        if ($role->permission_id == Constants::STORE_MANAGER_ROLE || $role->permission_id == Constants::STAFF_ROLE) {
                            echo $this->render('_form_company_staff', [
                                'model' => $model,
                            ]);
                        }
                        ?>
                       
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
