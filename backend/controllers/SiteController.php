<?php

namespace backend\controllers;

use common\models\Company;
use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\base\Exception;
use common\models\CompanyLoginForm;
use common\models\Booking;
use common\models\MasterMoneyInputOutput;
use common\models\MasterStore;
use common\models\MstOrder;
use common\models\CompanyStore;
use common\models\MstSystemMessage;
use common\components\Util;
use common\models\MasterStaff;

/**
 * Site controller
 */
class SiteController extends Controller {

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout', 'index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex() {

        //init()
        $result = array('totalMoneyDate' => 0, 'totalPeopleDate' => 0, 'averageMoney' => 0, 'countBooking' => 0,
            'countItemDelivery' => 0, 'totalMoneyMonth' => 0, 'totalPeopleMonth' => 0,
            'awayPeople' => 0, 'awayGimmickPeople' => 0, 'stores' => [], 'dataMaster' => [],
            'graphInfo' => array('time' => [], 'money' => [], 'people' => []));

        $permissionId = Yii::$app->user->identity->permission_id;
        $logout_flag = false;

        try {

            if ($permissionId === null) {
                $logout_flag = true;
                throw new Exception(Yii::t('backend','Cannot find your Permission'));
            }

            if ($permissionId === 0 || $permissionId == 1) {

                //is administrator
                $company_id = Util::getCookiesCompanyId();
                if ($company_id === null) {
                    $logout_flag = true;
                    throw new Exception(Yii::T('backend','Sorry, Cannot find Your StoreId'));
                }
                //find all store of company
                $stores = MasterStore::find()->where(['company_id' => $company_id, 'del_flg'=>'0'])->andWhere(['<>','store_code', '00000'])->distinct()->all();

                foreach ($stores as $store) {

                    array_push($result['stores'], array('id' => $store['id'], 'name_kana' => $store['name_kana'], 'name' => $store['name'], 'store_code' => $store['store_code']));
                    $storeCode = $store['store_code'];
                    $awayDate = $store['away_dates'] !== null ? $store['away_dates'] : 0;
                    $awayGimmickDate = $store['away_gimmick_dates'] !== null ? $store['away_gimmick_dates'] : 0;
                    $resultStore = $this->getIndexInfoStore($storeCode, $awayDate, $awayGimmickDate);
                    //var_dump($resultStore);
                    $result['totalMoneyDate'] += $resultStore['totalMoneyDate'];
                    $result['totalPeopleDate'] += $resultStore['totalPeopleDate'];
                    if ($result['totalPeopleDate'] != 0)
                        $result['averageMoney'] = $result['totalMoneyDate'] / $result['totalPeopleDate'];
                    $result['countBooking'] += $resultStore['countBooking'];
                    $result['countItemDelivery'] += $resultStore['countItemDelivery'];
                    $result['totalMoneyMonth'] += $resultStore['totalMoneyMonth'];
                    $result['totalPeopleMonth'] += $resultStore['totalPeopleMonth'];
                    $result['awayPeople'] += $resultStore['awayPeople'];
                    $result['awayGimmickPeople'] += $resultStore['awayGimmickPeople'];

                    if ($resultStore['graphInfo']['money'] != null) {
                        for ($i = 0; $i < 6; $i++) {

                            if (array_key_exists($i, $resultStore['graphInfo']['money'])) {
                                if (!array_key_exists($i, $result['graphInfo']['money'])) {
                                    $result['graphInfo']['money'][$i] = 0;
                                }
                                $result['graphInfo']['money'][$i] += $resultStore['graphInfo']['money'][$i];
                            }
                        }
                    }

                    if ($resultStore['graphInfo']['people'] != null) {
                        for ($i = 0; $i < 6; $i++) {
                            if (array_key_exists($i, $resultStore['graphInfo']['people'])) {
                                if (!array_key_exists($i, $result['graphInfo']['people'])) {
                                    $result['graphInfo']['people'][$i] = 0;
                                }
                                $result['graphInfo']['people'][$i] += $resultStore['graphInfo']['people'][$i];
                            }
                        }
                    }

                    array_push($result['dataMaster'], array('store_code' => $storeCode, 'totalMoneyDate' => $resultStore['totalMoneyDate'],
                        'totalPeopleDate' => $resultStore['totalPeopleDate'], 'averageMoney' => $resultStore['averageMoney'],
                        'countBooking' => $resultStore['countBooking'], 'countItemDelivery' => $resultStore['countItemDelivery'],
                        'totalMoneyMonth' => $resultStore['totalMoneyMonth'], 'totalPeopleMonth' => $resultStore['totalPeopleMonth'],
                        'awayPeople' => $resultStore['awayPeople'], 'awayGimmickPeople' => $resultStore['awayGimmickPeople'],
                        'graphInfo_money' => $resultStore['graphInfo']['money'], 'graphInfo_people' => $resultStore['graphInfo']['people']));
                }

                $result['graphInfo']['time'] = $this->getTimeMarkIndex();
                unset($result['graphInfo']['time'][0]);
            } else if ($permissionId == 2 || $permissionId == 3) {

                // is staff or store master
                $storeCode = null;
                $awayDate = 0;
                $awayGimmickDate = 0;
                $storeId = Yii::$app->user->identity->store_id;

                if (Yii::$app->session->get('store') === null) {

                    $store = MasterStore::find()->select(['store_code', 'away_dates', 'away_gimmick_dates'])->where(['id' => $storeId])->one();
                    if ($store === null) {
                        $logout_flag = true;
                        throw new Exception(Yii::t('backend','Sorry, Cannot find Your StoreCode' ));
                    }

                    Yii::$app->session->set('store', $store);
                    $storeCode = $store['store_code'];
                    $awayDate = $store['away_dates'];
                    $awayGimmickDate = $store['away_gimmick_dates'];
                } else {
                    $storeCode = Yii::$app->session->get('store')['store_code'];
                    $awayDate = Yii::$app->session->get('store')['away_dates'];
                    $awayGimmickDate = Yii::$app->session->get('store')['away_gimmick_dates'];
                }

                $result = $this->getIndexInfoStore($storeCode, $awayDate, $awayGimmickDate);
            } else {
                $logout_flag = true;
                throw new Exception(Yii::t('backend', 'Cannot Access with Your Permission'));
            }
        } catch (Exception $ex) {
            Yii::$app->getSession()->setFlash('error', ['error' => Yii::t('backend', $ex->getMessage())]);
            Yii::$app->getSession()->remove('store');
            if ($logout_flag == true)
                $this->actionLogout();
        }

        $system_messages = MstSystemMessage::find()->limit(10)->orderBy(['created_at' => SORT_DESC])->all();

        return $this->render('index', [
                    'total_people_date' => $result['totalPeopleDate'],
                    'total_money_date' => $result['totalMoneyDate'],
                    'average_money_date' => $result['averageMoney'],
                    'count_booking_date' => $result['countBooking'],
                    'item_delivery_date' => $result['countItemDelivery'],
                    'total_money_month' => $result['totalMoneyMonth'],
                    'total_people_month' => $result['totalPeopleMonth'],
                    'graph_customer_sale' => $result['graphInfo'],
                    'total_away_people' => $result['awayPeople'],
                    'total_away_gimmick_people' => $result['awayGimmickPeople'],
                    'stores' => $result['stores'],
                    'dataOriginal' => $result,
                    'system_messages' => $system_messages
        ]);
    }

    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin() {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new CompanyLoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            // Set session company_code
            $session = Yii::$app->session;
            $cookies = Yii::$app->response->cookies;
            //$cookies->add('code_company_cookie', $model->company_code);
            // Set cookies company store
//            Util::setCookiesCompanyCode($model->company_code);
            // Set cookies company id store
            $company = Company::findOne(['company_code' => $model->company_code]);
            Util::setCookiesCompanyId($company->id);
//            $cookies->add(new \yii\web\Cookie([
//                    'name' => 'code_company_cookie',
//                    'value' => $model->company_code
//                ]));

            $login_redirect = $session->get('login_redirect');
            if (isset($login_redirect) && !empty($login_redirect)) {
                return $this->redirect($login_redirect);
            }
            return $this->goBack();
        } else {
            return $this->render('login', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout() {
        Yii::$app->user->logout();
        $session = Yii::$app->session;
        $cookies = Yii::$app->response->cookies;
        $cookies->remove('code_company_cookie');
        $session->remove('store');
        return $this->goHome();
    }

    /**
     * Function support actionIndex
     *
     * @return array
     */
    private function getMoneyByHour($moneyDate = null, $timeMark = null) {
        if ($moneyDate !== null && $timeMark !== null) {
            $money1 = 0;
            $money2 = 0;
            $money3 = 0;
            $money4 = 0;
            $money5 = 0;
            $money6 = 0;
            $timemark0 = strtotime(date('Y-m-d') . ' ' . $timeMark[0] . ':00');
            $timemark1 = strtotime(date('Y-m-d') . ' ' . $timeMark[1] . ':00');
            $timemark2 = strtotime(date('Y-m-d') . ' ' . $timeMark[2] . ':00');
            $timemark3 = strtotime(date('Y-m-d') . ' ' . $timeMark[3] . ':00');
            $timemark4 = strtotime(date('Y-m-d') . ' ' . $timeMark[4] . ':00');
            $timemark5 = strtotime(date('Y-m-d') . ' ' . $timeMark[5] . ':00');
            $timemark6 = strtotime(date('Y-m-d') . ' ' . $timeMark[6] . ':00');

            foreach ($moneyDate as $order) {
                $time = strtotime($order['process_date'] . ' ' . $order['process_time'] . ':00');

                if ($time > $timemark0 && $time <= $timemark1) {
                    $money1 += (float) $order['total'];
                } else if ($time > $timemark1 && $time <= $timemark2) {
                    $money2 += (float) $order['total'];
                } else if ($time > $timemark2 && $time <= $timemark3) {
                    $money3 += (float) $order['total'];
                } else if ($time > $timemark3 && $time <= $timemark4) {
                    $money4 += (float) $order['total'];
                } else if ($time > $timemark4 && $time <= $timemark5) {
                    $money5 += (float) $order['total'];
                } else if ($time > $timemark5 && $time <= $timemark6) {
                    $money6 += (float) $order['total'];
                }
            }

            $result = array($money1, $money2, $money3, $money4, $money5, $money6);
            $count = count($result);
            for ($i = 1; $i <= count($count); $i++) {
                if ($i == $count)
                    break;
                if ($result[$count - $i] == 0) {
                    unset($result[$count - $i]);
                    $count--;
                    $i--;
                } else {
                    break;
                }
            }

            return $result;
        }

        return null;
    }

    private function getPeopleByHour($peopleDate = null, $timeMark = null) {
        if ($peopleDate !== null && $timeMark !== null) {
            $people1 = [];
            $people2 = [];
            $people3 = [];
            $people4 = [];
            $people5 = [];
            $people6 = [];
            $timemark0 = strtotime(date('Y-m-d') . ' ' . $timeMark[0] . ':00');
            $timemark1 = strtotime(date('Y-m-d') . ' ' . $timeMark[1] . ':00');
            $timemark2 = strtotime(date('Y-m-d') . ' ' . $timeMark[2] . ':00');
            $timemark3 = strtotime(date('Y-m-d') . ' ' . $timeMark[3] . ':00');
            $timemark4 = strtotime(date('Y-m-d') . ' ' . $timeMark[4] . ':00');
            $timemark5 = strtotime(date('Y-m-d') . ' ' . $timeMark[5] . ':00');
            $timemark6 = strtotime(date('Y-m-d') . ' ' . $timeMark[6] . ':00');

            foreach ($peopleDate as $people) {
                $time = strtotime($people['process_date'] . ' ' . $people['process_time'] . ':00');

                if ($time > $timemark0 && $time <= $timemark1) {
                    if (!in_array($people['customer_jan_code'], $people1))
                        array_push($people1, $people['customer_jan_code']);
                }else if ($time > $timemark1 && $time <= $timemark2) {
                    if (!in_array($people['customer_jan_code'], $people2))
                        array_push($people2, $people['customer_jan_code']);
                }else if ($time > $timemark2 && $time <= $timemark3) {
                    if (!in_array($people['customer_jan_code'], $people3))
                        array_push($people3, $people['customer_jan_code']);
                }else if ($time > $timemark3 && $time <= $timemark4) {
                    if (!in_array($people['customer_jan_code'], $people4))
                        array_push($people4, $people['customer_jan_code']);
                }else if ($time > $timemark4 && $time <= $timemark5) {
                    if (!in_array($people['customer_jan_code'], $people5))
                        array_push($people5, $people['customer_jan_code']);
                }else if ($time > $timemark5 && $time <= $timemark6) {
                    if (!in_array($people['customer_jan_code'], $people6))
                        array_push($people6, $people['customer_jan_code']);
                }
            }

            $result = array(count($people1), count($people2), count($people3), count($people4), count($people5), count($people6));
            return $result;
        }

        return null;
    }

    private function getTimeMarkIndex() {
        if (strtotime(date('Y-m-d H:i:s')) > strtotime(date('Y-m-d') . ' 12:00:00')) {
            $hour = date('H');
            if ((int) $hour % 2 != 0)
                $hour = (int) $hour - 1;
            return array(sprintf('%02s', ($hour - 12)) . ':00', sprintf('%02s', ($hour - 10)) . ':00', sprintf('%02s', ($hour - 8)) . ':00', sprintf('%02s', ($hour - 6)) . ':00', sprintf('%02s', ($hour - 4)) . ':00', sprintf('%02s', ($hour - 2)) . ':00', ($hour) . ':00');
        }else {
            return array('00:00', '02:00', '04:00', '06:00', '08:00', '10:00', '12:00');
        }
    }

    private function getIndexInfoStore($storeCode = null, $awayDate = 0, $awayGimmickDate = 0) {

        $totalMoneyDate = 0;
        $totalMoneyMonth = 0;
        $totalPeopleDate = 0;
        $averageMoney = 0;
        $countBooking = 0;
        $countItemDelivery = 0;
        $totalMoneyMonth = 0;
        $totalPeopleMonth = 0;
        $graphInfo = [];
        $graphInfo["time"] = [];
        $graphInfo['money'] = [];
        $graphInfo['people'] = [];
        $moneyDate = null;
        $peopleDate = null;
        $awayPeople = 0;
        $awayGimmickPeople = 0;


        //get Model
        $money_input_output = new MasterMoneyInputOutput();
        $booking = new Booking();
        $mst_order = new MstOrder;

        if ($storeCode !== null) {
            $storeId = \common\models\MasterStore::find()->select('id')->where(['store_code' => $storeCode])->one();
            $totalMoneyDate = $mst_order->findTotalMoneyDateStaff($storeCode);
            $totalPeopleDate = $mst_order->findTotalPeopleDateStaff($storeCode);
            if ($totalPeopleDate != 0)
                $averageMoney = (float) $totalMoneyDate / $totalPeopleDate;
            $countBooking = $booking->getBookingDateStaff($storeId);
            $countItemDelivery = $booking->countBookingDeliveryHome($countBooking);
            $countBooking = count($countBooking);
            $totalMoneyMonth = $mst_order->findtotalMoneyMonthStaff($storeCode);
            $totalPeopleMonth = $mst_order->findtotalPeoPleMonthStaff($storeCode);
            $moneyDate = $mst_order->findAllMoneyDateStaff($storeCode);
            $peopleDate = $mst_order->findAllPeopleDateStaff($storeCode);

            if ($awayGimmickDate > $awayDate)
                $awayPeople = $mst_order->findTotalAwayPeopleDateStaff($storeCode, $awayDate, $awayGimmickDate);
            if ($awayGimmickDate > 0)
                $awayGimmickPeople = $mst_order->findTotalAwayGimmickPeopleDateStaff($storeCode, $awayGimmickDate);
            //time mark
            $graphInfo["time"] = $this->getTimeMarkIndex();
            // get money & people for graph follow time
            $graphInfo['money'] = $this->getMoneyByHour($moneyDate, $graphInfo['time']);
            $graphInfo['people'] = $this->getPeopleByHour($peopleDate, $graphInfo['time']);
            unset($graphInfo['time'][0]);
        }

        return array('totalMoneyDate' => $totalMoneyDate,
            'totalPeopleDate' => $totalPeopleDate,
            'averageMoney' => $averageMoney,
            'countBooking' => $countBooking,
            'countItemDelivery' => $countItemDelivery,
            'totalMoneyMonth' => $totalMoneyMonth,
            'totalPeopleMonth' => $totalPeopleMonth,
            'awayPeople' => $awayPeople,
            'awayGimmickPeople' => $awayGimmickPeople,
            'graphInfo' => $graphInfo,
            'dataMaster' => [],
            'stores' => []);
    }

}
