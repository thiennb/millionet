<?php

namespace backend\controllers;

use Yii;
use common\models\MasterNoticeMemberSearch;
use common\models\MasterNoticeSearch;
use yii\web\Controller;
use yii\filters\VerbFilter;
use common\models\SettingAutoSendNotice;
use common\models\MasterNotice;
use Exception;
use common\models\NoticeCustomer;
use common\models\MasterStore;
use common\models\MasterCustomer;

/**
 * NoticeSendController implements the CRUD actions for MasterNoticeMember model.
 */
class NoticeSendController extends Controller {

    //variable connection database
    public $db;

    public function init() {
        //get connection
        $this->db = Yii::$app->db;
    }

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Customer for schedule send notice.
     * @return mixed
     */
    public function actionIndex()
    {
        $delay = \common\models\SiteSetting::find()->andWhere(['type' => '01', 'key_1'=>'01', 'key_2' => '01'])->one();
        $now = \DateTime::createFromFormat('Y-m-d H:i:s',date('Y-m-d H:i:s'));
        $delay_time = empty($delay->value_1)?0:($now->getTimestamp() - $delay->value_1*60);
        $arr_send_notice = \console\models\NoticeSend::find()
                ->join('INNER JOIN','notice_customer','notice_customer.notice_id = mst_notice.id')
                ->join('INNER JOIN','mst_customer','notice_customer.customer_id = mst_customer.id')
                ->join('INNER JOIN','user','"user"."id" = mst_customer.user_id')
                ->join('INNER JOIN','device_user','device_user.user_id = mst_customer.user_id')
                ->andWhere(['mst_notice.del_flg' => '0'])
                ->andWhere(['mst_customer.del_flg' => '0'])
                ->andWhere(['notice_customer.del_flg' => '0'])
                ->andWhere(['user.del_flg' => '0'])
                ->andWhere(['device_user.del_flg' => '0'])
                ->andWhere(['notice_customer.status_send' => '01'])
                ->andWhere(['<=','notice_customer.push_date' , $now->getTimestamp()])
                ->andWhere(['>=','notice_customer.push_date' , $delay_time])
                ->andWhere(['=','mst_customer.company_id' , \common\components\Util::getCookiesCompanyId()])
                ->select([
                    'device_user.device_token as device_token',
                    'mst_notice.title',
                    'mst_notice.id as notice_id',
                    'mst_customer.user_id as user_id',
                    'notice_customer.id as notice_customer_id',
                    'device_user.notice_received_flg as notice_received_flg',
                ])
                ->all();
        $device = '';
        foreach ($arr_send_notice as $notice){
            if($notice->notice_received_flg == '0'){
                NoticeCustomer::updateAll(['status_send'=>'03','updated_at' => strtotime("now")],['id' => $notice->notice_customer_id]);
                continue;
            }
            $device .= '----------'.$notice->device_token;
            if(empty($notice->device_token)){
                continue;
            }
            $device_token = explode(':',$notice->device_token);
            if(count($device_token) != 2 || empty($device_token[0]) || empty($device_token[1])){
                continue;
            }
            $tokens = ["$device_token[0]:$device_token[1]"];
            $json = $this->send_notification_notice($tokens,$notice->title,$notice->notice_id,$notice->user_id);
            $result = json_decode($json);
            if(!empty($result) && $result->success == 1){
                NoticeCustomer::updateAll(['status_send'=>'02','updated_at' => strtotime("now")],['id' => $notice->notice_customer_id]);
            }else{
                NoticeCustomer::updateAll(['status_send'=>'03','updated_at' => strtotime("now")],['id' => $notice->notice_customer_id]);
                \Yii::error('[NOTICE_SCHEDULE] ERROR='.$json);
            }
        }
        \Yii::error('[NOTICE_SCHEDULE] ERROR='.count($arr_send_notice));
    }

    /**
     * Detail notice send for customer by auto
     * parameter integer $id
     * @return mixed
     */
    public static function send_notification_notice($tokens, $message, $notice_id, $user_id) {
        try {
            $url = 'https://fcm.googleapis.com/fcm/send';
            //content send
            $fields = array(
                'registration_ids' => $tokens,
                'content_available' => true,
                "priority"=> "high",
                "notification" => [
                    "body"=> $message,
                    "badge"=> "0",
                    "sound"=> "default"
                ],
                'data' => [
                    "notice_id"=>$notice_id,
                    'user_id'   =>  $user_id
                ]
            );
            $headers = array(
                //server key
                'Authorization:key=AIzaSyBzrcWv-tNQcCbyGtstIRwhrJAQaY8-zx4',
                'Content-Type: application/json'
            );
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
            $result = curl_exec($ch);
            if ($result === FALSE) {
                die('Curl failed: ' . curl_error($ch));
            }
            curl_close($ch);
            return $result;
            
        } catch (Exception $ex) {
            //set error message
            Yii::$app->getSession()->setFlash('error', [
                'error' => Yii::t('backend', "System error happen. Please contact with System manager.")
            ]);
            //return index
            return $this->redirect(['/']);
        }
    }

    // Validate From
    public function actionValidate() {
        $model = new MasterNotice();
        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = 'json';
            $model = \yii\widgets\ActiveForm::validate($model);
            return $model;
        }
    }
    
    /**
     * Sends an email to the specified email address using the information collected by this model.
     *
     * @param string $email the target email address
     * @return boolean whether the email was sent
     */
    public static function actionScheduleSendNoticeDistance($customer_id = 113, $device_token = 'eMLIQ91yVs0:APA91bHQG9db0xWLQ44HxkTrD0QOtHVGTQ3lpipt9KLaYQA0XiM2BY5D5zH80WeZfjVwkjeMwpAbRIf-gz_WXR5r7JUKLdnC0s2wL-qwdqxtIuuP9MHOJn_LD0W3K2jiK_SviUvHpQRF', $store_id = 1, $distance = 2)
    {
//        try {
            $query      = SettingAutoSendNotice::find();
            $modelTemp  = new SettingAutoSendNotice(); 
            $dataProvider = new \yii\data\ActiveDataProvider([
                'query' => $query,
                'pagination' => [
                    'pageSize' => false,
                ],
                'sort' => false
            ]);
            $push_date = 0;
            $model                = \common\models\MasterCustomer::findOne(['id' =>$customer_id,'company_id' => \common\components\Util::getCookiesCompanyId()]);
            $modelCustormerStore  = \common\models\CustomerStore::find()
                                    ->where(['customer_id' => $customer_id, 'store_id' => $store_id])
                                    ->one();
            $params               = \common\models\BookingBusiness::convertModelToArray($model,$modelCustormerStore);
            $query->andWhere(['push_timming'=> \common\models\BookingBusiness::PUSH_TIMING_DISTANCE]);
            $query->andWhere(['distance_to_shop'=> $distance]);
            $push_date            = \Carbon\Carbon::now()->timestamp;

    //                        \common\components\Util::d($params);
            //filter by param 
            foreach ($params as $key => $value ){
                        if($modelTemp->hasAttribute($key) && $key !='black_list_flg'  ){
                            $query->andWhere(['or',['=',$key,  empty($value) ? null : $value],['is', $key ,null]]);
                        }
                        if($key == 'birth_date'){
                            $query->andWhere(['or',['<=','start_birth_date',$value],['is', 'start_birth_date' ,null]])
                                  ->andWhere(['or',['>=','end_birth_date',$value],['is', 'end_birth_date' ,null]]);
                        }
                        if($key == 'last_visit_date'){
                            $query->andWhere(['or',['<=','start_visit_date',$value],['is', 'start_visit_date' ,null]])
                                  ->andWhere(['or',['>=','end_visit_date',$value],['is', 'end_visit_date' ,null]]);
                        }
                        if($key == 'number_visit'){
                            $query->andWhere(['or',['>=','min_visit_number',$value],['is', 'min_visit_number' ,null]])
                                  ->andWhere(['or',['<=','max_visit_number',$value],['is', 'max_visit_number' ,null]]);
                        }
                        if($key == 'black_list_flg'){
                            $query->andWhere(['or',['=','black_list_flg',$value],['is', 'black_list_flg' ,null]]);
                        }
                        //TODO : tim theo last_staff_id
            }
            //get result from query
            $result  = $dataProvider->getModels();
//            \common\components\Util::d($query);
//            \common\components\Util::d($result);
            $transaction = Yii::$app->db->beginTransaction();
            if(!empty($result)){
                foreach ($result as $modelNotice) {
                    $device_user = \common\models\DeviceUser::find()->andWhere(['user_id' => $model->user_id,'device_token' => $device_token,'notice_received_flg' => '1'])->one();
                    $notice = \common\models\MasterNotice::findOne($modelNotice->notice_id);
                    $result_send = '';
                    if(!empty($device_user) && !empty($notice)){
                        $tmp_device_token = explode(':',$device_user->device_token);
                        if(count($tmp_device_token) != 2 || empty($tmp_device_token[0]) || empty($tmp_device_token[1])){
                            continue;
                        }
                        $tokens = ["$tmp_device_token[0]:$tmp_device_token[1]"];
                        $result_send = \console\models\NoticeSend::send_notification_notice($tokens, $notice->title, $notice->id, $model->user_id);
                    }else{
                        return FALSE;
                    }
                    $notice_custormer              = new NoticeCustomer();
                    $notice_custormer->notice_id   = $modelNotice->notice_id ;
                    $notice_custormer->customer_id = $customer_id ;
                    $notice_custormer->store_id    = $store_id ;
                    if(!empty($result_send)){
                        $result = json_decode($result_send);
                        if(!empty($result) && $result->success == 1){
                            $notice_custormer->status_send = '02' ;
                        }else{
                            $notice_custormer->status_send = '03' ;
                        }
                    }else{
                        $notice_custormer->status_send = '03' ;
                    }
                    $notice_custormer->push_date   = $push_date ;
                    $notice_custormer->save();
                }
                $transaction->commit();
                return TRUE;
            }else{
                return FALSE;
            }
    }

}
