<?php

namespace backend\controllers;

use Yii;
use common\models\MasterTax;
use common\models\MasterTaxSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use Exception;
use common\models\TaxDetail;

/**
 * MasterTaxController implements the CRUD actions for MasterTax model.
 */
class MasterTaxController extends Controller {

    //variable connection database
    public $db;
    
    public function init()
    {
        //get connection
        $this->db = Yii::$app->db;
    }
    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

     /**
     * List master tax
     * @param 
     * @return 
     * @throws 
     */
    public function actionIndex() {
        //create variable search
        $searchModel = new MasterTaxSearch();
        //get list tax
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        
        //return view
        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new MasterTax model.
     * @param 
     * @return 
     * @throws 
     */
    public function actionCreate() {
        try{
            //create transaction
            $transaction = $this->db->beginTransaction();
            //create mode
            $model = new MasterTax();
            //set scenariol for validate when case create
            $model->setScenario("create");
            //check submit form and validate input data
            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                //create tax with multiple rate(1 tax multi rate)
                $model->createMultilRate();
                //commit transaction
                $transaction->commit();
                Yii::$app->getSession()->setFlash('success', [
                    'success' => Yii::t('backend', "Create Success")
                ]);
                //return view list tax
                return $this->redirect(['index']);
            } else {//if init or not pass validate
                //return view create tax
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        }  catch (Exception $ex){//if have exception
            //rollback transaction
            $transaction->rollBack();
            //set error message
            Yii::$app->getSession()->setFlash('error', [
                'error'=>Yii::t('backend',"System error happen. Please contact with System manager.")
                ]);
            //return index
            return $this->redirect(['index']);
        }
    }

    /**
     * Updates an existing MasterTax model.
     * If update is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return 
     * @throws 
     */
    public function actionUpdate($id) {
        try{
            //create transaction
            $transaction = $this->db->beginTransaction();
            $check_tax = TaxDetail::findOne($id);
            if(empty($check_tax)){
                Yii::$app->getSession()->setFlash('error', [
                    'error'=>Yii::t('backend',"System error happen. Please contact with System manager.")
                    ]);
                //return index
                return $this->redirect(['index']);
            }
            //create mode
            $model = $this->findModel($check_tax->tax_id);
            if(!Yii::$app->request->post()){
                //get multiple rate of tax
                $model->mutil_rate = MasterTax::initMultilRate($check_tax->tax_id);
            }
            //check submit form and validate
            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                //save tax with multiple rate
                $model->updateMultilRate($check_tax->tax_id);
                //commit transaction
                $transaction->commit();
                Yii::$app->getSession()->setFlash('success', [
                    'success' => Yii::t('backend', "Update Success")
                ]);
                //returm list tax
                return $this->redirect(['index']);
            } else {//if init update tax or not pass validate
                //return update tax
                return $this->render('update', [
                            'model' => $model,
                ]);
            }
        }  catch (Exception $ex){//if have exception
            //rollback transaction
            $transaction->rollBack();
            //set error message
            Yii::$app->getSession()->setFlash('error', [
                'error'=>Yii::t('backend',"System error happen. Please contact with System manager.")
                ]);
            //return index
            return $this->redirect(['index']);
        }
    }

    /**
     * Deletes an existing MasterTax model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return 
     * @throws 
     */
    public function actionDelete($id) {
        try{
            //create transaction
            $transaction = $this->db->beginTransaction();
            $check_tax = TaxDetail::findOne($id);
            if(empty($check_tax)){
                Yii::$app->getSession()->setFlash('error', [
                    'error'=>Yii::t('backend',"System error happen. Please contact with System manager.")
                    ]);
                //return index
                return $this->redirect(['index']);
            }
            $model = $this->findModel($check_tax->tax_id);
            if(!$model->checkDelete($id)){
                return 'false';
            }else{
                //soft delete tax
                $model->softDelete();
                TaxDetail::updateAll(['del_flg' => '1','updated_at' => strtotime("now")], ['tax_id' => $check_tax->tax_id]);
                //commit data
                $transaction->commit(); 
                Yii::$app->getSession()->setFlash('success', [
                    'success' => Yii::t('backend', "Delete Success")
                ]);
                //return list tax
                return $this->redirect(['index']);
            }           
        }  catch (Exception $ex){//if have exception
            //rollback transaction
            $transaction->rollBack();
            //set error message
            return 'false';
        }
    }

    /**
     * Finds the MasterTax model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return MasterTax the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = MasterTax::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }   
    
       // Validate From
    public function actionValidate() {
        $model1 = new MasterTax();
        $model1->load(Yii::$app->request->post());
        $model = new MasterTax();
        $model = !empty($model1->id_tax)?MasterTax::findOne($model1->id_tax):$model;
        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = 'json';
            return \yii\bootstrap\ActiveForm::validate($model);
//          return \yii\bootstrap\ActiveForm::validateMultiple([$model,$model2]);
        }
    }

}
