<?php

namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use common\models\MasterCustomer;
use common\models\MasterCustomerSearch;
use common\models\ChargeHistorySearch;
use common\models\CustomerStore;
use yii\console\Exception;
use yii\helpers\Json;
use common\models\MasterStaff;
use yii\helpers\ArrayHelper;
use common\models\BookingSearch;
use common\components\Util;
use common\models\MasterStore;

/**
 * MasterCustomerController implements the CRUD actions for MasterCustomer model.
 */
class MasterCustomerController extends Controller {

    //variable connection database
    public $db;

    public function init() {
        //get connection
        $this->db = Yii::$app->db;
    }

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all MasterCustomer models.
     * @return mixed
     */
    public function actionIndex() {
        $searchModel = new MasterCustomerSearch();
        if ($searchModel->load(Yii::$app->request->get())) {
            $dataProvider = $searchModel->searchGet(Yii::$app->request->queryParams);
        } else {
            $dataProvider = $searchModel->search();
        }

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    public function actionIndexHistoryPoint($customer_id = null) {
        $searchModel = new MasterCustomerSearch();
        $dataProvider = $searchModel->searchPointHistory(Yii::$app->request->queryParams);
        if (!empty($customer_id)) {
            $dataProvider = $searchModel->searchPointHistory(Yii::$app->request->queryParams, $customer_id);
            $customer = MasterCustomer::findModelCustomer($customer_id);
            $searchModel->customer_jan_code = $customer->customer_jan_code;
            $searchModel->name = $customer->first_name . $customer->last_name;
            $searchModel->sex = $customer->sex;
        }
        return $this->render('index_history_point', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    public function actionIndexChargeHistory($customer_id = null) {
        $searchModel = new ChargeHistorySearch();
        $dataProvider = $searchModel->chargeHistorySearch(Yii::$app->request->queryParams, null);
        if (!empty($customer_id)) {
            $dataProvider = $searchModel->chargeHistorySearch(Yii::$app->request->queryParams, $customer_id);
            $customer = MasterCustomer::findModelCustomer($customer_id);
            $searchModel->customer_jan_code = $customer->customer_jan_code;
            $searchModel->name = $customer->first_name . $customer->last_name;
            $searchModel->sex = $customer->sex;
        }
        return $this->render('index_charge_history', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    // Select staff by id store
    public function actionSelectStaffByIdStore() {
        $out = [];
        $result = [];
        $selected = '';
        if (isset($_POST['depdrop_parents'])) {
            $parents = $_POST['depdrop_parents'];
            if ($parents != null) {
                $store_id = $parents[0];
                if (!empty($_POST['depdrop_params'])) {
                    $params = $_POST['depdrop_params'];
                    $selected = $params[0];
                }
                if ($store_id == null) {
                    echo Json::encode(['output' => '', 'selected' => $selected]);
                    return;
                } else {
                    $out = (new \common\models\MasterStaff)->listStaffByStore($store_id);
                    foreach ($out as $key => $value) {
                        $arr = ['id' => $key, 'name' => $value];
                        $result[] = $arr;
                    }
                    echo Json::encode(['output' => empty($result) ? '' : $result, 'selected' => $selected]);
                    return;
                }
            }
        }
        echo Json::encode(['output' => '', 'selected' => $selected]);
    }
    
    
    // Select staff by id store
    public function actionSelectStaffByIdStoreCt() {
        $out = [];
        $result = [];
        $selected = '';
        if (isset($_POST['depdrop_parents'])) {
            $parents = $_POST['depdrop_parents'];
            if ($parents != null) {
                $store_id = $parents[0];
                if (!empty($_POST['depdrop_params'])) {
                    $params = $_POST['depdrop_params'];
                    $selected = $params[0];
                }
                if ($store_id == null) {
                    echo Json::encode(['output' => '', 'selected' => $selected]);
                    return;
                } else {
                    $out = (new \common\models\MasterStaff)->listStaffByStoreCT($store_id);
                    foreach ($out as $key => $value) {
                        $arr = ['id' => $key, 'name' => $value];
                        $result[] = $arr;
                    }
                    echo Json::encode(['output' => empty($result) ? '' : $result, 'selected' => $selected]);
                    return;
                }
            }
        }
        echo Json::encode(['output' => '', 'selected' => $selected]);
    }

    // Select seat by id store
    public function actionSelectSeatByIdStore() {
        $out = [];
        $result = [];
        $selected = '';
        if (isset($_POST['depdrop_parents'])) {
            $parents = $_POST['depdrop_parents'];
            if ($parents != null) {
                $store_id = $parents[0];
                if (!empty($_POST['depdrop_params'])) {
                    $params = $_POST['depdrop_params'];
                    $selected = $params[0];
                }
                if ($store_id == null) {
                    echo Json::encode(['output' => '', 'selected' => $selected]);
                    return;
                } else {
                    $out = (new \common\models\MasterSeat)->listSeatByStore($store_id);
                    foreach ($out as $key => $value) {
                        $arr = ['id' => $key, 'name' => $value];
                        $result[] = $arr;
                    }
                    echo Json::encode(['output' => empty($result) ? '' : $result, 'selected' => $selected]);
                    return;
                }
            }
        }
        echo Json::encode(['output' => '', 'selected' => $selected]);
    }

    // Select seat type by id store
    public function actionSelectTypeSeatByIdStore() {
        $out = [];
        $result = [];
        $selected = '';
        if (isset($_POST['depdrop_parents'])) {
            $parents = $_POST['depdrop_parents'];
            if ($parents != null) {
                $store_id = $parents[0];
                if (!empty($_POST['depdrop_params'])) {
                    $params = $_POST['depdrop_params'];
                    $selected = $params[0];
                }
                if ($store_id == null) {
                    echo Json::encode(['output' => '', 'selected' => $selected]);
                    return;
                } else {
                    $out = (new \common\models\MasterTypeSeat)->listTypeSeatByStore($store_id);
                    foreach ($out as $key => $value) {
                        $arr = ['id' => $key, 'name' => $value];
                        $result[] = $arr;
                    }
                    echo Json::encode(['output' => empty($result) ? '' : $result, 'selected' => $selected]);
                    return;
                }
            }
        }
        echo Json::encode(['output' => '', 'selected' => $selected]);
    }

    public function actionIndexHistoryTicket($customer_id = null) {
        $searchModel = new MasterCustomerSearch();
        $dataProvider = $searchModel->searchTicketHistory(Yii::$app->request->queryParams);
        if (!empty($customer_id)) {
            $dataProvider = $searchModel->searchTicketHistory(Yii::$app->request->queryParams, $customer_id);
            $customer = MasterCustomer::findModelCustomer($customer_id);
            $searchModel->customer_jan_code = $customer->customer_jan_code;
            $searchModel->name = $customer->first_name . $customer->last_name;
            $searchModel->sex = $customer->sex;
        }
        return $this->render('index_history_ticket', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    public function actionIndexBookingHistory($customer_id = null) {

        $searchModel = new BookingSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        if (!empty($customer_id)) {
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $customer_id);
            $customer = MasterCustomer::findModelCustomer($customer_id);
            $searchModel->customer_jan_code = $customer->customer_jan_code;
            $searchModel->customer_first_name = $customer->first_name;
            $searchModel->customer_last_name = $customer->last_name;
            $searchModel->sex = $customer->sex;
            $searchModel->mobile = $customer->mobile;
        }
        $this->ResetSession();
        return $this->render('/booking/reservation/index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    public function ResetSession() {
        $session = Yii::$app->session;
        $couponsBooked = $session->remove('BookingCoupon');  //id of coupon selected
        $productsBooked = $session->remove('BookingProduct'); //id of product selected
        $optionsBooked = $session->remove('BookingOption'); //id of option selected
        $staffBooked = $session->remove('BookingStaff'); //id of staff and date booking selected
        $storeBooked = $session->remove('BookingStore'); //id of store , coutomer id , coutomer name
        return true;
    }

    public function actionIndexVisitHistory($customer_id = null) {
        $searchModel = new \common\models\MasterCustomerSearch();
        $dataProvider = $searchModel->searchVisitHistory(Yii::$app->request->queryParams);
        if (!empty($customer_id)) {
            $dataProvider = $searchModel->searchVisitHistory(Yii::$app->request->queryParams, $customer_id);
            $customer = MasterCustomer::findModelCustomer($customer_id);
            $searchModel->customer_jan_code = $customer->customer_jan_code;
            $searchModel->customer_name = $customer->first_name . $customer->last_name;
            $searchModel->sex = $customer->sex;
        }
        return $this->render('index_visit_history', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single MasterCustomer model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id) {

        $searchModel = new MasterCustomerSearch();

        $dataProvider = $searchModel->findModel($id);
        //HoangNQ set role <> STAFF_ROLE
        //$permission = \common\components\FindPermission::getRole()->permission_id;
        //\common\components\FindPermission::permissionBeforeAction($permission != \common\components\Constants::STAFF_ROLE && $permission != \common\components\Constants::STORE_MANAGER_ROLE, $dataProvider->store_id);
        //HoangNQ end
        //        

        return $this->render('view', [
                    'model' => $dataProvider,
        ]);
    }

    /**
     * Creates a new MasterCustomer model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {

        try {
            $model = new MasterCustomer();
            $modelCustomerStore = new CustomerStore();

            $modelCustomerStore->news_transmision_flg = 0;
            $modelCustomerStore->black_list_flg = 0;

            // Create Translate 
            $transaction = $this->db->beginTransaction();

            if ($model->load(Yii::$app->request->post()) && $modelCustomerStore->load(Yii::$app->request->post()) && $model->validate() && $modelCustomerStore->validate()) {
                $model->register_store_id = $modelCustomerStore->store_id;
                //$model->register_kbn = '1';
                $model->company_id = Util::getCookiesCompanyId();
                
                 // Check Store Exists
                if (!$this->checkStoreByExists($model->register_store_id)) {
                    throw new Exception('Object deleted');
                }
                
                if ($model->save()) {
                    // Get customer_id
                    $modelCustomerStore->customer_id = $model->id;
                    $modelCustomerStore->rank_id = '00';
                    //$modelCustomerStore->register_kbn = '1';

                    if ($modelCustomerStore->save()) {
                        // Commit Transaction
                        $transaction->commit();
                        Yii::$app->session->setFlash('success', Yii::t("backend", "Save Success"));
                        return $this->redirect(['index']);
                    }
                } else {
                    //rollback transaction
                    $transaction->rollBack();

                    $modelCustomerStore->black_list_flg = 0;
                    $modelCustomerStore->news_transmision_flg = 0;

                    return $this->render('create', [
                                'model' => $model,
                                'modelCustomerStore' => $modelCustomerStore,
                    ]);
                }
            } else {

                return $this->render('create', [
                            'model' => $model,
                            'modelCustomerStore' => $modelCustomerStore,
                ]);
            }
        } catch (Exception $ex) {
            //rollback transaction
            $transaction->rollBack();
            //set error message
            Yii::$app->getSession()->setFlash('error', [
                'error' => Yii::t('backend', "System error happen. Please contact with System manager.")
            ]);
            if($ex->getMessage() == 'Object deleted'){
                return $this->redirect(['index']);
            }
            //return index
            return $this->render('create', [
                        'model' => $model,
                        'modelCustomerStore' => $modelCustomerStore,
            ]);
        }
    }

    /**
     * Updates an existing MasterCustomer model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
        $model_customer_store = (new CustomerStore())->findModel($id);
        //HoangNQ set role <> STAFF_ROLE
        \common\components\FindPermission::permissionBeforeAction(\common\components\FindPermission::getRole()->permission_id != \common\components\Constants::STAFF_ROLE, $model_customer_store->store_id);
        //HoangNQ end
        try {


            $model = $model_customer_store->customer;

            // Create Translate 
            $transaction = $this->db->beginTransaction();

            if ($model->load(Yii::$app->request->post()) && $model_customer_store->load(Yii::$app->request->post())) {
                $model->register_store_id = $model_customer_store->store_id;
                  // Check Store Exists
                if (!$this->checkStoreByExists($model->register_store_id)) {
                    throw new Exception('Object deleted');
                }
                if ($model->save() && $model_customer_store->save()) {
                    // Commit Transaction
                    $transaction->commit();
                    Yii::$app->session->setFlash('success', Yii::t("backend", "Update Success"));
                    return $this->redirect(['index']);
                } else {
                    // Update Fail Then Roll Back
                    $transaction->rollBack();

                    return $this->render('update', [
                                'model' => $model,
                                'modelCustomerStore' => $model_customer_store,
                    ]);
                }
            } else {
                // Update Fail Then Roll Back
                $transaction->rollBack();

                return $this->render('update', [
                            'model' => $model,
                            'modelCustomerStore' => $model_customer_store,
                ]);
            }
        } catch (Exception $ex) {
            //rollback transaction
            $transaction->rollBack();
            //set error message
            Yii::$app->getSession()->setFlash('error', [
                'error' => Yii::t('backend', "System error happen. Please contact with System manager.")
            ]);
            if($ex->getMessage() == 'Object deleted'){
                return $this->redirect(['index']);
            }
            //return index
            return $this->redirect(['index']);
        }
    }

    /**
     * Deletes an existing MasterCustomer model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {
        try {
            //create transaction
            $transaction = Yii::$app->db->beginTransaction();
            $model = MasterCustomer::findModelCustomer($id);
            if (!$model->checkDelete($id)) {
                return 'false';
            } else {
                //soft delete tax
                $model->softDelete();
                //commit data
                $transaction->commit();
                //return list tax
                Yii::$app->session->setFlash('success', Yii::t("backend", "Delete Coupon Message"));
                return $this->redirect(['index']);
            }
        } catch (Exception $ex) {//if have exception
            //rollback transaction
            $transaction->rollBack();
            return 'false';
        }
    }

    // Validate From
    public function actionValidate() {

        $model1 = new MasterCustomer();
        $model1->load(Yii::$app->request->post());
        $model = new MasterCustomer();
        $model = !empty($model1->id_hd_customer) ? MasterCustomer::findOne($model1->id_hd_customer) : $model;

        $modelCustomerStore = new CustomerStore();

        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post()) && $modelCustomerStore->load(Yii::$app->request->post())
        ) {
            Yii::$app->response->format = 'json';
            return \yii\bootstrap\ActiveForm::validate($model, $modelCustomerStore);
//          return \yii\bootstrap\ActiveForm::validateMultiple([$model,$model2]);
        }
    }

    // Validate From
    public function actionGetCustomerLottery() {

        $member_code = Yii::$app->request->post()['member_code'];
        $member_first_name = Yii::$app->request->post()['member_first_name'];
        $member_last_name = Yii::$app->request->post()['member_last_name'];
        $query = MasterCustomer::find();
        if (!empty($member_code)) {
            $query->andWhere(['customer_jan_code' => $member_code]);
        }
        if (!empty($member_first_name)) {
            $query->andWhere(['or', ['=', 'first_name', $member_first_name], ['=', 'first_name_kana', $member_first_name]]);
        }
        if (!empty($member_last_name)) {
            $query->andWhere(['or', ['=', 'last_name', $member_last_name], ['=', 'last_name_kana', $member_last_name]]);
        }
        $customer = $query->one();
        Yii::$app->response->format = 'json';
        if (!empty($customer)) {
            $data = [];
            $data['name'] = $customer->first_name . $customer->last_name;
            $data['id'] = $customer->id;
            return $data;
        }
        return "false";
    }
    
    /**
     * Finds the Store model do not exists based on its primary key value.
     * If the model is not found, return false.
     * @param integer $id_store
     * @return true
     */
    public function checkStoreByExists($id_store) {
        if (($model = MasterStore::findOne($id_store)) !== null) {
            return true;
        } else {
            return false;
        }
    }
}
