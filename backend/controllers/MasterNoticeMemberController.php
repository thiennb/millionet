<?php

namespace backend\controllers;

use Yii;
use common\models\MasterNoticeMemberSearch;
use common\models\MasterNoticeSearch;
use yii\web\Controller;
use yii\filters\VerbFilter;
use common\models\SettingAutoSendNotice;
use common\models\MasterNotice;
use Exception;
use common\models\NoticeCustomer;
use common\models\MasterStore;
use common\models\MasterCustomer;

/**
 * MasterNoticeMemberController implements the CRUD actions for MasterNoticeMember model.
 */
class MasterNoticeMemberController extends Controller {

    //variable connection database
    public $db;

    public function init() {
        //get connection
        $this->db = Yii::$app->db;
    }

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Customer for schedule send notice.
     * @return mixed
     */
    public function actionSettingPlan() {
        //set role <> STAFF_ROLE
        \common\components\FindPermission::permissionBeforeAction(\common\components\FindPermission::getRole()->permission_id != \common\components\Constants::STAFF_ROLE);
        try {
            // find customer will be send notice
            $searchModel = new MasterNoticeMemberSearch();
            $hidden_form = new SettingAutoSendNotice();
            if (isset(Yii::$app->request->queryParams["MasterNoticeMemberSearch"])) {
                $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
            } else {
                $dataProvider = null;
            }
            if(count(Yii::$app->request->get("MasterNoticeMemberSearch")) > 0){
                foreach (Yii::$app->request->get("MasterNoticeMemberSearch") as $key => $value ){
                    if($hidden_form->hasAttribute($key)){
                        $hidden_form->$key = $value;
                    }
                    if($key == 'last_staff'){
                        $hidden_form->last_staff_id = $value;
                    }
                    if($key == 'black_list'){
                        $hidden_form->black_list_flg = $value;
                    }
                }
            }
            return $this->render('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
                'hidden_form' =>  $hidden_form
            ]);
        } catch (Exception $ex) {
            //set error message
            Yii::$app->getSession()->setFlash('error', [
                'error' => Yii::t('backend', "System error happen. Please contact with System manager.")
            ]);
            //return index
            return $this->redirect(['setting-plan']);
        }
    }

    /**
     * Setting auto send notice for member
     * @return mixed
     */
    public function actionSettingAutoSend() {
        //set role <> STAFF_ROLE
        \common\components\FindPermission::permissionBeforeAction(\common\components\FindPermission::getRole()->permission_id != \common\components\Constants::STAFF_ROLE);
        try {
            //new model
            $model = new SettingAutoSendNotice();
            //set scenariol for validate when case create
            //$model->setScenario("create");
            //check init or submit form
            if ($model->load(Yii::$app->request->post())) {//if submit form and save success data
                $model->black_list_flg = $model->black_list_flg == '1' ? '0' : '1';
                if ($model->validate()) {
                    $notice = new MasterNotice();
                    return $this->render('create_one_auto_send', [
                                'model' => $notice,
                                'setting_auto_send' => $model
                    ]);
                    //return $this->redirect(['create-auto-send', 'id' => $model->id]);
                }
                $model->black_list_flg = $model->black_list_flg == '1' ? '0' : '1';
            } else {
                $model->black_list_flg = true;
            }
            return $this->render('setting_auto_send_notice', [
                        'model' => $model,
            ]);
        } catch (Exception $ex) {
            //set error message
            Yii::$app->getSession()->setFlash('error', [
                'error' => Yii::t('backend', "System error happen. Please contact with System manager.")
            ]);
            //return index
            return $this->redirect(['setting-auto-send']);
        }
    }

    /**
     * Create Notice for a customer
     * @param integer $id
     * @return mixed
     */
    public function actionCreateOne() {
        try {
            //create transaction
            $transaction = $this->db->beginTransaction();
            //new model
            $model = new MasterNotice();
            $id = Yii::$app->request->post('customer_id_create_one');
            $store_id = Yii::$app->request->post('store_id_create_one');
            if(empty($id) || empty($store_id)){
                //rollback transaction
                $transaction->rollBack();
                //set error message
                Yii::$app->getSession()->setFlash('error', [
                    'error' => Yii::t('backend', "System error happen. Please contact with System manager.")
                ]);
                //return index
                return $this->redirect(['create-one']);
            }
            $customer = MasterCustomer::findOne($id);
            $hidden_form = new SettingAutoSendNotice();
            $hidden_form->load(Yii::$app->request->post());
            if ($customer === null) {
                throw new \yii\web\NotFoundHttpException('The requested page does not exist.');
            }
            //check init or submit form
            if ($model->load(Yii::$app->request->post()) && $model->save()) {//if submit form and save success data
                //save data will be send
                $model->sendNotice($id, $store_id);
                $hidden_form->status_send = '00';
                $hidden_form->del_flg = '0';
                $hidden_form->notice_id = $model->id;
                $hidden_form->save();
                $transaction->commit();
                Yii::$app->getSession()->setFlash('success', [
                    'success' => Yii::t('backend', "Save Success")
                ]);
                return $this->redirect(['list-plan']);
            }
            return $this->render('create_one', [
                        'hidden_form' =>   $hidden_form,
                        'model' => $model,
                        'customer' => $customer->first_name . ' ' . $customer->last_name,
                        'customer_id'   =>  $id,
                        'store_id'  =>  $store_id,
            ]);
        } catch (Exception $ex) {
            //rollback transaction
            $transaction->rollBack();
            //set error message
            Yii::$app->getSession()->setFlash('error', [
                'error' => Yii::t('backend', "System error happen. Please contact with System manager.")
            ]);
            //return index
            return $this->redirect(['create-one']);
        }
    }

    /**
     * Create Notice for multiple customer
     * @return mixed
     */
    public function actionCreateMulti() {
        try {
            //create transaction
            $transaction = $this->db->beginTransaction();
            $arr_customer = [];
            
            if (!is_null(Yii::$app->request->post('Customer'))) {
                $customer_arr = Yii::$app->request->post('Customer');
                $store_arr = Yii::$app->request->post('Store');
                foreach ($customer_arr as $key => $val) {
                    array_push($arr_customer, array('customer_id' => $val, 'store_id' => $store_arr[$key]));
                }
            }
            $customer = '';
            //create transaction
            foreach ($arr_customer as $condition) {
                $customerList = \common\models\CustomerStore::find()->andWhere(['AND', $condition])->one();
                $customer .= isset($customerList->customer) ? $customerList->customer->first_name . $customerList->customer->last_name
                        . \yii\helpers\Html::hiddenInput('Customer[]', $condition['customer_id']) . \yii\helpers\Html::hiddenInput('Store[]', $condition['store_id']) . '<br>' : "";
            }
            //new model
            $model = new MasterNotice();
            $hidden_form = new SettingAutoSendNotice();
            $hidden_form->load(Yii::$app->request->post());
            //check init or submit form
            if ($model->load(Yii::$app->request->post()) && $model->save()) {//if submit form and save success data
                //save notice for multiple customer
                $model->saveNoticeMulti($arr_customer);
                $hidden_form->status_send = '00';
                $hidden_form->del_flg = '0';
                $hidden_form->notice_id = $model->id;
                $hidden_form->save();
                $transaction->commit();
                Yii::$app->getSession()->setFlash('success', [
                    'success' => Yii::t('backend', "Save Success")
                ]);
                return $this->redirect(['list-plan']);
            }
            return $this->render('create_one', [
                        'hidden_form' =>   $hidden_form,
                        'model' => $model,
                        'customer' => $customer
            ]);
        } catch (Exception $ex) {
            //rollback transaction
            $transaction->rollBack();
            //set error message
            Yii::$app->getSession()->setFlash('error', [
                'error' => Yii::t('backend', "System error happen. Please contact with System manager.")
            ]);
            //return index
            return $this->redirect(['setting-plan']);
        }
    }

    /**
     * Create Notice for a auto send to customer
     * @param integer $id
     * @return mixed
     */
    public function actionCreateAutoSend() {
        try {
            //create transaction
            $transaction = $this->db->beginTransaction();
            //$setting_auto_send = SettingAutoSendNotice::findOne($id);
            $model = new MasterNotice();
            $setting_auto_send = new SettingAutoSendNotice();
            //check init or submit form
            $model->auto_push_flg = MasterNotice::PUSH_AUTOMATIC;
            if ($model->load(Yii::$app->request->post()) && $model->save()) {//if submit form and save success data
                $setting_auto_send->notice_id = $model->id;
                if ($setting_auto_send->load(Yii::$app->request->post()) && $setting_auto_send->save()) {
                    $transaction->commit();
                    Yii::$app->getSession()->setFlash('success', [
                        'success' => Yii::t('backend', "Save Success")
                    ]);
                    return $this->redirect(['list-auto-send']);
                } else {
                    //rollback transaction
                    $transaction->rollBack();

                }
            }
            return $this->render('create_one_auto_send', [
                        'model' => $model,
                        'setting_auto_send' => $setting_auto_send
            ]);
        } catch (Exception $ex) {
            //rollback transaction
            $transaction->rollBack();
            //set error message
            Yii::$app->getSession()->setFlash('error', [
                'error' => Yii::t('backend', "System error happen. Please contact with System manager.")
            ]);
            //return index
            return $this->redirect(['setting-auto-send']);
        }
    }

    /**
     * Lists the notice of the delivery schedule
     * @return mixed
     */
    public function actionListPlan() {
        try {
            //search list customer will be deliver
            $searchModel = new MasterNoticeSearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
            return $this->render('deliver_schedule', [
                        'searchModel' => $searchModel,
                        'dataProvider' => $dataProvider,
            ]);
        } catch (Exception $ex) {
            //set error message
            Yii::$app->getSession()->setFlash('error', [
                'error' => Yii::t('backend', "System error happen. Please contact with System manager.")
            ]);
            //return index
            return $this->redirect(['/']);
        }
    }

    /**
     * Lists the notice send auto of the delivery schedule
     * @return mixed
     */
    public function actionListAutoSend() {
        try {
            //search list customer will be auto deliver
            $searchModel = new MasterNoticeSearch();
            $dataProvider = $searchModel->searchListAutoSend(Yii::$app->request->queryParams);

            return $this->render('deliver_schedule_auto_send', [
                        'searchModel' => $searchModel,
                        'dataProvider' => $dataProvider,
            ]);
        } catch (Exception $ex) {
            //set error message
            Yii::$app->getSession()->setFlash('error', [
                'error' => Yii::t('backend', "System error happen. Please contact with System manager.")
            ]);
            //return index
            return $this->redirect(['/']);
        }
    }

    /**
     * Lists history the notice was delivery
     * @return mixed
     */
    public function actionListHistory() {
        try {
            //search list history deliver
            $searchModel = new MasterNoticeSearch();
            $dataProvider = $searchModel->searchListHistory(Yii::$app->request->queryParams);
            return $this->render('deliver_history', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
        } catch (Exception $ex) {
            //set error message
            Yii::$app->getSession()->setFlash('error', [
                'error' => Yii::t('backend', "System error happen. Please contact with System manager.")
            ]);
            //return index
            return $this->redirect(['/']);
        }
    }

    /**
     * Detail notice send for customer
     * parameter integer $id
     * @return mixed
     */
    public function actionDetailDeliverSchedule($id) {
        try {
            //find notice_customer
            $notice_customer = NoticeCustomer::findOne($id);
            //if stop deliver
            if (Yii::$app->request->post()) {
                $notice_customer = (new NoticeCustomer)->updateDeliverSchedule($id);
                Yii::$app->getSession()->setFlash('success', [
                    'success' => Yii::t('backend', "Cancel Success")
                ]);
                return $this->redirect(['list-plan']);
            }
            //get list customer follow notice
            $searchModel = new MasterNoticeSearch();
            $model = $searchModel->searchDetailDeliverSchedule($notice_customer->notice_id);
            //get info notice
            $notice = MasterNotice::findOne($notice_customer->notice_id);

            return $this->render('detail_deliver_schedule', [
                        'model' => $model,
                        'notice' => $notice,
                        'status' => $notice_customer->status_send,
            ]);
        } catch (Exception $ex) {
            //set error message
            Yii::$app->getSession()->setFlash('error', [
                'error' => Yii::t('backend', "System error happen. Please contact with System manager.")
            ]);
            //return index
            return $this->redirect(['list-plan']);
        }
    }

    /**
     * Detail notice send for customer by auto
     * parameter integer $id
     * @return mixed
     */
    public function actionDetailDeliverScheduleAutoSend($id) {
        try {            
            //find notice_customer
            $notice_customer = NoticeCustomer::findOne($id);
            //if stop deliver
            if (Yii::$app->request->post()) {
                Yii::$app->getSession()->setFlash('success', [
                    'success' => Yii::t('backend', "Cancel Success")
                ]);
                NoticeCustomer::updateAll(['status_send' => MasterNotice::STATUS_SEND_CANCEL,'updated_at' => strtotime("now")], ['id' => $id]);
                return $this->redirect(['list-auto-send']);
            }
            //get detail notice and info customer
            $searchModel = new MasterNoticeSearch();
            $model = $searchModel->searchDetailDeliverHistory($id);
            //get info setting
            $setting = SettingAutoSendNotice::find()->andWhere(['notice_id' => $notice_customer->notice_id])->one();
            return $this->render('detail_deliver_schedule_auto_send', [
                        'model' => $model,
                        'setting' => $setting,
                        'status' => $notice_customer->status_send,
            ]);
        } catch (Exception $ex) {
            //set error message
            Yii::$app->getSession()->setFlash('error', [
                'error' => Yii::t('backend', "System error happen. Please contact with System manager.")
            ]);
            //return index
            return $this->redirect(['list-auto-send']);
        }
    }

    /**
     * Detail history notice was send for customer
     * parameter integer $id
     * @return mixed
     */
    public function actionDetailDeliverHistory($id) {
        try {
            $notice_customer = NoticeCustomer::findOne($id);
            //get detail notice and info customer
            $searchModel = new MasterNoticeSearch();
            $model = $searchModel->searchDetailDeliverHistory($id);
            $model->last_staff_name = $searchModel->getLastStaff($model->customer_jan_code);

            return $this->render('detail_deliver_history', [
                'model' => $model,
                'status' => $notice_customer->status_send,
                'id' => $notice_customer->notice_id
            ]);
        } catch (Exception $ex) {
            //set error message
            Yii::$app->getSession()->setFlash('error', [
                'error' => Yii::t('backend', "System error happen. Please contact with System manager.")
            ]);
            //return index
            return $this->redirect(['list-history']);
        }
    }
    

    /**
     * Create Notice for a customer
     * @param integer $id
     * @return mixed
     */
    public function actionUpdateNotice($id) {
        try {
            //create transaction
            $transaction = $this->db->beginTransaction();
            //new model
            $model = MasterNotice::findOne($id);
            if ($model === null) {
                throw new \yii\web\NotFoundHttpException('The requested page does not exist.');
            }
            if($model->auto_push_flg === '1'){
                return $this->redirect(['update-auto-send-notice', 'notice_id' =>$id]);
            }else{
                return $this->redirect(['update-manual-send-notice', 'notice_id' =>$id]);
            }
        } catch (Exception $ex) {
            //rollback transaction
            $transaction->rollBack();
            //set error message
            Yii::$app->getSession()->setFlash('error', [
                'error' => Yii::t('backend', "System error happen. Please contact with System manager.")
            ]);
            //return index
            return $this->redirect(['list-history']);
        }
    }
    
    //update manual send notice
    public function actionUpdateManualSendNotice($notice_id) {
        //set role <> STAFF_ROLE
        \common\components\FindPermission::permissionBeforeAction(\common\components\FindPermission::getRole()->permission_id != \common\components\Constants::STAFF_ROLE);
        try {
            // find customer will be send notice
            
            $notice = MasterNotice::findOne($notice_id);
            $setting_auto_send_notice = SettingAutoSendNotice::findOne(['notice_id' => $notice_id]);
            if ($notice === null || $setting_auto_send_notice === null) {
                throw new \yii\web\NotFoundHttpException('The requested page does not exist.');
            }
            
            $searchModel = new MasterNoticeMemberSearch();
            $hidden_form = new SettingAutoSendNotice();
            if (isset(Yii::$app->request->queryParams["MasterNoticeMemberSearch"])) {
                $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
            } else {
                $dataProvider = $searchModel->search($setting_auto_send_notice->attributes);
                $searchModel->attributes = $setting_auto_send_notice->attributes;
                $searchModel->last_staff = $setting_auto_send_notice->last_staff_id;
                $searchModel->black_list = $setting_auto_send_notice->black_list_flg;
            }
            if(count(Yii::$app->request->get("MasterNoticeMemberSearch")) > 0){
                foreach (Yii::$app->request->get("MasterNoticeMemberSearch") as $key => $value ){
                    if($hidden_form->hasAttribute($key)){
                        $hidden_form->$key = $value;
                    }
                    if($key == 'last_staff'){
                        $hidden_form->last_staff_id = $value;
                    }
                    if($key == 'black_list'){
                        $hidden_form->black_list_flg = $value;
                    }
                }
            }
            return $this->render('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
                'hidden_form' =>  $hidden_form
            ]);
        } catch (Exception $ex) {
            //set error message
            Yii::$app->getSession()->setFlash('error', [
                'error' => Yii::t('backend', "System error happen. Please contact with System manager.")
            ]);
            //return index
            return $this->redirect(['list-history']);
        }
    }
    
    
    //update auto send notice
    public function actionUpdateAutoSendNotice($notice_id) {
        //set role <> STAFF_ROLE
        \common\components\FindPermission::permissionBeforeAction(\common\components\FindPermission::getRole()->permission_id != \common\components\Constants::STAFF_ROLE);
        try {
            // find customer will be send notice
            
            $notice = MasterNotice::findOne($notice_id);
            $model = SettingAutoSendNotice::findOne(['notice_id' => $notice_id]);
            if ($notice === null || $model === null) {
                throw new \yii\web\NotFoundHttpException('The requested page does not exist.');
            }
            
            //check init or submit form
            if ($model->load(Yii::$app->request->post())) {//if submit form and save success data
                $model->black_list_flg = $model->black_list_flg == '1' ? '0' : '1';
                if ($model->validate()) {
                    $notice = new MasterNotice();
                    return $this->render('create_one_auto_send', [
                                'model' => $notice,
                                'setting_auto_send' => $model
                    ]);
                    //return $this->redirect(['create-auto-send', 'id' => $model->id]);
                }
                $model->black_list_flg = $model->black_list_flg == '1' ? '0' : '1';
            } else {
                $model->black_list_flg = true;
            }
            return $this->render('setting_auto_send_notice', [
                        'model' => $model,
            ]);
        } catch (Exception $ex) {
            //set error message
            Yii::$app->getSession()->setFlash('error', [
                'error' => Yii::t('backend', "System error happen. Please contact with System manager.")
            ]);
            //return index
            return $this->redirect(['list-history']);
        }
    }

    // Validate From
    public function actionValidate() {
        $model = new MasterNotice();
        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = 'json';
            $model = \yii\widgets\ActiveForm::validate($model);
            return $model;
        }
    }

}
