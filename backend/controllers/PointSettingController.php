<?php

namespace backend\controllers;

use Yii;
use common\models\PointSetting;
use common\components\Util;

class PointSettingController extends \yii\web\Controller {

    public function actionIndex() {
        $model = new PointSetting();

        if (!empty(PointSetting::getPointSetting())) {
            $model = PointSetting::getPointSetting();
        }else{
            $model->grant_point_visit_flg = 0;
        }
        // Format point_conversion_p
        $model->point_conversion_p = $this->checkFormatPoint($model->point_conversion_p);
        
        return $this->render('index', ['model' => $model]);
    }

    public function actionCreate() {

        $model = new PointSetting();
        if (!empty(PointSetting::getPointSetting())) {
           $model = PointSetting::getPointSetting();
        }
        $company_id = Util::getCookiesCompanyId();
        $model->company_id = $company_id;
        if ($model->load(Yii::$app->request->post())) {
            
            $model->point_conversion_yen = str_replace(',','',$model->point_conversion_yen);
            $model->grant_yen_per_point = str_replace(',','',$model->grant_yen_per_point);
            
            if ($model->save()) {
                Yii::$app->session->setFlash('success', Yii::t("backend", "Save Success"));
                return $this->redirect(['index']);
            }else{
                return $this->render('index', ['model' => $model]);
            }
        } else {
            return $this->render('index', ['model' => $model]);
        }
    }
    
     /*
     * Check format point
     * If the amount ends in 00 it will eliminate 00, whereas if the amount ending in '0' will remove '0', otherwise unchanged 
     * @param string $point
     * @return $point_format
     */

    protected function checkFormatPoint($point) {
        
        $normal_point_rate_st = substr($point, -2, 2);
        if($point == '0'){
            return $point_format = '0';
        }else if ($normal_point_rate_st == '00') {
            return $point_format = rtrim($point, "00");
        } else {
            if (substr($point, -1, 1) == '0') {
                return $point_format = rtrim($point,"0");
            }else{
                return $point;
            }
        }
    }

}
