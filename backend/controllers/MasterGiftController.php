<?php

namespace backend\controllers;

use Yii;
use common\models\MasterGift;
use common\models\MasterGiftSearch;
use common\models\MstOrderSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\models\MasterGiftDetails;
use common\models\MasterCustomer;
use common\models\MasterGiftReceiver;
use yii\web\Response;

/**
 * MasterGiftController implements the CRUD actions for MasterGift model.
 */
class MasterGiftController extends Controller {

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all MasterGift models.
     * @return mixed
     */
    public function actionIndex() {
        $searchModel = new MasterGiftSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider
        ]);
    }

    public function actionIndexOrder() {
        $searchModel = new MstOrderSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index_order', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * 
     * @param 
     * @return mixed
     */
    public function actionSearchCustomer() {
        $searchModel = new \common\models\MasterCustomerSearch();
        $list_customer_select = $searchModel->searchCustomerInGift(Yii::$app->request->bodyParams);
        return $this->renderAjax('_search_customer', ['list_customer_select' => $list_customer_select,]);
    }

    /**
     * Creates a new MasterGift model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new MasterGift();
        $gift_receiver = new MasterCustomer();
        $add_address1 = new MasterCustomer();
        $mastergiftreceiver = new MasterGiftReceiver();
        $gift_receiver->setScenario('giftinsert');
        return $this->render('create', [
                    'model' => $model,
                    'gift_receiver' => $gift_receiver,
                    'list_add_address' => $list_add_address,
                    'mastergiftreceiver' => $master_gift_receiver
        ]);
    }

    public function actionCreateGift($id = null, $show = false, $idGift = null) {
        $searchModel = new \common\models\MstOrderDetailSearch();
        $dataProvider = $searchModel->searchProduct($id);
        return $this->render('create-gift', [
                    'id' => $id,
                    'idGift' => $idGift,
                    'popup_show' => $show,
                    'dataProvider' => $dataProvider,
        ]);
    }

    public function actionCreateGiftGiftDetail() {
        $params = \Yii::$app->request->queryParams;
        try {
            //create transaction
            $transaction = Yii::$app->db->beginTransaction();
            if (!empty($params['selection'])) {
                $modelGift = new MasterGift();
                $gift_id = $modelGift->insertGiftFromOrder($params['id']);
                if (!empty($gift_id)) {
                    $modelGiftDetail = new MasterGiftDetails();
                    $gift = MasterGift::findOne($gift_id);
                    $gift->total_quantity += $modelGiftDetail->insertGiftDetailFromOrderDetail($params['selection'], $gift_id);
                    if ($gift->save()) {
                        $transaction->commit();
                    } else {
                        Yii::$app->getSession()->setFlash('error', [
                            'error' => Yii::t('frontend', "System error happen. Please contact with System manager.")
                        ]);
                        $transaction->rollBack();
                    }
                    return $this->redirect(['create-gift', 'id' => $params['id'], 'show' => true, 'idGift' => $gift_id]);
                }
            }
            return $this->redirect(['create-gift', 'id' => $params['id'], 'show' => false]);
        } catch (Exception $ex) {
            //rollback transaction
            $transaction->rollBack();
            //set error message
            Yii::$app->getSession()->setFlash('error', [
                'error' => Yii::t('frontend', "System error happen. Please contact with System manager.")
            ]);
            //return index
            return $this->redirect(['create-gift', 'id' => $params['id'], 'show' => false]);
        }
    }

    /**
     * Displays a single MasterGift model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id) {
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Updates an existing MasterGift model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionBackupUpdate($id = null) {
        $model = $this->findModel($id);
        $gift_receiver = MasterCustomer::findModelCustomer($model->customer_id);
        $list_add_address = [];
        $product_gift = (new \common\models\MstProduct)->getListProductByProductId(MasterGiftDetails::find()->andWhere(['gift_id' => $model->id])->select('product_id'));
        foreach (MasterGiftReceiver::find()->andWhere(['gift_id' => $id])->all() as $receiver_model) {
            $customer_model = MasterCustomer::findModelCustomer($receiver_model->customer_id);
            array_push($list_add_address, ['list_customer' => $customer_model, 'list_receiver' => $receiver_model]);
        }
        if (empty($list_add_address))
            array_push($list_add_address, ['list_customer' => new MasterCustomer(), 'list_receiver' => new MasterGiftReceiver()]);

        if (!empty(Yii::$app->request->post())) {
            //Validate 
            $listModelCustomer = Yii::$app->request->post('MasterCustomer')['list_customer'];
            $listModelReceiver = Yii::$app->request->post('MasterGiftReceiver');
            $list_model = $this->validateAjaxReceiver($id, $listModelCustomer, $listModelReceiver);
            $list_add_address = $list_model['list_model'];
                
            if (Yii::$app->request->post('btn-validate') != null) {
                if ($list_model['validate'])
                    array_push($list_add_address, ['list_customer' => new MasterCustomer(), 'list_receiver' => new MasterGiftReceiver()]);
                return $this->render('update', [
                            'model' => $model,
                            'gift_receiver' => $gift_receiver,
                            'list_add_address' => $list_add_address,
                            'product_gift' => $product_gift
                ]);
            } else {
                $gift_receiver->load(["MasterCustomer" => Yii::$app->request->post("MasterCustomer")["customer_receiver"]]);
                $gift_receiver->birth_date = Yii::$app->formatter->asDate($gift_receiver->birth_date, 'php:Y/m/d');
                if ($list_model['validate'] && $gift_receiver->validate()) {
                    $this->saveCustomerReceiver($id, $listModelCustomer, $listModelReceiver);
                    if ($gift_receiver->save()) {
                        Yii::$app->session->setFlash('success', Yii::t("backend", "Save Success"));
                        return $this->redirect(['index']);
                    }
                }
            }
        }
        return $this->render('update', [
                    'model' => $model,
                    'gift_receiver' => $gift_receiver,
                    'list_add_address' => $list_add_address,
                    'product_gift' => $product_gift
        ]);
    }

    /**
     * validate by Ajax.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    private function validateAjaxReceiver($id = null, $listModelCustomer, $listModelReceiver) {
        //$model = $this->findModel($id);
        $list_add_address = [];
        //$product_gift = (new \common\models\MstProduct)->getListProductByProductId(MasterGiftDetails::find()->andWhere(['gift_id' => $id])->select('product_id'));
        $validate_flag = true;
        foreach ($listModelCustomer as $key => $customer) {
            $customer['id'] = empty($customer["customer_id_new"]) ? $customer['id'] : $customer["customer_id_new"];
            $model_customer = empty($customer['id']) ? new MasterCustomer() : MasterCustomer::findModelCustomer($customer['id']);
            $model_customer->birth_date = Yii::$app->formatter->asDate($model_customer->birth_date, 'php:Y/m/d');
            //if customer validate success then create receiver gift
            $customer['post_code'] = str_replace("-", '', $customer['post_code']);
            $model_customer->load(['MasterCustomer' => $customer]);
            if (!$model_customer->validate())
                $validate_flag = false;
            if (!empty($listModelReceiver[$key]["id"]))
                $model_receiver = MasterGiftReceiver::findOne($listModelReceiver[$key]["id"]);
            else
                $model_receiver = new MasterGiftReceiver();
            $model_receiver->gift_id = $id;
            $model_receiver->load(['MasterGiftReceiver' => $listModelReceiver[$key]]);
            if (!$model_receiver->validate())
                $validate_flag = false;
            //var_dump($model_customer)
            array_push($list_add_address, ['list_customer' => $model_customer, 'list_receiver' => $model_receiver]);
        }

        return array('list_model' => $list_add_address, 'validate' => $validate_flag);
    }

    /**
     * Deletes an existing MasterGift model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {
        try {
            //create transaction
            $transaction = Yii::$app->db->beginTransaction();
            $model = $this->findModel($id);
 
            if (empty($model)) {
                return 'false';
            } else {
                //soft delete
                $model->softDelete();
                //commit data
                $transaction->commit();
                //return index 
                Yii::$app->session->setFlash('success', Yii::t("backend", 'Deleted successfully.'));
                return $this->redirect(['index'],'308');
            }
        } catch (Exception $ex) {
            //if have exception
            //rollback transaction
            $transaction->rollBack();
            return 'false';
        }
    }

    /**
     * Finds the MasterGift model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return MasterGift the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = MasterGift::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    private function saveCustomerReceiver($id = null, $listModelCustomer, $listModelReceiver) {
        foreach ($listModelCustomer as $key => $customer) {
            //customer_id is empty 
            if (empty($customer["id"])) {
                if (empty($customer["customer_id_new"])) {
                    //and customer_id_new is empty then create new customer and receiver
                    $model_customer = new MasterCustomer();
                } else {
                    //and customer_id_new isn't empty then update customer and create receiver
                    $model_customer = MasterCustomer::findModelCustomer($customer["customer_id_new"]);
                    $customer["id"] = $customer["customer_id_new"];
                }
                $model_customer->load(['MasterCustomer' => $customer]);
                $model_customer->birth_date = Yii::$app->formatter->asDate($model_customer->birth_date, 'php:Y/m/d');
                $model_receiver = new MasterGiftReceiver();
                $model_receiver['gift_id'] = $id;
                $model_receiver->load(['MasterGiftReceiver' => $listModelReceiver[$key]]);
                if ($model_customer->save()) {
                    $model_receiver["customer_id"] = $model_customer->id;
                    $model_receiver->save();
                }
            }
            //customer_id isn't empty 
            else {
                //and customer_id_new is empty then update receiver and update customer
                if (empty($customer["customer_id_new"])) {
                    $model_customer = MasterCustomer::findModelCustomer($customer["id"]);
                    $model_customer->load(['MasterCustomer' => $customer]);
                }
                //and customer_id_new is empty then update receiver and update customer
                else {
                    $model_customer = MasterCustomer::findModelCustomer($customer["customer_id_new"]);
                    $model_customer->load(['MasterCustomer' => $customer]);
                    $model_customer->id = $customer["customer_id_new"];
                }
                $model_customer->birth_date = Yii::$app->formatter->asDate($model_customer->birth_date, 'php:Y/m/d');
                $model_receiver = MasterGiftReceiver::find()->andWhere(['id' => $listModelReceiver[$key]['id']])->one();
                $model_receiver['gift_id'] = $id;
                $model_receiver->load(['MasterGiftReceiver' => $listModelReceiver[$key]]);
                if ($model_customer->save()) {
                    $model_receiver['customer_id'] = $model_customer->id;
                    $model_receiver->save();
                }
            }
        }
    }

    /**
     * Exports of goods to csv file
     */
    public function actionExportCsvGift() {
        if(Yii::$app->request->isPost){
           
            $list_id = Yii::$app->request->post('selection');
            $fp = fopen('php://output', 'w');
            
            // Set utf8
            fprintf($fp, chr(0xEF).chr(0xBB).chr(0xBF));

            fputcsv($fp, array(
                // Sender information
                "住所録コード", "お届け先電話番号", "お届け先郵便番号", "お届け先住所１", "お届け先住所2", "お届け先名称１","お届け先名称2",
                "お客様管理ナンバー", "お客様コード", "部署・担当者", "荷送人電話番号",
                "ご依頼主電話番号", "ご依頼主郵便番号", "ご依頼主住所１", "ご依頼主住所2", "ご依頼主名称１", "ご依頼主名称2", "荷姿コード",
                // Products
                "品名",
                //品名１
                //品名２
                //品名３
                //品名４
                //品名５
                "出荷個数", "便種（スピードで選択）", "便種（商品）", "配達日", "配達指定時間帯", "配達指定時間（時分）", "代引金額", "消費税", "決済種別", "保険金額",
                "保険金額印字", "指定シール①", "指定シール②", "指定シール③", "営業店止め", "ＳＲＣ区分", "営業店コード", "元着区分"
                ));

            if (!empty($list_id)) {
                $gifts = MasterGift::find()->andWhere(['id' => $list_id])->all();

                foreach ($gifts as $gift) {
                    $__receivers = MasterGiftReceiver::findAll(['gift_id' => $gift->id, 'del_flg' => 0]);
                    $receivers = [];
                    foreach($__receivers as $__receiver){
                        $receivers[$__receiver->customer_id][] = $__receiver;
                    }

                    foreach($receivers as $key => $products){
                        $customer = $products[0]->customer;
                        $productName = [];
                        foreach($products as $product){
                            $productName[] = substr(str_pad($product->product->name, 32,'0', STR_PAD_LEFT),0,32);
                        }
                        $customerMobile = str_pad($customer->mobile, 11,'0', STR_PAD_LEFT);
                        $customerMobile = substr($customerMobile, 0,3). '-'.substr($customerMobile, 3,4).'-'.substr($customerMobile, 7,4);
                        
                        
                        $receiverPostcode = str_pad($gift->masterCustomer->post_code, 7,'0', STR_PAD_LEFT);
                        $receiverPostcode = substr($customerPostcode, 0,3).'-'.substr($customerPostcode, 3,4);                        
                        $receiverMobile = str_pad($gift->masterCustomer->mobile, 11, '0', STR_PAD_LEFT);
                        $receiverMobile = substr($receiverMobile, 0,3). '-'.substr($receiverMobile, 3,4).'-'.substr($receiverMobile, 7,4);
                        $receiverAddress1 = substr(str_pad(mb_convert_kana($gift->masterCustomer->address, "KVC"),32,' ',STR_PAD_RIGHT),0,32);
                        $receiverAddress2 = substr(str_pad(mb_convert_kana($gift->masterCustomer->address2, "KVC"),32,' ',STR_PAD_RIGHT),0,32);
                        $receiverName = substr(str_pad(mb_convert_kana($gift->masterCustomer->first_name.' '.$gift->masterCustomer->last_name, "KVC"),16,' ',STR_PAD_RIGHT),0,32);
                        $receiverNameKana = substr(str_pad(mb_convert_kana($gift->masterCustomer->first_name_kana.' '.$gift->masterCustomer->last_name_kana, "KVC"),16,' ',STR_PAD_RIGHT),0,32);
                        fputcsv($fp, array(
                            // Receiver information
                            "",//$gift->masterCustomer->customer_jan_code, //住所録コード
                            $customerMobile, //お届け先電話番号
                            $customer->post_code, //お届け先郵便番号
                            $customer->address, //お届け先住所１
                            $customer->address2, //お届け先住所2
                            $customer->first_name, //お届け先名称１
                            $customer->last_name, //お届け先名称2
                            "", //お客様管理ナンバー
                            "", //お客様コード
                            "", //部署・担当者,
                            "",//荷送人電話番号,
                            $receiverMobile, //ご依頼主電話番号,
                            $receiverPostcode, //ご依頼主郵便番号
                            $receiverAddress1, //ご依頼主住所１
                            $receiverAddress2, //ご依頼主住所2
                            $receiverName,//ご依頼主名称１
                            $receiverNameKana,//ご依頼主名称2
                            '008',//荷姿コード
                            implode(',', $productName),
                            //品名１
                            //品名２
                            //品名３
                            //品名４
                            //品名５
                            "", //出荷個数
                            "000", //便種（スピードで選択）
                            "001", //便種（商品）
                            "", //配達日
                            "", // 配達指定時間帯
                            "", //配達指定時間（時分）
                            "", //代引金額
                            "", //消費税
                            "0", //決済種別
                            "", //保険金額
                            "0", //保険金額印字
                            "", //指定シール①
                            "", //指定シール②
                            "", //指定シール③
                            "", //営業店止め
                            "", //ＳＲＣ区分
                            "", //営業店コード
                            "1" //元着区分
                        ));
                    }
                                        
                }
            }
            header('Content-Type: text/csv; charset=utf-8');
            header('Content-Disposition: attachment; filename="export_gift_' . date('YmdHmi') . '.csv"');

            fclose($fp);
            return;
        }
        
        throw new NotFoundHttpException();
    }
    
    /**
     * Action update gift
     * @return render
     */
    public function actionUpdate($id = null){
        // Get gift
        $gift = $this->findModel($id);
        
        // Check if exist this gift
        if(!$gift){
            return $this->redirect('/master-gift');
        }
        
        if(\Yii::$app->request->isPost){
            $postMasterCustomer = Yii::$app->request->post('MasterCustomer');
            $sender = isset($postMasterCustomer['customer_sender']) ? $postMasterCustomer['customer_sender'] : null;
            $receivers = isset($postMasterCustomer['customer_receiver']) ? $postMasterCustomer['customer_receiver'] : [];
            
            $products = Yii::$app->request->post('MasterGiftReceiver');
                        
            $errors = [];
            
            // Validate for receivers
            $giftSender = null;
            if(!empty($sender['id'])){
                $giftSender = MasterCustomer::find()->where(['id' => $sender['id'], 'del_flg' => 0])->one();
            }

            if(!$giftSender){
                $errors['error-sender'] = \Yii::t('backend', 'Please choose sender');
            }
            
            if(empty($receivers)){
                $errors['error-receiver'] = \Yii::t('backend', 'Please choose at least one receiver');
            }

            // Validates receiver
            $lastReceivers = [];
            $giftDetails = $gift->order->giftOrderDetail;
            $productQuantityUsed = [];
            
            foreach ($receivers as $key => $value){
                $giftReceiver = null;
                if(!empty($value['id'])){
                    $giftReceiver = MasterCustomer::find()->where(['id' => $value['id'], 'del_flg' => 0])->one();
                }
                
                if(!$giftReceiver){
                    $errors['error-receiver-'.$key] = \Yii::t('backend', 'Please choose receiver');
                } else {
                    if(in_array($value['id'], $lastReceivers)){
                        $errors['error-receiver-'.$key] = \Yii::t('backend', 'Receiver is already added');
                        continue;
                    }
                    
                    $lastReceivers[] = $value['id'];
                    
                    if(isset($products[$key])){
                        $lastProduct = [];
                        foreach ($products[$key] as $proKey => $productValue){
                            if(!empty($productValue['product_id'])){
                                if(in_array($productValue['product_id'], $lastProduct)){
                                    $errors['error-product-'.$key.'-'.$proKey] = \Yii::t('backend', 'Product is already added');
                                    continue;
                                }
                                
                                $lastProduct[] = $productValue['product_id'];
                                
                                
                                $product = \common\models\MstProduct::findOne(['id' => $productValue['product_id']]);

                                if(!$product){
                                    $errors['error-product-'.$key.'-'.$proKey] = \Yii::t('backend', 'Invalid product');
                                } else if(!isset($productValue['quantity']) 
                                        || $productValue['quantity'] < 1){
                                    $errors['error-product-'.$key.'-'.$proKey] = \Yii::t('backend', 'Invalid quantity');
                                } else {
                                    if(!isset($productQuantityUsed[$product->jan_code])){
                                        $productQuantityUsed[$product->jan_code] = 0;
                                    }
                                    
                                    foreach($giftDetails as $detailKey => $detailValue){
                                        if($detailValue->jan_code == $product->jan_code){
                                            if($productValue['quantity'] > ($detailValue->quantity - $productQuantityUsed[$product->jan_code])) {
                                                $errors['error-product-'.$key.'-'.$proKey] = \Yii::t('backend', 'Invalid quantity');
                                            } else {
                                                $productQuantityUsed[$product->jan_code] += $productValue['quantity'];
                                            }
                                            
                                            break;
                                        }
                                    }
                                }
                            } else {
                                $errors['error-product-'.$key.'-'.$proKey] = \Yii::t('backend', 'Invalid product');
                            }
                        }
                    } else {
                        $errors['error-receiver-'.$key] = \Yii::t('backend', 'Please choose product');
                    }
                }
            }
            
            if(empty($errors)){
                \Yii::$app->session->set('update-gift-'.$id, ['sender' => $sender, 'receivers' => $receivers, 'products' => $products]);
            }

            return json_encode(['status' => empty($errors), 'errors' => $errors]);
        }
                
        // Check
        if($session = \Yii::$app->session->get('update-gift-'.$id)){
            // Set sender info
            $giftSender = MasterCustomer::find()->where(['id' => $session['sender'], 'del_flg' => 0])->one();
            
            // Get receiver customers
            $receiverCustomers = [];

            foreach ($session['receivers'] as $key => $customer){
                if(!isset($receiverCustomers[$customer['id']]['customer'])){
                    $receiverCustomers[$customer['id']]['customer'] = MasterCustomer::findOne($customer['id']);
                }

                if(!isset($receiverCustomers[$customer['id']]['products'])){
                    $receiverCustomers[$customer['id']]['products'] = [];
                }
                
                foreach ($session['products'][$key] as $k => $product){
                    $gift_receiver = new MasterGiftReceiver();
                    $gift_receiver->id = isset($product['id']) ? $product['id'] : '';
                    $gift_receiver->quantity = $product['quantity'];
                    $gift_receiver->product_id = $product['product_id'];
                    
                    $receiverCustomers[$customer['id']]['products'][] = $gift_receiver;
                }
            }
        } else {
            // Set sender info
            $giftSender = MasterCustomer::find()->where(['id' => $gift->customer_id, 'del_flg' => 0])->one();
            
            if(!$giftSender){
                $giftSender = new MasterCustomer();
            }
            
            // Get receiver customers
            $__receiverCustomers = MasterGiftReceiver::find()->where(['gift_id' => $gift->id, 'del_flg' => 0])->all();
            $receiverCustomers = [];

            foreach ($__receiverCustomers as $key => $customer){
                if(!isset($receiverCustomers[$customer->customer_id]['customer'])){
                    $receiverCustomers[$customer->customer_id]['customer'] = $customer->customer;
                }

                if(!isset($receiverCustomers[$customer->customer_id]['products'])){
                    $receiverCustomers[$customer->customer_id]['products'] = [];
                }

                $receiverCustomers[$customer->customer_id]['products'][] = $customer;
            }
        }
        
        return $this->render('update', compact('gift', 'giftSender', 'receiverCustomers'));
    }
    
    /**
     * actionConfirmUpdate
     * @param type $id
     */
    public function actionConfirmUpdate($id){
        // Get gift
        $gift = $this->findModel($id);
        
        // Check if exist this gift
        if(!$gift){
            return $this->redirect('/admin/master-gift');
        }
        
        $session = \Yii::$app->session->get('update-gift-'.$id);

        // Check if request is post
        if(\Yii::$app->request->isPost && $session){
            
            $transaction = Yii::$app->db->beginTransaction();

            try{
                // Update sender
                $gift->customer_id = $session['sender']['id'];
                $gift->save(false);
                
                $products = [];
                
                $__oldReceivers = MasterGiftReceiver::find()->where(['gift_id' => $id])->all();
                $oldProducts = \yii\helpers\ArrayHelper::index($__oldReceivers, 'id');
                $oldReceivers = \yii\helpers\ArrayHelper::index($__oldReceivers, 'customer_id');
                
                // Get old detail
                $oldGiftDetails = MasterGiftDetails::findAll(['gift_id' => $gift->id]);
                $oldGiftDetails = \yii\helpers\ArrayHelper::index($oldGiftDetails, 'id');
                
                // Update gift receiver
                foreach($session['receivers'] as $key => $receiver){
                    unset($oldReceivers[$receiver['id']]);
                    foreach ($session['products'][$key] as $pro => $proInfo){
                        $masterReceiver = null;
                        
                        if(!empty($proInfo['id'])){
                            $masterReceiver = MasterGiftReceiver::findOne($proInfo['id']);
                        }
                        
                        if(!$masterReceiver){
                            foreach ($oldProducts as $k => $value){
                                if($value->product_id == $proInfo['product_id'] && $value->customer_id == $receiver['id']){
                                    $masterReceiver = $value;
                                    unset($oldProducts[$k]);
                                    break;
                                }
                            }
                            
                            if(!$masterReceiver){
                                $masterReceiver = new MasterGiftReceiver();
                                $masterReceiver->gift_id = $id;
                            }
                        }else{
                            unset($oldProducts[$proInfo['id']]);
                        }
                        
                        $masterReceiver->customer_id = $receiver['id'];
                        $masterReceiver->quantity = $proInfo['quantity'];
                        $masterReceiver->product_id = $proInfo['product_id'];
                        $masterReceiver->del_flg = 0;
                        $masterReceiver->save(false);
                        
                        if(!array_key_exists($proInfo['product_id'],$products)){
                            $products[$proInfo['product_id']] = $proInfo['quantity'];
                        } else {
                            $products[$proInfo['product_id']] = $products[$proInfo['product_id']] + $proInfo['quantity'];
                        }
                    }
                }
                
                // Update to gift detail
                foreach ($products as $key => $quantity){
                    $detail = MasterGiftDetails::findOne(['gift_id' => $id, 'product_id' => $key]);

                    if(!$detail){
                        $detail = new MasterGiftDetails();
                        $detail->gift_id = $id;
                        $detail->product_id = $key;
                    }else{
                        unset($oldGiftDetails[$detail->id]);
                    }
                    $detail->del_flg = 0;
                    $detail->quantity = $quantity;
                    $detail->save(false);
                }
                
                // Delete gift is not exist from gift_receiver
                foreach ($oldProducts as $value){
                    $value->del_flg = 1;
                    $value->save(false);
                }
                
                // Delete gift is not exist from gift_receiver
                foreach ($oldReceivers as $value){
                    $value->del_flg = 1;
                    $value->save(false);
                }
                
                // Delete gift is not exist from gift_receiver
                foreach ($oldGiftDetails as $value){
                    $value->del_flg = 1;
                    $value->save(false);
                }
                
                $transaction->commit();
                Yii::$app->session->setFlash('success', Yii::t("backend", "Save Success"));
                \Yii::$app->session->set('update-gift-'.$id, null);
                return $this->redirect('/admin/master-gift');
            } catch (\Exception $e){
                $transaction->rollBack();
                //set error message
                Yii::$app->getSession()->setFlash('error', [
                    'error' => Yii::t('frontend', "System error happen. Please contact with System manager.")
                ]);
            }
        }
        
        // Check
        if($session){
            // Set sender info
            $giftSender = MasterCustomer::findModelCustomer($session['sender']['id']);
            
            // Get receiver customers
            $receiverCustomers = [];

            foreach ($session['receivers'] as $key => $customer){
                if(!isset($receiverCustomers[$customer['id']]['customer'])){
                    $receiverCustomers[$customer['id']]['customer'] = MasterCustomer::findOne($customer['id']);
                }

                if(!isset($receiverCustomers[$customer['id']]['products'])){
                    $receiverCustomers[$customer['id']]['products'] = [];
                }
                
                foreach ($session['products'][$key] as $k => $product){
                    $gift_receiver = new MasterGiftReceiver();
                    $gift_receiver->id = isset($product['id']) ? $product['id'] : '';
                    $gift_receiver->quantity = $product['quantity'];
                    $gift_receiver->product_id = $product['product_id'];
                    
                    $receiverCustomers[$customer['id']]['products'][] = $gift_receiver;
                }
            }
            
            return $this->render('confirm-update', compact('gift', 'giftSender', 'receiverCustomers'));
        }
        
        return $this->redirect('update?id='.$id);
    }
    
    /**
     * 
     */
    public function actionAjaxProductGift(){
        if(Yii::$app->request->isPost){
            $gift = MasterGift::findOne(Yii::$app->request->post('id'));
            
            if(!$gift){
                return;
            }
            
            $order = $gift->order;
            
            if(!$order){
                return;
            }
            $cp_key = Yii::t('backend', 'Coupon');
            $products = [$cp_key => []];
            
            // Get product
            $details = $order->giftOrderDetail;
            
            if($details){
                foreach ($details as $key => $value){
                    if($coupon = $value->coupon && !empty($coupon->discount_price_set)){
                        //\common\components\Util::dd($coupon);
                        //$products[$cp_key][] = ['name' => $coupon->name, 'jan_code' => $coupon->coupon_jan_code, 'price' => $coupon->discount_price_set];
                    }elseif($product = $value->product){
                        $products[$value->product->category->name][] = ['name' => $product->name, 'jan_code' => $product->jan_code, 'price' => $product->unit_price, 'id' => $product->id];
                    }
                }  
            }
            
            if(empty($products[$cp_key])){
                unset($products[$cp_key]);
            }
            
            $this->layout = false;
            return $this->render('_product-gift', ['products' => $products]);
        }
    }
    
    /**
     * actionAjaxPrint
     */
    public function actionAjaxPrint(){
        $id = Yii::$app->request->post('id');
        
        // Get gift
        $gift = $this->findModel($id);
        
        // Check if exist this gift
        if(!$gift){
            return;
        }
        if($session = \Yii::$app->session->get('update-gift-'.$id)){
            // Set sender info
            $giftSender = MasterCustomer::findModelCustomer($session['sender']['id']);

            // Get receiver customers
            //$__receiverCustomers = MasterGiftReceiver::find()->where(['gift_id' => $gift->id])->all();
            $receiverCustomers = [];

            foreach ($session['receivers'] as $key => $customer){
                if(!isset($receiverCustomers[$customer['id']]['customer'])){
                    $receiverCustomers[$customer['id']]['customer'] = MasterCustomer::findOne($customer['id']);
                }

                if(!isset($receiverCustomers[$customer['id']]['products'])){
                    $receiverCustomers[$customer['id']]['products'] = [];
                }

                foreach ($session['products'][$key] as $k => $product){
                    $gift_receiver = new MasterGiftReceiver();
                    $gift_receiver->id = isset($product['id']) ? $product['id'] : '';
                    $gift_receiver->quantity = $product['quantity'];
                    $gift_receiver->product_id = $product['product_id'];

                    $receiverCustomers[$customer['id']]['products'][] = $gift_receiver;
                }
            }
            $this->layout = false;
            return $this->render('_print', compact('gift', 'giftSender', 'receiverCustomers'));
        }
    }
}
