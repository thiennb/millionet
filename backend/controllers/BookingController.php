<?php

namespace backend\controllers;

use Yii;
use common\models\Booking;
use common\models\BookingSearch;
use common\models\MasterStaff;
use common\models\MasterSeat;
use common\models\MasterCoupon;
use common\models\MstProduct;
use common\models\MasterCustomer;
use common\models\MasterCustomerSearch;
use common\models\StoreSchedule;
use common\models\MasterStore;
use common\models\BookingBusiness;
use common\models\MasterTicket;
use common\models\MstCategoriesProduct;
use common\models\MstOrder;
use common\models\MstOrderTmp;
use common\models\MstOrderDetail;
use common\models\MstOrderDetailTmp;
use common\models\CustomerStore;
use common\models\CustomerStoreTmp;
use common\models\PayCoupon;
use common\models\PayCouponTmp;
use common\models\TicketHistory;
use common\models\TicketHistoryTmp;
use common\models\PointSetting;
use common\models\MasterSeatSearch;
use common\models\PointHistory;
use common\models\PointHistoryTmp;
use common\models\ChargeHistory;
use common\models\ChargeHistoryTmp;
use common\models\BookingSeat;
use common\components\Constants;
use common\components\Util;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\base\Exception;
use yii\base\Security;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use Carbon\Carbon;

/**
 * BookingController implements the CRUD actions for MasterBooking model.
 */
class BookingController extends Controller {

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all MasterBooking models.
     * @return mixed
     */
    public function actionSchedule() {
    
        $request = Yii::$app->request;
        $session = Yii::$app->session;
        $searchModel = new BookingSearch();
        $dataProvider= $searchModel->search(Yii::$app->request->queryParams);
        $dateBooking = Yii::$app->request->post('dateBooking');
        $storeId     = array_keys(MasterStore::getListStore())[0];

        if (is_null($dateBooking)) {
            $dateBooking = Carbon::today()->format("Y-m-d");
        }
        if ($request->post('store_id') != null) {
            $storeId = $request->post('store_id');
        }
        
        if(MasterStore::findOne($storeId)->booking_resources == MasterStore::BOOKING_RESULT_SALON){
            $staffList = MasterStaff::listStaff($storeId);
            $datasSchedule = $this->genTableSchedule($dateBooking, $storeId);
        }
        if(MasterStore::findOne($storeId)->booking_resources == MasterStore::BOOKING_RESULT_RESTAURANCE){
            $staffList = MasterSeat::listSeatByType($storeId ,null ,null);
            $datasSchedule = $this->genTableSeatSchedule($dateBooking, $storeId);
        }
        
//         $booking = Booking::find()
//                            ->joinWith('storeMaster')
//                            ->joinWith('listSeat')
//                            ->andWhere([
////                            "start_time"  => '08:30',
////                            "booking_seat.seat_id"    => 11,
//                            "booking.booking_date"=> '2016-12-14',
////                            "mst_store.booking_resources" => MasterStore::BOOKING_RESULT_RESTAURANCE
//                        ])
//                        ->andWhere(['<>','booking.status',  Booking::BOOKING_STATUS_CANCEL])
//                        ->andWhere(['<>','booking.status',  Booking::BOOKING_STATUS_DENY])
//                        ->all();
//                    Util::dd($booking);
//        Util::dd( MasterBooking::find()->one());
//        \Yii::$app->gon->push('data', $datasSchedule['datas']);
//        \Yii::$app->gon->push('mergeData', $datasSchedule['mergeData']);
//        \Yii::$app->gon->push('dateBooking', $dateBooking);

        $data       = json_encode($datasSchedule['datas']);
        $mergeData  = json_encode($datasSchedule['mergeData']);
//        Util::dd($data);
        return $this->render('schedule', [
                    'searchModel' => $searchModel,
                    'dataProvider'=> $dataProvider,
                    'date'        => $dateBooking,
                    'data'        => $data,
                    'mergeData'   => $mergeData,
                    'storeId'     => $storeId,
                    'staffList'   => $staffList
        ]);
    }

    // Validate From
    public function actionValidate() {
        $session  = Yii::$app->session;
        $model    = new MasterCustomer();
        $modelBooking = new Booking();
        $modelCustormerStore = new \common\models\CustomerStore();
        $modelTemp = new MasterCustomer();
        $modelTemp->load(Yii::$app->request->post());

        if ($modelTemp->id != null) {

            $model = MasterCustomer::findOne($modelTemp->id);
        }
//        $session = Yii::$app->session;
        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post()) && $modelBooking->load(Yii::$app->request->post()) && $modelCustormerStore->load(Yii::$app->request->post())) {
            Yii::$app->response->format   = 'json';
            
            $modelBooking->setScenario('bookingAdmin');
            
            //change scenario if store is restaurant
            if($modelBooking->storeMaster->booking_resources == \common\models\MasterStore::BOOKING_RESULT_RESTAURANCE  ) {
                $modelBooking->setScenario('bookingSeatAdmin');
            }
            $modelBooking->productSelect  = ($session->has('BookingCoupon')&& count($session->get('BookingCoupon')) > 0 )||( $session->has('BookingProduct')&& count($session->get('BookingProduct')) > 0  )? 1 :null;
            $modelBooking->staffSelect    = ($session->has('BookingStaff') && count($session->get('BookingStaff')) > 0
                && (
                       (isset($session->get('BookingStaff')['staff_id']) && $session->get('BookingStaff')['staff_id'] != null ) 
                    || (isset($session->get('BookingStaff')['seatSuit']) && count($session->get('BookingStaff')['seatSuit'] > 0 ) ) 
                  )
                
                && isset($session->get('BookingStaff')['booking_date']) 
                ) ? 1 :null;
            $model->post_code = str_replace("-", "", $model->post_code);
            $modelBooking->customer_id = $model->id != null ? $model->id : 0;
           
//            $modelBooking->product = $session->has('BookingProduct') || $session->has('BookingCoupon') ? 1:null;
//            $modelBooking->staff = $session->has('BookingStaff')  ? 1:null;
            $result = \yii\widgets\ActiveForm::validate($model, $modelBooking);
//            Util::dd($result);
            return $result;
        }
    }
    /**
     * Lists all MasterBooking models.
     * @return mixed
     */
    public function actionCheckValidBooking() {
        $session  = Yii::$app->session;
        $model       = new \common\models\StaffScheduleDetail();

        $post        = Yii::$app->request->post();
        $time_execute_minute =  ceil($post['timeExcute'] / 30) * 30;
        $time_execute = date('H:i:00', mktime(0,$time_execute_minute));
        $bookingCode = !empty($post['bookingId']) ? Booking::findOne($post['bookingId'])->booking_code : null ;
//        Util::dd($time_execute);
        $storeId        = $session->get('BookingStore')['store_id'];

       if(!BookingBusiness::checkStoreSchedule($storeId, $post['dateBooking'], $post['startTime'] ,$time_execute_minute)) return  "false";
        $result = $model->checkBookingIsValid($post['staffSelect'], $post['startTime'] . ":00", $time_execute, $post['dateBooking'],$bookingCode);

        return $result;
    }
    /**
     * Lists all MasterBooking models.
     * @return mixed
     */
    public function actionCheckValidSeatBooking() {
        $session  = Yii::$app->session;
        $model       = new \common\models\SeatScheduleDetail();

        $post        = Yii::$app->request->post();
        $time_execute =  ceil($post['timeExcute'] / 30) * 30;

        $bookingCode = !empty($post['bookingId']) ? Booking::findOne($post['bookingId'])->booking_code : null ;
        
//        Util::dd($bookingCode);
        $storeId        = $session->get('BookingStore')['store_id'];
        //validate store open time 
        if(!BookingBusiness::checkStoreSchedule($storeId, $post['dateBooking'], $post['startTime'] ,$time_execute)) return \GuzzleHttp\json_encode(false);
        
        
        $result = $model::checkBookingSeatTypeIsValid($storeId,$post['staffSelect'],$post['numberPeople'],$post['noSmoking'] ,$post['startTime'] . ":00", $time_execute, $post['dateBooking'],$bookingCode);
       
        return json_encode($result);
    }
    /**
     * show  reception setting.
     * @return mixed
     */
    public function actionReceptionsetting($selectStore= null ,$selectYear = null  , $selectMonth = null ) {

        $years      = array_combine(range(date("Y") + 1, 2010), range(date("Y") + 1, 2010));
        $months     = array_combine(range(01, 12), range(01, 12));
        $selectYear == null ? $selectYear = date('Y'):'';
        $selectMonth== null ? $selectMonth= date('n'):'';
        
        $listStore  = MasterStore::getListStore();
        
        $hours= [];

        if(isset($listStore)) { 
           
            if ($selectStore == null) {
                reset($listStore);
                $selectStore= key($listStore);
            }
            $store = MasterStore::findOne($selectStore);
            $hours = Util::genArrayWorkHours($store->time_open, $store->time_close, 30);
            
        }

        $datas = $this->genScheduleMonth($selectMonth, $selectYear , $selectStore);
//        Util::dd(array_values($datas));
        \Yii::$app->gon->push('scheduleMonth', $datas);


        return $this->render('reception_setting', [
                    'years'       => $years,
                    'selectMonth' => $selectMonth,
                    'selectYear'  => $selectYear,
                    'selectStore' => $selectStore,
                    'listStore'   => $listStore,
                    'hours'       => $hours,
                    'months'      => $months,
                    'store'       => $store
        ]);
    }

    /**
     * array schedule calendar store work time. use for reception setting
     * @param int $month 
     * @param int $year 
     * @param int $storeId
     * @return array
     */
    public function genScheduleMonth($month , $year  , $storeId ) {
        $datas = [];

        $days = $this->datesMonth($month, $year);
        $storeModel = MasterStore::findOne($storeId);
        foreach ($days as $day) {
            $model = StoreSchedule::findOne(['schedule_date' => $day, 'store_id' => $storeId]);
            if (empty($model)) {
                $model = new StoreSchedule();
                $model->work_flg = 1;
                $model->start_time = $storeModel->time_open;
                $model->end_time = $storeModel->time_close;
            }
            $item = [
                "start"       => $day,
                "name"        => $day,
                "idTimeStart" => "timestart_$day",
                "timeStart"   => $model->start_time,
                "idTimeEnd"   => "timesend_$day",
                "timeEnd"     => $model->end_time,
                "workFlg"     => (int) $model->work_flg
            ];
            $datas[] = $item;
        }

        return $datas;
    }

    /**
     * array days in month.
     * @return mixed
     */
    public function datesMonth($month, $year) {
        $num = cal_days_in_month(CAL_GREGORIAN, $month, $year);
        $dates_month = array();

        for ($i = 1; $i <= $num; $i++) {
            $mktime = mktime(0, 0, 0, $month, $i, $year);
            $date = date("Y-m-d", $mktime);
            $dates_month[$i] = $date;
        }

        return $dates_month;
    }

    /**
     * array days in month.
     * @return mixed
     */
    public function actionSavereceptionsetting() {
        if (Yii::$app->request->isPost) {

            $postData   = Yii::$app->request->post();
            $selectMonth= Yii::$app->request->post('selectMonth');
            $selectStore= Yii::$app->request->post('selectStore');
            $selectYear = Yii::$app->request->post('selectYear');
            unset($postData['_csrf-backend']);
            unset($postData['selectMonth']);
            unset($postData['selectStore']);
            unset($postData['selectYear']);
            foreach ($postData as $key => $day) {
                $model = StoreSchedule::findOne(['schedule_date' => $key, 'store_id' => $selectStore]);
                if (empty($model)) {
                    $model = new StoreSchedule();
                    $model->work_flg = 1;
                }
                isset($day['work'])     ? $model->work_flg  = 0 : $model->work_flg = 1;
                isset($day['starTime']) ? $model->start_time= $day['starTime']: "";
                isset($day['endTime'])  ? $model->end_time  = $day['endTime'] : "";
                $model->schedule_date = $key;
                $model->store_id      = $selectStore;
                $model->save(false);
            }
            Yii::$app->session->setFlash('success',  Yii::t('backend', 'Save Success'));
            return $this->redirect(['receptionsetting','selectStore'=>$selectStore,'selectMonth' => $selectMonth ,  'selectYear' => $selectYear ]);
        }
    }

  /**
     * show  staff booking setting schedule . 5.5
     * @return mixed
     */
    public function actionStaffBookingSetting() {

        $years      = array_combine(range(date("Y") + 1, 2010), range(date("Y") + 1, 2010));
        $months     = array_combine(range(01, 12), range(01, 12));
        $selectYear = date('Y');
        $selectMonth= date('n');
        $storeList  = MasterStore::getListStore();
        $session    = Yii::$app->session;
        $session->open();
//        $session->remove('scheduleStaff');
        $selectStore = array_keys($storeList)[0] ;


        if ((Yii::$app->request->isGet && 
            !empty(Yii::$app->request->get('selectYear'))&&!empty(Yii::$app->request->get('selectMonth'))&&!empty(Yii::$app->request->get('selectStore'))
            )) {
            $selectYear  = Yii::$app->request->get('selectYear');
            $selectMonth = Yii::$app->request->get('selectMonth');
            $selectStore = Yii::$app->request->get('selectStore');
        }
        
        if(isset($session['scheduleStaff']) 
            && ($selectMonth !=  $session['scheduleStaff']['selectMonth']
            || $selectYear !=  $session['scheduleStaff']['selectYear']
            || $selectStore !=  $session['scheduleStaff']['storeId']  ) 
//            || $session['scheduleStaff']['isDirty']  == true
            ){
            $session->remove('scheduleStaff');
            $session->remove('storeId');
            $session->remove('selectMonth');
            $session->remove('selectYear');
            $session->remove('isDirty');
        }
        $scheduleStaff = $session['scheduleStaff'];
        $staffList = \common\models\MasterStaff::listStaff($selectStore);

        $days = $this->datesMonth($selectMonth, $selectYear);
        $arr  = [0 => 0];
        $days = $arr + $days;
        $datasStaff = [];
        
        //get session for schedule
        
        if (count($staffList) > 0) {
            foreach ($staffList as $key => $name) {
                
                //get work day from database
                $workdaySaved = \common\models\StaffSchedule::find()->select('schedule_date')
                                ->where(['staff_id' => $key, 'work_flg' => 1])
                                ->asArray()->all();
                //index work day
                $workdaySaved = ArrayHelper::getColumn($workdaySaved, 'schedule_date');
                //            Util::dd($workday);
                foreach ($days as $index => $day) {
                    if($day != 0 && $scheduleStaff['isDirty'] == false){
                        $staff_schedule_model = \common\models\StaffSchedule::findOne(['staff_id'=> $key,'schedule_date'=> $day]);
                        $staff_schedule = null ;
                        $staff_not_working = null;
                            if(count($staff_schedule_model) > 0){
                                $staff_schedule    = $staff_schedule_model->attributes;
                                $staff_not_working = \common\models\StaffNotWorking::find()->where(['staff_schedule_id' => $staff_schedule_model->id])->asArray()->all();
                                
                            }
                            $scheduleStaff[$key][$day]['staff_schedule']    = $staff_schedule;
                            $scheduleStaff[$key][$day]['staff_not_working'] = $staff_not_working;
                            $scheduleStaff['isDirty'] =  false;
                    }
//                    Util::dd($scheduleStaff);
                    //check if this day is working day in session
                    
                    $isWorkDay =  $day != 0 && ($scheduleStaff[$key][$day]['staff_schedule']['work_flg'] == '1' || $scheduleStaff[$key][$day]['staff_schedule'] == null ) ? true : false;
                    $datasStaff[$key][] = [
                        'id'    => $key,
                        'name'  => $name,
                        'day'   => $day,
                        'work'  => $isWorkDay  ? Yii::t('backend', 'Work') : Yii::t('backend', "Off"),
                        'class' => $isWorkDay  ? 'working-day' : 'off-day'
                    ];
                }
            }
        }
        $scheduleStaff['storeId']     =  $selectStore;
        $scheduleStaff['selectMonth'] =  $selectMonth;
        $scheduleStaff['selectYear']  =  $selectYear;
        $session['scheduleStaff']     =  $scheduleStaff;
        $datas = $this->genScheduleMonth($selectMonth, $selectYear,$selectStore);
        //update session and save back
//        Util::d($session->get('scheduleStaff'));
//        $scheduleStaff = $session['scheduleStaff'];
//        $scheduleStaff[42]['2016-11-01']['staff_schedule']=    ['3'=>'3'];
//        $session['scheduleStaff']=    $scheduleStaff;
//        Util::dd($session->get('scheduleStaff'));

        return $this->render('staff_booking_setting', [
                    'years'       => $years,
                    'selectMonth' => $selectMonth,
                    'selectYear'  => $selectYear,
                    'selectStore' => $selectStore,
                    'months'      => $months,
                    'datasStaff'  => $datasStaff,
                    'days'        => $days,
                    //'storeList'   => $storeList,
                    'staffCount'  => count($staffList)
        ]);
    }

    /**
     * render popup setting schedule staff.
     * @return mixed
     */
    public function actionSettingStaffUpdate() {
        $request = Yii::$app->request;
        $session = Yii::$app->session;
        if ($request->isPost) {

            $staffId = $request->post('staffId');
            $schedule_date = $request->post('day');
            $store = $request->post('store');
//            $model = \common\models\StaffSchedule::find()->where(['staff_id' => $staffId, 'schedule_date' => $day])->one();
            $scheduleStaff = $session['scheduleStaff'];
            
            
//            Util::d($session['scheduleStaff']);
            if ( $scheduleStaff[$staffId][$schedule_date]['staff_schedule'] == null) {
                $model = new \common\models\StaffSchedule();
                $model = $model->attributes;
                $modelNotWorking = null;
            } else {
                $model = $scheduleStaff[$staffId][$schedule_date]['staff_schedule'];
                $modelNotWorking = $scheduleStaff[$staffId][$schedule_date]['staff_not_working'];
            }

            $title = Yii::$app->formatter->asDate($schedule_date) . ' ' . MasterStaff::findOne($staffId)->name;
            return $this->renderAjax('_dialog_staff_setting', [
                        'model'           => $model,
                        'modelNotWorking' => $modelNotWorking,
                        'staffId'         => $staffId,
                        'schedule_date'   => $schedule_date,
                        'store_id'        => $store,
                        'title'           => $title
            ]);
        }
        return false;
    }

    /**
     * save staff setting workdate for each day .
     * @return mixed
     */
    public static function saveStaffWorkDayForOneDay($staffId , $scheduleDate ,$storeId,$staffSchedule ,$staffNotWorking  ) {
      
      
        ini_set('memory_limit', '512M');
        ini_set('max_execution_time', 0);
        // $selectDay['staff_id'],
//        $selectDay['schedule_date']
//        $selectDay['work_flg']   
//        $selectDay['shift_id'] ? 
//        $selectDay['store_id']
//        $selectDay['start_work_off']
        $shiftId  = $staffSchedule != null && isset($staffSchedule['shift_id'] )? $staffSchedule['shift_id'] : \common\models\StoreShift::search($storeId)->one()->id;
        $workFlg  = $staffSchedule != null ? $staffSchedule['work_flg'] : '1';
        $staff = \common\models\StaffSchedule::find()->where([
                    'staff_id'      => $staffId,
                    'schedule_date' => $scheduleDate,
                    'del_flg'       => '0'
                ])->one();
            //new create schedule

            if (empty($staff)) {

                //save to staff_schedule

                if (empty($staff)) {
                    //create new if not found
                    $staff = new \common\models\StaffSchedule();
                }
                $staff->staff_id = $staffId;
                $staff->schedule_date = $scheduleDate;
                $staff->work_flg = $workFlg;
                $staff->shift_id = $shiftId;
                $staff->save();
                //save to table staff_schedule_detail
            } else {
                //have schedule

                $staff->staff_id = $staffId;
                $staff->schedule_date = $scheduleDate;
                $staff->work_flg = $workFlg;
                $staff->shift_id = $staffSchedule != null ? $shiftId: \common\models\StoreShift::search($storeId)->one()->id;
                $staff->save();
                //delete all old data and make new data
                \common\models\StaffNotWorking::deleteAll(['staff_schedule_id' => $staff->id]);
                \common\models\StaffScheduleDetail::deleteAll(['AND',
                ['=','staff_id', $staff->staff_id], ['=','booking_date', $staff->schedule_date], ['=','store_id' , $storeId] , ['booking_code'=>null]]);
            }
            if ($workFlg == '0') {
                return false;
            }
            $shift = \common\models\StoreShift::findOne($shiftId);
            $startTime = $shift->start_time;
            $endTime = $shift->end_time;
            self::saveShiftToScheduleDetail($staffId, $scheduleDate , $storeId , null, $startTime, $endTime ,\common\models\StaffScheduleDetail::BOOKING_STATUS_WORKING,true);

            //save to staff_not_working if have rest time
            if (isset($staffNotWorking) && count($staffNotWorking) > 0) {

                foreach ($staffNotWorking as  $value) {
                    $staffNotWork = new \common\models\StaffNotWorking();
                    $staffNotWork->staff_schedule_id = $staff->id;
                    $staffNotWork->start_time = $value['start_time'];
                    $staffNotWork->end_time   = $value['end_time'];
                    $staffNotWork->reason     = $value['reason'];
                    $staffNotWork->save();
                    //update not working in detail
                    self::saveShiftToScheduleDetail($staffId, $scheduleDate, $storeId, null, $staffNotWork->start_time, $staffNotWork->end_time, \common\models\StaffScheduleDetail::BOOKING_STATUS_OFF);
                }
            }

            //save staff work date in settingShift session
//            Util::dd($session['settingShift']);
        
        return true;
    }
    /**
     * save all setting from session to table .
     * @return mixed
     */
    public function actionSaveAllStaffWorkDay() {
        try{
        
        $transaction = Yii::$app->db->beginTransaction();
        $request = Yii::$app->request;
        $session = Yii::$app->session;
        if ($request->isGet) {
            
            $scheduleStaff = $session['scheduleStaff'];
            $storeId = $scheduleStaff['storeId'];
            unset($scheduleStaff['isDirty']);
            unset($scheduleStaff['storeId']);
            unset($scheduleStaff['selectMonth']);
            unset($scheduleStaff['selectYear']);
            
            foreach ((array)$scheduleStaff as $staffId => $schedule) {
                foreach ((array)$schedule as $scheduleDate => $detailSchedule ){
                  self::saveStaffWorkDayForOneDay($staffId, $scheduleDate, $storeId,$detailSchedule['staff_schedule'],  $detailSchedule['staff_not_working']);
                }
              
            }
            Yii::$app->session->setFlash('success',  Yii::t('backend', 'Save Success'));
            $transaction->commit();
            $session->remove('scheduleStaff');
            $session->remove('storeId');
            $session->remove('selectMonth');
            $session->remove('selectYear');
            $session->remove('isDirty');
            
//            return $this->redirect(['staff-booking-setting']);
            return true;
        }
        }catch (Exception $ex) {
            $transaction->rollBack();
            Yii::$app->getSession()->setFlash('error', [
                'error' => Yii::t('backend', "System error happen. Please contact with System manager.")
            ]);
            //return index
            Yii::error( 'File : '.$ex->getFile().' .Line : ' .$ex->getLine().' .Message : '.$ex->getMessage());
            return $this->redirect(['staff-booking-setting']);

        }
          
        return false;
    }
       
    /**
     * save staff setting workdate to session.
     * @return mixed
     */
    public function actionSaveStaffWorkDay2() {
        $request = Yii::$app->request;
        $session = Yii::$app->session;
        if ($request->isPost) {
//            $session['scheduleStaff']=    $scheduleStaff;
//               $datas = $this->genScheduleMonth($selectMonth, $selectYear,$selectStore);
            //update session and save back
//            Util::d($request->post());
            $selectDay     = $request->post();
            $staffId       = $selectDay['staff_id'];
            $schedule_date = $selectDay['schedule_date'];
            $work_flg      = $selectDay['work_flg'];
            $shift_id      = isset($selectDay['shift_id'])?$selectDay['shift_id']:'';
            $scheduleStaff = $session['scheduleStaff'];
            
                $scheduleStaff[$staffId][$schedule_date]['staff_schedule']['staff_id'] = $staffId ; 
                $scheduleStaff[$staffId][$schedule_date]['staff_schedule']['schedule_date'] = $schedule_date ; 
                $scheduleStaff[$staffId][$schedule_date]['staff_schedule']['work_flg'] = $work_flg ; 
                $scheduleStaff[$staffId][$schedule_date]['staff_schedule']['shift_id'] = $shift_id ; 
                $scheduleStaff[$staffId][$schedule_date]['staff_not_working'] = null;
            if(!empty($selectDay['start_work_off']) ){
              
                foreach ((array)$selectDay['start_work_off'] as $key => $value) {
//                    $staffNotWork = new \common\models\StaffNotWorking();
//                    $staffNotWork->staff_schedule_id = $staff->id;
                    $scheduleStaff[$staffId][$schedule_date]['staff_not_working'][$key]['start_time'] = $selectDay['start_work_off'][$key];
                    $scheduleStaff[$staffId][$schedule_date]['staff_not_working'][$key]['end_time'] = $selectDay['end_work_off'][$key];
                    $scheduleStaff[$staffId][$schedule_date]['staff_not_working'][$key]['reason'] = $selectDay['reason_work_off'][$key];
                }

            } 
                
            
            $scheduleStaff['isDirty'] =  true;
            $session['scheduleStaff'] =  $scheduleStaff;
//            Util::d($session->get('scheduleStaff'));

          
        }
        return false;
    }

    /**
     * save detail schedule to staff_schedule_detail.
     * @return mixed
     */
    public static function saveShiftToScheduleDetail($staff_id, $booking_date, $store_id, $booking_code, $startTime, $endTime, $booking_status = '1' , $isStaffSetting = false ) {
        //staff_id , booking_date

        $startTime= new \DateTime('2010-01-01 ' . $startTime);
        $endTime  = new \DateTime('2010-01-01 ' . $endTime);
        $endTime->sub(new \DateInterval('PT30M'));
        $interval = new \DateInterval("PT30M");
        $timeStep = 30;
        $timeArray= array();

        while ($startTime <= $endTime) {
            $timeArray[] = $startTime->format('H:i');
            $startTime->add(new \DateInterval('PT' . $timeStep . 'M'));
        }
		
        foreach ($timeArray as $value) {
            $model = \common\models\StaffScheduleDetail::find()->where([
                        'booking_date'  => $booking_date,
                        'staff_id'      => $staff_id,
                        'store_id'      => $store_id,
                        'booking_time'  => $value
                    ])->one();
            if (empty($model)) {
                $model = new \common\models\StaffScheduleDetail();
                $model->booking_status= \common\models\StaffScheduleDetail::BOOKING_STATUS_WORKING;
            }else{
                //update only if row don't  busy
                if($model->booking_status != \common\models\StaffScheduleDetail::BOOKING_STATUS_BUSY 
                || ( $model->booking_status == \common\models\StaffScheduleDetail::BOOKING_STATUS_BUSY && $isStaffSetting == false )){
                    $model->booking_code  = $booking_code;
                    $model->booking_status= $booking_status;
                }
            }
            $model->staff_id      = $staff_id;
            $model->booking_date  = $booking_date;
            $model->booking_time  = $value;
            $model->store_id      = $store_id;
            
            
            $model->save();
//          Util::d($booking_status);
        }
//      Util::dd($timeArray);

        return true;
    }

    /**
     * gen array data schedule for staff setting 5.13 日時・スタッフ選択
     * @return mixed
     */
    public function genTableSchedule($dateBooking = null, $storeId = null) {
        $datas = [];

        $hours = Constants::WORK_HOUR;
        if ($storeId != null) {
            $store = MasterStore::findOne($storeId);
            $hours = Util::genArrayWorkHours($store->time_open, $store->time_close, 30);
        }
        if (!empty($dateBooking)) {
            $date = $dateBooking;
        } else {
            $date = \Carbon\Carbon::today()->format("Y-m-d");
        }

        $staffs = MasterStaff::listStaff($storeId);
//          ArrayHelper::map(MasterStaff::find()->orderBy('name ASC')->all(), 'id', 'name');
      // gen first row header . Make staff name

      $row = [Yii::t('backend', 'Register staff')];
      $mergeData = [];

      $rowIndex = 1 ;
//      foreach ($staffs as $staffId => $staffName) {
//        $row[] = $staffName;
//      }
        foreach ($hours as $hour) {
            $row[] = $hour;
        }
        $datas[] = $row;

//      Util::dd($date);
        //gen row by time and number span row  with format spanrow@idBooking@content
        foreach ($staffs as $staffId => $staffName) {
            $row = [];
            $row[] = $staffName;
            $colIndex = 0;
            foreach ($hours as $hour) {
                $colIndex += 1;
                //find booking
                $booking = Booking::find()
                        ->joinWith('storeMaster')
                        ->andWhere([
                            "start_time"  => $hour,
                            "staff_id"    => $staffId,
                            "booking_date"=> $date,
                            "mst_store.booking_resources" => MasterStore::BOOKING_RESULT_SALON
                        ])
                        ->andWhere(['<>','booking.status',  Booking::BOOKING_STATUS_CANCEL])
                        ->andWhere(['<>','booking.status',  Booking::BOOKING_STATUS_CANCEL_BY_CUSTOMER])
                        ->andWhere(['<>','booking.status',  Booking::BOOKING_STATUS_DENY])
                        ->one();
                // add to array merge cell if booking not cancel or deny status
                if (!empty($booking) && ($booking->status != Booking::BOOKING_STATUS_CANCEL && $booking->status != Booking::BOOKING_STATUS_CANCEL_BY_CUSTOMER && $booking->status != Booking::BOOKING_STATUS_DENY) ) {

                    $spanRow = (int) ((strtotime($booking->end_time) - strtotime($booking->start_time)) / 3600 / 0.5);
                    $id = $booking->id;
                    $cell = [
                      'spanRow'     => $spanRow ,
                      'bookingId'   => $id ,
                      'bookingDate' => $booking->booking_date ,
                      'content'     => $booking->masterCustomer->first_name."".$booking->masterCustomer->last_name .$booking->imageBlackList. " \n$booking->start_time ～ $booking->end_time" . $booking->tempStatus,
                      'bookingStatus'=> $booking->status,
                      'isFinish'    => $booking->status == Booking::BOOKING_STATUS_FINISH ? true : false,
                      'type'        => 'booking'

                    ];
                  $mergeData[] = ["row" => $rowIndex , "col" => $colIndex , "rowspan" => 1 ,"colspan" => $spanRow];

                } else {
                   //offday
                    $staffSchedule = \common\models\StaffSchedule::find()->andWhere(['staff_id'=>$staffId ,'schedule_date' => $date])->one();
                    $cell = 1;
                    if(!empty($staffSchedule) ){
                        $notWorkingTime = $staffSchedule->getStaffNotWorking()->where(['start_time'=>$hour ])->one();
                        if(!empty($notWorkingTime )){
                            $spanRow = (int) ((strtotime($notWorkingTime->end_time) - strtotime($notWorkingTime->start_time)) / 3600 / 0.5);

                            $id = $notWorkingTime->id;

                            $cell = [
                                    'spanRow'     => $spanRow ,
                                    'staffNotWorkingId' => $id ,
                                    'workOffDate' => $date ,
                                    'content'     => $notWorkingTime->reason ." <br> $notWorkingTime->start_time ～ $notWorkingTime->end_time",
                                    'workFlg'     => '0',
                                    'type'        => 'offday'
                            ];
                            $mergeData[] = ["row" => $rowIndex , "col" => $colIndex , "rowspan" => 1 ,"colspan" => $spanRow];
                        }
                        
                        if($staffSchedule->work_flg == '0') $cell = 'day-off';

                    }
            
                }

                //add cell to row
                $row[] = $cell;
            }

            $datas[] = $row;
            $rowIndex += 1;
        }

//      Util::dd($datas);
        return [
            'datas'     => $datas,
            'mergeData' => $mergeData
        ];
    }
    
    
    /**
     * gen array data schedule for seat setting 5.14 日時・席タイプ選
     * @return mixed
     */
    public function genTableSeatSchedule($dateBooking = null, $storeId = null, $seatType = null , $noSmoking = null) {
        $datas = [];

        $hours = Constants::WORK_HOUR;
        if ($storeId != null) {
            $store = MasterStore::findOne($storeId);
            $hours = Util::genArrayWorkHours($store->time_open, $store->time_close, 30);
        }
        if (!empty($dateBooking)) {
            $date = $dateBooking;
        } else {
            $date = \Carbon\Carbon::today()->format("Y-m-d");
        }

        $seats = MasterSeat::listSeatByType($storeId ,$seatType ,$noSmoking);

        $row = [Yii::t('backend', 'Name Seat')];
        $mergeData = [];

        $rowIndex = 1 ;

        foreach ($hours as $hour) {
            $row[] = $hour;
        }
        $datas[] = $row;

//      Util::dd($date);
        //gen row by time and number span row  with format spanrow@idBooking@content
        foreach ($seats as $seatId => $seatName) {
            $row = [];
            $row[] = $seatName;
            $colIndex = 0;
            foreach ($hours as $hour) {
                $colIndex += 1;
                //find booking
                $booking = Booking::find()
                            ->joinWith('storeMaster')
                            ->joinWith('listSeat')
                            ->andWhere([
                            "start_time"  => $hour,
                            "booking_seat.seat_id"    => $seatId,
                            "booking_date"=> $date,
                            "mst_store.booking_resources" => MasterStore::BOOKING_RESULT_RESTAURANCE
                        ])
                        ->andWhere(['<>','booking.status',  Booking::BOOKING_STATUS_CANCEL])
                        ->andWhere(['<>','booking.status',  Booking::BOOKING_STATUS_CANCEL_BY_CUSTOMER])
                        ->andWhere(['<>','booking.status',  Booking::BOOKING_STATUS_DENY])
                        ->one();

                if (!empty($booking)) {

                    $spanRow = (int) ((strtotime($booking->end_time) - strtotime($booking->start_time)) / 3600 / 0.5);

                    $id = $booking->id;
                    $cell = [
                      'spanRow'     => $spanRow ,
                      'bookingId'   => $id ,
                      'bookingDate' => $booking->booking_date ,
                      'content'     => $booking->masterCustomer->first_name."".$booking->masterCustomer->last_name .$booking->imageBlackList.  $booking->tempStatus." \n$booking->start_time ～ $booking->end_time",
                      'bookingStatus'=> $booking->status,
                      'isFinish'    => $booking->status == Booking::BOOKING_STATUS_FINISH ? true : false,
                      'type'        => 'booking'

                    ];
                  $mergeData[] = ["row" => $rowIndex , "col" => $colIndex , "rowspan" => 1 ,"colspan" => $spanRow,'bookingId'=>$id];

                } 
                else {
//                  //offday
//                    $staffSchedule = \common\models\StaffSchedule::find()->andWhere(['staff_id'=>$staffId ,'schedule_date' => $date])->one();
                    $cell = 1;
//                    if(!empty($staffSchedule) ){
//                        $notWorkingTime = $staffSchedule->getStaffNotWorking()->where(['start_time'=>$hour ])->one();
//                        if(!empty($notWorkingTime )){
//                            $spanRow = (int) ((strtotime($notWorkingTime->end_time) - strtotime($notWorkingTime->start_time)) / 3600 / 0.5);
//
//                            $id = $notWorkingTime->id;
//
//                            $cell = [
//                                    'spanRow'     => $spanRow ,
//                                    'staffNotWorkingId' => $id ,
//                                    'workOffDate' => $date ,
//                                    'content'     => $notWorkingTime->reason ." \n$notWorkingTime->start_time~$notWorkingTime->end_time",
//                                    'workFlg'     => '0',
//                                    'type'        => 'offday'
//                            ];
//                            $mergeData[] = ["row" => $rowIndex , "col" => $colIndex , "rowspan" => 1 ,"colspan" => $spanRow];
//                        }
//                        
//                        if($staffSchedule->work_flg == '0') $cell = 'day-off';
//
//                    }
//            
                }

                //add cell to row
                $row[] = $cell;
            }

            $datas[] = $row;
            $rowIndex += 1;
        }

//      Util::dd($datas);
        return [
            'datas'     => $datas,
            'mergeData' => $mergeData
        ];
    }



    /**
     * Lists all MasterBooking models.
     * @return mixed
     */
    public function actionIndex($date = null) {
        $searchModel = new BookingSearch();
        $prams = Yii::$app->request->queryParams;
        if (isset($date)) {
            $searchModel->booking_date_from = $date;
            $searchModel->booking_date_to   = $date;
        }
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $this->ResetSession();
        return $this->render('reservation/index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * render modal view simply booking.
     * @param integer $id
     * @return mixed
     */
    public function actionViewSimply($id)
    {
        $model = $this->findModel($id);

        $productList = ArrayHelper::getColumn(
                      \common\models\BookingProduct::find()->where(['booking_id'=>$model->id,'option_id'=>null])->select('product_id')->all()
                      ,'product_id');
//        $productName =  \common\models\MstProduct::find()->where(['id'=>$productList])->select('name')->all();
        $productName = BookingSearch::getProductPriceTable( implode(',', $productList), $model->booking_date);
//        $optionList = ArrayHelper::getColumn(
//                      \common\models\BookingProduct::find()->where(['booking_id'=>$model->id])->select('option_id')->all()
//                      ,'option_id');
        $optionName = \common\models\BookingProduct::find()->with(['product','option'])->where(['booking_id'=>$model->id ])->andWhere(['not', ['option_id' => null]])->all();

        $couponList = ArrayHelper::getColumn(
                \common\models\BookingCoupon::find()->where(['booking_id'=>$model->id])->select('coupon_id')->all()
                      ,'coupon_id');
//        $couponName =  \common\models\MasterCoupon::find()->where(['id'=>$couponList])->select('title')->all();
        $couponName = BookingSearch::getCouponPriceTable( implode(',', $couponList), $model->booking_date);
//        $moneyPoint  = $model->moneyFromPoint;

//        Util::d($productName);
//        Util::d($optionName);
//        Util::dd($couponName);
        if(Yii::$app->request->isAjax && Yii::$app->request->post('btnAction') != null){
            $model->status = Yii::$app->request->post('btnAction');
            $model->save();
        }

        return $this->renderAjax('_view_simple', [
                      'model'       => $model,
                      'productName' => $productName,
                      'optionName'  => $optionName ,
                      'couponName'  => $couponName
        ]);

    }


    /**
     * render modal view detail booking.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id) {
      try{
        \yii\helpers\Url::remember();
        $transaction = Yii::$app->db->beginTransaction();
        $model        = $this->findModel($id);
        $productList  = ArrayHelper::getColumn(
                        \common\models\BookingProduct::find()->andWhere(['booking_id' => $model->id, 'option_id' => null])->select('product_id')->all()
                        , 'product_id');
        $optionList   = \common\models\BookingProduct::find()->andWhere(['booking_id' => $model->id])->andWhere(['not', ['option_id' => null]])->all();
        $productsOption = [];
        if(isset($optionList)){
            foreach ($optionList as $value){
                $value->option_product_id = isset($value->option_product_id) ? $value->option_product_id : 0;
                $productsOption[]=$value->option_id.'/'.$value->product_id.'/'.$value->option_product_id;
            }
        }
        $couponList   = ArrayHelper::getColumn(
                        \common\models\BookingCoupon::find()->andWhere(['booking_id' => $model->id])->select('coupon_id')->all()
                        , 'coupon_id');
        $resultBooking = BookingBusiness::getPriceTable(implode(',', $couponList), implode(',', $productList) ,implode(',', $productsOption) ,$model->booking_date );
        $moneyPoint     = $model->moneyFromPoint;
        $model->booking_price   = $resultBooking['total_price'];
        $model->booking_total   = $resultBooking['total_price'] - $moneyPoint;
        
//         $moneyPoint  = $model->moneyFromPoint;
//        Util::dd($resultBooking);
//        Util::d($optionName);
//        Util::dd($couponName);
        if (Yii::$app->request->isAjax && Yii::$app->request->post('btnAction') != null) {
            
            $model->setScenario('statusBooking');
            $model->status = Yii::$app->request->post('btnAction');
            if(Yii::$app->request->post('btnAction') == Booking::BOOKING_STATUS_APPROVE){
                if($model->storeMaster->booking_resources == MasterStore::BOOKING_RESULT_SALON && $model->staff_id == null){                 
                  echo 'false' ;return false ;
                }
                if($model->storeMaster->booking_resources == MasterStore::BOOKING_RESULT_RESTAURANCE && count($model->listSeat) == 0){                 
                  echo 'false' ;return false ;
                }
                
                $modelSchedule = new \common\models\StaffScheduleDetail();
                $time_excute = (int) ((strtotime($model->end_time) - strtotime($model->start_time))/ 60 );
                $result = $modelSchedule->checkBookingIsValid($model->staff_id, $model->start_time . ":00", $time_excute, $model->booking_date,$model->booking_code);
                //TODO check time staff valid for booking
                $result==true ? self::saveShiftToScheduleDetail($model->staff_id, $model->booking_date, $model->store_id, $model->booking_code, $model->start_time, $model->end_time, \common\models\StaffScheduleDetail::BOOKING_STATUS_BUSY):'';
            }
            if(Yii::$app->request->post('btnAction') == Booking::BOOKING_STATUS_CANCEL || Yii::$app->request->post('btnAction') == Booking::BOOKING_STATUS_CANCEL_BY_CUSTOMER || Yii::$app->request->post('btnAction') == Booking::BOOKING_STATUS_DENY ){
                
                self::saveShiftToScheduleDetail($model->staff_id, $model->booking_date, $model->store_id, null, $model->start_time, $model->end_time, \common\models\StaffScheduleDetail::BOOKING_STATUS_WORKING);
            }
            if(Yii::$app->request->post('btnAction') == Booking::BOOKING_STATUS_FINISH ){
//                 $model->save();
//                 $transaction->commit();
                 return \yii\helpers\Url::to(['accounting-management','bookingId'=>$model->id]);
                
            }
            
            $model->save();
            $transaction->commit();
        }
        $check_list = \common\models\BookingChecklistView::find()
                ->innerJoin('checklist', 'checklist.id = booking_checklist.checklist_id')
                ->innerJoin('question', 'question.id = checklist.question_id')
                ->andWhere(['booking_checklist.booking_id' => $id,'booking_checklist.del_flg' => '0'])
                ->select([
                    'checklist.type as type',
                    'checklist.display_flg as display_flg',
                    'question.option as option',
                    'question.max_choice as max_choice',
                    'question.notice_content as notice_content',
                    'question.question_content as question_content',
                    'booking_checklist.answer_list as answer_content',
                    ])
                ->all();
        foreach ($check_list as $key=>$val){
            if((int)$val->type == 1){
                $check_list[$key]->answer_content = [0=>Yii::t('backend','Check list confirmed')]; 
            }else{
                if((int)$val->option == 1){
                    $answer = \common\models\QuestionAnswer::findOne(['id'=>(int)$val->answer_content]);
                    $check_list[$key]->answer_content = [0=>$answer->content]; 
                }else if((int)$val->option == 2){
                    $answer_list = explode(',', $val->answer_content);
                    $answer = \common\models\QuestionAnswer::find()->andWhere(['id'=>$answer_list])->all();
                    $answer_content = [];
                    foreach ($answer as $key1=>$val1){
                        $answer_content[$key1] = $val1->content;
                    }
                    $check_list[$key]->answer_content = $answer_content;
                }else{
                    $check_list[$key]->answer_content = [0=>$val->answer_content];
                }
            }
        }
        
        $order = $model->getOrder()->with('masterOrderDetail', 'payCoupons')->one();
        //$order_items = $order ? \common\models\MstOrderSearch::getOrderItems($order) : [];

        return $this->render('reservation/view', [
                                'resultBooking'   => $resultBooking,
                                'check_list'      => $check_list,
                                'model'           => $model,                   
                                'order'           => $order,
                                //'order_items' => $order_items
                            ]);
        } catch (Exception $ex) {
            //rollback transaction
            $transaction->rollBack();
            //set error message
            Yii::$app->getSession()->setFlash('error', [
                'error' => Yii::t('backend', "System error happen. Please contact with System manager.")
            ]);
            Yii::error( 'File : '.$ex->getFile()." . \n Line : " .$ex->getLine()." . \n Message : ".$ex->getMessage());
            
        }
    }

    public function ResetSession() {
        $session        = Yii::$app->session;
        $couponsBooked  = $session->remove('BookingCoupon');  //id of coupon selected
        $productsBooked = $session->remove('BookingProduct'); //id of product selected
        $optionsBooked  = $session->remove('BookingOption'); //id of option selected
        $staffBooked    = $session->remove('BookingStaff'); //id of staff and date booking selected
        $storeBooked    = $session->remove('BookingStore'); //id of store , coutomer id , coutomer name
        return true;
    }

    /**
     * Creates a new MasterBooking model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {

        try {
            $transaction = Yii::$app->db->beginTransaction();
        
            $request = Yii::$app->request;
            $session = Yii::$app->session;
            Url::remember();
            $model   = new MasterCustomer();
//            Util::d($session->get('BookingStaff'));
            $modelBooking = new Booking();
            $modelCustormerStore = new \common\models\CustomerStore();
            $modelCustormerStore->news_transmision_flg = 0;
            $couponsBookedName = [];
            $productBookedName = [];
            $optionsBookedName = [];
            $staffBookedName   = [];
            //pjax get selected from session
            $couponsBooked  = $session->has('BookingCoupon') ? $session->get('BookingCoupon') : []; //id of coupon selected
            $productsBooked = $session->has('BookingProduct')? $session->get('BookingProduct'): []; //id of product selected
            $optionsBooked  = $session->has('BookingOption') ? $session->get('BookingOption') : []; //id of option selected
            $staffBooked    = $session->get('BookingStaff'); //id of staff and date booking selected
            $storeBooked    = $session->get('BookingStore'); //id of store , coutomer id , coutomer name
            if (isset($storeBooked['custormer_id']) && $storeBooked['custormer_id'] != null) {

                $id = $storeBooked['custormer_id'];
                $model = MasterCustomer::findOne($id);
//            $modelCustormerStore->find()->where(['customer_id'=>$model->id,'store_id' => $model->register_store_id])->one();
                $modelCustormerStore = \common\models\CustomerStore::find()
                        ->where(['customer_id' => $model->id, 'store_id' => isset($storeBooked['store_id'])? $storeBooked['store_id'] : $model->register_store_id])
                        ->one();
                if (count($modelCustormerStore) == 0)
                    $modelCustormerStore = new \common\models\CustomerStore();
                    $modelCustormerStore->news_transmision_flg = 0;
            }else {
                $model->attributes = $storeBooked['custormerModel'];
                $modelCustormerStore->attributes = $storeBooked['custormerStoreModel'];
            }

            if (isset($storeBooked['bookingModel'])) {

                $modelBooking->attributes = $storeBooked['bookingModel'];
            }

//         Util::d($couponsBooked);
//         Util::d($productsBooked);
//         Util::d($optionsBooked);
//         Util::d($staffBooked);
//         Util::d($storeBooked);
//            Util::d(BookingSearch::searchCouponsById(implode(',', $couponsBooked), $storeBooked['store_id']));
            //coupon name array
            if (count($couponsBooked) > 0) {
                foreach ($couponsBooked as $couponId) {
                    $couponsBookedName[] = \common\models\MasterCoupon::findOne($couponId)->title;
                }
            }
            //product name
            if (count($productsBooked) > 0) {
                foreach ($productsBooked as $productId) {
                    $productBookedName[] = \common\models\MstProduct::findOne($productId)->name;
                }
            }
            //option name
            if (count($optionsBooked) > 0) {
                foreach ($optionsBooked as $optionId => $productId) { 
                    $optionId= explode('_', $optionId)[1];
                    
                    $optionsName = \common\models\MasterOption::findOne($optionId)->name .' : '. \common\models\MstProduct::findOne($productId)->name;
                    $optionsBookedName[] =$optionsName;
                }
            }
            //staff and date booked if salon type
            if (count($staffBooked) > 0 && isset($staffBooked['staff_id'] )) {
                $staffId = $staffBooked['staff_id'];
                $staffBookedName = $staffBooked;
                $staffBookedName['staff_name'] = \common\models\MasterStaff::findOne($staffId)->name;
            }
            //seat and date booked if retaurance type
            if (count($staffBooked) > 0 && isset($staffBooked['seatSuit'] )) {
                $seatIds = ArrayHelper::getColumn($staffBooked['seatSuit'], 'id');
                $staffBookedName = $staffBooked;
                $staffBookedName['seat_name'] = ArrayHelper::map(
                  MasterSeat::find()->andWhere(['id'=>$seatIds])->all(),
                  'id', 'name');
            }
            //          Util::dd($id);
//            $model->news_transmision_flg != 1? $model->news_transmision_flg = 0 : $model->news_transmision_flg = 1 ;
//        if(Yii::$app->request->isAjax || Yii::$app->request->isGet ){
//          //hande create when new  and pjax
//          Util::d('save2');
//        }
            //handle post request , save booking
            if ($request->isPost && $model->load(Yii::$app->request->post()) && $modelBooking->load(Yii::$app->request->post()) && $modelCustormerStore->load(Yii::$app->request->post()) && (count($couponsBooked) > 0 || count($productsBooked) > 0 )
            ) {
               
                //save new custormer if not find id
                if ($model->id == null) {
                    $model  = new MasterCustomer();
                    $data   = Yii::$app->request->post();
                    unset($data['MasterCustomer']['id']);
                    $model->load($data);
                    $model->register_store_id = $storeBooked['store_id'];
                    $model->save();
                    
                }
                if($modelCustormerStore->id == null){
                     $modelCustormerStore->addCustomerStore($storeBooked['store_id'],  $model->id ,$storeBooked['custormerStoreModel']['black_list_flg']);
                }
                

                $modelBooking->validate();
                $modelBooking->status = Booking::BOOKING_STATUS_APPROVE;

                //save storeBooked info
                $modelBooking->customer_id  = $model->id;
                $modelBooking->store_id     = $storeBooked['store_id'];

                //save staffBooked info
                $modelBooking->booking_date = $staffBooked['booking_date'];
                $modelBooking->staff_id     = isset($staffBooked['staff_id']) ? $staffBooked['staff_id']: null;
                if(isset($staffBooked['numberPeople'])){
                    $modelBooking->number_of_people = $staffBooked['numberPeople'];
                }
                $modelBooking->start_time   = $staffBooked['start_time'];
                $modelBooking->end_time     = $staffBooked['end_time'];
                //save
               
                $productsOption = [];
                foreach ($optionsBooked as $key => $value){
                  $arrKey = explode('_', $key);
                  $productsOption[]=$arrKey[1].'/'.$value.'/'.$arrKey[2];
                }
                $sumPriceProduct = 0 ;

                $result = BookingBusiness::getPriceTable(implode(',', $couponsBooked), implode(',', $productsBooked) ,implode(',', $productsOption) ,$modelBooking->booking_date );
//                Util::d($result);
//              
                //money from point to discount
                $moneyPoint = $modelBooking->moneyFromPoint;

                $modelBooking->booking_price    = $result['total_price'];
                $modelBooking->booking_discount = $moneyPoint;
                $modelBooking->booking_total    = $result['total_price'] - $moneyPoint;
                $modelBooking->save();
                // save coupons to table bookingcoupon
                foreach ($couponsBooked as $couponId) {
                    $sumPriceProduct  = isset($result['coupons'][$couponId]) ? $result['coupons'][$couponId]['total_price'] : 0;
                    $bookingCoupon    = new \common\models\BookingCoupon();
                    $bookingCoupon->booking_id = $modelBooking->id;
                    $bookingCoupon->coupon_id  = $couponId;
                    $bookingCoupon->quantity   = 1;
                    
                    $bookingCoupon->unit_price = $sumPriceProduct;
                    $bookingCoupon->save();
                }
                //save product to table booking_product

                
                foreach ($productsBooked as $productId) {
                    $sumPriceProduct            = isset($result['products'][$productId]) ? $result['products'][$productId]['total_price'] : 0;
                    $bookingProduct             = new \common\models\BookingProduct();
                    $bookingProduct->booking_id = $modelBooking->id;
                    $bookingProduct->product_id = $productId;
                    $bookingProduct->quantity   = 1;
                  
                    $bookingProduct->unit_price = $sumPriceProduct;
                    $bookingProduct->save();
                }
                //save option to table booking_product

                foreach ($optionsBooked as $optionIdName => $productId) {
                  //$optionId have format option_$idOption_$idProductSelect
                    $optionId                   = explode('_', $optionIdName)[1];
                    $productSelectId            = explode('_', $optionIdName)[2];
                    $keyOption                  = $optionId.'/'.$productId.'/'.$productSelectId;
                    $sumPriceProduct            = isset($result['options'][$keyOption]) ? $result['options'][$keyOption]['total_price'] : 0;
                    $bookingProduct             = new \common\models\BookingProduct();
                    $bookingProduct->booking_id = $modelBooking->id;
                    $bookingProduct->option_id  = $optionId;
                    $bookingProduct->option_product_id  = $productSelectId !=0 ? $productSelectId :null;
                    $bookingProduct->product_id = $productId;
                    $bookingProduct->quantity   = 1;
                    $bookingProduct->unit_price = $sumPriceProduct;
                    $bookingProduct->save();
                }
                
                //update schedule detail 
                if(isset($staffBooked['staff_id'])){
                    self::saveShiftToScheduleDetail($modelBooking->staff_id, $modelBooking->booking_date, $modelBooking->store_id, $modelBooking->booking_code, $modelBooking->start_time, $modelBooking->end_time, \common\models\StaffScheduleDetail::BOOKING_STATUS_BUSY);
                }
                if(isset($staffBooked['seatSuit'])){
                    foreach ((array)$staffBooked['seatSuit'] as $index => $item) {
                        $bookingSeat = new BookingSeat();
                        $bookingSeat->booking_id  = $modelBooking->id;
                        $bookingSeat->seat_id     = $item['id'];
                        $bookingSeat->actual_load = $item['actual_load'];
                        $bookingSeat->save();
                        self::saveShiftToSeatScheduleDetail($item['id'], $modelBooking->booking_date, $modelBooking->store_id, $modelBooking->booking_code, $modelBooking->start_time, $modelBooking->end_time, \common\models\StaffScheduleDetail::BOOKING_STATUS_BUSY);
                    }
                    
                }
                //setting pust notice
                BookingBusiness::PushMessage(BookingBusiness::PUSH_TIMING_BOOKING,$modelBooking->id);
                BookingBusiness::PushMessage(BookingBusiness::PUSH_TIMING_BOOKING_BEFORE,$modelBooking->id);
                $transaction->commit();
                
                $session->setFlash('success', Yii::t("backend", "Save Success"));
                Yii::$app->session->setFlash('success', Yii::t("backend", "Save Success"));
                return $this->redirect( ['index', 'date' => date("Y/m/d")]);
//            $modelBooking->start_time
//            $modelBooking->save();
//            return $this->redirect(['index']);
            }

            return $this->render('reservation/create', [
                        'model'             => $model,
                        'modelBooking'      => $modelBooking,
                        'modelCustormerStore'=> $modelCustormerStore,
                        'couponsBookedName' => $couponsBookedName,
                        'productBookedName' => $productBookedName,
                        'optionsBookedName' => $optionsBookedName,
                        'staffBookedName'   => $staffBookedName,
                        'storeBooked'       => $storeBooked
            ]);
        } catch (\Exception $ex) {
            //rollback transaction
            $transaction->rollBack();
            //set error message
            Yii::$app->getSession()->setFlash('error', [
                'error' => Yii::t('backend', "System error happen. Please contact with System manager.")
            ]);
            //return index
            Yii::error( 'File : '.$ex->getFile().' .Line : ' .$ex->getLine().' .Message : '.$ex->getMessage());
            return $this->redirect(['index']);
        }
    }
    
    
    /**
     * Update MasterBooking model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionUpdate($id) {

        try {

            $request = Yii::$app->request;
            $session = Yii::$app->session;
            Url::remember();
            $transaction = Yii::$app->db->beginTransaction();
            $modelBooking = $this->findModel($id);
            
            $model   = MasterCustomer::findOne($modelBooking->customer_id);
//            Util::d($session->get('BookingStaff'));
            
            $modelCustormerStore = \common\models\CustomerStore::find()
                        ->where(['customer_id' => $model->id, 'store_id' => $model->register_store_id])
                        ->one();
            if(empty($modelCustormerStore) ){
              $modelCustormerStore = new \common\models\CustomerStore;
              $modelCustormerStore->customer_id = $model->id;
              $modelCustormerStore->store_id    = $model->register_store_id;
            }
            $modelCustormerStore->news_transmision_flg = $modelCustormerStore->news_transmision_flg !=null ? $modelCustormerStore->news_transmision_flg : 0;
            $couponsBookedName = [];
            $productBookedName = [];
            $optionsBookedName = [];
            $staffBookedName   = [];
            //pjax get selected from session
            if(!$request->isAjax){
            $this->GenSessionUpdate($modelBooking, $model, $modelCustormerStore);
            }
            $couponsBooked  = $session->has('BookingCoupon') ? $session->get('BookingCoupon') : [];  //id of coupon selected
            $productsBooked = $session->has('BookingProduct') ? $session->get('BookingProduct') : []; //id of product selected
            $optionsBooked  = $session->has('BookingOption') ? $session->get('BookingOption') : []; //id of option selected
            $staffBooked    = $session->get('BookingStaff'); //id of staff and date booking selected
            $storeBooked    = $session->get('BookingStore'); //id of store , coutomer id , coutomer name
            
            if (isset($storeBooked['custormer_id']) && $storeBooked['custormer_id'] != null) {

                $id = $storeBooked['custormer_id'];
                $model = MasterCustomer::findOne($id);
//            $modelCustormerStore->find()->where(['customer_id'=>$model->id,'store_id' => $model->register_store_id])->one();
                $modelCustormerStore = \common\models\CustomerStore::find()
                        ->where(['customer_id' => $model->id, 'store_id' => $model->register_store_id])
                        ->one();
                if (count($modelCustormerStore) == 0)
                    $modelCustormerStore = new \common\models\CustomerStore();
            }else {
                $model->attributes = $storeBooked['custormerModel'];
                $modelCustormerStore->attributes = $storeBooked['custormerStoreModel'];
            }

            if (isset($storeBooked['bookingModel'])) {

                $modelBooking->attributes = $storeBooked['bookingModel'];
            }

//         Util::d($couponsBooked);
//         Util::d($productsBooked);
//         Util::d($optionsBooked);
//         Util::d($staffBooked);
//         Util::d($storeBooked);
        
//            Util::d(BookingSearch::searchCouponsById(implode(',', $couponsBooked), $storeBooked['store_id']));
            //coupon name array
            if (count($couponsBooked) > 0) {
                foreach ($couponsBooked as $couponId) {
                    $couponsBookedName[] = \common\models\MasterCoupon::findOne($couponId)->title;
                }
            }
            //product name
            if (count($productsBooked) > 0) {
                foreach ($productsBooked as $productId) {
                    $productBookedName[] = \common\models\MstProduct::findOne($productId)->name;
                }
            }            
            //option name
            if (count($optionsBooked) > 0) {
                foreach ($optionsBooked as $optionId => $productId) {
                    $optionId= explode('_', $optionId)[1];
                    
                    $optionsName = \common\models\MasterOption::findOne($optionId)->name .' : '. \common\models\MstProduct::findOne($productId)->name;
                    $optionsBookedName[] =$optionsName;
                }
            }
            //staff and date booked if salon type
            if (count($staffBooked) > 0 && isset($staffBooked['staff_id'] )) {
                $staffId = $staffBooked['staff_id'];
                $staffBookedName = $staffBooked;
               
                $staffBookedName['staff_name'] = $staffId!= null ? \common\models\MasterStaff::findOne($staffId)->name :'';
            }
            //seat and date booked if retaurance type
            if (count($staffBooked) > 0 && isset($staffBooked['seatSuit'] )) {
                $seatIds = ArrayHelper::getColumn($staffBooked['seatSuit'], 'id');
                $staffBookedName = $staffBooked;
                $staffBookedName['seat_name'] = ArrayHelper::map(
                  MasterSeat::find()->andWhere(['id'=>$seatIds])->all(),
                  'id', 'name');
            }
            
            //js variable for table schedule
            $store      = MasterStore::findOne($modelBooking->store_id);
            $staffList  = \common\models\MasterStaff::listStaff($modelBooking->store_id);
            $hourList   = Util::genArrayWorkHours($store->time_open, $store->time_close, 30);
            $jsStaffBooking = [
                'content'     => "$model->name  \\n $modelBooking->start_time ～ $modelBooking->end_time" ,
                'staffIndex'  => Util::getIndexArray($staffList, $modelBooking->staff_id) ,
                'timeIndex'   => Util::getIndexArray($hourList, $modelBooking->start_time) ,
                'timeSpan'    => count(Util::genArrayWorkHours($modelBooking->start_time , $modelBooking->end_time, 30)) -1 ,
                'startTime'   => $modelBooking->start_time ,
                'endTime'     => $modelBooking->end_time ,
                'dateBooking' => $modelBooking->booking_date
            ];

            
//            \common\components\Util::dd(\common\models\BookingBusiness::PushMessage('01',$model,$modelCustormerStore,$modelBooking));
            //handle post request , save booking
            if ($request->isPost && $model->load(Yii::$app->request->post()) && $modelBooking->load(Yii::$app->request->post()) && $modelCustormerStore->load(Yii::$app->request->post()) && (count($couponsBooked) > 0 || count($productsBooked) > 0 )
            )   {
            
                
                //save new custormer if not find id
               $modelBookingOld =Booking::findOne($modelBooking->id); 

                $modelBooking->validate();
//                $modelBooking->status = Booking::BOOKING_STATUS_APPROVE;

                //save storeBooked info
                $modelBooking->customer_id  = $model->id;
                $modelBooking->store_id     = $storeBooked['store_id'];

                //save staffBooked info
                $modelBooking->booking_date = $staffBooked['booking_date'];
                $modelBooking->staff_id     = isset($staffBooked['staff_id']) ? $staffBooked['staff_id']: null;
                if(isset($staffBooked['numberPeople'])){
                    $modelBooking->number_of_people = $staffBooked['numberPeople'];
                }
                $modelBooking->start_time   = $staffBooked['start_time'];
                $modelBooking->end_time     = $staffBooked['end_time'];
                //save
                $productsOption = [];
                foreach ($optionsBooked as $key => $value){
                  $arrKey = explode('_', $key);
                  $productsOption[]=$arrKey[1].'/'.$value.'/'.$arrKey[2];
                }
                $sumPriceProduct = 0 ;

                $result = BookingBusiness::getPriceTable(implode(',', $couponsBooked), implode(',', $productsBooked) ,implode(',', $productsOption) ,$modelBooking->booking_date );
//                Util::d($result);
//              
                //money from point to discount
                $moneyPoint = $modelBooking->moneyFromPoint;

                $modelBooking->booking_price    = $result['total_price'];
                $modelBooking->booking_discount = $moneyPoint;
                $modelBooking->booking_total    = $result['total_price'] - $moneyPoint;
                $modelBooking->save();
                // save coupons to table bookingcoupon
                \common\models\BookingCoupon::updateAll(['del_flg' => '1','updated_at' => strtotime("now")],['booking_id'=> $modelBooking->id]);
                \common\models\BookingProduct::updateAll(['del_flg' => '1','updated_at' => strtotime("now")],['booking_id'=> $modelBooking->id]);
                // save coupons to table bookingcoupon
                foreach ($couponsBooked as $couponId) {
                    $sumPriceProduct  = isset($result['coupons'][$couponId]) ? $result['coupons'][$couponId]['total_price'] : 0;
                    $bookingCoupon    = new \common\models\BookingCoupon();
                    $bookingCoupon->booking_id = $modelBooking->id;
                    $bookingCoupon->coupon_id  = $couponId;
                    $bookingCoupon->quantity   = 1;
                    
                    $bookingCoupon->unit_price = $sumPriceProduct;
                    $bookingCoupon->save();
                }
                //save product to table booking_product

                
                foreach ($productsBooked as $productId) {
                    $sumPriceProduct            = isset($result['products'][$productId]) ? $result['products'][$productId]['total_price'] : 0;
                    $bookingProduct             = new \common\models\BookingProduct();
                    $bookingProduct->booking_id = $modelBooking->id;
                    $bookingProduct->product_id = $productId;
                    $bookingProduct->quantity   = 1;
                  
                    $bookingProduct->unit_price = $sumPriceProduct;
                    $bookingProduct->save();
                }
                //save option to table booking_product
                foreach ($optionsBooked as $optionIdName => $productId) {
                  //$optionId have format option_$idOption_$idProductSelect
                    $optionId                   = explode('_', $optionIdName)[1];
                    $productSelectId            = explode('_', $optionIdName)[2];
                    $keyOption                  = $optionId.'/'.$productId.'/'.$productSelectId;
                    $sumPriceProduct            = isset($result['options'][$keyOption]) ? $result['options'][$keyOption]['total_price'] : 0;
                    $bookingProduct             = new \common\models\BookingProduct();
                    $bookingProduct->booking_id = $modelBooking->id;
                    $bookingProduct->option_id  = $optionId;
                    $bookingProduct->option_product_id  = $productSelectId !=0 ? $productSelectId :null;
                    $bookingProduct->product_id = $productId;
                    $bookingProduct->quantity   = 1;
                  
                    $bookingProduct->unit_price = $sumPriceProduct;
                    $bookingProduct->save();
                }
                
                //update schedule detail 
                if(isset($staffBooked['staff_id'])){
                  
                    //if salon
                    self::saveShiftToScheduleDetail($modelBookingOld->staff_id, $modelBookingOld->booking_date, $modelBookingOld->store_id, null, $modelBookingOld->start_time, $modelBookingOld->end_time, \common\models\StaffScheduleDetail::BOOKING_STATUS_WORKING);
                    self::saveShiftToScheduleDetail($modelBooking->staff_id, $modelBooking->booking_date, $modelBooking->store_id, $modelBooking->booking_code, $modelBooking->start_time, $modelBooking->end_time, \common\models\StaffScheduleDetail::BOOKING_STATUS_BUSY);
                }
                if(isset($staffBooked['seatSuit'])){
                    //if restaurant
                    foreach ((array)$modelBookingOld->listSeat as $indexOld => $itemOld) {
                        BookingSeat::updateAll(['del_flg' => '1','updated_at' => strtotime("now")],['booking_id'=> $modelBooking->id]);
                        self::saveShiftToSeatScheduleDetail($itemOld['seat_id'], $modelBookingOld->booking_date, $modelBookingOld->store_id, $modelBookingOld->booking_code, $modelBookingOld->start_time, $modelBookingOld->end_time, \common\models\StaffScheduleDetail::BOOKING_STATUS_WORKING);
                    }
                    foreach ((array)$staffBooked['seatSuit'] as $index => $item) {
                        $bookingSeat = new BookingSeat();
                        $bookingSeat->booking_id  = $modelBooking->id;
                        $bookingSeat->seat_id     = $item['id'];
                        $bookingSeat->actual_load = $item['actual_load'];
                        $bookingSeat->save();
                        self::saveShiftToSeatScheduleDetail($item['id'], $modelBooking->booking_date, $modelBooking->store_id, $modelBooking->booking_code, $modelBooking->start_time, $modelBooking->end_time, \common\models\StaffScheduleDetail::BOOKING_STATUS_BUSY);
                    }
                    
                }

                $transaction->commit();
               
                $session->setFlash('success', Yii::t("backend", "Save Success"));
                return $this->redirect( ['index', 'date' => date("Y/m/d")]);
//            $modelBooking->start_time
//            $modelBooking->save();
//            return $this->redirect(['index']);
            }

            return $this->render('reservation/update', [
                        'model'             => $model,
                        'modelBooking'      => $modelBooking,
                        'modelCustormerStore' => $modelCustormerStore,
                        'couponsBookedName' => $couponsBookedName,
                        'productBookedName' => $productBookedName,
                        'optionsBookedName' => $optionsBookedName,
                        'staffBookedName'   => $staffBookedName,
                        'storeBooked'       => $storeBooked ,
                        'jsStaffBooking'   => $jsStaffBooking
            ]);
        } catch (Exception $ex) {
            //rollback transaction
            $transaction->rollBack();
            //set error message
            Yii::$app->getSession()->setFlash('error', [
                'error' => Yii::t('backend', "System error happen. Please contact with System manager.")
            ]);
            Yii::error( 'File : '.$ex->getFile().' .  Line : ' .$ex->getLine().' . Message : '.$ex->getMessage());
            //return index
            return $this->redirect(['index']);
        }
    }
    
    /**
     * gen session for update action base on update info (use for pjax).
     *
     * @return mixed
     */
    public function GenSessionUpdate($modelBooking,$model,$modelCustormerStore) {
        $session        = Yii::$app->session;
          
        $couponsBooked  = ArrayHelper::getColumn($modelBooking->bookingCoupon, 'coupon_id');
        $productsBooked = ArrayHelper::getColumn($modelBooking->getBookingProduct()->andWhere(['option_id' => null])->asArray()->all(), 'product_id');
        $optionsBookedModels  = $modelBooking->getBookingProduct()->andWhere(['not', ['option_id' => null]])->all();
        $optionsBooked = [];
        if(count($optionsBookedModels) > 0){
            foreach ($optionsBookedModels as $option){
              
              $option_product_id = $option->option_product_id != null ? $option->option_product_id :0;
              $name = 'option_'.$option->option_id.'_'.$option_product_id;
              $optionsBooked[ $name]  = $option->product_id;
            }
        }
        
        //session when type salon
        if($modelBooking->storeMaster->booking_resources == MasterStore::BOOKING_RESULT_SALON){
            $staffBooked    = [
                                'staff_id'    =>  $modelBooking->staff_id,                          
                                'booking_date'=> $modelBooking->booking_date,
                                'start_time'  => $modelBooking->start_time,
                                'end_time'    => $modelBooking->end_time,
                                'custormerName'=> $model->first_name.$model->last_name,
                                'sumMinute'   => (int)((strtotime($modelBooking->end_time) - strtotime($modelBooking->start_time)) /60),
                            ];
        }
        
        //session when type retaurant
        if($modelBooking->storeMaster->booking_resources == MasterStore::BOOKING_RESULT_RESTAURANCE){
            $seatSuit = [];
            $numPeople = 0;
            foreach ($modelBooking->listSeat as $key => $value) {
                // 'id' => 11 'seat_code' => '001' 'capacity' => 6 'near_max' => 10 'near_level' => 1 'actual_load' => 6
                $seatSuit[] = [
                    'id'          =>  $value['seat_id'],
                    'seat_code'   =>  MasterSeat::findOne($value['seat_id'])->seat_code,
                    'capacity'    =>  MasterSeat::findOne($value['seat_id'])->capacity_max,
                    'actual_load' =>  $value->actual_load,
                    
                ];
                $numPeople += $value->actual_load;
            }
            $staffBooked    = [
                                                      
                                'booking_date'=> $modelBooking->booking_date,
                                'start_time'  => $modelBooking->start_time,
                                'end_time'    => $modelBooking->end_time,
                                'custormerName'=> $model->first_name.$model->last_name,
                                'sumMinute'   => (int)((strtotime($modelBooking->end_time) - strtotime($modelBooking->start_time)) /60),
                                'numberPeople'=> $modelBooking->number_of_people,
                                'seatType'=> $modelBooking->type_seat_id,
                                'seatSuit'    => $seatSuit
                            ];
        }
        
        $storeModel = MasterStore::findOne($modelBooking->store_id);
        $storeBooked  = [
                        'store_id'      => $modelBooking->store_id, 
                        'booking_resources'=> $storeModel->booking_resources,
                        'custormer_id'  => $modelBooking->customer_id,
                        'custormer_name'=>$model->first_name.$model->last_name ,
                        'custormerModel'=> $model,
                        'custormerStoreModel' => $modelCustormerStore,
                        'bookingModel'  =>$modelBooking
                        ];
        
        !$session->has('BookingCoupon') ? $session->set('BookingCoupon',$couponsBooked) : '';
        !$session->has('BookingProduct')? $session->set('BookingProduct',$productsBooked) : '';
        !$session->has('BookingOption') ? $session->set('BookingOption',$optionsBooked) : '';
        !$session->has('BookingStaff')  ? $session->set('BookingStaff',$staffBooked) : '';
        !$session->has('BookingStore')  ? $session->set('BookingStore',$storeBooked) : '';
        
          return true;
    }

    /**
     * get preview price
     * @param integer $id
     * @return mixed
     */
    public function actionGetPreviewSumPrice($point_use =  null) {
        $request        = Yii::$app->request;
        $session        = Yii::$app->session;
        $modelBooking   = new Booking();
        $couponsBooked  = $session->has('BookingCoupon') ? $session->get('BookingCoupon') : [];  //id of coupon selected
        $productsBooked = $session->has('BookingProduct')? $session->get('BookingProduct'): []; //id of product selected
        $optionsBooked  = $session->has('BookingOption') ? $session->get('BookingOption') : []; //id of option selected
        $staffBooked    = $session->get('BookingStaff'); //id of staff and date booking selected
        $storeBooked    = $session->get('BookingStore'); //id of store , coutomer id , coutomer name
        //sum price product on coupon
        $productsOption = [];
        foreach ($optionsBooked as $key => $value){
          $arrKey = explode('_', $key);
          $productsOption[]=$arrKey[1].'/'.$value.'/'.$arrKey[2];
        }
        $sumPriceProduct = 0 ;
      
        $modelBooking->booking_date = $staffBooked['booking_date'];
        $result = BookingBusiness::getPriceTable(implode(',', $couponsBooked), implode(',', $productsBooked) ,implode(',', $productsOption) ,$staffBooked['booking_date'] );
        //money from point to discount
        $modelBooking->point_use = $point_use;
        $moneyPoint     = $modelBooking->moneyFromPoint;

        $modelBooking->booking_price    = $result['total_price'];
        $modelBooking->booking_discount = $moneyPoint;
        $modelBooking->booking_total    = $result['total_price'] - $moneyPoint;

        return Yii::$app->formatter->asDecimal($modelBooking->booking_total ,0 );
    }

    

    /**
     * Updates an existing MasterBooking model in dialog.
     *
     * @param integer $id
     * @return mixed
     */
    public function actionDialogupdate($id) {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->renderAjax('_dialog_update', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing MasterBooking model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {
//        $this->findModel($id)->delete();
        $this->findModel($id)->softDelete();


        return $this->redirect(['index']);
    }

    /**
     * Finds the MasterBooking model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Booking the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = Booking::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Lists all booking models.
     * @return mixed
     */
    public function actionSearchcustomer() {
        $request = Yii::$app->request;
        if ($request->isPost) {
            $params = [
                'first_name'      => $request->post('firstname'),
                'last_name'       => $request->post('lastname'),
                'first_name_kana' => $request->post('firstnamekana'),
                'last_name_kana'  => $request->post('lastnamekana'),
                'mobile'          => $request->post('mobile')
            ];
            $searchModel  = new MasterCustomerSearch($params);
            $dataProvider = $searchModel->searchCustomer($params);

            return $this->renderAjax('reservation/_search_custormer_modal', [
                        'models' => $dataProvider
            ]);
        }
        return false;
    }

    /**
     * render select product and coupon.
     * @return mixed
     */
    public function actionSearchproduct() {
        $request = Yii::$app->request;
        $session = Yii::$app->session;
        if ($request->isAjax) {
            $storeId = $session->get('BookingStore') != null ? $session->get('BookingStore')['store_id'] : null;
//            $listCoupon = \common\models\MasterCoupon::getListCoupon($storeId);
            $customerModel=  new MasterCustomer();
            $customerModel->attributes = $request->get('MasterCustomer');
            $listCoupon = BookingSearch::searchCouponsByStoreId($storeId ,null, $customerModel ,true );
            if(count($listCoupon) > 0){
            $listCoupon   = BookingSearch::searchCouponsByStoreId($storeId ,null, $customerModel ,true )->all();
           
            $listCoupon   = ArrayHelper::index($listCoupon,'id' );
            }
//            Util::dd($listCoupon);
            $listCategory = \common\models\MstCategoriesProduct::listCategory();
            $products = [];
            foreach ($listCategory as $key => $value) {
                $products[$key] = \common\models\MstProduct::find()
                        ->andWhere(['category_id' => $key, 'store_id' => $storeId ,   'assign_fee_flg'=> '0' , 'option_condition_flg' =>'0','show_flg' =>'1'  ])
                        ->asArray()
                        ->all();
            }

//           Util::dd($products);
            return $this->renderAjax('reservation/_search_product_modal', [
                        'listCoupon'   => $listCoupon,
                        'listCategory' => $listCategory,
                        'products'     => $products
            ]);
        }
        return false;
    }

    /**
     * render booking select staff and date view. and save post  to session
     * @return mixed
     */
    public function actionSearchstaffschedule() {
        try{
        $request = Yii::$app->request;
        $session = Yii::$app->session;
        $dateBooking = null;
        //get back date booking from session
        if($request->isPost){
            $dateBooking = Yii::$app->request->post('dateBooking');
        }else if($session->has('BookingStaff') && $session->get('BookingStaff')['booking_date']){
            $dateBooking = $session->get('BookingStaff')['booking_date'];
        }

        if ($session->get('BookingStore') != null) {
            $storeId        = $session->get('BookingStore')['store_id'];
            $custormerName  = $session->get('BookingStore')['custormer_name'];
            $customerSpecify= $session->get('BookingStore')['bookingModel']['customer_specify'];
            
        }else{
          return $this->redirect(Url::previous());
        }
        $store          = MasterStore::findOne($storeId);
        $datasSchedule  = $this->genTableSchedule($dateBooking, $storeId);
        
        $hourList       = Util::genArrayWorkHours($store->time_open, $store->time_close, 30);
        $staffList      = MasterStaff::listStaff($storeId);
//        Util::dd($staffList);
//        \Yii::$app->gon->push('data', $datasSchedule['datas']);
//        \Yii::$app->gon->push('mergeData', $datasSchedule['mergeData']);
//        \Yii::$app->gon->push('dateBooking', $dateBooking);
        
        $data      = json_encode($datasSchedule['datas']);
        $mergeData = json_encode($datasSchedule['mergeData']);

        $couponsBooked  = $session->get('BookingCoupon');  //id of coupon selected
        $productsBooked = $session->get('BookingProduct'); //id of product selected
        $optionsBooked  = $session->get('BookingOption');  //id of option selected
        $staffBooked    = $session->get('BookingStaff');   //id of option selected
        
        $sumMinute = 0;
        if (is_null($dateBooking)) {
            $dateBooking =  isset($staffBooked['booking_date']) ? $staffBooked['booking_date'] :Carbon::today()->format("Y-m-d");
//            $dateBooking = Carbon::today()->format("Y-m-d");
        }
        if (count($couponsBooked) > 0) {
            //sum time product of coupon with benefit content is 02 or 03
            foreach ($couponsBooked as $couponId) {
                $couponModel = MasterCoupon::findOne($couponId);
                if(isset($couponModel) 
                    && ($couponModel->benefits_content == MasterCoupon::BENEFITS_CONTENT_DISCOUNT_SET_PRICE || $couponModel->benefits_content == MasterCoupon::BENEFITS_CONTENT_DISCOUNT_FREE ) )
                    {                   
                        $arrProduct = ArrayHelper::getColumn(\common\models\ProductCoupon::find()
                                        ->where(['coupon_id' => $couponId])
                                        ->all(), 'product_id');
                        $sumMinute += MstProduct::find()->where(['id' => $arrProduct])->sum('time_require');
                    }
                
            }
        }        
        
        //save product to table booking_product
//            product_id can set lai co the null
        //check thoi gian thuc thi
        if (count($productsBooked) > 0) {

            $sumMinute += MstProduct::find()->select('time_require')->where(['id' => $productsBooked])->sum('time_require');
        }
        
        if (count($optionsBooked) > 0) {
            foreach ($optionsBooked as $optionName => $productId){
            $sumMinute += MstProduct::find()->select('time_require')->where(['id' => $productId])->sum('time_require');
            }
        }
        
        // validate if sum minute < 0 redirect back 
        if(!$sumMinute>0){
          
            Yii::$app->getSession()->setFlash('error', [
                'error' => Yii::t('backend', "There is no time required for the selected product. Please select another product.")
            ]);
            
            if($request->post('bookingId') != null){
                return $this->redirect(['update','id'=>$request->post('bookingId')]);
            }else{
             
                return $this->redirect('create');
            }
        }
//        Util::d($session->get('BookingStaff'));
//        Util::d($sumMinute);
//        Util::d($session->get('BookingStore'));
        if ($request->isPost && $request->post('isDecision') == 1 && $request->post('end_time') != null) {
            //handle post form
//          Util::dd($request);
            $session->remove('BookingStaff');
            $bookingStaff = [
                'staff_id'    => $request->post('staff_select'),
                'booking_date'=> $request->post('dateBooking'),
                'start_time'  => $request->post('start_time'),
                'end_time'    => $request->post('end_time'),
                'custormerName'=> $custormerName,
                'sumMinute'   => $sumMinute
            ];
            $session->set('BookingStaff', $bookingStaff);
            //assign_product_id fee to product 
              $modelStaff  = MasterStaff::findOne($bookingStaff['staff_id']); 
            $assignFeeId = ($modelStaff->assign_product_id != null && $customerSpecify == '1') ? [$modelStaff->assign_product_id] : []; 
            $productsBookedTemp = [];
            if(count($productsBooked) > 0){
                foreach ($productsBooked as $productId){
                    $modelTemp = MasterStaff::find()->andWhere(['assign_product_id'=>$productId ])->one();
                    if( empty($modelTemp)  ) $productsBookedTemp[]= $productId  ;
                }
            }
            $session->set('BookingProduct',  array_merge($productsBookedTemp, $assignFeeId));
            if($request->post('bookingId') != null){
                return $this->redirect(['update','id'=>$request->post('bookingId')]);
            }else{
                return $this->redirect('create');
            }
        }

        return $this->render('reservation/_search_staff_schedule', [
                    'date'      => $dateBooking,
                    'data'      => $data,
                    'hourList'  => $hourList,
                    'staffList' => $staffList,
                    'staffBooked'=> $staffBooked,
                    'mergeData' => $mergeData,
                    'custormerName' => $custormerName,
                    'sumMinute' => $sumMinute
        ]);

//        return $this->renderAjax('reservation/_search_staff_schedule');

        } catch (Exception $ex) {
           
            //set error message
            Yii::$app->getSession()->setFlash('error', [
                'error' => Yii::t('backend', "System error happen. Please contact with System manager.")
            ]);
            Yii::error( 'File : '.$ex->getFile().' .  Line : ' .$ex->getLine().' . Message : '.$ex->getMessage());
            //return index
//            return $this->redirect(['index']);
        }
    }
        
    /**
     * render booking select seat and date view. and save post  to session
     * @return mixed
     */
    public function actionSearchseatchedule() {
        try{
        $request = Yii::$app->request;
        $session = Yii::$app->session;
        $dateBooking = null;
        //get back date booking from session
        if($request->isPost){
            $dateBooking = Yii::$app->request->post('dateBooking');
        }else if($session->has('BookingStaff') && $session->get('BookingStaff')['booking_date']){
            $dateBooking = $session->get('BookingStaff')['booking_date'];
        }

        if ($session->get('BookingStore') != null) {
            $storeId        = $session->get('BookingStore')['store_id'];
            $custormerName  = $session->get('BookingStore')['custormer_name'];
            
        }else{
            return $this->redirect(Url::previous());
        }
        $store          = MasterStore::findOne($storeId);
        $seatTypeList   = \common\models\MasterTypeSeat::listTypeSeatByStore($storeId);
        if($request->isPost ){
            $seatType   = Yii::$app->request->post('seat_type_select');
            $noSmoking  = Yii::$app->request->post('no_smoking');
        }else{
            if(empty($seatTypeList)) {
                Yii::$app->getSession()->setFlash('error', [
                  'error' => Yii::t('backend', "This store don't have any seat")
                  ]);
                return $this->redirect(Url::previous());
            }
            //get season update seattype
            if(isset($session->get('BookingStaff')['seatType'])  ){
                $seatType   = $session->get('BookingStaff')['seatType'] ;
            }else{
                $seatType   = array_keys($seatTypeList)[0] ;
            }
            $noSmoking  = null;
        }
//        Util::dd(Yii::$app->request->post());
        $datasSchedule  = $this->genTableSeatSchedule($dateBooking, $storeId, $seatType ,$noSmoking);
        
        $hourList       = Util::genArrayWorkHours($store->time_open, $store->time_close, 30);
        
//        Util::dd($staffList);
//        \Yii::$app->gon->push('data', $datasSchedule['datas']);
//        \Yii::$app->gon->push('mergeData', $datasSchedule['mergeData']);
//        \Yii::$app->gon->push('dateBooking', $dateBooking);
        
        $data      = json_encode($datasSchedule['datas']);
        $mergeData = json_encode($datasSchedule['mergeData']);

        $couponsBooked  = $session->get('BookingCoupon');  //id of coupon selected
        $productsBooked = $session->get('BookingProduct'); //id of product selected
        $optionsBooked  = $session->get('BookingOption');  //id of option selected
        $staffBooked    = $session->get('BookingStaff');   //id of option selected
        
        $sumMinute = 0;
        if (is_null($dateBooking)) {
            $dateBooking =  isset($staffBooked['booking_date']) ? $staffBooked['booking_date'] :Carbon::today()->format("Y-m-d");
//            $dateBooking = Carbon::today()->format("Y-m-d");
        }
        if (count($couponsBooked) > 0) {
            //sum time product of coupon with benefit content is 02 or 03
            foreach ($couponsBooked as $couponId) {
                $couponModel = MasterCoupon::findOne($couponId);
                if(isset($couponModel) 
                    && ($couponModel->benefit_content == MasterCoupon::BENEFITS_CONTENT_DISCOUNT_SET_PRICE || $couponModel->benefit_content == MasterCoupon::BENEFITS_CONTENT_DISCOUNT_FREE ) )
                    {
                        $arrProduct = ArrayHelper::getColumn(\common\models\ProductCoupon::find()
                                        ->where(['coupon_id' => $couponId])
                                        ->all(), 'product_id');
                        $sumMinute += MstProduct::find()->where(['id' => $arrProduct])->sum('time_require');
                    }
                
            }
        }
            
        //save product to table booking_product
//            product_id can set lai co the null
        //check thoi gian thuc thi
        if (count($productsBooked) > 0) {

            $sumMinute += MstProduct::find()->select('time_require')->where(['id' => $productsBooked])->sum('time_require');
        }
        
        if (count($optionsBooked) > 0) {
            foreach ($optionsBooked as $optionName => $productId){
            $sumMinute += MstProduct::find()->select('time_require')->where(['id' => $productId])->sum('time_require');
            }
        }
        
        // validate if sum minute < 0 redirect back 
        if(!$sumMinute>0){
            Yii::$app->getSession()->setFlash('error', [
                'error' => Yii::t('backend', "There is no time required for the selected product. Please select another product.")
                ]);
            if($request->post('bookingId') != null){
                return $this->redirect(['update','id'=>$request->post('bookingId')]);
            }else{
                return $this->redirect('create');
            }
        }
        
        //set number people and seat choose to view
        if($session->has('BookingStaff') && $session->get('BookingStaff')['booking_date'] == $dateBooking){
            $seatBooking  = \GuzzleHttp\json_encode($session->get('BookingStaff')['seatSuit']);
            $numberPeople = isset( $session->get('BookingStaff')['numberPeople'])?$session->get('BookingStaff')['numberPeople']: null;
        }else{
            $seatBooking  = null;
            $numberPeople = null;
        }
//        Util::d($session->get('BookingStaff'));
//        Util::d($sumMinute);
//        Util::d($session->get('BookingStore'));
        //is final decision save to session
        if ($request->isPost && $request->post('isDecision') == 1 && $request->post('end_time') != null) {
            //handle post form
//          Util::dd($request);
            $session->remove('BookingStaff');
            $seatSuit = \GuzzleHttp\json_decode($request->post('seat_suit'),true);
            
            $bookingStaff = [
//                'staff_id'    => '',
                'booking_date'=> $request->post('dateBooking'),
                'start_time'  => $request->post('start_time'),
                'end_time'    => $request->post('end_time'),
                'custormerName'=>$custormerName,
                'sumMinute'   => $sumMinute,
                'numberPeople'=> $request->post('number_people'),
                'seatType'    => $request->post('seat_type_select'),
                'seatSuit'    => $seatSuit
            ];
            $session->set('BookingStaff', $bookingStaff);
            //assign_product_id fee to product 
             
            
            $session->set('BookingProduct',  $productsBooked);
            if($request->post('bookingId') != null){
//              Util::dd($session->get('BookingStaff'));
              $this->redirect(['update','id'=>$request->post('bookingId')]);
            }else{
             $this->redirect('create');
            }
        }

        return $this->render('reservation/_search_seat_schedule', [
                                        'date'         => $dateBooking,
                                        'data'         => $data,
                                        'hourList'     => $hourList,
                                        'seatTypeList' => $seatTypeList,
                                        'storeId'      => $storeId,
                                        'seatType'     => $seatType,
                                        'noSmoking'    => $noSmoking,
                                        'staffBooked'  => $staffBooked,
                                        'mergeData'    => $mergeData,
                                        'custormerName'=> $custormerName,
                                        'sumMinute'    => $sumMinute,
                                        'seatBooking'  => $seatBooking,
                                        'numberPeople' => $numberPeople
                            ]);
        } catch (Exception $ex) {
            
            //set error message
            Yii::$app->getSession()->setFlash('error', [
                'error' => Yii::t('backend', "System error happen. Please contact with System manager.")
            ]);
            Yii::error( 'File : '.$ex->getFile().' .  Line : ' .$ex->getLine().' . Message : '.$ex->getMessage());
            //return index
            return $this->redirect(['index']);
        }

    }

    /**
     * save booking to session
     * @return bool
     */
    public function actionBookingproduct() {
        $request = Yii::$app->request;
        $session = Yii::$app->session;
        //save booking coupon to session BookingCoupon
        if ($request->isPost) {
            $session->set('BookingCoupon', $request->post('BookingCoupon'));
            $session->set('BookingProduct', $request->post('BookingProduct'));


            return true;
        }
        return false;
    }

    /**
     * render modal search option
     * @return bool
     */
    public function actionSearchoption() {
        $request = Yii::$app->request;
        $session = Yii::$app->session;

        if ($request->isAjax) {
            $coupons  = $session->get('BookingCoupon');
            $products = [];
            $products = $session->get('BookingProduct');
            $arrProducts = MasterCoupon::getArrProduct($coupons);

            if(count($products) > 0 ){
//                $arrProductOption = ArrayHelper::getColumn(\common\models\ProductOption::find()->where(['product_id'=>$products])->select('option_id')->all(),'option_id') ;
                $arrProducts        = array_unique(array_merge($arrProducts,$products));
            }
             
            $options = [];
            $productOption = \common\models\ProductOption::find()->andWhere(['product_id'=>$products])->all();
            
            if(count($productOption) > 0){
              
                foreach ($productOption as $product) {
                
                    if(count(\common\models\MasterOption::findOne($product->option_id))>0 && \common\models\MasterOption::findOne($product->option_id)->type == '0'  ){
                        $result =  \common\models\ProductOption::find()->with('product')->select('product_id')
                                                                              ->andWhere(['option_id'=>$product->option_id])
                                                                              ->andWhere(['<>','product_id',$product->product_id])
                                                                            
                                                                              ->asArray()->all();
                        if(count($result) > 0) $options[$product->option_id][$product->product_id] = ArrayHelper::index($result, 'product_id');
                    }else{
                        $result =  \common\models\ProductOption::find()->select('product_id')->with('product')
                                                                              ->andWhere(['option_id'=>$product->option_id])
                                                                              ->andWhere(['not in','product_id',$arrProducts])
                                                                              
                                                                              ->asArray()->all();
                        if(count($result) > 0) $options[$product->option_id][0] =  ArrayHelper::index($result,'product_id');
                    }
 
                }
                 
            }

//Util::dd($options);

            return $this->renderAjax('reservation/_search_option_modal', [
                        'options' => $options,
            ]);

            return true;
        }
        return false;
    }
    

    /**
     * set option to session
     * @return bool
     */
    public function actionBookingoption() {
        $request = Yii::$app->request;
        $session = Yii::$app->session;
        //save booking coupon to session BookingCoupon
        if ($request->isPost) {
            $option = $request->post();
            unset($option['_csrf-backend']);
            $productId = [];
            if (!empty($option)) {
              //name of option have format option_$idOption_$idProductSelect
              //if $idProductSelect == 0 mean it's option summary
                foreach ($option as $key => $value) {
                    if($value != -1 ) $productId[ $key] = $value ;
                }
            }
//          Util::d($productId);
            $session->set('BookingOption', $productId);
//            Util::d($session->get('BookingOption'));

            return true;
        }
        return false;
    }

    /**
     * set store_id and custormer to session for booking
     * @return bool
     */
    public function actionSetSessionBooking() {
        $request = Yii::$app->request;
        $session = Yii::$app->session;
        //save booking coupon to session BookingCoupon

        if ($request->isPost) {
            $custormerId = $custormerName = null;

            $custormerModel = new MasterCustomer();
            $custormerModel->load($request->post());
            $custormerStoreModel = new \common\models\CustomerStore();
            $bookingModel   = new Booking();
            if ($custormerModel->id != null) {
                $custormerId    = $custormerModel->id;
                $custormerName  = MasterCustomer::findOne($custormerId)->first_name . MasterCustomer::findOne($custormerId)->last_name;
            }
            //remove assign free in product booked array sesion when uncheck customer_specify 
            if ($request->post('customer_specify') == "false") {
                
                if( $session->has('BookingStaff')&& $session->has('BookingProduct')  && isset($session->get('BookingStaff')['staff_id']) ){
                    $BookingProduct =   $session->get('BookingProduct');
                    $staffModel = MasterStaff::findOne($session->get('BookingStaff')['staff_id']);
                    $assign_product_id= isset($staffModel)? $staffModel->assign_product_id : '';
                    if (($key = array_search($assign_product_id, $BookingProduct)) !== false) {
                        unset($BookingProduct[$key]);
                    }
                    $session->set('BookingProduct',$BookingProduct);
                  
                }
  
            }  
            //add assign free in product booked array sesion when check customer_specify
            if ($request->post('customer_specify') == "true") {
                if( $session->has('BookingStaff')&& $session->has('BookingProduct')  && isset($session->get('BookingStaff')['staff_id']) ){
                    $BookingProduct =   $session->get('BookingProduct');
                    $staffModel = MasterStaff::findOne($session->get('BookingStaff')['staff_id']);
                    $assign_product_id= isset($staffModel)? $staffModel->assign_product_id : '';
                    
                    if($assign_product_id != null && !in_array($assign_product_id, $BookingProduct)) $BookingProduct[]= $assign_product_id;
                    $session->set('BookingProduct',$BookingProduct);
                  
                }
            } 
            if ($request->post('change_store')=== "true") {
              
                $session->remove('BookingCoupon');
                $session->remove('BookingProduct');
                $session->remove('BookingOption');
                $session->remove('BookingStaff');
               
            }
            $storeModel = MasterStore::findOne($request->post('store_id'));
            $custormerStoreModel->load($request->post());
            $bookingModel->load($request->post());
            $arr = [
                'store_id'        => $request->post('store_id'),
                'booking_resources'=> $storeModel->booking_resources,
                'custormer_id'    => $request->post('custormer_id'),
                'custormer_name'  => $custormerName,
                'custormerModel'  => $custormerModel->toArray(),
                'custormerStoreModel' => $custormerStoreModel->toArray(),
                'bookingModel'    => $bookingModel->toArray(),
            ];

            $session->set('BookingStore', $arr);

//            Util::d($session->get('BookingStore'));

            return true;
        }
        return false;
    }
    
    
    
    public function actionAccountingManagement(){
        $request = Yii::$app->request;
        //init        
        $result = array(
            'pjax_first'        => array(
                'customer'      => array(),
                'customerStore' => array(),
                'store'         => array(),
                ),
            'categories'    => array(),
            'ticket'        => array(),
            'grant_yen_per_point' => 1,
            'point_conversion_ratio' => 1,
            'round_down_flg' => '1',
            'round_process_flg' => '1',
        );
        try{
            $companyId = Util::getCookiesCompanyId();
            
            $pointSetting = PointSetting::find()->where(['company_id'=>$companyId])->andWhere(['del_flg'=> '0'])->one();
            if(empty($pointSetting)){
                throw new Exception(Yii::t('backend','Point setting is empty, please setup point setting'));
            }else{
                $result['grant_yen_per_point'] = $pointSetting['grant_yen_per_point'];
                $result['point_conversion_ratio'] = (float)$pointSetting['point_conversion_yen']/$pointSetting['point_conversion_p'];
            }
            
            $storeId = null;
            
            if($request->isPost){
                if($request->post('pjax_first') === '1'){
                    $result['pjax_first'] = $this->searchCustomerInfo();
                }

                if($request->post('pjax_product') === '1'){
                    $storeId = $request->post('store_id');
                }
            }

            if($storeId !== null){
                $store = MasterStore::find()->where(['id'=>$storeId])->andWhere(['del_flg'=>'0'])->one();
                if(!empty($store)){
                    $categories = MstCategoriesProduct::findCategory($storeId);
                    $ticket = MasterTicket::getAllTicketByStoreId($storeId);
                    $result['categories'] = $categories;
                    $result['ticket'] = $ticket;
                    $result['round_down_flg'] = $store['round_down_flg'];
                    $result['round_process_flg'] = $store['round_process_flg'];
                    $result['store_id'] = $storeId;
                }                
            }

            return $this->render('accounting-management', ['data'=>$result]);
            
        }catch(Exception $ex){
            Yii::$app->getSession()->setFlash('error', [
                'error' => Yii::t('backend', $ex->getMessage())
            ]);
            $this->goHome();
        }
    }
    
    public function actionOrderCheckout(){
        $request = Yii::$app->request;
        
        if($request->isGet){
            $this->goHome();
        }
        if($request->isPost){
            $moneyCredit = 0;
            $moneyTicket = 0;
            $moneyOther = 0;
            
            $return_good_status = $request->post('return_good_status');
            $yinyang = $return_good_status != '1' ? 1 : -1;
            
            $bookingId = $request->post('bookingId');
            $listProduct = $request->post('listProduct');
            $storeId = $request->post('storeId');
            $staffId = $request->post('staffId');
            $rejiStaffId = $request->post('rejiStaffId');
            $totalTax = $request->post('totalTax');
            $totalInnerTax = $request->post('totalInnerTax');
            $totalOutterTax = $request->post('totalOutterTax');
            $subTotal = $request->post('subTotal');
            $endMoney = $request->post('endMoney');
            $afterReduceMoney = $request->post('afterReduceMoney');
            $realCash = $request->post('realCash');
            $refundMoney = $request->post('refundMoney');
            $adjustmentMoney = $request->post('adjustmentMoney');
            $moneyCheckoutType = $request->post('moneyCheckoutType');
            $customerId = $request->post('customerId');
            $totalDetailMoney = $request->post('totalDetailMoney');
            $totalCheckoutMoney = $request->post('totalCheckoutMoney');
            
            $moneyDiscountYen = $request->post('money_discount_yen');
            $moneyDiscountPercent = $request->post('money_discount_percent');
            
            $pointUse = $request->post('point_use');
            $pointReduce = $request->post('point_reduce');
            $pointIncrease = $request->post('point_increase');
            $pointAllocation = $request->post('point_allocation');
            $moneyFromPoint = $request->post('money_conversion_from_point');
            $totalPoint = $pointAllocation + $pointIncrease - $pointReduce - $pointUse;
            
            $chargeAdd = $request->post('charge_add');
            $chargeUse = $request->post('charge_use');
            $chargeAddMoney = $request->post('charge_add_money');
            $chargeBalance = $chargeAdd - $chargeUse;
            
            $stringIdProduct = '';
            $stringIdCoupon = '';
            
            $status = '1';
            $message = '';
            
            foreach($moneyCheckoutType as $type){
                if($type['type']=='product')
                    $moneyTicket = $type['money'];
                if($type['type']=='credit_card')
                    $moneyCredit = $type['money'];
                if($type['type']=='other')
                    $moneyOther = $type['money'];
            }
            
            try{
                $transaction = Yii::$app->db->beginTransaction(); 
                if($bookingId !== ''){
                    $booking = Booking::find()->where(['id'=>$bookingId])->andWhere(['del_flg'=> '0'])->one();
                    if(!empty($booking)){
                        if($booking['status'] == Booking::BOOKING_STATUS_FINISH){
                            throw new Exception(Yii::t('backend','Booking has Completed'));
                        }
                    }else{
                        throw new Exception(Yii::t('backend','Cannot find Booking'));
                    }
                }
                
                $companyId = Util::getCookiesCompanyId();
                $customer = $customerId != '' ? MasterCustomer::find()->where(['id'=> $customerId])->one() : array();
                $customerStore = array();
                $store = MasterStore::find()->where(['id'=>$storeId])->one();
                
                if(!empty($store)){
                    $customerJancode = '';
                    
                    $rejiStaff = $rejiStaffId !== '' ? MasterStaff::find()->where(['id'=>$rejiStaffId])->one() : array();
                    if(empty($rejiStaff)){
                        throw new Exception(Yii::t('backend','Cannot find reji Staff'));
                    }

                    $staffId = $staffId !== '' ? MasterStaff::find()->where(['id'=>$staffId])->one() : array();
                    if(empty($staffId)){
                        throw new Exception(Yii::t('backend','Cannot find Kanri Staff'));
                    }
                    
                    if($store['pos_use'] == '1' && $store['temporary_stop'] == '1'){
                        $customerStoreModel = CustomerStoreTmp::className();
                        $orderModel = MstOrderTmp::className();
                        $orderDetailModel = MstOrderDetailTmp::className();
                        $ticketHistoryModel = TicketHistoryTmp::className();
                        $payCouponModel = PayCouponTmp::className();
                        $pointHistoryModel = PointHistoryTmp::className();
                        $chargeHistoryModel = ChargeHistoryTmp::className();
                        
                    }else if($store['pos_use'] == '0'){
                        $customerStoreModel = CustomerStore::className();
                        $orderModel = MstOrder::className();
                        $orderDetailModel = MstOrderDetail::className();
                        $ticketHistoryModel = TicketHistory::className();
                        $payCouponModel = PayCoupon::className();
                        $pointHistoryModel = PointHistory::className();
                        $chargeHistoryModel = ChargeHistory::className();
                    }
                    
                    if(!($store['pos_use'] == '1' && $store['temporary_stop'] == '0')){
                        if(!empty($customer)){
                            $customerJancode = $customer['customer_jan_code'];
                            $customerStore = $customerStoreModel::find()->where(['customer_id'=>$customer['id']])->andWhere(['store_id'=> $storeId])->one();
                        }
                                                
                        //var_dump($listProduct);
                        $totalMoneyOrder = BookingBusiness::getTotalMoneyOrder($listProduct, $adjustmentMoney, $totalOutterTax, $moneyFromPoint, $chargeUse, $chargeAddMoney);
                        if($totalMoneyOrder > $endMoney + 2 || $totalMoneyOrder < $endMoney - 2){
                            throw new Exception(Yii::t('backend','Cannot save Order'));
                        }
                        
                        if($return_good_status == '0'){
                            if($refundMoney > 10000){
                                throw new Exception(Yii::t('backend','Refund Money is large than 10000'));
                            }
                            
                            if(($totalCheckoutMoney + $realCash) < $endMoney){
                                throw new Exception(Yii::t('backend','Cannot save Order'));
                            }
                        }
                        
                        $order = new $orderModel;

                        $order->order_code = $orderModel::genOrderCode($storeId);
                        $order->order_type = $return_good_status != '1' ? '0' : '2';
                        $order->process_date = date('Y-m-d');
                        $order->process_time = date('H:i');
                        $order->company_id = $companyId;
                        $order->customer_jan_code = $customerJancode;
                        $order->booking_id = $bookingId !== '' ? $bookingId : 0;
                        $order->reji_management_id = $rejiStaff->managementLogin['management_id'];
                        $order->order_management_id = $staffId->managementLogin['management_id'];
                        $order->point_use = $pointUse;
                        $order->point_use_money = $moneyFromPoint;
                        $order->charge_use = $chargeUse;
                        $order->charge_add = $chargeAdd;
                        $order->charge_add_money = $chargeAddMoney;
                        $order->charge_balance = empty($customerStore) ? 0 : $customerStore['charge_balance'];
                        $order->discount_percent = $moneyDiscountPercent;
                        $order->discount_value = $moneyDiscountYen;
                        $order->discount_coupon = 0;
                        $order->detail_money = (int)$totalDetailMoney*$yinyang;
                        $order->subtotal = (int)$subTotal*$yinyang;
                        $order->total = (int)$endMoney*$yinyang;
                        $order->product_include_tax = (int)$totalInnerTax*$yinyang;
                        $order->product_exclude_tax = (int)$totalOutterTax*$yinyang;
                        $order->tax = (int)$totalTax*$yinyang;
                        $order->point_add = (int)$pointAllocation*$yinyang;
                        $order->point_increase = $pointIncrease;
                        $order->point_decrease = $pointReduce;
                        $order->point_balance = empty($customerStore) ? 0 : $customerStore['total_point'];
                        $order->money_credit = $moneyCredit;
                        $order->money_ticket = $moneyTicket;
                        $order->money_other = $moneyOther;
                        $order->money_receive = $realCash;
                        $order->money_cash = (int)($endMoney - $moneyCredit - $moneyTicket -$moneyOther)*$yinyang; 
                        $order->money_refund = $refundMoney;
                        $order->money_adjust = $adjustmentMoney;
                        $order->request_reciept_ouput_times = 0;
                        $order->lotery = $return_good_status != '1' ? str_pad(rand(0,999999999), 10, '0', STR_PAD_LEFT) : '' ;
                        $order->weather = 0;
                        $order->coupond_output_times = 0;
                        
                        if(!$order->save()){
                            throw new Exception(Yii::t('backend','Cannot save Order'));
                        }
                        
                        foreach($listProduct as $product){
                            $firstJancode = substr($product['jancode'],0,2);

                            if($firstJancode == '21' 
                                    || ($firstJancode == '22' && ($product['benefits_content'] == '02' || $product['benefits_content'] == '03')) 
                                    || ($firstJancode == '23' && ($product['status_ticket'] == 'buy'))){
                                
                                $managementloginId = '';
                                $productStaff = $product['staff_product'] != '' ? MasterStaff::find()->where(['id'=> $product['staff_product']])->one() : $rejiStaff;
                                if(!empty($productStaff)){
                                    $managementloginId = $productStaff->managementLogin['management_id'];
                                }
                                
                                $order_detail = new $orderDetailModel;

                                $order_detail->company_id = $companyId;
                                $order_detail->order_code = $order->order_code;
                                $order_detail->product_management_id = $managementloginId;
                                $order_detail->jan_code = $product['jancode'];
                                $order_detail->product_display_name = $product['display_name'];
                                $order_detail->quantity = $product['quantity'];
                                $order_detail->price = $product['unit_price'];
                                $order_detail->price_down_type = '0';
                                $order_detail->price_down_value = 0;
                                $order_detail->product_sub_total = (int)$product['normal_price']*$yinyang;
                                $order_detail->product_total = (int)$product['last_price']*$yinyang;
                                $order_detail->product_tax = $product['tax_rate'];
                                $order_detail->product_tax_money = (int)$product['tax_money']*$yinyang;
                                $order_detail->tax_display_method = $product['tax_method'];
                                $order_detail->gift_flg = isset($product['gift_product'])? $product['gift_product'] : '0';
                                if(isset($product['price_down_type']) && isset($product['price_down_value'])){
                                    if($product['price_down_type'] !== '' && $product['price_down_value'] !== ''){

                                        $order_detail->price_down_type = $product['price_down_type'] == '0' ? '1':'2';
                                        $order_detail->price_down_value = $product['price_down_value'];
                                    }
                                }

                                if(!$order_detail->save()){
                                    throw new Exception(Yii::t('backend','Cannot add order detail'));
                                }                            
                            }

                            //add TicketHistory
                            if(!empty($customer)){
                                if($firstJancode == '23'){
                                    $processType = $product['status_ticket'] == 'buy' ? '0' : '1';
                                    
                                    if($return_good_status == '1'){
                                        $processType = '1';
                                    }
                                    $ticket_balance = 0;
                                    $getTicketBalance = $ticketHistoryModel::getTicketBalance($product['jancode'], $customerJancode);

                                    if(!empty($getTicketBalance)){
                                        $ticket_balance = $getTicketBalance['ticket_balance'];
                                    }
                                    $ticket_history = new $ticketHistoryModel;
                                    $ticket_history->store_id = (int)$storeId;
                                    $ticket_history->order_code = $order->order_code;
                                    $ticket_history->ticket_jan_code = $product['jancode'];
                                    $ticket_history->ticket_balance = $processType == '0'? (int)$ticket_balance + (int)$product['set_ticket'] : (int)$ticket_balance - (int)$product['set_ticket'];
                                    $ticket_history->process_number = (int)$product['set_ticket']*$yinyang;
                                    $ticket_history->process_type = $processType;
                                    if(!$ticket_history->save()){
                                        throw new Exception(Yii::t('backend','Cannot add ticket history').$ticket_history->errors);
                                    } 
                                }
                            }

                            if($firstJancode == '22' && ($product['benefits_content'] == '00' || $product['benefits_content'] == '01')){
                                $priceDownType = $product['benefits_content'] == '00' ? '2': '1';

                                $pay_coupon = new $payCouponModel();
                                $pay_coupon->store_id = $storeId;
                                $pay_coupon->order_code = $order->order_code;
                                $pay_coupon->coupon_jan_code = $product['jancode'];
                                $pay_coupon->quantity = $product['quantity'];
                                $pay_coupon->price_down_type = $priceDownType;
                                $pay_coupon->price_down_value = (int)$product['discount_coupon']*$yinyang;
                                $pay_coupon->price_down_money = (int)$product['last_price']*$yinyang;
                                if(!$pay_coupon->save()){
                                    throw new Exception(Yii::t('backend','Cannot add pay coupon'));
                                }
                            }
                        }

                        if(!empty($customer)){

                            //addPointHistory                                                                
                            for($i = 0; $i <= 3; $i++){
                                $pointHistory = new $pointHistoryModel;
                                $pointHistory->customer_id	= $customer['id'];
                                $pointHistory->company_id	= $companyId;
                                $pointHistory->store_code	= $store['store_code'];
                                $pointHistory->staff_management_id	= $rejiStaff->managementLogin['management_id'];
                                $pointHistory->process_date	= date('Y-m-d');
                                $pointHistory->process_time	= date('H:i');

                                switch ($i){
                                    case 0: 
                                        $pointHistory->process_type	= '0';
                                        $pointHistory->process_point = (int)$pointAllocation*$yinyang;
                                        break;
                                    case 1: 
                                        $pointHistory->process_type	= '1';
                                        $pointHistory->process_point = $pointIncrease;
                                        break;
                                    case 2: 
                                        $pointHistory->process_type	= '2';
                                        $pointHistory->process_point = $pointUse;
                                        break;
                                    case 3: 
                                        $pointHistory->process_type	= '3';
                                        $pointHistory->process_point = $pointReduce;
                                        break;
                                }
                                if( ($i == 0 && $pointAllocation > 0) || ($i == 1 && $pointIncrease > 0) || ($i == 2 && $pointUse > 0) || ($i == 3 && $pointReduce > 0)){
                                    if(!$pointHistory->save()){
                                        throw new Exception(Yii::t('backend','Cannot add point history').$pointHistory->errors);
                                    }
                                }                                    
                            }

                            //add Charge History
                            $currentChargeBalance = empty($customerStore) ? 0 : $customerStore['charge_balance'];
                            for($i = 0; $i <= 1; $i++){

                                $chargeHistoy = new $chargeHistoryModel();
                                $chargeHistoy->customer_id	= $customer['id'];
                                $chargeHistoy->company_id	= $companyId;
                                $chargeHistoy->store_code	= $store['store_code'];
                                $chargeHistoy->staff_management_id	= $rejiStaff->managementLogin['management_id'];
                                $chargeHistoy->process_date	= date('Y-m-d');
                                $chargeHistoy->process_time	= date('H:i');

                                switch ($i){
                                    case 0:
                                        $currentChargeBalance = $currentChargeBalance + $chargeAdd;
                                        $chargeHistoy->process_type = '0';
                                        $chargeHistoy->input_money = $chargeAddMoney;
                                        $chargeHistoy->process_money = $chargeAdd;
                                        $chargeHistoy->charge_balance = $currentChargeBalance;
                                        break;
                                    case 1:
                                        $currentChargeBalance = $currentChargeBalance - $chargeUse;
                                        $chargeHistoy->process_type = '1';
                                        $chargeHistoy->input_money = 0;
                                        $chargeHistoy->process_money = $chargeUse;
                                        $chargeHistoy->charge_balance = $currentChargeBalance;
                                        break;
                                        }
                                
                                if(($i == 0 && (int)$chargeAdd > 0) || ($i == 1 && (int)$chargeUse > 0)){
                                    if(!$chargeHistoy->save()){

                                        $message = '';
                                        if($chargeHistoy->errors['charge_balance'] != ''){
                                            $message = $chargeHistoy->errors['charge_balance'][0].$currentChargeBalance;
                                        }
                                        throw new Exception(Yii::t('backend','Cannot add charge history: '.$message));
                                    }
                                }                                    	
                            }

                            $balanceTicket = $ticketHistoryModel::getBalanceTicketForUpdateCustomerStore($customer['id'], $store['store_code']);
                            
                            if((float)$totalPoint > 1000000000){
                                throw new Exception(Yii::t('backend','The total point of the customer exceeds 1,000,000,000 point'));
                            }
                            if(empty($customerStore)){

                                $customerStore = new $customerStoreModel();
                                
                                $customerStore->customer_id = $customer['id'];
                                $customerStore->store_id = $storeId;
                                $customerStore->total_point	= $totalPoint;
                                $customerStore->total_amount = $order->total;
                                $customerStore->rank_id = '00';
                                $customerStore->number_visit = 1;
                                $customerStore->number_booking = 0;
                                $customerStore->last_visit_date = date('Y-m-d');
                                $customerStore->first_visit_date = date('Y-m-d');
                                $customerStore->tickets_balance = $balanceTicket;
                                $customerStore->charge_balance = 0;
                                $customerStore->news_transmision_flg = '0';
                                $customerStore->black_list_flg = '0';
                                $customerStore->register_kbn = '1';
                                $customerStore->update_kbn = '1';

                            }else{

                                $customerStore->total_point	= $customerStore->total_point + $totalPoint*$yinyang;
                                $customerStore->total_amount = $customerStore->total_amount + $order->total*$yinyang;
                                $customerStore->number_visit = $customerStore->number_visit + 1;
                                $customerStore->last_visit_date = date('Y-m-d');
                                $customerStore->tickets_balance = $balanceTicket;
                                $customerStore->charge_balance = $customerStore->charge_balance + $chargeBalance;
                                $customerStore->news_transmision_flg = '0';
                                $customerStore->black_list_flg = '0';
                                $customerStore->register_kbn = '1';
                                $customerStore->update_kbn = '1';
                            }

                            if(!$customerStore->save()){
                                throw new Exception(Yii::t('backend','Cannot Save customer Store').$customerStore->errors);
                            }
                        }
                        if($store['pos_use'] == '0' && $bookingId !== ''){
                            $booking = Booking::find()->where(['id'=> $bookingId])->one();
                            if(!empty($booking)){
                                $booking->status = Booking::BOOKING_STATUS_FINISH;
                                if(!$booking->save()){
                                    throw new Exception(Yii::t('backend','Cannot Save Booking'));
                                }
                            }
                        }

                        $transaction->commit();
                        $status = '1';
                        $message = Yii::t('backend','Accounting Successfully');
                    }else{
                        throw new Exception(Yii::t('backend','Store is accounting by POS'));
                    }
                    //end transaction
                }
                
            } catch (\Exception $ex) {
                $transaction->rollBack();
                $status = '0';
                $message = $ex->getMessage();
            }
            $result = array('status'=> $status, 'message'=> $message);
            return \GuzzleHttp\json_encode(array('result'=>$result));
        }        
        
    }
    
    public function actionSearchlistcustomer() {
        $request = Yii::$app->request;
        if ($request->isPost) {
            $params = [
                'first_name'      => $request->post('firstname'),
                'last_name'       => $request->post('lastname'),
                'first_name_kana' => $request->post('firstnamekana'),
                'last_name_kana'  => $request->post('lastnamekana'),
                'mobile'          => $request->post('mobile')
            ];
            $searchModel  = new MasterCustomerSearch($params);
            $dataProvider = $searchModel->searchCustomer($params);

            return $this->renderAjax('reservation/_search_list_customer_modal', [
                        'models' => $dataProvider
            ]);
        }
        return false;
    }
    
    public function searchCustomerInfo(){
        $request = Yii::$app->request;
        
        if($request->isPost){
            $custormerModel = new MasterCustomer();            
            
            
            $customer = MasterCustomer::getCustomerDetail($request->post('custormer_id'));
            $store = MasterStore::find()->where(['id'=>$request->post('store_id')])->one();

            if($store['pos_use'] == '1' && $store['temporary_stop'] == '1'){
                $custormerStoreModel = new \common\models\CustomerStoreTmp();
                $customerStore = $custormerStoreModel->getCustomerStoreDetail($request->post('custormer_id'), $request->post('store_id'));
            }else{
                $custormerStoreModel = new \common\models\CustomerStore();
                $customerStore = $custormerStoreModel->getCustomerStoreDetail($request->post('custormer_id'), $request->post('store_id'));
            }
                       
            $arr = [
                'customer'  => $customer,
                'customerStore' => $customerStore,
            ];
            
            return $arr;
        
        }
    }
    
    public function actionSearchbyjancode(){
        $request = Yii::$app->request;
        
        if($request->isPost){
            $janCode = $request->post('jancode');
            $storeId = $request->post('store_id');
            $firstCharacter = substr($janCode, 0, 2);
            //var_dump($janCode);var_dump($storeId);
            $result = array(); $arrayProduct = array();
            switch ($firstCharacter){
                case '21':
                    $result = MstProduct::getProductByJancode($janCode, $storeId);
                    break;
                case '22':
                    $result = MasterCoupon::getCouponByJancode($janCode, $storeId);
                    
                    if(!empty($result)){
                        $couponProducts = MasterCoupon::find()->where(['id'=>$result['id']])->one()->productsCoupon;
                        foreach($couponProducts as $couponProduct){
                            array_push($arrayProduct, $couponProduct->product_id);
                        }
                    }
                        
                    break;
                case '23':
                    $result = MasterTicket::getTicketByJancode($janCode, $storeId);
                    break;
                default: break;
            }
            return \GuzzleHttp\json_encode(array('result'=>$result, 'coupon_product'=>$arrayProduct));
        }
    }
    
    public function actionGettaxproduct(){
        $request = Yii::$app->request;
        if($request->isPost){
            $taxRateId = $request->post('tax-rate-id');
            $taxRate = 0;
            if($taxRateId != ''){                
                $taxRate = Util::getTaxProduct($taxRateId);
            }
            return \GuzzleHttp\json_encode(array('tax'=> array('tax-id'=>$taxRateId, 'rate'=>$taxRate )));
        }
    }
    
    public function actionGettaxcoupon(){
        $request = Yii::$app->request;
        if($request->isPost){
            $taxRateId = '';
            $productId = $request->post('product_id');
            $product = MstProduct::find()->where(['id'=>$productId])->one();
            if(!empty($product)){
                $taxRateId = $product['tax_rate_id'];
            }
            $taxRate = 0;
            if($taxRateId != ''){
                $taxDetail = \common\models\TaxDetail::find()
                    ->where(['tax_id' => $taxRateId])
                    ->andWhere(['<=','start_date', date('Y-m-d')])
                    ->andWhere(['>=','end_date', date('Y-m-d')])
                    ->one();
            
                if(!empty($taxDetail)){
                    $taxRate = $taxDetail['rate'];
                }                
            }
            return \GuzzleHttp\json_encode(array('tax'=> array('tax-id'=>$taxRateId, 'rate'=>$taxRate )));
        }
    }
    
    public function actionPointSetting() {
        $request = Yii::$app->request;
        if ($request->isPost) {
            $params = [
                'customer_id'      => $request->post('customer_id'),
                'store_id'      => $request->post('store_id'),
            ];
            
            $store = MasterStore::find()->where(['id'=>$request->post('store_id')])->one();
            if($store['pos_use'] == '1' && $store['temporary_stop'] == '1'){
                $customerStoreModel = CustomerStoreTmp::className();
            }else{
                $customerStoreModel = CustomerStore::className();
            }
            $customer_store = $customerStoreModel::findOne(['customer_id' => $params['customer_id'], 'store_id' => $params['store_id']]);
            
            return $this->renderAjax('POS_page/_point_detail',[
                    'total_point' => empty($customer_store)?0:$customer_store->total_point,
                ]);
        }
        return false;
    }
    
    public function actionChargeSetting() {
        $request = Yii::$app->request;
        if ($request->isPost) {
            $params = [
                'customer_id'      => $request->post('customer_id'),
                'store_id'      => $request->post('store_id'),
            ];
            
            $store = MasterStore::find()->where(['id'=>$request->post('store_id')])->one();
            if($store['pos_use'] == '1' && $store['temporary_stop'] == '1'){
                $customerStoreModel = CustomerStoreTmp::className();
            }else{
                $customerStoreModel = CustomerStore::className();
            }
            
            $charge = $customerStoreModel::find()->andWhere(['customer_id' => $params['customer_id'], 'store_id' => $params['store_id']])->orderBy('id DESC')->one();
            
            return $this->renderAjax('POS_page/_charge_detail',[
                    'charge' => $charge,
                ]);
        }
        return false;
    }
    
    public function actionTicketSetting() {
        $request = Yii::$app->request;
        if ($request->isPost) {
            $params = [
                'customer_id'      => $request->post('customer_id'),
                'store_id'      => $request->post('store_id'),
            ];
            
            $store = MasterStore::find()->where(['id'=>$request->post('store_id')])->one();
            if($store['pos_use'] == '1' && $store['temporary_stop'] == '1'){
                $ticketHistoryModel = TicketHistoryTmp::className();
            }else{
                $ticketHistoryModel = TicketHistory::className();
            }
            
            $list_ticket = $ticketHistoryModel::getTicketOfCustomerInStore($params['customer_id'], $params['store_id']);
            return $this->renderAjax('POS_page/_ticket_detail',[
                    'list_ticket' => $list_ticket,
                ]);
        }
        return false;
    }
    
    public function actionProductSetting() {
        $request = Yii::$app->request;
        if ($request->isPost) {
            $params = [
                'jan_code'      => $request->post('jan_code'),
                'quantity'      => $request->post('quantity'),
                'discount'      => $request->post('discount'),
                'unit_price'    => $request->post('unit_price'),
                'product_reduction' =>  $request->post('product_reduction'),
                'product_discount_status'   =>  $request->post('product_discount_status'),
                'gift_product'   =>  $request->post('gift_product')
            ];
            if(empty($params['discount'])){
                $product = MstProduct::findOne(['jan_code' => $params['jan_code']]);
                
                return $this->renderAjax('POS_page/_product_detail',[
                    'product' => $product,
                    'product_jan_code' => $params['jan_code'],
                    'quantity'  =>  $params['quantity'],
                    'unit_price'  =>  $params['unit_price'],
                    'product_reduction'  =>  $params['product_reduction'],
                    'product_discount_status'  =>  $params['product_discount_status'],
                    'gift_product'  =>  $params['gift_product'],
                ]);
            }else{
                return $this->renderAjax('POS_page/_product_detail_discount',[
                    'discount' => $params['discount'],
                ]);
            }
        }
        return false;
    }
    
    /**
     * 予約受付一括設定 5.8 / get Staff Schedule
     * @param int $month 
     * @param int $year 
     * @param int $storeId
     * @return array
     */
    public function getStaffSchedule($month , $year  , $staff ) {
        $datas = [];

        $days = $this->datesMonth($month, $year);

        foreach ($days as $day) {
            $model = \common\models\StaffSchedule::find()
                                ->where(['staff_id' => $staff])
                                ->andWhere(['schedule_date' => $day])
                                ->one();

            if (empty($model)) {
                $item = [
                    "start"       => $day,
                    "name"        => $day,
                    "workFlg"     => 1
                ];
            } else {
                $item = [
                    "start"       => $day,
                    "name"        => $day,
                    "workFlg"     => (int) $model->work_flg
                ];
            }
            
            $datas[] = $item;
        }

        return $datas;
    }
    
    /**
     * 予約受付一括設定 5.8/ Bulk booking reception setting
     * @return render
     */
    public function actionBulkReceptionSetting(){
        if(Yii::$app->request->isPost){
            // Get year and month from get parameter
            $year = Yii::$app->request->post('year');
            $month = Yii::$app->request->post('month');
            $store_id = Yii::$app->request->post('store_id');

            // Check if valid request data
            if(!$store_id || !$year || !$month){
                throw new \yii\web\HttpException(404, 'The requested Item could not be found.');
            }

            // Get and check if is exists this store
            if($store = MasterStore::findOne($store_id)){
                $hours = Util::genArrayWorkHours($store->time_open, $store->time_close, 30);
                $data = $this->genScheduleMonth($month, $year , $store_id);
                $this->layout = false;
                return $this->render('_dialog_bulk_reception_setting', compact('year','month', 'store', 'hours', 'data'));
            }
        }
        // Return if Is not exists this store
        throw new \yii\web\HttpException(404, 'The requested Item could not be found.');
    }
    
    /**
     * スタッフ予約受付設定から呼び出した時 5.8/ Bulk staff booking setting
     * @return render
     */
    public function actionBulkStaffBookingSetting(){
        // Check request is post method
        if(Yii::$app->request->isPost){
            // Get parameters from request
            $year     = Yii::$app->request->post('year');
            $month    = Yii::$app->request->post('month');
            $store_id = Yii::$app->request->post('store_id');
            $staff_id = Yii::$app->request->post('staff_id');

            // Check if valid request data
            if(!$store_id || !$year || !$month || !$staff_id){
                throw new \yii\web\HttpException(404, 'The requested Item could not be found.');
            }
            
            // Get and check if is exists this staff with store
            if($staff = MasterStaff::findOne($staff_id)){
                $store = MasterStore::findOne($store_id);
                
                // Get working hours
                $hours = Util::genArrayWorkHours($store->time_open, $store->time_close, 30);
                                
                //get staff schedules
                $staffSchedules = $this->getStaffSchedule($month, $year, $staff->id);
                                
                // Get store shift
                $storeShift = \common\models\StoreShift::search($store_id)->all();
                
                //index work day
                $storeShift = ArrayHelper::map($storeShift, 'id', 'name');
                                
                // Get none-working hourse
                $staffNotWorkingHours = \common\models\StaffNotWorking::find()
                        ->where(['staff_schedule_id' => $staff->id])
                        ->asArray()
                        ->all();
                                
                $this->layout = false;
                
                return $this->render('_dialog_bulk_staff_booking_setting', compact('year','month', 'store', 'hours', 'staff',
                        'staffSchedules', 'staffNotWorkingHours' ,'storeShift'));
            }
        }
        
        // Return if Is not exists this store
        throw new \yii\web\HttpException(404, 'The requested Item could not be found.');
    }
    
    /**
     * AJAX for スタッフ予約受付設定から呼び出した時 5.8/ Bulk staff booking setting
     * @return render
     */
    public function actionAjaxSetSessionBulkStaffBooking(){
        // Check request is post method
        $session = Yii::$app->session;
        if(Yii::$app->request->isPost){
            $postData = Yii::$app->request->post();

            // Get old staff setting from session
            $schedule = $session['scheduleStaff'];
            $staff_id = (int)Yii::$app->request->post('staff_id');

            // Check staff is exists and loop to set data for this staff
            if(isset($schedule[$staff_id])){                
//                foreach($schedule[$staff_id] as $day => $value){
//                    $schedule[$staff_id][$day]['staff_not_working'] = !empty($postData['notworking']) ? $postData['notworking'] : [];
//                    
//                    if(empty($schedule[$staff_id][$day]['staff_schedule'])){
//                        $schedule[$staff_id][$day]['staff_schedule']['shift_id'] = $postData['shift_id'];
//                        $schedule[$staff_id][$day]['staff_schedule']['schedule_date'] = $day;
//                        $schedule[$staff_id][$day]['staff_schedule']['staff_id'] = $staff_id;
//                    }
//                    
//                    if(isset($postData[$day])){
//                        $schedule[$staff_id][$day]['staff_schedule']['work_flg'] = empty($postData[$day]) || $postData[$day] == '0' ? 0 : 1;
//                    }
//                }
                if(isset($postData['work_flg']) && ($postData['work_flg'] == '1' || $postData['work_flg'] == 1)){
                    foreach($schedule[$staff_id] as $day => $value){
                        if(isset($postData[$day])){
                            if(empty($schedule[$staff_id][$day]['staff_schedule'])){
                                $schedule[$staff_id][$day]['staff_schedule']['schedule_date'] = $day;
                                $schedule[$staff_id][$day]['staff_schedule']['staff_id'] = $staff_id;
                            }
                            
                            $schedule[$staff_id][$day]['staff_schedule']['shift_id'] = $postData['shift_id'];
                            $schedule[$staff_id][$day]['staff_not_working'] = !empty($postData['notworking']) ? $postData['notworking'] : [];
                            $schedule[$staff_id][$day]['staff_schedule']['work_flg'] = '1';
                        }
                    }
                } else {
                    foreach($schedule[$staff_id] as $day => $value){
                        if(isset($postData[$day])){
                            if(empty($schedule[$staff_id][$day]['staff_schedule'])){
                                $schedule[$staff_id][$day]['staff_schedule']['schedule_date'] = $day;
                                $schedule[$staff_id][$day]['staff_schedule']['staff_id'] = $staff_id;
                            }
                            
                            //$schedule[$staff_id][$day]['staff_schedule']['shift_id'] = $postData['shift_id'];
                            $schedule[$staff_id][$day]['staff_schedule']['work_flg'] = '0';
                        }
                    } 
                }
                $schedule['isDirty'] = true;
                // Set new schedule staff to session
                $session['scheduleStaff'] = $schedule;

                return json_encode(['status' => true]);
            }

            // Set session
            return json_encode(['status' => false]);
        }
    }
    
    public function actionEditResponsibleStaff ($id) {
        $booking = $this->findModel($id);
        /* @var $order \common\models\MstOrder */
        $order = $booking->getOrder()->with('masterOrderDetail')->one();
        $staff_list = ArrayHelper::index(MasterStaff::find()->andWhere([
            'store_id' => $booking->store_id
        ])->with('managementLogin')->all(), 'id');
        // get all order detail
        $order_detail = ArrayHelper::index($order->masterOrderDetail, 'id');
        
        $post = Yii::$app->request->post();
        
        if (isset($post['staff']) && is_array($post['staff'])) {
            $transaction = Yii::$app->db->beginTransaction();
            try {
                $errors = [];
                foreach ($post['staff'] as $detail_id => $staff_id) {
                    if ($detail_id == 0) {
                        if (isset($staff_list[$staff_id])) {
                            //var_dump($order->order_management_id);
                            $order->order_management_id = $staff_list[$staff_id]->managementLogin->management_id;
                            $order->save();
                            $errors = array_merge($errors, $order->errors);
                        }
                    }
                    elseif (isset($order_detail[$detail_id])) {
                        $order_detail[$detail_id]->product_management_id = $staff_list[$staff_id]->managementLogin->management_id;
                        $order_detail[$detail_id]->save();
                        $errors = array_merge($errors, $order_detail[$detail_id]->errors);
                        
                        // updated_at aHoc aHung
                        $order->updated_at = time();
                        $order->save();
                    }
                }
                
                $transaction->commit();
                
                return $this->redirect(['', 'id' => Yii::$app->request->get('id')]);
            }
            catch (Exception $e) {
//                var_dump($e);
                $transaction->rollBack();
            }
        }
        
        return $this->render('edit-responsible-staff', [
            'model' => $booking,
            'order' => $order,
            'order_detail'=> $order_detail,
            'staff_list' => $staff_list
        ]);
    }
    
    /**
     * save detail schedule to seat_schedule_detail.
     * @return mixed
     */
    public static function saveShiftToSeatScheduleDetail($seat_id, $booking_date, $store_id, $booking_code, $startTime, $endTime, $booking_status = '2') {
        //staff_id , booking_date

        $startTime= new \DateTime('2010-01-01 ' . $startTime);
        $endTime  = new \DateTime('2010-01-01 ' . $endTime);
        $endTime->sub(new \DateInterval('PT30M'));
        $interval = new \DateInterval("PT30M");
        $timeStep = 30;
        $timeArray= array();

        while ($startTime <= $endTime) {
            $timeArray[] = $startTime->format('H:i');
            $startTime->add(new \DateInterval('PT' . $timeStep . 'M'));
        }
		
        foreach ($timeArray as $value) {
            $model = \common\models\SeatScheduleDetail::find()->where([
                        'booking_date'  => $booking_date,
                        'seat_id'      => $seat_id,
                        'store_id'      => $store_id,
                        'booking_time'  => $value
                    ])->one();
            // create new if not found
            if (empty($model)) {
                $model = new \common\models\SeatScheduleDetail();
            }
            // update when not busy
            if ($model->booking_status != \common\models\StaffScheduleDetail::BOOKING_STATUS_BUSY) {
                $model->booking_status= \common\models\StaffScheduleDetail::BOOKING_STATUS_BUSY;
                $model->booking_code  = $booking_code;
                
            }
            $model->seat_id       = $seat_id;
            $model->booking_date  = $booking_date;
            $model->booking_time  = $value;
            $model->store_id      = $store_id;
            $model->booking_status= $booking_status;
            
            $model->save();
//          Util::d($booking_status);
        }
//      Util::dd($timeArray);

        return true;
    }
    
    /**
     * choose seat modal.
     * @return mixed
     */
    public function actionChooseSeat () {
        $seatChoose      = Yii::$app->request->post('seatChoose');
        $storeId    = Yii::$app->request->post('storeId');
        $typeSelect = Yii::$app->request->post('typeSelect');
        $noSmoking  = Yii::$app->request->post('noSmoking');
        $seatFree   = Yii::$app->request->post('seatFree');
        $seatChoose   = ArrayHelper::index($seatChoose, 'id');
        $numberPeople= Yii::$app->request->post('numberPeople');
        $seatId = ArrayHelper::getColumn($seatChoose, 'id');
        return $this->renderAjax('reservation/_search_seat_choose_seat_modal',[
                                                             'seatChoose' =>$seatChoose,
                                                             'seats'      =>$seatFree ,
                                                             'seatId'    => $seatId ,
                                                             'numberPeople'=>$numberPeople
                                                            ]);
    }
  }
