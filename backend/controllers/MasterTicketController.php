<?php

namespace backend\controllers;

use Yii;
use common\models\MasterTicket;
use common\models\MasterTicketSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\components\Util;
use common\components\Constants;
use common\models\MstProduct;
use common\models\MstProductSearch;
use common\models\MstCategoriesProduct;
use Exception;
use common\models\ProductTicket;
use common\models\MasterStore;

/**
 * MasterTicketController implements the CRUD actions for MasterTicket model.
 */
class MasterTicketController extends Controller {

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all MasterTicket models.
     * @return mixed
     */
    public function actionIndex() {
        $searchModel = new MasterTicketSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single MasterTicket model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id) {
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new MasterTicket model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($list_product = []) {
        //HoangNQ set role <> STAFF_ROLE
        \common\components\FindPermission::permissionBeforeAction(\common\components\FindPermission::getRole()->permission_id != \common\components\Constants::STAFF_ROLE);
        //HoangNQ end
        try {
            //create transaction
            $transaction = Yii::$app->db->beginTransaction();
            //create model            
            $model = new MasterTicket();
            //get list Category
            $listCategory = (new MstCategoriesProduct)->listCategory();
            //get list Product
            $listProduct = (new MstProduct)->getListProductAll($listCategory);
            //check init or submit
            if ($model->load(Yii::$app->request->post())) {
                //ひとつも表示されていない状態では回数券を登録できません。
                if ($model->option_hidden === '') {
                    $list_product_select = (new MstProductSearch)->searchProduct($list_product);
                    return $this->render('create', [
                                'model' => $model,
                                'listProduct' => $listProduct,
                                'list_product_select' => $list_product_select,
                                'listCategory' => $listCategory,
                    ]);
                }
                $list_product = (count($list_product) > 0) ? $list_product : $model->option_hidden;
                //get list product had selected
                $list_product_select = (new MstProductSearch)->searchProduct($list_product);

                if (empty($model->ticket_jan_code)) {
                    $model->ticket_jan_code = Util::genJanCodeAuto(Constants::JAN_TYPE_TICKET);
                } else {
                    //回数券の JAN コードは 2300000000009(13桁)~2399999999999(13 桁)までの範囲を使用します
                    $check_jan_code = Util::checkJanCodeTicket(Constants::JAN_TYPE_TICKET, $model->ticket_jan_code);

                    if (!($check_jan_code)) {
                        $model->addError('ticket_jan_code', Yii::t('backend', "JAN_Code invalid, please try again."));
                        return $this->render('create', [
                                    'model' => $model,
                                    'listProduct' => $listProduct,
                                    'list_product_select' => $list_product_select,
                                    'listCategory' => $listCategory,
                        ]);
                    }
                }
                //回数券の売価を入力します。対象商品とセット回数が入力されたとき、この項目が未入力であれば、対象商品の単価の合計とセット回数をかけた数値を入力します。
                if ($model->price === '') {
                    //$data = \yii\helpers\ArrayHelper::map($list_product_select, 'id', 'unit_price');
                    $total_price = 0;
                    foreach ($list_product_select->models as $temp) {
                        $total_price += $temp->unit_price;
                    }
                    $model->price = ($total_price * $model->set_ticket !== 0) ? $total_price * $model->set_ticket : $model->price;
                }
                if (empty($model->store_id)) {
                    $store_is_common = (new MasterStore)->findStoreCommon('00000');
                    $model->store_id = $store_is_common->id;
                }

                // Check Store Exists
                if (!$this->checkStoreByExists($model->store_id)) {
                    throw new Exception('Oject deleted');
                }

                // Check Products Exists
                if (!empty($list_product)) {
                    if (!$this->CheckProductsExists($list_product)) {
                        throw new Exception('Oject deleted');
                    }
                }

                if (!$model->save()) {
                    $transaction->rollBack();
                    Yii::$app->getSession()->setFlash('error', [
                        'error' => Yii::t('backend', "Sorry can't create ticket, please try again.")
                    ]);
                    return $this->render('create', [
                                'model' => $model,
                                'listProduct' => $listProduct,
                                'list_product_select' => $list_product_select,
                                'listCategory' => $listCategory,
                    ]);
                }
                $model->saveProductTicket($model->id, $model->option_hidden);
                $transaction->commit();
                Yii::$app->getSession()->setFlash('success', [
                    'success' => Yii::t('backend', "Save Success")
                ]);
                return $this->redirect(['index']);
            } else {

                $list_product = 0;
                if (!empty(Yii::$app->request->post()) && isset(Yii::$app->request->post()['option_hidden'])) {
                    $list_product = !empty(Yii::$app->request->post()['option_hidden']) ? Yii::$app->request->post()['option_hidden'] : (-1);
                }
                $list_product = ($list_product != 0) ? $list_product : $model->option_hidden;
                //get list product had selected
                $list_product_select = (new MstProductSearch)->searchProduct($list_product);

                $role = \common\components\FindPermission::getRole();
                $model->_role = $role->permission_id;
                $model->id_store_comon = (new MasterStore)->findStoreCommon('00000');

                return $this->render('create', [
                            'model' => $model,
                            'listProduct' => $listProduct,
                            'list_product_select' => $list_product_select,
                            'listCategory' => $listCategory
                ]);
            }
        } catch (Exception $ex) {
            //rollback transaction
            $transaction->rollBack();
            //set error message
            if ($ex->getMessage() == 'Oject deleted') {
                
                Yii::$app->getSession()->setFlash('error', [
                    'error' => Yii::t('backend', "System error happen. Please contact with System manager.")
                ]);
                return $this->redirect(['index']);
                
            }
            Yii::$app->getSession()->setFlash('error', [
                'error' => Yii::t('backend', "Sorry can't create ticket, please try again.")
            ]);
            return $this->render('create', [
                        'model' => $model,
                        'listProduct' => $listProduct,
                        'list_product_select' => $list_product_select,
                        'listCategory' => $listCategory
            ]);
        }
    }

    public function actionGetTotalUnitPriceByIdProduct() {
        $request = Yii::$app->request;
        if ($request->isAjax) {
            $data = Yii::$app->request->post();
            foreach ($data as $value) {
                $list_id_product = $value;
            }
            // Get list UnitPrice of Product
            $list_unit_price = (new MstProductSearch)->searchUnitPriceByListIdProduct($list_id_product);
            $unit_price_select = 0;

            if (count($list_unit_price) > 0) {
                foreach ($list_unit_price as $value) {
                    $unit_price_select = $value['sum'];
                }
            }
            //$list_id_product = explode(":", $data[0]['listIdProduct']);
            return $unit_price_select;
        }
    }

    /**
     * Updates an existing MasterTicket model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
        //get param
        $list_product = [];
        $id_arr = !isset($id) ? $id : explode('_', $id);
        $id = (count($id_arr) <= 0) ? $id : $id_arr[0];
        $list_product = (count($id_arr) <= 1) ? $list_product : $id_arr[1];
        //create model
        $model = $this->findModel($id);
        //HoangNQ set role <> STAFF_ROLE
        \common\components\FindPermission::permissionBeforeAction(\common\components\FindPermission::getRole()->permission_id != \common\components\Constants::STAFF_ROLE, $model->store_id);
        //HoangNQ end
        try {
            //create transaction
            $transaction = Yii::$app->db->beginTransaction();
            //get list Category
            $listCategory = (new MstCategoriesProduct)->listCategory();
            //get list Product
            $listProduct = (new MstProduct)->getListProductAll($listCategory);

            $list_product_select = (new MstProductSearch)->searchProduct([]);
            //check init or submitx
            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                //ひとつも表示されていない状態では回数券を登録できません。
                if ($model->option_hidden === '') {
                    //$list_product_select = (new MstProductSearch)->searchProduct([]);
                    return $this->render('update', [
                                'model' => $model,
                                'listProduct' => $listProduct,
                                'list_product_select' => $list_product_select,
                                'listCategory' => $listCategory,
                    ]);
                }
                $list_product = 0;
                if (!empty(Yii::$app->request->post()) && isset(Yii::$app->request->post()['option_hidden'])) {
                    $list_product = !empty(Yii::$app->request->post()['option_hidden']) ? Yii::$app->request->post()['option_hidden'] : (-1);
                }
                $option_hidden = ProductTicket::find()->andWhere(['ticket_id' => $model->id])->select(['product_id'])->all();

                if (empty($model->option_hidden)) {
                    foreach ($option_hidden as $val) {
                        $model->option_hidden .= $val->product_id . ',';
                    }
                }
                $list_product = ($list_product != 0) ? $list_product : $model->option_hidden;
                //get list product had selected
                $list_product_select = (new MstProductSearch)->searchProduct($list_product);
                if (empty($model->ticket_jan_code)) {
                    $model->ticket_jan_code = Util::genJanCodeAuto(Constants::JAN_TYPE_TICKET);
                } else {
                    //回数券の JAN コードは 2300000000009(13桁)~2399999999999(13 桁)までの範囲を使用します
                    $check_jan_code = Util::checkJanCodeTicket(Constants::JAN_TYPE_TICKET, $model->ticket_jan_code);
                    if (!($check_jan_code)) {
                        $model->addError('ticket_jan_code', Yii::t('backend', "JAN_Code invalid, please try again."));
                        return $this->render('update', [
                                    'model' => $model,
                                    'listProduct' => $listProduct,
                                    'list_product_select' => $list_product_select,
                                    'listCategory' => $listCategory,
                        ]);
                    }
                }
                //回数券の売価を入力します。対象商品とセット回数が入力されたとき、この項目が未入力であれば、対象商品の単価の合計とセット回数をかけた数値を入力します。
                if ($model->price === '') {
                    //$data = \yii\helpers\ArrayHelper::map($list_product_select, 'id', 'unit_price');
                    $total_price = 0;
                    foreach ($list_product_select->models as $temp) {
                        $total_price += $temp->unit_price;
                    }
                    $model->price = ($total_price * $model->set_ticket !== 0) ? $total_price * $model->set_ticket : $model->price;
                }
                if (empty($model->store_id)) {
                    $store_is_common = (new MasterStore)->findStoreCommon('00000');
                    $model->store_id = $store_is_common->id;
                }

                // Check Store Exists
                if (!$this->checkStoreByExists($model->store_id)) {
                    throw new Exception('Oject deleted');
                }

                // Check Products Exists
                if (!empty($list_product)) {
                    if (!$this->CheckProductsExists($list_product)) {
                        throw new Exception('Oject deleted');
                    }
                }

                if (!($model->save())) {
                    return $this->render('update', [
                                'model' => $model,
                                'listProduct' => $listProduct,
                                'list_product_select' => $list_product_select,
                                'listCategory' => $listCategory,
                    ]);
                }
                $model->saveProductTicket($model->id, $model->option_hidden);
                $transaction->commit();
                Yii::$app->getSession()->setFlash('success', [
                    'success' => Yii::t('backend', "Update Success")
                ]);
                return $this->redirect(['index']);
            } else {

                $model->ticket_jan_code_old = $model->ticket_jan_code;
                $list_product = 0;
                if (!empty(Yii::$app->request->post()) && isset(Yii::$app->request->post()['option_hidden'])) {
                    $list_product = !empty(Yii::$app->request->post()['option_hidden']) ? Yii::$app->request->post()['option_hidden'] : (-1);
                }
                $option_hidden = ProductTicket::find()->andWhere(['ticket_id' => $model->id])->select(['product_id'])->all();
                if (empty($model->option_hidden)) {
                    foreach ($option_hidden as $val) {
                        $model->option_hidden .= $val->product_id . ',';
                    }
                }
                $list_product = ($list_product != 0) ? $list_product : $model->option_hidden;
                //get list product had selected
                $list_product_select = (new MstProductSearch)->searchProduct($list_product);

                $role = \common\components\FindPermission::getRole();
                $model->_role = $role->permission_id;
                $model->id_store_comon = (new MasterStore)->findStoreCommon('00000');

                return $this->render('update', [
                            'model' => $model,
                            'listProduct' => $listProduct,
                            'list_product_select' => $list_product_select,
                            'listCategory' => $listCategory,
                ]);
            }
        } catch (Exception $ex) {
            //rollback transaction
            $transaction->rollBack();
            //set error message
            if ($ex->getMessage() == 'Oject deleted') {
                
                Yii::$app->getSession()->setFlash('error', [
                    'error' => Yii::t('backend', "System error happen. Please contact with System manager.")
                ]);
                return $this->redirect(['index']);
                
            }
            //set error message
            Yii::$app->getSession()->setFlash('error', [
                'error' => Yii::t('backend', "Sorry can't update ticket, please try again.")
            ]);
            return $this->render('update', [
                        'model' => $model,
                        'listProduct' => $listProduct,
                        'list_product_select' => $list_product_select,
                        'listCategory' => $listCategory,
            ]);
        }
    }

    /**
     * Deletes an existing MasterTicket model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {
        try {
            //create transaction
            $transaction = Yii::$app->db->beginTransaction();
            $model = $this->findModel($id);
            if (!$model->checkDelete($id)) {
                return 'false';
            } else {
                //soft delete tax
                $model->softDelete();
                //commit data
                $transaction->commit();
                //return list tax
                Yii::$app->getSession()->setFlash('success', [
                    'success' => Yii::t('backend', "Deleted successfully.")
                ]);
                return $this->redirect(['index'],200);
            }
        } catch (Exception $ex) {//if have exception
            //rollback transaction
            $transaction->rollBack();
            return 'false';
        }
    }

    /**
     * Finds the MasterTicket model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return MasterTicket the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = MasterTicket::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    // Validate From
    public function actionValidate() {

        $model = new MasterTicket();
        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {

            Yii::$app->response->format = 'json';

            return \yii\bootstrap\ActiveForm::validate($model);
        }
    }

    /**
     * Finds the Store model exists based on its primary key value.
     * If the model is not found, return false.
     * @param integer $id_store
     * @return true
     */
    public function checkStoreByExists($id_store) {
        if (($model = MasterStore::findOne($id_store)) !== null) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Check Products model exists based on its primary key value.
     * If the model is not exists, return false.
     * @param integer $id_product
     * @return true
     */
    public function CheckProductsExists($id_product) {
        if (!empty($id_product)) {
            $check_value_product = substr($id_product, -1);
            if ($check_value_product == ',') {
                $id_product = rtrim($id_product, ",");
                $count_array_products = count(explode(",", $id_product));
            } else {
                $count_array_products = count(explode(",", $id_product));
            }
            if ((new MstProduct())->getCountProductsExists($id_product) != $count_array_products) {
                return false;
            } else {
                return true;
            }
        } else {
            return true;
        }
    }

}
