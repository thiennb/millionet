<?php

namespace backend\controllers;

use Yii;
use common\models\MstProduct;
use common\models\MstProductSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\components\Util;
use common\components\Constants;
use yii\web\UploadedFile;
use Exception;
use common\models\MasterOption;
use common\models\ProductOption;
use yii\helpers\ArrayHelper;
use common\models\MasterStore;

/**
 * MstProductController implements the CRUD actions for MstProduct model.
 */
class MstProductController extends Controller {

    //variable connection database
    public $db;

    public function init() {
        //get connection
        $this->db = Yii::$app->db;
    }

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all MstProduct models.
     * @return mixed
     */
    public function actionIndex() {
        $searchModel = new MstProductSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $session = Yii::$app->session;
        $session->remove('tmp_image1');
        $session->remove('tmp_image2');
        $session->remove('tmp_image3');
        //get list store
        $listStore = (new MasterStore)->getListStoreHaveStoreCommon();

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'listStore' => $listStore,
        ]);
    }

    /**
     * Creates a new MstProduct model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        try {
            //create transaction
            $transaction = $this->db->beginTransaction();
            //get list option 
            $mstOption = new MasterOption();
            $store_id = -1;
            if (!empty(Yii::$app->request->post()) && !empty(Yii::$app->request->post()['store_id'])) {
                $store_id = Yii::$app->request->post()['store_id'];
            }
            $listOption = $mstOption->listOptionStore($store_id);
            //get list store
            $listStore = (new MasterStore)->getListStoreNotStoreCommon();
            //create mode
            $model = new MstProduct();
            //check submit form (method post)
            if ($model->load(Yii::$app->request->post()) && $model->validate()) {//case submit form
                //get imange
                $model->tmp_image1 = UploadedFile::getInstance($model, 'tmp_image1');
                $model->tmp_image2 = UploadedFile::getInstance($model, 'tmp_image2');
                $model->tmp_image3 = UploadedFile::getInstance($model, 'tmp_image3');


                //check image 1 data
                if ($model->tmp_image1) {
                    $model->image1 = Yii::$app->security->generateRandomString() . '.' . $model->tmp_image1->extension;
                }
                //check image 2 data
                if ($model->tmp_image2) {
                    $model->image2 = Yii::$app->security->generateRandomString() . '.' . $model->tmp_image2->extension;
                }
                //check image 3 data
                if ($model->tmp_image3) {
                    $model->image3 = Yii::$app->security->generateRandomString() . '.' . $model->tmp_image3->extension;
                }
                //check jan_code
                if (empty($model->jan_code)) {
                    $model->jan_code = Util::genJanCodeAuto(Constants::JAN_TYPE_PRODUCT);
                }

                if ($model->assign_fee_flg == 1) {
                    $model->show_flg = '0';
                }

                if (empty($model->store_id)) {
                    $store_is_common = (new MasterStore)->findStoreCommon('00000');
                    $model->store_id = $store_is_common->id;
                }
                // Check Store Exists
                if (!$this->checkStoreByExists($model->store_id)) {
                    throw new Exception('Object deleted');
                }
                // Check Category Exists
                if (!$this->checkCategoryExists($model->category_id)) {
                    throw new Exception('Object deleted');
                }
                // Check Tax Exists
                if (!$this->checkTaxExists($model->tax_rate_id)) {
                    throw new Exception('Object deleted');
                }
                // Check Option Exists
                if (!empty($model->option) && (count($model->option) > 0)) {
                    if (!$this->CheckOptionExists($model->option)) {
                        throw new Exception('Object deleted');
                    }
                }

                //validate and save product
                if ($model->save()) {// case ok
                    //upload image
                    if (!empty($model->image1)) {
                        Util::uploadFile($model->tmp_image1, $model->image1);
                    }
                    if (!empty($model->image2)) {
                        Util::uploadFile($model->tmp_image2, $model->image2);
                    }
                    if (!empty($model->image3)) {
                        Util::uploadFile($model->tmp_image3, $model->image3);
                    }
                    //save option
                    if (!empty($model->option) && (count($model->option) > 0)) {
                        $model->saveOption();
                    }

                    //commit transaction
                    $transaction->commit();
                    Yii::$app->getSession()->setFlash('success', [
                        'success' => Yii::t('backend', "Create Success")
                    ]);
                    //return index
                    return $this->redirect(['index']);
                }

                return $this->render('create', [
                            'model' => $model,
                            'listOption' => $listOption,
                            'listStore' => $listStore,
                ]);
            } else {//case init
                return $this->render('create', [
                            'model' => $model,
                            'listOption' => $listOption,
                            'listStore' => $listStore,
                ]);
            }
        } catch (Exception $ex) {//if have exception
            //rollback transaction
            $transaction->rollBack();
            //set error message
            Yii::$app->getSession()->setFlash('error', [
                'error' => Yii::t('backend', "System error happen. Please contact with System manager.")
            ]);
            if($ex->getMessage() == 'Object deleted'){
                return $this->redirect(['index']);
            }
            //return index
            return $this->render('create', [
                        'model' => $model,
                        'listOption' => $listOption,
                        'listStore' => $listStore,
            ]);
        }
    }

    /**
     * Updates an existing MstProduct model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
        try {
            //create transaction
            $transaction = $this->db->beginTransaction();
            //get list store
            $listStore = (new MasterStore)->getListStoreNotStoreCommon();
            //create mode
            $model = $this->findModel($id);
            //get list option 
            $mstOption = new MasterOption();
            $store_id = -1;
            if (!empty(Yii::$app->request->post()) && !empty(Yii::$app->request->post()['store_id'])) {
                $store_id = Yii::$app->request->post()['store_id'];
            }
            $store_id = !($store_id == -1) ? $store_id : $model->store_id;
            $listOption = $mstOption->listOptionStore($store_id);

            //check submit form (method post)
            if ($model->load(Yii::$app->request->post()) && $model->validate()) {//case submit form
                //check delete image 1
                if (empty($model->hidden_image1) && !empty($model->image1)) {
                    $model->hidden_image1 = $model->image1;
                    $model->image1 = '';
                }
                //check delete image 2
                if (empty($model->hidden_image2) && !empty($model->image2)) {
                    $model->hidden_image2 = $model->image2;
                    $model->image2 = '';
                }
                //check delete image 3
                if (empty($model->hidden_image3) && !empty($model->image3)) {
                    $model->hidden_image3 = $model->image3;
                    $model->image3 = '';
                }
                //get imange from seeson
                $model->tmp_image1 = UploadedFile::getInstance($model, 'tmp_image1');
                $model->tmp_image2 = UploadedFile::getInstance($model, 'tmp_image2');
                $model->tmp_image3 = UploadedFile::getInstance($model, 'tmp_image3');


                //check image 1 data
                if ($model->tmp_image1) {
                    $model->image1 = Yii::$app->security->generateRandomString() . '.' . $model->tmp_image1->extension;
                }
                //check image 2 data
                if ($model->tmp_image2) {
                    $model->image2 = Yii::$app->security->generateRandomString() . '.' . $model->tmp_image2->extension;
                }
                //check image 3 data
                if ($model->tmp_image3) {
                    $model->image3 = Yii::$app->security->generateRandomString() . '.' . $model->tmp_image3->extension;
                }
                //check jan_code
                if (empty($model->jan_code)) {
                    $model->jan_code = Util::genJanCodeAuto(Constants::JAN_TYPE_PRODUCT);
                }
                if ($model->assign_fee_flg == 1) {
                    $model->show_flg = '0';
                }

                if (empty($model->store_id)) {
                    $store_is_common = (new MasterStore)->findStoreCommon('00000');
                    $model->store_id = $store_is_common->id;
                }
               // Check Store Exists
                if (!$this->checkStoreByExists($model->store_id)) {
                    throw new Exception('Object deleted');
                }
                // Check Category Exists
                if (!$this->checkCategoryExists($model->category_id)) {
                    throw new Exception('Object deleted');
                }
                // Check Tax Exists
                if (!$this->checkTaxExists($model->tax_rate_id)) {
                    throw new Exception('Object deleted');
                }
                // Check Option Exists
                if (!empty($model->option) && (count($model->option) > 0)) {
                    if (!$this->CheckOptionExists($model->option)) {
                        throw new Exception('Object deleted');
                    }
                }

                if ($model->save()) {// case ok
                    //upload image
                    if (!empty($model->image1)) {
                        Util::uploadFile($model->tmp_image1, $model->image1);
                    }
                    if (!empty($model->image2)) {
                        Util::uploadFile($model->tmp_image2, $model->image2);
                    }
                    if (!empty($model->image3)) {
                        Util::uploadFile($model->tmp_image3, $model->image3);
                    }
                    //save option
                    if (!empty($model->option) && (count($model->option) > 0)) {
                        $model->saveOption();
                    }
                    //commit transaction
                    $transaction->commit();
                    //delete image 1
                    if (!empty($model->hidden_image1) && empty($model->image1)) {
                        Util::deleteFile($model->hidden_image1);
                    }
                    //delete image 2
                    if (!empty($model->hidden_image2) && empty($model->image2)) {
                        Util::deleteFile($model->hidden_image2);
                    }
                    //delete image 3
                    if (!empty($model->hidden_image3) && empty($model->image3)) {
                        Util::deleteFile($model->hidden_image3);
                    }
                    Yii::$app->getSession()->setFlash('success', [
                        'success' => Yii::t('backend', "Update Success")
                    ]);
                    //return index
                    return $this->redirect(['index']);
                } else {// validate fail or save error
                    //rollback transaction
                    $transaction->rollBack();
                    //return view create
                    return $this->render('update', [
                                'model' => $model,
                                'listOption' => $listOption,
                                'listStore' => $listStore,
                    ]);
                }
            } else {//case init
                //get option of product
                $arr_option = ArrayHelper::toArray(ProductOption::find()->andWhere(['product_id' => $id])->select('option_id')->all());
                if (count($arr_option) > 0) {
                    foreach ($arr_option as $val) {
                        $model->option[] += $val['option_id'];
                        $model->option_hidden = $model->option_hidden . $val['option_id'] . ',';
                    }
                }
                $model->hidden_image1 = $model->image1;
                $model->hidden_image2 = $model->image2;
                $model->hidden_image3 = $model->image3;

                return $this->render('update', [
                            'model' => $model,
                            'listOption' => $listOption,
                            'listStore' => $listStore,
                ]);
            }
        } catch (Exception $ex) {//if have exception
            //rollback transaction
            $transaction->rollBack();
            //set error message
            Yii::$app->getSession()->setFlash('error', [
                'error' => Yii::t('backend', "System error happen. Please contact with System manager.")
            ]);
            //return index
            return $this->redirect(['index']);
        }
    }

    /**
     * Deletes an existing MstProduct model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {
        try {
            //create transaction
            $transaction = $this->db->beginTransaction();
            $model = $this->findModel($id);
            if (!$model->checkDelete($id)) {
                return 'false';
            } else {
                //soft delete tax
                $model->softDelete();
                //commit data
                $transaction->commit();
                Yii::$app->getSession()->setFlash('success', [
                    'success' => Yii::t('backend', "Deleted successfully.")
                ]);
                //return list tax
                return $this->redirect(['index'],200);
            }
        } catch (Exception $ex) {//if have exception
            //rollback transaction
            $transaction->rollBack();
            //set error message
            return 'false';
        }
    }

    /**
     * Finds the MstProduct model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return MstProduct the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = MstProduct::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    // Validate From
    public function actionValidate() {
        $model1 = new MstProduct();
        $model1->load(Yii::$app->request->post());
        $model = new MstProduct();
        $model = !empty($model1->id_hd) ? MstProduct::findOne($model1->id_hd) : $model;

        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            $model->unit_price = str_replace(",", "", $model->unit_price);
            $model->cost = str_replace(",", "", $model->cost);
            Yii::$app->response->format = 'json';
            return \yii\bootstrap\ActiveForm::validate($model);
        }
    }

    /**
     * Finds the Store model exists based on its primary key value.
     * If the model is not found, return false.
     * @param integer $id
     * @return true
     */
    public function checkStoreByExists($id) {
        if (($model = MasterStore::findOne($id)) !== null) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Finds the Product Category model exists based on its primary key value.
     * If the model is not found, return false.
     * @param integer $id_category
     * @return true
     */
    public function checkCategoryExists($id_category) {
        if (($model = \common\models\MstCategoriesProduct::findOne($id_category)) !== null) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Finds the Tax model exists based on its primary key value.
     * If the model is not found, return false.
     * @param integer $id_tax
     * @return true
     */
    public function checkTaxExists($id_tax) {
        if (($model = \common\models\MasterTax::findOne($id_tax)) !== null) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Check Options model exists based on its primary key value.
     * If the model is not exists, return false.
     * @param integer $id
     * @return true
     */
    public function CheckOptionExists($id_option) {
        $option = $id_option;
        $count_array_options = count($option);
        if ((new MstProduct())->getCountOption($option) != $count_array_options) {
            return false;
        } else {
            return true;
        }
    }

}
