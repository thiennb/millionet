<?php

namespace backend\controllers;

use Yii;
use common\models\MasterSeat;
use common\models\MasterSeatSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \common\models\MasterTypeSeat;
// Transaction
use Exception;
use yii\helpers\Json;
use common\components\Constants;
use common\models\MasterStore;
use common\components\CommonCheckExistModel;

/**
 * MasterSeatController implements the CRUD actions for MasterSeat model.
 */
class MasterSeatController extends Controller {

    //variable connection database
    public $db;

    public function init() {
        //get connection
        $this->db = Yii::$app->db;
    }

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all MasterSeat models.
     * @return mixed
     */
    public function actionIndex() {
        $searchModel = new MasterSeatSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single MasterSeat model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id) {
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new MasterSeat model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        try {
            //create transaction
            $transaction = $this->db->beginTransaction();
            $model = new MasterSeat();
            $request = Yii::$app->request;
            if (Yii::$app->request->isAjax && $request->post('btn-confirm') != null && $model->load(Yii::$app->request->post())) {
                //validate by pjax before preview not save model  

                $model->validate();
                return $this->render('create', [
                            'model' => $model,
                ]);
            }

            if ($model->load(Yii::$app->request->post())) {
                // Commit Transaction
                if (empty($model->store_id)) {
                    $store_is_common = (new MasterStore)->findStoreCommon('00000');
                    $model->store_id = $store_is_common->id;
                }
                $model->seat_code = $model->generateSeatCode($model->store_id);

                // Check Store Exist
                if (!(new CommonCheckExistModel)->checkStoreByExists($model->store_id)) {
                    throw new Exception('Oject delete');
                }
                // Check Type Seat Exist
                if (!(new CommonCheckExistModel)->CheckTypeSeatExists($model->type_seat_id)) {
                    throw new Exception('Oject delete');
                }

                if ($model->save()) {
                    $transaction->commit();
                    Yii::$app->session->setFlash('success', Yii::t("backend", "Create Success"));
                    return $this->redirect(['index']);
                }
            } else {

                return $this->render('create', [
                            'model' => $model,
                ]);
            }
        } catch (Exception $ex) {
            //rollback transaction
            $transaction->rollBack();
            if ($ex->getMessage() == 'Oject deleted') {

                Yii::$app->getSession()->setFlash('error', [
                    'error' => Yii::t('backend', "System error happen. Please contact with System manager.")
                ]);
                return $this->redirect(['index']);
            }
            //set error message
            Yii::$app->getSession()->setFlash('error', [
                'error' => Yii::t('backend', "Sorry can't create seat, please try again.")
            ]);
            // Return View Create
            return $this->render('create', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing MasterSeat model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
        try {
            //create transaction
            $transaction = $this->db->beginTransaction();
            $model = $this->findModel($id);

            if (Yii::$app->request->isAjax && Yii::$app->request->post('btn-confirm') != null && $model->load(Yii::$app->request->post())) {
                //validate by pjax before preview not save model  
                $model->validate();
                return $this->render('update', [
                            'model' => $model,
                ]);
            }

            if ($model->load(Yii::$app->request->post())) {
                // Check Store Exist
                if (!(new CommonCheckExistModel)->checkStoreByExists($model->store_id)) {
                    throw new Exception('Oject delete');
                }
                // Check Type Seat Exist
                if (!(new CommonCheckExistModel)->CheckTypeSeatExists($model->type_seat_id)) {
                    throw new Exception('Oject delete');
                }
                if ($model->save()) {
                    // Commit Transaction
                    $transaction->commit();
                    Yii::$app->session->setFlash('success', Yii::t("backend", "Update Success"));
                    return $this->redirect(['index']);
                }
            } else {
//            \common\components\Util::dd($model);exit();
                return $this->render('update', [
                            'model' => $model,
                ]);
            }
        } catch (Exception $ex) {

            //rollback transaction
            $transaction->rollBack();
            if ($ex->getMessage() == 'Oject deleted') {

                Yii::$app->getSession()->setFlash('error', [
                    'error' => Yii::t('backend', "System error happen. Please contact with System manager.")
                ]);
                return $this->redirect(['index']);
            }
            //set error message
            Yii::$app->getSession()->setFlash('error', [
                'error' => Yii::t('backend', "Sorry can't update seat, please try again.")
            ]);
            //Return index
            return $this->redirect(['index']);
        }
    }

    /**
     * Deletes an existing MasterSeat model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {
        try {
            //create transaction
            $transaction = $this->db->beginTransaction();
            $model = $this->findModel($id);

            if (!$model->checkDeleteBooking($id)) {
                return 'false';
            } else {
                //soft delete tax
                $model->softDelete();
                //commit data
                $transaction->commit();
                //return list tax
                Yii::$app->session->setFlash('success', Yii::t("backend", "Deleted successfully."));
                return $this->redirect(['index'],200);
            }
        } catch (Exception $ex) {//if have exception
            //rollback transaction
            $transaction->rollBack();
            //set error message
            return 'false';
        }
    }

    /**
     * Finds the MasterSeat model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return MasterSeat the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = MasterSeat::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    // Validate From
    public function actionValidate() {

        $model = new MasterSeat();
        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = 'json';
            return \yii\bootstrap\ActiveForm::validate($model);
        }
    }

    // Select staff by id store
    public function actionSelecttypeseatbyidstore() {
        $out = [];
        $result = [];
        if (isset($_POST['depdrop_parents'])) {
            $parents = $_POST['depdrop_parents'];
            if ($parents != null) {
                $store_id = $parents[0];
                if ($store_id == null) {
                    $role = \common\components\FindPermission::getRole();
                    if ($role->permission_id != Constants::STORE_MANAGER_ROLE || $role->permission_id != Constants::STAFF_ROLE) {
                        $store_id_common = (new MasterStore)->findStoreCommon('00000');
                        $out = (new MasterTypeSeat)->listTypeSeat($store_id_common);
                        foreach ($out as $key => $value) {
                            $arr = ['id' => $key, 'name' => $value];
                            $result[] = $arr;
                        }
                        echo Json::encode(['output' => $result, 'selected' => '']);
                        return;
                    } else {
                        echo Json::encode(['output' => $result, 'selected' => '']);
                        return;
                    }
                } else {
                    $out = (new MasterTypeSeat)->listTypeSeat($store_id);
                    foreach ($out as $key => $value) {
                        $arr = ['id' => $key, 'name' => $value];
                        $result[] = $arr;
                    }
                    echo Json::encode(['output' => $result, 'selected' => '']);
                    return;
                }
            }
        }
        echo Json::encode(['output' => '', 'selected' => '']);
    }

}
