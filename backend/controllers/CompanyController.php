<?php

namespace backend\controllers;

use common\models\Company;
use Yii;
use common\components\Constants;

class CompanyController extends \yii\web\Controller {

    public function actionIndex() {
        $model = new Company();

        $model_company = $model->listCompany();
        // Get Role
        $role = \common\components\FindPermission::getRole();
        // Operating Admin
        if ($role->permission_id == Constants::APP_MANAGER_ROLE) {
            return $this->render('index', ['model' => $model_company]);
        } else {
            return $this->render('index', ['model' => $model_company]);
        }
    }

    public function actionCreate() {

        throw new \yii\web\BadRequestHttpException("Don't update orther company");

        $model = new Company();

        //check submit form and validate input data
        if ($model->load(Yii::$app->request->post()) && $model->save()) {

            Yii::$app->session->setFlash('success', Yii::t("backend", "Create Success"));
            return $this->redirect(['index']);
        } else {
            return $this->render('index', ['model' => $model]);
        }
    }

    public function actionUpdate() {

        $model = new Company();
        // Get Role
        $role = \common\components\FindPermission::getRole();

        $model_company = $model->listCompany();

        if ($role->permission_id == Constants::COMPANY_MANAGER_ROLE || $role->permission_id == Constants::APP_MANAGER_ROLE) {

            if ($model_company->load(Yii::$app->request->post())) {
                if ($model_company->save()) {
                    Yii::$app->session->setFlash('success', Yii::t("backend", "Update Success"));
                    return $this->render('index', ['model' => $model_company]);
                } else {
                    Yii::$app->getSession()->setFlash('error', [
                        'error' => Yii::t('backend', "System error happen. Please contact with System manager.")
                    ]);
                    return $this->render('index', ['model' => $model_company]);
                }
            } else {
                return $this->render('index', ['model' => $model_company]);
            }
        } else {
            return $this->render('index', ['model' => $model_company]);
        }
    }

}
