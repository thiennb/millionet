<?php

namespace backend\controllers;

use Yii;
use common\models\MasterStore;
use common\models\StoreSearch;
use yii\web\Controller;
use yii\filters\VerbFilter;
use common\components\Util;
use yii\web\UploadedFile;
use Exception;
use yii\web\NotFoundHttpException;
use \common\models\Checklist;
use \common\models\Question;
use \common\models\QuestionAnswer;
use \common\models\StoreChecklist;
use common\models\CompanyStore;
use common\models\StoreShift;

/**
 * StoreController implements the CRUD actions for Store model.
 */
class StoreController extends Controller {

    //variable connection database
    public $db;

    public function init() {
        //get connection
        $this->db = Yii::$app->db;
    }

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Store models.
     * @return mixed
     */
    public function actionIndex() {
        $searchModel = new StoreSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Store model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id) {
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Store model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        \common\components\FindPermission::permissionBeforeAction(\common\components\FindPermission::getRole()->permission_id == \common\components\Constants::APP_MANAGER_ROLE || \common\components\FindPermission::getRole()->permission_id == \common\components\Constants::COMPANY_MANAGER_ROLE);
        try {
            // Get Information Store 
            $model = new MasterStore();

            // Set Defaul Radio
            $model->show_flg = 0;
            $model->booking_resources = "01";
            $model->show_in_booking_flg = 1;
            $model->show_in_notice_flg = 1;
            $model->show_in_coupon_flg = 1;
            $model->show_in_product_category_flg = 1;
            $model->show_in_my_page_flg = 1;
            $model->show_in_staff_flg = 1;
            // End Set Defaul Radio 
            // Get Store code Max
            $ModelStore = new MasterStore();
            $code_store_max = $ModelStore->generateStoreCode();
            $model->store_code = $code_store_max;
            $model->company_id = Util::getCookiesCompanyId();
            // Create Translate 
            $transaction = $this->db->beginTransaction();

            $check_list = null;

            if ($model->load(Yii::$app->request->post()) && $model->validate()) {

                $Array_Question = (array) json_decode(Yii::$app->request->post('Question_Answer'));

                $model->tmp_image1 = UploadedFile::getInstance($model, 'tmp_image1');
                $model->tmp_image2 = UploadedFile::getInstance($model, 'tmp_image2');
                $model->tmp_image3 = UploadedFile::getInstance($model, 'tmp_image3');
                $model->tmp_image4 = UploadedFile::getInstance($model, 'tmp_image4');
                $model->tmp_image5 = UploadedFile::getInstance($model, 'tmp_image5');
                $model->tmp_app_logo_img = UploadedFile::getInstance($model, 'tmp_app_logo_img');
                $model->tmp_receipt_logo_img = UploadedFile::getInstance($model, 'tmp_receipt_logo_img');
                $model->tmp_gold_ticket_image = UploadedFile::getInstance($model, 'tmp_gold_ticket_image');
                $model->tmp_icon_1 = UploadedFile::getInstance($model, 'tmp_icon_1');
                $model->tmp_icon_2 = UploadedFile::getInstance($model, 'tmp_icon_2');
                $model->tmp_icon_3 = UploadedFile::getInstance($model, 'tmp_icon_3');

                // ***************** //
                // Render Name Image //
                // ***************** //
                if ($model->tmp_image1) {
                    $model->image1 = Yii::$app->security->generateRandomString() . '.' . $model->tmp_image1->extension;
                }
                if ($model->tmp_image2) {
                    $model->image2 = Yii::$app->security->generateRandomString() . '.' . $model->tmp_image2->extension;
                }
                if ($model->tmp_image3) {
                    $model->image3 = Yii::$app->security->generateRandomString() . '.' . $model->tmp_image3->extension;
                }
                if ($model->tmp_image4) {
                    $model->image4 = Yii::$app->security->generateRandomString() . '.' . $model->tmp_image4->extension;
                }
                if ($model->tmp_image5) {
                    $model->image5 = Yii::$app->security->generateRandomString() . '.' . $model->tmp_image5->extension;
                }
                if ($model->tmp_icon_1) {
                    $model->link_icon_1 = Yii::$app->security->generateRandomString() . '.' . $model->tmp_icon_1->extension;
                }
                if ($model->tmp_icon_2) {
                    $model->link_icon_2 = Yii::$app->security->generateRandomString() . '.' . $model->tmp_icon_2->extension;
                }
                if ($model->tmp_icon_3) {
                    $model->link_icon_3 = Yii::$app->security->generateRandomString() . '.' . $model->tmp_icon_3->extension;
                }

                if ($model->tmp_app_logo_img) {
                    $model->app_logo_img = Yii::$app->security->generateRandomString() . '.' . $model->tmp_app_logo_img->extension;
                }
                if ($model->tmp_receipt_logo_img) {
                    $model->receipt_logo_img = Yii::$app->security->generateRandomString() . '.' . $model->tmp_receipt_logo_img->extension;
                }
                if ($model->tmp_gold_ticket_image) {
                    $model->gold_ticket_image = Yii::$app->security->generateRandomString() . '.' . $model->tmp_gold_ticket_image->extension;
                }
                // ********************* //
                // Render Name Image End //
                // ********************* //  
                if ($model->expiration_date_flg == 1 && ( $model->supply_year == null || $model->supply_year == 0)) {
                    $model->expiration_date_flg = 0;
                }

                if ($model->save()) {
                    // ****************** //
                    // Upload Image Start //
                    // ****************** //                
                    if (!empty($model->image1)) {
                        // Util::deleteFile($old_store_avatar);
                        Util::uploadFile($model->tmp_image1, $model->image1);
                    }

                    if (!empty($model->image2)) {
                        // Util::deleteFile($old_store_image1);
                        Util::uploadFile($model->tmp_image2, $model->image2);
                    }

                    if (!empty($model->image3)) {
                        //Util::deleteFile($old_store_image2);
                        Util::uploadFile($model->tmp_image3, $model->image3);
                    }

                    if (!empty($model->image4)) {
                        // Util::deleteFile($old_store_avatar);
                        Util::uploadFile($model->tmp_image4, $model->image4);
                    }
                    if (!empty($model->image5)) {
                        // Util::deleteFile($old_store_image1);
                        Util::uploadFile($model->tmp_image5, $model->image5);
                    }
                    if (!empty($model->tmp_icon_1)) {
                        //Util::deleteFile($old_store_image2);
                        Util::uploadFile($model->tmp_icon_1, $model->link_icon_1);
                    }

                    if (!empty($model->tmp_icon_2)) {
                        //Util::deleteFile($old_store_image2);
                        Util::uploadFile($model->tmp_icon_2, $model->link_icon_2);
                    }

                    if (!empty($model->tmp_icon_3)) {
                        //Util::deleteFile($old_store_image2);
                        Util::uploadFile($model->tmp_icon_3, $model->link_icon_3);
                    }

                    if (!empty($model->tmp_app_logo_img)) {
                        //Util::deleteFile($old_store_image2);
                        Util::uploadFile($model->tmp_app_logo_img, $model->app_logo_img);
                    }

                    if (!empty($model->tmp_receipt_logo_img)) {
                        //Util::deleteFile($old_store_image2);
                        Util::uploadFile($model->tmp_receipt_logo_img, $model->receipt_logo_img);
                    }
                    if (!empty($model->tmp_gold_ticket_image)) {
                        //Util::deleteFile($old_store_image2);
                        Util::uploadFile($model->tmp_gold_ticket_image, $model->gold_ticket_image);
                    }

                    // Insert Check List

                    if (!empty($Array_Question)) {
                        // Create model

                        if (count($Array_Question) >= 2) {
                            // var_dump(count($Array_Question));
                            for ($i = 1; $i < count($Array_Question); $i++) {
                                $arr = (array) $Array_Question[$i];
                                if ($arr != null) {
                                    $value_type_question = $arr['type_question'];
                                    $value_display_flg = $arr['display_flg'];
                                    $store_id = $model->id;
                                    $value_notice_content = '';
                                    if ($value_type_question == '1') {
                                        $value_notice_content = $arr['notice_content'];
                                        // =====================
                                        // Start Insert Question
                                        // =====================
                                        $model_question = new Question();
                                        $model_question->notice_content = $value_notice_content;
                                        $model_question->save();
                                        // =====================
                                        // End Insert Question
                                        // =====================
                                        //====================
                                        // Start Insert Check List
                                        //====================
                                        $model_check_list = new Checklist();
                                        $model_check_list->type = $value_type_question;
                                        $model_check_list->display_flg = $value_display_flg;
                                        $model_check_list->question_id = $model_question->id;
                                        $model_check_list->save();
                                        //====================
                                        // End Insert Check List
                                        //====================
                                        //====================
                                        // Start Insert Store Check List
                                        //====================
                                        $model_store_checklist = new StoreChecklist();
                                        $model_store_checklist->store_id = $store_id;
                                        $model_store_checklist->checklist_id = $model_check_list->id;
                                        $model_store_checklist->save();
                                        //====================
                                        // Start Insert Store Check List
                                        //====================
                                    } else if ($value_type_question == '2') {

                                        $value_option = $arr['option'];


                                        if ($value_option == 1) {
                                            $value_question_content = $arr['question_content'];
                                            // =====================
                                            // Start Insert Question
                                            // =====================
                                            $model_question = new Question();
                                            $model_question->option = $value_option;
                                            $model_question->question_content = $value_question_content;
                                            $model_question->save();
                                            // =====================
                                            // End Insert Question
                                            // =====================
                                            // =====================
                                            // Start Insert Question Answer
                                            // =====================
                                            $value_answer = $arr['answer'];
                                            $answer_array = $value_answer;
                                            $count_answer = count($answer_array);

                                            if ($count_answer == 1 && $answer_array[0] != 'no_data') {
                                                $model_question_answer = new QuestionAnswer();
                                                $model_question_answer->content = $answer_array[0];
                                                $model_question_answer->question_id = $model_question->id;
                                                $model_question_answer->save();
                                            } else {
                                                for ($k = 0; $k < $count_answer; $k++) {
                                                    if ($answer_array[$k] != 'no_data') {
                                                        $model_question_answer = new QuestionAnswer();
                                                        $model_question_answer->content = $answer_array[$k];
                                                        $model_question_answer->question_id = $model_question->id;
                                                        $model_question_answer->save();
                                                    }
                                                }
                                            }

                                            // =====================
                                            // End Insert Question
                                            // =====================
                                            //====================
                                            // Start Insert Check List
                                            //====================
                                            $model_check_list = new Checklist();
                                            $model_check_list->type = $value_type_question;
                                            $model_check_list->display_flg = $value_display_flg;
                                            $model_check_list->question_id = $model_question->id;
                                            $model_check_list->save();
                                            //====================
                                            // End Insert Check List
                                            //====================
                                            //====================
                                            // Start Insert Store Check List
                                            //====================
                                            $model_store_checklist = new StoreChecklist();
                                            $model_store_checklist->store_id = $store_id;
                                            $model_store_checklist->checklist_id = $model_check_list->id;
                                            $model_store_checklist->save();
                                            //====================
                                            // Start Insert Store Check List
                                            //====================
                                        } else if ($value_option == 2) {
                                            $value_question_content = $arr['question_content'];
                                            // =====================
                                            // Start Insert Question
                                            // =====================
                                            $model_question = new Question();
                                            $value_count_asw = $arr['CountAsw'];
                                            $model_question->option = $value_option;
                                            $model_question->question_content = $value_question_content;
                                            $model_question->max_choice = $value_count_asw;
                                            $model_question->save();
                                            // =====================
                                            // End Insert Question
                                            // =====================
                                            // =====================
                                            // Start Insert Question Answer
                                            // =====================
                                            $value_answer = $arr['answer'];
                                            $answer_array = $value_answer;
                                            $count_answer = count($answer_array);

                                            if ($count_answer == 1 && $answer_array[0] != 'no_data') {
                                                $model_question_answer = new QuestionAnswer();
                                                $model_question_answer->content = $answer_array[0];
                                                $model_question_answer->question_id = $model_question->id;
                                                $model_question_answer->save();
                                            } else {
                                                for ($j = 0; $j < $count_answer; $j++) {
                                                    if ($answer_array[$j] != 'no_data') {
                                                        $model_question_answer = new QuestionAnswer();
                                                        $model_question_answer->content = $answer_array[$j];
                                                        $model_question_answer->question_id = $model_question->id;
                                                        $model_question_answer->save();
                                                    }
                                                }
                                            }

                                            // =====================
                                            // End Insert Question
                                            // =====================
                                            //====================
                                            // Start Insert Check List
                                            //====================
                                            $model_check_list = new Checklist();
                                            $model_check_list->type = $value_type_question;
                                            $model_check_list->display_flg = $value_display_flg;
                                            $model_check_list->question_id = $model_question->id;
                                            $model_check_list->save();
                                            //====================
                                            // End Insert Check List
                                            //====================
                                            //====================
                                            // Start Insert Store Check List
                                            //====================
                                            $model_store_checklist = new StoreChecklist();
                                            $model_store_checklist->store_id = $store_id;
                                            $model_store_checklist->checklist_id = $model_check_list->id;
                                            $model_store_checklist->save();
                                            //====================
                                            // Start Insert Store Check List
                                            //====================
                                        } else if ($value_option == 3) {
                                            $value_question_content = $arr['question_content'];
                                            // =====================
                                            // Start Insert Question
                                            // =====================
                                            $model_question = new Question();
                                            $model_question->option = $value_option;
                                            $model_question->question_content = $value_question_content;
                                            $model_question->save();
                                            // =====================
                                            // End Insert Question
                                            // =====================
                                            //====================
                                            // Start Insert Check List
                                            //====================
                                            $model_check_list = new Checklist();
                                            $model_check_list->type = $value_type_question;
                                            $model_check_list->display_flg = $value_display_flg;
                                            $model_check_list->question_id = $model_question->id;
                                            $model_check_list->save();
                                            //====================
                                            // End Insert Check List
                                            //====================
                                            //====================
                                            // Start Insert Store Check List
                                            //====================
                                            $model_store_checklist = new StoreChecklist();
                                            $model_store_checklist->store_id = $store_id;
                                            $model_store_checklist->checklist_id = $model_check_list->id;
                                            $model_store_checklist->save();
                                            //====================
                                            // Start Insert Store Check List
                                            //====================
                                        }
                                    }
                                }
                            }
                        }
                    }

                    // ****************** //
                    // Upload Image End //
                    // ****************** //
                    // Get company_code from session
                    // $company_code = Util::getCookiesCompanyCode();
                    //
                    // $model_company_store = new CompanyStore();
                    // $model_company_store->store_id = $model->id;
                    // $model_company_store->company_code = $company_code;
                    // $model_company_store->save();
                    // make defause shift for store 
                    $store_shift = new \common\models\StoreShift();
                    $store_shift->store_id = $model->id;
                    $store_shift->start_time = $model->time_open;
                    $store_shift->end_time = $model->time_close;
                    $store_shift->name = Yii::t('backend', 'All day');
                    $store_shift->short_name = Yii::t('backend', 'All day');
                    $store_shift->save();

                    // Commit Transaction                    
                    $transaction->commit();
                    // Return view Index
                    Yii::$app->session->setFlash('success', Yii::t("backend", "Create Success"));
                    return $this->redirect(['index']);
                } else {
                    //rollback transaction
                    $transaction->rollBack();
                    //set error message
                    Yii::$app->getSession()->setFlash('error', [
                        'error' => Yii::t('backend', "System error happen. Please contact with System manager.")
                    ]);
                    // Return view create
                    return $this->render('create', [
                                'model' => $model,
                                'check_list' => $check_list
                    ]);
                }
            } else {
                $model->gold_ticket_supply_flg = '0';
                $model->expiration_date_flg = '0';

                return $this->render('create', [
                            'model' => $model,
                            'check_list' => $check_list
                ]);
            }
        } catch (Exception $ex) {
            //rollback transaction
            $transaction->rollBack();
            //set error message
            Yii::$app->getSession()->setFlash('error', [
                'error' => Yii::t('backend', "System error happen. Please contact with System manager.")
            ]);
            //return index
            return $this->render('create', [
                        'model' => $model,
                        'check_list' => $check_list
            ]);
        }
    }

    /**
     * Updates an existing Store model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
        try {
            // Search Information Store 
            $model = $this->findModel($id);

            //create transaction
            $transaction = $this->db->beginTransaction();

            $check_list = $model->selectCheckListStore($id);

            if ($model->load(Yii::$app->request->post())) {

                // Get value check list
                $Array_Question = (array) json_decode(Yii::$app->request->post('Question_Answer'));

                // ******************** //
                // Check Delete Image   //
                // Image 1 > 5 ******** //
                if (empty($model->hidden_image1) && !empty($model->image1)) {
                    $model->hidden_image1 = $model->image1;
                    $model->image1 = '';
                }

                if (empty($model->hidden_image2) && !empty($model->image2)) {
                    $model->hidden_image2 = $model->image2;
                    $model->image2 = '';
                }

                if (empty($model->hidden_image3) && !empty($model->image3)) {
                    $model->hidden_image3 = $model->image3;
                    $model->image3 = '';
                }

                if (empty($model->hidden_image4) && !empty($model->image4)) {
                    $model->hidden_image4 = $model->image4;
                    $model->image4 = '';
                }

                if (empty($model->hidden_image5) && !empty($model->image5)) {
                    $model->hidden_image5 = $model->image5;
                    $model->image5 = '';
                }
                // End Image 1 > 5
                // Image hidden_icon_1 -> hidden_icon_3

                if (empty($model->hidden_icon_1) && !empty($model->link_icon_1)) {
                    $model->hidden_icon_1 = $model->link_icon_1;
                    $model->link_icon_1 = '';
                }

                if (empty($model->hidden_icon_2) && !empty($model->link_icon_2)) {
                    $model->hidden_icon_2 = $model->link_icon_2;
                    $model->link_icon_2 = '';
                }

                if (empty($model->hidden_icon_3) && !empty($model->link_icon_3)) {
                    $model->hidden_icon_3 = $model->link_icon_3;
                    $model->link_icon_3 = '';
                }

                if (empty($model->hidden_app_logo_img) && !empty($model->app_logo_img)) {
                    $model->hidden_app_logo_img = $model->app_logo_img;
                    $model->app_logo_img = '';
                }

                if (empty($model->hidden_receipt_logo_img) && !empty($model->receipt_logo_img)) {
                    $model->hidden_receipt_logo_img = $model->receipt_logo_img;
                    $model->receipt_logo_img = '';
                }

                if (empty($model->hidden_gold_ticket_image) && !empty($model->gold_ticket_image)) {
                    $model->hidden_gold_ticket_image = $model->gold_ticket_image;
                    $model->gold_ticket_image = '';
                }

                // ********************* //
                // End Check Delete Image //
                // ********************* //
                // Get Imange
                $model->tmp_image1 = UploadedFile::getInstance($model, 'tmp_image1');
                $model->tmp_image2 = UploadedFile::getInstance($model, 'tmp_image2');
                $model->tmp_image3 = UploadedFile::getInstance($model, 'tmp_image3');
                $model->tmp_image4 = UploadedFile::getInstance($model, 'tmp_image4');
                $model->tmp_image5 = UploadedFile::getInstance($model, 'tmp_image5');
                $model->tmp_icon_1 = UploadedFile::getInstance($model, 'tmp_icon_1');
                $model->tmp_icon_2 = UploadedFile::getInstance($model, 'tmp_icon_2');
                $model->tmp_icon_3 = UploadedFile::getInstance($model, 'tmp_icon_3');
                $model->tmp_app_logo_img = UploadedFile::getInstance($model, 'tmp_app_logo_img');
                $model->tmp_receipt_logo_img = UploadedFile::getInstance($model, 'tmp_receipt_logo_img');
                $model->tmp_gold_ticket_image = UploadedFile::getInstance($model, 'tmp_gold_ticket_image');
                // End Get Imange
                // ****************** //
                // Check Image 1 Data //
                // ****************** //
                if ($model->tmp_image1) {
                    $model->image1 = Yii::$app->security->generateRandomString() . '.' . $model->tmp_image1->extension;
                }
                if ($model->tmp_image2) {
                    $model->image2 = Yii::$app->security->generateRandomString() . '.' . $model->tmp_image2->extension;
                }
                if ($model->tmp_image3) {
                    $model->image3 = Yii::$app->security->generateRandomString() . '.' . $model->tmp_image3->extension;
                }
                if ($model->tmp_image4) {
                    $model->image4 = Yii::$app->security->generateRandomString() . '.' . $model->tmp_image4->extension;
                }
                if ($model->tmp_image5) {
                    $model->image5 = Yii::$app->security->generateRandomString() . '.' . $model->tmp_image5->extension;
                }
                if ($model->tmp_icon_1) {
                    $model->link_icon_1 = Yii::$app->security->generateRandomString() . '.' . $model->tmp_icon_1->extension;
                }
                if ($model->tmp_icon_2) {
                    $model->link_icon_2 = Yii::$app->security->generateRandomString() . '.' . $model->tmp_icon_2->extension;
                }
                if ($model->tmp_icon_3) {
                    $model->link_icon_3 = Yii::$app->security->generateRandomString() . '.' . $model->tmp_icon_3->extension;
                }

                if ($model->tmp_app_logo_img) {
                    $model->app_logo_img = Yii::$app->security->generateRandomString() . '.' . $model->tmp_app_logo_img->extension;
                }

                if ($model->tmp_receipt_logo_img) {
                    $model->receipt_logo_img = Yii::$app->security->generateRandomString() . '.' . $model->tmp_receipt_logo_img->extension;
                }

                if ($model->tmp_gold_ticket_image) {
                    $model->gold_ticket_image = Yii::$app->security->generateRandomString() . '.' . $model->tmp_gold_ticket_image->extension;
                }
                // ********************* //
                // Render Name Image End //
                // ********************* //
//                var_dump($model);
//                exit();

                if ($model->expiration_date_flg == 1 && ( $model->supply_year == null || $model->supply_year == 0)) {
                    $model->expiration_date_flg = 0;
                }

                if ($model->save()) {
                    // ****************** //
                    // Upload Image Start //
                    // ****************** //                
                    if (!empty($model->image1)) {
                        // Util::deleteFile($old_store_avatar);
                        Util::uploadFile($model->tmp_image1, $model->image1);
                    }

                    if (!empty($model->image2)) {
                        // Util::deleteFile($old_store_image1);
                        Util::uploadFile($model->tmp_image2, $model->image2);
                    }

                    if (!empty($model->image3)) {
                        //Util::deleteFile($old_store_image2);
                        Util::uploadFile($model->tmp_image3, $model->image3);
                    }

                    if (!empty($model->image4)) {
                        // Util::deleteFile($old_store_avatar);
                        Util::uploadFile($model->tmp_image4, $model->image4);
                    }
                    if (!empty($model->image5)) {
                        // Util::deleteFile($old_store_image1);
                        Util::uploadFile($model->tmp_image5, $model->image5);
                    }
                    if (!empty($model->tmp_icon_1)) {
                        //Util::deleteFile($old_store_image2);
                        Util::uploadFile($model->tmp_icon_1, $model->link_icon_1);
                    }

                    if (!empty($model->tmp_icon_2)) {
                        //Util::deleteFile($old_store_image2);
                        Util::uploadFile($model->tmp_icon_2, $model->link_icon_2);
                    }

                    if (!empty($model->tmp_icon_3)) {
                        //Util::deleteFile($old_store_image2);
                        Util::uploadFile($model->tmp_icon_3, $model->link_icon_3);
                    }

                    if (!empty($model->tmp_app_logo_img)) {
                        //Util::deleteFile($old_store_image2);
                        Util::uploadFile($model->tmp_app_logo_img, $model->app_logo_img);
                    }

                    if (!empty($model->tmp_receipt_logo_img)) {
                        //Util::deleteFile($old_store_image2);
                        Util::uploadFile($model->tmp_receipt_logo_img, $model->receipt_logo_img);
                    }

                    if (!empty($model->tmp_gold_ticket_image)) {
                        //Util::deleteFile($old_store_image2);
                        Util::uploadFile($model->tmp_gold_ticket_image, $model->gold_ticket_image);
                    }
                    // ****************** //
                    // Upload Image End //
                    // ****************** //
                    // ***************** //
                    //  Delete Image     //
                    // ***************** //
                    // Image 1 -> Image 5
                    if (!empty($model->hidden_image1) && empty($model->image1)) {
                        Util::deleteFile($model->hidden_image1);
                    }

                    if (!empty($model->hidden_image2) && empty($model->image2)) {
                        Util::deleteFile($model->hidden_image2);
                    }

                    if (!empty($model->hidden_image3) && empty($model->image3)) {
                        Util::deleteFile($model->hidden_image3);
                    }

                    if (!empty($model->hidden_image4) && empty($model->image4)) {
                        Util::deleteFile($model->hidden_image4);
                    }

                    if (!empty($model->hidden_image5) && empty($model->image5)) {
                        Util::deleteFile($model->hidden_image5);
                    }
                    // 
                    // End Image 1 -> Image 5
                    // Image hidden_icon_1 -> hidden_icon_3
                    if (empty($model->hidden_icon_1) && !empty($model->link_icon_1)) {
                        Util::deleteFile($model->hidden_icon_1);
                    }

                    if (empty($model->hidden_icon_2) && !empty($model->link_icon_2)) {
                        Util::deleteFile($model->hidden_icon_2);
                    }

                    if (empty($model->hidden_icon_3) && !empty($model->link_icon_3)) {
                        Util::deleteFile($model->hidden_icon_3);
                    }

                    if (empty($model->hidden_app_logo_img) && !empty($model->app_logo_img)) {
                        Util::deleteFile($model->hidden_app_logo_img);
                    }

                    if (empty($model->hidden_receipt_logo_img) && !empty($model->receipt_logo_img)) {
                        Util::deleteFile($model->hidden_receipt_logo_img);
                    }

                    if (empty($model->hidden_gold_ticket_image) && !empty($model->gold_ticket_image)) {
                        Util::deleteFile($model->hidden_gold_ticket_image);
                    }
                    // End Image hidden_icon_1 -> hidden_icon_3                 
                    // ***************** //
                    //  End Delete Image //
                    // ***************** //
                    // Insert Check List
//                    var_dump($Array_Question);
//                    exit();
                    if (!empty($Array_Question)) {
                        // Create model

                        if (count($Array_Question) >= 2) {
                            for ($i = 1; $i < count($Array_Question); $i++) {
                                $arr = (array) $Array_Question[$i];
                                if ($arr != null) {
                                    $value_type_question = $arr['type_question'];
                                    $value_display_flg = $arr['display_flg'];
                                    $store_id = $model->id;
                                    $value_notice_content = '';

                                    if ($arr['id_checklist'] != '0' && $arr['id_question'] != '0') {

                                        if ($arr['id_option_db'] != $arr['option'] && !empty($arr['id_option_db'])) {

                                            if ($arr['type_question'] == '1') {
                                                //======================
                                                // Start Update Check List
                                                //======================
                                                $id_checklist = $arr['id_checklist'];
                                                $model_check_list = $this->findModelCheckList($id_checklist);
                                                $model_check_list->del_flg = '1';
                                                $model_check_list->save();

                                                $value_notice_content = $arr['notice_content'];
                                                // =====================
                                                // Start Insert Question
                                                // =====================
                                                if ($value_notice_content != 'no_data') {
                                                    $model_question = new Question();
                                                    $model_question->notice_content = $value_notice_content;
                                                    $model_question->save();
                                                    // =====================
                                                    // End Insert Question
                                                    // =====================
                                                    //====================
                                                    // Start Insert Check List
                                                    //====================
                                                    $model_check_list = new Checklist();
                                                    $model_check_list->type = $value_type_question;
                                                    $model_check_list->display_flg = $value_display_flg;
                                                    $model_check_list->question_id = $model_question->id;
                                                    $model_check_list->save();
                                                    //====================
                                                    // End Insert Check List
                                                    //====================
                                                    //====================
                                                    // Start Insert Store Check List
                                                    //====================
                                                    $model_store_checklist = new StoreChecklist();
                                                    $model_store_checklist->store_id = $store_id;
                                                    $model_store_checklist->checklist_id = $model_check_list->id;
                                                    $model_store_checklist->save();
                                                    //====================
                                                    // Start Insert Store Check List
                                                    //====================  
                                                }
                                            } else if ($arr['type_question'] == '2') {

                                                $id_checklist = $arr['id_checklist'];
                                                $model_check_list = $this->findModelCheckList($id_checklist);
                                                $model_check_list->del_flg = '1';
                                                $model_check_list->save();

                                                $value_option = $arr['option'];

                                                if ($value_option == 1) {
                                                    $value_question_content = $arr['question_content'];
                                                    // =====================
                                                    // Start Insert Question
                                                    // =====================
                                                    $model_question = new Question();
                                                    $model_question->option = $value_option;
                                                    $model_question->question_content = $value_question_content;
                                                    $model_question->save();
                                                    // =====================
                                                    // End Insert Question
                                                    // =====================
                                                    // =====================
                                                    // Start Insert Question Answer
                                                    // =====================
                                                    $value_answer = $arr['answer'];
                                                    $answer_array = $value_answer;
                                                    $count_answer = count($answer_array);

                                                    if ($count_answer == 1 && $answer_array[0] != 'no_data') {
                                                        $model_question_answer = new QuestionAnswer();
                                                        $model_question_answer->content = $answer_array[0];
                                                        $model_question_answer->question_id = $model_question->id;
                                                        $model_question_answer->save();
                                                    } else {
                                                        for ($k = 0; $k < $count_answer; $k++) {
                                                            if ($answer_array[$k] != 'no_data') {
                                                                $model_question_answer = new QuestionAnswer();
                                                                $model_question_answer->content = $answer_array[$k];
                                                                $model_question_answer->question_id = $model_question->id;
                                                                $model_question_answer->save();
                                                            }
                                                        }
                                                    }

                                                    // =====================
                                                    // End Insert Question
                                                    // =====================
                                                    //====================
                                                    // Start Insert Check List
                                                    //====================
                                                    $model_check_list = new Checklist();
                                                    $model_check_list->type = $value_type_question;
                                                    $model_check_list->display_flg = $value_display_flg;
                                                    $model_check_list->question_id = $model_question->id;
                                                    $model_check_list->save();
                                                    //====================
                                                    // End Insert Check List
                                                    //====================
                                                    //====================
                                                    // Start Insert Store Check List
                                                    //====================
                                                    $model_store_checklist = new StoreChecklist();
                                                    $model_store_checklist->store_id = $store_id;
                                                    $model_store_checklist->checklist_id = $model_check_list->id;
                                                    $model_store_checklist->save();
                                                    //====================
                                                    // Start Insert Store Check List
                                                    //====================
                                                } else if ($value_option == 2) {
                                                    $value_question_content = $arr['question_content'];
                                                    // =====================
                                                    // Start Insert Question
                                                    // =====================
                                                    $model_question = new Question();
                                                    $value_count_asw = $arr['CountAsw'];
                                                    $model_question->option = $value_option;
                                                    $model_question->question_content = $value_question_content;
                                                    $model_question->max_choice = $value_count_asw;
                                                    $model_question->save();
                                                    // =====================
                                                    // End Insert Question
                                                    // =====================
                                                    // =====================
                                                    // Start Insert Question Answer
                                                    // =====================
                                                    $value_answer = $arr['answer'];
                                                    $answer_array = $value_answer;
                                                    $count_answer = count($answer_array);

                                                    if ($count_answer == 1 && $answer_array[0] != 'no_data') {
                                                        $model_question_answer = new QuestionAnswer();
                                                        $model_question_answer->content = $answer_array[0];
                                                        $model_question_answer->question_id = $model_question->id;
                                                        $model_question_answer->save();
                                                    } else {
                                                        for ($j = 0; $j < $count_answer; $j++) {
                                                            if ($answer_array[$j] != 'no_data') {
                                                                $model_question_answer = new QuestionAnswer();
                                                                $model_question_answer->content = $answer_array[$j];
                                                                $model_question_answer->question_id = $model_question->id;
                                                                $model_question_answer->save();
                                                            }
                                                        }
                                                    }

                                                    // =====================
                                                    // End Insert Question
                                                    // =====================
                                                    //====================
                                                    // Start Insert Check List
                                                    //====================
                                                    $model_check_list = new Checklist();
                                                    $model_check_list->type = $value_type_question;
                                                    $model_check_list->display_flg = $value_display_flg;
                                                    $model_check_list->question_id = $model_question->id;
                                                    $model_check_list->save();
                                                    //====================
                                                    // End Insert Check List
                                                    //====================
                                                    //====================
                                                    // Start Insert Store Check List
                                                    //====================
                                                    $model_store_checklist = new StoreChecklist();
                                                    $model_store_checklist->store_id = $store_id;
                                                    $model_store_checklist->checklist_id = $model_check_list->id;
                                                    $model_store_checklist->save();
                                                    //====================
                                                    // Start Insert Store Check List
                                                    //====================
                                                } else if ($value_option == 3) {
                                                    $value_question_content = $arr['question_content'];
                                                    // =====================
                                                    // Start Insert Question
                                                    // =====================
                                                    $model_question = new Question();
                                                    $model_question->option = $value_option;
                                                    $model_question->question_content = $value_question_content;
                                                    $model_question->save();
                                                    // =====================
                                                    // End Insert Question
                                                    // =====================
                                                    //====================
                                                    // Start Insert Check List
                                                    //====================
                                                    $model_check_list = new Checklist();
                                                    $model_check_list->type = $value_type_question;
                                                    $model_check_list->display_flg = $value_display_flg;
                                                    $model_check_list->question_id = $model_question->id;
                                                    $model_check_list->save();
                                                    //====================
                                                    // End Insert Check List
                                                    //====================
                                                    //====================
                                                    // Start Insert Store Check List
                                                    //====================
                                                    $model_store_checklist = new StoreChecklist();
                                                    $model_store_checklist->store_id = $store_id;
                                                    $model_store_checklist->checklist_id = $model_check_list->id;
                                                    $model_store_checklist->save();
                                                    //====================
                                                    // Start Insert Store Check List
                                                    //====================
                                                }
                                            }
                                        } else if ($arr['id_type_db'] != $arr['type_question']) {
                                            if ($arr['type_question'] == '1') {
                                                //======================
                                                // Start Update Check List
                                                //======================
                                                $id_checklist = $arr['id_checklist'];
                                                $model_check_list = $this->findModelCheckList($id_checklist);
                                                $model_check_list->del_flg = '1';
                                                $model_check_list->save();

                                                $value_notice_content = $arr['notice_content'];
                                                // =====================
                                                // Start Insert Question
                                                // =====================
                                                $model_question = new Question();
                                                $model_question->notice_content = $value_notice_content;
                                                $model_question->save();
                                                // =====================
                                                // End Insert Question
                                                // =====================
                                                //====================
                                                // Start Insert Check List
                                                //====================
                                                $model_check_list = new Checklist();
                                                $model_check_list->type = $value_type_question;
                                                $model_check_list->display_flg = $value_display_flg;
                                                $model_check_list->question_id = $model_question->id;
                                                $model_check_list->save();
                                                //====================
                                                // End Insert Check List
                                                //====================
                                                //====================
                                                // Start Insert Store Check List
                                                //====================
                                                $model_store_checklist = new StoreChecklist();
                                                $model_store_checklist->store_id = $store_id;
                                                $model_store_checklist->checklist_id = $model_check_list->id;
                                                $model_store_checklist->save();
                                                //====================
                                                // Start Insert Store Check List
                                                //====================
                                            } else if ($arr['type_question'] == '2') {

                                                $value_option = $arr['option'];

                                                //======================
                                                // Start Update Check List
                                                //======================
                                                $id_checklist = $arr['id_checklist'];
                                                $model_check_list = $this->findModelCheckList($id_checklist);
                                                $model_check_list->del_flg = '1';
                                                $model_check_list->save();

                                                if ($value_option == 1) {
                                                    $value_question_content = $arr['question_content'];
                                                    // =====================
                                                    // Start Insert Question
                                                    // =====================
                                                    $model_question = new Question();
                                                    $model_question->option = $value_option;
                                                    $model_question->question_content = $value_question_content;
                                                    $model_question->save();
                                                    // =====================
                                                    // End Insert Question
                                                    // =====================
                                                    // =====================
                                                    // Start Insert Question Answer
                                                    // =====================
                                                    $value_answer = $arr['answer'];
                                                    $answer_array = $value_answer;
                                                    $count_answer = count($answer_array);

                                                    if ($count_answer == 1 && $answer_array[0] != 'no_data') {
                                                        $model_question_answer = new QuestionAnswer();
                                                        $model_question_answer->content = $answer_array[0];
                                                        $model_question_answer->question_id = $model_question->id;
                                                        $model_question_answer->save();
                                                    } else {
                                                        for ($k = 0; $k < $count_answer; $k++) {
                                                            if ($answer_array[$k] != 'no_data') {
                                                                $model_question_answer = new QuestionAnswer();
                                                                $model_question_answer->content = $answer_array[$k];
                                                                $model_question_answer->question_id = $model_question->id;
                                                                $model_question_answer->save();
                                                            }
                                                        }
                                                    }

                                                    // =====================
                                                    // End Insert Question
                                                    // =====================
                                                    //====================
                                                    // Start Insert Check List
                                                    //====================
                                                    $model_check_list = new Checklist();
                                                    $model_check_list->type = $value_type_question;
                                                    $model_check_list->display_flg = $value_display_flg;
                                                    $model_check_list->question_id = $model_question->id;
                                                    $model_check_list->save();
                                                    //====================
                                                    // End Insert Check List
                                                    //====================
                                                    //====================
                                                    // Start Insert Store Check List
                                                    //====================
                                                    $model_store_checklist = new StoreChecklist();
                                                    $model_store_checklist->store_id = $store_id;
                                                    $model_store_checklist->checklist_id = $model_check_list->id;
                                                    $model_store_checklist->save();
                                                    //====================
                                                    // Start Insert Store Check List
                                                    //====================
                                                } else if ($value_option == 2) {
                                                    $value_question_content = $arr['question_content'];
                                                    // =====================
                                                    // Start Insert Question
                                                    // =====================
                                                    $model_question = new Question();
                                                    $value_count_asw = $arr['CountAsw'];
                                                    $model_question->option = $value_option;
                                                    $model_question->question_content = $value_question_content;
                                                    $model_question->max_choice = $value_count_asw;
                                                    $model_question->save();
                                                    // =====================
                                                    // End Insert Question
                                                    // =====================
                                                    // =====================
                                                    // Start Insert Question Answer
                                                    // =====================
                                                    $value_answer = $arr['answer'];
                                                    $answer_array = $value_answer;
                                                    $count_answer = count($answer_array);

                                                    if ($count_answer == 1 && $answer_array[0] != 'no_data') {
                                                        $model_question_answer = new QuestionAnswer();
                                                        $model_question_answer->content = $answer_array[0];
                                                        $model_question_answer->question_id = $model_question->id;
                                                        $model_question_answer->save();
                                                    } else {
                                                        for ($j = 0; $j < $count_answer; $j++) {
                                                            if ($answer_array[$j] != 'no_data') {
                                                                $model_question_answer = new QuestionAnswer();
                                                                $model_question_answer->content = $answer_array[$j];
                                                                $model_question_answer->question_id = $model_question->id;
                                                                $model_question_answer->save();
                                                            }
                                                        }
                                                    }

                                                    // =====================
                                                    // End Insert Question
                                                    // =====================
                                                    //====================
                                                    // Start Insert Check List
                                                    //====================
                                                    $model_check_list = new Checklist();
                                                    $model_check_list->type = $value_type_question;
                                                    $model_check_list->display_flg = $value_display_flg;
                                                    $model_check_list->question_id = $model_question->id;
                                                    $model_check_list->save();
                                                    //====================
                                                    // End Insert Check List
                                                    //====================
                                                    //====================
                                                    // Start Insert Store Check List
                                                    //====================
                                                    $model_store_checklist = new StoreChecklist();
                                                    $model_store_checklist->store_id = $store_id;
                                                    $model_store_checklist->checklist_id = $model_check_list->id;
                                                    $model_store_checklist->save();
                                                    //====================
                                                    // Start Insert Store Check List
                                                    //====================
                                                } else if ($value_option == 3) {
                                                    $value_question_content = $arr['question_content'];
                                                    // =====================
                                                    // Start Insert Question
                                                    // =====================
                                                    $model_question = new Question();
                                                    $model_question->option = $value_option;
                                                    $model_question->question_content = $value_question_content;
                                                    $model_question->save();
                                                    // =====================
                                                    // End Insert Question
                                                    // =====================
                                                    //====================
                                                    // Start Insert Check List
                                                    //====================
                                                    $model_check_list = new Checklist();
                                                    $model_check_list->type = $value_type_question;
                                                    $model_check_list->display_flg = $value_display_flg;
                                                    $model_check_list->question_id = $model_question->id;
                                                    $model_check_list->save();
                                                    //====================
                                                    // End Insert Check List
                                                    //====================
                                                    //====================
                                                    // Start Insert Store Check List
                                                    //====================
                                                    $model_store_checklist = new StoreChecklist();
                                                    $model_store_checklist->store_id = $store_id;
                                                    $model_store_checklist->checklist_id = $model_check_list->id;
                                                    $model_store_checklist->save();
                                                    //====================
                                                    // Start Insert Store Check List
                                                    //====================
                                                }
                                            }
                                        } else {

                                            if ($value_type_question == '1') {

                                                if ($arr['notice_content'] == 'no_data') {

                                                    $id_checklist = $arr['id_checklist'];
                                                    $model_check_list = $this->findModelCheckList($id_checklist);
                                                    $model_check_list->del_flg = '1';
                                                    $model_check_list->save();
                                                } else {

                                                    $value_notice_content = $arr['notice_content'];
                                                    $id_question = $arr['id_question'];
                                                    // =====================
                                                    // Start Update Question
                                                    // =====================
                                                    //$model_question = new Question();
                                                    $model_question = $this->findModelQuestion($id_question);
                                                    $model_question->notice_content = $value_notice_content;
                                                    $model_question->save();
                                                    // =====================
                                                    // End Update Question
                                                    // =====================
                                                    //====================
                                                    // Start Update Check List
                                                    //====================
                                                    $id_checklist = $arr['id_checklist'];
                                                    $model_check_list = $this->findModelCheckList($id_checklist);
                                                    $model_check_list->type = $value_type_question;
                                                    $model_check_list->display_flg = $value_display_flg;
                                                    $model_check_list->save();
                                                    //====================
                                                    // End Update Check List
                                                    //==================== 
                                                }
                                            } else {

                                                $value_option = $arr['option'];
                                                if ($value_option == 1) {
                                                    $value_question_content = $arr['question_content'];
                                                    // =====================
                                                    // Start Update Question
                                                    // =====================
                                                    $id_question = $arr['id_question'];
                                                    $model_question = $this->findModelQuestion($id_question);
                                                    $model_question->option = $value_option;
                                                    $model_question->question_content = $value_question_content;
                                                    $model_question->save();
                                                    // =====================
                                                    // End Update Question
                                                    // =====================
                                                    // =====================
                                                    // Start Update Question Answer
                                                    // =====================
                                                    $value_answer = $arr['answer'];
                                                    $value_id_answer = $arr['id_answer'];
                                                    $value_id_question = $arr['id_question'];

                                                    $answer_array = $value_answer;
                                                    $id_answer_array = explode(",", $value_id_answer);

                                                    $count_answer = count($answer_array);
                                                    $count_answer_id = count($id_answer_array);

                                                    // Select id question_answer exit
                                                    if (strlen($value_id_answer) > 0) {

                                                        // Select id question_answer exit
                                                        $id_answer_db = $model->selectIdQuestionAnswer($value_id_question);

                                                        $answer_array_id = explode(",", $value_id_answer);

                                                        $value_answer = $arr['answer'];
                                                        $answer_array = $value_answer;
                                                        $count_answer = count($answer_array);
                                                        $count_answer_id_db = count($id_answer_db);

                                                        $count_answer_id = count($answer_array_id);

                                                        for ($k = 0; $k < $count_answer_id_db; $k++) {
                                                            $model_question_answer = $this->findModelQuestionAswer($answer_array_id[$k]);
                                                            if ($answer_array[$k] == 'no_data') {
                                                                $model_question_answer->del_flg = '1';
                                                                $model_question_answer->save();
                                                            } else {
                                                                $model_question_answer->content = $answer_array[$k];
                                                                $model_question_answer->del_flg = '0';
                                                                $model_question_answer->save();
                                                            }
                                                        }

                                                        if ($count_answer > $count_answer_id_db) {

                                                            $count_insert_new = $count_answer - $count_answer_id_db;

                                                            for ($k = ($count_answer - 1); $k > ($count_answer_id_db - 1); $k--) {
                                                                if ($answer_array[$k] != 'no_data') {
                                                                    $model_question_answer = new QuestionAnswer();
                                                                    $model_question_answer->content = $answer_array[$k];
                                                                    $model_question_answer->question_id = $arr['id_question'];
                                                                    $model_question_answer->save();
                                                                }
                                                            }
                                                        }
                                                    }

                                                    // =====================
                                                    // End Update Question
                                                    // =====================
                                                    //====================
                                                    // Start Update Check List
                                                    //====================
                                                    $id_checklist = $arr['id_checklist'];
                                                    $model_check_list = $this->findModelCheckList($id_checklist);
                                                    $model_check_list->type = $value_type_question;
                                                    $model_check_list->display_flg = $value_display_flg;
                                                    $model_check_list->question_id = $model_question->id;
                                                    $model_check_list->save();
                                                    //====================
                                                    // End Update Check List
                                                    //====================
                                                } else if ($value_option == 2) {

                                                    $value_id_answer = $arr['id_answer'];
                                                    $value_id_question = $arr['id_question'];

                                                    if (strlen($value_id_answer) > 0) {

                                                        // Select id question_answer exit
                                                        $id_answer_db = $model->selectIdQuestionAnswer($value_id_question);

                                                        $answer_array_id = explode(",", $value_id_answer);

                                                        $value_answer = $arr['answer'];
                                                        $answer_array = $value_answer;
                                                        $count_answer = count($answer_array);
                                                        $count_answer_id_db = count($id_answer_db);

                                                        $count_answer_id = count($answer_array_id);

                                                        for ($k = 0; $k < $count_answer_id_db; $k++) {
                                                            $model_question_answer = $this->findModelQuestionAswer($answer_array_id[$k]);
                                                            if ($answer_array[$k] == 'no_data') {
                                                                $model_question_answer->del_flg = '1';
                                                                $model_question_answer->save();
                                                            } else {
                                                                $model_question_answer->content = $answer_array[$k];
                                                                $model_question_answer->del_flg = '0';
                                                                $model_question_answer->save();
                                                            }
                                                        }

                                                        if ($count_answer > $count_answer_id_db) {

                                                            $count_insert_new = $count_answer - $count_answer_id_db;


                                                            for ($k = ($count_answer - 1); $k > ($count_answer_id_db - 1); $k--) {
                                                                if ($answer_array[$k] != 'no_data') {
                                                                    $model_question_answer = new QuestionAnswer();
                                                                    $model_question_answer->content = $answer_array[$k];
                                                                    $model_question_answer->question_id = $arr['id_question'];
                                                                    $model_question_answer->save();
                                                                }
                                                            }
                                                        }
                                                        //======================
                                                        // Start Update Check List
                                                        //======================
                                                        $id_checklist = $arr['id_checklist'];
                                                        $model_check_list = $this->findModelCheckList($id_checklist);
                                                        $model_check_list->type = $value_type_question;
                                                        $model_check_list->display_flg = $value_display_flg;
                                                        $model_check_list->save();
                                                        //====================
                                                        // End Update Check List
                                                        //====================
                                                        // Start Insert Question
                                                        // =====================
                                                        $id_question = $arr['id_question'];
                                                        $model_question = $this->findModelQuestion($id_question);
                                                        $value_count_asw = $arr['CountAsw'];
                                                        $model_question->option = $value_option;
                                                        $model_question->question_content = $arr['question_content'];
                                                        $model_question->max_choice = $value_count_asw;
                                                        $model_question->save();
                                                        // =====================
                                                        // End Insert Question
                                                        // =====================
                                                    }
                                                } else if ($value_option == 3) {

                                                    if ($arr['question_content'] == 'no_data') {
                                                        $id_checklist = $arr['id_checklist'];
                                                        $model_check_list = $this->findModelCheckList($id_checklist);
                                                        $model_check_list->del_flg = '1';
                                                        $model_check_list->save();
                                                    } else {
                                                        $value_question_content = $arr['question_content'];
                                                        // =====================
                                                        // Start Update Question
                                                        // =====================

                                                        $value_id_answer = $arr['id_answer'];

                                                        if (strlen($value_id_answer) > 0) {

                                                            $answer_array_id = explode(",", $value_id_answer);
                                                            $count_answer = count($answer_array_id);

                                                            for ($k = 0; $k < $count_answer; $k++) {
                                                                $model_question_answer = $this->findModelQuestionAswer($answer_array_id[$k]);
                                                                $model_question_answer->del_flg = '1';
                                                                $model_question_answer->save();
                                                            }
                                                        }

                                                        $id_question = $arr['id_question'];
                                                        $model_question = $this->findModelQuestion($id_question);
                                                        $model_question->option = $value_option;
                                                        $model_question->question_content = $value_question_content;
                                                        $model_question->save();
                                                        // =====================
                                                        // End Insert Question
                                                        // =====================
                                                        //======================
                                                        // Start Update Check List
                                                        //======================
                                                        $id_checklist = $arr['id_checklist'];
                                                        $model_check_list = $this->findModelCheckList($id_checklist);
                                                        $model_check_list->type = $value_type_question;
                                                        $model_check_list->display_flg = $value_display_flg;
                                                        $model_check_list->save();
                                                        //====================
                                                        // End Update Check List
                                                        //====================
                                                    }
                                                }
                                            }
                                        }
                                    } else if ($arr['id_checklist'] == '0' && $arr['id_question'] == '0') {
                                        if ($value_type_question == '1') {
                                            $value_notice_content = $arr['notice_content'];
                                            // =====================
                                            // Start Insert Question
                                            // =====================
                                            $model_question = new Question();
                                            $model_question->notice_content = $value_notice_content;
                                            $model_question->save();
                                            // =====================
                                            // End Insert Question
                                            // =====================
                                            //====================
                                            // Start Insert Check List
                                            //====================
                                            $model_check_list = new Checklist();
                                            $model_check_list->type = $value_type_question;
                                            $model_check_list->display_flg = $value_display_flg;
                                            $model_check_list->question_id = $model_question->id;
                                            $model_check_list->save();
                                            //====================
                                            // End Insert Check List
                                            //====================
                                            //====================
                                            // Start Insert Store Check List
                                            //====================
                                            $model_store_checklist = new StoreChecklist();
                                            $model_store_checklist->store_id = $store_id;
                                            $model_store_checklist->checklist_id = $model_check_list->id;
                                            $model_store_checklist->save();
                                            //====================
                                            // Start Insert Store Check List
                                            //====================
                                        } else if ($value_type_question == '2') {

                                            $value_option = $arr['option'];


                                            if ($value_option == 1) {
                                                $value_question_content = $arr['question_content'];
                                                // =====================
                                                // Start Insert Question
                                                // =====================
                                                $model_question = new Question();
                                                $model_question->option = $value_option;
                                                $model_question->question_content = $value_question_content;
                                                $model_question->save();
                                                // =====================
                                                // End Insert Question
                                                // =====================
                                                // =====================
                                                // Start Insert Question Answer
                                                // =====================
                                                $value_answer = $arr['answer'];
                                                $answer_array = $value_answer;
                                                $count_answer = count($answer_array);

                                                if ($count_answer == 1 && $answer_array[0] != 'no_data') {
                                                    $model_question_answer = new QuestionAnswer();
                                                    $model_question_answer->content = $answer_array[0];
                                                    $model_question_answer->question_id = $model_question->id;
                                                    $model_question_answer->save();
                                                } else {
                                                    for ($k = 0; $k < $count_answer; $k++) {
                                                        if ($answer_array[$k] != 'no_data') {
                                                            $model_question_answer = new QuestionAnswer();
                                                            $model_question_answer->content = $answer_array[$k];
                                                            $model_question_answer->question_id = $model_question->id;
                                                            $model_question_answer->save();
                                                        }
                                                    }
                                                }

                                                // =====================
                                                // End Insert Question
                                                // =====================
                                                //====================
                                                // Start Insert Check List
                                                //====================
                                                $model_check_list = new Checklist();
                                                $model_check_list->type = $value_type_question;
                                                $model_check_list->display_flg = $value_display_flg;
                                                $model_check_list->question_id = $model_question->id;
                                                $model_check_list->save();
                                                //====================
                                                // End Insert Check List
                                                //====================
                                                //====================
                                                // Start Insert Store Check List
                                                //====================
                                                $model_store_checklist = new StoreChecklist();
                                                $model_store_checklist->store_id = $store_id;
                                                $model_store_checklist->checklist_id = $model_check_list->id;
                                                $model_store_checklist->save();
                                                //====================
                                                // Start Insert Store Check List
                                                //====================
                                            } else if ($value_option == 2) {
                                                $value_question_content = $arr['question_content'];
                                                // =====================
                                                // Start Insert Question
                                                // =====================
                                                $model_question = new Question();
                                                $value_count_asw = $arr['CountAsw'];
                                                $model_question->option = $value_option;
                                                $model_question->question_content = $value_question_content;
                                                $model_question->max_choice = $value_count_asw;
                                                $model_question->save();
                                                // =====================
                                                // End Insert Question
                                                // =====================
                                                // =====================
                                                // Start Insert Question Answer
                                                // =====================
                                                $value_answer = $arr['answer'];
                                                $answer_array = $value_answer;
                                                $count_answer = count($answer_array);

                                                if ($count_answer == 1 && $answer_array[0] != 'no_data') {
                                                    $model_question_answer = new QuestionAnswer();
                                                    $model_question_answer->content = $answer_array[0];
                                                    $model_question_answer->question_id = $model_question->id;
                                                    $model_question_answer->save();
                                                } else {
                                                    for ($j = 0; $j < $count_answer; $j++) {
                                                        if ($answer_array[$j] != 'no_data') {
                                                            $model_question_answer = new QuestionAnswer();
                                                            $model_question_answer->content = $answer_array[$j];
                                                            $model_question_answer->question_id = $model_question->id;
                                                            $model_question_answer->save();
                                                        }
                                                    }
                                                }

                                                // =====================
                                                // End Insert Question
                                                // =====================
                                                //====================
                                                // Start Insert Check List
                                                //====================
                                                $model_check_list = new Checklist();
                                                $model_check_list->type = $value_type_question;
                                                $model_check_list->display_flg = $value_display_flg;
                                                $model_check_list->question_id = $model_question->id;
                                                $model_check_list->save();
                                                //====================
                                                // End Insert Check List
                                                //====================
                                                //====================
                                                // Start Insert Store Check List
                                                //====================
                                                $model_store_checklist = new StoreChecklist();
                                                $model_store_checklist->store_id = $store_id;
                                                $model_store_checklist->checklist_id = $model_check_list->id;
                                                $model_store_checklist->save();
                                                //====================
                                                // Start Insert Store Check List
                                                //====================
                                            } else if ($value_option == 3) {
                                                $value_question_content = $arr['question_content'];
                                                // =====================
                                                // Start Insert Question
                                                // =====================
                                                $model_question = new Question();
                                                $model_question->option = $value_option;
                                                $model_question->question_content = $value_question_content;
                                                $model_question->save();
                                                // =====================
                                                // End Insert Question
                                                // =====================
                                                //====================
                                                // Start Insert Check List
                                                //====================
                                                $model_check_list = new Checklist();
                                                $model_check_list->type = $value_type_question;
                                                $model_check_list->display_flg = $value_display_flg;
                                                $model_check_list->question_id = $model_question->id;
                                                $model_check_list->save();
                                                //====================
                                                // End Insert Check List
                                                //====================
                                                //====================
                                                // Start Insert Store Check List
                                                //====================
                                                $model_store_checklist = new StoreChecklist();
                                                $model_store_checklist->store_id = $store_id;
                                                $model_store_checklist->checklist_id = $model_check_list->id;
                                                $model_store_checklist->save();
                                                //====================
                                                // Start Insert Store Check List
                                                //====================
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    // make defause shift for store 
                    $store_shift = \common\models\StoreShift::find()->andWhere(['store_id' => $model->id])->one();
                    if ($store_shift) {
                        $store_shift->start_time = $model->time_open;
                        $store_shift->end_time = $model->time_close;
                        $store_shift->save();
                    }
                    //commit transaction
                    $transaction->commit();
                    Yii::$app->session->setFlash('success', Yii::t("backend", "Update Success"));
                    return $this->redirect(['index']);
                } else {
                    // Update Fail Then Roll Back
                    $transaction->rollBack();
                    //set error message
//                    Yii::$app->getSession()->setFlash('error', [
//                        'error' => Yii::t('backend', "System error happen. Please contact with System manager.")
//                    ]);
                    //Return View Update
                    return $this->render('update', [
                                'model' => $model,
                                'check_list' => $check_list
                    ]);
                }
            } else {

                // Set Hiden Image
                $model->hidden_image1 = $model->image1;
                $model->hidden_image2 = $model->image2;
                $model->hidden_image3 = $model->image3;
                $model->hidden_image4 = $model->image4;
                $model->hidden_image5 = $model->image5;
                $model->hidden_icon_1 = $model->link_icon_1;
                $model->hidden_icon_2 = $model->link_icon_2;
                $model->hidden_icon_3 = $model->link_icon_3;
                $model->hidden_app_logo_img = $model->app_logo_img;
                $model->hidden_receipt_logo_img = $model->receipt_logo_img;
                $model->hidden_gold_ticket_image = $model->gold_ticket_image;
                // End Set Hiden Image
                //Return View Update

                return $this->render('update', [
                            'model' => $model,
                            'check_list' => $check_list
                ]);
            }
        } catch (Exception $ex) {
            //rollback transaction
            $transaction->rollBack();

            //Util::dd($ex);
            //set error message
            Yii::$app->getSession()->setFlash('error', [
                'error' => Yii::t('backend', "System error happen. Please contact with System manager.")
            ]);
            Yii::error('File : ' . $ex->getFile() . ' .Line : ' . $ex->getLine() . ' .Message : ' . $ex->getMessage());
            //return index
            return $this->redirect(['index']);
        }
    }

    /**
     * Deletes an existing Store model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {
        try {
            //create transaction
            $transaction = $this->db->beginTransaction();
            $model = $this->findModel($id);
            if (!$model->checkDelete($id)) {
                return 'false';
            } else {
                StoreShift::updateAll(['del_flg' => '1','updated_at' => strtotime("now")],['store_shift.store_id'=> $id]);
                //soft delete tax
                $model->softDelete();
                //commit data
                $transaction->commit();
                Yii::$app->session->setFlash('success', Yii::t("backend", "Delete Success"));
                //return list tax
                return $this->redirect(['index'],200);
            }
        } catch (Exception $ex) {//if have exception
            //rollback transaction
            $transaction->rollBack();
            //set error message
            return 'false';
        }
    }

    /**
     * Finds the Store model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Store the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function findModel($id) {
        if (($model = MasterStore::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function findModelQuestion($id) {
        if (($model = Question::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function findModelCheckList($id) {
        if (($model = Checklist::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function findModelQuestionAswer($id) {
        if (($model = QuestionAnswer::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    // Validate From
    public function actionValidate() {

        $model = new MasterStore();

        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {

            Yii::$app->response->format = 'json';
            return \yii\bootstrap\ActiveForm::validate($model);
        }
    }

    // =========================================================
    // Confirm Booking Start
    // =========================================================
    public function actionConfirmBooking() {
        // Get Information Store 
        $model_check_list = new Checklist();
        $model_question = new Question();
        $model_check_list->display_flg = 0;

        return $this->render('confirm', [
                    'model_check_list' => $model_check_list,
                    'model_question' => $model_question
        ]);
    }

    public function actionConfirmBookingAttention() {
        // Get Information Store 
        $model_check_list = new Checklist();
        $model_question = new Question();

        $model_check_list->display_flg = 0;

        return $this->renderAjax('_attention', [
                    'model_check_list' => $model_check_list,
                    'model_question' => $model_question
        ]);
    }

    public function actionConfirmBookingQuestion() {
        // Get Information Store 
        $model_check_list = new Checklist();
        $model_question = new Question();
        $model_question_answer = new QuestionAnswer;

        $model_check_list->display_flg = 0;

        return $this->renderAjax('_questions', [
                    'model_check_list' => $model_check_list,
                    'model_question' => $model_question,
                    'model_question_answer' => $model_question_answer
        ]);
    }

    protected function actionConfirmBookingAnswer() {

        $model_question_answer = new QuestionAnswer;

        return $this->renderAjax('_answer', [
                    'model_question_answer' => $model_question_answer
        ]);
    }

    // =========================================================
    // Confirm Booking End
    // =========================================================
}
