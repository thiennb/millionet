<?php

namespace backend\controllers;

use Yii;
use common\models\StoreShift;
use common\models\MasterStore;
use common\models\StaffSchedule;
use yii\web\Response;
use \yii\widgets\ActiveForm;
use yii\filters\VerbFilter;

class StoreShiftController extends \yii\web\Controller {

    //variable connection database
    public $db;

    public function init() {
        //get connection
        $this->db = Yii::$app->db;
    }

    public function actionIndex() {
        $model = new StoreShift();
        // Get all store
        if (\common\components\FindPermission::getRole()->permission_id != \common\components\Constants::STAFF_ROLE) {
            $list_store = StoreShift::getListStore();
        }
        else {
            $list_store = MasterStore::find()
                    ->andWhere(['id' => Yii::$app->user->identity->store_id])
                    ->all();
        }

        if (!empty($list_store)) {
            $model->store_id = $list_store[0]['id'];
        }

        // Get store id when delete
        if (!empty(Yii::$app->request->getQueryParam('store_id'))) {
            $model->store_id = Yii::$app->request->getQueryParam('store_id');
        }

        // Get store id when submit search
        if (isset(Yii::$app->request->post('StoreShift')['store_id']) && !empty(Yii::$app->request->post('StoreShift')['store_id'])) {
            $model->store_id = Yii::$app->request->post('StoreShift')['store_id'];
        }

        $list_store = StoreShift::getListStoreID($model->store_id);
        $list_time[$list_store[0]['time_open']] = $list_store[0]['time_open'];
        $time_temp = $list_store[0]['time_open'];
        if ($list_store[0]['time_open'] != null) {
            $flag = true;
            while ($flag) {
                $time_temp = date('H:i', strtotime("+30 minutes", strtotime($time_temp)));
                $list_time[$time_temp] = $time_temp;
                if ($time_temp == $list_store[0]['time_close']) {
                    $flag = false;
                }
            }
        }
        $storeShifts = StoreShift::search($model->store_id)->all();
        //return view
        return $this->render('index', [
                    'model' => $model,
                    'storeShifts' => $storeShifts,
                    'list_time' => $list_time
        ]);
    }

    public function actionCreate() {

        $model = new StoreShift();
        $list_store = StoreShift::getListStore();

        if (!empty($list_store)) {
            $model->store_id = $list_store[0]['id'];
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', Yii::t("backend", "Create Success"));
            return $this->redirect(['index', 'store_id' => Yii::$app->request->post('StoreShift')['store_id']]);
        } else {
            $storeShifts = StoreShift::search($model->store_id)->all();
            $list_store = StoreShift::getListStoreID($model->store_id);
            $list_time[$list_store[0]['time_open']] = $list_store[0]['time_open'];
            $time_temp = $list_store[0]['time_open'];
            $flag = true;
            while ($flag) {
                $time_temp = date('H:i', strtotime("+30 minutes", strtotime($time_temp)));
                $list_time[$time_temp] = $time_temp;
                if ($time_temp == $list_store[0]['time_close']) {
                    $flag = false;
                }
            }
            return $this->render('index', ['model' => $model, 'storeShifts' => $storeShifts, 'list_time' => $list_time]);
        }
    }

    public function actionDelete() {

        if (empty(StaffSchedule::getStaffSchedule(Yii::$app->request->getQueryParam('id')))) {

            $model = $this->findModel(Yii::$app->request->getQueryParam('id'));

            $result = $model->softDelete();

            if ($result) {
                Yii::$app->session->setFlash('success', Yii::t("backend", "Delete Success"));
            } else {
                Yii::$app->getSession()->setFlash('error', [
                    'error' => Yii::t('backend', "System error happen. Please contact with System manager.")
                ]);
            }

            return $this->redirect(['index', 'store_id' => Yii::$app->request->getQueryParam('store_id')]);
        } else {
            return "false";
        }
    }

    protected function findModel($id) {
        if (($model = StoreShift::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
