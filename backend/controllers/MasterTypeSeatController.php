<?php

namespace backend\controllers;

use Yii;
use common\models\MasterTypeSeat;
use common\models\MasterTypeSeatSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\components\Util;
use yii\web\UploadedFile;
use Exception;
use common\models\MasterStore;
use common\components\CommonCheckExistModel;

/**
 * MasterTypeSeatController implements the CRUD actions for MasterTypeSeat model.
 */
class MasterTypeSeatController extends Controller {

    //variable connection database
    public $db;

    public function init() {
        //get connection
        $this->db = Yii::$app->db;
    }

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all MasterTypeSeat models.
     * @return mixed
     */
    public function actionIndex() {
        $searchModel = new MasterTypeSeatSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single MasterTypeSeat model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id) {
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new MasterTypeSeat model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        try {

            $model = new MasterTypeSeat();

            // Create Translate 
            $transaction = $this->db->beginTransaction();

            if ($model->load(Yii::$app->request->post())) {

                $model->tmp_image1 = UploadedFile::getInstance($model, 'tmp_image1');
                $model->tmp_image2 = UploadedFile::getInstance($model, 'tmp_image2');
                $model->tmp_image3 = UploadedFile::getInstance($model, 'tmp_image3');

                if ($model->tmp_image1) {
                    $model->image1 = Yii::$app->security->generateRandomString() . '.' . $model->tmp_image1->extension;
                }
                if ($model->tmp_image2) {
                    $model->image2 = Yii::$app->security->generateRandomString() . '.' . $model->tmp_image2->extension;
                }
                if ($model->tmp_image3) {
                    $model->image3 = Yii::$app->security->generateRandomString() . '.' . $model->tmp_image3->extension;
                }
                if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {

                    $model->validate();


                    return $this->render('create', [
                                'model' => $model,
                    ]);
                }
                //var_dump($model->errors);exit();

                if ($model->store_id == '' || (empty($model->store_id))) {
                    $store_is_common = (new MasterStore)->findStoreCommon('00000');
                    $model->store_id = $store_is_common->id;
                }
                
                if(!(new CommonCheckExistModel)->checkStoreByExists($model->store_id)){
                    throw new Exception('Object delete');
                }
                
                if ($model->save()) {
                    if (!empty($model->image1)) {
                        Util::uploadFile($model->tmp_image1, $model->image1);
                    }
                    if (!empty($model->image2)) {
                        Util::uploadFile($model->tmp_image2, $model->image2);
                    }
                    if (!empty($model->image3)) {
                        Util::uploadFile($model->tmp_image3, $model->image3);
                    }
                    // Commit Transaction
                    $transaction->commit();
                    //var_dump($model->errors);exit();
                    Yii::$app->session->setFlash('success', Yii::t("backend", "Create Success"));
                    return $this->redirect(['index']);
                } else {
                    return $this->render('create', [
                                'model' => $model,
                    ]);
                }
            } else {

                return $this->render('create', [
                            'model' => $model,
                ]);
            }
        } catch (Exception $ex) {
            //rollback transaction
            $transaction->rollBack();
            //set error message
            Yii::$app->getSession()->setFlash('error', [
                'error' => Yii::t('backend', "System error happen. Please contact with System manager.")
            ]);
            if($ex->getMessage() == 'Object delete'){
                return $this->redirect(['index']);
            }
            //return index
            return $this->render('create', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing MasterTypeSeat model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
        //HoangNQ set role <> STAFF_ROLE
        // \common\components\FindPermission::permissionBeforeAction(\common\components\FindPermission::getRole()->permission_id != \common\components\Constants::STAFF_ROLE);
        //HoangNQ end
        try {

            $model = $this->findModel($id);

            // Create Translate 
            $transaction = $this->db->beginTransaction();

            if ($model->load(Yii::$app->request->post())) {
                if (empty($model->hidden_image1) && !empty($model->image1)) {
                    $model->hidden_image1 = $model->image1;
                    $model->image1 = '';
                }
                //check delete image 2
                if (empty($model->hidden_image2) && !empty($model->image2)) {
                    $model->hidden_image2 = $model->image2;
                    $model->image2 = '';
                }
                //check delete image 3
                if (empty($model->hidden_image3) && !empty($model->image3)) {
                    $model->hidden_image3 = $model->image3;
                    $model->image3 = '';
                }

                $model->tmp_image1 = UploadedFile::getInstance($model, 'tmp_image1');
                $model->tmp_image2 = UploadedFile::getInstance($model, 'tmp_image2');
                $model->tmp_image3 = UploadedFile::getInstance($model, 'tmp_image3');
                if ($model->tmp_image1) {
                    $model->image1 = Yii::$app->security->generateRandomString() . '.' . $model->tmp_image1->extension;
                }
                if ($model->tmp_image2) {
                    $model->image2 = Yii::$app->security->generateRandomString() . '.' . $model->tmp_image2->extension;
                }
                if ($model->tmp_image3) {
                    $model->image3 = Yii::$app->security->generateRandomString() . '.' . $model->tmp_image3->extension;
                }
                if ($model->save()) {
                    if (!empty($model->image1)) {
                        Util::uploadFile($model->tmp_image1, $model->image1);
                    }
                    if (!empty($model->image2)) {
                        Util::uploadFile($model->tmp_image2, $model->image2);
                    }
                    if (!empty($model->image3)) {
                        Util::uploadFile($model->tmp_image3, $model->image3);
                    }
                    if (!empty($model->hidden_image1) && empty($model->image1)) {
                        Util::deleteFile($model->hidden_image1);
                    }
                    //delete image 2
                    if (!empty($model->hidden_image2) && empty($model->image2)) {
                        Util::deleteFile($model->hidden_image3);
                    }
                    //delete image 3
                    if (!empty($model->hidden_image3) && empty($model->image3)) {
                        Util::deleteFile($model->hidden_image3);
                    }
                    // Commit Transaction
                    $transaction->commit();

                    Yii::$app->session->setFlash('success', Yii::t("backend", "Update Success"));

                    return $this->redirect(['index']);
                } else {
                    //rollback transaction
                    $transaction->rollBack();
                    // Return view create
                    return $this->render('update', [
                                'model' => $model,
                    ]);
                }
            } else {
                $model->hidden_image1 = $model->image1;
                $model->hidden_image2 = $model->image2;
                $model->hidden_image3 = $model->image3;
                return $this->render('update', [
                            'model' => $model,
                ]);
            }
        } catch (Exception $ex) {
            $status = $ex->statusCode;
            if ($status == 404) {
                throw new \yii\web\BadRequestHttpException('The requested page does not permisstion.');
            }
            //rollback transaction
            $transaction->rollBack();
            //set error message
            Yii::$app->getSession()->setFlash('error', [
                'error' => Yii::t('backend', "System error happen. Please contact with System manager.")
            ]);
            //return index
            return $this->redirect(['index']);
        }
    }

    /**
     * Deletes an existing MasterTypeSeat model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {
        try {
            //create transaction
            $transaction = $this->db->beginTransaction();

            $model = $this->findModel($id);
            if (!$model->checkDeleteSeat($id)) {
                return 'false';
            } else {

                //soft delete tax
                $model->softDelete();
                //commit data
                $transaction->commit();
                Yii::$app->session->setFlash('success', Yii::t("backend", "Deleted successfully."));
                //return list tax
                return $this->redirect(['index'],200);
            }
        } catch (Exception $ex) {//if have exception
            //rollback transaction
            $transaction->rollBack();
            //set error message
            return 'false';
        }
    }

    /**
     * Finds the MasterTypeSeat model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return MasterTypeSeat the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = MasterTypeSeat::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    // Validate From
    public function actionValidate() {

        $model = new MasterTypeSeat();
        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = 'json';
            return \yii\bootstrap\ActiveForm::validate($model);
        }
    }

}
