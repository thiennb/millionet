<?php

namespace backend\controllers;

use Yii;
use common\models\MstCategoriesProduct;
use common\models\MstCategoriesProductSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use Exception;
use common\components\Util;

/**
 * MstCategoriesProductController implements the CRUD actions for MstCategoriesProduct model.
 */
class MstCategoriesProductController extends Controller {

    //variable connection database
    public $db;

    public function init() {
        //get connection
        $this->db = Yii::$app->db;
    }

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all MstCategoriesProduct models.
     * @return mixed
     */
    public function actionIndex($success = '') {
        $searchModel = new MstCategoriesProductSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        if ($success == '1') {
            Yii::$app->session->setFlash('success', Yii::t("backend", "Save Success"));
        }
        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new MstCategoriesProduct model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($name) {
        try {
            if (Yii::$app->request->isAjax) {
                //create transaction
                $transaction = $this->db->beginTransaction();
                //create mode
                $model = new MstCategoriesProduct();
                $model->name = $name;
                // Get company_code from session
                //$session = Yii::$app->session;
                //$company_code =  Util::getCookiesCompanyCode();
//                if(empty($company_code)){
//                    $company_code = \common\models\CompanyStore::find()->join('INNER JOIN', 'mst_customer','mst_customer.register_store_id = company_store.store_id')
//                                    ->andWhere(['mst_customer.user_id' => Yii::$app->user->id])->one()->company_code;
//                }
                $model->company_id = Util::getCookiesCompanyId();
                //check submit form and validate input data
                $model->save();
                Yii::$app->session->setFlash('success', Yii::t("backend", "Save Success Product Category"));
                //commit transaction
                $transaction->commit();
            }
            //return view list tax
//            return $this->redirect(['index']);
            return 'true';
        } catch (Exception $ex) {//if have exception
            //rollback transaction
            $transaction->rollBack();
            //set error message
            Yii::$app->getSession()->setFlash('error', [
                'error' => Yii::t('backend', "System error happen. Please contact with System manager.")
            ]);
            //return index
//            return $this->redirect(['index']);
            return 'false';
        }
    }

    /**
     * Creates a new MstCategoriesProduct model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionUpdate() {
        try {
            if (Yii::$app->request->isAjax) {
                //create transaction
                $transaction = $this->db->beginTransaction();
                $name = Yii::$app->request->post()['name'];
                $id = Yii::$app->request->post()['id'];
                //create mode
                $model = !empty($id) ? MstCategoriesProduct::findOne($id) : null;
                //empty category
                if (empty($model)) {
                    //return
                    return 'false';
                }
                $model->name = $name;
                //check submit form and validate input data
                $model->save();
                //commit transaction
                $transaction->commit();
                //return view list tax
                return 'true';
            }
            return 'false';
        } catch (Exception $ex) {//if have exception
            $transaction->rollBack();
            //return
            return 'false';
        }
    }

    /**
     * Deletes an existing MstCategoriesProduct model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {
        try {
            //create transaction
            $transaction = $this->db->beginTransaction();
            $model = $this->findModel($id);
            if (!$model->checkDelete($id)) {
                return 'false';
            } else {
                //soft delete tax
                $model->softDelete();
                //commit data
                $transaction->commit();
                Yii::$app->getSession()->setFlash('success', [
                    'success' => Yii::t('backend', "Delete Success")
                ]);
                //return list tax
                return $this->redirect(['index'],200);
            }
        } catch (Exception $ex) {//if have exception
            //rollback transaction
            $transaction->rollBack();
            //set error message
            return 'false';
        }
    }

    /**
     * Deletes an existing MstCategoriesProduct model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionReload() {
//        Yii::$app->session->setFlash('success', Yii::t("backend", "Save Success"));
        return $this->redirect(['index'],200);
    }

    /**
     * Finds the MstCategoriesProduct model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return MstCategoriesProduct the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = MstCategoriesProduct::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
