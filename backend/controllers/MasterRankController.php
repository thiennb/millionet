<?php

namespace backend\controllers;

use Yii;
use common\models\MasterRank;
use common\models\MasterRankSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\components\Util;

/**
 * MasterRankController implements the CRUD actions for MasterRank model.
 */
class MasterRankController extends Controller {

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all MasterRank models.
     * @return mixed
     */
    public function actionIndex() {
        $model = MasterRank::find()->one();
        (empty($model)) ? $model = new MasterRank() : $model;
        // Create session : get company code
        //$session = Yii::$app->session;
        $company_id = Util::getCookiesCompanyId();
        $model->company_id = $company_id;

        if ($model->load(Yii::$app->request->post())) {
            // Check emty point
            $model->normal_point_rate = $this->checkEmtyOfPoint($model->normal_point_rate);
            $model->bronze_point_rate = $this->checkEmtyOfPoint($model->bronze_point_rate);
            $model->sliver_point_rate = $this->checkEmtyOfPoint($model->sliver_point_rate);
            $model->gold_point_rate = $this->checkEmtyOfPoint($model->gold_point_rate);
            $model->black_point_rate = $this->checkEmtyOfPoint($model->black_point_rate);
            
            if ($model->save()) {
                Yii::$app->session->setFlash('success', Yii::t("backend", "Update Success"));
            }
        }
        $ms_rank_count = MasterRank::find()->count();

        if ($ms_rank_count == 0) {
            $model->normal_point_rate = 1;
            $model->normal_time_visit = 0;
            $model->normal_point_total = 0;
            $model->normal_use_money = 0;
            $model->rank_condition = '00';
        }
        // Check format
        $model->normal_point_rate = $this->checkFormatPoint($model->normal_point_rate);
        $model->bronze_point_rate = $this->checkFormatPoint($model->bronze_point_rate);
        $model->sliver_point_rate = $this->checkFormatPoint($model->sliver_point_rate);
        $model->gold_point_rate = $this->checkFormatPoint($model->gold_point_rate);
        $model->black_point_rate = $this->checkFormatPoint($model->black_point_rate);
        $model->normal_point_total = '0';
        
        return $this->render('index', [
                    'model' => $model,
        ]);
    }

    /**
     * Finds the MasterRank model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return MasterRank the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = MasterRank::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /*
     * Check format point
     * If the amount ends in 00 it will eliminate 00, whereas if the amount ending in '0' will remove '0', otherwise unchanged 
     * @param string $point
     * @return $point_format
     */

    protected function checkFormatPoint($point) {
        
        $normal_point_rate_st = substr($point, -2, 2);
        if($point == '0'){
            return $point_format = '0';
        }else if ($normal_point_rate_st == '00') {
            return $point_format = rtrim($point, "00");
        } else {
            if (substr($point, -1, 1) == '0') {
                return $point_format = rtrim($point,"0");
            }else{
                return $point;
            }
        }
    }
    
    /*
     * Check emty point 
     * If point emty 
     * @return 0
     * @param string point
     * @return $point
    */
    protected function checkEmtyOfPoint($point) {  
       if(empty($point)){
           return '0';
       }else{
           return $point;
       }
    }

}
