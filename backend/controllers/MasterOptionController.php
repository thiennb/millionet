<?php

namespace backend\controllers;

use Yii;
use common\models\MasterOption;
use common\models\MasterOptionSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\models\MasterStore;
use common\models\MstProduct;
use common\models\MstProductSearch;
use common\models\MstCategoriesProduct;
use Exception;
use common\models\ProductOption;
use common\components\CommonCheckExistModel;

/**
 * MasterOptionController implements the CRUD actions for MasterOption model.
 */
class MasterOptionController extends Controller {

    //variable connection database
    public $db;

    public function init() {
        //get connection
        $this->db = Yii::$app->db;
    }

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all MasterOption models.
     * @return mixed
     */
    public function actionIndex() {
        $searchModel = new MasterOptionSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $listStore = (new MasterStore)->getListStoreHaveStoreCommon();

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'listStore' => $listStore,
        ]);
    }

    /**
     * Displays a single MasterOption model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id) {
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new MasterOption model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        try {
            //create transaction
            $transaction = $this->db->beginTransaction();
            //create model            
            $model = new MasterOption();
            //get list Store
            $listStore = (new MasterStore)->getListStoreNotStoreCommon();
            //get list Category
            $listCategory = (new MstCategoriesProduct)->listCategory();
            //get list Product
            $store_id = 0;
            if (!empty(Yii::$app->request->post()) && isset(Yii::$app->request->post()['store_id'])) {
                $store_id = empty(Yii::$app->request->post()['store_id']) ? (new MasterStore)->findStoreCommon('00000') : Yii::$app->request->post()['store_id'];
            }
            $listProduct = (new MstProduct)->getListProductStoreAll($listCategory, $store_id);
            //get list product had selected
            $option_hidden = 0;
            if (!empty(Yii::$app->request->post()) && isset(Yii::$app->request->post()['option_hidden'])) {
                $option_hidden = empty(Yii::$app->request->post()['option_hidden']) ? (new MasterStore)->findStoreCommon('00000') : Yii::$app->request->post()['option_hidden'];
            }
            $list_product_select = (new MstProductSearch)->searchProduct($option_hidden);
            //check init or submit
            if ($model->load(Yii::$app->request->post())) {
                if (empty($model->store_id)) {
                    $store_is_common = (new MasterStore)->findStoreCommon('00000');
                    $model->store_id = $store_is_common->id;
                }
                // Check Store Exists
                if (!(new CommonCheckExistModel)->checkStoreByExists($model->store_id)) {
                    throw new Exception('Object deleted');
                }
                // Check Option Exists
                if (!empty($model->option_hidden)) {
                    if (!(new CommonCheckExistModel)->CheckProductsExists($model->option_hidden)) {
                        throw new Exception('Object deleted');
                    }
                }
                if ($model->save()) {
                    $model->saveProductOption();
                    $transaction->commit();
                    Yii::$app->getSession()->setFlash('success', [
                        'success' => Yii::t('backend', "Create Success")
                    ]);
                    return $this->redirect(['index']);
                }
            } else {//if init
                $role = \common\components\FindPermission::getRole();
                $model->_role = $role->permission_id;
                $model->id_store_option = (new MasterStore)->findStoreCommon('00000');

                return $this->render('create', [
                            'model' => $model,
                            'listStore' => $listStore,
                            'listProduct' => $listProduct,
                            'list_product_select' => $list_product_select,
                            'listCategory' => $listCategory,
                ]);
            }
        } catch (Exception $ex) {
            //rollback transaction
            $transaction->rollBack();
            //set error message
            Yii::$app->getSession()->setFlash('error', [
                'error' => Yii::t('backend', "Sorry can't create option, please try again.")
            ]);
            if ($ex->getMessage() == 'Object deleted') {
                return $this->redirect(['index']);
            }
            //return index
            return $this->render('create', [
                        'model' => $model,
                        'listStore' => $listStore,
                        'listProduct' => $listProduct,
                        'list_product_select' => $list_product_select,
                        'listCategory' => $listCategory,
            ]);
        }
    }

    /**
     * Updates an existing MasterOption model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
        try {
            //create transaction
            $transaction = $this->db->beginTransaction();
            //create model
            $model = $this->findModel($id);
            //get list Store
            $listStore = (new MasterStore)->getListStoreNotStoreCommon();
            //get list Category
            $listCategory = (new MstCategoriesProduct)->listCategory();
            //get list Product
            $store_id = 0;
            if (!empty(Yii::$app->request->post()) && isset(Yii::$app->request->post()['store_id'])) {
                $store_id = empty(Yii::$app->request->post()['store_id']) ? (new MasterStore)->findStoreCommon('00000') : Yii::$app->request->post()['store_id'];
            }
            $store_id = ($store_id == 0) ? $model->store_id : $store_id;
            $listProduct = (new MstProduct)->getListProductStoreAll($listCategory, $store_id);
            //get list product had selected            
            $option_hidden = ProductOption::find()->where(['option_id' => $model->id, 'del_flg' => '0'])->select(['product_id'])->all();
            $model->option_hidden = '';
            foreach ($option_hidden as $val) {
                $model->option_hidden .= $val->product_id . ',';
            }
            $option_hidden = 0;
            if (!empty(Yii::$app->request->post()) && isset(Yii::$app->request->post()['option_hidden'])) {
                $option_hidden = empty(Yii::$app->request->post()['option_hidden']) ? -1 : Yii::$app->request->post()['option_hidden'];
            }
            $option_hidden = ($option_hidden == '0') ? $model->option_hidden : $option_hidden;
            $list_product_select = (new MstProductSearch)->searchProduct($option_hidden);
            //check init or submitx
            if ($model->load(Yii::$app->request->post())) {
                if (empty($model->store_id)) {
                    $store_is_common = (new MasterStore)->findStoreCommon('00000');
                    $model->store_id = $store_is_common->id;
                }
                // Check Store Exists
                if (!(new CommonCheckExistModel)->checkStoreByExists($model->store_id)) {
                    throw new Exception('Object deleted');
                }
                // Check Option Exists
                if (!empty($model->option_hidden)) {
                    if (!(new CommonCheckExistModel)->CheckProductsExists($model->option_hidden)) {
                        throw new Exception('Object deleted');
                    }
                }
                if ($model->save()) {
                    $model->saveProductOption();
                    $transaction->commit();
                    Yii::$app->getSession()->setFlash('success', [
                        'success' => Yii::t('backend', "Update Success")
                    ]);
                    return $this->redirect(['index']);
                }
            } else {
                $role = \common\components\FindPermission::getRole();
                $model->_role = $role->permission_id;
                $model->id_store_option = (new MasterStore)->findStoreCommon('00000');

                return $this->render('update', [
                            'model' => $model,
                            'listStore' => $listStore,
                            'listProduct' => $listProduct,
                            'list_product_select' => $list_product_select,
                            'listCategory' => $listCategory,
                ]);
            }
        } catch (Exception $ex) {
            //rollback transaction
            $transaction->rollBack();
            //set error message
            Yii::$app->getSession()->setFlash('error', [
                'error' => Yii::t('backend', "Sorry can't update option, please try again.")
            ]);
            if ($ex->getMessage() == 'Object deleted') {
                return $this->redirect(['index']);
            }
            //return index
            return $this->redirect(['index']);
        }
    }

    /**
     * Deletes an existing MasterOption model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {
        try {
            //create transaction
            $transaction = $this->db->beginTransaction();

            $model = $this->findModel($id);
            if (!$model->checkDelete($id)) {
                return 'false';
            } else {
                //soft delete tax
                $model->softDelete();
                //commit data
                Yii::$app->getSession()->setFlash('success', [
                    'success' => Yii::t('backend', "Delete Success")
                ]);
                $transaction->commit();
                //return list tax
                return $this->redirect(['index']);
            }
        } catch (Exception $ex) {//if have exception
            //rollback transaction
            $transaction->rollBack();
            //set error message
            return 'false';
        }
    }

    /**
     * Finds the MasterOption model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return MasterOption the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = MasterOption::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Displays a single MasterOption model.
     * @param integer $id
     * @return mixed
     */
    public function actionValidate() {
        $model = new MasterOption();
        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = 'json';

            return \yii\bootstrap\ActiveForm::validate($model);
//          return \yii\bootstrap\ActiveForm::validateMultiple([$model,$model2]);
        }
    }

    /**
     * Finds the Store model exists based on its primary key value.
     * If the model is not found, return false.
     * @param integer $id
     * @return true
     */
    public function checkStoreByExists($id) {
        if (($model = MasterStore::findOne($id)) !== null) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Check Products model exists based on its primary key value.
     * If the model is not exists, return false.
     * @param integer $id_product
     * @return true
     */
    public function CheckProductsExists($id_product) {
        if (!empty($id_product)) {
            $count_array_products = count(explode(",", $id_product));
            if ((new MstProduct())->getCountProductsExists($id_product) != $count_array_products) {
                return false;
            } else {
                return true;
            }
        } else {
            return true;
        }
    }

}
