<?php

namespace backend\controllers;

use Yii;
use common\models\MasterCoupon;
use common\models\MasterCouponSeach;
use common\models\MasterStaffSearch;
use common\models\MstCategoriesProduct;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\components\Util;
use yii\web\UploadedFile;
// Datpdt 19/09/2016 start
use common\models\StoreSearch;
use common\models\MasterStore;
use common\models\MstProduct;
use common\models\MstProductSearch;
use Exception;
use common\models\ProductCoupon;
use common\models\MasterStaff;
use yii\helpers\Json;
use common\components\Constants;

// Datpdt 19/09/2016 end

/**
 * MasterCouponController implements the CRUD actions for MasterCoupon model.
 */
class MasterCouponController extends Controller {

    //variable connection database
    public $db;

    public function init() {
        //get connection
        $this->db = Yii::$app->db;
    }

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all MasterCoupon models.
     * @return mixed
     */
    public function actionIndex() {

        $searchModel = new MasterCouponSeach();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        // Datpdt 19/09/2016 start
        // Get name Store
        $searchListStore = MasterStore::getListStore();
        // Get staff
        $searchStaff = MasterStaffSearch::searchStaffByCustomer();
        // Get Category
        $searchModelCategory = MstCategoriesProduct::listCategory();

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'searchListStore' => $searchListStore,
                    'searchStaff' => $searchStaff,
                    'searchModelCategory' => $searchModelCategory,
        ]);
    }

    /**
     * Displays a single MasterCoupon model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id) {
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new MasterCoupon model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        //HoangNQ set role <> STAFF_ROLE
        \common\components\FindPermission::permissionBeforeAction(\common\components\FindPermission::getRole()->permission_id != \common\components\Constants::STAFF_ROLE);
        //HoangNQ end
        try {
            $model = new MasterCoupon();

            $model->benefits_content = '00';

            // Datpdt 20/09/2016 start
            // Get name from Store
            $searchListStore = (new MasterStore)->getListStoreNotStoreCommon();
            // Get staff
            $searchStaff = MasterStaffSearch::searchStaffByCustomer();
            // Datpdt 20/09/2016 start
            //get list Category
            $listCategory = (new MstCategoriesProduct)->listCategory();
            //get list Product
            $listProduct = (new MstProduct)->getListProductCheck($listCategory);

            // Create Translate 
            $transaction = $this->db->beginTransaction();

            if ($model->load(Yii::$app->request->post()) && $model->validate()) {

                $model->tmp_image = UploadedFile::getInstance($model, 'tmp_image');

                // ***************** //
                // Render Name Image //
                // ***************** //
                if ($model->tmp_image) {
                    $model->image = Yii::$app->security->generateRandomString() . '.' . $model->tmp_image->extension;
                }
                // ********************* //
                // Render Name Image End //
                // ********************* //
                $showCoupon = null;
                if (!empty($model->show_coupon)) {
                    $i = 0;
                    foreach ($model->show_coupon as $key => $value) {
                        $i++;
                        if ($i == 1) {
                            $showCoupon .= $value;
                        } else {
                            $showCoupon .= ("," . $value);
                        }
                    }
                }

                $model->show_coupon = $showCoupon;

                if (empty($model->code_membership)) {
                    $model->code_membership = NULL;
                }

                $tmp_discount_yen = $model->discount_yen;
                $tmp_discount_percent = $model->discount_percent;
                $tmp_discount_price_set = $model->discount_price_set;
                $tmp_discount_price_set_tax_type = $model->discount_price_set_tax_type;
                $tmp_discount_drink_eat = $model->discount_drink_eat;
                $tmp_discount_drink_eat_tax_type = $model->discount_drink_eat_tax_type;

                $model->discount_yen = null;
                $model->discount_percent = null;
                $model->discount_price_set = null;
                $model->discount_price_set_tax_type = null;
                $model->discount_drink_eat = null;
                $model->discount_drink_eat_tax_type = null;

                if (($model->benefits_content) == '00') {
                    $model->discount_yen = $tmp_discount_yen;
                }

                if (($model->benefits_content) == '01') {
                    $model->discount_percent = $tmp_discount_percent;
                }

                if (($model->benefits_content) == '02') {
                    $model->discount_price_set = $tmp_discount_price_set;
                    $model->discount_price_set_tax_type = $tmp_discount_price_set_tax_type;
                }

                if (($model->benefits_content) == '03') {
                    $model->discount_drink_eat = $tmp_discount_drink_eat;
                    $model->discount_drink_eat_tax_type = $tmp_discount_drink_eat_tax_type;
                }
                if ($model->expire_auto_set == 1) {
                    $model->expire_date = date("Y/m/t", strtotime("0 month"));
                }
                if (empty($model->store_id)) {
                    $store_is_common = (new MasterStore)->findStoreCommon('00000');
                    $model->store_id = $store_is_common->id;
                }
                
                // Check Products Exists
                if(!$model->CheckProductsExists()){
                    throw new Exception('Products deleted');
                }
                 // Check Store Exists
                if(!$this->checkStoreByExists($model->store_id)){
                    throw new Exception('Store deleted');
                }
                
                if ($model->save()) {
                    // ****************** //
                    // Upload Image Start //
                    // ****************** //                
                    if (!empty($model->image)) {
                        // Util::deleteFile($old_store_avatar);
                        Util::uploadFile($model->tmp_image, $model->image);
                    }
                    $model->saveProductCoupon();
                    // Commit Transaction
                    $transaction->commit();
                    // Return view Index
                    Yii::$app->session->setFlash('success', Yii::t("backend", "Create Success"));
                    return $this->redirect(['index']);
                } else {
                    //rollback transaction
                    $transaction->rollBack();
                    //set error message
                    Yii::$app->getSession()->setFlash('error', [
                        'error' => Yii::t('backend', "System error happen. Please contact with System manager.")
                    ]);
                    // Return view create
                    return $this->render('create', [
                                'model' => $model,
                                'searchListStore' => $searchListStore,
                                'searchStaff' => $searchStaff,
                                'listProduct' => $listProduct,
                                'list_product_select' => $list_product_select,
                                'listCategory' => $listCategory,
                    ]);
                }
            } else {
                //get list product had selected

                $list_product = 0;
                if (!empty(Yii::$app->request->post()) && isset(Yii::$app->request->post()['option_hidden'])) {
                    $list_product = !empty(Yii::$app->request->post()['option_hidden']) ? Yii::$app->request->post()['option_hidden'] : (-1);
                }
                //get list product had selected
                $list_product_select = (new MstProductSearch)->searchProduct($list_product);

                $model->jan_code_before_create = Util::genJanCodeAuto(\common\components\Constants::JAN_TYPE_COUPON);

                $role = \common\components\FindPermission::getRole();

                $model->_role = $role->permission_id;
                $model->id_store_comon = (new MasterStore)->findStoreCommon('00000');

                return $this->render('create', [
                            'model' => $model,
                            'searchListStore' => $searchListStore,
                            'searchStaff' => $searchStaff,
                            'listProduct' => $listProduct,
                            'list_product_select' => $list_product_select,
                            'listCategory' => $listCategory,
                ]);
            }
        } catch (Exception $ex) {
            //rollback transaction
            $transaction->rollBack();
            //set error message
            Yii::$app->getSession()->setFlash('error', [
                'error' => Yii::t('backend', "System error happen. Please contact with System manager.")
            ]);
            //return index
            //return index
            return $this->redirect(['index']);
        }
    }

    /**
     * Updates an existing MasterCoupon model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);
        //HoangNQ set role <> STAFF_ROLE
        \common\components\FindPermission::permissionBeforeAction(\common\components\FindPermission::getRole()->permission_id != \common\components\Constants::STAFF_ROLE, $model->store_id);
        //HoangNQ end
        try {

//            $list_product = [];
//            //get param
//            $id_arr = !isset($id) ? $id : explode('_', $id);
//            $id = (count($id_arr) <= 0) ? $id : $id_arr[0];
//            $list_product = (count($id_arr) <= 1) ? $list_product : $id_arr[1];
            //create model
            // Datpdt 20/09/2016 start
            // Get name from Store
            $searchListStore = (new MasterStore)->getListStoreNotStoreCommon();
            // Get staff
            $searchStaff = MasterStaffSearch::searchStaffByCustomer();
            // Datpdt 20/09/2016 start
            //get list Category
            $listCategory = (new MstCategoriesProduct)->listCategory();
            //get list Product
            $listProduct = (new MstProduct)->getListProductCheck($listCategory);
            //check init or submitx
            // Create Translate 
            $transaction = $this->db->beginTransaction();
            if ($model->load(Yii::$app->request->post())) {
                if ($model->expire_auto_set == 1) {
                    $model->expire_date = "";
                }
            }


            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                
                // ******************** //
                // Check Delete Image   //
                // Image 1 > 5 ******** //
                if (empty($model->tmp_image) && !empty($model->image)) {
                    $model->tmp_image = $model->image;
                    $model->image = '';
                }
                

                $model->tmp_image = UploadedFile::getInstance($model, 'tmp_image');
                
                // ***************** //
                // Render Name Image //
                // ***************** //
                if ($model->tmp_image) {
                    $model->image = Yii::$app->security->generateRandomString() . '.' . $model->tmp_image->extension;
                }
                // ********************* //
                // Render Name Image End //
                // ********************* //
                $showCoupon = null;
                $check_coupon = 0;
                if (!empty($model->show_coupon)) {

                    foreach ($model->show_coupon as $key => $value) {
                        $check_coupon++;
                        if ($check_coupon === 1) {
                            $showCoupon .= $value;
                        } else {
                            $showCoupon .= ("," . $value);
                        }
                    }
                }
                $model->show_coupon = $showCoupon;

                if (empty($model->code_membership)) {
                    $model->code_membership = NULL;
                }

                $tmp_discount_yen = $model->discount_yen;
                $tmp_discount_percent = $model->discount_percent;
                $tmp_discount_price_set = $model->discount_price_set;
                $tmp_discount_price_set_tax_type = $model->discount_price_set_tax_type;
                $tmp_discount_drink_eat = $model->discount_drink_eat;
                $tmp_discount_drink_eat_tax_type = $model->discount_drink_eat_tax_type;

                $model->discount_yen = null;
                $model->discount_percent = null;
                $model->discount_price_set = null;
                $model->discount_price_set_tax_type = null;
                $model->discount_drink_eat = null;
                $model->discount_drink_eat_tax_type = null;

                if (($model->benefits_content) == '00') {
                    $model->discount_yen = $tmp_discount_yen;
                }

                if (($model->benefits_content) == '01') {
                    $model->discount_percent = $tmp_discount_percent;
                }

                if (($model->benefits_content) == '02') {
                    $model->discount_price_set = $tmp_discount_price_set;
                    $model->discount_price_set_tax_type = $tmp_discount_price_set_tax_type;
                }

                if (($model->benefits_content) == '03') {
                    $model->discount_drink_eat = $tmp_discount_drink_eat;
                    $model->discount_drink_eat_tax_type = $tmp_discount_drink_eat_tax_type;
                }
                if ($model->expire_auto_set == 1) {
                    $model->expire_date = date("Y/m/t", strtotime("0 month"));
                }
                if (empty($model->store_id)) {
                    $store_is_common = (new MasterStore)->findStoreCommon('00000');
                    $model->store_id = $store_is_common->id;
                }
                // Check Products Exists
                if(!$model->CheckProductsExists()){
                    throw new Exception('Products deleted');
                }
                 // Check Store Exists
                if(!$this->checkStoreByExists($model->store_id)){
                    throw new Exception('Store deleted');
                }
                if ($model->save()) {
                    // ****************** //
                    // Upload Image Start //
                    // ****************** //                
                    if (!empty($model->image)) {
                        // Util::deleteFile($old_store_avatar);
                        Util::uploadFile($model->tmp_image, $model->image);
                    }
                    // ***************** //
                    //  Delete Image     //
                    // ***************** //
                    // Image 1 -> Image 5
                    if (!empty($model->tmp_image) && empty($model->image)) {
                        Util::deleteFile($model->tmp_image);
                    }
                    $model->saveProductCoupon();
                    // Commit Transaction
                    $transaction->commit();
                    // Return view Index
                    Yii::$app->session->setFlash('success', Yii::t("backend", "Update Success"));
                    return $this->redirect(['index']);
                } else {
                    //rollback transaction
                    $transaction->rollBack();
                    //set error message
                    Yii::$app->getSession()->setFlash('error', [
                        'error' => Yii::t('backend', "System error happen. Please contact with System manager.")
                    ]);
                    // Return view create
                    return $this->render('update', [
                                'model' => $model,
                                'searchListStore' => $searchListStore,
                                'searchStaff' => $searchStaff,
                                'listProduct' => $listProduct,
                                'list_product_select' => $list_product_select,
                                'listCategory' => $listCategory,
                    ]);
                }
            } else {

                //get list product had selected 
                $list_product = 0;

                if (!empty(Yii::$app->request->post()) && isset(Yii::$app->request->post()['option_hidden'])) {
                    $list_product = !empty(Yii::$app->request->post()['option_hidden']) ? Yii::$app->request->post()['option_hidden'] : (-1);
                }
                $option_hidden = ProductCoupon::find()->where(['coupon_id' => $model->id,'product_coupon.del_flg'=>'0'])->select(['product_id'])->all();

                if (empty($model->option_hidden)) {
                    foreach ($option_hidden as $val) {
                        $model->option_hidden .= $val->product_id . ',';
                    }
                }
                $list_product = ($list_product != 0) ? $list_product : $model->option_hidden;
                //get list product had selected
                $list_product_select = (new MstProductSearch)->searchProduct($list_product);
                
                $role = \common\components\FindPermission::getRole();
                $model->_role = $role->permission_id;
                $model->id_store_comon = (new MasterStore)->findStoreCommon('00000');
                
                if (!$model->load(Yii::$app->request->post())) {
                    $lenght = strlen($model->show_coupon);
                    if ($lenght > 4) {
                        $showcoupon = explode(",", $model->show_coupon);
                        $model->show_coupon = $showcoupon;
                    }
                } else {
                    $lenght = count($model->show_coupon);
                    if ($lenght > 2) {
                        $showcoupon = explode(",", $model->show_coupon);
                        $model->show_coupon = $showcoupon;
                    }
                }

                $model->jan_code_before_create = $model->coupon_jan_code;

                return $this->render('update', [
                            'model' => $model,
                            'searchListStore' => $searchListStore,
                            'searchStaff' => $searchStaff,
                            'listProduct' => $listProduct,
                            'list_product_select' => $list_product_select,
                            'listCategory' => $listCategory,
                ]);
            }
        } catch (Exception $ex) {
            //rollback transaction
            $transaction->rollBack();
            //set error message
            Yii::$app->getSession()->setFlash('error', [
                'error' => Yii::t('backend', "System error happen. Please contact with System manager.")
            ]);
            //return index
            //return index
            return $this->redirect(['index']);
        }
    }

    /**
     * Deletes an existing MasterCoupon model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {

        try {
            //create transaction
            $transaction = $this->db->beginTransaction();
            $model = $this->findModel($id);
            $productCoupon = ProductCoupon::find()->where(['coupon_id' => $id])->all();

            if (!$model->checkDelete($id, $model->coupon_jan_code)) {
                return 'false';
            } else {
                //soft delete tax
                // Delete coupon
                $model->softDelete();
                // Delete Product Coupon
                if (count($productCoupon) >= 1) {
                    for ($index = 0; $index < count($productCoupon); $index++) {
                        $productCoupon[$index]->softDelete();
                    }
                }

                //commit data
                $transaction->commit();
                Yii::$app->session->setFlash('success', Yii::t("backend", "Deleted successfully."));
                //return list tax
                return $this->redirect(['index'],200);
            }
        } catch (Exception $ex) {//if have exception
            //rollback transaction
            $transaction->rollBack();
            //set error message
            return 'false';
        }
    }

    /**
     * Finds the MasterCoupon model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return MasterCoupon the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = MasterCoupon::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionLoadproductbyidstore() {

        if (Yii::$app->request->isAjax) {

            $data = Yii::$app->request->post();

            $list_product = [];

            //get model Master Coupon
            $model = new MasterCoupon();

            $searchIdStore = explode(":", $data['idStore']);
            //get list Category
            $listCategory = (new MstCategoriesProduct)->listCategoryByStoreId($searchIdStore[0]);
            //get list Product
            $listProduct = (new MstProduct)->getListProductCheckByStore($listCategory, $searchIdStore);
            //get list product had selected
            $list_product_select = (new MstProductSearch)->searchProduct($list_product);

            return $this->renderAjax('_product', [
                        'listCategory' => $listCategory,
                        'listProduct' => $listProduct,
                        'list_product_select' => $list_product_select,
                        'model' => $model,
            ]);
        }
    }

    // Select staff by id store
    public function actionSelectstaffbyidstore() {
        $out = [];
        $result = [];
        if (isset($_POST['depdrop_parents'])) {
            $parents = $_POST['depdrop_parents'];
            if ($parents != null) {
                $store_id = $parents[0];
                if ($store_id == null) {
                    $role = \common\components\FindPermission::getRole();
                    if ($role->permission_id != Constants::STORE_MANAGER_ROLE || $role->permission_id != Constants::STAFF_ROLE) {
                        $store_id_common = (new MasterStore)->findStoreCommon('00000');
                        $out = (new MasterStaff)->listStaff($store_id_common->id);
                        foreach ($out as $key => $value) {
                            $arr = ['id' => $key, 'name' => $value];
                            $result[] = $arr;
                        }
                        echo Json::encode(['output' => $result, 'selected' => '']);
                        return;
                    } else {
                        echo Json::encode(['output' => $result, 'selected' => '']);
                        return;
                    }
                } else {
                    $out = (new MasterStaff)->listStaff($store_id);
                    foreach ($out as $key => $value) {
                        $arr = ['id' => $key, 'name' => $value];
                        $result[] = $arr;
                    }
                    echo Json::encode(['output' => $result, 'selected' => '']);
                    return;
                }
            }
        }
        echo Json::encode(['output' => '', 'selected' => '']);
    }

    // Validate From
    public function actionValidate() {

        $model = new MasterCoupon();
        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {

            Yii::$app->response->format = 'json';
            return \yii\bootstrap\ActiveForm::validate($model);
        }
    }
    
    /**
     * Finds the Store model exists based on its primary key value.
     * If the model is not found, return false.
     * @param integer $id
     * @return true
     */
    public function checkStoreByExists($id) {
        if (($model = MasterStore::findOne($id)) !== null) {
            return true;
        } else {
            return false;
        }
    }
   
}
