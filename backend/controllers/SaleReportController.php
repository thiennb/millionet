<?php

namespace backend\controllers;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\base\Exception;
use yii\web\Cookie;
use common\components\Util;
use common\models\SaleCustomerSearch;
use common\models\SaleReportProductSearch;
use common\models\MstCategoriesProduct;
use common\models\MstCategoriesProductSearch;
use common\models\MstOrder;
use common\models\MstOrderDetail;
use common\models\MstOrderDetailSearch;
use common\models\MstProduct;
use common\models\MstProductSearch;
use common\models\PointSetting;
use \common\models\MasterStore;
use Yii;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class SaleReportController extends Controller {

    public function actionIndex() {
        $searchModel = new SaleReportProductSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    
    public function actionDailySales(){
        $permissionId = Yii::$app->user->identity->permission_id;
        $logout_flag = false;
        $result = array('store_id'=>null,'revenues'=> null);
        
        $paramTotalUnit = Yii::$app->request->get('total_unit');
        $paramDay = Yii::$app->request->get('day');
        $paramMonth = Yii::$app->request->get('month');
        $paramYear = Yii::$app->request->get('year');
        $timeMark = array();
        
        try{
            
            if($permissionId == 0 || $permissionId == 1){
                $storeId = Yii::$app->request->get('store_id');
                $company_id = Util::getCookiesCompanyId();
                $stores = MasterStore::find()->select('id')->where(['company_id'=>$company_id, 'del_flg'=> '0'])->orderBy('id')->distinct()->all();
                if($storeId === null && $stores!== null){
                    $storeId = $stores[0]['id'];
                }
                $result['store_id'] = $storeId;
                $store = MasterStore::StoreDetail($result['store_id'])->one();
                
                if(!empty($store)){                    
                    $result['revenues'] = $this->getDailySalesReport($paramTotalUnit,$paramYear,$paramMonth, $paramDay,$store['store_code'] );
                }
                
            }else if($permissionId == 2 || $permissionId == 3){
                $result['store_id'] = Yii::$app->user->identity->store_id;
                $store = MasterStore::StoreDetail($result['store_id'])->one();
                
                if(!empty($store)){                    
                    $result['revenues'] = $this->getDailySalesReport($paramTotalUnit,$paramYear,$paramMonth, $paramDay,$store['store_code'] );
                }               
                
            }else{
                $logout_flag = true; throw  new Exception('Your permission is not accept');
            }
            
        } catch (Exception $ex) {
            var_dump($ex->getMessage());
            if($logout_flag === true){
                Yii::$app->user->logout();
                return $this->goHome();
            }
        }
        
        return $this->render('daily-sales',['data' => $result]);
    }
    
    public function actionProductSales(){
        $permissionId = Yii::$app->user->identity->permission_id;
        $logout_flag = false; $page=1;
        $result = array('store_id'=>null,
            'category_id' => Yii::$app->request->get('category_id'), 
            'start_date'=> Yii::$app->request->get('start_date') != '' ? Yii::$app->request->get('start_date') : '', 
            'end_date'=> Yii::$app->request->get('end_date') != '' ? Yii::$app->request->get('end_date') : date('Y/m/d'), 
            'list_categories'=> array(), 'sumInfo'=> array(), 'dataTable'=> array(), 'pages'=> array());
        
        //for pagination        
        if(Yii::$app->request->get('page')!= null){
            $page = Yii::$app->request->get('page');
        }
        $limit = 10; $offset = $limit*($page-1);
        
        try{
            
            if($permissionId === 0 || $permissionId === 1){
                //is admin
                $storeId = Yii::$app->request->get('store_id');
                $company_id = Util::getCookiesCompanyId();
                $stores = MasterStore::find()->select('id')->where(['company_id'=>$company_id, 'del_flg'=> '0'])->orderBy('id')->distinct()->all();
                if($storeId === null && $stores!== null){
                    $storeId = $stores[0]['id'];
                }
                $list_main_category = array();
                foreach($stores as $store){
                    $listCategories = MstCategoriesProduct::listCategoryByStoreId($store['id']);
                    if(!empty($listCategories)){
                        $listcopy = array('all'=> Yii::t('backend', 'All categories'));
                        foreach($listCategories as $key => $value) {
                            $listcopy[$key] = $value;
                        }
                        $listCategories = $listcopy;
                    }

                    if($store['id'] == $storeId){
                        $list_main_category = $listCategories;
                    }
                    array_push($result['list_categories'], array('store_id'=>$store['id'], 'list_categories'=>$listCategories));
                }
                
                $result['store_id'] = $storeId;
                
                $saleReport = $this->getSalesReport($storeId, $result['category_id'], $list_main_category, $result['start_date'], $result['end_date'], $offset, $limit);
                
                if($saleReport !== null){
                    $result['pages'] = $saleReport['pages']; 
                    $result['sumInfo'] = $saleReport['sumInfo'];
                    $result['dataTable'] = $saleReport['dataTable'];
                }
                
            }else if($permissionId === 2 || $permissionId === 3){
                //is staff
                $storeId = Yii::$app->user->identity->store_id;
                $listCategories = MstCategoriesProduct::listCategoryByStoreId($storeId);
                
                if(!empty($listCategories)){
                    $listcopy = array('all'=>Yii::t('backend', 'All categories'));
                    foreach($listCategories as $key => $value) {
                        $listcopy[$key] = $value;
                    }
                    $listCategories = $listcopy;
                }

                $result['store_id'] = $storeId;
                array_push($result['list_categories'], array('store_id'=>$storeId, 'list_categories'=> $listCategories));
                
                $saleReport = $this->getSalesReport($storeId, $result['category_id'], $listCategories, $result['start_date'], $result['end_date'], $offset, $limit);
                if($saleReport !== null){
                    $result['pages'] = $saleReport['pages']; 
                    $result['sumInfo'] = $saleReport['sumInfo'];
                    $result['dataTable'] = $saleReport['dataTable'];
                }
                
            }else{
                $logout_flag = true; throw new Exception('Your permission is not accept');
            }
            
        } catch (Exception $ex) {
            if($logout_flag === true){
                Yii::$app->user->logout();
                return $this->goHome();
            }
        }
        
        return $this->render('product-sales',['data' => $result]);
    }
    
    private function getSalesReport($storeId = null, $categoryId = null, $listCategories = null, $startDay = '', $endDate = '', $offset = 0, $limit = 10){
        
        if($storeId !== null && $categoryId != null && $listCategories!= null){
            
            if(array_key_exists($categoryId, $listCategories)){
                $arrayIdCategory = array();
                if($categoryId === 'all'){
                    $arrayIdCategory = array_keys($listCategories); unset($arrayIdCategory[0]);
                }else{
                    $arrayIdCategory[] = $categoryId;
                }

                $sumInfo = MstOrderDetail::getSumRevenueQuantity($arrayIdCategory, $startDay, $endDate, $storeId);
                $query = MstOrderDetail::findProductSales($arrayIdCategory, $startDay, $endDate, $storeId);
                if(!empty($query)){
                    $pages = new \yii\data\Pagination(['totalCount' => $query->count()]); $pages->setPageSize($limit);
                    $models = $query->offset($offset)->limit($limit)->all();

                    $result['sumInfo'] = $sumInfo;
                    $result['dataTable'] = $models;
                    $result['pages'] = $pages;
                    return $result;
                }
            }
        }
        return array('pages' => [], 'sumInfo'=>null, 'dataTable' => null);
    }
    
    private function getDailySalesReport($paramTotalUnit, $paramYear, $paramMonth, $paramDay, $storeCode){
        $revenues = array();
        switch ($paramTotalUnit){
            case 'by_hour':
                $timeMark = ['00:00','02:00', '04:00', '06:00', '08:00', '10:00', '12:00', '14:00', '16:00', '18:00', '20:00', '22:00', '23:59'];
                for($i = 0; $i <= 11 ; $i++){
                    $revenueTimeMark = MstOrder::findTotalMoneyByHour($paramYear, $paramMonth, $paramDay, $timeMark[$i], $timeMark[$i+1], $storeCode);
                    $revenues[] = $revenueTimeMark;
                }
                break;
            case 'by_day':
                $revenues = MstOrder::findTotalMoneyByDate($paramYear, $paramMonth, $storeCode);
                break;
            case 'by_month' : 
                $revenues = MstOrder::findTotalMoneyByMonth($paramYear, $storeCode);
                break;
            case 'by_year' : 
                $revenues = MstOrder::findTotalMoneyByYear($storeCode);
            default: break;
        }        
        return $revenues;
    }

}
