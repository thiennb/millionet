<?php

namespace backend\controllers;

use Yii;
use common\models\MasterStaff;
use common\models\MasterStaffSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\components\Util;
use yii\web\UploadedFile;
use Exception;
use common\models\ManagementLogin;
use common\models\MasterStore;
use common\models\MstProduct;
use yii\helpers\Json;
use common\components\CommonCheckExistModel;

/**
 * MasterStaffController implements the CRUD actions for MasterStaff model.
 */
class MasterStaffController extends Controller {

    //variable connection database
    public $db;

    public function init() {
        //get connection
        $this->db = Yii::$app->db;
    }

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all MasterStaff models.
     * @return mixed
     */
    public function actionIndex() {
        //search
        $searchModel = new MasterStaffSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        //get list store
        $listStore = (new MasterStore)->getListStore();

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'listStore' => $listStore,
        ]);
    }

    /**
     * Creates a new MasterStaff model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        try {
            //create transaction
            $transaction = $this->db->beginTransaction();
            //get list store
            $listStore = (new MasterStore)->getListStore();

            // Datpdt 23/09/2016 Update End 
            //create mode
            $model = new MasterStaff();
            // Datpdt 23/09/2016 Update Start
            $listProduct = (new MstProduct)->listProductByStoreId($model->store_id);
            //set scenariol for validate when case create
            $model->setScenario("create");
            //check submit form (method post)
            if ($model->load(Yii::$app->request->post())) {//case submit form
                //get imange
                $model->file_avatar = UploadedFile::getInstance($model, 'file_avatar');
                $model->file_image1 = UploadedFile::getInstance($model, 'file_image1');
                $model->file_image2 = UploadedFile::getInstance($model, 'file_image2');
                $model->file_icon_1 = UploadedFile::getInstance($model, 'file_icon_1');
                $model->file_icon_2 = UploadedFile::getInstance($model, 'file_icon_2');
                $model->file_icon_3 = UploadedFile::getInstance($model, 'file_icon_3');
                if (!$model->validate()) {
                    //return view create
                    return $this->render('create', [
                                'model' => $model,
                                'listStore' => $listStore,
                                'listProduct' => $listProduct,
                    ]);
                }
                //check image 1 data
                if ($model->file_avatar) {
                    //set value for variable image 1 before insert data
                    $model->avatar = Yii::$app->security->generateRandomString() . '.' . $model->file_avatar->extension;
                }
                //check image 2 data
                if ($model->file_image1) {
                    //set value for variable image 2 before insert data
                    $model->image1 = Yii::$app->security->generateRandomString() . '.' . $model->file_image1->extension;
                }
                //check image 3 data
                if ($model->file_image2) {
                    //set value for variable image 3 before insert data
                    $model->image2 = Yii::$app->security->generateRandomString() . '.' . $model->file_image2->extension;
                }
                //check image 1 data
                if ($model->file_icon_1) {
                    //set value for variable image 1 before insert data
                    $model->link_icon_1 = Yii::$app->security->generateRandomString() . '.' . $model->file_icon_1->extension;
                }
                //check image 2 data
                if ($model->file_icon_2) {
                    //set value for variable image 2 before insert data
                    $model->link_icon_2 = Yii::$app->security->generateRandomString() . '.' . $model->file_icon_2->extension;
                }
                //check image 3 data
                if ($model->file_icon_3) {
                    //set value for variable image 3 before insert data
                    $model->link_icon_3 = Yii::$app->security->generateRandomString() . '.' . $model->file_icon_3->extension;
                }
                //create management ID
                $model->saveManagementId($model->password_new);
                //validate and save product
//                if (!$this->findStoreByStoreId($model->store_id)) {
//                    throw new Exception('Object deleted');
//                }
                if(!(new CommonCheckExistModel)->checkStoreByExists($model->store_id)){
                     throw new Exception('Object deleted');
                }
                if(!(new CommonCheckExistModel)->CheckProductsExists($model->assign_product_id)){
                    throw new Exception('Object deleted');
                }
                if ($model->save()) {// case ok
                    // $model->id
                    // 
                    //upload image
                    if (!empty($model->avatar)) {
                        Util::uploadFile($model->file_avatar, $model->avatar);
                    }
                    if (!empty($model->image1)) {
                        Util::uploadFile($model->file_image1, $model->image1);
                    }
                    if (!empty($model->image2)) {
                        Util::uploadFile($model->file_image2, $model->image2);
                    }
                    if (!empty($model->link_icon_1)) {
                        Util::uploadFile($model->file_icon_1, $model->link_icon_1);
                    }
                    if (!empty($model->link_icon_2)) {
                        Util::uploadFile($model->file_icon_2, $model->link_icon_2);
                    }
                    if (!empty($model->link_icon_3)) {
                        Util::uploadFile($model->file_icon_3, $model->link_icon_3);
                    }

                    Yii::$app->getSession()->setFlash('success', [
                        'success' => Yii::t('backend', "Create Success")
                    ]);
                    //commit transaction
                    $transaction->commit();
                    //return index
                    return $this->redirect(['index']);
                } else {// validate fail or save error
                    //rollback transaction
                    $transaction->rollBack();
                    //return view create
                    return $this->render('create', [
                                'model' => $model,
                                'listStore' => $listStore,
                                'listProduct' => $listProduct,
                    ]);
                }
            } else {//case init
                $model->permission_id = '3';

                return $this->render('create', [
                            'model' => $model,
                            'listStore' => $listStore,
                            'listProduct' => $listProduct,
                ]);
            }
        } catch (Exception $ex) {//if have exception
            //rollback transaction
            $transaction->rollBack();
            //set error message
            Yii::$app->getSession()->setFlash('error', [
                'error' => Yii::t('backend', "System error happen. Please contact with System manager.")
            ]);
            if ($ex->getMessage() == 'Object deleted') {
                return $this->redirect(['index']);
            }
            //Util::dd($ex);
            //return index
            return $this->render('create', [
                        'model' => $model,
                        'listStore' => $listStore,
                        'listProduct' => $listProduct,
            ]);
        }
    }

    /**
     * Finds the Store model based on its primary key value.
     * If the model is not found, return false.
     * @param integer $id
     * @return true
     */
    public function findStoreByStoreId($id) {
        if (($model = MasterStore::findOne($id)) !== null) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Updates an existing MasterStaff model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
        try {
            //create transaction
            $transaction = $this->db->beginTransaction();
            //get list store
            $listStore = (new MasterStore)->getListStore();
            // Datpdt 23/09/2016 Update End 
            //create mode
            $model = $this->findModel($id);

            // Datpdt 23/09/2016 Update Start
            $listProduct = (new MstProduct)->listProductByStoreId($model->store_id);
            //set scenariol for validate when case create
            $model->setScenario("update");
            //check submit form (method post)
            if ($model->load(Yii::$app->request->post()) && $model->validate()) {//case submit form
                //check password
                $manage_login = ManagementLogin::findOne($model->management_login_id);
                if (isset($manage_login) && !empty($model->password)) {
                    if (!$manage_login->validatePassword($model->password)) {
                        $model->addError('password', Yii::t('backend', "Password not match, please try again."));
                        return $this->render('update', [
                                    'model' => $model,
                                    'listStore' => $listStore,
                                    'listProduct' => $listProduct,
                        ]);
                    }
                } else if (!isset($manage_login)) {
                    //set error message
                    Yii::$app->getSession()->setFlash('error', [
                        'error' => Yii::t('backend', "System error happen. Please contact with System manager.")
                    ]);
                    //return index
                    return $this->redirect(['index']);
                }
                //check delete image 1
                if (empty($model->hidden_avatar) && !empty($model->avatar)) {
                    $model->hidden_avatar = $model->avatar;
                    $model->avatar = '';
                }
                //check delete image 2
                if (empty($model->hidden_image1) && !empty($model->image1)) {
                    $model->hidden_image1 = $model->image1;
                    $model->image1 = '';
                }
                //check delete image 3
                if (empty($model->hidden_image2) && !empty($model->image2)) {
                    $model->hidden_image2 = $model->image2;
                    $model->image2 = '';
                }
                if (empty($model->hidden_icon1) && !empty($model->link_icon_1)) {
                    $model->hidden_icon1 = $model->link_icon_1;
                    $model->link_icon_1 = '';
                }
                //check delete image 2
                if (empty($model->hidden_icon2) && !empty($model->link_icon_2)) {
                    $model->hidden_icon2 = $model->link_icon_2;
                    $model->link_icon_2 = '';
                }
                //check delete image 3
                if (empty($model->hidden_icon3) && !empty($model->link_icon_3)) {
                    $model->hidden_icon3 = $model->link_icon_3;
                    $model->link_icon_3 = '';
                }
                //get imange
                $model->file_avatar = UploadedFile::getInstance($model, 'file_avatar');
                $model->file_image1 = UploadedFile::getInstance($model, 'file_image1');
                $model->file_image2 = UploadedFile::getInstance($model, 'file_image2');
                $model->file_icon_1 = UploadedFile::getInstance($model, 'file_icon_1');
                $model->file_icon_2 = UploadedFile::getInstance($model, 'file_icon_2');
                $model->file_icon_3 = UploadedFile::getInstance($model, 'file_icon_3');
                //check image 1 data
                if ($model->file_avatar) {
                    //set value for variable image 1 before insert data
                    $model->avatar = Yii::$app->security->generateRandomString() . '.' . $model->file_avatar->extension;
                }
                //check image 2 data
                if ($model->file_image1) {
                    //set value for variable image 2 before insert data
                    $model->image1 = Yii::$app->security->generateRandomString() . '.' . $model->file_image1->extension;
                }
                //check image 3 data
                if ($model->file_image2) {
                    //set value for variable image 3 before insert data
                    $model->image2 = Yii::$app->security->generateRandomString() . '.' . $model->file_image2->extension;
                }
                //check image 1 data
                if ($model->file_icon_1) {
                    //set value for variable image 1 before insert data
                    $model->link_icon_1 = Yii::$app->security->generateRandomString() . '.' . $model->file_icon_1->extension;
                }
                //check image 2 data
                if ($model->file_icon_2) {
                    //set value for variable image 2 before insert data
                    $model->link_icon_2 = Yii::$app->security->generateRandomString() . '.' . $model->file_icon_2->extension;
                }
                //check image 3 data
                if ($model->file_icon_3) {
                    //set value for variable image 3 before insert data
                    $model->link_icon_3 = Yii::$app->security->generateRandomString() . '.' . $model->file_icon_3->extension;
                }
                //validate and save product
                if ($model->save()) {// case ok
                    //update password
                    if (!empty($model->password_new) && !empty($model->password_new_re)) {
                        $model->updatePasswordManagementLogin($model->password_new);
                    }
                    //upload image
                    if (!empty($model->avatar)) {
                        Util::uploadFile($model->file_avatar, $model->avatar);
                    }
                    if (!empty($model->image1)) {
                        Util::uploadFile($model->file_image1, $model->image1);
                    }
                    if (!empty($model->image2)) {
                        Util::uploadFile($model->file_image2, $model->image2);
                    }
                    if (!empty($model->link_icon_1)) {
                        Util::uploadFile($model->file_icon_1, $model->link_icon_1);
                    }
                    if (!empty($model->link_icon_2)) {
                        Util::uploadFile($model->file_icon_2, $model->link_icon_2);
                    }
                    if (!empty($model->link_icon_3)) {
                        Util::uploadFile($model->file_icon_3, $model->link_icon_3);
                    }
                    //commit transaction
                    $transaction->commit();
                    //delete image 1
                    if (!empty($model->hidden_avatar) && empty($model->avatar)) {
                        Util::deleteFile($model->hidden_avatar);
                    }
                    //delete image 2
                    if (!empty($model->hidden_image1) && empty($model->image1)) {
                        Util::deleteFile($model->hidden_image1);
                    }
                    //delete image 3
                    if (!empty($model->hidden_image2) && empty($model->image2)) {
                        Util::deleteFile($model->hidden_image2);
                    }
                    if (!empty($model->hidden_icon1) && empty($model->link_icon_1)) {
                        Util::deleteFile($model->hidden_icon1);
                    }
                    //delete image 2
                    if (!empty($model->hidden_icon2) && empty($model->link_icon_2)) {
                        Util::deleteFile($model->hidden_icon2);
                    }
                    //delete image 3
                    if (!empty($model->hidden_icon3) && empty($model->link_icon_3)) {
                        Util::deleteFile($model->hidden_icon3);
                    }
                    Yii::$app->getSession()->setFlash('success', [
                        'success' => Yii::t('backend', "Update Success")
                    ]);
                    //return index
                    return $this->redirect(['index']);
                } else {// validate fail or save error
                    //rollback transaction
                    $transaction->rollBack();
                    //return view create
                    return $this->render('update', [
                                'model' => $model,
                                'listStore' => $listStore,
                                'listProduct' => $listProduct,
                    ]);
                }
            } else {//case init
                $model->hidden_avatar = $model->avatar;
                $model->hidden_image1 = $model->image1;
                $model->hidden_image2 = $model->image2;
                $model->hidden_icon1 = $model->link_icon_1;
                $model->hidden_icon2 = $model->link_icon_2;
                $model->hidden_icon3 = $model->link_icon_3;
                return $this->render('update', [
                            'model' => $model,
                            'listStore' => $listStore,
                            'listProduct' => $listProduct,
                ]);
            }
        } catch (Exception $ex) {//if have exception
            //rollback transaction
            $transaction->rollBack();
            //set error message
            Yii::$app->getSession()->setFlash('error', [
                'error' => Yii::t('backend', "System error happen. Please contact with System manager.")
            ]);
            if ($ex->getMessage() == 'Store deleted') {
               return $this->redirect(['index']);
            }
            //return index
            return $this->redirect(['index']);
        }
    }

    /**
     * Deletes an existing MasterStaff model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {
        try {
            //create transaction
            $transaction = $this->db->beginTransaction();

            $model = $this->findModel($id);
            if (!$model->checkDelete($id)) {
                return 'false';
            } else {
                $model_management_login = ManagementLogin::findOne(['id' => $model->management_login_id]);
                $model_management_login->softDelete();
                //soft delete tax
                $model->softDelete();
                //commit data
                $transaction->commit();
                Yii::$app->getSession()->setFlash('success', [
                    'success' => Yii::t('backend', "Delete Success")
                ]);
                //return list tax
                return $this->redirect(['index']);
            }
        } catch (Exception $ex) {//if have exception
            //rollback transaction
            $transaction->rollBack();
            //set error message
            return 'false';
        }
    }

    /**
     * Finds the MasterStaff model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return MasterStaff the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = MasterStaff::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Lists all MasterStaff models.
     * @return mixed
     */
    public function actionGetpostcode() {

        $code = Yii::$app->request->post('postCode');
        $result = \common\models\PostCode::getAddressName($code);
        if ($result != false)
            return $result;
        return false;
    }

    // Validate From
    public function actionValidate() {
        $model = new MasterStaff();
        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            !empty($model->id_staff_hd) ? $model->setScenario("update") : $model->setScenario("create");

            Yii::$app->response->format = 'json';
            return \yii\bootstrap\ActiveForm::validate($model);
        }
    }

    // Select staff by id store
    public function actionSelectAssignProductByStoreid() {
        $out = [];
        $result = [];
        if (isset($_POST['depdrop_parents'])) {
            $parents = $_POST['depdrop_parents'];
            if ($parents != null) {
                $store_id = $parents[0];
                if ($store_id == null) {
                    echo Json::encode(['output' => $result, 'selected' => '']);
                    return;
                } else {
                    $out = (new MstProduct)->listProductByStoreId($store_id);
                    foreach ($out as $key => $value) {
                        $arr = ['id' => $key, 'name' => $value];
                        $result[] = $arr;
                    }
                    echo Json::encode(['output' => $result, 'selected' => '']);
                    return;
                }
            }
        }
        echo Json::encode(['output' => '', 'selected' => '']);
    }

}
