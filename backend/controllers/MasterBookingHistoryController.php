<?php

namespace backend\controllers;

use Yii;
use common\models\MasterBookingHistory;
use common\models\MasterBookingHistorySearch;
use common\models\MstCategoriesProduct;
use common\models\MasterStaff;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\helpers\Json;

/**
 * MasterBookingHistoryController implements the CRUD actions for MasterBookingHistory model.
 */
class MasterBookingHistoryController extends Controller {

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all MasterBookingHistory models.
     * @return mixed
     */
    public function actionIndex() {
        $searchModel = new MasterBookingHistorySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        // Get name from table mst_categories_product
        $searchModelCategory = MstCategoriesProduct::listCategory();

        // Get name and id from table mst_staff
        $searchModelStaff = MasterStaff::listStaff();

//        \common\components\Util::dd($searchModelCategory);
        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'searchModelCategory' => $searchModelCategory,
                    'searchModelStaff' => $searchModelStaff,
        ]);
    }

    // Select staff by id store
    public function actionSelectStaffByIdStore() {
        $out = [];
        $result = [];
        $selected = '';
        //echo Json::encode(['output' => '', 'selected' => $selected, 'disabled' => 'true']);exit;
        if (isset($_POST['depdrop_parents'])) {
            $parents = $_POST['depdrop_parents'];
            if ($parents != null) {
                $store_id = $parents[0];
                if (!empty($_POST['depdrop_params'])) {
                    $params = $_POST['depdrop_params'];
                    $selected = $params[0];
                }
                if ($store_id == null) {
                    echo Json::encode(['output' => '', 'selected' => $selected]);
                    return;
                } else {
                    $out = (new \common\models\MasterStaff)->listStaffByStoreCT($store_id);
                    foreach ($out as $key => $value) {
                        $arr = ['id' => $key, 'name' => $value];
                        $result[] = $arr;
                    }
                    echo Json::encode(['output' => empty($result)? '' : $result , 'selected' => $selected]);
                    return;
                }
            }
        }
        echo Json::encode(['output' => '', 'selected' => $selected]);
    }

    // Select staff by id store
    public function actionSelectSeatByIdStore() {
        $out = [];
        $result = [];
        echo Json::encode(['output' => '', 'selected' => '']);
        exit;
        if (isset($_POST['depdrop_parents'])) {
            $parents = $_POST['depdrop_parents'];
            if ($parents != null) {
                $store_id = $parents[0];
                if ($store_id == null) {
                    echo Json::encode(['output' => $result, 'selected' => '', 'disabled' => 'true']);
                    return;
                } else {
                    $out = (new MasterStaff)->listStaff($store_id);
                    foreach ($out as $key => $value) {
                        $arr = ['id' => $key, 'name' => $value];
                        $result[] = $arr;
                    }
                    echo Json::encode(['output' => $result, 'selected' => '']);
                    return;
                }
            }
        }
        echo Json::encode(['output' => '', 'selected' => '']);
    }

    /**
     * Displays a single MasterBookingHistory model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id) {
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new MasterBookingHistory model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new MasterBookingHistory();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing MasterBookingHistory model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing MasterBookingHistory model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the MasterBookingHistory model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return MasterBookingHistory the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = MasterBookingHistory::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
