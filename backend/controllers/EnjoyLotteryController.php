<?php

namespace backend\controllers;

use Yii;
use common\models\MasterEnjoyLottery;
use common\models\MasterEnjoyLotterySearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use Exception;

/**
 * EnjoyLotteryController implements the CRUD actions for MasterEnjoyLottery model.
 */
class EnjoyLotteryController extends Controller
{
    //variable connection database
    public $db;
    
    public function init()
    {
        //get connection
        $this->db = Yii::$app->db;
    }
    /**
     * @inheritdoc
     */
    
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all MasterEnjoyLottery models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new MasterEnjoyLotterySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new MasterEnjoyLottery model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        try{
            $transaction = $this->db->beginTransaction();
            $model = new MasterEnjoyLottery();
            $searchModel = new MasterEnjoyLotterySearch();
            if (Yii::$app->request->isAjax) {    
                parse_str(Yii::$app->request->post()['form'],$form);
                $model->load($form);
                $model->earn_period_from_max = str_replace(',','',$model->earn_period_from_max);
                $model->earn_period_from_min = str_replace(',','',$model->earn_period_from_min);
                $list_member_search = $searchModel->searchCustomer($model);
                $customer = (!empty($model->customer_id_hd))?\common\models\MasterCustomer::findOne(['id' => $model->customer_id_hd]):null;
                return $this->render('create', [
                    'model' => $model,
                    'list_member_search' => !empty($list_member_search)?$list_member_search:null,
                    'customer' =>  $customer,
                    'count_winner'  =>  count($list_member_search->models)
                ]);
            }
            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                //commit transaction
                $transaction->commit();
                Yii::$app->getSession()->setFlash('success', [
                    'success' => Yii::t('backend', "Create Success")
                ]);
                return $this->redirect(['index']);
            } else {
                $model1 = new MasterEnjoyLottery();
                $model1->load($model);
                $model1->customer_id_hd = -1;

                $list_member_search = $searchModel->searchCustomer($model1);
                $customer = (!empty($model->customer_id_hd))?\common\models\MasterCustomer::findOne(['id' => $model->customer_id_hd]):null;
                return $this->render('create', [
                    'model' => $model,
                    'list_member_search' => !empty($list_member_search)?$list_member_search:null,
                    'customer' =>  $customer,
                    'count_winner'  =>  count($list_member_search->models)
                ]);
            }
        } catch (Exception $ex) {
            //rollback transaction
            $transaction->rollBack();
            //set error message
            Yii::$app->getSession()->setFlash('error', [
                'error'=>Yii::t('backend',"System error happen. Please contact with System manager.")
                ]);
              
            //return index
            return $this->redirect(['index']);
        }        
    }

    /**
     * Updates an existing MasterEnjoyLottery model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        try{
            $transaction = $this->db->beginTransaction();
            $model = self::findModel($id);
            $searchModel = new MasterEnjoyLotterySearch();
            if (Yii::$app->request->isAjax && Yii::$app->request->post()) {    
                parse_str(Yii::$app->request->post()['form'],$form);
                $model->load($form);
                $model->earn_period_from_max = str_replace(',','',$model->earn_period_from_max);
                $model->earn_period_from_min = str_replace(',','',$model->earn_period_from_min);
                $list_member_search = $searchModel->searchCustomer($model);
                $customer = (!empty($model->customer_id_hd))?\common\models\MasterCustomer::findOne(['id' => $model->customer_id_hd]):null;
                return $this->render('update', [
                    'model' => $model,
                    'list_member_search' => !empty($list_member_search)?$list_member_search:null,
                    'customer' =>  $customer,
                    'count_winner'  =>  count($list_member_search->models)
                ]);
            }
            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                //commit transaction
                $transaction->commit();
                Yii::$app->getSession()->setFlash('success', [
                    'success' => Yii::t('backend', "Update Success")
                ]);
                return $this->redirect(['index']);
            } else {
                $model->customer_id_hd = empty($model->customer_id_hd)?$model->member_specified:$model->customer_id_hd;

                $list_member_search = $searchModel->searchCustomer($model);
                $customer = (!empty($model->customer_id_hd))?\common\models\MasterCustomer::findOne(['id' => $model->customer_id_hd]):null;
                if(!empty($model->rank_id)){
                    $tmp = $model->rank_id;
                    if(!is_array($tmp)){
                        $tmp = explode(',', $tmp);
                    }
                    foreach ($tmp AS $index => $value)
                        $tmp[$index] = (int)$value;
                    $model->rank_id = $tmp;
                }
                return $this->render('update', [
                    'model' => $model,
                    'list_member_search' => !empty($list_member_search)?$list_member_search:null,
                    'customer' =>  $customer,
                    'count_winner'  =>  count($list_member_search->models)
                ]);
            }
        } catch (Exception $ex) {
            //rollback transaction
            $transaction->rollBack();
            //set error message
            Yii::$app->getSession()->setFlash('error', [
                'error'=>Yii::t('backend',"System error happen. Please contact with System manager.")
                ]);
              
            //return index
            return $this->redirect(['index']);
        }
    }

    /**
     * Deletes an existing MasterEnjoyLottery model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->softDelete();
        
        return $this->redirect(['index']);
    }

    /**
     * Finds the MasterEnjoyLottery model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return MasterEnjoyLottery the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = MasterEnjoyLottery::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
        

    // Validate From
    public function actionValidate() {

        $model = new MasterEnjoyLottery();
//        $model1->load(Yii::$app->request->post());
//        $model = new MasterCustomer();
//        $model = !empty($model1->id_hd_customer) ? MasterCustomer::findOne($model1->id_hd_customer) : $model;

//        $modelCustomerStore = new CustomerStore();

        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post()))
        {
            Yii::$app->response->format = 'json';
            return \yii\bootstrap\ActiveForm::validate($model);
//          return \yii\bootstrap\ActiveForm::validateMultiple([$model,$model2]);
        }
    }

    // Validate From
    public function actionValidateSearch() {

        $model = new MasterEnjoyLottery();
        parse_str(Yii::$app->request->post()['form'],$form);
        if (Yii::$app->request->isAjax && $model->load($form))
        {
//            Yii::$app->response->format = 'json';
            $validate =  \yii\bootstrap\ActiveForm::validate($model);
            unset($validate['masterenjoylottery-winner_announcement_date']);
            unset($validate['masterenjoylottery-winner_announcement_time']);
            unset($validate['masterenjoylottery-count_winner']);
            return json_encode($validate);
        }
    }
}
