<?php

$config = [
    'homeUrl' => '/',
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => '4EqtV5wATWmIWN9SNkkdb_t8jTdQiGye',
            'baseUrl' => '',
        ],
        'session' => [
            'name' => '_frontendSessionId', // unique for frontend
            'savePath' => __DIR__ . '/../runtime', // a temporary folder on frontend
        ],
    ],
];

return $config;
