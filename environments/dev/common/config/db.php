<?php

return [
    'class' => 'yii\db\Connection',
    'dsn' => 'pgsql:host=localhost;dbname=millionet', 
    'username' => 'postgres',
    'password' => 'postgres',
    'charset' => 'utf8',
    'schemaMap' => [
      'pgsql'=> [
        'class'=>'yii\db\pgsql\Schema',
        'defaultSchema' => 'public' //specify your schema here
      ]
    ], // PostgreSQL
];
//'dsn' => 'pgsql:host=139.162.50.62;dbname=milionet', 
//'dsn' => 'pgsql:host=192.168.1.122;dbname=milionet_final', 