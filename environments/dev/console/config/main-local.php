<?php
return [
    'bootstrap' => ['gii'],
    'modules' => [
        'gii' => 'yii\gii\Module',
    ],
    'components' =>[
       'mailer' => require(__DIR__ . '/mail.php'), 
    ]
];
