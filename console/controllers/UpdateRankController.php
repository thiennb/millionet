<?php

/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace console\controllers;

use yii\console\Controller;
use Yii;

/**
 * This command echoes the first argument that you have entered.
 *
 * This command is provided as an example for you to learn how to create console commands.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class UpdateRankController extends Controller {

    /**
     * This command echoes what you have entered as the message.
     * @param string $message the message to be echoed.
     */
    public function actionIndex() {
        $connection = Yii::$app->getDb();
        if(date('j') === '1') { 
            $command = $connection->createCommand('select update_rank_monthly()');
            $command->queryAll();
            Yii::error('[UPDATE RANK_MONTHLY] ERROR=pass function update rank');
        }
        
        $commandResetRankDaily = $connection->createCommand('select reset_rank_daily()');
        $commandResetRankDaily->queryAll();
        Yii::error('[RESET RANK DAILY] ERROR=pass function update rank');
    }
    
    /**
     * This command echoes what you have entered as the message.
     * @param string $message the message to be echoed.
     */
    public function actionDailyUpdate() {
        $connection = Yii::$app->getDb();
        
        $commandDailyUpdate = $connection->createCommand('select update_rank_daily()');
        $commandDailyUpdate->queryAll();
        
        //$command = (new \yii\db\Query())->select('select update_rank_daily()')->createCommand();
        //$command->queryAll();
    }
}
