<?php

namespace console\models;

use Yii;
use yii\base\Model;

/**
 * ContactForm is the model behind the contact form.
 */
class Test extends Model
{
    public $name;
    public $email;
    public $subject;
    public $body;
    public $verifyCode;


    

    /**
     * Sends an email to the specified email address using the information collected by this model.
     *
     * @param string $email the target email address
     * @return boolean whether the email was sent
     */
    public function setLog()
    {
        Yii::error('[TEST_SCHEDULE] '. (int)('10:10' > '10:11'));
    }
}
