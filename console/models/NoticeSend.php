<?php

namespace console\models;

use Yii;
//use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii2tech\ar\softdelete\SoftDeleteBehavior;
use common\models\NoticeCustomer;
use common\models\SiteSetting;
use DateTime;
use common\models\SettingAutoSendNotice;
use yii\data\ActiveDataProvider;
use common\models\BookingBusiness;
use Carbon\Carbon;
use common\models\MasterNotice;
/**
 * ContactForm is the model behind the contact form.
 */
class NoticeSend extends \yii\db\ActiveRecord
{
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'mst_notice';
    }

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            TimestampBehavior::className(),
//            BlameableBehavior::className(),
            'softDeleteBehavior' => [
                'class' => SoftDeleteBehavior::className(),
                'softDeleteAttributeValues' => [
                    'del_flg' => '1'
                ],
            ],
        ];
    }
    
    //token customer
    public $device_token;
    //user id
    public $user_id;
    //user id
    public $notice_id;
    //notice customer id
    public $notice_customer_id;
    //notice received flg
    public $notice_received_flg;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
        ];
    }   
    

    /**
     * Sends an email to the specified email address using the information collected by this model.
     *
     * @param string $email the target email address
     * @return boolean whether the email was sent
     */
    public function scheduleSendNotice()
    {
        $delay = SiteSetting::find()->andWhere(['type' => '01', 'key_1'=>'01', 'key_2' => '01'])->one();
        $now = DateTime::createFromFormat('Y-m-d H:i:s',date('Y-m-d H:i:s'));
        $delay_time = empty($delay->value_1)?0:($now->getTimestamp() - $delay->value_1*60);
        $arr_send_notice = self::find()
                ->join('INNER JOIN','notice_customer','notice_customer.notice_id = mst_notice.id')
                ->join('INNER JOIN','mst_customer','notice_customer.customer_id = mst_customer.id')
                ->join('INNER JOIN','user','"user"."id" = mst_customer.user_id')
                ->join('INNER JOIN','device_user','device_user.user_id = mst_customer.user_id')
                ->andWhere(['mst_notice.del_flg' => '0'])
                ->andWhere(['mst_customer.del_flg' => '0'])
                ->andWhere(['notice_customer.del_flg' => '0'])
                ->andWhere(['user.del_flg' => '0'])
                ->andWhere(['device_user.del_flg' => '0'])
                ->andWhere(['notice_customer.status_send' => '01'])
                ->andWhere(['<=','notice_customer.push_date' , $now->getTimestamp()])
                ->andWhere(['>=','notice_customer.push_date' , $delay_time])
                ->select([
                    'device_user.device_token as device_token',
                    'mst_notice.title',
                    'mst_notice.id as notice_id',
                    'mst_customer.user_id as user_id',
                    'notice_customer.id as notice_customer_id',
                    'device_user.notice_received_flg as notice_received_flg',
                ])
                ->all();
        $device = '';
        foreach ($arr_send_notice as $notice){
            if($notice->notice_received_flg == '0'){
                NoticeCustomer::updateAll(['status_send'=>MasterNotice::STATUS_SEND_ERROR],['id' => $notice->notice_customer_id]);
                continue;
            }
            $device .= '----------'.$notice->device_token;
            if(empty($notice->device_token)){
                continue;
            }
            $device_token = explode(':',$notice->device_token);
            if(count($device_token) != 2 || empty($device_token[0]) || empty($device_token[1])){
                continue;
            }
            $tokens = ["$device_token[0]:$device_token[1]"];
            $json = $this->send_notification_notice($tokens,$notice->title,$notice->notice_id,$notice->user_id);
            $result = json_decode($json);
            if(!empty($result) && $result->success == 1){
                NoticeCustomer::updateAll(['status_send'=>MasterNotice::STATUS_SEND_DONE],['id' => $notice->notice_customer_id]);
            }else{
                NoticeCustomer::updateAll(['status_send'=>MasterNotice::STATUS_SEND_ERROR],['id' => $notice->notice_customer_id]);
                Yii::error('[NOTICE_SCHEDULE] ERROR='.$json);
            }
        }
    }
    

    /**
     * Detail notice send for customer by auto
     * parameter integer $id
     * @return mixed
     */
    public static function send_notification_notice($tokens, $message, $notice_id, $user_id) {
        try {
            $url = 'https://fcm.googleapis.com/fcm/send';
            //content send
            $fields = array(
                'registration_ids' => $tokens,
                'content_available' => true,
                "priority" => "high",
                "notification" => [
                    "body" => $message,
                    "badge" => "0",
                    "sound" => "default",
                    "click_action" => "openMain"
                ],
                'data' => [
                    "notice_id"=> $notice_id,
                    'user_id'   => $user_id
                ]
            );
            $headers = array(
                //server key
                'Authorization:key=AIzaSyBzrcWv-tNQcCbyGtstIRwhrJAQaY8-zx4',
                'Content-Type: application/json'
            );
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
            $result = curl_exec($ch);
            if ($result === FALSE) {
                die('Curl failed: ' . curl_error($ch));
            }
            curl_close($ch);
            return $result;
            
        } catch (Exception $ex) {
            //set error message
            Yii::$app->getSession()->setFlash('error', [
                'error' => Yii::t('backend', "System error happen. Please contact with System manager.")
            ]);
            //return index
            return $this->redirect(['/']);
        }
    }
    
    /**
     * Sends an email to the specified email address using the information collected by this model.
     *
     * @param string $email the target email address
     * @return boolean whether the email was sent
     */
    public function scheduleSendNoticeDistance($customer_id = null, $device_token = null, $store_id = null, $distance = null)
    {
        $query      = SettingAutoSendNotice::find();
        $modelTemp  = new SettingAutoSendNotice(); 
        $dataProvider = new \yii\data\ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => false,
            ],
            'sort' => false
        ]);
        $push_date = 0;
        $model                = \common\models\MasterCustomer::findOne($customer_id);
        $modelCustormerStore  = \common\models\CustomerStore::find()
                                ->where(['customer_id' => $customer_id, 'store_id' => $store_id])
                                ->one();
        $params               = \common\models\BookingBusiness::convertModelToArray($model,$modelCustormerStore);
        $query->andWhere(['push_timming'=> \common\models\BookingBusiness::PUSH_TIMING_DISTANCE]);
        $query->andWhere(['distance_to_shop'=> $distance]);
        $push_date            = \Carbon\Carbon::now()->timestamp;

//                        \common\components\Util::d($params);
        //filter by param 
        foreach ($params as $key => $value ){
            if($modelTemp->hasAttribute($key) && $key !='black_list_flg'  ){
                $query->andWhere(['or',['=',$key,  empty($value) ? null : $value],['is', $key ,null]]);
            }
            if($key == 'birth_date'){
                $query->andWhere(['or',['<=','start_birth_date',$value],['is', 'start_birth_date' ,null]])
                      ->andWhere(['or',['>=','end_birth_date',$value],['is', 'end_birth_date' ,null]]);
            }
            if($key == 'last_visit_date'){
                $query->andWhere(['or',['<=','start_visit_date',$value],['is', 'start_visit_date' ,null]])
                      ->andWhere(['or',['>=','end_visit_date',$value],['is', 'end_visit_date' ,null]]);
            }
            if($key == 'number_visit'){
                $query->andWhere(['or',['>=','min_visit_number',$value],['is', 'min_visit_number' ,null]])
                      ->andWhere(['or',['<=','max_visit_number',$value],['is', 'max_visit_number' ,null]]);
            }
            if($key == 'black_list_flg' && $value == '1'){
                $query->andWhere(['or',['=','black_list_flg',$value],['is', 'black_list_flg' ,null]]);
            }
            if ($key == 'last_staff_id' && !empty($value)) {
                $command = "(SELECT order_detail.order_management_id staff_id FROM mst_order WHERE customer_jan_code = 
                        (SELECT customer_jan_code FROM mst_customer WHERE id = customer_store.customer_id) ORDER BY mst_order.process_date DESC, mst_order.process_time DESC LIMIT 1)";
                $query->andWhere([
                    'IN',$command, $value
                ]);
            }
        }
        $query->andWhere(['status_send' => BookingBusiness::NOTICE_STATUS_ENABLE]);
        //get result from query
        $result  = $dataProvider->getModels();
        $transaction = Yii::$app->db->beginTransaction();
        if(!empty($result)){
            foreach ($result as $modelNotice) {
                $device_user = \common\models\DeviceUser::find()->andWhere(['user_id' => $model->user_id,'device_token' => $device_token,'notice_received_flg' => '1'])->one();
                $notice = MasterNotice::findOne($modelNotice->notice_id);
                $result_send = '';
                if(!empty($device_user) && !empty($notice)){
                    $tmp_device_token = explode(':',$device_user->device_token);
                    if(count($tmp_device_token) != 2 || empty($tmp_device_token[0]) || empty($tmp_device_token[1])){
                        continue;
                    }
                    $tokens = ["$tmp_device_token[0]:$tmp_device_token[1]"];
                    $result_send = \console\models\NoticeSend::send_notification_notice($tokens, $notice->title, $notice->id, $model->user_id);
                }else{
                    return FALSE;
                }
                $notice_custormer              = new NoticeCustomer();
                $notice_custormer->notice_id   = $modelNotice->notice_id ;
                $notice_custormer->customer_id = $customer_id ;
                $notice_custormer->store_id    = $store_id ;
                if(!empty($result_send)){
                    $result = json_decode($result_send);
                    if(!empty($result) && $result->success == 1){
                        $notice_custormer->status_send = MasterNotice::STATUS_SEND_DONE ;
                    }else{
                        $notice_custormer->status_send = MasterNotice::STATUS_SEND_ERROR ;
                    }
                }else{
                    $notice_custormer->status_send = MasterNotice::STATUS_SEND_ERROR ;
                }
                $notice_custormer->push_date   = $push_date ;
                $notice_custormer->save();
            }
            $transaction->commit();
            return TRUE;
        }else{
            return FALSE;
        }
    }
}
