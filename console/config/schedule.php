<?php

/**
 * @var \omnilight\scheduling\Schedule $schedule
 */


// Place here all of your cron jobs
//$schedule->call(function()
//{
//    $notice_send = new \console\models\NoticeSend();
//    $notice_send->scheduleSendNotice();
//})->everyMinute();

$schedule->command('notice-send')->everyMinute();
$schedule->command('update-rank')->dailyAt('12:10');
$schedule->command('update-rank/daily-update')->dailyAt('23:30');
