<?php
namespace frontend\controllers;

use Yii;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use frontend\models\ContactForm;
use Exception;
use common\models\Company;
use common\components\Util;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['get'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        //return $this->render('index');
        return $this->redirect(['/home-test']);
    }

    /**
     * Logs in a user.
     *
     * @return mixed
     */
    public function actionLogin()
    {
        try{
            if (!Yii::$app->user->isGuest) {
                return $this->goHome();
            }

            $model = new LoginForm();
            $model->scenario = 'login_phone';
            if ($model->load(Yii::$app->request->post()) && $model->loginPhone()) {
                // Set session company_code
                $session = Yii::$app->session;
                //$session->set('store_id_rg', $model->company_code);
                $login_redirect = $session->get('login_redirect');
                
                $company = Company::find()->innerJoin('mst_customer','mst_customer.company_id = company.id')
                        ->andWhere(['mst_customer.user_id' => Yii::$app->user->id])->one();
                // Set cookies company store
                if(!empty($company)){
//                    Util::setCookiesCompanyCode($company->company_code);
                    // Set cookies company id store
                    Util::setCookiesCompanyId($company->id);
                }
                
                if(isset($login_redirect) && !empty($login_redirect)){
                    return $this->redirect($login_redirect);
                }
                return $this->goBack();
            } else {
                $model->password = '';
                return $this->render('login', [
                    'model' => $model,
                ]);
            }
        }  catch (Exception $e){
            Yii::$app->session->setFlash('error', [
                'error'=>Yii::t('frontend',"System error happen. Please contact with System manager.")
            ]);
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }
    /**
     * Logs in a user.
     *
     * @return mixed
     */
    public function actionLoginEmail()
    {
        try{
            if (!Yii::$app->user->isGuest) {
                return $this->goHome();
            }

            $model = new LoginForm();
            $model->scenario = 'login_email';
            if ($model->load(Yii::$app->request->post()) && $model->loginEmail()) {
                // Set session company_code
                $session = Yii::$app->session;
//                $session->set('store_id_rg', $model->company_code);
                $login_redirect = $session->get('login_redirect');
                if(isset($login_redirect) && !empty($login_redirect)){
                    return $this->redirect($login_redirect);
                }
                return $this->goBack();
            } else {
                $model->password = '';
                return $this->render('login_email', [
                    'model' => $model,
                ]);
            }
        }  catch (Exception $e){
            Yii::$app->session->setFlash('error', [
                'error'=>Yii::t('frontend',"Email or password is incorrect.")
            ]);
            return $this->render('login_email', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();
        
        return $this->redirect(['afterlogout']);
    }

    /**
     * Displays contact page.
     *
     * @return mixed
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail(Yii::$app->params['adminEmail'])) {
                Yii::$app->session->setFlash('success', 'Thank you for contacting us. We will respond to you as soon as possible.');
            } else {
                Yii::$app->session->setFlash('error', 'There was an error sending email.');
            }

            return $this->refresh();
        } else {
            return $this->render('contact', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Displays about page.
     *
     * @return mixed
     */
    public function actionAbout()
    {
        return $this->render('about');
    }

    /**
     * Signs user up.
     *
     * @return mixed
     */
    public function actionSignup()
    {
        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->signup()) {
                if (Yii::$app->getUser()->login($user)) {
                    return $this->goHome();
                }
            }
        }

        return $this->render('signup', [
            'model' => $model,
        ]);
    }

    /**
     * Requests password reset.
     *
     * @return mixed
     */
    public function actionRequestPasswordReset()
    {
        try {
            $company_id = \common\components\Util::getCookiesCompanyId();
            if(empty($company_id)){
                return $this->redirect(['/']);
            }

            $model = new PasswordResetRequestForm();
                $model->setScenario('request_reset');
            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                $user = $model->checkUser($company_id);
                if (!empty($user)) {
    //                Yii::$app->session->setFlash('success', Yii::t('backend','For detail instruction, please confirm email'));                

                    if (!\common\models\User::isPasswordResetTokenValid($user->password_reset_token)) {
                        $user->generatePasswordResetToken();
                        $user->save();
                    }
                    return $this->redirect(['reset-password-by-phone','user_id' => $user->id,'token' => $user->password_reset_token]);
                } else {
                    Yii::$app->session->setFlash('error', Yii::t('frontend',"Can't find member with input value."));
                }
            }

            return $this->render('requestPasswordResetToken', [
                'model' => $model,
            ]);
        } catch (\ErrorException $e) {
            Yii::$app->session->setFlash('error', [
                'error'=>Yii::t('frontend',"System error happen. Please contact with System manager.")
            ]);
            return $this->redirect(['login']);
        }
    }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPasswordByPhone($user_id, $token)
    {
        try {
            $model = new PasswordResetRequestForm();
            $model->setScenario('reset_password');
        

            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                if($model->resetPassword($user_id, $model->password,$token)){
                    Yii::$app->session->setFlash('success', Yii::t('frontend','The new password was successfully saved.'));
                }else{
                    Yii::$app->session->setFlash('error', Yii::t('frontend',"Cannot reset password"));
                }
                return $this->redirect(['login']);
            }

            return $this->render('resetPasswordByPhone', [
                'model' => $model,
            ]);
        } catch (\ErrorException $e) {
            Yii::$app->session->setFlash('error', [
                'error'=>Yii::t('frontend',"System error happen. Please contact with System manager.")
            ]);
            return $this->redirect(['login']);
        }
    }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token)
    {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', Yii::t('backend','The new password was successfully saved.'));

            return $this->goHome();
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }
    
    public function actionAfterlogout() {
        return $this->render('afterlogout');

    }
}
