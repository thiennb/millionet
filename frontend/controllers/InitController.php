<?php

namespace frontend\controllers;

use common\models\Company;
use Yii;
use yii\web\Controller;
use yii\base\Exception;
use common\models\MasterStore;
use common\models\MasterStaff;
use common\models\Init;

/**
 * Site controller
 */
class InitController extends Controller {

    /**
     * @inheritdoc
     */
    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex() {
        $model = new Init();

        $result = $model->find();

        return $this->render('init', ['model' => $model, 'result' => $result]);
    }

    // Dat code test insert company, store , staff 
    public function actionCreate() {
        try {

            $model = new Init();
            $result = $model->find();
            $date = date_create();

            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                //create transaction
                $transaction = Yii::$app->db->beginTransaction();

                $model_company = new Company();
                $model_company->detachBehaviors();
                $model_company->company_code = $model->company_code;
                $model_company->name = $model->name_company;
                $model_company->del_flg = '0';
                $model_company->created_at = date_timestamp_get($date);
                $model_company->created_by = 1;
                $model_company->updated_at = date_timestamp_get($date);
                $model_company->updated_by = 1;

                if ($model_company->save()) {

                    //$count_store = MasterStore::find()->where(['mst_store.del_flg' => '0'])->one();
                    $model_store = new MasterStore();
                    $model_store->detachBehaviors();
                    $model_store->company_id = $model_company->id;
                    $code_store_max = $model_store->generateStoreCode();
                    //if ($count_store == null){
                    $model_store->store_code = '00000';
//                    }else{
//                        $model_store->store_code = $code_store_max;
//                    }
                    $model_store->time_open = '08:00';
                    $model_store->time_close = '18:00';
                    $model_store->booking_resources = '01';
                    $model_store->name = $model->name_store;
                    $model_store->created_at = date_timestamp_get($date);
                    $model_store->created_by = 1;
                    $model_store->updated_at = date_timestamp_get($date);
                    $model_store->updated_by = 1;

                    if ($model_store->save()) {

                        $model_shift_store = new \common\models\StoreShift();
                        $model_shift_store->detachBehaviors();
                        $model_shift_store->store_id = $model_store->id;
                        $model_shift_store->name = '全日';
                        $model_shift_store->short_name = '全日';
                        $model_shift_store->start_time = '08:00';
                        $model_shift_store->end_time = '18:00';
                        $model_shift_store->created_at = date_timestamp_get($date);
                        $model_shift_store->created_by = 1;
                        $model_shift_store->updated_at = date_timestamp_get($date);
                        $model_shift_store->updated_by = 1;
                        
                        if ($model_shift_store->save()) {
                            $model_staff = new MasterStaff;
                            $model_staff->detachBehaviors();
                            $model_staff->saveManagementIdInit($model->pass_word);
                            $model_staff->name = $model->name_staff;
                            $model_staff->email = 'test@gmail.com';
                            $model_staff->permission_id = 3;
                            $model_staff->short_name = $model->short_name_staff;
                            $model_staff->store_id = $model_store->id;
                            $model_staff->created_at = date_timestamp_get($date);
                            $model_staff->created_by = 1;
                            $model_staff->updated_at = date_timestamp_get($date);
                            $model_staff->updated_by = 1;
                            if ($model_staff->save()) {
                                $transaction->commit();
                                Yii::$app->session->setFlash('success', Yii::t("backend", "Create Success"));
                                return $this->render('init', ['model' => $model, 'result' => $result]);
                            }
                        }
                    }
                }
            } else {
                return $this->render('init', ['model' => $model, 'result' => $result]);
            }

            // Commit Transaction                    
        } catch (Exception $ex) {
            $transaction->rollBack();
            Yii::$app->getSession()->setFlash('error', [
                'error' => Yii::t('backend', "System error happen. Please contact with System manager.")
            ]);
            return $this->render('init', ['model' => $model, 'result' => $result]);
        }
    }

}
