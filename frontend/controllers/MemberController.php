<?php

namespace frontend\controllers;

use Yii;
use common\models\MasterMember;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\models\User;
use common\models\MasterCustomer;
use Exception;
use common\models\PostCode;
use common\models\CustomerStore;
use common\models\MasterRank;
use common\models\CompanyStore;
use DateTime;
use common\models\BookingBusiness;

/**
 * MasterMemberController implements the CRUD actions for MasterMember model.
 */
class MemberController extends Controller {

    //variable connection database
    public $db;

    public function init()
    {
        //get connection
        $this->db = Yii::$app->db;
    }
    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Link to create member step 1
     * @return mixed
     */
    public function actionIndex() {
        if (!$this->valid()) return $this->errorUserDetected();
        //check exists session
        $session = Yii::$app->session;
        //remove session
        $session->remove('Step_1_mobile');
        $session->remove('Step_1_email');
        $session->remove('Key_confirm_email');
        $session->remove('Step_2');
        $session->remove('Step_3');

        return $this->redirect(['step1']);
    }

    /**
     * Create member step 1
     * @return mixed
     */
    public function actionStep1() {
        if (!$this->valid()) return $this->errorUserDetected();
        try{
            //check exists session
            $session = Yii::$app->session;
            $session->remove('Step_1_email');
            if (empty($session->get('Step_1_mobile'))) {
                $model = new MasterMember();
            }else{
                $model = new MasterMember();
                $model->mobile = $session->get('Step_1_mobile');
            }
            //set Scenario for rules validate in model
            $model->setScenario('create_phone');

            //check init or submit form
            if ($model->load(Yii::$app->request->post()) && $model->validate()) {//submit form
                //mobile
                $mobile = $model->phone_1.$model->phone_2.$model->phone_3;
                //check phone exists
                $phone = MasterCustomer::findOne(['mobile'=>$mobile]);
                if(!empty($phone->mobile)){//if exists phone in database
                    $model->addError('phone_1', Yii::t('frontend',"Sory Phone Number was been used."));
                    $model->addError('phone_2', '');
                    $model->addError('phone_3', '');
                    return $this->render('step1', [
                            'model' => $model,
                    ]);
                }
                $session->set('Step_1_mobile', $mobile);
                return $this->redirect(['step2']);
            } else {//init                
                if(strlen($model->mobile) == 10){
                    $model->phone_1 = substr($model->mobile, 0, 2);
                    $model->phone_2 = substr($model->mobile, 2, 4);
                    $model->phone_3 = substr($model->mobile, 6, 4);
                } else{
                    $model->phone_1 = substr($model->mobile, 0, 3);
                    $model->phone_2 = substr($model->mobile, 3, 4);
                    $model->phone_3 = substr($model->mobile, 7, 4);
                }
                return $this->render('step1', [
                            'model' => $model,
                ]);
            }
        } catch (Exception $ex) {//if have exception
            //set error message
            Yii::$app->getSession()->setFlash('error', [
                'error'=>Yii::t('frontend',"System error happen. Please contact with System manager.")
                ]);
            //return index
            return $this->redirect(['/']);
        }
    }



    /**
     * Create member step 1
     * @return mixed
     */
    public function actionStep1Email() {
        if (!$this->valid()) return $this->errorUserDetected();
        try{
            //check exists session
            $session = Yii::$app->session;
            $session->remove('Step_1_mobile');
            if (empty($session->get('Step_1_email'))) {
                $model = new MasterMember();
            }else{
                $model = new MasterMember();
                $model->email = $session->get('Step_1_email');
            }
            //set Scenario for rules validate in model
            $model->setScenario('step_1_email');
            //check init or submit form
            if ($model->load(Yii::$app->request->post()) && $model->validate()) {//submit form
                //check phone exists
                $email = MasterCustomer::findOne(['email'=>$model->email]);
                if(!empty($email->email)){//if exists phone in database
                    $model->addError('email', Yii::t('frontend', "Sory Email was been used."));
                    return $this->render('step1_email', [
                            'model' => $model,
                    ]);
                }
                $model->key_confirm_again = Yii::$app->security->generateRandomString(6);
                $model->sendKeyConfirmEmail($model);
                $session->set('Step_1_email', $model->email);
                $key_confirm_email = [];
                $key_confirm_email['key'] =    $model->key_confirm_again;
                $key_confirm_email['time'] =    date('Y-m-d H:i:s');
                $session->set('Key_confirm_email', $key_confirm_email);
                return $this->redirect('step2');
            } else {//init
                return $this->render('step1_email', [
                            'model' => $model,
                ]);
            }
        } catch (Exception $ex) {//if have exception
            //set error message
            Yii::$app->getSession()->setFlash('error', [
                'error'=>Yii::t('frontend',"System error happen. Please contact with System manager.")
                ]);
            //return index
            return $this->redirect(['step1-email']);
        }
    }

    /**
     * Input key confirm if register by phone
     * @return mixed
     */
    public function actionStep2() {
        if (!$this->valid()) return $this->errorUserDetected();
        try {
            //get session
            $session = Yii::$app->session;
            if(!empty($session->get('Step_1_mobile'))){
                $model = new MasterMember();
                $model->mobile = $session->get('Step_1_mobile');
                $model->mode = MasterMember::REGISTER_PHONE;
            }else if(!empty($session->get('Step_1_email'))){
                $model = new MasterMember();
                $model->email = $session->get('Step_1_email');
                $model->mode = MasterMember::REGISTER_EMAIL;
                $key_confirm_again = $session->get('Key_confirm_email');
                $model->key_confirm_again = $key_confirm_again['key'];
                $now = DateTime::createFromFormat('Y-m-d H:i:s',date('Y-m-d H:i:s'));
                $time_key = DateTime::createFromFormat('Y-m-d H:i:s',date('Y-m-d H:i:s',  strtotime($key_confirm_again['time'])));
                if(($now->getTimestamp() - $time_key->getTimestamp()) > 30*60){
                    //set error message
                    Yii::$app->getSession()->setFlash('error', [
                        'error'=>Yii::t('frontend',"Time of the authentication key input has exceeded the time limit.")
                        ]);
                    //return index
                    return $this->redirect(['step1-email']);
                }
            }else{
                Yii::$app->getSession()->setFlash('error', [
                    'error'=>Yii::t('frontend',"You must create step 1!")
                ]);
                return $this->redirect('step1');
            }
            //set Scenario for rules validate in model
            $model->setScenario('step_2');
            //check init or submit
            if ($model->load(Yii::$app->request->post()) && $model->validate()) {//if submit
                if ($model->mode == MasterMember::REGISTER_EMAIL && !($model->key_confirm == $model->key_confirm_again)) {
                    $model->addError('key_confirm', Yii::t('frontend',"Key confirm is not correct."));
                    return $this->render('step2', [
                                'model' => $model,
                    ]);
                }
                //save data in session
                $session->set('Step_2',  'true');
                return $this->redirect('step3');
            } else {//if init
                return $this->render('step2', [
                            'model' => $model,
                ]);
            }
        } catch (Exception $ex) {
            //set error message
            Yii::$app->getSession()->setFlash('error', [
                'error'=>Yii::t('frontend',"System error happen. Please contact with System manager.")
                ]);
            //return index
            return $this->redirect(['step1']);
        }

    }

    /**
     * Input info member
     * @return mixed
     */
    public function actionStep3() {
        if (!$this->valid()) return $this->errorUserDetected();
        try {
            //get session
            $session = Yii::$app->session;
            if (!empty($session->get('Step_3'))) {
                $model = new MasterMember();
                $model->load($session->get('Step_3')->post());
                if(!empty($session->get('Step_1_mobile'))){
                    $model->mobile = $session->get('Step_1_mobile');
                    $model->mode = MasterMember::REGISTER_PHONE;
                }else{
                    $model->email = $session->get('Step_1_email');
                    $model->mode = MasterMember::REGISTER_EMAIL;
                }
            }else if(!empty($session->get('Step_1_mobile'))  && !empty($session->get('Step_2'))){
                $model = new MasterMember();
                $model->mobile = $session->get('Step_1_mobile');
                $model->mode = MasterMember::REGISTER_PHONE;
            }else if(!empty($session->get('Step_1_email'))  && !empty($session->get('Step_2'))){
                $model = new MasterMember();
                $model->email = $session->get('Step_1_email');
                $model->mode = MasterMember::REGISTER_EMAIL;
            }else{
                Yii::$app->getSession()->setFlash('error', [
                    'error'=>Yii::t('frontend',"You must create step 1!")
                ]);
                return $this->redirect('step1');
            }
            //set Scenario for rules validate in model
            ($model->mode == MasterMember::REGISTER_EMAIL)?$model->setScenario('step_3_email'):$model->setScenario('step_3');

            //check init or submit
            if ($model->load(Yii::$app->request->post()) && $model->validate()) {//if submit form
                //check exists email
                $email = MasterCustomer::findOne(['email'=>$model->email]);
                if(!empty($email->email)){
                    $key = 'email';
                    $model->addError($key, Yii::t('frontend',"Sory Email was been used."));
                    return $this->render('step3', [
                                'model' => $model,
                    ]);
                }
                //check exists phone
                $model->mobile = (!empty($model->mobile))?$model->mobile:$model->phone_1.$model->phone_2.$model->phone_3;
                $phone = MasterCustomer::findOne(['mobile'=>$model->mobile]);
                if(!empty($phone->mobile)){
                    $model->addError('phone_1', Yii::t('frontend',"Sory Phone Number was been used."));
                    $model->addError('phone_2', '');
                    $model->addError('phone_3', '');
                    return $this->render('step3', [
                                'model' => $model,
                    ]);
                }
                //save data in session
                $session->set('Step_3', Yii::$app->request);
                return $this->redirect('step4');
            } else {//if init
                return $this->render('step3', [
                            'model' => $model,
                ]);
            }
        } catch (Exception $ex) {
            //set error message
            Yii::$app->getSession()->setFlash('error', [
                'error'=>Yii::t('frontend',"System error happen. Please contact with System manager.")
                ]);
            //return index
            return $this->redirect(['step1']);
        }
    }

    /**
     * Confirm input
     * @return mixed
     */
    public function actionStep4() {
        if (!$this->valid()) return $this->errorUserDetected();
        try {
            //create transaction
            $transaction = $this->db->beginTransaction();
            //get session
            $session = Yii::$app->session;
            if ( empty($session->get('Step_3')) ){
                Yii::$app->getSession()->setFlash('error', [
                    'error'=>Yii::t('frontend',"You must create step 1!")
                ]);
                return $this->redirect('step1');
            }
            $model = new MasterMember();
            $model->mobile = $session->get('Step_1_mobile');
            $model->email = $session->get('Step_1_email');
            $model->load($session->get('Step_3')->post());
            $model->first_name_kana = mb_convert_kana($model->first_name_kana,'k');
            $model->last_name_kana = mb_convert_kana($model->last_name_kana,'k');
            //check init or submit form
            if (Yii::$app->request->post()) {//if submit
                $model->mobile = (!empty($model->mobile))?$model->mobile:$model->phone_1.$model->phone_2.$model->phone_3;
                // Get session store_id
                $store_id = $session->get('store_id_rg');
                //save database
                $model->register_store_id = $store_id;
                //save data
                $model->saveData($model);
                //send email
                if(!empty($model->email)){
                    $model->sendEmailRegister($model);
                }
                BookingBusiness::PushMessage(\common\models\BookingBusiness::PUSH_TIMING_REGISTER_MEMBER,$model->id);
                BookingBusiness::PushMessage(\common\models\BookingBusiness::PUSH_TIMING_REGISTER_MEMBER_AFTER,$model->id);
                //remove session
                $session->remove('Step_1_mobile');
                $session->remove('Step_1_email');
                $session->remove('Key_confirm_email');
                $session->remove('Step_2');
                $session->remove('Step_3');
                $transaction->commit();
                return $this->redirect(['step5','id'=>$model->id]);
            } else {//if init
                if(!empty($model->mobile)){
                    if(strlen($model->mobile) == 10){
                        $model->phone_1 = substr($model->mobile, 0, 2);
                        $model->phone_2 = substr($model->mobile, 2, 4);
                        $model->phone_3 = substr($model->mobile, 6, 4);
                    } else{
                        $model->phone_1 = substr($model->mobile, 0, 3);
                        $model->phone_2 = substr($model->mobile, 3, 4);
                        $model->phone_3 = substr($model->mobile, 7, 4);
                    }
                }
                return $this->render('step4', [
                            'model' => $model,
                ]);
            }
        } catch (Exception $ex) {
            //rollback transaction
            $transaction->rollBack();
            //set error message
            Yii::$app->getSession()->setFlash('error', [
                'error'=>Yii::t('frontend',"System error happen. Please contact with System manager.")
                ]);
            //return index
            return $this->redirect(['step3']);
        }
    }

    /**
     * Process after create member
     * @return mixed
     */
    public function actionStep5($id) {
        if (!$this->valid_login()) return $this->errorUserDetected();
        try{
            //find member
    //        $model = self::findModel($id);
            if (Yii::$app->request->post()) {//if submit form and validate success
                $login_redirect = Yii::$app->session->get('login_redirect');
                if(isset($login_redirect) && !empty($login_redirect)){
                    return $this->redirect($login_redirect);
                }
                return $this->redirect('/');
            } else {//if init
                return $this->render('step5', [
    //                'model' => $model,
                ]);
            }
        }  catch (Exception $ex){
            //set error message
            Yii::$app->getSession()->setFlash('error', [
                'error'=>Yii::t('frontend',"System error happen. Please contact with System manager.")
                ]);
            //return index
            return $this->redirect(['step3']);
        }
    }

    /**
     * Update info of member
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
        if (!$this->valid_login()) return $this->errorUserDetected();
        try {
            //get session
            $session = Yii::$app->session;
            //find member
            $model = self::findModel($id);
            $request = $session->get('Update_member_request');
            if (!empty($request) && !empty($id)) {
                $model->load($request->post());
                $model->post_code = $model->post_code_1.$model->post_code_2;
            }
            if ($model->user_id != Yii::$app->user->id) {
                Yii::$app->getSession()->setFlash('error', [
                    'error'=>Yii::t('frontend',"System error happen. Please contact with System manager.")
                    ]);
                //return index
                return $this->redirect(['/']);
            }
            //set Scenario for rules validate
            $model->setScenario('update');
            $prefecture = PostCode::getListPrefecture();
            //check init or submit
            if ($model->load(Yii::$app->request->post()) && $model->validate()) {//if submit form and validate success
                $session->set('Update_member_request', Yii::$app->request);
                return $this->redirect(['update-confirm','id'=>$id]);
            } else {//if init
                if(!empty($model->post_code)){
                    $model->post_code_1 = substr($model->post_code,0,3);
                    $model->post_code_2 = substr($model->post_code,3,4);
                };
                return $this->render('update', [
                    'model' => $model,
                    'prefecture'   =>  $prefecture,
                ]);
            }
        } catch (Exception $ex) {
            //set error message
            Yii::$app->getSession()->setFlash('error', [
                'error'=>Yii::t('frontend',"System error happen. Please contact with System manager.")
                ]);
            //return index
            return $this->redirect(['/']);
        }
    }
    
    /**
     * Confirm update member
     * @return mixed
     */
    public function actionUpdateConfirm($id) {
        if (!$this->valid_login()) return $this->errorUserDetected();
        try {
            //get session
            $session = Yii::$app->session;
            $request = $session->get('Update_member_request');
            if (!empty($request) && !empty($id)) {
                $model = self::findModel($id);
                $model->load($request->post());
                $model->post_code_1 = $request->post()['MasterMember']['post_code_1'];
                $model->post_code_2 = $request->post()['MasterMember']['post_code_2'];
                $model->first_name_kana = mb_convert_kana($model->first_name_kana,'k');
                $model->last_name_kana = mb_convert_kana($model->last_name_kana,'k');
                $model->post_code = $model->post_code_1.$model->post_code_2;
            }else{
                //set error message
                Yii::$app->getSession()->setFlash('error', [
                    'error'=>Yii::t('frontend',"System error happen. Please contact with System manager.")
                    ]);
                //return index
                return $this->redirect(['update','id'=>$id]);
            }            
            if ($model->user_id != Yii::$app->user->id) {
                Yii::$app->getSession()->setFlash('error', [
                    'error'=>Yii::t('frontend',"System error happen. Please contact with System manager.")
                    ]);
                //return index
                return $this->redirect(['/']);
            }
            //create transaction
            $transaction = $this->db->beginTransaction();
            //check init or submit
            if (Yii::$app->request->post() && $model->save()) {//if submit form and save success
                $transaction->commit();
                $session->remove('Update_member_request');
                return $this->redirect(['update-complete', 'id' => $id]);
            } else {//if init
                //init data for field in screen
                return $this->render('update_confirm', [
                            'model' => $model,
                ]);
            }
        } catch (Exception $ex) {
            //rollback transaction
            $transaction->rollBack();
            //set error message
            Yii::$app->getSession()->setFlash('error', [
                'error'=>Yii::t('frontend',"System error happen. Please contact with System manager.")
                ]);
            //return index
            return $this->redirect(['/']);
        }
    }

    /**
     * Confirm update member
     * @return mixed
     */
    public function actionUpdateComplete($id) {
        if (!$this->valid_login()) return $this->errorUserDetected();
        try {
            //init data for field in screen
            return $this->render('update_complete', [
                        'id' => $id,
            ]);
        } catch (Exception $ex) {
            //set error message
            Yii::$app->getSession()->setFlash('error', [
                'error'=>Yii::t('frontend',"System error happen. Please contact with System manager.")
                ]);
            //return index
            return $this->redirect(['/']);
        }
    }
    /**
     * Update phone of member
     * @param integer $id
     * @return mixed
     */
    public function actionUpdatePhone($id) {
        if (!$this->valid_login()) return $this->errorUserDetected();
        try {
            //get session
            $session = Yii::$app->session;
            //find member
            $model = self::findModel($id);
            
            if ($model->user_id != Yii::$app->user->id) {
                Yii::$app->getSession()->setFlash('error', [
                    'error'=>Yii::t('frontend',"System error happen. Please contact with System manager.")
                    ]);
                //return index
                return $this->redirect(['/']);
            }
            //set Scenario for rules validate
            $model->setScenario('update_phone');
            $model->birth_date = !empty($model->birth_date)?date('Y/m/d', strtotime($model->birth_date)):$model->birth_date;
            //check init or submit
            if ($model->load(Yii::$app->request->post()) && $model->validate()) {//if submit
                $mobile = $model->phone_1_new.$model->phone_2_new.$model->phone_3_new;
                $check_mobile = MasterMember::find()->andWhere(['<>','id',$id])->andWhere(['mobile'=>$mobile])->one();
                if(!empty($check_mobile)){
                    $model->addError('phone_1_new', Yii::t('frontend', "Sory Phone Number was been used."));
                    $model->addError('phone_2_new', '');
                    $model->addError('phone_3_new', '');                    
                    return $this->render('update_phone', [
                                'model' => $model,
                    ]);
                }
                //save data in session
                $session->set('Update_phone_request', $mobile);
                return $this->redirect(['update-phone-key-confirm', 'id' => $id]);
            } else {//if init
                if(strlen($model->mobile) == 10){
                    $model->phone_1 = substr($model->mobile, 0, 2);
                    $model->phone_2 = substr($model->mobile, 2, 4);
                    $model->phone_3 = substr($model->mobile, 6, 4);
                } else{
                    $model->phone_1 = substr($model->mobile, 0, 3);
                    $model->phone_2 = substr($model->mobile, 3, 4);
                    $model->phone_3 = substr($model->mobile, 7, 4);
                }
                return $this->render('update_phone', [
                            'model' => $model,
                ]);
            }
        } catch (Exception $ex) {
            //set error message
            Yii::$app->getSession()->setFlash('error', [
                'error'=>Yii::t('frontend',"System error happen. Please contact with System manager.")
                ]);
            //return index
            return $this->redirect(['update', 'id' => $id]);
        }        
    }

    /**
     * Input key confirm when update phone number
     * @param integer $id
     * @return mixed
     */
    public function actionUpdatePhoneKeyConfirm($id) {
        if (!$this->valid_login()) return $this->errorUserDetected();
        try {
            //create transaction
            $transaction = $this->db->beginTransaction();        
            //get session
            $session = Yii::$app->session;     
            $mobile = $session->get('Update_phone_request');
            if ( empty($mobile) ){
                Yii::$app->getSession()->setFlash('error', [
                    'error'=>Yii::t('frontend',"System error happen. Please contact with System manager.")
                ]);
                return $this->redirect(['update-phone','id' => $id]);
            }
            $model = $model = self::findModel($id);
            
            if ($model->user_id != Yii::$app->user->id) {
                Yii::$app->getSession()->setFlash('error', [
                    'error'=>Yii::t('frontend',"System error happen. Please contact with System manager.")
                    ]);
                //return index
                return $this->redirect(['/']);
            }
            $model->mobile = $mobile;
            $model->birth_date = !empty($model->birth_date)?date('Y/m/d', strtotime($model->birth_date)):$model->birth_date;
            //check if init or submit form
            if ($model->load(Yii::$app->request->post()) && $model->save()) {//if submit form and save success
                $user = User::findOne($model->user_id);
                $user->tel_no = $model->mobile;
                $user->save();
                $transaction->commit();
                $session->remove('Update_phone_request');
                return $this->redirect(['update-complete', 'id' => $id]);
            } else {//if init
                return $this->render('update_phone_key_confirm', [
                            'model' => $model,
                ]);
            }
        } catch (Exception $ex) {
            //rollback transaction
            $transaction->rollBack();
            //set error message
            Yii::$app->getSession()->setFlash('error', [
                'error'=>Yii::t('frontend',"System error happen. Please contact with System manager.")
                ]);
            //return index
            return $this->redirect(['update', 'id' => $id]);
        }
    }


    /**
     * Update email for member
     * @param integer $id
     * @return mixed
     */
    public function actionUpdateEmail($id) {
        if (!$this->valid_login()) return $this->errorUserDetected();
        try {
            //create transaction
            $transaction = $this->db->beginTransaction();
            //find member
            $model = self::findModel($id);
            if ($model->user_id != Yii::$app->user->id) {
                Yii::$app->getSession()->setFlash('error', [
                    'error'=>Yii::t('frontend',"System error happen. Please contact with System manager.")
                    ]);
                //return index
                return $this->redirect(['/']);
            }
            //set Scenario for rules validate
            $model->setScenario('update_email');
            $model->birth_date = !empty($model->birth_date)?date('Y/m/d', strtotime($model->birth_date)):$model->birth_date;
            //check init or submit form
            if ($model->load(Yii::$app->request->post()) && $model->save()) {//if submit form and save data success
                //send email
                $model->sendEmailRegister($model);
                $transaction->commit();
                return $this->redirect(['update-complete', 'id' => $id]);
            } else {
                return $this->render('update_email', [
                            'model' => $model,
                            'email' =>  self::findModel($id)->email,
                ]);
            }
        } catch (Exception $ex) {
            //rollback transaction
            $transaction->rollBack();
            //set error message
            Yii::$app->getSession()->setFlash('error', [
                'error'=>Yii::t('frontend',"System error happen. Please contact with System manager.")
                ]);
            //return index
            return $this->redirect(['update', 'id' => $model->id]);
        }
    }




    /**
     * Update password of member
     * @param integer $id
     * @return mixed
     */
    public function actionUpdatePassword($id) {
        if (!$this->valid_login()) return $this->errorUserDetected();
        try {
            //create transaction
            $transaction = $this->db->beginTransaction();
            //find member
            $model = self::findModel($id);
            if ($model->user_id != Yii::$app->user->id) {
                Yii::$app->getSession()->setFlash('error', [
                    'error'=>Yii::t('frontend',"System error happen. Please contact with System manager.")
                    ]);
                //return index
                return $this->redirect(['/']);
            }
            //Set Scenario for rules validate
            $model->setScenario('update_password');
            $model->birth_date = !empty($model->birth_date)?date('Y/m/d', strtotime($model->birth_date)):$model->birth_date;
            //check init or submit form
            if ($model->load(Yii::$app->request->post()) && $model->validate()) {//if submit form
                //check user password old
                $user = User::findOne($model->user_id);
                if (!$user->validatePassword($model->password_old)){
                    $model->addError('password_old', Yii::t('backend','Old password is incorrect.'));
                    return $this->render('update_password', [
                            'model' => $model,
                    ]);
                }
                //save password new
                $user->setPassword($model->password);
                $user->save();
                //send mail
                if(!empty($model->email)){
                    $model->sendEmailRegister($model);
                }
                $transaction->commit();
                return $this->redirect(['update-complete', 'id' => $id]);
            } else {
                return $this->render('update_password', [
                            'model' => $model,
                ]);
            }
        } catch (Exception $ex) {
            //rollback transaction
            $transaction->rollBack();
            //set error message
            Yii::$app->getSession()->setFlash('error', [
                'error'=>Yii::t('frontend',"System error happen. Please contact with System manager.")
                ]);
            //return index
            return $this->redirect(['update', 'id' => $model->id]);
        }

    }


    /**
     * Finds the MasterMember model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return MasterMember the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = MasterMember::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Lists all MasterStaff models.
     * @return mixed
     */
    public function actionGetpostcode()
    {

      $code = Yii::$app->request->post('postCode');
      $result = PostCode::find()->select(['country_name'])->andWhere(['post_code' => $code])->one();
      if($result != false ) return $result->country_name;
      return false;

    }

    
    /**
     * Rank Info Page
     * @return view rank_info
     * Metho : GET
     */
    public function actionRankInfo(){
        try{
            $user_id = Yii::$app->user->id;
            $customer = MasterCustomer::findOne(['user_id'=> $user_id]);
            
            if($customer == null)
                return $this->goHome();   

            //rankinfo customer
            $query = CustomerStore::getRankInfo($customer->id);

            //pagination
            $pages = new \yii\data\Pagination(['totalCount' => $query->count()]); $page=1;
            if(Yii::$app->request->get('page')!= null){
                $page = Yii::$app->request->get('page');
            }

            $limit = 5; $offset = $limit*($page-1); $pages->setPageSize($limit);
            $models = $query->offset($offset)->limit($limit)->all();

            return $this->render('rank_info', ['models' => $models,'pages' => $pages,]);
        }catch(Exception $ex){
            Yii::$app->session->setFlash('error', ['error'=>Yii::t('frontend','Cannot find your request')]);
            return $this->goHome();
        }
        
    }
    
    /**
     * Rank Info Detail Page
     * @return view rank_info_detail
     * Metho : GET
     */
    public function actionRankInfoDetail(){
        try{
            $id = Yii::$app->request->get('id');

            if(!is_numeric($id)){
                Yii::$app->session->setFlash('error', ['error'=>Yii::t('frontend','Cannot find your request')]);
                return $this->goHome();
            }
            $customer = MasterCustomer::findOne(['user_id'=> Yii::$app->user->id]);        
            $customerStore = CustomerStore::find()->where(['id'=>$id])->one();
            $store = $customerStore->getStore()->one();

            //find rank_setting
            $rank_setting = MasterRank::findRankbyCompanyId($store['company_id']);   
            if(($customerStore ==  null) || ($customerStore["customer_id"]!= $customer->id)){
                Yii::$app->session->setFlash('error', ['error'=>Yii::t('frontend','Cannot find your request')]);
                return $this->goHome();
            }
            
            return $this->render('rank_info_detail', ['customer_store'=>$customerStore, 'rank_setting'=> $rank_setting, 'store'=>$store]);
        }catch(Exception $ex){
            Yii::$app->session->setFlash('error', ['error'=>Yii::t('frontend','Cannot find your request')]);
            return $this->goHome();
        }
    }
    
    public function actionTestUpdate(){
        $connection = Yii::$app->getDb();
        //if(date('j', $timestamp) === '1') { 
            $command = $connection->createCommand('select update_rank_monthly()');
            $command->queryAll();
        //}
        
        //$commandResetRankDaily = $connection->createCommand('select reset_rank_daily()');
        //$commandResetRankDaily->queryAll();
        die;
    }
    
    public function errorUserDetected () {
        return $this->redirect(['/']);
    }
    
    public function valid() {
        return Yii::$app->user->isGuest && Yii::$app->session->get('store_id_rg');
    }
    
    public function valid_login() {
        return !Yii::$app->user->isGuest && Yii::$app->session->get('store_id_rg');
    }
}
