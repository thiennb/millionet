<?php

namespace frontend\controllers;

use Yii;
use common\models\Booking;
use common\models\CustomerStore;
use common\models\MasterCustomer;
use common\models\BookingHistory;
use common\models\MasterRank;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Url;

/**
 * HistoryController implements the CRUD actions for Booking model.
 */
class TopController extends Controller {

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionIndex() {
        if (Yii::$app->user->isGuest) {
            return $this->redirect(Url::base(TRUE) . '/site/login');
        }
        $user_id = Yii::$app->user->id;
        $customer = \common\models\MasterCustomer::findOne(['user_id' => \Yii::$app->user->identity->id]);
        $model = new CustomerStore();
        $dataProvider = $model->searchTop($customer->id)->asArray()->all();
        $notice = $model->getNotice($customer->id)->asArray()->all();
        $dataBooking = $model->getBooking($customer->id)->asArray()->all();
        $history = (new BookingHistory())->searchHistory([], $customer);
        $product_coupons = [];

        foreach ($history->models as $item) {
            $product_coupons[] = \yii\helpers\ArrayHelper::merge(BookingHistory::searchCoupons($item->id)->all(), BookingHistory::searchProducts($item->id)->all());
        }
        $customer_store = \yii\helpers\ArrayHelper::index(CustomerStore::find()->andWhere(['customer_id' => $customer->id])->all(), 'store_id');
        return $this->render('index', [
                    'dataProvider' => $dataProvider,
                    'notice' => $notice,
                    'dataBooking' => $history->models,
                    'customerId' => $customer->id,
                    'product_coupons' => $product_coupons,
                    'customer_store' => $customer_store
        ]);
    }

}
