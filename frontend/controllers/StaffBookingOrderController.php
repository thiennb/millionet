<?php

namespace frontend\controllers;

use common\models\StaffScheduleDetail;
use Yii;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use Carbon\Carbon;


class StaffBookingOrderController extends \yii\web\Controller
{
    public function actionIndex()
    {
        // Variable week current
        $week = 0;
        // Variable date current
        $date = date('Y/m/d');

        if ( !empty(Yii::$app->getRequest()->getQueryParam('time_execute'))
            && !empty(Yii::$app->getRequest()->getQueryParam('staff_id'))) {
            $staff_id = Yii::$app->getRequest()->getQueryParam('staff_id');
            $time_execute = Yii::$app->getRequest()->getQueryParam('time_execute');
            //$time_execute = preg_replace("/^([\d]{1,2})\:([\d]{2})$/", "00:$1:$2", $time_execute);
            //var_dump($time_execute);exit;
            sscanf($time_execute, "%d:%d:%d", $hours, $minutes, $seconds);
            $time_seconds = $hours * 3600 + $minutes * 60 + $seconds;
            $number_row = $time_seconds/1800;
        }

        // Get param week
        if ( !empty(Yii::$app->getRequest()->getQueryParam('week')) ) {

            $week = Yii::$app->getRequest()->getQueryParam('week');
            $date = date('Y/m/d', strtotime($date . ' +' . Yii::$app->getRequest()->getQueryParam('week') * 7 . ' day'));
        }

        $start_date = $date;
        $end_date = date('Y/m/d', strtotime($date . ' +14 day'));

        // Get all Staff Schedule Detail working
        $staffScheduleDetails = StaffScheduleDetail::getStaffScheduleDetailBetweenDates($staff_id,$number_row,$time_execute,$start_date,$end_date);

        for ($i = 0 ; $i < 14 ; $i++) {
            $model[$i]['date'] = date('Y/m/d', strtotime($date . ' +' . $i . ' day'));
            foreach ( $staffScheduleDetails as $staffScheduleDetail ) {
                $date_staffscheduledetail = date_create($staffScheduleDetail['booking_date']);
                if ( date_format($date_staffscheduledetail,"Y/m/d") == $model[$i]['date'] ) {
                    $booking_time =  $staffScheduleDetail['booking_time'];
                    $model[$i][$booking_time] = $staffScheduleDetail['booking_status'];
                }
            }
        }

        // Setting request again
        $setting['date'] = $date;
        $setting['week'] = $week;
        $setting['time_execute'] = $time_execute;
        $setting['staff_id'] = $staff_id;

        return $this->render('index',['model' => $model , 'setting' => $setting ] );
    }

}
