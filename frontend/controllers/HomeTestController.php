<?php

namespace frontend\controllers;

use Yii;
use common\models\Booking;
use common\models\BookingSearch;
use common\models\MstCategoriesProduct;
use common\models\StoreScheduleTemp;
use common\models\MasterCustomer;
use common\models\FrontFormBookingOrder;
use common\models\StaffScheduleDetail;
use common\models\MasterStore;
use common\models\BookingCoupon;
use common\models\BookingProduct;
use common\models\MstProduct;
use common\models\MasterStaff;
use common\models\MasterOption;
use common\models as Model;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\data\Sort;
use Carbon\Carbon;
use yii\helpers\ArrayHelper;
use yii\db\Query;
use api\controllers as api;
use yii\helpers\Url;
/**
 * BookingController implements the CRUD actions for Booking model.
 */
class HomeTestController extends Controller {

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Booking models.
     * @return mixed
     */
    public function actionIndex() {
        //$dataProvider = BookingSearch::search(Yii::$app->request->queryParams);
        $list_company = $this->selectListCompanyAndStore();
        return $this->render('index', [
                    'list_company' => $list_company,
                ]);
    }
    
      // =========================================================
    // Select Check List Store
    // =========================================================
    public function selectListCompanyAndStore() {
        $command = "SELECT
                    company.name,
                    array_to_string(array_agg(mst_store.id),',')  as store_id,
                    array_to_string(array_agg(mst_store.name),',')  as store_name
                    FROM
                    company
                    INNER JOIN mst_store
                    ON
                    mst_store.company_id = company.id and mst_store.del_flg = '0'";
        if (!Yii::$app->user->isGuest) {
            $command .= "WHERE company.del_flg = '0' and company.id = " . \common\components\Util::getCookiesCompanyId();
        }
        $command .= " GROUP BY
                    company.name";

        $command = Yii::$app->db->createCommand($command);
        
        return $command->queryAll();
    }
}
