<?php

namespace frontend\controllers;

use Yii;
use common\models\Booking;
use common\models\BookingSearch;
use common\models\ChargeHistory;
use common\models\MstOrder;
use common\models\ChargeHistorySearch;
use common\models\MasterCustomer;
use common\models\PointHistory;
use common\models\CustomerStore;
use common\models\MasterTicketSearch;
use common\models\MstOrderDetail;
use common\models\MstOrderSearch;
use common\models\MasterStore;
use common\models\MasterTicket;
use common\models\BookingHistory;
use common\models\BookingBusiness;
use common\models\CompanyStore;
use common\models\PointSetting;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * HistoryController implements the CRUD actions for Booking model.
 */
class HistoryController extends Controller {

    public $booking_date_from;
    public $booking_date_to;

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /*
     * Search and Display booking history for customer
     * @author hungrom
     * @param (int) store id
     * @param (date) start date, end date
     * @param (int) status []
     */

    public function actionBookinghistory() {
        $request = Yii::$app->request;
        if (Yii::$app->user->isGuest) {
            return $this->redirect(Url::base(TRUE) . '/site/login');
        }

        $customer = MasterCustomer::findFrontEnd()->where(['user_id' => Yii::$app->user->id])->one();
        $customer_store = ArrayHelper::index(CustomerStore::find()->andWhere(['customer_id' => $customer->id])->all(), 'store_id');

        $stores = ArrayHelper::merge(['' => '全て'], ArrayHelper::map(MasterStore::find()->andWhere([
                                    'id' => array_keys($customer_store)
                                ])->all(), 'id', 'name'));

        $model = new BookingHistory();
        $booking_statuses = \common\components\Constants::LIST_BOOKING_STATUS;
        array_pop($booking_statuses);
        array_pop($booking_statuses);

        $dataProvider = $model->searchHistory($request->get(), $customer);
        // gather point settings for companies
        $point_settings = [];

        $product_coupons = [];

        foreach ($dataProvider->models as $item) {
            $current_point_setting = PointSetting::getPointSetting($item->storeMaster->company_id);
            if ($current_point_setting) {
                $point_settings[$item->store_id] = $current_point_setting;
            }
            $product_coupons[] = ArrayHelper::merge(BookingHistory::searchCoupons($item->id)->all(), BookingHistory::searchProducts($item->id)->all());
        }


        return $this->render('booking/history', [
                    'stores' => $stores,
                    'model' => $model,
                    'booking_statuses' => $booking_statuses,
                    'items' => $dataProvider,
                    'total_page' => ceil($dataProvider->totalCount / 3),
                    'product_coupons' => $product_coupons,
                    'customer_store' => $customer_store,
                    'point_settings' => $point_settings
        ]);
    }

    /*
     * 
     */

    public function actionBookingdetail($booking_id) {
        $request = Yii::$app->request;
        if (Yii::$app->user->isGuest) {
            return $this->redirect(Url::base(TRUE) . '/site/login');
        }

        if ($booking_id) {
            $customer = MasterCustomer::findFrontEnd()->where(['user_id' => Yii::$app->user->id])->one();
            $customer_store = CustomerStore::findOne(['customer_id' => $customer->id]);
            /* @var $booking \common\models\Booking */
            $booking = Booking::findFrontEnd()
                    ->andWhere(['id' => $booking_id])
                    ->andWhere(['customer_id' => $customer->id])
                    ->with(['storeMaster', 'staff'])
                    ->one();

            if (!$booking) {
                return $this->redirect(Url::to(['bookinghistory']));
            }

            $booking_coupons = BookingHistory::searchCoupons($booking->id)->all();

            $booking_products = BookingHistory::searchProducts($booking->id)->all();

            $product_coupons = ArrayHelper::merge($booking_coupons, $booking_products);

            $cancelled = false;
            $rejectable = $booking->cancelOffset->timestamp > time(); // boolean

            if ($request->post('action') === 'cancel-reservation' && $rejectable) {
                $booking->status = Booking::BOOKING_STATUS_CANCEL_BY_CUSTOMER;
                try {
                    $transaction = Yii::$app->db->beginTransaction();
                    if ($booking->save()) {
                        \backend\controllers\BookingController::saveShiftToScheduleDetail($booking->staff_id, $booking->booking_date, $booking->store_id, null, $booking->start_time, $booking->end_time, \common\models\StaffScheduleDetail::BOOKING_STATUS_WORKING);
                        $booking_seats = \common\models\BookingSeat::findFrontEnd()
                                ->where(['booking_id' => $booking->id])
                                ->all();
                        foreach ($booking_seats as $booking_seat) {
                            \backend\controllers\BookingController::saveShiftToSeatScheduleDetail($booking_seat->seat_id, $booking->booking_date, $booking->store_id, null, $booking->start_time, $booking->end_time, \common\models\StaffScheduleDetail::BOOKING_STATUS_WORKING);
                        }
                    }
                    $transaction->commit();
                }
                catch (\Exception $e) {
                    $transaction->rollBack();
                }
            }

            $pointSetting = PointSetting::getPointSetting($booking->storeMaster->company_id);

            return $this->render('booking/detail', [
                        'item' => $booking,
                        'product_coupons' => $product_coupons,
                        'granted_points' => BookingBusiness::getPointFromMoney($booking->booking_total, $customer_store, $pointSetting),
                        'cancelled' => $cancelled,
                        'rejectable' => $rejectable,
                        'show_cancel_button' => ($booking->status == Booking::BOOKING_STATUS_PENDING || $booking->status == Booking::BOOKING_STATUS_APPROVE)
            ]);
        }
    }

    /**
     * Displays a single Booking model.
     * @param integer $id
     * @return mixed
     */
    public function actionBookingView($id) {
        $model = $this->findModelBooking($id);
        $modelStore = \common\models\MasterStore::find()->andWhere(['id' => $model->store_id])->one();
        return $this->render('view-booking', [
                    'modelStore' => $modelStore,
                    'model' => $model,
        ]);
    }

    protected function getStore($customer_id) {
        $query = MasterStore::find()->select([
            'mst_store.id',
            'mst_store.name',
        ]);
        $query->innerJoin(CustomerStore::tableName(), CustomerStore::tableName() . '.store_id = ' . MasterStore::tableName() . '.id');
        $query->andWhere(['customer_store.customer_id' => $customer_id]);
        return $query;
    }

    /**
     * Update field action to cancel booking (03).
     * If update is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionCancelBooking($id) {
        $model = $this->findModelBooking($id);
        $model->status = "04"; // '04': キャンセル
        if ($model->save()) {
            Yii::$app->getSession()->setFlash('success', [
                'success' => Yii::t('backend', "Cancel Success!")
            ]);
        } else {
            Yii::$app->getSession()->setFlash('error', [
                'error' => Yii::t('backend', "Sorry, don't cancel. Please try again.")
            ]);
        }
        return $this->redirect(['booking-view', 'id' => $model->id]);
    }

    /**
     * Finds the Booking model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Booking the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModelBooking($id) {
        if (($model = Booking::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Lists all Charge History models.
     * @return mixed
     */
    public function actionCharge() {

        $searchModel = new ChargeHistorySearch();
        $customer_id = \common\models\MasterCustomer::findOne(['user_id' => \Yii::$app->user->identity->id])->id;
        $dataProvider = $searchModel->searchHistory(Yii::$app->request->queryParams, $customer_id);
        return $this->render('index-charge', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Charge History model.
     * @param integer $id
     * @return mixed
     */
    public function actionChargeView($id) {
        $model = $this->findModelCharge($id);
        $modelStore = \common\models\MasterStore::find()->andWhere(['store_code' => $model->store_code])->one();
        return $this->render('view-charge', [
                    'modelStore' => $modelStore,
                    'model' => $model,
        ]);
    }

    /**
     * Finds the Charge History model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Charge History the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModelCharge($id) {
        if (($model = ChargeHistory::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /*
     * Search and show info of buy hishory by customer 
     * Author : vuongvt
     * @param (date)start_date, (date)end_date, (integer)store_id,
     * @param (integer) customer_id 
     */

    public function actionBuyhistory() {
        if (Yii::$app->user->isGuest)
            return $this->redirect(Url::base(TRUE) . '/site/login');
        $user_id = Yii::$app->user->id;
        $customer_model = new MasterCustomer;
        $customer = MasterCustomer::findOne(['user_id' => $user_id]);
        $company_id = $customer_model->getCompanyId($customer->id);
        $stores = $this->getStore($customer->id)->all();
        $modelOrderSearch = new MstOrderSearch();
        $model = new MstOrder();
        $request = Yii::$app->request;
        $store_name = $request->get('store_name') ? $request->get('store_name') : NULL;
        $store_code = MasterStore::find()->select('store_code')
                        ->where(['id' => $store_name])
                        ->asArray()->one();
        $dataProvider = $modelOrderSearch->searchHistory($request->queryParams, $store_code, $company_id, $customer->customer_jan_code);
        return $this->render('buy_history', [
                    'dataProvider' => $dataProvider,
                    'modelOrderSearch' => $modelOrderSearch,
                    'stores' => $stores,
                    'model' => $model
        ]);
    }

    /*
     * Show info of order detail after search
     * Author : vuongvt
     * @param (integer) order_code of order
     */

    public function actionBuydetail($order_code) {
        if (Yii::$app->user->isGuest)
            return $this->redirect(Url::base(TRUE) . '/site/login');
        if ($order_code) {
            $customer_model = new MasterCustomer;
            $customerId = \common\models\MasterCustomer::findOne(['user_id' => \Yii::$app->user->identity->id])->id;
            $company_id = $customer_model->getCompanyId($customerId);
            $baseUrl = Url::base(TRUE);
            $model = new MstOrderDetail();
            $modelOrderSearch = new MstOrderSearch();
            $modelOrder = new MstOrder ();
            $checkOrderCode = MstOrderDetail::find()
                    ->where(['order_code' => $order_code])
                    ->asArray()
                    ->one();
            if (empty($checkOrderCode)) {
                return $this->redirect($baseUrl . '/history/buyhistory');
            }
            // get info of store 
            $Store = $modelOrder->filterDetail($order_code, $company_id);
            $detailOrder = (new \yii\db\Query())->select([
                        'order_detail.id',
                        'order_detail.product_display_name',
                        'order_detail.quantity',
                        'order_detail.order_code',
                        'order_detail.product_total',
                        'order_detail.product_tax_money',
                        'order_detail.tax_display_method',
                        'order_detail.gift_flg'])
                    ->from('order_detail')
                    ->where(['order_detail.order_code' => $order_code])
                    ->andWhere(['order_detail.company_id' => $company_id])
                    ->andWhere(['order_detail.del_flg' => 0])
                    ->all();
            $moneyType = $modelOrderSearch->getMoneyType($order_code, $company_id)->asArray()->one();
            $check_home_flg = $modelOrderSearch->get_home_deliver_flg($order_code, $company_id);
            $check_gift_flg = $modelOrderSearch->check_gift_flg($order_code, $company_id);

            return $this->render('buy_detail', [
                        'Store' => $Store,
                        'detailOrder' => $detailOrder,
                        'moneyType' => $moneyType,
                        'check_home_flg' => $check_home_flg,
                        'check_gift_flg' => $check_gift_flg,
            ]);
        }
    }

    /*
     * Display list store history by customer
     * Author : vuongvt
     * @param (int) customerId
     */

    public function actionStorehistory() {
        if (Yii::$app->user->isGuest)
            return $this->redirect(Url::base(TRUE) . '/site/login');
        $customer_model = new MasterCustomer;
        $customerId = \common\models\MasterCustomer::findOne(['user_id' => \Yii::$app->user->identity->id])->id;
        $company_id = $customer_model->getCompanyId($customerId);
        $modelCustomerStore = new CustomerStore();
        $dataProvider = $modelCustomerStore->searchStoreHistory($customerId, $company_id);
        return $this->render('store_history', [
                    'modelCustomerStore' => $modelCustomerStore,
                    'dataProvider' => $dataProvider
        ]);
    }

    /*
     * Show info detail of store by customer
     * Author : vuongvt
     * @param (int) store_id 
     */

    public function actionStoredetail($id) {
        if ($id) {
            if (Yii::$app->user->isGuest)
                return $this->redirect(Url::base(TRUE) . '/site/login');
            $baseUrl = Url::base(TRUE);
            $modelStore = new MasterStore();
            $checkId = MasterStore::find()->where(['id' => $id])->exists();
            if ($checkId == FALSE)
                return $this->redirect($baseUrl . '/history/storehistory');
            $store = MasterStore::findFrontEnd()->andWhere(['id' => $id])->one();
            $staff_list = \common\models\MasterStaff::findFrontEnd()->andWhere(['store_id' => $id, 'show_flg' => '1'])->all();
            $staff_item_keys = [];

            for ($i = 1; $i < 6; $i++) {
                $key = $store['staff_item_' . $i];
                if (!empty($key)) {
                    $staff_item_keys[$i] = $key;
                }
            }

            return $this->render('store_detail', [
                        'store' => $store,
                        'modelStore' => $modelStore,
                        'baseUrl' => $baseUrl,
                        'staff_list' => $staff_list,
                        'staff_item_keys' => $staff_item_keys
            ]);
        }
        if ($id) {
            if (Yii::$app->user->isGuest)
                return $this->redirect(Url::base(TRUE) . '/site/login');
            $baseUrl = Url::base(TRUE);
            $modelStore = new MasterStore();
            $checkId = MasterStore::find()->where(['id' => $id])->exists();
            if ($checkId == FALSE)
                return $this->redirect($baseUrl . '/history/storehistory');
            $dataProvider = $modelStore->StoreDetail($id)->all();
            return $this->render('store_detail', [
                        'dataProvider' => $dataProvider,
                        'modelStore' => $modelStore,
                        'baseUrl' => $baseUrl
            ]);
        }
    }

    /*
     * Search and Display ticket History by customer
     * Author : vuong_vt
     * @param (int) store_id
     * @param (date) start_date, end_date
     */

    public function actionTickethistory() {

        if (Yii::$app->user->isGuest)
            return $this->redirect(Url::base(TRUE) . '/site/login');
        $user_id = Yii::$app->user->id;
        $customer_model = new MasterCustomer;
        $customer = MasterCustomer::findOne(['user_id' => $user_id]);
        $company_id = $customer_model->getCompanyId($customer->id);
        $stores = $this->getStore($customer->id)->all();
        $model = new MasterTicket();
        $modelTicketSearch = new MasterTicketSearch();
        $request = Yii::$app->request;
        $dataProvider = $modelTicketSearch->searchHistory($request->queryParams, $customer->customer_jan_code);
        return $this->render('ticket_history', [
                    'dataProvider' => $dataProvider,
                    'modelTicketSearch' => $modelTicketSearch,
                    'stores' => $stores,
                    'model' => $model
        ]);
    }

    public function actionTicketdetail($code) {
        if (Yii::$app->user->isGuest)
            return $this->redirect(Url::base(TRUE) . '/site/login');
        if ($code) {
            $user_id = Yii::$app->user->id;
            $customer = MasterCustomer::findOne(['user_id' => $user_id]);
            $store_id = isset($_GET['store_id']) ? $_GET['store_id'] : 0;
            $ticket_id = isset($_GET['id']) ? $_GET['id'] : 0;
            $modelTicketSearch = new MasterTicketSearch();
            $findTicket = MasterTicket::find()->select(['store_id', 'ticket_jan_code'])
                    ->where(['ticket_jan_code' => $code])
                    ->asArray()
                    ->one();
            if (!$findTicket)
                return $this->redirect(Url::base(true) . '/history/tickethistory');
            $storeName = $modelTicketSearch->getStoreName($store_id);
            $ticketInfo = $modelTicketSearch->getTicketInfo($code, $ticket_id, $store_id);
            $dataProvider = $modelTicketSearch->ticket_detail($code, $customer->customer_jan_code);
            return $this->render('ticket_detail', [
                        'dataProvider' => $dataProvider,
                        'storeName' => $storeName,
                        'ticketInfo' => $ticketInfo
            ]);
        }
    }

    /*
     * Search and Display Charge History by customer
     * Author : vuong_vt
     * @param (int) store_id
     * @param (date) start_date, end_date
     */

    public function actionChargehistory() {
        if (Yii::$app->user->isGuest)
            return $this->redirect(Url::base(TRUE) . '/site/login');
        $customerId = \common\models\MasterCustomer::findOne(['user_id' => \Yii::$app->user->identity->id])->id;
        $customer_model = new MasterCustomer;
        $company_id = $customer_model->getCompanyId($customerId);
        $search_latest = (new \yii\db\Query())
                ->select([
                    'c.*',
                    'store_name' => 'store.name'
                ])
                ->from(['s' => ChargeHistorySearch::searchLastestChargeHistory([$customerId], $company_id)])
                ->innerJoin(['c' => ChargeHistory::tableName()], 'c.customer_id = s.customer_id '
                        . 'and c.store_code = s.store_code '
                        . 'and c.process_date = s.process_date '
                        . 'and c.process_time = s.process_time')
                ->innerJoin(['store' => MasterStore::tableName()], 'store.store_code = s.store_code');

        $dataProvider = new \yii\data\ActiveDataProvider([
            'query' => $search_latest,
            'pagination' => [
                'pageSize' => 5,
            ],
            'sort' => false]);

        return $this->render('charge_history', [
                    //'ChargeModel' => $ChargeModel,
                    'dataProvider' => $dataProvider
        ]);
    }

    /*
     * Display charge history by charge id
     * Author : vuong_vt
     * params (int) $id
     */

    public function actionChargedetail($store_code) {
        if (Yii::$app->user->isGuest)
            return $this->redirect(Url::base(TRUE) . '/site/login');
        if ($store_code) {
            $id = isset($_GET['id']) ? $_GET['id'] : '';
            $customerId = \common\models\MasterCustomer::findOne(['user_id' => \Yii::$app->user->identity->id])->id;
            $customer_model = new MasterCustomer;
            $company_id = $customer_model->getCompanyId($customerId);
            $ChargeHistoryModel = new ChargeHistorySearch();
            $checkStore = MasterStore::find('store_code')
                    ->where(['store_code' => $store_code])
                    ->exists();
            if ($checkStore == FALSE)
                return $this->redirect(URL::base(true) . '/history/chargehistory');
            $dataProvider = $ChargeHistoryModel->chargeDetail($customerId, $store_code, $company_id)
                    ->asArray()
                    ->all();
            $chargeBalance = $ChargeHistoryModel->getCharge_Balance($customerId, $id, $company_id);
            if (!$chargeBalance)
                return $this->redirect(Url::base(true) . '/history/chargehistory');
            return $this->render('charge_detail', [
                        'dataProvider' => $dataProvider,
                        'chargeBalance' => $chargeBalance
            ]);
        }
    }

    /*
     * Display Point history by customer id
     * Author : vuong_vt
     * @params (int) customer_id
     */

    public function actionPointhistory() {
        if (Yii::$app->user->isGuest)
            return $this->redirect(Url::base(TRUE) . '/site/login');
        $customerId = \common\models\MasterCustomer::findOne(['user_id' => \Yii::$app->user->identity->id])->id;
        $customer_model = new MasterCustomer;
        $company_id = $customer_model->getCompanyId($customerId);
        $PointHistoryModel = new PointHistory();
        $dataProvider = $PointHistoryModel->searchPoint($customerId, $company_id);
        return $this->render('point_history', [
                    'PointHistoryModel' => $PointHistoryModel,
                    'dataProvider' => $dataProvider
        ]);
    }

    /*
     * Display detail point by customer
     * @params (int) pointhistory id 
     * Author : vuong_vt
     */

    public function actionPointdetail($store_code) {
        if (Yii::$app->user->isGuest)
            return $this->redirect(Url::base(TRUE) . '/site/login');
        if ($store_code) {
            $baseUrl = Url::base(TRUE);
            $customerId = \common\models\MasterCustomer::findOne(['user_id' => \Yii::$app->user->identity->id])->id;
            $customer_model = new MasterCustomer;
            $company_id = $customer_model->getCompanyId($customerId);
            $PointHistoryModel = new PointHistory();
            $checkId = MasterStore::find()->select(['store_code'])
                    ->where(['store_code' => $store_code])
                    ->exists();
            if ($checkId == FALSE)
                return $this->redirect($baseUrl . '/history/pointhistory');
            $storeInfo = $PointHistoryModel->getInfoStore($store_code, $company_id['company_id'])->asArray()->one();
            $getPoint = $PointHistoryModel->getPoint($storeInfo['id'], $customerId)->asArray()->one();
            $dataProvider = $PointHistoryModel->HistoryDetail($store_code, $customerId, $company_id)
                    ->asArray()
                    ->all();
            return $this->render('point_detail', [
                        'dataProvider' => $dataProvider,
                        'storeInfo' => $storeInfo,
                        'getPoint' => $getPoint
            ]);
        }
    }

}
