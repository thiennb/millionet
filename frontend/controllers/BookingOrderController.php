<?php

namespace frontend\controllers;

use Yii;
use common\models as Model;
use common\models\BookingSearch;
use common\models\MasterCustomer;
use common\models\MasterStore;
use common\models\MasterOption;
use common\models\BookingBusiness;
use common\models\Checklist;
use common\models\PointSetting;
use common\models\Company;
use common\models\MasterSeatSearch;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\db\Query;
use common\components\Util;
use api\controllers as api;
use Carbon\Carbon;

/**
 * BookingController implements the CRUD actions for Booking model.
 */
class BookingOrderController extends Controller {

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Booking models.
     * @return mixed
     */
    public function actionIndex() {
        $id_store_session = Yii::$app->request->get('storeId');
        $session = Yii::$app->session;
        $session->set('store_id_rg', $id_store_session);
        $session->set('reservation_step', 1);
        $company = Company::find()->innerJoin('mst_store', 'mst_store.company_id = company.id')
                        ->andWhere(['mst_store.id' => $id_store_session])->one();
        if (!empty($company)) {
            // Set cookies company id store
            Util::setCookiesCompanyId($company->id);
        }
        if (Yii::$app->user->isGuest) {
            return $this->needLogin();
        }

        if (!$this->getStore()) {
            return $this->errorStoreNotFound();
        }

        //$dataProvider = BookingSearch::search(Yii::$app->request->queryParams);

        return $this->render('index');
    }

    /**
     * Information store by id store
     * @return view
     */
    public function actionLoadstoreinformation() {
        $request = Yii::$app->request;
        if ($request->isAjax) {
            $dataProvider = MasterStore::find()->where(['id' => $request->post('storeId')])->one();

            return $this->renderAjax('_header', [
                        'search' => $dataProvider
            ]);
        }
    }

    /**
     * Lists all product coupon by id category models.
     * @return view
     */
    public function actionLoadproductbyid() {

        if (Yii::$app->request->isAjax) {

            $data = Yii::$app->request->post();

            $searchCategory = explode(":", $data['searchCategory']);

            $searchStoreId = explode(":", $data['storeId']);

            $searchCountCategory = explode(":", $data['countCategory']);

            $dataProvider = BookingSearch::searchProduct($searchCategory[0], $searchStoreId[0], $searchCountCategory[0]);

            return $this->renderAjax('actionLoadProductById', [
                        'search' => $dataProvider
            ]);
        }
    }

    public function actionLoadcoupons() {
        $request = Yii::$app->request;

        if ($request->isAjax && $request->post('storeId')) {
            // Create session store id : insert customer_store
            Yii::$app->session->set('store_id_rg', $request->post('storeId'));

            $current_page = $request->post('page') ? $request->post('page') : 1;

            $customer = MasterCustomer::find()->where(['user_id' => Yii::$app->user->id])->one();

            $coupons = BookingSearch::searchCouponsByStoreId($request->post('storeId'), '', MasterCustomer::findFrontEnd()->where(['mobile' => '77777777777'])->one());

            //get total page
            $total_page = ceil($coupons->count() / 10);

            if ($current_page < 1) {
                $current_page = 1;
            }

            if ($current_page > $total_page) {
                $current_page = $total_page;
            }

            //$coupons->orderBy(['expire_date' => \SORT_ASC]);
            $price_search = BookingBusiness::getCouponPriceTable(function ($query) {
                        $query->andWhere(['c.store_id' => Yii::$app->request->post('storeId')]);
                    });
            if ($price_search->count() > 0) {
                $coupons->leftJoin([
                    'price_search' => $price_search
                        ], 'price_search.id = c.id');

                $coupons->orderBy([
                    'apply_condition' => SORT_ASC,
                    'expire_date' => SORT_ASC,
                    'price_search.type' => SORT_ASC,
                    'price_search.discount_yen' => SORT_DESC,
                    'price_search.discount_percent' => SORT_DESC,
                    'price_search.discount_price_set' => SORT_ASC,
                    'price_search.discount_drink_eat' => SORT_ASC
                ]);
            } else {
                $coupons->orderBy([
                    'expire_date' => SORT_ASC
                ]);
            }

            $coupons->offset(($current_page - 1) * 10)->limit(10);

            $items = $coupons->all();

            $selected_coupons = BookingSearch::searchCouponsById($request->post('selectedCoupons'), $request->post('storeId'));

            $price_table = BookingBusiness::getPriceTable(implode(',', ArrayHelper::getColumn($items, 'id')))['coupons'];

            $non_combinable = array_search('0', ArrayHelper::getColumn($selected_coupons, 'combine_with_other_coupon_flg')) !== false;

            $selected_set_coupons = [];

            foreach ($selected_coupons as $coupon) {
                if ($coupon['benefits_content'] == '02' || $coupon['benefits_content'] == '03') {
                    $selected_set_coupons[] = $coupon['id'];
                }
            }

            return $this->renderAjax('actionLoadProductById', [
                        'type' => 'coupons',
                        'search' => $items,
                        'price_table' => $price_table,
                        'current_page' => $current_page,
                        'total_page' => $total_page, 'type' => 'coupons',
                        'non_combinable' => $non_combinable,
                        'selected_set_coupons' => $selected_set_coupons
            ]);
        }
    }

    public function actionLoadcategories() {
        $request = Yii::$app->request;
        if ($request->isAjax) {
            $dataProvider = BookingSearch::searchCategoryByStoreId($request->post('storeId'));

            return $this->renderAjax('_filter_menu', [
                        'searchModelCategory' => $dataProvider
            ]);
        }
    }

    public function actionLoadconfirmmenu() {
        $request = Yii::$app->request;
        if ($request->isAjax) {

            $search = api\BookingorderController::queryAllItems($request->post('storeId'), $request->post('coupons'), $request->post('products'));

            $total_time = array_sum(ArrayHelper::getColumn($search->items, 'time_require'));

            $hour = 0;
            $minute = 0;

            $hour = floor($total_time / 60);
            $minute = $total_time % 60;

            return $this->renderAjax('_confirm_menu', [
                        'searchConfirmmenu' => $search->items,
                        'hour' => $hour,
                        'minute' => $minute,
                        'total_time' => array_sum(ArrayHelper::getColumn($search->items, 'time_require')),
                        'coupon_price_table' => $search->price_search['coupons'],
                        'product_price_table' => $search->price_search['products']
                            //'total_price' => $total_price
            ]);
        }
    }

    public function actionTimesalonschedule() {

        if (Yii::$app->request->isAjax) {

            $data = Yii::$app->request->post();

            $searchCouponId = explode(":", $data['coupons']);

            $searchStoreId = explode(":", $data['storeId']);

            $searchModelCategory = new BookingSearch();

            $dataProvider = $searchModelCategory->searchTimeSalonsChedule($searchCouponId[0], $searchStoreId[0]);

            $total_time_allcoupon = 0;
            $hour = 0;
            $minute = 0;

            foreach ($dataProvider as $key => $value) {
                $total_time_allcoupon += $value["totaltime"];
            }

            $hour = floor($total_time_allcoupon / 60);
            $minute = $total_time_allcoupon % 60;

            return $this->renderAjax('_time_slon_schedule', [
                        'hour' => $hour,
                        'minute' => $minute
            ]);
        }
    }

    public function actionLoadoptionmenu() {

        if (Yii::$app->request->isAjax) {

            $data = Yii::$app->request->post();

            $coupons = array_filter(explode(',', $data['coupons']));

            $dataProvider = BookingSearch::searchOptionProductByIdCoupon($data['coupons']);

            if (!empty($data['products'])) {
                $dataProvider = ArrayHelper::merge($dataProvider, BookingSearch::searchOptionProductByIdProduct($data['products']));
            }


            $price_table = BookingSearch::getProductPriceTable(implode(',', ArrayHelper::getColumn($dataProvider, 'id')));

            $price_table = ArrayHelper::index($price_table, 'id');

            $options = [];

            $option_info = [];

            $search_by_names = [];

            // filter type = 1
//            $coupon_products = ArrayHelper::index((new Query())
//                                    ->select('product_id')
//                                    ->from('product_coupon')
//                                    ->where(['del_flg' => '0', 'coupon_id' => $coupons])
//                                    ->all(), 'product_id');

            $selected_products = array_filter(explode(',', $data['products']));

//            $selected_products = ArrayHelper::merge($selected_products, array_keys($coupon_products));
            // merge options
            foreach ($dataProvider as $key => $value) {
                $id = $value['id'];
                $option_id = $value['option_id'];
                $option_type = $value['option_type'];

                $option_info[$option_id]['name'] = $value['option_name'];
                $option_info[$option_id]['type'] = $option_type;
                // due to option_product_id
                $search_by = $value['search_by'];
                if ($option_type == '1') {
                    if (!in_array($id, $selected_products)) {
                        if (!isset($option_info[$option_id]['search_by'])) {
                            $option_info[$option_id]['search_by'] = $search_by;
                        }

                        // merge product in option
                        $options[$option_id][$id] = $value;
                    }
                } else {
                    // separated by search_by
                    $options[$option_id][$search_by][$id] = $value;
                    $search_by_names[$option_id][$search_by] = $value['search_by_name'];
                }
            }

            return $this->renderAjax('ajaxOptionMenu', [
                        'options' => $options,
                        'option_info' => $option_info,
                        'price_table' => $price_table,
                        'search_by_names' => $search_by_names
            ]);
        }
    }

    public function actionSalonschedule() {
        if (Yii::$app->user->isGuest) {
            return $this->needLogin();
        }

        $store = null;

        if (!$this->getStore($store)) {
            return $this->errorStoreNotFound();
        }

        if (!$this->checkStep(3)) {
            return $this->errorWrongStep();
        }

        // Variable week current
        $week = 0;
        // Variable date current
        $date = date('Y-m-d');

        $request = Yii::$app->request;

        // Get param time execute and store id
        if ($request->get('executeTime') !== '' && !empty($request->get('storeId'))) {
            $store_id = $request->get('storeId');
            $time_execute = $request->get('executeTime');
            $time_execute = preg_replace("/^([\d]{1,2})\:([\d]{2})$/", "00:$1:$2", $time_execute);
            sscanf($time_execute, "%d:%d:%d", $hours, $minutes, $seconds);
            $time_seconds = $hours * 3600 + $minutes * 60 + $seconds;
            $number_row = $time_seconds / 1800;
        } else {
            return $this->errorMissingData();
        }

        $time_string = '';
        if ($hours)
            $time_string .= $hours . '時間 ';
        if ($minutes)
            $time_string .= $minutes . '分';

        // Get param week
        if (!empty($request->get('week'))) {

            $week = $request->get('week');
            $date = date('Y-m-d', strtotime($date . ' +' . $request->get('week') * 7 . ' day'));
        }

        $start_date = $date;
        $end_date = date('Y-m-d', strtotime($date . ' +14 day'));

        // Get all Store Schedule Temp between start_date and end_date
        $storeScheduleTemps = BookingSearch::searchStoreScheduleBetweenDates($store_id, $number_row, $time_execute, $start_date, $end_date, $store['reservations_possible_time'], $request->get('staff'));

        for ($i = 0; $i < 14; $i++) {
            $model[$i]['date'] = date('Y-m-d', strtotime($date . ' +' . $i . ' day'));
            foreach ($storeScheduleTemps as $storeScheduleTemp) {

                $date_storeSchedule = date_create($storeScheduleTemp['booking_date']);
                if (date_format($date_storeSchedule, "Y-m-d") == $model[$i]['date']) {
                    $model[$i][$storeScheduleTemp['booking_time']] = $storeScheduleTemp['booking_status'];
                }
            }
        }

        // Setting request again
        $setting['date'] = $date;
        $setting['week'] = $week;
        $setting['time_execute'] = $time_execute;
        $setting['store_id'] = $store_id;

        // Row number calendar
        $count = (strtotime($store->time_close) - strtotime($store->time_open)) / (60 * 30);

        $store_schedule = Model\StoreSchedule::find()
                ->andWhere(['store_id' => $store->id])
                ->andWhere(['>=', 'schedule_date', $start_date])
                ->andWhere(['<=', 'schedule_date', $end_date])
                ->all();

        $store_schedule = ArrayHelper::index($store_schedule, 'schedule_date');

        return $this->render('salonSchedule', [
                    'setting' => $setting,
                    'model' => $model,
                    'timeString' => $time_string,
                    'count' => $count,
                    'time_open' => $store->time_open,
                    'store' => $store,
                    'store_schedule' => $store_schedule,
                    'number_row' => $number_row,
        ]);
    }

    public function actionStaff() {
        if (Yii::$app->user->isGuest) {
            return $this->needLogin();
        }

        if (!$this->getStore()) {
            return $this->errorStoreNotFound();
        }

        if (!$this->checkStep(4)) {
            return $this->errorWrongStep();
        }

        $request = Yii::$app->request;

        if (empty($request->get('storeId')) || empty($request->get('executeTime'))) {
            return $this->errorMissingData();
        }

        $store_id = $request->get('storeId');
        $time_execute = $request->get('executeTime');
        $time_execute = preg_replace("/^([\d]{1,2})\:([\d]{2})$/", "00:$1:$2", $time_execute);
        sscanf($time_execute, "%d:%d:%d", $hours, $minutes, $seconds);
        $time_seconds = $hours * 3600 + $minutes * 60 + $seconds;
        $number_row = $time_seconds / 1800;

        $time_string = '';
        if ($hours)
            $time_string .= $hours . '時間 ';
        if ($minutes)
            $time_string .= $minutes . '分';

        $store = MasterStore::find()->where(['id' => $store_id])->one();

        if (!$store) {
            return $this->errorMissingData();
        }

        // Get param time execute and store id
        if (!empty($request->get('date'))) {
            // Variable date current
            $date = Carbon::createFromTimestamp($request->get('date'));
            // start_date = end_date
            $available_staff = BookingSearch::searchStaffByDateTime($store['id'], $number_row, $time_execute, $date->format('Y-m-d'), $date->format('Y-m-d'), $store['reservations_possible_time'], $date->format('H:i:s'));
            $baseURL = 'detail?' .
                    'storeId=' . $request->get('storeId') .
                    '&coupons=' . $request->get('coupons') .
                    '&date=' . $request->get('date') .
                    '&executeTime=' . $request->get('executeTime') .
                    '&products=' . $request->get('products') .
                    '&options=' . $request->get('options') .
                    '&staff=';
        } else {
            $start_date = Carbon::now()->format('Y-m-d');
            $end_date = Carbon::now()->addDays(14)->format('Y-m-d');

            $available_staff = BookingSearch::searchStaffByDateTime($store['id'], $number_row, $time_execute, $start_date, $end_date, $store['reservations_possible_time']);
            $baseURL = 'salonschedule?' .
                    'storeId=' . $request->get('storeId') .
                    '&coupons=' . $request->get('coupons') .
                    '&executeTime=' . $request->get('executeTime') .
                    '&products=' . $request->get('products') .
                    '&options=' . $request->get('options') .
                    '&staff=';
        }

        $price_date = isset($date) ? $date->format('Y-m-d') : '';

        $price_table = BookingSearch::getProductPriceTable(implode(',', array_filter(ArrayHelper::getColumn($available_staff, 'assign_product_id'))), $price_date);

        $price_table = ArrayHelper::index($price_table, 'id');

        $staff_detail = Model\MasterStaff::findFrontEnd()->where(['id' => ArrayHelper::getColumn($available_staff, 'id')])->all();

        $staff_item_keys = [];

        for ($i = 1; $i < 6; $i++) {
            $key = $store['staff_item_' . $i];
            if (!empty($key)) {
                $staff_item_keys[$i] = $key;
            }
        }

        return $this->render('staff', [
                    'available_staff' => $available_staff,
                    'baseURL' => $baseURL,
                    'timeString' => $time_string,
                    'price_table' => $price_table,
                    'staff_detail' => $staff_detail,
                    'staff_item_keys' => $staff_item_keys
        ]);
    }

    public function buildURL($parts, $params) {
        $url = $parts[0];
        array_shift($parts);

        if (count($parts)) {
            $value = isset($params[$parts[0]]) ? $params[$parts[0]] : '';
            $url .= '?' . $parts[0] . '=' . $value;
        }

        array_shift($parts);

        foreach ($parts as $key) {
            $value = isset($params[$key]) ? $params[$key] : '';
            $url .= '&' . $key . '=' . $value;
        }

        return $url;
    }

    /**
     * Render addition detail for current booking order
     * @return mixed
     */
    public function actionDetail() {
        if (Yii::$app->user->isGuest) {
            return $this->needLogin();
        }

        if (!$this->getStore()) {
            return $this->errorStoreNotFound();
        }

        if (!$this->checkStep(5)) {
            return $this->errorWrongStep();
        }

        $request = Yii::$app->request;
        $formModel = new Model\ApiFormBookingDetail();

        $customer = Model\MasterCustomer::find()->where(['user_id' => Yii::$app->user->id])->one();

        // query current user point
        $point = Model\CustomerStore::find()->where([
                    'customer_id' => $customer->id,
                    'store_id' => Yii::$app->request->get('storeId')
                ])->one();

        $total_point = $point ? (int) $point->total_point : 0;

        // Get Check list
        $checklists = Checklist::getChecklistByStore(Yii::$app->request->get('storeId'));

        $seat = null;
        if ($request->get('seat')) {
            $seat = Model\MasterTypeSeat::findFrontEnd()
                    ->where(['id' => $request->get('seat')])
                    ->one();
        }

        if (!empty($request->post())) {
            $requestData = $request->post();
            // Set data to model to load                        
            $formModel->load($requestData);

            if ($formModel->validate()) {
                $strValues = api\BookingorderController::convertPostChecklist($requestData, $checklists);
                if ($strValues !== false) {
                    // Set data to session to get them in next step
                    Yii::$app->session->set('booking-demand', $formModel->demand);
                    Yii::$app->session->set('booking-point', (int) $formModel->point_use);
                    Yii::$app->session->set('booking-checklist', $strValues);

                    return $this->redirect([
                                $this->buildURL(['confirm', 'storeId', 'products', 'coupons', 'options', 'executeTime', 'staff', 'date', 'noStaff', 'seat', 'capacity'], Yii::$app->request->get())
                    ]);
                }

                $formModel->addError('checklist', Yii::t('frontend', 'Please input for reqired question'));
            }
        }


        return $this->render('detail', [
                    'user' => $customer,
                    'model' => $formModel,
                    'total_point' => $total_point,
                    'checklists' => $checklists,
                    'seat' => $seat
        ]);
    }

    public function actionLoadorderdetail() {
        if (Yii::$app->request->isAjax) {
            $request = Yii::$app->request;

            if (!$request->post('storeId') && !$request->post('date'))
                return $this->errorMissingData();

            $extra_products = [];
            $options = [];
            $store_id = $request->post('storeId');

            if (preg_match('/^\d+\-\d+\-\d+(,\d+\-\d+\-\d+){0,}$/', $request->post('options'))) {
                $explode = array_filter(explode(',', $request->post('options')));

                foreach ($explode as $optionProduct) {
                    $ex = explode('-', $optionProduct);
                    $options[] = MasterOption::findFrontEnd()->where(['id' => $ex[0]])->one();
                    $extra_products[] = $ex[1];
                }
            }

            $products = implode(',', array_filter(ArrayHelper::merge(array_filter(explode(',', $request->post('products'))), $extra_products)));
            $coupons = $request->post('coupons');

            $queryItems = api\BookingorderController::queryAllItems($store_id, $request->post('coupons'), $request->post('products'), $request->post('staff'), $request->post('options'));

            // query customer
            $customer = Model\MasterCustomer::find()->where(['user_id' => Yii::$app->user->id])->one();

            // query current user point
            $customer_store = Model\CustomerStore::find()->andWhere([
                        'customer_id' => $customer->id,
                        'store_id' => $store_id
                    ])->one();
            // Check if customer store is exists
            // If have no data, create new customer store
            if (!$customer_store) {
                // Call method to insert new customer store when it is not found
                $customer_store = Model\CustomerStore::addCustomerStore($store_id, $customer->id);
            }

            $total_point = $customer_store ? (int) $customer_store->total_point : 0;

            $seat = null;
            if ($request->post('seat')) {
                $seat = Model\MasterTypeSeat::findFrontEnd()
                        ->where(['id' => $request->post('seat')])
                        ->one();
            }

            return $this->renderAjax('ajaxLoadOrderDetail', [
                        'items' => $queryItems->items,
                        'price_table' => $queryItems->price_table,
                        'total_price' => $queryItems->total_price,
                        'options' => $options,
                        'total_point' => $total_point,
                        'product_no_option_count' => count(explode(',', $products)) - count($extra_products),
                        'pointSetting' => PointSetting::getPointSetting($customer_store->store->company_id),
                        'seat' => $seat,
                        'capacity' => $request->post('capacity')
            ]);
        }
    }

    public function actionConfirm() {
        if (Yii::$app->user->isGuest) {
            return $this->needLogin();
        }

        $store = null;

        if (!$this->getStore($store)) {
            return $this->errorStoreNotFound();
        }

        if (!$this->checkStep(6)) {
            return $this->errorWrongStep();
        }

        $request = Yii::$app->request;

        if (!$request->get('storeId') || !$request->get('date'))
            return $this->errorMissingData();

        $extra_products = [];
        $options = [];
        //$option_products = [];

        if (preg_match('/^\d+\-\d+\-\d+(,\d+\-\d+\-\d+){0,}$/', $request->get('options'))) {
            $explode = array_filter(explode(',', $request->get('options')));

            foreach ($explode as $optionProduct) {
                $ex = explode('-', $optionProduct);
                // option-product-searchby
                /* if(!isset($options[$ex[1]])) {
                  $options[$ex[1]] = [];
                  } */
                $options[] = MasterOption::findFrontEnd()->where(['id' => $ex[0]])->one();
                $extra_products[] = $ex[1];
                //$option_products[$ex[1]] = $product_search[$ex[1]];
                //$product_no_option_count--;
            }
        }

        $products = implode(',', array_filter(ArrayHelper::merge(explode(',', $request->get('products')), $extra_products)));
        $coupons = $request->get('coupons');

        $queryItems = api\BookingorderController::queryAllItems($store->id, $coupons, $request->get('products'), $request->get('staff'), $request->get('options'));

        // query customer
        $customer = Model\MasterCustomer::find()->where(['user_id' => Yii::$app->user->id])->one();

        // query current user point
        $customerStore = Model\CustomerStore::find()->where([
                    'customer_id' => $customer->id,
                    'store_id' => $store->id
                ])->one();

        // Check if customer store is exists
        // If have no data, create new customer store
        if (!$customerStore) {
            // Call method to insert new customer store when it is not found
            $customerStore = Model\CustomerStore::addCustomerStore($store->id, $customer->id);
        }

        $total_point = $customerStore ? (int) $customerStore->total_point : 0;

        $model = new Model\ApiFormBooking();

        if ($store->booking_resources === '01') {
            $model->scenario = 'restaurant';
        }

        $demand = Yii::$app->session->get('booking-demand');
        $point = Yii::$app->session->get('booking-point');
        $total_time = array_sum(ArrayHelper::getColumn($queryItems->items, 'time_require'));
        $total_price = array_sum($queryItems->price_table);
        // Get checklist from session    
        $checklistSession = Yii::$app->session->get('booking-checklist');

        if (Yii::$app->request->post('_action') === 'confirm') {
            $model->load(Yii::$app->request->post());
            if ($model->validate()) {
                $checklists = [];
                if (!empty($checklistSession)) {
                    foreach ($checklistSession as $types) {
                        foreach ($types as $checklistID => $answer) {
                            $checklists[$checklistID] = $answer;
                        }
                    }
                }
                // DONE
                if (api\BookingorderController::createBooking($model->storeId, $model->coupons, $model->products, $model->staff, $model->date, $total_time, $point, $demand, $total_price, Yii::$app->request->get('options'), $checklists, $model->seat, $model->capacity)) {
                    return $this->redirect(['success?' . $_SERVER['QUERY_STRING']]);
                }
            }
        }

        //$company_store = Model\CompanyStore::findFrontEnd($request->get('storeId'))->one();
        $pointSetting = PointSetting::getPointSetting($customerStore->store->company_id);
        $final_price = $queryItems->total_price - BookingBusiness::getMoneyFromPoint($point, $pointSetting);
        $point_gained = BookingBusiness::getPointFromMoney($final_price, $customerStore, $pointSetting);

        // Get data by check list
        $selectedChecklistData = api\BookingorderController::getChecklistFromSession($checklistSession);

        $seat = null;
        if ($request->get('seat')) {
            $seat = Model\MasterTypeSeat::findFrontEnd()
                    ->where(['id' => $request->get('seat')])
                    ->one();
        }

        return $this->render('confirm', [
                    'customer' => $customer,
                    'formModel' => $model,
                    'items' => $queryItems->items,
                    'price_table' => $queryItems->price_table,
                    'total_price' => $queryItems->total_price,
                    'total_time' => $total_time,
                    'options' => $options,
                    'total_point' => $total_point,
                    'product_no_option_count' => count(explode(',', $products)) - count($extra_products),
                    'final_price' => $final_price,
                    'point_gained' => $point_gained,
                    'point' => $point,
                    'demand' => $demand,
                    'checklists' => $selectedChecklistData,
                    'pointSetting' => $pointSetting,
                    'seat' => $seat
        ]);
    }

    public function actionSuccess() {
        Yii::$app->session->remove('reservation_step');

        return $this->render('success');
    }

    /**
     * Get current user information
     * @return yii\base\Model
     */
    private function _getUser() {
        return MasterCustomer::find()->where(['user_id' => Yii::$app->user->id])->one();
    }

    public function actionLoadproducts() {
        $request = Yii::$app->request;

        if ($request->isAjax && $request->post('storeId')) {

            $items = Model\MstProduct::findFrontend()
                    ->select(['id', 'name', 'time_require', 'description', 'image1', 'category_id'])
                    ->with('category')
                    ->andWhere(['store_id' => Yii::$app->request->post('storeId')])
                    ->andWhere(['or', ['tel_booking_flg' => null], ['tel_booking_flg' => '0']])
                    ->andWhere(['or', ['option_condition_flg' => null], ['option_condition_flg' => '0']])
                    ->andWhere(['show_flg' => '1']);

            if ($request->post('categories')) {
                $items = $items->andWhere(['category_id' => explode(',', $request->post('categories'))]);
            }

            $current_page = Yii::$app->request->post('page') ? Yii::$app->request->post('page') : 1;

            $total_page = ceil($items->count() / 10);

            if ($current_page < 1) {
                $current_page = 1;
            }

            if ($current_page > $total_page) {
                $current_page = $total_page;
            }

            $items = $items->offset(($current_page - 1) * 10)->limit(10)->orderBy(['category_id' => SORT_ASC])->all();

            $price_table = BookingSearch::getProductPriceTable(implode(',', ArrayHelper::getColumn($items, 'id')));

            $price_table = ArrayHelper::index($price_table, 'id');

            // sort using price
            api\BookingorderController::sortProducts($items, $price_table);

            return $this->renderAjax('actionLoadProductById', [
                        'type' => 'products',
                        'search' => $items,
                        'price_table' => $price_table,
                        'current_page' => $current_page,
                        'total_page' => $total_page, 'type' => 'products'
            ]);
        }
    }

    public function actionSearchcouponsbycategories() {
        if (Yii::$app->request->isAjax) {

            $data = Yii::$app->request->post();

            $searchCategory = explode(":", $data['categories']);

            $searchStoreId = explode(":", $data['storeId']);

            $search = BookingSearch::searchCouponByCategories($searchCategory[0], $searchStoreId[0]);

            $current_page = $data['page'];

            $total_page = ceil($search->count() / 10);

            $dataProvider = $search->offset(($current_page - 1) * 10)->limit(10)->orderBy(['category_id' => SORT_ASC])->all();

            $price_table = BookingSearch::getCouponPriceTable(implode(',', ArrayHelper::getColumn($dataProvider, 'id')));

            $price_table = ArrayHelper::index($price_table, 'id');

            // sort using price
            uasort($dataProvider, function ($p1, $p2) {
                $pid1 = $p1['id'];
                $pid2 = $p2['id'];

                // obey apply_condition order
                if ($p1['apply_condition'] < $p2['apply_condition']) {
                    return -1;
                } else if ($p1['apply_condition'] > $p2['apply_condition']) {
                    return 1;
                }

                if (!isset($price_table[$pid1])) {
                    return -1;
                }

                if (!isset($price_table[$pid2])) {
                    return 1;
                }

                $price1 = $price_table[$pid1]['total_price'];
                $price2 = $price_table[$pid2]['total_price'];

                if ($price1 < $price2) {
                    return -1;
                } elseif ($price1 > $price2) {
                    return 1;
                } else {
                    return 0;
                }
            });

            return $this->renderAjax('actionLoadProductById', [
                        'search' => $dataProvider,
                        'price_table' => $price_table,
                        'current_page' => $current_page,
                        'total_page' => $total_page, 'type' => 'coupons'
            ]);
        }
    }

    public function errorMissingData() {
        return $this->redirect(\yii\helpers\Url::to(['index', 'storeId' => Yii::$app->request->get('storeId')]));
    }

    public function errorStoreNotFound() {
        return $this->redirect(['404']);
    }

    public function getStore(&$saved_store = false) {
        $store_id = Yii::$app->request->get('storeId');
        $store = MasterStore::findFrontEnd()->andWhere(['id' => $store_id])->one();
        if ($saved_store !== false) {
            $saved_store = $store;
        }
        return $store_id && $store;
    }

    public function needLogin() {
        // add sesion for redirect after login
        Yii::$app->session->set('login_redirect', 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']);

        return $this->redirect(['site/login']);
    }

    public function queryItems($store_id, $coupons = '', $products = '', $staff = '') {
        $product_ids = array_filter(explode(',', $products));
        $coupon_ids = array_filter(explode(',', $coupons));
        // if request coupons
        $coupon_search = ArrayHelper::index(BookingSearch::searchCouponsById($coupons, $store_id), 'id');

        $coupon_price_table = BookingSearch::getCouponPriceTable(implode(',', array_keys($coupon_search)));
        $coupon_price_table = ArrayHelper::index($coupon_price_table, 'id');

        // if request products
        $product_search = ArrayHelper::index(BookingSearch::searchProductsById($products, $store_id), 'id');

        // if request staff
        $staffs = BookingSearch::searchStaffById($staff, $store_id);

        $product_staff_price_id_list = ArrayHelper::merge(
                        ArrayHelper::getColumn($product_search, 'id'),
                        // staff.assign_product_id is nullable!
                        array_filter(ArrayHelper::getColumn($staffs, 'assign_product_id'))
        );

        $product_staff_price_table = BookingSearch::getProductPriceTable(implode(',', $product_staff_price_id_list));
        $product_staff_price_table = ArrayHelper::index($product_staff_price_table, 'id');

        // then build a price_table
        $price_table = [];
        $items = [];
        $coupons = [];
        $products = [];

        foreach ($coupon_ids as $coupon_id) {
            if (isset($coupon_search[$coupon_id])) {
                $items[] = $coupon_search[$coupon_id];
                $price_table[] = isset($coupon_price_table[$coupon_id]) ? (int) $coupon_price_table[$coupon_id]['total_price'] : 0;
            }
        }

        foreach ($product_ids as $product_id) {
            if (isset($product_search[$product_id])) {
                $items[] = $product_search[$product_id];
                $price_table[] = isset($product_staff_price_table[$product_id]) ? (int) $product_staff_price_table[$product_id]['total_price'] : 0;
            }
        }

        foreach ($staffs as $staff) {
            $items[] = $staff;
            $price_table[] = $staff['assign_product_id'] && isset($product_staff_price_table[$staff['assign_product_id']]) ? (int) $product_staff_price_table[$staff['assign_product_id']]['total_price'] : 0;
        }

        $ret = new \stdClass();

        $ret->items = $items;
        $ret->price_table = $price_table;

        return $ret;
    }

    public function errorNoProductSelected() {
        return '<h1>NO PRODUCT SELECTED</h1';
    }

    public function errorWrongStep() {
        Yii::$app->session->setFlash('reservation_error', '予約セッションは終了しました。 もう一度お試しください。');
        return $this->redirect(['index', 'storeId' => Yii::$app->request->get('storeId')]);
    }

    public function checkStep($step) {
        $session = Yii::$app->session;
        $current_step = $session->get('reservation_step');
        if ($current_step) {
            if ($current_step === $step || $current_step === $step - 1 || $current_step === $step + 1 || $step === 5 && $current_step === 3 || $step === 3 && $current_step === 5) {
                Yii::$app->session->setFlash('reservation_error', '');
                $session->set('reservation_step', $step);
                return true;
            } else {
                return false;
            }
        }

        return false;
    }

    public function actionSeatschedule() {
        if (Yii::$app->user->isGuest) {
            return $this->needLogin();
        }

        $store = null;

        if (!$this->getStore($store)) {
            return $this->errorStoreNotFound();
        }

        if (!$this->checkStep(3)) {
            return $this->errorWrongStep();
        }

        // Variable week current
        $week = 0;
        // Variable date current
        $date = date('Y-m-d');
        $number_of_date = 14;

        $request = Yii::$app->request;
        $time_execute = $request->get('executeTime');
        $type_seat_id = $request->get('seat');
        $capacity = $request->get('capacity');

        // Get param time execute and store id
        if ($time_execute) {
            $time_execute = preg_replace("/^([\d]{1,2})\:([\d]{2})$/", "00:$1:$2", $time_execute);
            sscanf($time_execute, "%d:%d:%d", $hours, $minutes, $seconds);
            $time_seconds = $hours * 3600 + $minutes * 60 + $seconds;
            $time_minutes = $hours * 60 + $minutes;
            $number_row = $time_seconds / 1800;
        } else {
            return $this->errorMissingData();
        }

        $time_string = '';
        if ($hours) {
            $time_string .= $hours . '時間 ';
        }
        if ($minutes) {
            $time_string .= $minutes . '分';
        }


        // Get param week
        if (!empty($request->get('week'))) {

            $week = $request->get('week');
            $date = date('Y/m/d', strtotime($date . ' +' . $request->get('week') * 7 . ' day'));
        }

        $start_date = $date;
        $end_date = date('Y-m-d', strtotime($date . ' +14 day'));

        $seat_search = Model\MasterTypeSeat::findFrontEnd()
                ->andWhere([
            'store_id' => $store->id
        ]);

        $seat_search->andFilterWhere([
            'id' => $type_seat_id ? $type_seat_id : null
        ]);

        $seats = $seat_search->all();

        $usable_seats = api\BookingorderController::buildUsableSeatList($store->id, $seats, $capacity)['usable_seats'];

        foreach ($usable_seats as $usable_seat) {
            $usable_seat->shifts = $usable_seat->detailedSchedule;
        }

        $model = [];
        for ($i = 0; $i < $number_of_date; $i++) {
            $model[$i]['date'] = date('Y-m-d', strtotime($date . ' +' . $i . ' day'));
        }

        // Setting request again
        $setting['date'] = $date;
        $setting['week'] = $week;
        $setting['time_execute'] = $time_execute;
        $setting['store_id'] = $store->id;

        // Row number calendar
        $count = (strtotime($store->time_close) - strtotime($store->time_open)) / (60 * 30);
        $store_schedule = Model\StoreSchedule::find()
                ->andWhere(['store_id' => $store->id])
                ->andWhere(['>=', 'schedule_date', $start_date])
                ->andWhere(['<=', 'schedule_date', $end_date])
                ->all();

        $store_schedule = ArrayHelper::index($store_schedule, 'schedule_date');

        return $this->render('seat_schedule', [
                    'setting' => $setting,
                    'model' => $model,
                    'timeString' => $time_string,
                    'count' => $count,
                    'time_open' => $store->time_open,
                    'seats' => $usable_seats,
                    'reservation_possible_seconds' => $store['reservations_possible_time'] * 60,
                    'number_row' => $number_row,
                    'seat_count' => count($seats),
                    'store_schedule' => $store_schedule,
                    'store' => $store
        ]);
    }

    public function actionOptions() {
        if (Yii::$app->user->isGuest) {
            return $this->needLogin();
        }

        $store = null;

        if (!$this->getStore($store)) {
            return $this->errorStoreNotFound();
        }

        if (!$this->checkStep(2)) {
            return $this->errorWrongStep();
        }

        $request = Yii::$app->request;

        if ($request->get('storeId') && ($request->get('products') || $request->get('coupons'))) {
            $mode = $store->booking_resources === '01' ? 'seat' : 'salon';

            $dataProvider = BookingSearch::searchOptionProductByIdProduct($request->get('products'));

            $searchCoupons = BookingSearch::searchCouponsById($request->get('coupons'), $request->get('storeId'));
            $searchProducts = BookingSearch::searchProductsById($request->get('products'), $request->get('storeId'));

            $booked = ArrayHelper::merge($searchCoupons, $searchProducts);
            $time = array_sum(ArrayHelper::getColumn($booked, 'time_require'));
            
            if(!$time) {
                Yii::$app->session->setFlash('reservation_error', '選択している商品は所要時間がありません。他の商品を選択してください。');
                
                return $this->redirect(['index?storeId=' . $request->get('storeId') . '&coupons=' . $request->get('coupons') . '&products=' . $request->get('products')]);
            }

            if (empty($dataProvider)) {
                $h = floor($time / 60);
                $m = $time % 60;

                $time = sprintf('%d:%d:00', $h, $m);

                return $this->redirect([$mode . 'schedule?noOption=1&storeId=' . $request->get('storeId') . '&coupons=' . $request->get('coupons') . '&addMenu=2&executeTime=' . $time . '&products=' . $request->get('products')]);
            }

            return $this->render('options', [
                        'mode' => $mode
            ]);
        }

        return $this->errorNoProductSelected();
    }

    public function actionSeat() {
        if (Yii::$app->user->isGuest) {
            return $this->needLogin();
        }

        $store = null;

        if (!$this->getStore($store)) {
            return $this->errorStoreNotFound();
        }

        if (!$this->checkStep(4)) {
            return $this->errorWrongStep();
        }

        $request = Yii::$app->request;

        if (empty($request->get('storeId')) || empty($request->get('executeTime'))) {
            return $this->errorMissingData();
        }

        $execute_time = $request->get('executeTime');
        $date = $request->get('date');
        $capacity = $request->get('capacity');

        //$store_id = $request->get('storeId');
        $time_execute = preg_replace("/^([\d]{1,2})\:([\d]{2})$/", "00:$1:$2", $execute_time);
        sscanf($time_execute, "%d:%d:%d", $hours, $minutes, $seconds);
        //$number_row = $time_seconds / 1800;

        $time_string = '';
        if ($hours) {
            $time_string .= $hours . '時間 ';
        }
        if ($minutes) {
            $time_string .= $minutes . '分';
        }

        // form for validating seat and capacity
        $formModel = new Model\ApiFormBookingSeat();
        $formModel->attributes = $request->get();
        $seats = Model\MasterTypeSeat::findFrontEnd()
                ->andWhere([
                    'store_id' => $store->id
                ])
                ->all();

        $usable_seat_list = api\BookingorderController::buildUsableSeatList($store, $seats, $capacity, $date);
        $usable_seats = $usable_seat_list['usable_seats'];
        $seats = ArrayHelper::index($seats, 'id');
        if ($request->get('capacity') !== '' && $request->get('capacity') !== null) {
            if ($formModel->validate()) {
                if (isset($_GET['seat'])) {
                    if (!$request->get('date')) {
                        return $this->redirect(['seatschedule',
                                    'storeId' => $formModel->storeId,
                                    'products' => $formModel->products,
                                    'options' => $formModel->options,
                                    'coupons' => $formModel->coupons,
                                    'noOption' => $formModel->noOption,
                                    'executeTime' => $formModel->executeTime,
                                    'capacity' => $formModel->capacity,
                                    'seat' => $formModel->seat,
                                    'date' => $formModel->date
                        ]);
                    } elseif ($request->get('seat') === '0' || key_exists($request->get('seat'), $seats)) {
                        return $this->redirect(['detail',
                                    'storeId' => $formModel->storeId,
                                    'products' => $formModel->products,
                                    'options' => $formModel->options,
                                    'coupons' => $formModel->coupons,
                                    'noOption' => $formModel->noOption,
                                    'executeTime' => $formModel->executeTime,
                                    'capacity' => $formModel->capacity,
                                    'seat' => $formModel->seat,
                                    'date' => $formModel->date
                        ]);
                    }
                }
            }
        }

        $seat_capacity_list_keys = range($usable_seat_list['capacity_min'], $usable_seat_list['capacity_max']);

        $seat_capacity_list = array_combine($seat_capacity_list_keys, $seat_capacity_list_keys);
        $seat_capacity_list[0] = '人数を選択してください';
        ksort($seat_capacity_list);

        $fullURL = \yii\helpers\Url::home() . Yii::$app->controller->id . '/seat';

        $seat_item_keys = [];

        for ($i = 1; $i < 6; $i++) {
            $key = $store['staff_item_' . $i];
            if (!empty($key)) {
                $seat_item_keys[$i] = $key;
            }
        }

        return $this->render('seat', [
                    'timeString' => $time_string,
                    'items' => $usable_seats,
                    'seat_capacity_list' => $seat_capacity_list,
                    'fullURL' => $fullURL,
                    'seat_item_keys' => $seat_item_keys,
                    'formModel' => $formModel
        ]);
    }

}
