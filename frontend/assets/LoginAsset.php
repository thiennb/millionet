<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main backend application asset bundle.
 */
class LoginAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $css = [
		//'css/bootstrap-checkbox.css',
        'css/login-box.css'
    ];

	public $depends = [
        'backend\assets\AdminLteAsset',
        'justinvoelker\awesomebootstrapcheckbox\Asset',
    ];

	public $jsOptions = [
        'position' => \yii\web\View::POS_END
    ];
}
