<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/common.css',
        'css/site.css',
        'css/rank.css',
        'css/jquery.easy-pie-chart.css',
        'css/responsive.css',
        'css/shared.css'
//        'css/salon_header.css',
//        'css/choose_menu.css'
    ];
    public $js = [
        // Scroll
//        'js/pageScrollTop.js',
        'js/common-js.js',
        'js/jquery.easy-pie-chart.js',
        // Get Category
        //'js/ajax_category_onload.js',
        // Get content coupon
        // 'js/ajax_choose.js',
        // 'js/ajax_option.js',
        //'js/common.js',
        // 'js/ajax_step2.js',
        // Get information store
        //'js/ajax_store_onload.js',
        // Click category
        //'js/check_all_category.js',
        //'js/check_coupon.js',
        //'js/paging.js'
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        //'justinvoelker\awesomebootstrapcheckbox\Asset'
    ];
    public $jsOptions = [
        'position' => \yii\web\View::POS_HEAD
    ];
}
