<?php

namespace frontend\assets;
use yii\web\AssetBundle;
//use frontend\assets\dmstr\widgets\Menu;
/**
 * Main frontend application asset bundle.
 */
class BookingOrderAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        //'css/common.css',
        'css/site.css',
        'css/salon_header.css',
        'css/choose_menu.css',
        'css/booking-order.css',
        'css/common.css'
    ];
    public $js = [
        // Scroll
        'js/pageScrollTop.js',
        // Get Category
        //'js/ajax_category_onload.js',
        // Get content coupon
        'js/booking-order.js',
        //'js/ajax_option.js',
        //'js/common.js',
        // 'js/ajax_step2.js',
        // Get information store
        //'js/ajax_store_onload.js',
        // Click category
        //'js/check_all_category.js',
        //'js/check_coupon.js',
        //'js/paging.js'
        'js/booking-checklist.js'
    ];
    public $jsOptions = [
        //'position' => \yii\web\View::POS_LOAD
    ];
	public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'dmstr\web\AdminLteAsset'
    ];
     
}