<?php
namespace frontend\models;

use Yii;
use yii\base\Model;
use common\models\User;

/**
 * Password reset request form
 */
class PasswordResetRequestForm extends Model
{
    public $email;
    public $phone_1;
    public $phone_2;
    public $phone_3;
    public $first_name;
    public $last_name;
    public $first_name_kana;
    public $last_name_kana;
    public $password;
    public $password_re;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['email', 'trim'],
//            ['email', 'required'],
            ['email', 'email'],
            ['email', 'string', 'max'=>60],
            ['email', 'checkExistsEmail'],
            
            [['phone_1','phone_2','phone_3','first_name','last_name','first_name_kana','last_name_kana'], 'required', 'on' => 'request_reset'],
            [['phone_1', 'phone_2', 'phone_3'], 'integer'],
            [['phone_1'], 'string', 'min' => 2, 'max' => 3],
            [['phone_2', 'phone_3'], 'string', 'min' => 4, 'max' => 4],
            [['last_name_kana', 'first_name_kana'], 'match', 'pattern' => '/^([ｧ-ﾝﾞﾟァ-ヺ・ーヽヾヿ]+)$/u', 'message' => Yii::t('frontend', 'Please input Kana character.')],
            [['first_name', 'last_name', 'first_name_kana', 'last_name_kana'], 'string', 'max' => 40],
            
            [['password', 'password_re'], 'required', 'on' => 'reset_password'],
            [['password_re', 'password'], 'match', 'pattern' => "/^([a-zA-Z0-9!@#$%^&*]+)$/u", 'message' => Yii::t('frontend', 'Please enter alphanumeric symbols.')],
            ['password_re', 'compare', 'compareAttribute' => 'password', 'message' => Yii::t('frontend', "Input content does not match")],
            [['password', 'password_re'], 'string', 'min' => 6, 'message' => Yii::t('backend', "The number of digits is not enough"),],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => Yii::t('backend', 'ID'),
            'first_name' => Yii::t('backend', 'First name'),
            'last_name' => Yii::t('backend', 'Last name'),
            'first_name_kana' => Yii::t('backend', 'First name kana'),
            'last_name_kana' => Yii::t('backend', 'Last name kana'),
            'phone_1' => Yii::t('frontend', '※ mobile'),
            'phone_2' => Yii::t('frontend', '※ mobile'),
            'phone_3' => Yii::t('frontend', '※ mobile'),
            'password' => Yii::t('frontend', '※ password'), //Yii::t('backend', 'Password'),
            'password_re' => Yii::t('frontend', '※ password re'), //Yii::t('backend','Password Re'),
        ];
    }

    /**
     * Sends an email with a link, for resetting the password.
     *
     * @return boolean whether the email was send
     */
    public function checkUser($company_id)
    {
        /* @var $user User */
        $user = User::find()->join('INNER JOIN','mst_customer','"user".id = "mst_customer".user_id')->where([
            'status' => User::STATUS_ACTIVE,
            'mobile' => $this->phone_1.$this->phone_2.$this->phone_3,
            'first_name' => $this->first_name,
            'last_name' => $this->last_name,
            'first_name_kana' => $this->first_name_kana,
            'last_name_kana' => $this->last_name_kana,            
            'company_id' => $company_id,
            '"user".del_flg'    =>'0',
            '"mst_customer".del_flg'    =>'0',
        ])->one();

        if (!$user) {
            return null;
        }
        
//        if (!User::isPasswordResetTokenValid($user->password_reset_token)) {
//            $user->generatePasswordResetToken();
//            if (!$user->save()) {
//                return null;
//            }
//        }

//        return Yii::$app
//            ->mailer
//            ->compose(
//                ['html' => 'passwordResetToken-html', 'text' => 'passwordResetToken-text'],
//                ['user' => $user]
//            )
//            ->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->name . ' robot'])
//            ->setTo($this->email)
//            ->setSubject('Password reset for ' . Yii::$app->name)
//            ->send();
        return $user;
    }

    /**
     * Sends an email with a link, for resetting the password.
     *
     * @return boolean whether the email was send
     */
    public function checkExistsEmail($attribute, $params)
    {
        /* @var $user User */
        $user = User::find()->join('INNER JOIN','mst_customer','"user".id = "mst_customer".user_id')->where([
            'status' => User::STATUS_ACTIVE,
            'email' => $this->email,
            '"user".del_flg'    =>'0',
            '"mst_customer".del_flg'    =>'0',
        ])->one();

        if (!$user) {            
            $this->addError($attribute, Yii::t('backend', 'There is no user with such email.'));
        }
    }

    /**
     * Resets password.
     *
     * @return boolean if password was reset.
     */
    public function resetPassword($user_id, $password, $token)
    {
        $user = User::findOne(['id'=>$user_id,'password_reset_token' =>$token]);
        if(empty($user)){
            return FALSE;
        }else{
            $user->setPassword($password);
            $user->removePasswordResetToken();
            $user->save(false);
            return TRUE;
        }
    }
}
