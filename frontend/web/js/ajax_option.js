$(document).ready(function () {
    // **********************
    // Click button Choosse option
    // **********************
    /*$(document).on('click', '.checkCoupon', function () {
        $("input[name=coupon]").on('change', function () {
            var self = $(this);
            var couponId = self.attr('couponId');

            if (self.is(":checked")) {

                $("#" + couponId).removeClass('disabled').removeAttr("disabled");

            } else {

                $("#" + couponId).attr("disabled", true).addClass('disabled');
            }
        });
    });*/
    // **********************
    // End Click button Choosse option
    // **********************

    // **********************
    // Load Clict option
    // **********************
	function addProductToBundle (pr, pr_id, dontAddNodes) {
		var name = pr.find('.radio').text(),
			price = pr.find('.pr-price').text(),
			time = pr.find('.pr-time').text();
		if (!dontAddNodes) {
			var tr ='<tr class="added-pr added-pr-' + pr_id +'"> <td>' + name + '</td> <td>' + price + '</td> <td>' + time + '</td> </tr>';

			$('#confirm_menu_addon').append(tr);
		}

		return parseInt(time);
	}

	function removeProductFromBundle (pr, pr_id) {
		//var time = pr.find('.pr-time').text();

		$('.added-pr-' + pr_id).remove();
	}

	var baseTime = 0;
	var products = [];

	function changeCurrentTotalTime (minutes) {
		var total_time = baseTime;

		total_time += parseInt(minutes);

		var h = Math.floor(total_time / 60);
		var m = total_time % 60;

		var time_text_h = h ? h + ' 時間 ' : '';
		var time_text_m = m ? m + ' 分 ' : '';

		$('.totaltime').html(time_text_h + ' ' + time_text_m);
	}

	// return more time
	function checkAllOptionProducts(dontAddNodes) {
		var moreTime = 0;

		$('.pr-checkbox').each(function (i, e) {
			var pr_id = $(e).val();
			if (pr_id != -1) {
				var pr = $(e).closest('.pr');

				if ($(e).is(':checked')) {
					moreTime += addProductToBundle(pr, pr_id, dontAddNodes);
					products.push(pr_id.split('-')[2]);
				}
				else {
					removeProductFromBundle(pr, pr_id);
					products.push(false);
				}
			}
		});

		return moreTime;
	}

	function getAddonProductQueryString () {
		var ret = [], plength = products.length;

		for (var i = 0; i < plength; i++) {
			// if (products[i] !== false && ret.indexOf(products[i]) === -1) {
			// 	ret.push(products[i]);
			// }
			if (products[i] !== false) {
				ret.push(products[i]);
			}
		}

		if (ret.length) {
			return '&products=' + ret.join(',');
		}
		else {
			return '';
		}
	}

	$(document).on('click', '.pr-checkbox', function () {
		$('#confirm_menu_addon').html('');
		products = [];
		//changeCurrentTotalTime(0);
		changeCurrentTotalTime(checkAllOptionProducts());
	});

    // **********************
    // End Load Clict option
    // **********************

    // **********************
    // Load data Confirm Menu
    // **********************
    // var addMenu = GetURLParameter('addMenu');
    var idStore = GetURLParameter('storeId');
    var couponId = GetURLParameter('couponId');

    if ($("#confirm_menu").length) {
        var pathControler = '/booking-order/getconfirmmenu';
        $.ajax({
            url: pathControler,
            type: 'post',
            data: {
                couponId: couponId,
                idStore: idStore
            },
            success: function (data) {
                $("#confirm_menu").append(data);

				baseTime = parseInt($('#total_time_allcoupon').val());
            }
        });
    }
    // **********************
    // Load data Option menu
    // **********************
    if ($("#option_menu").length) {

        var pathControler = '/booking-order/optionbyidcoupon';

        $.ajax({
            url: pathControler,
            type: 'post',
            data: {
                couponId: couponId,
                idStore: idStore
            },
            success: function (data) {
                $("#option_menu").append(data);

				//************************
				// Reload chosen options
				//************************
				if ($('#confirm_menu_addon').length) {
					var selectedProducts = GetURLParameter('products').split(',');

					$('#confirm_menu_addon').html('');
					products = [];
					$('.pr-checkbox').each(function () {
						var id = this.value.split('-')[2];

						var findPr = selectedProducts.indexOf(id);

						if (findPr !== -1) {
							$(this).prop('checked', 'checked');
							selectedProducts = selectedProducts.slice(findPr, 1);
						}
					});
					changeCurrentTotalTime(checkAllOptionProducts());
				}
				//************************
				// End Reload chosen options
				//************************
            }
        });
    }

    // **********************
    // End Load data Option menu
    // **********************

    // **********************
    // End load confirm menu
    // **********************

    // **********************
    // Load data Time Salon Schedule
    // **********************
    if ($("#time_salon").length) {

        var pathControler = '/booking-order/timesalonschedule';
        $.ajax({
            url: pathControler,
            type: 'post',
            data: {
                couponId: couponId,
                idStore: idStore
            },
            success: function (data) {
                $("#time_salon").append(data);
            }
        });
    }
    // **********************
    // End Load data Time Salon Schedule
    // **********************

    // **********************
    // Get Information coupon for Salon Schedule
    // **********************
    function GetInformationCouponForSalonSchedule()
    {

    }
    // **********************
    // End Get Information coupon for Salon Schedule
    // **********************
    //
	// **********************
    // Load order detail
    // **********************
    var totalPoint = 0;
	var totalMoney = 0;
	var oldValidPoint = 0;
    if ($('#load-order-detail-here').length) {

        var pathControler = '/booking-order/loadorderdetail';
        $.ajax({
            url: pathControler,
            type: 'post',
            data: {
                couponId: couponId,
                storeId: idStore, date: GetURLParameter('date'),
				productId: GetURLParameter('products'),
				staffId: GetURLParameter('staff')
            },
            success: function (data) {
                $('#load-order-detail-here').append(data.replace(/<script.+script>/, ''));

				$('#booking-need-to-pay').html($('#booking-money').html());

				totalMoney = parseInt($('#booking-money').text().replace(',', ''));
				totalPoint = parseInt($('#booking-point-total').text());

				$('#booking-points').change(function () {
					var usingPoints = parseInt($(this).val());
					if (!usingPoints) usingPoints = 0;

					if (usingPoints <= totalPoint) {
						oldValidPoint = usingPoints;

						$('#booking-need-to-pay').html(totalMoney - oldValidPoint);
					}
					else {
						alert('Exceed maximum point rate');

						$(this).val(oldValidPoint);
					}
				});
            }
        });
    }
    // **********************
    // End Load order detail
    // **********************

    // **********************
    // Get parameter url
    // **********************
    function GetURLParameter(sParam)
    {
        var sPageURL = window.location.search.substring(1);
        var sURLVariables = sPageURL.split('&');
        for (var i = 0; i < sURLVariables.length; i++)
        {
            var sParameterName = sURLVariables[i].split('=');
            if (sParameterName[0] === sParam)
            {
                return sParameterName[1];
            }
        }
    }
    // **********************
    // End Get parameter url
    // **********************

	$(document).on('click', '.btn_to_time_schedule', function ()
    {
        var check_coupon = 0;
        var couponId = GetURLParameter('couponId');
		products = [];
		var time_execute = baseTime + checkAllOptionProducts(true);
		var idStore = GetURLParameter('storeId');

		var h = parseInt(time_execute / 60);
		var m = time_execute % 60;
        var pathname = '/booking-order/salonschedule' + '?storeId=' + idStore + '&couponId=' + couponId+ '&addMenu=2&executeTime=' + h + ':' + m + ':00' + getAddonProductQueryString();
        $(location).attr('href', pathname);
    });
});
