$('.multiple-input-list').load(".input-decimal", function(){
    $(this).numeric({ decimal : ".",  negative : false, scale: 3 });
});
$('#mastermember-phone_1').keyup(function(){
    if($(this).val().length == 3){
        $('#mastermember-phone_2').focus();
    }
});

$('#mastermember-phone_2').keyup(function(){
    if($(this).val().length == 4){
        $('#mastermember-phone_3').focus();
    }
});

$('#loginform-phone_1').keyup(function(){
    if($(this).val().length == 3){
        $('#loginform-phone_2').focus();
    }
});

$('#loginform-phone_2').keyup(function(){
    if($(this).val().length == 4){
        $('#loginform-phone_3').focus();
    }
});

$('#mastermember-phone_1_re').keyup(function(){
    if($(this).val().length == 3){
        $('#mastermember-phone_2_re').focus();
    }
});

$('#mastermember-phone_2_re').keyup(function(){
    if($(this).val().length == 4){
        $('#mastermember-phone_3_re').focus();
    }
});

$('#mastermember-phone_1_new').keyup(function(){
    if($(this).val().length == 3){
        $('#mastermember-phone_2_new').focus();
    }
});

$('#mastermember-phone_2_new').keyup(function(){
    if($(this).val().length == 4){
        $('#mastermember-phone_3_new').focus();
    }
});