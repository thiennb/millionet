$(function(){
   $('.max-choice-checkbox').change(function(e){
        var parent = $(this).closest('td');
        if($(parent).find('input:checked').length > $(parent).data('maxchoice')){
            $(this).prop('checked', false);
        }
    });
    
    $(document).on('change keyup paste', '.freedom-text', function(e){
        if($(this).val().length > 100){
           $(this).val($(this).val().substring(0, 100)); 
        }
    });
    
    function checkIsCheckedRequired(){
        $isOK = true;
        $('.check-row').each(function(){
            if($(this).find('input').length != 0){
                $isOK = false;
                if($(this).find('input:checked').length != 0){
                    $isOK = true;
                }
            }
        });
        
        $('.agree-checkbox').each(function(){
            if(!$(this).is(":checked")){
                $isOK = false;
            }
        });
        
        $('.freedom-text').each(function(){
            if($(this).val() == ''){
                $isOK = false;
            }
        });
        
        if(!$isOK){
            $('.btn-submit').prop('disabled', true);
            $('.btn-submit').addClass('btn disabled');
        } else {
            $('.btn-submit').prop('disabled', false);
            $('.btn-submit').removeClass('btn disabled');
        }
    }
    
    $(document).on('change keyup paste', '.agree-checkbox, .freedom-text, .check-row input', function(event){
        checkIsCheckedRequired();
    });
    
    checkIsCheckedRequired(); 
});