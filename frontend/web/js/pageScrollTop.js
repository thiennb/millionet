// 著作権  ：Copyright(c) 2015 RECRUIT CO.,LTD.
// 会社名  ：株式会社リクルート

var HPB = HPB || {};
HPB.PAGETOPSCROLL = HPB.PAGETOPSCROLL || {};

HPB.PAGETOPSCROLL.$win = $(window);

HPB.PAGETOPSCROLL.pageTopMove = function() {
	var scrollElement,
		scrollTimer,
		topDistance,
		_self;
	return {
		init: function() {
			_self = this;
			scrollElement = '<a href="javascript:void(0);" class="pageTopImg" id="jsiPageTopScrollButton">PAGETOP</a>';
			topDistance = 300;
			scrollTimer = false;
			scrollTimer = setTimeout(function() {
				_self.showScrollElement(HPB.PAGETOPSCROLL.$win);
			}, 200);
			this.bind();
		},
		bind: function() {
			_self = this;
			$('body').append(scrollElement);
			_self.showScrollElement(HPB.PAGETOPSCROLL.$win);
			HPB.PAGETOPSCROLL.$win.scroll(function() {
				if (scrollTimer !== false) {
					clearTimeout(scrollTimer);
				}
				scrollTimer = setTimeout(function() {
					_self.showScrollElement($(this));
				}, 200);
			});
			$('#jsiPageTopScrollButton').on('click', function () {
				$('html, body').animate({scrollTop: 0 });
			});
		},
		showScrollElement: function(_self){
			if(_self.scrollTop() > topDistance) {
				$('#jsiPageTopScrollButton').fadeIn();
			} else {
				$('#jsiPageTopScrollButton').fadeOut();
			}
		}
	};
};

$(function() {
	HPB.PAGETOPSCROLL.pageTopMove().init();
});
