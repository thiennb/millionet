// @author HR
// @version 1.0.2
//======== CHANGELOG ==========
// @since 1.0.2 Products no longer contains options, they are SEPARATED, then MERGED in controller
(function ($) {
    'use strict';
    var combine = null;
    $(document).ready(function () {
        var page = 1;
        var totalPage = 1;

        function createList(constructor, baseClass) {
            var childClass = constructor;
            childClass.prototype = new baseClass();
            childClass.prototype.constructor = constructor;
            return childClass;
        }

        // -------------------
        // BaseList
        var BaseList = function () { this.content = []; };
        BaseList.prototype = {
            // @return array List of content
            all: function () {
                var ret = [];
                for (var i = 0; i < this.content.length; i++) {
                    if (this.content[i] !== false)
                        ret.push(this.content[i]);
                }

                return ret;
            },
            // @return string List of content element separated by commas
            list: function () {
                return this.all().join(',');
            },
            // @return boolean Check if this list content an id
            has: function (id) {
                return this.content.indexOf(id) !== -1;
            },
            add: function (id) {
                if (!this.has(id)) {
                    this.content.push(id);
                }
            },
            remove: function (id) {
                for (var i = 0; i < this.content.length; i++) {
                    if (this.content[i] == id)
                        this.content[i] = false;
                }
            },
            removeOne: function (id) {
                for (var i = 0; i < this.content.length; i++) {
                    if (this.content[i] == id)
                        this.content[i] = false;
                    return true;
                }

                return false;
            }
        };

        // COUPONS
        var coupons = (function (c, combine) {
            function updateCouponList(id) {
                if (id !== undefined) {
                    var coupon = $('#coupon-' + id);

                    if (coupon.hasClass('coupon-non-combinable')) {
                        $('.coupon').each(function () {
                            $(this).addClass('disabled');
                            var id = this.id.replace('coupon-', '');
                            $('#submit-coupon-' + id).prop('disabled', true).addClass('disabled');
                        })
                        $('.select-booking-coupon').prop('disabled', true);
                        coupon.removeClass('disabled');
                        coupon.find('.select-booking-coupon').prop('disabled', false);
                        $('#submit-coupon-' + id).prop('disabled', false).removeClass('disabled');
                    } else {
                        $('.coupon').each(function () {
                            if ($(this).hasClass('coupon-non-combinable')) {
                                $(this).find('.select-booking-coupon').prop('disabled', true);
                                $(this).addClass('disabled');

                                var id = this.id.replace('coupon-', '');
                                $('#submit-coupon-' + id).prop('disabled', true).addClass('disabled');
                            }
                        });

                        coupon.removeClass('disabled');
                        coupon.find('.select-booking-coupon').prop('disabled', false);
                        $('#submit-coupon-' + id).prop('disabled', false).removeClass('disabled');
                    }
                } else {
                    if (!coupons.list()) {
                        // revert coupons to their original status
                        $('.coupon').removeClass('disabled');
                        $('.select-booking-coupon').prop('disabled', false);
                    } else {
                        updateCouponList(coupons.all()[0]);
                    }
                }
            }

            var List = createList(function (coupons) {
                if (typeof coupons !== 'string' || !coupons.match(/^\d+(,\d+){0,}$/)) {
                    this.content = [];
                } else {
                    this.content = coupons.split(',');
                }

                this.combinable = null;
                this.realCoupons = new BaseList(); // coupon benefits_content 02 03

                var currentList = this;

                $(document).on('click', '.select-booking-coupon', function () {
                    $(this).toggleClass('selected').toggleText();

                    if ($(this).is(':checked')) {
                        currentList.add(this.value);
                        $('#submit-coupon-' + this.value).prop('disabled', false).removeClass('disabled');
                        if ($('#coupon-' + this.value).hasClass('coupon-benefit-02') || $('#coupon-' + this.value).hasClass('coupon-benefit-03')) {
                            currentList.realCoupons.add(this.value);
                        }
                        currentList.combine();
                    } else {
                        currentList.remove(this.value);
                        $('#submit-coupon-' + this.value).prop('disabled', true).addClass('disabled');
                        currentList.realCoupons.remove(this.value);
                        currentList.combine();
                    }
                });
            }, BaseList);

            List.prototype.combine = function () {
                if (this.all().length === 0) {
                    // revert coupons to their original status
                    $('.coupon').removeClass('disabled');
                    $('.select-booking-coupon').prop('disabled', false);
                    return true; // exit
                }

                var all = this.all();
                var id  = all[all.length - 1];
                var coupon = $('#coupon-' + id);

                if (coupon.length) {
                    if (coupon.hasClass('coupon-non-combinable')) {
                        this.combinable = false;
                    }
                    else {
                        this.combinable = true;
                    }
                }

                if (this.combinable === true) {
                    $('.coupon').each(function () {
                        if ($(this).hasClass('coupon-non-combinable')) {
                            $(this).find('.select-booking-coupon').prop('disabled', true);
                            $(this).addClass('disabled');

                            var id = this.id.replace('coupon-', '');
                            $('#submit-coupon-' + id).prop('disabled', true).addClass('disabled');
                        }
                    });

                    coupon.removeClass('disabled');
                    coupon.find('.select-booking-coupon').prop('disabled', false);
                    $('#submit-coupon-' + id).prop('disabled', false).removeClass('disabled');
                }
                else if (this.combinable === false) {
                    $('.coupon').each(function () {
                        $(this).addClass('disabled');
                        var id = this.id.replace('coupon-', '');
                        $('#submit-coupon-' + id).prop('disabled', true).addClass('disabled');
                    });
                    $('.select-booking-coupon').prop('disabled', true);
                    coupon.removeClass('disabled');
                    coupon.find('.select-booking-coupon').prop('disabled', false);
                    $('#submit-coupon-' + id).prop('disabled', false).removeClass('disabled');
                }
            };

            List.prototype.init = function () {
                var currentList = this;
                currentList.combine();
                $('.select-booking-coupon').each(function () {
                    if (currentList.has(this.value) && $(this).prop('disabled', false)) {
                        $(this).trigger('click');
                    }
                });
            };

            return new List(c);
        })(getParam('coupons'));

        // PRODUCTS
        var products = (function (c) {
            var List = createList(function (input) {
                if (typeof input !== 'string' || !input.match(/^\d+(,\d+){0,}$/)) {
                    this.content = [];
                } else {
                    this.content = input.split(',');
                }

                var currentList = this;

                $(document).on('click', '.select-booking-product', function () {
                    $(this).toggleClass('selected').toggleText();

                    if ($(this).is(':checked')) {
                        currentList.add(this.value);
                        $('#submit-product-' + this.value).prop('disabled', false).removeClass('disabled');
                    } else {
                        currentList.remove(this.value);
                        $('#submit-product-' + this.value).prop('disabled', true).addClass('disabled');
                    }
                });
            }, BaseList);

            List.prototype.init = function () {
                var currentList = this;
                $('.select-booking-product').each(function () {
                    if (currentList.has(this.value)) {
                        $(this).trigger('click');
                    }
                });
            };

            return new List(c);
        })(getParam('products'));

        // CATEGORIES
        var categories = (function (c) {
            var List = createList(function (input) {
                if (typeof input !== 'string' || !input.match(/^\d+(,\d+){0,}$/)) {
                    this.content = [];
                } else {
                    this.content = input.split(',');
                }

                var currentList = this;

                $(document).on('click', '.filter-category', function () {
                    if ($(this).is(':checked')) {
                        currentList.add(this.value);
                    } else {
                        currentList.remove(this.value);
                    }
                });
            }, BaseList);

            return new List(c);
        })();

        // MODES
        var modes = (function (c) {
            var List = createList(function (input) {
                if (typeof input !== 'string') {
                    this.content = [];
                } else {
                    this.content = input.split(',');
                }

                var currentList = this;

                $(document).on('click', '.filter-mode', function () {
                    currentList.content = [];
                    currentList.add(this.value);
                    window.location.hash = this.value;
                    page = 1;

                    loadCouponProduct();
                });
            }, BaseList);

            return new List(c);
        })(window.location.hash.replace('#', '') || 'coupons');

        // OPTIONS
        var options = (function (c) {
            function updateOptionList(currentList) {
                var optionList = [];
                var optionListHTML = [];
                var added_time = 0, added_price = 0;

                $('.select-option').each(function () {
                    if (this.value != -1) {
                        var product_id = this.value.split('-')[1];

                        if ($(this).is(':checked')) {
                            var option = $(this).parent(),
                                    name = option.children('.option-name').val(),
                                    time = parseInt(option.children('.option-time').val()),
                                    price = parseInt(option.children('.option-price').val());

                            var cate = option.parent().children('.pr-category').html();

                            optionList.push(this.value);
                            optionListHTML.push('<tr class="added-pr added-pr-' + this.value + '"><td>' + cate + name + '</td><td>' + formatMoney(price) + '</td><td>' + time + ' 分</td></tr>');

                            //products.add(product_id);

                            added_time += time;
                            added_price += price;
                        } else {
                            //products.remove(product_id);
                        }
                    }
                });

                $('#confirm_menu_addon').html(optionListHTML);
                $('#total-time').html(formatTime(currentList.baseTime + added_time));
                $('#total-price').html(formatMoney(currentList.basePrice + added_price));

                if (!optionList.length) {
                    $('.no-option-alert').show();
                }
                else {
                    $('.no-option-alert').hide();
                }

                return optionList;
            }

            var List = createList(function (input) {
                if (typeof input !== 'string') {
                    this.content = [];
                } else {
                    this.content = input.split(',');
                }

                this.baseTime = 0;
                this.basePrice = 0;

                var currentList = this;

                $(document).on('click', '.select-option', function () {
                    currentList.content = updateOptionList(currentList);
                });
            }, BaseList);

            List.prototype.getTotalTime = function () {
                var added_time = 0, added_price = 0;

                $('.select-option').each(function () {
                    if (this.value != -1 && $(this).is(':checked')) {
                        var option = $(this).parent();
                        var time = parseInt(option.children('.option-time').val()),
                                price = parseInt(option.children('.option-price').val());

                        added_time += time;
                        added_price += price;
                    }
                });

                return this.baseTime + added_time;
            };

            List.prototype.init = function () {
                var currentList = this;

                $('.select-option').each(function () {
                    if (currentList.has(this.value)) {
                        this.checked = true;
                    }
                });

                updateOptionList(this);
            };

            return new List(c);
        })(getParam('options'));

        //=============================
        // POINT INPUT CHANGE EVENT
        //============================
        var point = (function () {
            var PointInputProfessor = function () {
                this.ratio = 0; // conversionYen / conversionP
                this.totalMoney = 0;
                this.totalPoint = 0;
            };

            PointInputProfessor.prototype.pointToMoney = function (point) {
                return point * this.ratio;
            };

            PointInputProfessor.prototype.moneyToPoint = function (money) {
                if (this.ratio == 0) {
                    return 0;
                }

                return money / this.ratio;
            };

            PointInputProfessor.prototype.suggestPoint = function () {
                return Math.floor(this.moneyToPoint(this.totalMoney));
            };

            function inputFailEvent() {
                $(this).select();
                return false;
            }

            function inputChange(input, professor, keyCode) {
                var val = $(input).val();

                if (val.match(/^[\-]{0,1}\d+$/) || val.length == 0) {
                    var usingPoints = parseInt(val);
                    if (!usingPoints)
                        usingPoints = 0;

                    if (usingPoints < 0) {
                        showInputError(input, 'ご利用ポイント は0よりてはいけません。');
                        return inputFailEvent.bind(input);
                    }

                    if (usingPoints > professor.totalPoint) {
                        showInputError(input, '利用ポイント は現在ポイントより大きくてはいけません。');
                        return inputFailEvent.bind(input);
                    }

                    var finalMoney = professor.totalMoney - professor.pointToMoney(usingPoints);

                    if (finalMoney < 0) {
                        showInputError(input, usingPoints + "ポイントを利用して購入したら、支払い予定金額が0円より小さくなっていますので再度入力してください。<br>（支払い予定金額が0円なら、" + professor.suggestPoint() + "ポイントをご利用してください。）");
                        return inputFailEvent.bind(input);
                    } else {
                        $('#booking-need-to-pay').html(formatMoney(finalMoney));
                        showInputError(input, ''); //hide error
                        return true;
                    }
                } else {
                    showInputError(input, 'ポイントの書式が正しくありません。');
                    return inputFailEvent.bind(input);
                }
            }

            PointInputProfessor.prototype.bindEventOn = function (selector, callback) {
                var professor = this;
                if (!callback) {
                    callback = function () {};
                }

                $(document).on('keypress', selector, function (e) {
                    if (e.which == 13) // Enter key = keycode 13
                    {
                        e.preventDefault();
                    }
                    //callback.bind(this, inputChange(this, professor));
                });

                $(document).on('keyup', selector, function (e) {
                    // callback input, validate = true/false
                    callback.bind(this, inputChange(this, professor));
                });
            };

            return new PointInputProfessor();
        })();

        point.ratio = parseFloat($('#point-yen').val()) / parseFloat($('#point-p').val());
        point.totalMoney = parseInt($('#total-money').val());
        point.totalPoint = parseInt($('#total-point').val());

        point.bindEventOn('#booking-points');

        function loadPartial(selector, url, post, callback) {
            var loaddingMessage = $('<div/>', {class: 'loading-message'}).text('Loading ...');
            var container = $(selector);
            if (container.length) {
                //container.append(loaddingMessage);

                $.ajax({
                    url: url,
                    type: 'post',
                    data: post,
                    success: function (data) {
                        callback(container, data);
                        loaddingMessage.remove();
                    }
                });
            }
        }

        loadPartial('#header', SITE_CONTROLLER + 'loadstoreinformation', {
            storeId: getParam('storeId')
        }, function (container, data) {
            container.html(data);
        });

        loadPartial('#list-filters', SITE_CONTROLLER + 'loadcategories', {
            storeId: getParam('storeId')
        }, function (container, data) {
            container.html(filterData(data));

            $('#btn-filter').click(function () {
                loadCouponProduct();
            });
        });

        // !frontend only
        loadPartial('#load-option-menu', SITE_CONTROLLER + 'loadoptionmenu', {
            storeId: getParam('storeId'),
            coupons: coupons.list(),
            products: products.list()
        }, function (container, data) {
            container.replaceWith(filterData(data));
            options.baseTime = parseInt($('#base-time').val());
            options.basePrice = parseInt($('#base-price').val());

            options.init();
        });

        loadPartial('#load-confirm-menu', SITE_CONTROLLER + 'loadconfirmmenu', {
            storeId: getParam('storeId'),
            coupons: coupons.list(),
            products: products.list()
        }, function (container, data) {
            container.replaceWith(filterData(data));
            options.baseTime = parseInt($('#base-time').val());
            options.basePrice = parseInt($('#base-price').val());

            options.init();
        });

        loadPartial('#list-staff', SITE_CONTROLLER + 'loadstaff', {
            storeId: getParam('storeId'),
            executeTime: getParam('executeTime'),
            products: getParam('products'),
            coupons: getParam('coupons'),
            options: getParam('options')
        }, function (container, data) {
            container.replaceWith(filterData(data));
        });

        loadPartial('#list-money-and-point', SITE_CONTROLLER + 'listmoneyandpoint', {
            storeId: getParam('storeId'),
            executeTime: getParam('executeTime'),
            products: getParam('products'),
            coupons: getParam('coupons'),
            options: getParam('options'),
            date: getParam('date'),
            staff: getParam('staff')
        }, function (container, data) {
            container.replaceWith(filterData(data));
        });

        loadPartial('#load-order-detail', SITE_CONTROLLER + 'loadorderdetail', {
            storeId: getParam('storeId'),
            executeTime: getParam('executeTime'),
            products: getParam('products'),
            coupons: getParam('coupons'),
            options: getParam('options'),
            date: getParam('date'),
            staff: getParam('staff'),
            seat: getParam('seat'),
            capacity: getParam('capacity')
        }, function (container, data) {
            container.html(filterData(data));

            point.ratio = parseFloat($('#point-yen').val()) / parseFloat($('#point-p').val());
            point.totalMoney = parseInt($('#total-money').val());
            point.totalPoint = parseInt($('#total-point').val());

            console.log($('#point-yen').val());
            console.log($('#point-p').val());

            $('#booking-need-to-pay').html(formatMoney(point.totalMoney));
        });

        function loadCouponProduct() {
            $('#couponTableData').html('');
            $('#booking-error').hide();

            if (modes.has('coupons')) {
                loadPartial('#couponTableData', SITE_CONTROLLER + 'loadcoupons', {
                    storeId: getParam('storeId'),
                    categories: categories.list(),
                    selectedCoupons: coupons.list(),
                    page: page
                }, function (container, data) {
                    data = filterData(data);
                    container.html(data);
                    if ($('.nothing-found').length) {
                        showError('何も見つかりませんでした。条件を変えて検索してください。');
                    }
                    if (typeof bookingCombinable !== 'undefined') {
                        coupons.combinable = bookingCombinable;
                    }
                    coupons.realCoupons.content = bookingSetCoupons;
                    coupons.init();
                    totalPage = parseInt($('.total-page').text());
                });
            }

            if (modes.has('products')) {
                loadPartial('#couponTableData', SITE_CONTROLLER + 'loadproducts', {
                    storeId: getParam('storeId'),
                    categories: categories.list(), page: page
                }, function (container, data) {
                    data = filterData(data);
                    container.html(data);
                    if ($('.nothing-found').length) {
                        showError('何も見つかりませんでした。条件を変えて検索してください。');
                    }
                    products.init();
                    totalPage = parseInt($('.total-page').text());
                });
            }
        }
        loadCouponProduct();

        //============================
        // jQuery Plugins
        //============================
        $.fn.toggleText = function () {
            // new toggle text
            var newText = $(this).data('toggleText') || $(this).text();

            // backup current text
            $(this).data('toggleText', $(this).text());

            // apply toggleText
            $(this).text(newText);
        };

        //=============================
        // HELPERS
        //============================
        // @ref
        function getParam(sParam) {
            var sPageURL = window.location.search.substring(1);
            var sURLVariables = sPageURL.split('&');
            for (var i = 0; i < sURLVariables.length; i++)
            {
                var sParameterName = sURLVariables[i].replace(/%2C/g, ',').split('=');
                if (sParameterName[0] === sParam)
                {
                    return sParameterName[1];
                }
            }

            return '';
        }

        function formatMoney(money) {
            return '¥' + money.toFixed(1).replace(/(\d)(?=(\d{3})+\.)/g, '$1,').split('.')[0];
        }

        function formatTime(min) {
            var h = Math.floor(min / 60);
            var m = min % 60;
            var ret = '';

            if (h > 0) {
                ret += h + '時間';
            } else if (m == 0) {
                ret = 0 + '分';
            }

            if (m > 0) {
                ret += m + '分';
            }

            return ret;
        }

        function filterData(data) {
            return data.replace(/<script.+script>/, '');
        }

        function pointToMoney(point) {
            return parseInt(point) * conversionYen / conversionP;
        }

        function showInputError(input, msg) {
            $(input).parent().find('.help-block-error').html(msg);
        }

        function showError(message) {
            $('#booking-error').children('label').text(message);
            $('#booking-error').show().css('display', 'block !important');
            window.scrollTo(0, $('#booking-error').offset().top - 20);
        }

        $(document).on('click', '.btn-close-alert', function () {
            $(this).parent().hide().css('display', 'none !important');
        });

        // buttons
        $(document).on('click', '.prev-page.enabled', function () {
            page--;
            if (page < 1)
                page = 1;
            loadCouponProduct();
        });

        $(document).on('click', '.next-page.enabled', function () {
            page++;
            if (page > totalPage)
                page = totalPage;
            loadCouponProduct();
        });

        $('.btn-top').click(function () {
            window.scrollTo(0, 0);
        });

        $('.btn-next').click(function () {
            switch (this.value) {
                case '1':
                    // nothing chosen
                    if (!products.list() && !coupons.list()) {
                        showError('クーポンまたは商品を選択してください。');

                        return;
                    }

                    window.location.href = 'options?storeId=' + getParam('storeId') + '&products=' + products.list() + '&coupons=' + coupons.list() + '&options=' + options.list();
                    break;
                case '2':
                    window.location.href = 'staff?storeId=' + getParam('storeId') + '&products=' + products.list() + '&coupons=' + coupons.list() + '&options=' + options.list() + '&executeTime=' + options.getTotalTime();
                    break;
                default:

            }
        });

        function checkProductCoupons () {
            if (products.all().length === 0) {
                if (coupons.all().length === 0) {
                    showError('クーポンまたは商品を選択してください。');
                    return false;
                }
                else if (coupons.realCoupons.all().length === 0) {
                    showError('セットのクーポンまたは商品を選択してお願います。');
                    return false;
                }

                return true;
            }
            return true;
        }

        function processToOptionsPage () {
            if (checkProductCoupons()) {
                var pathname = SITE_CONTROLLER + 'options' + '?storeId=' + getParam('storeId') + '&coupons=' + coupons.list() + '&products=' + products.list() + '&options=' + options.list() + '&addMenu=1';
                $(location).attr('href', pathname);
            }
        }

        $(document).on('click', '.btn_submitCoupon_check', processToOptionsPage);

        $(document).on('click', '.btn_sumitCoupon', processToOptionsPage);

        $(document).on('click', '.btn_to_time_schedule', function () {
            var time_execute = options.getTotalTime();
            var h = parseInt(time_execute / 60);
            var m = time_execute % 60;
            if (typeof SCHEDULE_MODE === 'undefined') {
                SCHEDULE_MODE = 'salon';
            }
            var pathname = SITE_CONTROLLER + SCHEDULE_MODE + 'schedule' + '?storeId=' + getParam('storeId') + '&coupons=' + coupons.list() + '&addMenu=2&executeTime=' + h + ':' + m + ':00' + '&products=' + products.list() + '&options=' + options.list();
            $(location).attr('href', pathname);
        });
    });
})(jQuery);

jQuery.fn.centerModal = function () {
    $(this).modal();
    $(this).on('shown.bs.modal', function () {
        $(this).find('.modal-content').each(function(i, e) {
            $(e).animate({marginTop : (($(window).height() - $(e).height())/2) + 'px'})
        });
    });
};
