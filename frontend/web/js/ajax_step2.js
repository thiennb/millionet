$(document).ready(function () {

    // Choose styple list
    $(document).on('click', '.btn_styleList', function () {

        var idStore = GetURLParameter('storeId');

        var idCoupon = GetURLParameter('couponId');

        var pathname = '/booking-order/scheduledstylist' + '?storeId=' + idStore + '&couponId=' + idCoupon;
        $(location).attr('href', pathname);

    });

     // Choose styple store
    $(document).on('click', '.btn_timedate_store', function () {

        var idStore = GetURLParameter('storeId');

        var idCoupon = GetURLParameter('couponId');

        var pathname = '/booking-order/salonschedule' + '?storeId=' + idStore + '&couponId=' + idCoupon;
        $(location).attr('href', pathname);

    });

    function GetURLParameter(sParam)
    {
        var sPageURL = window.location.search.substring(1);
        var sURLVariables = sPageURL.split('&');
        for (var i = 0; i < sURLVariables.length; i++)
        {
            var sParameterName = sURLVariables[i].split('=');
            if (sParameterName[0] === sParam)
            {
                return sParameterName[1];
            }
        }
    }

});
