function Paging(options,idCategory,idAppDefault) {
    
        //=============================================
        //Cac gia mac cua options
        //=============================================
        var defaults = {
            "rows"       : "#couponTableData",
            "pages"      : "#pages",
            "items"      : 6,
            "currentPage": 1,
            "total"      : 0,
            "btnPrevious": ".goPrevious",
            "btnNext"    : ".goNext",
            "txtCurrentPage": "#currentPage",
            "pageInfo": ".pageInfo"
        };
        options = $.extend(defaults, options);

        //=============================================
        //Cac bien se su dung trong Plugin
        //=============================================
        var rows            = $(options.rows);
        var pages           = $(options.pages);
        var btnPrevious     = $(options.btnPrevious);
        var btnNext         = $(options.btnNext);
        var txtCurrentPage  = $(options.txtCurrentPage);
        var lblPageInfo     = $(options.pageInfo);

        var aRows = '';

        //=============================================
        //Khoi tao cac ham can thi khi Plugin duoc su dung
        //=============================================
        init();

        //=============================================
        //Ham khoi dong
        //=============================================
        function init() {
            //Lay tong so trang 
            var pathname = '/booking-order/sample';
            
            $.ajax({
                url: pathname,
                type: 'post',
                data: {
                    searchCategory: idCategory,
                    limit : options.items
                },
                success: function (data) {
                    options.total = data.total;
                    console.log(options);
                    pageInfo();
                    loadData(options.currentPage);
                    
                }
            });

            //Gan su kien vao cho btnPrevious - btnNext - txtCurrentPage
            setCurrentPage(options.currentPage);

            btnPrevious.on("click", function (e) {
                goPrevious();
                e.stopImmediatePropagation();
            });

            btnNext.on("click", function (e) {
                goNext();
                e.stopImmediatePropagation();
            });
        }

        //=============================================
        //Ham xu ly khi nhan vao nut btnPrevious
        //=============================================
        function goPrevious() {
          
            if (options.currentPage != 1) {
                var p = options.currentPage - 1;
                loadData(p);
                setCurrentPage(p);
                options.currentPage = p;
                pageInfo();
            }
        }
        function gotop() {
            $('html,body').animate({
                scrollTop: 0
            }, 800);
        }

        //=============================================
        //Ham xu ly khi nhan vao nut btnNext
        //=============================================
        function goNext() {
            
            if (options.currentPage != options.total) {
                var p = options.currentPage + 1;
                loadData(p);
                setCurrentPage(p);
                options.currentPage = p;
                pageInfo();
            }
        }

        //=============================================
        //Ham xu ly gan gia tri vao 
        //trong o textbox currentPage
        //=============================================
        function setCurrentPage(value) {
            txtCurrentPage.val(value);
        }

        //=============================================
        //Ham hien thi thong tin phan trang
        //=============================================
        function pageInfo() {
            lblPageInfo.text("Page " + options.currentPage + " of " + options.total);
            
        }
        
        //=============================================
        //Ham load cac thong trong database va dua vao #row
        //=============================================
        function loadData(page) {

            $.ajax({
                url: "../controler_pagin/Pagin_CSDL.php?type=list&items="+options.items+"&idCategory="+idCategory+"&idAppDefault="+idAppDefault+"&currentPage="+page,
                type: "GET",
                dataType: "json",
            }).done(function (data) {

                if (data.length > 0) {
                    var url = document.location.origin + '/';
                    rows.empty();
                    $.each(data, function (i, val) {
                       
                        var str = '';
                        rows.append(str);
                          
                    });
                    
                }else{
                   
                    $('#blog-pager-tb').show();
                    $('#blog-pager').hide();
                    $('#blog-pager1').hide();
                    
                }
            });
        }
    }
    
function callPaging(idCategory)
{
    var obj = {'items': 10};

    Paging(obj,idCategory,idAppDefault);
}




