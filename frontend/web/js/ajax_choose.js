$(document).ready(function () {
	var currentPage = 1;
	var totalPage = 0;
	var loadMode = window.location.hash || '#coupons';
	var coupons = GetURLParameter('couponId');
	var baseTime = 0;
	var addon = [];

	var options = [];

	function showError (message) {
		$('#booking-error').children('label').text(message);
		$('#booking-error').show();
	}

	$(document).on('click', '.btn-close-alert', function(){
		$(this).parent().hide();
	});

	// COUPONS
	if (typeof coupons !== 'string' || !coupons.match(/^\d+(,\d+){0,}$/)) {
		coupons = [];
	}
	else {
		coupons = coupons.split(',');
	}
	function reInitCoupons () {
		if (coupons.length > 0) {
			$('.checkCoupon').each(function () {
				if(coupons.indexOf(this.value) !== -1) {
					this.checked = true;

					addCoupon(this.value);
				}
			});
		}
	}

	function addCoupon (id) {
		if (coupons.indexOf(id) === -1) {
			coupons.push(id);
		}

		$('#submit-checkCoupon-' + id).prop('disabled', false).removeClass('disabled');
	}

	function removeCoupon (id) {
		for (var i = 0; i < coupons.length; i++) {
			if (coupons[i] == id) coupons[i] = false;
		}

		$('#submit-checkCoupon-' + id).prop('disabled', true).addClass('disabled');
	}

	function getCouponList () {
		var ret = [];
		for (var i = 0; i < coupons.length; i++) {
			if (coupons[i] !== false) ret.push(coupons[i]);
		}

		return ret.join(',');
	}

	$(document).on('change', '.checkCoupon', function () {
		if (this.checked) {
			addCoupon(this.value);
		}
		else {
			removeCoupon(this.value);
		}
	});

	var options = GetURLParameter('options').split(',');
	// PRODUCTS
	var products = GetURLParameter('products');

	if (typeof products !== 'string' || !products.match(/^\d+(,\d+){0,}$/)) {
		products = [];
	}
	else {
		products = products.split(',');

		if(GetURLParameter('addMenu') == '1') {
			if(options.length > 1) {
				products = products.splice(0, products.length - options.length);
			}
		}
	}

	console.log(products);


	function reInitProducts () {
		if (products.length > 0) {
			$('.checkProduct').each(function () {
				if(products.indexOf(this.value) !== -1) {
					this.checked = true;

					addProduct(this.value);
				}
			});
		}
	}

	function addProduct (id) {
		products.push(id);

		console.log(getProductList());

		$('#submit-checkProduct-' + id).prop('disabled', false).removeClass('disabled');
	}

	function removeProduct (id) {
		for (var i = 0; i < products.length; i++) {
			if (products[i] == id) products[i] = false;
		}

		$('#submit-checkProduct-' + id).prop('disabled', true).addClass('disabled');
	}

	function getProductList (onlyProducts) {
		var ret = [];
		for (var i = 0; i < products.length; i++) {
			if (products[i] !== false) ret.push(products[i]);
		}

		if (!onlyProducts) {
			for (var i = 0, pr_id; i < addon.length; i++) {
				pr_id = addon[i].split('-')[2];
				ret.push(pr_id);
			}
		}

		return ret.join(',');
	}

	$(document).on('change', '.checkProduct', function () {
		if (this.checked) {
			addProduct(this.value);
		}
		else {
			removeProduct(this.value);
		}
	});

	// OPTIONS
	//
	var addon = GetURLParameter('options');

	if (typeof addon !== 'string' || !addon.match(/^\d+\-\d+\-\d+(,\d+\-\d+\-\d+){0,}$/)) {
		addon = [];
	}
	else {
		addon = addon.split(',');
	}

	function getOptionList () {
		var ret = [];
		for (var i = 0; i < addon.length; i++) {
			if (addon[i] !== false) ret.push(addon[i].replace('pr-', ''));
		}

		return ret.join(',');
	}

	 // LOADING METHODS
	function loadAll (page) {
		var storeId = GetURLParameter('storeId');

		if (loadMode === '#products') {
			//$('.filter-no-coupons').prop('disabled', false);

			loadAllProducts(storeId, page);
		}
		else {
			/*$('.filter-no-coupons').each(function () {
				if ($(this).is(':checked')) {
					$(this).trigger('click');
				}

				$(this).prop('disabled', true);
			});*/

			var category_check = "";
	        var countCategory = 0;
	        var idStore = GetURLParameter('storeId');

	        $('input[name="category"]:checked').each(function () {
	            countCategory++;
	            if (countCategory === 1) {
	                category_check += this.value;

	            } else {
	                category_check += ("," + this.value);
	            }
	        });

			if(category_check.length){
	            var count = countCategory - 1;
	            countCategory = 0;
	            var category = category_check.split(",");
	            category_check = "";

	            for (var i in category) {
	                if (category[i] === "0"){
	                }else{
	                    countCategory ++;
	                    if (countCategory === 1) {
	                            category_check += category[i];
	                     } else {
	                            category_check += ("," + category[i]);
	                    }
	                }
	            }

	            loadCouponsByCategories(category_check, idStore, countCategory, page);
	        }
			else {
	            loadAllCoupons(idStore, page);
	        }
		}
	}

    // **********************
    // Click button Filter Submit Search
    // **********************
    $(document).on('click', '.btnCouponFilterSubmit', function (){
        loadAll(1);
    });

	function loadCouponsByCategories(category_check, idStore, count, page) {
		var pathname = '/booking-order/loadallcouponandcategory';

		$.ajax({
			url: pathname,
			type: 'post',
			data: {
				searchCategory: category_check,
				idStore: idStore,
				countCategory: count,
				page: page
			},
			success: function (data) {
				updateData(data);
			}
		});
	}

    // **********************
    // End Click button Filter Submit Search
    // **********************
    var idStore = GetURLParameter('storeId');
    // **********************
    // On Load Information Store
    // **********************
    function loadStoreInformation () {
		var pathname = '/booking-order/getinformationstore';

	    $.ajax({
	        url: pathname,
	        type: 'post',
	        data: {
	            idStore: idStore
	        },
	        success: function (data) {
	            $("#header").append(data);
	        }
	    });
	}
    // **********************
    // End On Load Information Store
    // **********************

    // **********************
    //  On Load Category
    // **********************
    function loadCategories () {
		var pathname = '/booking-order/getcategorybyidstore';

	    $.ajax({
	        url: pathname,
	        type: 'post',
	        data: {
	            idStore: idStore
	        },
	        success: function (data) {
	            $("#filter_menu").append(data);
	            loadAll(1);
	        }
	    });
	}
    // **********************
    //  End On Load Category
    // **********************

    // **********************
    //  On Load All Coupon
    // **********************
    function loadAllCoupons (idStore, page) {
		var pathname = '/booking-order/loadallcoupon';

	    $.ajax({
	        url: pathname,
	        type: 'post',
	        data: {
	            idStore: idStore,
				page   : page
	        },
	        success: function (data) {
	            updateData(data);
	        }
	    });
	}
    // **********************
    //  End On Load All Coupon
    // **********************

    // **********************
    // Get parameter url
    // **********************
    function GetURLParameter (sParam) {
        var sPageURL = window.location.search.substring(1);
        var sURLVariables = sPageURL.split('&');
        for (var i = 0; i < sURLVariables.length; i++)
        {
            var sParameterName = sURLVariables[i].split('=');
            if (sParameterName[0] === sParam)
            {
                return sParameterName[1];
            }
        }

		return '';
    }
    // **********************
    // End Get parameter url
    // **********************
    //
	loadStoreInformation();

	if ($('#filter_menu').length) {
		loadCategories();
	}

	function updateData(data) {
		$("#couponTableData").html(data);

		if($('.nothing-found').length) {
			showError('ご希望の条件でクーポンが見つかりませんでした。条件を変えて検索してください。');
		}
		reInitCoupons();
		reInitProducts();
		updatePagination();
	}

	function updatePagination() {
		currentPage = parseInt($('.current-page').text());
		totalPage = parseInt($('.total-page').text());
	}

	$(document).on('click', '.prev-page.enabled', function() {
		currentPage--;
		if (currentPage < 1) currentPage = 1;
		loadAll(currentPage);
	});

	$(document).on('click', '.next-page.enabled', function() {
		currentPage++;
		if (currentPage > totalPage) currentPage = totalPage;
		loadAll(currentPage);
	});

	// **********************
    // Click button sumit check coupon
    // **********************
    $(document).on('click', '.btn_submitCoupon_check', function ()
    {
		couponId = getCouponList();
		productId = getProductList();

        var pathname = '/booking-order/aftercouponoption' + '?storeId=' + GetURLParameter('storeId') + '&couponId=' + couponId + '&products=' + productId + '&addMenu=1';
        $(location).attr('href', pathname);

    });
    // **********************
    // End Click button sumit check coupon
    // **********************
    //
    // // **********************
    // Click button sumit coupon
    // **********************
    $(document).on('click', '.btn_sumitCoupon', function ()
    {
		couponId = getCouponList();
		productId = getProductList();

        if (couponId.length === 0 && productId.length === 0) {

            showError('クーポンまたは商品を選択してください。');

        } else {

	        var pathname = '/booking-order/aftercouponoption' + '?storeId=' + GetURLParameter('storeId') + '&couponId=' + couponId + '&products=' + productId + '&addMenu=1';
	        $(location).attr('href', pathname);
        }
    });
    // **********************
    // End Click button sumit coupon
    // **********************
	// **********************
    //  On Load All Coupon
    // **********************
    function loadAllProducts (storeId, page) {
		var pathname = '/booking-order/loadallproducts';

		var category_check = "";
		var countCategory = 0;
		var idStore = GetURLParameter('storeId');

		$('input[name="category"]:checked').each(function () {
			countCategory++;
			if (countCategory === 1) {
				category_check += this.value;

			} else {
				category_check += ("," + this.value);
			}
		});

		if(category_check.length){
			var count = countCategory - 1;
			countCategory = 0;
			var category = category_check.split(",");
			category_check = "";

			for (var i in category) {
				if (category[i] === "0"){
				}else{
					countCategory ++;
					if (countCategory === 1) {
							category_check += category[i];
					 } else {
							category_check += ("," + category[i]);
					}
				}
			}
		}

	    $.ajax({
	        url: pathname,
	        type: 'post',
	        data: {
	            storeId : storeId,
				page   : page,
				categories : category_check
	        },
	        success: function (data) {
	            updateData(data);
	        }
	    });
	}
    // **********************
    //  End On Load All Coupon
    // **********************
    $(document).on('click', '.booking-show-coupons', function () {
		loadMode = '#coupons';
		window.location.hash = loadMode;
		loadAll(1);
	});

	$(document).on('click', '.booking-show-products', function () {
		loadMode = '#products';
		window.location.hash = loadMode;
		loadAll(1);
	});

	function addProductToBundle (pr, pr_id, dontAddNodes) {
		var name = pr.find('.radio').text(),
			price = pr.find('.pr-price').text(),
			time = pr.find('.pr-time').text();
		if (!dontAddNodes) {
			var cate = pr.find('.pr-category').html();
			var tr ='<tr class="added-pr added-pr-' + pr_id +'"> <td>' + cate + name + '</td> <td>' + price + '</td> <td>' + time + '</td> </tr>';

			$('#confirm_menu_addon').append(tr);
		}

		//addProduct(id);
		//addOption(pr_id);
		//console.log(addons);

		return parseInt(time);
	}

	function removeProductFromBundle (pr, pr_id) {
		var id = pr_id.split('-')[2];
		//removeProduct(id);

		$('.added-pr-' + pr_id).remove();
	}

	function changeCurrentTotalTime (minutes) {
		var total_time = baseTime;

		total_time += parseInt(minutes);

		var h = Math.floor(total_time / 60);
		var m = total_time % 60;

		var time_text_h = h ? h + ' 時間 ' : '';
		var time_text_m = m ? m + ' 分 ' : '';

		$('.totaltime').html(time_text_h + ' ' + time_text_m);
	}

	// return more time
	function checkAllOptionProducts(dontAddNodes) {
		var moreTime = 0;

		$('.pr-checkbox').each(function (i, e) {
			var pr_id = $(e).val();
			if (pr_id != -1) {
				var pr = $(e).closest('.pr');

				if ($(e).is(':checked')) {
					moreTime += addProductToBundle(pr, pr_id, dontAddNodes);
					addon.push(pr_id);
					//console.log(addon);
					//addProduct(pr_id.split('-')[2]);
				}
				else {
					removeProductFromBundle(pr, pr_id);
					//addon.push(false);
					//addProduct(pr_id.split('-')[2]);
				}
			}
		});

		return moreTime;
	}

	function getAddonProductQueryString () {
		return '&products=' + getProductList() + '&options=' + getOptionList();
	}

	$(document).on('click', '.pr-checkbox', function () {
		$('#confirm_menu_addon').html('');
		addon = [];
		changeCurrentTotalTime(checkAllOptionProducts());
	});

	if ($("#confirm_menu").length) {
        var pathControler = '/booking-order/getconfirmmenu';
        $.ajax({
            url: pathControler,
            type: 'post',
            data: {
                couponId: getCouponList(),
                idStore : idStore,
				products: getProductList(true),
				options : GetURLParameter('options')
            },
            success: function (data) {
                $("#confirm_menu").append(data);

				baseTime = parseInt($('#total_time_allcoupon').val());
				//************************
				// Reload chosen options
				//************************
				var selectedProducts = GetURLParameter('options').split(',');

				$('#confirm_menu_addon').html('');
				$('.pr-checkbox').each(function () {
					var id = this.value.replace('pr-', '');
					var findPr = addon.indexOf(id);

					if (findPr !== -1) {
						$(this).prop('checked', true);
						//selectedProducts = selectedProducts.slice(findPr, 1);
					}
				});
				changeCurrentTotalTime(checkAllOptionProducts());
				//************************
				// End Reload chosen options
				//************************
            }
        });
    }
    // **********************
    // Load data Option menu
    // **********************
    if ($("#option_menu").length) {

        var pathControler = '/booking-order/optionbyidcoupon';

        $.ajax({
            url: pathControler,
            type: 'post',
            data: {
                couponId: getCouponList(),
                idStore: idStore,
				products: getProductList(true)
            },
            success: function (data) {
                $("#option_menu").append(data);
				//************************
				// Reload chosen options
				//************************
				var selectedProducts = GetURLParameter('options').split(',');

				$('#confirm_menu_addon').html('');
				$('.pr-checkbox').each(function () {
					var id = this.value.replace('pr-', '');
					var findPr = addon.indexOf(id);

					if (findPr !== -1) {
						$(this).prop('checked', true);
						//selectedProducts = selectedProducts.slice(findPr, 1);
					}
				});
				changeCurrentTotalTime(checkAllOptionProducts());
				//************************
				// End Reload chosen options
				//************************
            }
        });
    }

    // **********************
    // End Load data Option menu
    // **********************

    // **********************
    // End load confirm menu
    // **********************

    // **********************
    // Load data Time Salon Schedule
    // **********************
    if ($("#time_salon").length) {

        var pathControler = '/booking-order/timesalonschedule';
        $.ajax({
            url: pathControler,
            type: 'post',
            data: {
                couponId: getCouponList(),
                idStore: idStore
            },
            success: function (data) {
                $("#time_salon").append(data);
            }
        });
    }
    // **********************
    // End Load data Time Salon Schedule
    // **********************
	// **********************
    // Load order detail
    // **********************
    var totalPoint = 0;
	var totalMoney = 0;
	var oldValidPoint = 0;
	var conversionYen = 0;
	var conversionP = 0;
	var letGo = false;
	var needAccepted = 0;

    if ($('#load-order-detail-here').length) {

        var pathControler = '/booking-order/loadorderdetail';
        $.ajax({
            url: pathControler,
            type: 'post',
            data: {
                couponId: getCouponList(),
                storeId: idStore,
				date: GetURLParameter('date'),
				options : getOptionList(),
				productId: getProductList(true),
				staffId: GetURLParameter('staff')
            },
            success: function (data) {
                $('#load-order-detail-here').append(data.replace(/<script.+script>/, ''));

				$('#booking-need-to-pay').html($('#booking-money').html());

				totalMoney = parseInt($('#booking-money').text().replace(',', ''));
				totalPoint = parseInt($('#booking-point-total').text());

				conversionYen = parseFloat($("#point-conversion-yen").val());
				conversionP = parseFloat($("#point-conversion-p").val());

				$('#booking-points').change(function () {
					pointChange(this);
				});
            }
        });
    }
	function pointToMoney (point) {
		return parseInt(point) * conversionYen / conversionP;
	}
	function showInputError (input, msg){
		//console.log($(input).parent().find('.help-block-error')[0]);
		$(input).parent().find('.help-block-error').html(msg);
	}
	function pointChange (input) {
		var val = $(input).val();

		if (val.match(/^[\-]{0,1}\d+$/) || val === '') {

			var usingPoints = parseInt($(input).val());

			if (!usingPoints) usingPoints = 0;

			if (usingPoints < 0) {
				showInputError(input, 'ご利用ポイント は0よりてはいけません。');
				letGo = false;
				return $(input).select();
			}

			if (usingPoints > totalPoint){
				showInputError(input, '利用ポイント は現在ポイントより大きくてはいけません。');
				letGo = false;
				return $(input).select();

			}

			var finalMoney = totalMoney - pointToMoney(usingPoints);

			if (finalMoney < 0) {
				letGo = false;
				oldValidPoint = Math.floor(totalMoney * conversionP / conversionYen);
				//console.log(oldValidPoint);
				showInputError(input, usingPoints + "ポイントを利用して購入したら、支払い予定金額が0円より小さくなっていますので再度入力してください。<br>（支払い予定金額が0円なら、" + oldValidPoint + "ポイントをご利用してください。）");
				return $(input).select();
			}
			else {
				letGo = true;
				oldValidPoint = usingPoints;
				$('#booking-need-to-pay').html(formatMoney(finalMoney));
			}

			//$(input).val(oldValidPoint);
			//finalMoney = totalMoney - pointToMoney(oldValidPoint);
		}
		else {
			showInputError(input, 'ポイントの書式が正しくありません。');
			letGo = false;
			return $(input).select();

			//$(input).val(oldValidPoint);
		}

		/*if (typeof usingPoints === 'NaN') {
			console.log('hahaha');
		}*/
	}

	function formatMoney(money) {
		return money.toFixed(1).replace(/(\d)(?=(\d{3})+\.)/g, '$1,').split('.')[0];
	}
    // **********************
    // End Load order detail
    // **********************
	$('#booking-points').keypress(function(e){
	    if ( e.which == 13 ) // Enter key = keycode 13
	    {
			pointChange(this);
	        $(this).next().focus();  //Use whatever selector necessary to focus the 'next' input
	        return false;
	    }
	});

	$('#need-js-validation').submit(function (e){
		pointChange($('#booking-points').get());
		if(!letGo && needAccepted < 2) {
			e.preventDefault();
			return false;
		}
	});

	$(document).on('click', '.btn_to_time_schedule', function ()
    {
        var check_coupon = 0;
        var couponId = GetURLParameter('couponId');
		addon = [];
		var time_execute = baseTime + checkAllOptionProducts(true);
		var idStore = GetURLParameter('storeId');

		var h = parseInt(time_execute / 60);
		var m = time_execute % 60;
        var pathname = '/booking-order/salonschedule' + '?storeId=' + idStore + '&couponId=' + couponId+ '&addMenu=2&executeTime=' + h + ':' + m + ':00' + getAddonProductQueryString();
        $(location).attr('href', pathname);
    });
});
