$(function () {    
    $(".input-number").keyup(function() {
        $(this).each(function(){
            if (/\D/g.test($(this).val())) {
              $(this).val($(this).val().replace(/\D/g, ''));
        }
        })
    });
    $(".input-money").keyup(function() {
        $(this).each(function(){
            if (/\D/g.test($(this).val())) {
              $(this).val($(this).val().replace(/\D/g, ''));
        }
        })
    });
    $(".input-money").on('change', function () {
        var $val = $(this).val();
        $val = $val.replace(/\,/g, '')
        $(this).val('');
        $val = $val.replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        $(this).val($val);
    });
    
    $(document).on('submit', function () {
        $(".input-money").each(function() {
            $(this).each(function(){
                if (/\D/g.test($(this).val())) {
                  $(this).val($(this).val().replace(/\D/g, ''));
            }
            })
        });
    });
});
