<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel common\models\BookingSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('frontend', 'Staff Booking Orders');
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="row">
    <div class="col-lg-12 col-md-12" id="header">
    </div>
    <div class="col-lg-12 col-md-12">
        <div class="col-md-8">
            <ol class="rsvStepList cFix">
                <li class="rslStep1 passive"><span>STEP1</span>クーポン・メニューを選ぶ</li>
                <li class="rslStep2 active"><span>STEP2</span>日時を指定する</li>
                <li class="rslStep3 "><span>STEP3</span>お客様情報入力</li>
                <li class="rslStep4 "><span>STEP4</span>予約内容の確認</li>
                <li class="rslStepEnd ">予約完了</li>
            </ol>
        </div>
        <div class="col-md-8">
            <table cellspacing="0" class="wFull bdCell pCell5 mT20">
                <tbody>
                    <tr>
                        <th class="w130 fgRbrown bgLGray2 taC " style="width: 20em;">施術時間合計（目安）</th>
                        <td id="timeallCoupon">
                            <span class="fgRbrown pT5 b" id="time_salon">
                              
                            </span>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="col-md-8" style="padding-left: 2.5em;">
        <div class="common-box">
            <div class="box-header with-border common-box-h4 col-md-6">
                <h4 class="text-title"><b><?= Yii::t('frontend', 'Filter menu') ?></b></h4>
            </div>
        </div>
    </div>
    <div class="col-md-8" style="padding-left: 2.5em;">
        <div class="mT10 pr">
            <ul class="couponTypeFilter cFix">
                <li>                    
                    <?= Html::submitButton(Yii::t('backend', 'お店の空き状況'), ['class' => 'btnCouponTypeFilter isCr btn_timedate_store']) ?>
                </li>
                <li>
                    <?= Html::submitButton(Yii::t('backend', 'スタッフ別空き状況'), ['class' => 'btnCouponTypeFilter jscCouponType btn_styleList']) ?>
                </li>
            </ul>
        </div>
    </div>
    <div class="col-md-7" style="padding-left: 2.5em; margin-top: 2em">
        <div id="jsRsvCdTbl" class="ReserveConditionTable underTabContents">
            <div class="coverTable">
                <div class="whiteTable2">
                    <table id="calendarM" class="innerTable taC nowrap" cellpadding="0" cellspacing="0">
                        <tbody>
                            <tr id="headCal"></tr>
                            <tr id="dayRow"></tr>
                            <tr id="valRow">
                                <th class="innerCell">
                                    <table cellpadding="0" cellspacing="0" class="moreInnerTable vaT">
                                        <tbody>
                                            <?php $h = "9"; $s1 = "00"; $s2 = "30"; ?>
                                            <?php for ($i = 0; $i < 22; $i++) :?>
                                                <?php if ( $i%2 == 0 ) : ?>
                                                    <?php $h += 1; ?>
                                                    <tr><th class="timeCell"><p class="hourR"><?= $h . ':' . $s1 ?></p></th></tr>
                                                    <tr><th class="separate"></th></tr>
                                                <?php else : ?>
                                                    <tr><th class="timeCell"><p class="hourR"><?= $h . ':' . $s2 ?></p></th></tr>
                                                    <tr><th class="separate"></th></tr>
                                                <?php endif; ?>
                                            <?php endfor; ?>
                                        </tbody>
                                    </table>
                                </th>
                                
                                <?php foreach ($model as $day) :?>
                                <?php //var_dump($day['setting']);exit; ?>
                                    <th class="innerCell">
                                        <table cellpadding="0" cellspacing="0" class="moreInnerTable" data-id="<?= $day['date'] ?>">
                                            <tbody>
                                                <?php $h = "9"; $s1 = "00"; $s2 = "30"; ?>
                                                <?php for ($i = 0; $i < 22; $i++) :?>
                                                    <?php if ( $i%2 == 0 ) : ?>
                                                        <?php $h += 1; ?>
                                                        <tr data-id="<?= $h . '' . $s1 ?>">
                                                            <?php $key = $h.':'.$s1 . ':00'; ?>
                                                            <?php if ( isset($day[$key]) && $day[$key] == 1  ) :?>
                                                                <td class="openColor" data-id="<?= $day['date'] ?>"><p class="scheduleR"><a href="https://beauty.hotpepper.jp/CSP/bt/reserve/afterSchedule?storeId=H000193247&amp;rsvRequestDate1=20160913&amp;rsvRequestTime1=1300" title="◎" class="bS db reserveImmediately offL vaT">◎</a></p></td>
                                                                <tr><th class="separate"></th></tr>
                                                            <?php elseif ( isset($day[$key]) && $day[$key] == 2 ) :?>
                                                                <td class="closeColor" data-id="<?= $day['date'] ?>"><p class="scheduleR"><span class="bS db reserveTel offL">TEL</span></p></td>
                                                                <tr><th class="separate"></th></tr>
                                                            <?php else :?>
                                                                <td class="closeColor" data-id="<?= $day['date'] ?>"><p class="scheduleR"><span class="bS db reserveImpossible offL">×</span></p></td>
                                                                <tr><th class="separate"></th></tr>
                                                            <?php endif; ?> 
                                                        </tr>
                                                    <?php else : ?>
                                                        <tr data-id="<?= $h . '' . $s2 ?>">
                                                            <?php $key = $h.':'.$s2 . ':00'; ?>
                                                            <?php if ( isset($day[$key]) && $day[$key] == 1 ) :?>
                                                                <td class="openColor" data-id="<?= $day['date'] ?>"><p class="scheduleR"><a href="https://beauty.hotpepper.jp/CSP/bt/reserve/afterSchedule?storeId=H000193247&amp;rsvRequestDate1=20160913&amp;rsvRequestTime1=1300" title="◎" class="bS db reserveImmediately offL vaT">◎</a></p></td>
                                                                <tr><th class="separate"></th></tr>
                                                            <?php elseif ( isset($day[$key]) && $day[$key] == 2 ) :?>
                                                                <td class="closeColor" data-id="<?= $day['date'] ?>"><p class="scheduleR"><span class="bS db reserveTel offL">TEL</span></p></td>
                                                                <tr><th class="separate"></th></tr>
                                                            <?php else :?>
                                                                <td class="closeColor" data-id="<?= $day['date'] ?>"><p class="scheduleR"><span class="bS db reserveImpossible offL">×</span></p></td>
                                                                <tr><th class="separate"></th></tr>
                                                            <?php endif; ?>
                                                        </tr>
                                                    <?php endif; ?>
                                                <?php endfor; ?>
                                            </tbody>
                                        </table>
                                <?php endforeach; ?>
                                
                                <td valign="top">
                                    <table cellpadding="0" cellspacing="0" class="moreInnerTable vaT">
                                        <tbody>
                                            <?php $h = "9"; $s1 = "00"; $s2 = "30"; ?>
                                            <?php for ($i = 0; $i < 22; $i++) :?>
                                                <?php if ( $i%2 == 0 ) : ?>
                                                    <?php $h += 1; ?>
                                                    <tr><th class="timeCell"><p class="hourR"><?= $h . ':' . $s1 ?></p></th></tr>
                                                    <tr><th class="separate"></th></tr>
                                                <?php else : ?>
                                                    <tr><th class="timeCell"><p class="hourR"><?= $h . ':' . $s2 ?></p></th></tr>
                                                    <tr><th class="separate"></th></tr>
                                                <?php endif; ?>
                                            <?php endfor; ?>
                                        </tbody>
                                    </table>
                                </td>

                            </tr>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
var selectedDay = '<?= $setting['date'] ?>';   
var week = '<?= $setting['week'] ?>';
var time_execute = '<?= $setting['time_execute'] ?>';
var staff_id = '<?= $setting['staff_id'] ?>';
</script>

<?php $this->registerCSSFile('@web/css/salon_header.css',['position' => \yii\web\View::POS_HEAD]);?>
<?php $this->registerCSSFile('@web/css/choose_menu.css',['position' => \yii\web\View::POS_HEAD]);?>
<?php $this->registerJsFile('@web/js/staffschedule.js',['depends' => [\yii\web\JqueryAsset::className()]]); ?>