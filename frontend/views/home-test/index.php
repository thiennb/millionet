<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel common\models\BookingSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

//frontend\assets\BookingOrderAsset::register($this);

$this->title = "理美容 POS システム";
?>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" href="/resources/demos/style.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>
    $(function () {
        $("#accordion").accordion();
    });
</script>

<div class="container">
    <div class="alert alert-danger danger"><b>店舗を選択してください。</b></div>
    <div id="accordion">

        <?php
        for ($i = 0; $i < count($list_company) ; $i++) {
            
                $id_array_store = explode(",", $list_company[$i]['store_id']);
                $store_name = explode(",", $list_company[$i]['store_name']);
//                echo '<h3>' .$list_company[$i]['name']. '</h3><div><ul>';
//                        foreach ($id_array_store as $key => $value) {
//                            echo '<li>'.$value.'</li>';
//                        }
//                    '</ul>
//                 </div>';
                    
                echo '<h3 style="
                    background: #86235d;
                    height: 2.3em;
                    font-size: large;
                    border: none;
                    color: white;">'.$list_company[$i]['name'].'</h3>'.
                     '<div>
                      <ul style="display: block;
                                list-style-type: disc;
                                margin-top: 1em;
                                margin-bottom: 1 em;
                                margin-left: 0;
                                margin-right: 0;
                                padding-left: 40px;">';
                        $j = 0;
                        $actual_link = "http://$_SERVER[HTTP_HOST]";
                        foreach ($id_array_store as $key => $value) {
                            
                            echo '<li><a href="'.$actual_link.'/booking-order?storeId='.$value.'" style="color: #1d5df3;font-size: 15px;
                            " target="_blank">'.$store_name[$j].'</a></li>';
                            $j++;
                        }
                echo '</ul>
                    </div>';
        }
        ?>
    </div>
</div>
