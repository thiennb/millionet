<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel common\models\BookingSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

//frontend\assets\BookingOrderAsset::register($this);

$this->title = "理美容 POS システム";
?>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" href="/resources/demos/style.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<div class="" style="min-height: 511px;">
    <div class="content">
        <div class="row">
            <div class="col-lg-12 col-md-12">
                <div class="box box-info box-solid" style="height: 100%;border: 1px solid #ffffff;background: #f6f6f6;">
                    <div class="box-header with-border" style="background-color: #af1f70;">
                        <h4 class="text-title"><b>Auto Insert</b></h4>
                    </div>
                    <div class="box-body content">
                        <div class="col-md-12">
                            <?php
                            $form = ActiveForm::begin([
                                        'action' => ['create'],
                                        'method' => 'post',
                                        'enableClientValidation' => false,
                            ]);
                            ?>
                            <div class="col-md-10" style="margin-top: 1em;">
                                <div class="col-md-12">
                                    <div class="col-md-2 label-margin required-star">
                                        <label class="control-label "
                                               for="mastercustomer-firstname"> <?= Yii::t("backend", "Company Code"); ?></label>
                                    </div>
                                    <div class="col-md-3 input_store_ct">
                                        <?= $form->field($model, 'company_code')->label(false)->textInput(['maxlength' => true, 'required' => 'required', 'maxlength' => 4, 'placeholder' => 'Company Code']); ?>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="col-md-2 label-margin required-star">
                                        <label class="control-label"for="mastercustomer-firstname"> <?= Yii::t("backend", "Company Name"); ?></label>
                                    </div>
                                    <div class="col-md-3 input_store_ct">
                                        <?= $form->field($model, 'name_company')->label(false)->textInput(['maxlength' => true, 'required' => 'required', 'maxlength' => 100, 'placeholder' => 'Company Name']) ?>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="col-md-2 label-margin required-star">
                                        <label class="control-label"for="mastercustomer-firstname"> <?= Yii::t("backend", "Name Store"); ?></label>
                                    </div>
                                    <div class="col-md-3 input_store_ct">
                                        <?= $form->field($model, 'name_store')->label(false)->textInput(['maxlength' => true, 'required' => 'required', 'maxlength' => 100, 'placeholder' => 'Name Store']) ?>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="col-md-2 label-margin required-star">
                                        <label class="control-label"for="mastercustomer-firstname"> <?= Yii::t("backend", "Name"); ?></label>
                                    </div>
                                    <div class="col-md-3 input_store_ct">
                                        <?= $form->field($model, 'name_staff')->label(false)->textInput(['maxlength' => true, 'required' => 'required', 'maxlength' => 80, 'placeholder' => 'Name Staff']) ?>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="col-md-2 label-margin required-star">
                                        <label class="control-label"for="mastercustomer-firstname"> <?= Yii::t("backend", "Short name"); ?></label>
                                    </div>
                                    <div class="col-md-3 input_store_ct">
                                        <?= $form->field($model, 'short_name_staff')->label(false)->textInput(['maxlength' => true, 'required' => 'required', 'maxlength' => 5, 'placeholder' => 'Short Name Staff']) ?>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="col-md-2 label-margin required-star">
                                        <label class="control-label"for="mastercustomer-firstname"> <?= Yii::t("backend", "Password"); ?></label>
                                    </div>
                                    <div class="col-md-3 input_store_ct">
                                        <?= $form->field($model, 'pass_word')->label(false)->textInput(['maxlength' => true, 'required' => 'required', 'type' => 'password', 'maxlength' => 255, 'placeholder' => 'Pass']) ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group" style="margin-top: 1em;"> 
                                    <?= Html::submitButton(Yii::t('backend', '登録'), ['class' => 'btn common-button-submit', 'style' => '
                                        background: #f75617;
                                        color: #ffffff;
                                        height: 2.8em;
                                        font-weight: bold;
                                        margin-right: 1em;']) ?>
                                    <?php ActiveForm::end(); ?>
                                </div>
                            </div>   
                        </div>
                        <table class="table table-striped table-bordered text-center">
                            <colgroup>
                                <col class="with-table-no">
                                <col>
                                <col>
                                <col>
                                <col>
                                <col class="with-table-button">
                            </colgroup>
                            <thead>
                                <tr>
                                    <th>管理ID</th>
                                    <th>企業コード</th>
                                    <th>企業名</th>
                                    <th>店舗名</th>
                                    
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                if (!empty($result)) {
                                    foreach ($result as $key => $value) {
                                        echo '<tr><td>'.$value['management_id'].'</td><td>'.$value['company_code'].'</td><td>'.$value['company_name'].'</td><td>'.$value['store_name'].'</td><tr>';
                                    }
                                }
                                ?>
                            </tbody>
                        </table>


                    </div>
                </div>
            </div>
        </div>
    </div>
</div>