<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\bootstrap\ActiveForm;
use yii\web\Session;

/* @var $this yii\web\View */
/* @var $searchModel common\models\MasterMemberSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->registerCssFile('@web/css/member.css', ['depends' => [yii\bootstrap\BootstrapAsset::className()]]);
$this->title = Yii::t('frontend', 'Completion of registration');
?>
<div class="row">
    <div class="col-lg-12 col-md-12">
        <div class="box box-info box-solid">
            <div class="box-body content">
                <div class="col-md-12">
                    <div class="common-box col-md-12">
                        <div class="col-md-12">
                            <ol class="rsvStepList cFix">
                                <li class="rslStep1 passive"><span></span>宛先登録</li>
                                <li class="rslStep2 passive"><span></span>認証キー入力</li>
                                <li class="rslStep3 passive"><span></span>会員情報入力</li>
                                <li class="rslStep4 passive"><span></span>会員情報確認</li>
                                <li class="rslStepEnd active">登録完了</li>
                            </ol>
                        </div>
                        <div class="col-md-12"
                             <div class="row">
                                <div class="box-header with-border common-box-h4 col-md-8">
                                    <h4 class="text-title"><b><?= Yii::t('frontend', 'Member registration completion') ?></b></h4>
                                </div>
                            </div>
                            <div class="row row-inline"><div class="form-group"></div></div>
                            <div class="row">
                                <p><?= Html::encode(Yii::t('frontend','Your membership registration has been completed.')) ?></p>
                                <div class="row row-inline"><div class="form-group"></div></div>
                                <div class="col-xs-12 text-center">
                                    <?php $form = ActiveForm::begin(); ?>
                                    <?= Html::submitButton(Yii::t('frontend', 'next'), ['class' => 'btn cm-btn-submit']) ?>
                                    <?php ActiveForm::end(); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
