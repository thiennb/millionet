<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\bootstrap\ActiveForm;
use dmstr\widgets\Alert;

/* @var $this yii\web\View */
/* @var $searchModel common\models\MasterMemberSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */


$this->registerCssFile('@web/css/member.css', ['depends' => [yii\bootstrap\BootstrapAsset::className()]]);
$this->registerJsFile('@web/js/member.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->title = Yii::t('backend', 'Register Member');
?>
<div class="row">
    <div class="col-lg-12 col-md-12">
        <div class="box box-info box-solid">
            <div class="box-body content">
                <div class="col-md-12">
                    <div class="common-box col-md-10 col-md-offset-1">
                        <div class="row">
                            <div class="col-md-10 col-md-offset-1">
                                <ol class="rsvStepList cFix">
                                    <li class="rslStep1 passive"><span></span>宛先登録</li>
                                    <li class="rslStep2 passive"><span></span>認証キー入力</li>
                                    <li class="rslStep3 active"><span></span>会員情報入力</li>
                                    <li class="rslStep4 "><span></span>会員情報確認</li>
                                    <li class="rslStepEnd ">登録完了</li>
                                </ol>
                            </div>
                        </div>
                        <div class="form-group">
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="col-md-12">
                                    <div class="col-md-10 col-md-offset-1">
                                        <div class="row">
                                            <div class="box-header with-border common-box-h4 col-md-6">
                                                <h4 class="text-title"><b><?= Yii::t('backend', 'Input of member information') ?></b></h4>
                                            </div>
                                        </div>
                                        <div class="form-group"><?= Alert::widget() ?></div>
                                        <div class="box-body content">
                                            <div class="col-md-12">
                                                <?= $this->render('_form', [
                                                    'model' => $model,
                                                ]) ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
