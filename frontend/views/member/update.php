<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\MasterMember */

$this->registerCSSFile('@web/css/history.css',['position' => \yii\web\View::POS_HEAD]);
$this->registerCssFile('@web/css/member.css', ['depends' => [yii\bootstrap\BootstrapAsset::className()]]);
$this->registerJsFile('@web/js/member.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->title = Yii::t('backend', 'Member information change');
?>
<div class="shared-common-container">
    <div class="row box box-info box-solid">
        <div class="box-body content">
            <div class="shared-section-title">
                <?= Yii::t('backend', 'Change of member information 1') ?>
            </div>
            <div class="clearfix"> </div> <div class="row shared-toppad-lg"> </div>
            <div class="col-xs-12">
                <div class="row row-inline">
                    <div class="col-xs-3 bg_header div-horizontal">
                        <?= Yii::t('backend', 'Phone'); ?>
                    </div>
                    <div class="col-xs-5 clear-form-group div-horizontal">
                        <?php if(!empty($model->mobile)){ ?>
                            <?php if(strlen($model->mobile) == 10){ ?>
                                <?= substr($model->mobile, 0,2) ?>&nbsp;-
                                <?= substr($model->mobile, 2,4) ?>&nbsp;-
                                <?= substr($model->mobile, 6,4) ?>
                            <?php } else{ ?>
                                <?= substr($model->mobile, 0,3) ?>&nbsp;-
                                <?= substr($model->mobile, 3,4) ?>&nbsp;-
                                <?= substr($model->mobile, 7,4) ?>
                            <?php } ?>
                        <?php } else{
                            echo Yii::t('backend',"No Setting");
                        } ?>
                    </div>
                    <div class="col-xs-4">

                        <?= Html::a('<i class="fa fa-caret-right bt-caret-right" aria-hidden="true"></i>'.Yii::t('backend', 'Update member phone number'),
                                ['update-phone', 'id' => $model->id], ['class' => 'btn btn-default common-button-1']) ?>
                    </div>
                </div>
                <div class="row row-inline">
                    <div class="col-xs-3 bg_header div-horizontal">
                        <?= Yii::t('backend', 'Password'); ?>
                    </div>
                    <div class="col-xs-5 clear-form-group">
                        <span><?= '**********' ?></span><br>
                        <span  class="text-pink"><?= Yii::t('backend', 'not displayed for security protection') ?></span>
                    </div>
                    <div class="col-xs-4 div-horizontal">
                        <?= Html::a('<i class="fa fa-caret-right bt-caret-right" aria-hidden="true"></i>'.Yii::t('backend', 'Change password'),
                                ['update-password', 'id' => $model->id], ['class' => 'btn btn-default common-button-1']) ?>
                    </div>
                </div>
                <div class="row row-inline">
                    <div class="col-xs-3 bg_header div-horizontal">
                        <?= Yii::t('backend', 'Email'); ?>
                    </div>
                    <div class="col-xs-5 clear-form-group div-horizontal">
                        <?= !empty($model->email)?$model->email:Yii::t('backend',"No Setting") ?>
                    </div>
                    <div class="col-xs-4 div-horizontal">
                        <?= Html::a('<i class="fa fa-caret-right bt-caret-right" aria-hidden="true"></i>'.Yii::t('backend', 'E-mail address change'),
                                ['update-email', 'id' => $model->id], ['class' => 'btn btn-default common-button-1']) ?>
                    </div>
                </div>
            </div>
            <div class="clearfix"> </div> <div class="row shared-toppad-lg"> </div>
            <div class="shared-section-title">
                <?= Yii::t('backend', 'Change of member information 2') ?>
            </div>
            <div class="clearfix"> </div> <div class="row shared-toppad-lg"> </div>
            <div>
                <div class="col-xs-12">
                    <?=
                    $this->render('_form_update', [
                        'model' => $model,
                        'prefecture'   =>  $prefecture,
                    ])
                    ?>
                </div>
            </div>
            <div class="clearfix"> </div>
        </div>
        <div class="clearfix"> </div>
    </div>
    <div class="clearfix"> </div>
</div>
<div class="clearfix"> </div>
