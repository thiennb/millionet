<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\Session;
use yii\jui\DatePicker;
use common\components\Constants;
/* @var $this yii\web\View */
/* @var $model common\models\MasterMember */
/* @var $form yii\widgets\ActiveForm */
$session = new Session;
?>

<div class="master-member-form">

    <?php $form = ActiveForm::begin(['enableClientValidation' => FALSE]); ?>
    <div class="row row-inline">
        <table class="col-md-12 update-member">
            <tr>
                <th class="bg_header" colspan="3" style="width: 25%;">
                    <div class="col-md-12 div-horizontal">
                        <?= Yii::t('backend','Name'); ?>
                        <span class="required-text">必須</span>
                    </div>
                </th>
                <td colspan="9">
                    <div class="col-md-12 clear-form-group text-left">
                        <?= $form->field($model, 'first_name',[
                            'template' => "<div class='col-md-2 clear-label-requir'>{label}</div>"
                                        . '<div class="col-md-4">{input}{error}</div>'
                        ])->textInput(['maxlength' => true]) ?>
                        <?= $form->field($model, 'last_name',[
                            'template' => "<div class='col-md-2 clear-label-requir'>{label}</div>"
                                        . '<div class="col-md-4">{input}{error}</div>'
                        ])->textInput(['maxlength' => true]) ?>
                        <div class="div-margin"></div>
                        <?= $form->field($model, 'first_name_kana',[
                            'template' => "<div class='col-md-2 clear-label-requir'>{label}</div>"
                                        . '<div class="col-md-4">{input}{error}</div>'
                        ])->textInput(['maxlength' => true]) ?>
                        <?= $form->field($model, 'last_name_kana',[
                            'template' => "<div class='col-md-2 clear-label-requir'>{label}</div>"
                                        . '<div class="col-md-4">{input}{error}'
                                        . '</div>'
                        ])->textInput(['maxlength' => true]) ?>
                    </div>
                </td>
            </tr>
            <tr>
                <th class="bg_header" colspan="3">
                    <div class="col-md-12 div-horizontal">
                        <?= Yii::t('backend','Birthday'); ?>
                    </div>
                </th>
                <td colspan="9">
                    <div class="col-md-12 clear-form-group">
                        <?= $form->field($model, 'birth_date',[
                            'template' => '<div class="col-md-5">{input}{error}</div><div class="col-md-7"></div>'
                        ])->widget(DatePicker::classname(), [
                            'language' => 'ja',
                            'dateFormat' => 'yyyy/MM/dd',
                            'clientOptions' => [
                                "changeMonth" => true,
                                "changeYear" => true,
                                "yearRange"=> "1900:+0"
                            ]
                        ]) ?>
                    </div>
                </td>
            </tr>
            <tr>
                <th class="bg_header" colspan="3">
                    <div class="col-md-12 div-horizontal">
                        <?= Yii::t('backend','Sex'); ?>
                    </div>
                </th>
                <td colspan="9">
                    <div class="col-md-12 clear-form-group text-left">
                        <?php echo $form->field($model, 'sex',[
                            'template' => '<div class="col-md-9">{input}{error}</div>'
                        ])->radioList(Constants::LIST_SEX,['separator' => '&nbsp;&nbsp;&nbsp;&nbsp;', 'tabindex' => 3]); ?>
                    </div>
                </td>
            </tr>
            <tr>
                <th class="bg_header" colspan="1" rowspan="3">
                    <div class="col-md-12 div-horizontal">
                        <?= Yii::t('backend','Address'); ?>
                    </div>
                </th>
                <td colspan="2" class="bg_header">
                    <div class="col-md-12 clear-form-group">
                        <?= Yii::t('backend','PostCode'); ?>
                    </div>
                </td>
                <td colspan="9">
                    <div class="col-md-12 clear-form-group">
                        <div class="col-md-12 clear-form-group-1" style="padding: 0">
                            <div class="col-md-4 text-left">
                                <div class="col-md-12" style="padding: 0">
                                    <div class="col-md-5 clear-help-block clear-form-group" style="padding: 0">
                                        <?= $form->field($model, 'post_code_1',[
                                            'template' => '{input}{error}'
                                        ])->textInput(['maxlength' => true])->widget(\yii\widgets\MaskedInput::className(), [
                                            'mask' => '999',
                                            'clientOptions' => [
                                                'removeMaskOnSubmit' => true
                                            ]
                                        ]) ?>
                                    </div>
                                    <div class="col-md-2 div-horizontal-all padding-10"><i class="fa fa-minus padding-10" aria-hidden="true"></i></div>
                                    <div class="col-md-5 clear-help-block clear-form-group" style="padding: 0">
                                        <?= $form->field($model, 'post_code_2',[
                                            'template' => '{input}{error}'
                                        ])->textInput(['maxlength' => true])->widget(\yii\widgets\MaskedInput::className(), [
                                            'mask' => '9999',
                                            'clientOptions' => [
                                                'removeMaskOnSubmit' => true
                                            ]
                                        ]) ?>
                                    </div>
                                </div>
                                <div class="col-md-12" style="padding: 0">
                                    <?= Yii::t('frontend','Half-size number') ?>
                                </div>
                            </div>
                            <div class="col-md-4 text-left">
                                <?= Html::a('<i class="fa fa-caret-right bt-caret-right" aria-hidden="true"></i>'.Yii::t('backend', 'Automatic input an address'), ['#'],
                                        ['class' => 'btn btn-default common-button-1','id'=>'search-code']) ?>
                            </div>
                            <div class="col-md-4 text-left">
                            </div>
                        </div>
                    </div>
                </td>
            </tr>
            <tr>
                <td colspan="2" class="bg_header">
                    <div class="col-md-12 clear-form-group">
                        <?= Yii::t('backend','Prefectures'); ?>
                    </div>
                </td>
                <td colspan="9">
                    <div class="col-md-12 clear-form-group">
                        <?= $form->field($model, 'prefecture',[
                        'template' => '<div class="col-md-5">{input}{error}'
                                    . '</div><div clas="col-md-7"></div>'
                        ])->dropDownList($prefecture, ['prompt' => Yii::t('backend', 'Please Select')]) ?>
                    </div>
                </td>
            </tr>
            <tr>
                <td colspan="2" class="bg_header">
                    <div class="col-md-12 clear-form-group">
                        <?= Yii::t('backend','The city where your business is located below'); ?>
                    </div>
                </td>
                <td colspan="9">
                    <div class="col-md-12 clear-form-group text-left">
                        <?= $form->field($model, 'address',[
                        'template' => '<div class="col-md-12">{input}{error}'
                                    . '</div>'
                        ])->textInput(['maxlength' => true]) ?>
                        <div  class="col-md-12 text-pink"><?= Yii::t('frontend','※ If there is a building name (apartment name apartment name), please be sure to fill in') ?></div>
                    </div>
                </td>
            </tr>
        </table>
    </div>
    <div class="form-group"></div>
    <div class="row">
        <div class="form-group text-center">
            <?= Html::submitButton(Yii::t('backend', 'Check'),
                    ['class' => 'btn cm-btn-submit']) ?>
        </div>
    </div>
    <?php ActiveForm::end(); ?>

</div>
<script>
    $("#search-code").click(function (e) {
        var code_ele_1 = '#mastermember-post_code_1',
            code_ele_2 = '#mastermember-post_code_2',
            address_ele = '#mastermember-prefecture';
        var code = $(code_ele_1).val()+''+$(code_ele_2).val();
        code = code.replace(/[^0-9\.]+/g, "");

    //       $(address_ele).val(code);
        if (code != "") {
            var pathControler = SITE_ROOT + 'member/getpostcode';
            $.ajax({
                url: pathControler,
                type: 'post',
                data: {
                    postCode: code
                },
                success: function (data) {
                    $(code_ele_1).closest('.form-group').removeClass('has-error').find('.help-block').text('');
                    $(code_ele_2).closest('.form-group').removeClass('has-error').find('.help-block').text('');
                    if (data != '') {
                        $(address_ele).val(data);
                    } else {
                        $(code_ele_1).closest('.form-group').addClass('has-error').find('.help-block').text('入力した郵便番号に該当する住所は存在しません。');
                        $(code_ele_2).closest('.form-group').addClass('has-error').find('.help-block');
                    }
                }
            });
        } else {
            $(code_ele_1).closest('.form-group').addClass('has-error').find('.help-block').text('郵便番号を入力してください。');
            $(code_ele_2).closest('.form-group').addClass('has-error').find('.help-block');
        }
        return false;
    });
</script>
