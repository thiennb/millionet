<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use dmstr\widgets\Alert;

/* @var $this yii\web\View */
/* @var $searchModel common\models\MasterMemberSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->registerCssFile('@web/css/member.css', ['depends' => [yii\bootstrap\BootstrapAsset::className()]]);
$this->title = Yii::t('backend', 'Authentication key input');
?>
<div class="row">
    <div class="col-lg-12 col-md-12">
        <div class="box box-info box-solid">
            <div class="box-body content">
                <div class="common-box col-md-12">
                    <div class="row">
                        <div class="box-header with-border common-box-h4 col-md-4">
                            <h4 class="text-title"><b><?= $this->title ?></b></h4>
                        </div>
                    </div>
                    <div class="form-group">
                    </div>
                    <div class="row">
                        <div class="col-md-4 col-md-offset-4">
                            <div class="box box-info box-solid">
                                <div class="box-header-1 box-header with-border text-center">
                                    <h4 class="text-title"><b><?= $this->title ?></b></h4>
                                </div>
                                <div class="box-body content">
                                    <div class="col-md-12">
                                        <div class="col-md-10 col-md-offset-1">
                                            <div class="row">
                                                <div class="box-header with-border common-box-h4-1 col-md-12">
                                                    <h4 class="text-title"><b><?= Yii::t('frontend', 'Please enter the authentication key') ?></b></h4>
                                                </div>
                                            </div>
                                            <div class="form-group"><?= Alert::widget() ?></div>
                                            <div class="row">
                                                <?php $form = ActiveForm::begin(['enableClientValidation' => FALSE]); ?>
                                                    <div class="row clear-form-group">
                                                        <?= $form->field($model, 'key_confirm',[
                                                            'template' => '<div class="col-md-12">{input}{error}'
                                                                            . '<p class="help-block help-block-error"></p>'
                                                                        . '</div>'
                                                        ])->textInput(['maxlength' => true]) ?>
                                                    </div>
                                                    <p><?= Yii::t('frontend', 'Please enter the authentication key that was delivered to the phone number addressed to your registered. The authentication key is valid for 30 minutes.') ?></p>
                                                    <div class="row text-center">
                                                        <?php //Html::a(Yii::t('backend', 'Return'), ['step1'], ['class' => 'btn btn-default common-button-default']) ?>
                                                        <?= Html::submitButton(Yii::t('backend', 'Input key confirm'), ['class' => 'btn cm-btn-submit']) ?>
                                                    </div>

                                                <?php ActiveForm::end(); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12 text-center">
                     <?= Html::a(Yii::t('frontend', 'Send phone key confirm again'), ['update-phone','id'=>$model->id]) ?>
                </div>
                <div class="form-group"></div>
                <div class="form-group"></div>
            </div>
        </div>
    </div>
</div>
