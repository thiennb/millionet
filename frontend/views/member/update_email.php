<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use common\models\MasterMember;
use dmstr\widgets\Alert;

/* @var $this yii\web\View */
/* @var $searchModel common\models\MasterMemberSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('frontend', 'E-mail address change');
?>
<div class="shared-common-container">
    <div class="shared-section-title">
        <?= $this->title ?>
    </div>
    <div class="col-xs-12">
        <div class="row shared-toppad-lg"></div>
        <div class="row text-center">
            <?= Yii::t('backend','If you want change email, please input new email below') ?>
        </div>
        <div class="form-group"></div>
        <?php $form = ActiveForm::begin(['enableClientValidation' => FALSE]); ?>
        <div class="row row-inline">
            <div class="col-md-4 bg_header div-horizontal">
                <?= Yii::t('backend', 'Email before change'); ?>
            </div>
            <div class="col-md-8 clear-form-group">
                <div class="col-md-12">
                    <div class="col-md-12">
                        <?=
                        !empty($email)?Html::encode($email):Yii::t('backend', 'No Setting')
                        ?>
                    </div>
                </div>
            </div>
        </div>
        <?php $model->email = ($email == $model->email)?'':$model->email; ?>
        <div class="row row-inline">
            <div class="col-md-4 bg_header div-horizontal">
                <?= Yii::t('backend', 'New Email'); ?>
            </div>
            <div class="col-md-8 clear-form-group">
                <div class="col-md-12">
                    <?=
                    $form->field($model, 'email', [
                        'template' => '<div class="col-md-12">{input}{error}</div>'
                    ])->textInput(['maxlength' => true])
                    ?>
                </div>
                <div class="col-md-12">
                    <?=
                    $form->field($model, 'email_re', [
                        'template' => '<div class="col-md-12">{input}{error}</div>'
                    ])->textInput(['maxlength' => true])
                    ?>
                </div>
                <div class="col-md-12">
                    <div class="col-md-12">
                        <?= Yii::t('backend', 'Please enter again to confirm'); ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-group"></div>
        <div class="row">
            <div class="col-md-4 text-left clear-padding">
                <?= Html::a('<i class="fa fa-caret-left" aria-hidden="true" style="color:#FF0066;">&nbsp</i>'.Yii::t('backend', 'Return'), ['update','id'=>$model->id], ['class' => 'btn common-back']) ?>
            </div>
            <div class="col-md-8 text-left" style="padding-left: 0">
                <?= Html::submitButton(Yii::t('backend', 'Change'), ['class' => 'btn cm-btn-submit']) ?>
            </div>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>
