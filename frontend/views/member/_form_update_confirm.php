<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\Session;
use yii\jui\DatePicker;
use common\components\Constants;
/* @var $this yii\web\View */
/* @var $model common\models\MasterMember */
/* @var $form yii\widgets\ActiveForm */;
?>

<div class="master-member-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="col-md-12 row-inline">
        <div class="col-md-4 div-border bg_header div-horizontal">
            <?= Yii::t('backend', 'Name'); ?>
        </div>
        <div class="col-md-8 ">
            <div class="col-md-12 row-inline div-padding clear-padding">
                <div class="col-md-5">
                    <?php  $first_name = !empty($model->first_name)?
                            $model->first_name
                            : Yii::t('backend','No Setting');
                    ?>
                    <?= Html::encode($first_name) ?>
                </div>
                <div class="col-md-7">
                    <?php  $last_name = !empty($model->last_name)?
                            $model->last_name
                            : Yii::t('backend','No Setting');
                    ?>
                    <?= Html::encode($last_name) ?>
                </div>
            </div>
            <div class="col-md-12 row-inline div-padding clear-padding">
                <div class="col-md-5">
                    <?php  $first_name_kana = !empty($model->first_name_kana)?
                            $model->first_name_kana
                            : Yii::t('backend','No Setting');
                    ?>
                    <?= Html::encode($first_name_kana) ?>
                </div>
                <div class="col-md-7">
                    <?php  $last_name_kana = !empty($model->last_name_kana)?
                            $model->last_name_kana
                            : Yii::t('backend','No Setting');
                    ?>
                    <?= Html::encode($last_name_kana) ?>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12 row-inline">
        <div class="col-md-4 div-border bg_header">
            <?= Yii::t('backend', 'Birthday'); ?>
        </div>
        <div class="col-md-8">
            <div class="col-md-12 div-padding">
                <?php  $birth_date = !empty($model->birth_date)?
                        date('Y 年 m 月 d 日', strtotime($model->birth_date))
                        : Yii::t('backend','No Setting');
                ?>
                <?= Html::encode($birth_date) ?>
            </div>
        </div>
    </div>
    <div class="col-md-12 row-inline">
        <div class="col-md-4 div-border bg_header">
            <?= Yii::t('backend', 'Sex'); ?>
        </div>
        <div class="col-md-8">
            <div class="col-md-12 div-padding">
                <?php
                if (!empty($model->sex) || $model->sex == '0'){
                    if($model->sex == 1){
                        echo Yii::t('backend', 'Female');
                    } else {
                        echo Yii::t('backend', 'Male');
                    }
                }else{
                    echo Yii::t('backend','No Setting');
                }
                ?>
            </div>
        </div>
    </div>
    <div class="col-md-12 row-inline">
        <div class="col-md-4 bg_header div-border div-horizontal">
            <?= Yii::t('backend','Address'); ?>
        </div>
        <div class="col-md-8">
            <div class="col-md-12 clear-padding div-padding">
                <?php if(!empty($model->post_code) || !empty($model->prefecture) || !empty($model->address)){ ?>
                <div class="col-md-12">
                    <?= !empty($model->post_code)?Html::encode(substr($model->post_code, 0,3)):'' ?>
                    &nbsp;<?= !empty($model->post_code)?'-':'' ?>
                    &nbsp;<?= !empty($model->post_code)?Html::encode(substr($model->post_code, 3,7)):'' ?></div>
                <div class="col-md-12"><?= Html::encode($model->prefecture) ?>&nbsp;&nbsp;<?= Html::encode($model->address) ?></div>
                <?php }else {
                    echo '<div class="col-md-12">'.Yii::t('backend','No Setting').'</div>';
                } ?>
            </div>
        </div>
    </div>
    <div class="col-md-12 form-group">
    </div>
    <div class="col-md-12 form-group">
    </div>
    <div class="col-md-12">
        <div class="form-group text-center">
            <?= Html::a('<i class="fa fa-caret-left" aria-hidden="true" style="color:#FF0066;">&nbsp</i>'.Yii::t('backend', 'Return'), ['update','id'=>$model->id], ['class' => 'btn common-back']) ?>
            <?= Html::submitButton(Yii::t('backend', 'Change'), ['class' => 'btn cm-btn-submit'])
            ?>
        </div>
    </div>
    <?php ActiveForm::end(); ?>

</div>
