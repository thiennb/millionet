<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\MasterMember */

$this->title = Yii::t('frontend', 'Confirmation of member information');
?>
<div class="shared-common-container">
    <div class="shared-section-title">
        <?= Html::encode($this->title) ?>
    </div>
    <div class="row shared-toppad-lg">

    </div>
    <div class="row">
        <?=
        $this->render('_form_update_confirm', [
            'model' => $model,
        ])
        ?>
    </div>
</div>
