<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\bootstrap\ActiveForm;
use yii\web\Session;

/* @var $this yii\web\View */
/* @var $searchModel common\models\MasterMemberSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->registerCssFile('@web/css/member.css', ['depends' => [yii\bootstrap\BootstrapAsset::className()]]);
$this->title = Yii::t('frontend', 'Completion of registration');
?>
<div class="row">
    <div class="col-lg-12 col-md-12">
        <div class="box box-info box-solid">
            <div class="box-body content">
                <div class="col-md-12">
                    <div class="common-box col-md-12">
                        <div class="col-md-6 col-md-offset-3"
                             <div class="col-md-12">
                                <div class="box-header with-border common-box-h4 col-md-8">
                                    <h4 class="text-title"><b><?= Yii::t('frontend', 'Confirmation of member information') ?></b></h4>
                                </div>
                            </div>
                            <div class="col-md-12 row-inline"><div class="form-group"></div></div>
                            <div class="col-md-12">
                                <div class="col-md-12 text-center">
                                    <div class="row row-inline"><div class="form-group"></div></div>
                                    <p><?= Yii::t('frontend','Change of member information has been completed.') ?></p>
                                    <div class="row row-inline"><div class="form-group"></div></div>
                                    <?= Html::a(Yii::t('backend', 'Back to Service').'&nbsp;<i class="fa fa-caret-right" aria-hidden="true" style="color:#FF0066;">&nbsp;</i>', ['/top'], ['class' => 'btn common-back']) ?>
                                    <div class="row row-inline"><div class="form-group"></div></div>
                                    <?= Html::a(Yii::t('backend', 'To member information change').'&nbsp;<i class="fa fa-caret-right" aria-hidden="true" style="color:#FF0066;">&nbsp;</i>', ['update', 'id' => $id], ['class' => 'btn common-back']) ?>
                                </div>
                            </div>                            
                            <div class="col-md-12 row-inline"><div class="form-group"></div></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
