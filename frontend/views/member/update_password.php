<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use common\models\MasterMember;
use dmstr\widgets\Alert;

/* @var $this yii\web\View */
/* @var $searchModel common\models\MasterMemberSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('backend', 'Change password');
?>
<div class="shared-common-container">
    <div class="shared-section-title">
        <?= $this->title ?>
    </div>
    <div class="col-xs-12">
        <div class="form-group"><?= Alert::widget() ?></div>
        <div class="row text-center">
            <?= Yii::t('backend','Set a new password, please proceed in the "change".') ?>
        </div>
        <div class="form-group"></div>
        <?php $form = ActiveForm::begin(['enableClientValidation' => FALSE]); ?>
        <div class="row row-inline">
            <div class="col-md-3 bg_header div-horizontal">
                <?= Yii::t('backend', 'Password old'); ?>
            </div>
            <div class="col-md-9 clear-form-group">
                <div class="col-md-12">
                    <?=
                    $form->field($model, 'password_old', [
                        'template' => '<div class="col-md-12">{input}{error}</div>'
                    ])->passwordInput(['maxlength' => true])
                    ?>
                </div>
            </div>
        </div>
        <?php $model->email = ''; ?>
        <div class="row row-inline">
            <div class="col-md-3 bg_header div-horizontal">
                <?= Yii::t('backend', 'Update New Password'); ?>
            </div>
            <div class="col-md-9 clear-form-group">
                <div class="col-md-12">
                    <?=
                    $form->field($model, 'password', [
                        'template' => '<div class="col-md-12">{input}{error}</div>'
                    ])->passwordInput(['maxlength' => true])
                    ?>
                    <div class="col-md-12"><?= Yii::t('backend', 'Please enter in the 6 to 20-digit alphanumeric'); ?></div>
                </div>
                <div class="col-md-12">
                    <?=
                    $form->field($model, 'password_re', [
                        'template' => '<div class="col-md-12">{input}{error}</div>'
                    ])->passwordInput(['maxlength' => true])
                    ?>
                    <div class="col-md-12"><?= Yii::t('backend', 'Please enter again to confirm'); ?></div>
                </div>
            </div>
        </div>
        <div class="form-group"></div>
        <div class="row text-center">
            <div class="col-md-3 clear-padding">
                <?= Html::a('<i class="fa fa-caret-left" aria-hidden="true" style="color:#FF0066;">&nbsp</i>'.Yii::t('backend', 'Return'), ['update','id'=>$model->id], ['class' => 'btn common-back pull-left']) ?>
            </div>
            <div class="col-md-9 text-left" style="padding-left: 0">
                <?= Html::submitButton(Yii::t('backend', 'Change'), ['class' => 'btn cm-btn-submit']) ?>
            </div>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>
