<?php
use common\components\Util;
use api\models\ApiSupport;
use common\models\CompanyStore;
use common\models\MasterRank;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$this->title=Yii::t('frontend','Rank Info');
?>
<div class="shared-common-container">
    <div class="shared-section-title">
        <?= Yii::t('frontend','Rank Info List') ?>
    </div>
    <div class="clearfix"> </div> <div class="row shared-toppad-lg"> </div>
    <div class="top-pagination-rank">
        <?php
            echo \yii\widgets\LinkPager::widget([
                'pagination' => $pages,
                'prevPageLabel' => '◀ '.Yii::t('frontend', 'Prev page'),
                'nextPageLabel' => Yii::t('frontend', 'Next page'). ' ▶',
                'pageCssClass' => 'hidden',
                ]);

        ?>
        <?php if($pages->getPageCount()>0) : ?>
            <div class="count-page-pagination"><span><?= ((int)$pages->getPage()+1).'/'.$pages->getPageCount().' '.Yii::t('frontend', 'ページ')?></span></div>
        <?php endif; ?>
    </div>
    <div class="clearfix"></div>
    <div class="rank-item main-content">
        <?php foreach($models as $model) : ?>
        <?php 
            $rank_setting = MasterRank::findRankbyCompanyId($model->store->company_id);
        ?>
            <div class="rank-item-detail">
                <div class="row">
                    <div class="col-md-12">
                        <div class="rank-item-head">
                            <div class="rank-item-name">
                                <b><span><?= $model['name'] ?></span></b>
                            </div>
                            <div class="line-heading"></div>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="rank-item-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="rank-item-info">
                                        <div class="rank-name">
                                            <span><?= Yii::t('frontend','Rank Name')?> : </span>
                                            <span class="rank-level-name">
                                                <?php
                                                    $image_rank = '';
                                                    if($model['rank_id'] == null){
                                                        $image_rank = '01_rank.png';
                                                        echo Yii::t('frontend', 'Normal');
                                                    }else{
                                                        switch ($model['rank_id']){
                                                            case '00': $image_rank = '01_rank.png'; echo Yii::t('frontend', 'Normal'); break;
                                                            case '01': $image_rank = '02_rank.png'; echo Yii::t('frontend', 'Bronze'); break;
                                                            case '02': $image_rank = '03_rank.png'; echo Yii::t('frontend', 'Silver'); break;
                                                            case '03': $image_rank = '04_rank.png'; echo Yii::t('frontend', 'Golden'); break;
                                                            case '04': $image_rank = '05_rank.png'; echo Yii::t('frontend', 'Black'); break;
                                                            default: break;
                                                        }
                                                    }
                                                ?>
                                            </span>
                                        </div>
                                        <div class="rank-image">
                                            <image src="<?= ApiSupport::getRankUrl($image_rank) ?>" />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="rank-item-condition">
                                        <?php if($rank_setting['rank_apply_type'] == '00'): ?>
                                            <?= Util::getCompleteRankCondition( $model['rank_id'],$model['total_point'],$model['number_visit'],$model['total_amount'], $rank_setting)['text']?>
                                        <?php else: ?>
                                            <?php $rankUpInfo = Yii::$app->user->identity->customer->getRankInfoInMonth(Yii::$app->user->identity->customer->id, $model['store_id']); ?>
                                            <?= Util::getCompleteRankCondition( $model['rank_id'],$rankUpInfo['total_point'],$rankUpInfo['number_visit'],$rankUpInfo['total_amount'], $rank_setting)['text'] ?>
                                        <?php endif; ?>
                                    </div>
                                    <div class="link-rank-detail">
                                        <a href="/member/rank-info-detail?id=<?= $model['id'] ?>">
                                            <span class="arrow-link">▶</span> <?= Yii::t('frontend','View Detail') ?>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
</div>
<div class="clearfix"></div>
