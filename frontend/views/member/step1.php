<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use common\models\MasterMember;
use dmstr\widgets\Alert;

/* @var $this yii\web\View */
/* @var $searchModel common\models\MasterMemberSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->registerCssFile('@web/css/member.css', ['depends' => [yii\bootstrap\BootstrapAsset::className()]]);
$this->registerJsFile('@web/js/member.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->title = Yii::t('backend', 'Register Member');
?>
<div class="row">
    <div class="col-lg-12 col-md-12">
        <div class="box box-info box-solid">
            <div class="box-body content">
                <div class="common-box col-md-12">
                    <div class="row">
                        <div class="col-md-8 col-md-offset-2">
                            <ol class="rsvStepList cFix">
                                <li class="rslStep1 active"><span></span>宛先登録</li>
                                <li class="rslStep2 "><span></span>認証キー入力</li>
                                <li class="rslStep3 "><span></span>会員情報入力</li>
                                <li class="rslStep4 "><span></span>会員情報確認</li>
                                <li class="rslStepEnd ">登録完了</li>
                            </ol>
                        </div>
                    </div>
                    <div class="form-group">
                    </div>
                    <div class="row">
                        <div class="col-md-4 col-md-offset-4">
                            <div class="box box-info box-solid">
                                <div class="box-header with-border">
                                    <h4 class="text-title text-center"><b><?= Yii::t('backend', 'Input phone number') ?></b></h4>
                                </div>
                                <div class="box-body content">
                                    <div class="col-md-12">
                                        <div class="col-md-10 col-md-offset-1">
                                            <div class="row">
                                                <div class="box-header with-border common-box-h4-1 col-md-12">
                                                    <?= Yii::t('backend', '電話番号を入力してください') ?>
                                                </div>
                                            </div>
                                            <div class="form-group"><?= Alert::widget() ?></div>
                                            <div class="row text-center">
                                                <?php $form = ActiveForm::begin(['enableClientValidation' => FALSE]); ?>
                                                    <div class="row clear-form-group col-md-12">
                                                        <?= $form->field($model, 'phone_1',[
                                                            'template' => '<div class="col-md-3 clear-padding">{input}'
                                                                            . '{error}'
                                                                        . '</div>'
                                                        ])->textInput(['maxlength' => true,'class'=>'form-control text-right input-number']) ?>
                                                        <?= $form->field($model, 'phone_2',[
                                                            'template' => '<div class="col-md-1 div-horizontal padding-10"><i class="fa fa-minus" aria-hidden="true"></i></div>'
                                                                        . '<div class="col-md-3 clear-padding">{input}'
                                                                            . '{error}'
                                                                        . '</div>'
                                                        ])->textInput(['maxlength' => true,'class'=>'form-control text-right input-number']) ?>
                                                        <?= $form->field($model, 'phone_3',[
                                                            'template' => '<div class="col-md-1 div-horizontal padding-10"><i class="fa fa-minus" aria-hidden="true"></i></div>'
                                                                        . '<div class="col-md-3 clear-padding">{input}'
                                                                            . '{error}'
                                                                        . '</div>'
                                                        ])->textInput(['maxlength' => true,'class'=>'form-control text-right input-number']) ?>
                                                    </div>
                                                    <p><?= Yii::t('backend', '※ ご登録された電話番号に認証キーをお送りします。') ?></p>
                                                    <div class="form-group"></div>
                                                    <div class="row text-center">
                                                        <?= Html::submitButton(Yii::t('backend', 'Register'), ['class' => 'btn cm-btn-submit']) ?>
                                                    </div>

                                                <?php ActiveForm::end(); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
