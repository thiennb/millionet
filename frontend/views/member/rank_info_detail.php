 <?php
use \yii\widgets;
use common\components\Util;
use \yii\helpers\Html;
use api\models\ApiSupport;
use common\models\BookingBusiness;

$this->title=Yii::t('frontend','Rank Info Detail');
$this->registerCSSFile('@web/css/history.css',['position' => \yii\web\View::POS_HEAD]);
?>
<div class="shared-common-container">
    <div class="shared-section-title">
        <?php echo Yii::t('frontend','Rank Info Detail') ?>
    </div>
    <div class="clearfix"></div>

        <div class="col-md-12">
            <div class="row rank-item main-content">
                <div class="rank-item-detail rank-detail-page" style="margin:15px 0">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="rank-item-head">
                                <div class="rank-item-name">
                                    <b><span><?php echo $store['name'] ?></span></b>
                                </div>
                                <div class="line-heading"></div>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="rank-item-body">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="rank-item-info">
                                            <div class="rank-name">
                                                <span><?php echo Yii::t('frontend','Rank Name')?> : </span>
                                                <span class="rank-level-name">
                                                    <?php
                                                        $image_rank = '';
                                                        if($customer_store['rank_id'] == null){
                                                            $image_rank = '01_rank.png';
                                                            echo Yii::t('frontend', 'Normal');
                                                        }else{
                                                            switch ($customer_store['rank_id']){
                                                                case '00': $image_rank = '01_rank.png'; echo Yii::t('frontend', 'Normal'); break;
                                                                case '01': $image_rank = '02_rank.png'; echo Yii::t('frontend', 'Bronze'); break;
                                                                case '02': $image_rank = '03_rank.png'; echo Yii::t('frontend', 'Silver'); break;
                                                                case '03': $image_rank = '04_rank.png'; echo Yii::t('frontend', 'Golden'); break;
                                                                case '04': $image_rank = '05_rank.png'; echo Yii::t('frontend', 'Black'); break;
                                                                default: break;
                                                            }
                                                        }
                                                    ?>
                                                </span>
                                            </div>
                                            <div class="rank-image">
                                                <img src="<?php echo ApiSupport::getRankUrl($image_rank) ?>">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="rank-item-condition">
                                            <?php
                                                if($rank_setting['rank_apply_type'] == '00'):
                                                    $rankUpInfo = $customer_store;
                                                    $currentCondition = Util::getCompleteRankCondition($customer_store['rank_id'],$customer_store['total_point'],$customer_store['number_visit'],$customer_store['total_amount'], $rank_setting);

                                                else:
                                                    $rankUpInfo = Yii::$app->user->identity->customer->getRankInfoInMonth(Yii::$app->user->identity->customer->id, $customer_store['store_id']);
                                                    $currentCondition = Util::getCompleteRankCondition( $customer_store['rank_id'],$rankUpInfo['total_point'],$rankUpInfo['number_visit'],$rankUpInfo['total_amount'], $rank_setting);
                                                endif;
                                                $requireCondition = '3';
                                                switch($rank_setting['rank_condition']){
                                                    case '00' : $requireCondition = '3'; break;
                                                    case '01' : $requireCondition = '2'; break;
                                                    case '02' : $requireCondition = '1'; break;
                                                    default: break;
                                                }
                                            ?>
                                            <?php echo $currentCondition['text'] ?>
                                        </div>
                                        <div class="complete-rank">
                                            <?php
                                                if((int)$currentCondition['condition'] >= (int)$requireCondition){
                                                    echo '<span class="text-complete-rank">'.Yii::t('frontend', 'Completed Condition').' !</span>';
                                                }
                                            ?>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php if($currentCondition['next']!='00') : ?>
        <div class="clearfix"></div>
        <div class="row shared-toppad-lg"></div>
        <div class="shared-section-title">
            <?php echo Yii::t('frontend','Condition Rank Up') ?>
        </div>
        <div class="clearfix"></div>
        <div class="row">
            <div class="row shared-toppad-sm"></div>
            <div class="clearfix"></div>
            <div class="col-xs-12">
                <?php
                    $month_end = strtotime('last day of this month', time());
                ?>
                <?php if($rank_setting['rank_apply_type'] == '01') : ?>
                <span class="date-time-rank">
                    <?php echo date('m', $month_end) ?><?php echo Yii::t('frontend', 'Month') ?><?php echo date('d', $month_end) ?><?php echo Yii::t('frontend', 'Day') ?><?php echo Yii::t('frontend', 'To') ?><?php echo Yii::t('frontend', 'Following Condtions') ?><span style="color: red"><?php echo ($requireCondition == '3') ? Yii::t('frontend', 'Entire') : $requireCondition . Yii::t('frontend', 'Or More') ?></span><?php echo Yii::t('frontend', 'Rank Up By') ?>
                </span>
                <?php endif; ?>
                <?php
                    $condition = $currentCondition['condition_require'];
                    $classNameColumn = 'col-md-4';

                ?>
                <div class="row">
                    <?php if((int)$condition['numVisitCondition'] > 0): ?>
                        <?php $percent = (float)$rankUpInfo['number_visit']/(float)$condition['numVisitCondition'] * 100; ?>
                        <div class="<?php echo $classNameColumn ?>">
                            <div class="condition-rank-detail" >
                                <div class="number-rank-info">
                                    <span class="number-rank"><?php echo BookingBusiness::formatPoint($rankUpInfo['number_visit']) ?></span>
                                    <span class="number-rank-test"><?php echo Yii::t('frontend', 'Visit number of times')?></span>
                                </div>
                                <div class="percentage" data-percent="<?php echo $percent > 100 ? 100 : $percent ?>">
                                    <div class="text-center">
                                        <span><?php echo Yii::t('frontend', 'After')?></span>
                                        <span class="middle"><?php echo BookingBusiness::formatPoint((float)$rankUpInfo['number_visit'] - (float)$condition['numVisitCondition'] >= 0? 0: (float)$condition['numVisitCondition']-(float)$rankUpInfo['number_visit']) ?></span>
                                        <span><?php echo Yii::t('frontend', 'Number of times')?></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>

                    <?php if((int)$condition['pointCondition'] > 0 ): ?>
                        <?php $percent = (float)$rankUpInfo['total_point']/(float)$condition['pointCondition'] * 100; ?>
                        <div class="<?php echo $classNameColumn ?>">
                            <div class="condition-rank-detail">
                                <div class="number-rank-info">
                                    <span class="number-rank"><?php echo BookingBusiness::formatPoint($rankUpInfo['total_point']) ?></span>
                                    <span class="number-rank-test"><?php echo Yii::t('frontend', 'Accumulated points')?></span>
                                </div>
                                <div class="percentage" data-percent="<?php echo $percent > 100 ? 100:$percent ?>">
                                    <div class="text-center">
                                        <span><?php echo Yii::t('frontend', 'After')?></span>
                                        <span class="middle" ><?php echo BookingBusiness::formatPoint((float)$rankUpInfo['total_point'] - (float)$condition['pointCondition'] >= 0? 0: (float)$condition['pointCondition']-(float)$rankUpInfo['total_point']) ?></span>
                                        <span><?php echo Yii::t('frontend', 'Point')?></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>

                    <?php if((int)$condition['useMoneyCondition'] > 0): ?>
                        <?php $percent = (float)$rankUpInfo['total_amount']/(float)$condition['useMoneyCondition'] * 100; ?>
                        <div class="<?php echo $classNameColumn ?>">
                            <div class="condition-rank-detail">
                                <div class="number-rank-info">
                                    <span class="number-rank"><?php echo BookingBusiness::formatPoint($rankUpInfo['total_amount']) ?></span>
                                    <span class="number-rank-test"><?php echo Yii::t('frontend', 'Purchase price')?></span>
                                </div>
                                <div class="percentage" data-percent="<?php echo $percent > 100 ? 100:$percent ?>">
                                    <div class="text-center">
                                        <span><?php echo Yii::t('frontend', 'After')?></span>
                                        <span class="middle">¥ <?php echo BookingBusiness::formatMoney((float)$rankUpInfo['total_amount'] - (float)$condition['useMoneyCondition'] >= 0? 0: (float)$condition['useMoneyCondition']-(float)$rankUpInfo['total_amount'])  ?></span>
                                        <span><?php echo Yii::t('frontend', 'Purchase')?></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
        <?php endif; ?>
        <div class="clearfix"></div>
        <div style="margin: 2em 0 1em 0">
            <?php echo Html::a('◀ ' .Yii::t('frontend','Back'), ['member/rank-info'], ['class'=>'btn btn-default btn-pink btn-empty'])?>
        </div>
        <div class="clearfix"></div>
    </div>
<div class="clearfix"></div>
</div>
<div class="clearfix"></div>
<script>
    $(function () {
        $('.percentage').easyPieChart({
            animate: 1000,
            scaleColor:false,
            lineWidth: 10,
            size:180,
        });
    });
</script>
