<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\Session;
use yii\jui\DatePicker;
use common\components\Constants;
use common\models\MasterMember;
/* @var $this yii\web\View */
/* @var $model common\models\MasterMember */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="master-member-form">

    <?php $form = ActiveForm::begin(['enableClientValidation' => FALSE]); ?>
    <div class="row row-inline">
        <div class="col-md-3 bg_header div-horizontal">
            <label class='mws-form-label'><?= Yii::t('backend','Name'); ?></label>
            <span class="required-text">必須</span>
        </div>
        <div class="col-md-9 clear-form-group">
            <?= $form->field($model, 'first_name',[
                'template' => "<div class='col-md-2 clear-label-requir'>{label}</div>"
                            . '<div class="col-md-4">{input}{error}</div>'
            ])->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'last_name',[
                'template' => "<div class='col-md-2 clear-label-requir'>{label}</div>"
                            . '<div class="col-md-4">{input}{error}</div>'
            ])->textInput(['maxlength' => true]) ?>
            <div class="div-margin"></div>
            <?= $form->field($model, 'first_name_kana',[
                'template' => "<div class='col-md-2 clear-label-requir'>{label}</div>"
                            . '<div class="col-md-4">{input}{error}</div>'
            ])->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'last_name_kana',[
                'template' => "<div class='col-md-2 clear-label-requir'>{label}</div>"
                            . '<div class="col-md-4">{input}{error}'
                            . '</div>'
            ])->textInput(['maxlength' => true]) ?>
        </div>
    </div>
    <div class="row row-inline">
        <div class="col-md-3 bg_header div-horizontal">
            <label class='mws-form-label'><?= Yii::t('backend','Password'); ?></label>
            <span class="required-text">必須</span>
        </div>
        <div class="col-md-9 clear-form-group">
            <?= $form->field($model, 'password',[
                'template' => '<div class="col-md-12">{input}'.Yii::t('backend','Please enter in the 6 to 20-digit alphanumeric').'{error}</div>'
            ])->passwordInput(['maxlength' => true]) ?>
            <div class="div-margin"></div>
            <?= $form->field($model, 'password_re',[
                'template' => '<div class="col-md-12">{input}'.Yii::t('backend','For confirmation, please enter the other over the degree of').'{error}</div>'
            ])->passwordInput(['maxlength' => true]) ?>
        </div>
    </div>
    <?php if($model->mode == MasterMember::REGISTER_PHONE){ ?>
        <div class="row row-inline">
            <div class="col-md-3 bg_header div-horizontal">
                <label class='mws-form-label'><?= Yii::t('backend','Phone'); ?></label>
            </div>
            <div class="col-md-9 clear-form-group">
                &nbsp;&nbsp;&nbsp;&nbsp;
                <?php if(strlen($model->mobile) == 10){ ?>                
                    <?= substr($model->mobile, 0,2) ?>-
                    <?= substr($model->mobile, 2,4) ?>-
                    <?= substr($model->mobile, 6,4) ?>
                <?php } else{ ?>
                    <?= substr($model->mobile, 0,3) ?>-
                    <?= substr($model->mobile, 3,4) ?>-
                    <?= substr($model->mobile, 7,4) ?>
                <?php } ?>
            </div>
        </div>
        <div class="row row-inline">
            <div class="col-md-3 bg_header div-horizontal">
                <label class='mws-form-label'><?= Yii::t('backend','Email'); ?></label>
            </div>
            <div class="col-md-9 clear-form-group">
                <?= $form->field($model, 'email',[
                    'template' => '<div class="col-md-12">{input}{error}</div>'
                ])->input(['email'],['placeholder' => Yii::t('backend','Example︓○○○@exalmple.com')]) ?>
                <div class="div-margin"></div>
                <?= $form->field($model, 'email_re',[
                    'template' => '<div class="col-md-12">{input}'.Yii::t('backend','For confirmation, please enter the other over the degree of').'{error}</div>'
                ])->input('email') ?>
            </div>
        </div>
    <?php }else if($model->mode == MasterMember::REGISTER_EMAIL){ ?>
        <div class="row row-inline">
            <div class="col-md-3 bg_header div-horizontal">
                <label class='mws-form-label'><?= Yii::t('backend','Phone'); ?></label>
                <span class="required-text">必須</span>
            </div>
            <div class="col-md-9 clear-form-group">
                <?= $form->field($model, 'phone_1',[
                    'template' => '<div class="col-md-3">{input}'
                                    . '{error}'
                                . '</div>'
                ])->textInput(['maxlength' => true,'class'=>'form-control text-right input-number']) ?>
                <?= $form->field($model, 'phone_2',[
                    'template' => '<div class="col-md-1 text-center"><i class="fa fa-minus" aria-hidden="true"></i></div>'
                                . '<div class="col-md-3">{input}'
                                    . '{error}'
                                . '</div>'
                ])->textInput(['maxlength' => true,'class'=>'form-control text-right input-number']) ?>
                <?= $form->field($model, 'phone_3',[
                    'template' => '<div class="col-md-1 text-center"><i class="fa fa-minus" aria-hidden="true"></i></div>'
                                . '<div class="col-md-3">{input}'
                                    . '{error}'
                                . '</div>'
                ])->textInput(['maxlength' => true,'class'=>'form-control text-right input-number']) ?>
            </div>
        </div>
        <div class="row row-inline">
            <div class="col-md-3 bg_header div-horizontal">
                <label class='mws-form-label'><?= Yii::t('backend','Email'); ?></label>
            </div>
            <div class="col-md-9 clear-form-group">
                &nbsp;&nbsp;&nbsp;&nbsp;
                <?= $model->email; ?>
            </div>
        </div>
    <?php } ?>
    <div class="row row-inline">
        <div class="col-md-3 bg_header div-horizontal">
            <label class='mws-form-label'><?= Yii::t('backend','Birthday'); ?></label>
        </div>
        <div class="col-md-9 clear-form-group">
            <?= $form->field($model, 'birth_date',[
                'template' => '<div class="col-md-12">{input}{error}</div>'
            ])->widget(DatePicker::classname(), [
                'language' => 'ja',
                'dateFormat' => 'yyyy/MM/dd',
                'clientOptions' => [
                    "changeMonth" => true,
                    "changeYear" => true,
                    "yearRange"=> "1900:+0"
                ]
            ]) ?>
        </div>
    </div>
    <div class="row row-inline">
        <div class="col-md-3 bg_header div-horizontal">
            <label class='mws-form-label'><?= Yii::t('backend','Sex'); ?></label>
        </div>
        <div class="col-md-9 clear-form-group">
            <?php echo $form->field($model, 'sex',[
                'template' => '<div class="col-md-9">{input}{error}</div>'
            ])->radioList(Constants::LIST_SEX); ?>
        </div>
    </div>
    <div class="row">
        <div class="form-group text-center">
            
            <?php 
//                $model->mode==MasterMember::REGISTER_PHONE?
//                Html::a(Yii::t('backend', 'Return'), ['step2'], ['class' => 'btn common-button-default'])
//                :Html::a(Yii::t('backend', 'Return'), ['step1'], ['class' => 'btn common-button-default']) 
            ?>
            <?= Html::submitButton(Yii::t('backend', 'Go to the confirmation screen'), 
                    ['class' => 'btn cm-btn-submit']) ?>
        </div>
    </div>
    <?php ActiveForm::end(); ?>

</div>
