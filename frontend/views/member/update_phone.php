<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use common\models\MasterMember;
use dmstr\widgets\Alert;

/* @var $this yii\web\View */
/* @var $searchModel common\models\MasterMemberSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->registerCssFile('@web/css/member.css', ['depends' => [yii\bootstrap\BootstrapAsset::className()]]);
$this->registerJsFile('@web/js/member.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile('@web/js/common-js.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->title = Yii::t('frontend', 'Phone number changes');
?>
<div class="clearfix"></div>
<div class="shared-common-container">
    <div class="shared-section-title">
        <?= $this->title ?>
    </div>
    <div class="row shared-toppad-lg"></div>
    <div class="row" style="margin: 0 -30px">
        <div class="col-xs-12">
            <div class="col-md-12 text-center"><p><?= Yii::t('frontend','To set up a new phone number, please proceed in the change.') ?></p></div>
                <?php $form = ActiveForm::begin(['enableClientValidation' => FALSE]); ?>
            <div class="col-md-12 row-inline">
                <div class="col-md-3 bg_header div-horizontal">
                    <?= Yii::t('frontend', 'Before change the phone number'); ?>
                </div>
                <div class="col-md-9 clear-form-group">
                    <div class="col-md-12">
                        <div class="col-md-3">
                            <?=
                            $form->field($model, 'phone_1', [
                                'template' => '{input}{error}'
                            ])->textInput(['maxlength' => true, 'class' => 'form-control text-right input-number'])
                            ?>
                        </div>
                        <div class="col-md-1 div-horizontal padding-10 text-center"><i class="fa fa-minus" style="margin: 0 auto" aria-hidden="true"></i></div>
                        <div class="col-md-3">
                            <?=
                            $form->field($model, 'phone_2', [
                                'template' => '{input}{error}'
                            ])->textInput(['maxlength' => true, 'class' => 'form-control text-right input-number'])
                            ?>
                        </div>
                        <div class="col-md-1 div-horizontal padding-10 text-center"><i class="fa fa-minus" style="margin: 0 auto" aria-hidden="true"></i></div>
                        <div class="col-md-3">
                            <?=
                            $form->field($model, 'phone_3', [
                                'template' => '{input}{error}'
                            ])->textInput(['maxlength' => true, 'class' => 'form-control text-right input-number'])
                            ?>
                        </div>
                        <div class="col-md-2"></div>
                    </div>
                </div>
            </div>
            <div class="col-md-12 row-inline">
                <div class="col-md-3 bg_header div-horizontal">
                            <?= Yii::t('frontend', 'New phone number'); ?>
                </div>
                <div class="col-md-9 clear-form-group">
                    <div class="col-md-12">
                        <div class="col-md-3">
                            <?=
                            $form->field($model, 'phone_1_new', [
                                'template' => '{input}{error}'
                            ])->textInput(['maxlength' => true, 'class' => 'form-control text-right input-number'])
                            ?>
                        </div>
                        <div class="col-md-1 div-horizontal padding-10 text-center"><i class="fa fa-minus" style="margin: 0 auto" aria-hidden="true"></i></div>
                        <div class="col-md-3">
                            <?=
                            $form->field($model, 'phone_2_new', [
                                'template' => '{input}{error}'
                            ])->textInput(['maxlength' => true, 'class' => 'form-control text-right input-number'])
                            ?>
                        </div>
                        <div class="col-md-1 div-horizontal padding-10 text-center"><i class="fa fa-minus" style="margin: 0 auto" aria-hidden="true"></i></div>
                        <div class="col-md-3">
                            <?=
                            $form->field($model, 'phone_3_new', [
                                'template' => '{input}{error}'
                            ])->textInput(['maxlength' => true, 'class' => 'form-control text-right input-number'])
                            ?>
                        </div>
                        <div class="col-md-2"></div>
                    </div>
                    <div class="col-md-12 text-left"><div class="col-md-12"><?= Yii::t('frontend', 'Please enter the 10 to 11-digit alphanumeric'); ?></div></div>
                    <div class="col-md-12">
                        <div class="col-md-3">
                            <?=
                            $form->field($model, 'phone_1_re', [
                                'template' => '{input}{error}'
                            ])->textInput(['maxlength' => true, 'class' => 'form-control text-right input-number'])
                            ?>
                        </div>
                        <div class="col-md-1 div-horizontal padding-10 text-center"><i class="fa fa-minus" style="margin: 0 auto" aria-hidden="true"></i></div>
                        <div class="col-md-3">
                            <?=
                            $form->field($model, 'phone_2_re', [
                                'template' => '{input}{error}'
                            ])->textInput(['maxlength' => true, 'class' => 'form-control text-right input-number'])
                            ?>
                        </div>
                        <div class="col-md-1 div-horizontal padding-10 text-center"><i class="fa fa-minus" style="margin: 0 auto" aria-hidden="true"></i></div>
                        <div class="col-md-3">
                            <?=
                            $form->field($model, 'phone_3_re', [
                                'template' => '{input}{error}'
                            ])->textInput(['maxlength' => true, 'class' => 'form-control text-right input-number'])
                            ?>
                        </div>
                        <div class="col-md-2"></div>
                    </div>
                    <div class="col-md-12 text-left"><div class="col-md-12"><?= Yii::t('backend', 'For confirmation, please enter the other over the degree of'); ?></div></div>
                </div>
            </div>
            <div class="col-md-12 form-group"></div>
            <div class="col-md-12">
                <div class="col-md-3 clear-padding">
                    <?= Html::a('<i class="fa fa-caret-left" aria-hidden="true" style="color:#FF0066;">&nbsp</i>' . Yii::t('backend', 'Return'), ['update', 'id' => $model->id], ['class' => 'btn common-back']) ?>
                </div>
                <div class="col-md-6 text-left" style="padding-left: 0">
                    <?= Html::submitButton(Yii::t('backend', 'Change'), ['class' => 'btn cm-btn-submit']) ?>
                </div>
            </div>
            <div class="col-md-12 form-group"></div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
<div class="clearfix"></div>
