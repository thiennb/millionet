<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use dmstr\widgets\Alert;
use common\models\MasterMember;

/* @var $this yii\web\View */
/* @var $searchModel common\models\MasterMemberSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->registerCssFile('@web/css/member.css', ['depends' => [yii\bootstrap\BootstrapAsset::className()]]);
$this->title = Yii::t('backend', 'Register Member');
?>
<div class="row">
    <div class="col-lg-12 col-md-12">
        <div class="box box-info box-solid">
            <div class="box-body content">
                <div class="common-box col-md-12">
                    <div class="row">
                        <div class="col-md-10 col-md-offset-1">
                            <ol class="rsvStepList cFix">
                                <li class="rslStep1 passive"><span></span>宛先登録</li>
                                <li class="rslStep2 active"><span></span>認証キー入力</li>
                                <li class="rslStep3 "><span></span>会員情報入力</li>
                                <li class="rslStep4 "><span></span>会員情報確認</li>
                                <li class="rslStepEnd ">登録完了</li>
                            </ol>
                        </div>
                    </div>
                    <div class="form-group">
                    </div>
                    <div class="row">
                        <div class="col-md-4 col-md-offset-4">
                            <div class="box box-info box-solid">
                                <div class="box-header with-border">
                                    <h4 class="text-title text-center"><b><?= Yii::t('backend', 'Authentication key input') ?></b></h4>
                                </div>
                                <div class="box-body content">
                                    <div class="col-md-12">
                                        <div class="col-md-10 col-md-offset-1">
                                            <div class="row">
                                                <div class="box-header with-border common-box-h4 col-md-12">
                                                    <h4 class="text-title"><b><?= Yii::t('backend', '認証キーを入力してください') ?></b></h4>
                                                </div>
                                            </div>
                                            <div class="form-group"><?= Alert::widget() ?></div>
                                            <div class="row">
                                                <?php $form = ActiveForm::begin(['enableClientValidation' => FALSE]); ?>
                                                    <div class="row clear-form-group">
                                                        <?= $form->field($model, 'key_confirm',[
                                                            'template' => '<div class="col-md-12">{input}{error}'
                                                                            . '<p class="help-block help-block-error"></p>'
                                                                        . '</div>'
                                                        ])->textInput(['maxlength' => true]) ?>
                                                    </div>
                                                    <p><?= Yii::t('backend', 'ご登録いただいた電話番号宛に届いた認証キーを入力してください。認証キーの有効期限は 30 分です。') ?></p>
                                                    <div class="row text-center">
                                                        <?php //Html::a(Yii::t('backend', 'Return'), ['step1'], ['class' => 'btn btn-default common-button-default']) ?>
                                                        <?= Html::submitButton(Yii::t('backend', 'Input key confirm'), ['class' => 'btn cm-btn-submit']) ?>
                                                    </div>
                                                <?php ActiveForm::end(); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row text-center">
                        <?php if($model->mode == MasterMember::REGISTER_EMAIL){ ?> 
                            <p><?= Html::a(Yii::t('frontend','Here if you want to send the other over the degree of message'),['step1-email'])?></p>
                        <?php }else{ ?>
                            <p><?= Html::a(Yii::t('frontend','Here if you want to send the other over the degree of message'),['step1'])?></p>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
