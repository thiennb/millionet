<?php

use yii\helpers\Url;
use yii\helpers\Html;
use common\components\Util;

?>
<?php
$baseUrl = Url::base(TRUE);
?>
<div class="clearfix">
    <div class="col-xs-12 black-bordered-box" id="main_cointain">
        <div class="row">
            <div class="col-md-6">
                <span><?= ($model->name) ? $model->name : ''; ?></span>
            </div>
        </div>
        <div style="height:20px"></div>
        <div class="related-post">
        </div>
        <!-- IMAGE AND INFO STORE -->
        <div class="row">
            <div class="col-md-4" id="image_store">
                <img src="<?php echo Util::getUrlImage($model->image1) ?>" class="img-responsive" style="height: 200px">
            </div>
            <div class="col-md-8">
                <div class="form-group">
                    <?= ($model->introduce) ? $model->introduce : ''; ?>
                </div>
                <div class="form-group">
                    <?php
                    $tel = ($model->tel)? $model->tel : '';
                    $tel_formart = ($model->tel != '') ? sprintf("%s-%s-%s", substr($tel, 0, 3), substr($tel, 3, 3), substr($tel, 6)) : '';
                    ?>
                    <?= Yii::t('backend', 'Phone') ?> : <?php echo $tel_formart ; ?>
                </div>
                <div class="form-group">
                    <?= Yii::t('backend', 'Business hours') ?> : <?= $model->time_open?> 〜 <?= $model->time_close ; ?>
                </div>
                <div class="form-group">
                    <?= Yii::t('backend', 'Regular Holiday') ?> : <?= ($model->regular_holiday) ?  $model->regular_holiday : ''; ?>
                </div>
                <div class="form-group">
                    URL : <?= ($model->website) ? $model->website : ''; ?>
                </div>
                <div class="form-group">
                    <a href="<?php echo $baseUrl . '/history/storedetail?id=' . $model->id; ?>" id ="store_detail" class="pull-right">
                        <?= Html::submitButton('詳細を見る', ['class' => 'btn common-button-submit', 'id' => 'btn-submit']) ?>
                    </a>
                </div>
            </div>
        </div>
        <!-- END IMAGE AND INFO STORE -->
    </div>
</div>
<!-- SPACE  -->
