<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\grid\GridView;
use yii\widgets\LinkPager;
use yii\helpers\Url;
use yii\jui\DatePicker;
/* @var $this yii\web\View */
/* @var $searchModel common\models\MasterNoticeMemberSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->title = Yii::t('backend', '購入履歴 詳細');
?>
<style>
    tr,td {
        border :1px solid black;
        border-collapse: collapse;
        }
    td.col-md-1 {
        height : 44px;
    }
    tr:hover td {background:#F9EDED}
    #space {
        height: 30px;
    }
</style>
<div class="row">
    <div class="col-lg-12 col-md-12">
        <div class="box box-info box-solid">
            <div class="box-header with-border">
                <h4 class="text-title"><b><?= $this->title ?></b></h4>
            </div>
            <div class="box-body content">
                <div class="col-md-12">
                    <div class="common-box">
                        <div class="row">
                            <div class="box-header with-border common-box-h4 col-md-6">
                                <h4 class="text-title"><b><?= Yii::t('frontend', 'Booking Title') ?></b></h4>
                            </div>
                        </div>
                        <div class="box-body content">
                            <?php // if ($Store) { ?>
                            <div class="row">
                                <div class="col-md-2" style="height:300px;border:1px solid black; margin-left:1.5%;">
                                    <p style="padding:50% 0 0 35%"><?= Yii::t('frontend', 'Store Info') ?></p>
                                </div>
                                <div class="col-md-5" style="height:300px;border:1px solid black">
                                    <div class="form-group">
                                        <?= Yii::t('frontend','Store Name') ?> : <?php  echo ($storeInfo['name']) ? $storeInfo['name'] : ''; ?>
                                    </div>
                                    <div class="form-group">
                                        <?= Yii::t('frontend','Store Address') ?> : <?php // secho ($Store['name']) ? $Store['name'] : ''; ?>
                                    </div>
                                    <div class="form-group">
                                        <?php  if (!empty($storeInfo['longitude']) && !empty($storeInfo['latitude'])) { ?>
                                        <div class="row row-inline table-bordered">
                                            <iframe width="100%" 
                                                    height="200" 
                                                    frameborder="0" 
                                                    scrolling="no" 
                                                    marginheight="0" 
                                                    marginwidth="0" 
                                                    src="https://maps.google.com/maps?f=q&source=s_q&hl=en&geocode=&q=<?php echo  str_replace(" ", "+", $storeInfo['address']) ?>&ie=UTF8&hq=&t=m&z=14&ll=<?php  echo $storeInfo['latitude'] ?>,<?php  echo $storeInfo['longitude']; ?>&output=embed">
                                            </iframe>
                                         </div>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                            <div class="row" style="height:20px">
                            </div>
                            <div class="row" style="margin-top:-1.3%">
                                <div class="col-md-8" >
                                     <table style="margin-left:7.5px; width:90.2%">
                                         <tr class="something">
                                            <td class="new1"  style="width:28.5%; height:30px" id="top_border"><?= Yii::t('frontend','Point') ;?></td>
                                            <td class="new2"><?php // echo ($Store['point_use']) ? $Store['point_use'] : '[OP]' ;?></td> 
                                        </tr>                        
                                    </table>    
                                </div>                   
                            </div>
                            <?php // } ?>
                        </div>
                        <!--  TALBE BOOKING DETAIL  -->

                        <div class="row">
                        <div class="col-md-8" style="margin-left:-1.77%;width :69%">
                                     <table style="margin-left:1%; width:90.1%">
                                        <tr class="something" style="width:30%">
                                            <td class="col-md-1" style="width:26.2%" id="top_border"><?= Yii::t('frontend','Product Buyed') ;?></td>
                                            <td class="col-md-4">購入商品の詳細を見る</td>
                                            <td class="col-md-4">購入商品の詳細を見る</td>
                                             
                                        </tr>  

                                        <tr class="something" style="width:30%">
                                            <td class="col-md-1" style="width:26.2%" id="top_border"><?= Yii::t('frontend','Product Buyed') ;?></td>
                                            <td class="col-md-4">購入商品の詳細を見る</a>
                                            <td class="col-md-4">購入商品の詳細を見る</a>
                                            </td> 
                                        </tr>  
                                         <tr class="something" style="width:30%">
                                            <td class="col-md-1" style="width:26.2%" id="top_border"><?= Yii::t('frontend','Product Buyed') ;?></td>
                                            <td class="col-md-4">購入商品の詳細を見る</a>
                                            <td class="col-md-4">>購入商品の詳細を見る</a>
                                            </td> 
                                        </tr>  
                                         <tr class="something" style="width:30%">
                                            <td class="col-md-1" style="width:26.2%" id="top_border"><?= Yii::t('frontend','Product Buyed') ;?></td>
                                            <td class="col-md-4">購入商品の詳細を見る</a>
                                            <td class="col-md-4">購入商品の詳細を見る</a>
                                            </td> 
                                        </tr>  
                                         <tr class="something" style="width:30%">
                                            <td class="col-md-1" style="width:26.2%" id="top_border"><?= Yii::t('frontend','Product Buyed') ;?></td>
                                            <td class="col-md-4">購入商品の詳細を見る</a>
                                            <td class="col-md-4">購入商品の詳細を見る</a>
                                            </td> 
                                        </tr>  
                                         


                                         <tr class="something" style="width:30%">
                                            <td class="col-md-1" style="width:26.2%" id="top_border"><?= Yii::t('frontend','Product Buyed') ;?></td>
                                            <td class="col-md-4">購入商品の詳細を見る</a>
                                            <td class="col-md-4">購入商品の詳細を見る</a>
                                            </td> 
                                        </tr>  
                                         <tr class="something" style="width:30%">
                                            <td class="col-md-1" style="width:26.2%" id="top_border"><?= Yii::t('frontend','Product Buyed') ;?></td>
                                            <td class="col-md-4">購入商品の詳細を見る</a>
                                            <td class="col-md-4">購入商品の詳細を見る</a>
                                            </td> 
                                        </tr>  
                                         <tr class="something" style="width:30%">
                                            <td class="col-md-1" style="width:26.2%" id="top_border"><?= Yii::t('frontend','Product Buyed') ;?></td>
                                            <td class="col-md-4">購入商品の詳細を見る</a>
                                            <td class="col-md-4">購入商品の詳細を見る</a>
                                            </td> 
                                        </tr>  
                                         <tr class="something" style="width:30%">
                                            <td class="col-md-1" style="width:26.2%" id="top_border"><?= Yii::t('frontend','Product Buyed') ;?></td>
                                            <td class="col-md-4">購入商品の詳細を見る</a>
                                            <td class="col-md-4">購入商品の詳細を見る</a>
                                            </td> 
                                        </tr>  

                                    </table>    
                                </div>          
                            </div>
                        <!-- END TABLE BOOKING DETAIL -->
                        </div>               
                        <div class="row" style="margin-top:3%">
                        <div class="col-md-7">
                        <div class="col-md-2" style="margin-left:1%">
                                    <a href="<?php echo Url::base(TRUE).'/history/buyhistory'?>" style="width : 100%" class="btn common-button-submit btn_search_byhistory">◀<?= Yii::t('frontend','Back'); ?></a>
                        </div>

                         <div class="col-md-2 pull-right" style="margin-left:1%">
                                    <a href="<?php echo Url::base(TRUE).'/history/buyhistory'?>" style="width : 100%" class="btn common-button-submit btn_search_byhistory">◀<?= Yii::t('frontend','Back'); ?></a>
                        </div>
                        </div>
                        </div>

                    </div>
                   


                </div>
            </div>
        </div>
    </div>
</div>
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <!--<div style="max-height:200px;overflow:scroll;"> -->
            <div id="scroll">
            <table style="width:100%" id="myTable" class="table table-bordered">
                <tr>
                    <th><?= Yii::t('frontend','Product Name')?></th>
                    <th><?= Yii::t('frontend','Quantity')?></th> 
                    <th><?= Yii::t('frontend','Total Money')?></th>
                </tr>
                <?php  // if ($detailOrder) { ?>                
                <?php // foreach ($detailOrder as $Order) : ?>
                <tr class="view_scroll">
                    <td ><?php // echo $Order['product_display_name']; ?>
                    <?php // if ($Order['gift_flg'] == 1) { ?>
                        <i class="fa fa-gift" aria-hidden="true"></i>
                    <?php // } ?> 
                    </td>
                    <td><?php // echo $Order['product_total']; ?></td> 
                    <td><?php // echo $Order['price']; ?></td>
                </tr>
                <?php // endforeach; ?>
                <?php // } ?>
            </table>
        </div>
      
    </div>

  </div>
</div>
<script>
    $(document).ready(function(){
    var id = $('#myTable tr.view_scroll').length;
        if (id > 5) {
            $("#scroll").css({
                'max-height': '200px',
                'overflow' : 'scroll'
            });
        };
       $("top_border").css("border-top", "1px solid black");
    })
</script>