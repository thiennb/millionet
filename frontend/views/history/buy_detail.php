<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\grid\GridView;
use yii\widgets\LinkPager;
use yii\helpers\Url;
use yii\jui\DatePicker;
use common\models\MstOrderSearch;
/* @var $this yii\web\View */
/* @var $searchModel common\models\MasterNoticeMemberSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->title = '購入履歴 詳細';
?>
<?php $this->registerCSSFile('@web/css/history.css',['position' => \yii\web\View::POS_HEAD]);?>
<style>
    tr,td {
        border :1px solid black;
        border-collapse: collapse;
        }
    td.col-md-1 {
        height : 44px;
    }
    td {
        text-align: left;
    }
    tr:hover td {background:#F9EDED}
</style>
<div class="shared-common-container">
    <div class="shared-section-title">
        <?= Yii::t('backend', '購入履歴 詳細') ?>
    </div>
    <div class="row shared-toppad-lg"></div>
    <?php if ($Store) { ?>
    <div class="row">
        <div class="col-xs-12">
            <div class="col-xs-2" style="height:300px;border:1px solid black;">
                <p style="padding:50% 0 0 35%"><?= Yii::t('frontend', 'Store Info') ?></p>
            </div>
            <div class="col-xs-10" style="height:300px;border:1px solid black; border-left: 0">
                <div class="form-group">
                    <?php echo ($Store['name']) ? $Store['name'] : ''; ?>
                </div>
                <div class="form-group">
                    <?php echo ($Store['address']) ? $Store['address'] : ''; ?>
                </div>
                <div class="form-group">
                    <?php // if (!empty($Store['longitude']) && !empty($Store['latitude']) && !empty($Store['address'])) { ?>
                    <div class="row row-inline table-bordered">
                        <iframe width="100%"
                                height="223"
                                frameborder="0"
                                scrolling="no"
                                marginheight="0"
                                marginwidth="0"
                                src="https://maps.google.com/maps?f=q&source=s_q&hl=en&geocode=&q=<?= str_replace(" ", "+", $Store['address']) ?>&ie=UTF8&hq=&t=m&z=14&ll=<?= $Store['latitude'] ?>,<?= $Store['longitude'] ?>&output=embed">
                        </iframe>
                     </div>
                    <?php // } ?>
                </div>
            </div>
        </div>
    </div>
    <div class="row shared-toppad-lg"></div>
    <div class="row shared-toppad-lg"></div>
    <div class="row">
        <div class="col-md-12">
             <table class="table table-responsive" style="border-top: solid 2px #666">
                <tr class="something">
                    <td class="col-xs-3" id="top_border"><?= Yii::t('frontend','Product Buyed') ;?></td>
                    <td class="col-md-9">
                        <div>
                            <a href="#" style="line-height:56px;" data-toggle="modal" data-target="#myModal"><?= Yii::t('frontend','Model Buy History') ?></a>
                            <div class="pull-right">
                            <?php if ($check_home_flg && $check_home_flg['to_home_delivery_flg'] == 1) { ?>
                                <i class="fa fa-truck" aria-hidden="true" style="color: #3eb2c5;font-size: 4em;"></i>
                            <?php  } ?>
                         <?php if (($check_gift_flg) && in_array("1",$check_gift_flg)) { ?>
                            <i class="fa fa-gift" aria-hidden="true" style="color: hotpink;font-size: 4em;"></i>
                        <?php } ?>
                        </div>
                        </div>

                    </td>
                </tr>
                <tr class="something">
                    <td class="col-md-1"><?= Yii::t('frontend','Money') ;?></td>
                    <td class="col-md-1"><?php echo ($Store['total']) ? '¥'.' '. number_format($Store['total']) : '¥'.'0'; ?>
                        <?php if ($moneyType): ?>
                            <?php if (isset($moneyType['money_cash']) && $moneyType['money_cash'] != 0)  : ?>
                                <button class="btn btn-primary"><?= Yii::t('frontend','CASH'); ?></button>
                            <?php endif ?>
                            <?php if (isset($moneyType['point_use']) && $moneyType['point_use'] != 0)  : ?>
                                <button class="btn btn-primary"><?= Yii::t('frontend','POINT');?></button>
                            <?php endif ?>
                            <?php if (isset($moneyType['charge_use']) && $moneyType['charge_use'] != 0)  : ?>
                                <button class="btn btn-primary"><?= Yii::t('frontend','CHARGE');?></button>
                            <?php endif ?>
                            <?php if (isset($moneyType['money_ticket']) && $moneyType['money_ticket'] != 0)  : ?>
                               <button class="btn btn-primary"><?= Yii::t('frontend','PRODUCT TICKET');?></button>
                            <?php endif ?>
                            <?php if (isset($moneyType['money_credit']) && $moneyType['money_credit'] != 0)  : ?>
                                <button class="btn btn-primary"><?= Yii::t('frontend','CREDIT');?></button>
                            <?php endif ?>
                            <?php if (isset($moneyType['money_other']) && $moneyType['money_other'] != 0)  : ?>
                                <button class="btn btn-primary"><?= Yii::t('frontend','ORDER');?></button>
                            <?php endif ?>
                        <?php endif; ?>
                    </td>
                </tr>
                 <tr class="something">
                    <td class="col-md-1"><?= Yii::t('frontend','Buy Point Use') ;?></td>
                    <td class="col-md-1"><?php echo ($Store['point_use']) ? number_format($Store['point_use']).'  '.'P' : '0'.' P' ;?></td>
                </tr>
            </table>
        </div>
    </div>
    <?php } ?>
    <div class="row">
        <div class="col-xs-12">
            <a href="<?php echo Url::base(TRUE).'/history/buyhistory'?>" title="予約履歴" class="btn btn-default btn-pink btn-empty">
                ◀<?php echo Yii::t('frontend','Back') ?>
            </a>
        </div>
    </div>
</div>
<div class="modal-container">
    <div class="modal fade shared-modal" id="myModal" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-body">
              <div id="scroll" style="max-height: 290px; overflow: hidden; overflow-y: auto;">
              <table style="width:100%" id="myTable" class="table table-bordered">
                  <tr>
                      <th><?= Yii::t('frontend','Product Name')?></th>
                      <th><?= Yii::t('frontend','Quantity')?></th>
                      <th><?= Yii::t('frontend','Total Money')?></th>
                  </tr>
                  <?php  if ($detailOrder) { ?>
                  <?php foreach ($detailOrder as $Order) : ?>
                  <tr class="view_scroll" style="border-right: 0">
                      <td ><?php echo $Order['product_display_name']; ?>
                      <?php if ($Order['gift_flg'] == 1) { ?>
                          <i class="fa fa-gift" aria-hidden="true" style="color:hotpink;font-size:2em"></i>
                      <?php } ?>
                      </td>
                      <td><?php echo $Order['quantity']; ?></td>
                      <td><?php echo $Order['tax_display_method'] == 01 ? ($Order['product_total'] + $Order['product_tax_money']) : $Order['product_total']; ?></td>
                  </tr>
                  <?php endforeach; ?>
                  <?php } ?>
              </table>
              </div>
          </div>
        </div>
        <button type="button" class="btn-close" data-dismiss="modal" name="button">
            <i class="fa fa-times"></i>
        </button>
      </div>
    </div>
</div>
