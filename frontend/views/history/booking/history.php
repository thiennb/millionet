<?php use yii\widgets\ListView; ?>
<?php echo $this->render('part/header.php'); ?>
<?php $this->title = '予約履歴' ?>
<div class="clearfix"></div>
<div class="container-fluid booking-history shared-common-container">
    <div class="shared-section-title">
        予約情報一覧
    </div>
    <div class="clearfix"></div>
    <!-- SEARCH BOX -->
    <?php echo $this->render('part/searchform-new', [
        'model' => $model,
        'stores' => $stores,
        'booking_statuses' => $booking_statuses
    ]) ?>
    <!-- END SEARCH BOX -->
    <div class="clearfix"></div>
    <div>
        <div class="shared-toppad-lg shared-section-title col-xs-6">
            検索結果
        </div>
        <div class="col-xs-6">
            <!-- <?php echo $this->render('part/pager', [
                'total_page' => $total_page
            ]) ?> -->
        </div>
    </div>
    <div class="clearfix"></div>
    <?php if ($items->totalCount === 0) : ?>
        <?php echo $this->render('part/search-not-found') ?>
    <?php else : ?>
        <div class="row shared-toppad-lg"></div>
        <?php echo ListView::widget([
            'dataProvider' => $items,
            'summary' => false,
            'summaryOptions' => ['class' => 'common-clr-fl-right'],
            'pager' => [
                'class' => 'common\components\CustomizeLinkPager',
                'options' => ['class' => 'pagination '],
            ],
            'layout' => "{pager}{summary}{items}",
            'itemView' => 'part/search-item',
            'viewParams' => [
                'product_coupons'=> $product_coupons,
                'customer_store' => $customer_store,
                'point_settings' => $point_settings
            ],
        ]); ?>
    <?php endif ?>
</div>
<div class="clearfix"></div>
<div class="row shared-toppad-lg"></div>
<script>
    (function ($) {
        $('.date').each(function () {
            $(this).children('.input-group-addon').click(function () {
                $(this).datepicker('show');
            });
        });
    })(jQuery);
</script>
