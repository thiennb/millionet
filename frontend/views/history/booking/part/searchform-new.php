<?php
use yii\bootstrap\ActiveForm;
use yii\jui\DatePicker;
use yii\helpers\Url;
?>
<div class="row shared-toppad-lg"></div>
<?php $form = ActiveForm::begin([
       'method' => 'get',
       'enableClientValidation' => false,
       'action' => Url::home() . Yii::$app->controller->id . '/bookinghistory',
       'fieldClass' => 'justinvoelker\awesomebootstrapcheckbox\ActiveField',
       'options' => [
           'class' => 'history-search-form'
       ]
   ]); ?>
   <div class="col-md-9">
       <div class="row">
           <div class="col-md-2 label-margin">
               <?= Yii::t('backend', 'Store By') ?>
           </div>
           <div class="col-md-10">
               <?php echo $form->field($model, 'store_id')->label(false)->dropDownList($stores) ?>
           </div>
       </div>
       <div class="row shared-toppad-lg"></div>
       <div class="row">
           <div class="col-md-2 label-margin">
               <?= Yii::t('backend', 'Purchase Date') ?>
           </div>
           <div class="col-md-4 col-md-4-5" >
               <div class="form-group">
                   <div class='input-group date'>
                   <?= $form->field($model, 'booking_date_from', [
                       'options' => ['class' => 'form-control'],
                       'template' => '<div class="clear-padding">{input}{error}</div>'
                   ])->widget(DatePicker::classname(), [
                       'language' => 'ja',
                       'dateFormat' => 'yyyy/MM/dd',
                       'clientOptions' => [
                           "changeMonth" => true,
                           "changeYear" => true,
                           "yearRange" => "1900:+0"
                       ],
                   ])->textInput(['maxlength' => 10,
                       'value' => (!empty(Yii::$app->request->get('start_date')) ?Yii::$app->request->get('start_date') : ''),
                       'class' => 'form-control'
                   ])
               ?>
                       <span class="input-group-addon">
                           <span class="glyphicon glyphicon-calendar"></span>
                       </span>
                   </div>
               </div>
           </div>
           <div class="col-md-1 label-margin label-center between-date"> ～ </div>
           <div class="col-md-4 col-md-4-5">
               <div class="form-group">
                   <div class='input-group date'>
                   <?= $form->field($model, 'booking_date_to', [
                       'options' => ['class' => 'form-control'],
                       'template' => '<div class="clear-padding">{input}{error}</div>'
                   ])->widget(DatePicker::classname(), [
                       'language' => 'ja',
                       'dateFormat' => 'yyyy/MM/dd',
                       'clientOptions' => [
                           "changeMonth" => true,
                           "changeYear" => true,
                           "yearRange" => "1900:+0"
                       ],
                   ])->textInput(['maxlength' => 10,
                       'value' => (!empty(Yii::$app->request->get('end_date')) ?Yii::$app->request->get('end_date') : ''),
                       'class' => 'form-control'
                   ])
               ?>
                       <span class="input-group-addon">
                           <span class="glyphicon glyphicon-calendar"></span>
                       </span>
                   </div>
               </div>
           </div>
       </div>
       <!-- END DATE SELECTION -->
       <div class="clearfix"></div>
       <!-- STATUS SELECTION -->
       <div class="row">
           <div class="col-md-2 label-margin">
               予約状況:
           </div>
           <div class="col-md-10">
               <?= $form->field($model, 'list_status')->checkboxList($booking_statuses, [1,1,1,1])->label(false) ?>
           </div>
       </div>
       <!-- END STATUS SELECTION -->
   </div>
   <div class="col-md-3">
        <div class="col-xs-12 text-center" style="background: whitesmoke;padding-top: 55px;height: 150px;">
            <button type="submit" class="common-button-submit btn_search_byhistory">
                <i class="fa fa-search"></i> 検索
            </button>
       </div>
   </div>
<?php ActiveForm::end() ?>
