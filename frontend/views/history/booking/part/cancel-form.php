<?php
    use yii\widgets\ActiveForm;
    use common\components\Util;
?>
<div class="modal fade in" id="booking-cancel" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
          <?php if (!$rejectable) : ?>
              キャンセル期限をすぎているため、<br/>
              直接、店舗へご確認くださ<br/>
              <br/>
              店舗電話番号:<br/>
              <div class="text-center">
                  <h2><?php echo Util::formatPhoneNumber($item->storeMaster->tel) ?></h2>
              </div>
          <?php else : ?>
              <h3>
                  <?php echo Yii::t('frontend', 'Are you sure to cancel this reservation?') ?>
              </h3>
          <?php endif ?>
      </div>
      <div class="modal-footer">
          <button type="button" class="btn btn-default btn-pink btn-empty pull-left" data-dismiss="modal">◀戻る</button>
          <?php if ($rejectable) : ?>
              <?php ActiveForm::begin([
                  'method' => 'POST',
                  'options'=> [
                      'class' => 'pull-right'
                  ]
                  ]) ?>

                  <button type="submit" class="btn btn-default btn-pink btn-empty" name="action" value="cancel-reservation">キャンセルを確定する</button>
              <?php ActiveForm::end() ?>
          <?php endif ?>
      </div>
    </div>
  </div>
</div>
