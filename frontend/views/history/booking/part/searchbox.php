<?php
use yii\bootstrap\ActiveForm;
use yii\jui\DatePicker;
?>
<div class="shared-toppad-lg shared-box search-box">
    <?php
    $form = ActiveForm::begin([
                'method' => 'get',
                'enableClientValidation' => false,
                'fieldClass' => 'justinvoelker\awesomebootstrapcheckbox\ActiveField'
    ]);
    ?>
    <div class="clearfix"></div>
    <div class="col-md-9" style="padding: 16px;">
        <!-- STORE LIST -->
        <div class="form-group">
            <label class="col-md-2 search-label shared-text-right" for="">店舗: </label>
            <div class="col-md-4" style="padding: 0">
                <?php echo $form->field($model, 'store_id')->label(false)->dropDownList($stores) ?>
            </div>
        </div>
        <!-- END STORE LIST -->
        <div class="clearfix"></div>
        <div class="shared-toppad-lg"></div>
        <!-- DATE SELECTION -->
        <div class="form-group">
            <label class="col-md-2 search-label shared-text-right" for="">予約日: </label>
            <div class='input-group date col-md-4' style="float:left">
                <?php
                echo $form->field($model, 'booking_date_from', [
                    'options' => ['class' => 'form-control'],
                    'template' => '<div class="shared-date-picker">{input}</div>'
                ])->widget(DatePicker::classname(), [
                    'language' => 'ja',
                    'dateFormat' => 'yyyy/MM/dd',
                    'clientOptions' => [
                        "changeMonth" => true,
                        "changeYear" => true,
                        "yearRange" => "1900:+0"
                    ],
                ])->textInput([
                    'maxlength' => 10,
                    'class' => 'form-control'
                ])
                ?>
                <span class="input-group-addon">
                    <span class="glyphicon glyphicon-calendar"></span>
                </span>
            </div>
            <div class="col-md-1 between-date">~</div>
            <div class='input-group date col-md-4'>
                <?=
                $form->field($model, 'booking_date_to', [
                    'options' => ['class' => 'form-control'],
                    'template' => '<div class="shared-date-picker">{input}</div>'
                ])->widget(DatePicker::classname(), [
                    'language' => 'ja',
                    'dateFormat' => 'yyyy/MM/dd',
                    'clientOptions' => [
                        "changeMonth" => true,
                        "changeYear" => true,
                        "yearRange" => "1900:+0"
                    ],
                ])->textInput([
                    'maxlength' => 10,
                    'class' => 'form-control'
                ])
                ?>
                <span class="input-group-addon">
                    <span class="glyphicon glyphicon-calendar"></span>
                </span>
            </div>
        </div>
        <!-- END DATE SELECTION -->
        <div class="clearfix"></div>
        <!-- STATUS SELECTION -->
        <div class="form-group">
            <label class="col-md-2 search-label shared-text-right" for="">予約状況: </label>
            <?= $form->field($model, 'list_status')->checkboxList($booking_statuses, [1,1,1,1])->label(false) ?>
        </div>
        <!-- END STATUS SELECTION -->
    </div>
    <div class="col-md-3 right-search-btn">
        <button type="submit" class="btn btn-default btn-search btn-pink btn-empty">
            <i class="fa fa-search"></i> 検索
        </button>
    </div>
    <div class="clearfix"></div>
    <?php ActiveForm::end() ?>
</div>
