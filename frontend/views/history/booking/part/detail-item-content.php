<?php use yii\helpers\Html; use common\models\BookingBusiness; ?>
<tr>
    <th class="col-xs-3">
        <?php echo $key ?>
    </th>
    <td class="col-xs-7">
        <?php if ($mode === 'coupon') : ?>
            <?php $categories = $item->listCategories()->all() ?>
            <?php if (count($categories)) : ?>
                <ul class="list-inline shared-category-list">
                <?php foreach ($categories as $category): ?>
                    <li><?php echo Html::encode($category->name) ?></li>
                <?php endforeach; ?>
                </ul>
            <?php endif ?>
        <?php elseif ($mode === 'product' && $item->category) : ?>
            <ul class="list-inline shared-category-list">
                <li><?php echo Html::encode($item->category->name) ?></li>
            </ul>
        <?php endif ?>
        <?php if ($mode === 'coupon') : ?>
            <?php echo Html::encode($item['title']) ?>
        <?php elseif ($mode === 'option') : ?>
            <?php echo Html::encode($item['option_name']) ?> : <?php echo Html::encode($item['name']) ?>
        <?php elseif ($mode === 'staff') : ?>
            <?php echo Html::encode($staff->name) ?>
        <?php else : ?>
            <?php echo Html::encode($item['name']) ?>
        <?php endif ?>
    </td>
    <td>
        <?php if ($total_price < 0) : ?>
            - ¥<?php echo BookingBusiness::formatMoney(abs($total_price)) ?>
        <?php else : ?>
            ¥<?php echo BookingBusiness::formatMoney($total_price) ?>
        <?php endif ?>
    </td>
</tr>
