<?php
use yii\helpers\Html;
use common\models\BookingBusiness;
use yii\helpers\Url;
//use Carbon\Carbon;

/* @var $item \common\models\Booking */
$booking_statuses = common\components\Constants::LIST_BOOKING_STATUS;
$item = $model;
$product_coupons = $product_coupons[$index];
$customer_store  = isset($customer_store[$item->store_id]) ? $customer_store[$item->store_id] : null;
?>
<div class="clearfix"></div>
<div class="shared-box search-item" style="<?php if ($index === 0) echo 'margin-top: 0;' ?>">
    <header role="heading">
        <div class="store-name">
            <?php echo HTML::encode($item->storeMaster->name) ?>

            <div class="fake-btn pull-right booking-status-<?php echo $item->status ?>">
                <?php echo $booking_statuses[$item->status] ?>
            </div>
        </div>
    </header>
    <div class="clearfix"></div>
    <div class="row" role="article">
        <div class="col-sm-8">
            <span class="item-key">予約日時:</span> <?php echo strtr($item->booking_date, '-', '/') ?> <?php echo $item->start_time ?>
        </div>
        <div class="col-sm-4">
            <span class="item-key">お支払い予定金額:</span> <span class="item-price">¥<?php echo BookingBusiness::formatMoney($item->booking_total) ?></span>
        </div>
        <div class="clearfix"></div>
        <div class="col-sm-8">
          <span class="item-key">約商品:</span>
          <ul class="list-group list-no-border">
              <?php foreach ($product_coupons as $product_coupon) : $mode = key_exists('display_condition', $product_coupon->attributes) ? 'coupon' : 'product' ?>
                  <?php if (key_exists('display_condition', $product_coupon->attributes)){
                      $mode = 'coupon';
                  }
                  elseif ($product_coupon->assign_fee_flg === '1') {
                      $mode = 'product'; // old : 'staff';
                  }
                  else {
                      $mode = 'product';
                  }
                   ?>
                  <li class="list-group-item">
                      <?php echo $this->render('search-item-content', [
                          'mode' => $mode,
                          'item' => $product_coupon,
                          'staff'=> $item->staff
                      ]) ?>
                  </li>
              <?php endforeach ?>
          </ul>
        </div>
        <div class="col-sm-4">
          <span class="item-key">予定付与ポイント:</span> <?php echo (isset($point_settings[$item->store_id]) && $customer_store) ?  BookingBusiness::formatPoint(BookingBusiness::getPointFromMoney($item->booking_total, $customer_store, $point_settings[$item->store_id])) : 0 ?>P
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="row shared-toppad-lg">
        <div class="col-xs-12">
            <a href="<?php echo Url::to(['bookingdetail', 'booking_id' => $item->id, 'page' => Yii::$app->request->get('page')]) ?>" class="btn btn-default pull-right">
                詳細を見る
            </a>
        </div>
    </div>
</div>
