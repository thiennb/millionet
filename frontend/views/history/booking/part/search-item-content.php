<?php use yii\helpers\Html; ?>
<?php if ($mode === 'coupon') : ?>
    <?php echo Html::encode($item['title']) ?>
<?php elseif ($mode === 'staff') : ?>
    <?php if ($staff) echo Html::encode($staff->name); ?>
<?php elseif ($item['option_name']) : ?>
    <?php echo Html::encode($item['option_name']) ?> : <?php echo Html::encode($item['name']) ?>
<?php else : ?>
    <?php echo Html::encode($item['name']) ?>
<?php endif ?>
