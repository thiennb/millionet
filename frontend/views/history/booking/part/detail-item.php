<?php
    use yii\helpers\Html;
    use common\models\BookingBusiness;
    use common\components\Util;
    use common\models\BookingSeat;
?>
<?php if (!$item) : ?>
    <div class="clearfix"></div>
    <div class="row shared-panel-error">
        ご予約情報が見つかりませんでした。
    </div>
<?php else : ?>
    <?php $booking_seats = BookingSeat::findFrontEnd()->where(['booking_id' => $item->id])->all() ?>
    <div class="clearfix"></div>
    <div class="row shared-toppad-lg"></div>
    <!-- STORE INFORMATION -->
    <table class="table table-responsive table-bordered">
        <tbody>
            <tr>
                <th class="col-xs-3">
                    店舗情報
                </th>
                <td>
                    <h4><?php echo Html::encode($item->storeMaster->name) ?></h4>
                    <p><?php echo Html::encode($item->storeMaster->address) ?></p>
                    <div class="col-xs-12">
                        <iframe width="100%"
                                height="300"
                                frameborder="0"
                                scrolling="no"
                                marginheight="0"
                                marginwidth="0"
                                src="https://maps.google.com/maps?f=q&source=s_q&hl=en&geocode=&q=<?php echo str_replace(' ', '+', $item->storeMaster->address) ?>&ie=UTF8&hq=&t=m&z=14&ll=<?php echo $item->storeMaster->latitude ?>,<?php echo $item->storeMaster->longitude ?>&output=embed">
                        </iframe>
                    </div>
                </td>
            </tr>

            <tr>
                <th class="col-xs-3">
                    店舗連絡先
                </th>
                <td>
                    <?php echo Util::formatPhoneNumber($item->storeMaster->tel) ?>
                </td>
            </tr>
        </tbody>
    </table>
    <!-- END STORE INFORMATION -->
    <div class="row shared-toppad-lg"></div>
    <!-- BOOKING INFORMATION -->
    <table class="table table-responsive table-bordered">
        <tbody>
            <?php
                $coupon_idx = 0;
                $product_idx = 0;
            ?>
            <?php foreach ($product_coupons as $product_coupon): ?>
                <?php

                    $coupon_key = Yii::t('frontend', 'Coupon');
                    $product_key = Yii::t('frontend', 'Product');
                    $staff_key = Yii::t('frontend', 'Staff');
                    if (key_exists('display_condition', $product_coupon->attributes)) {
                        $mode = 'coupon';
                        $key  = $coupon_key . ' ' . ($coupon_idx+=1);
                    }
                    // products
                    elseif ($product_coupon['option_name']) {
                        $mode = 'option';
                        $key  = 'オプションメニュー';
                    }
                    elseif ($product_coupon['assign_fee_flg'] == '1') {
                        $mode = 'staff';
                        $key  = $staff_key;
                    }
                    else {
                        $mode = 'product';
                        $key  = $product_key . ' ' . ($product_idx+=1);
                    }
                ?>
                <?php echo $this->render('detail-item-content', [
                    'item' => $product_coupon,
                    'mode' => $mode,
                    'key'  => $key,
                    'staff'=> $item->staff,
                    'total_price'=> (int)$product_coupon['total_price'],
                    'seats' => $booking_seats
                    ]) ?>
            <?php endforeach; ?>
            <?php foreach ($booking_seats as $booking_seat): ?>
                <?php if ($booking_seat->seat): ?>
                    <tr>
                        <th>席タイプ</th>
                        <td colspan="2"><?php echo $booking_seat->seat->name ?></td>
                    </tr>
                <?php endif; ?>
            <?php endforeach; ?>

            <tr>
                <th>
                    ご利用ポイント
                </th>
                <td colspan="2">
                    <?php echo BookingBusiness::formatPoint($item->point_use) ?>P
                </td>
            </tr>
            <tr>
                <th>
                    お支払い金額
                </th>
                <td colspan="2">
                    ¥<?php echo BookingBusiness::formatMoney($item->booking_total) ?>
                    (<?php echo BookingBusiness::formatPoint($granted_points) ?>P 付与)
                </td>
            </tr>
            <tr>
                <th>
                    キャンセル期限
                </th>
                <td colspan="2">
                    <?php echo $item->cancelOffset->format('Y/m/d H:i') ?>時まで
                </td>
            </tr>
        </tbody>
    </table>
    <!-- END BOOKING INFORMATION -->
<?php endif ?>
