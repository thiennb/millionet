<?php use yii\helpers\Url; ?>
<?php $current_page = Yii::$app->request->get('page') ?>
<?php if (!$current_page) $current_page = 1; ?>
<?php if ($total_page == 0) $current_page = 0; ?>
<?php if ($current_page > $total_page) $current_page = $total_page ?>
<?php $url_parts = Yii::$app->request->get(); $url_parts['page'] = $current_page; array_unshift($url_parts, ''); ?>
<?php $next_page_parts = $url_parts; $next_page_parts['page'] += 1 ?>
<?php $prev_page_parts = $url_parts; $prev_page_parts['page'] -= 1 ?>
<ul class="list-inline shared-pagination">
  <?php if ($current_page > 1) : ?>
      <li class="prev-page">
          <a href="<?php echo Url::to($prev_page_parts) ?>" title="前へ">◀前へ</a>
      </li>
  <?php endif ?>
  <li class="current-page"><?php echo $current_page ?>/<?php echo $total_page ?> ページ</li>
  <?php if ($current_page < $total_page) : ?>
      <li class="next-page">
          <a href="<?php echo Url::to($next_page_parts) ?>" title="次へ">次へ▶</a>
      </li>
  <?php endif ?>
</ul>
