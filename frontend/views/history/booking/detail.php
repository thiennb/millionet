<?php
    use yii\helpers\Url;
    use common\models\Booking;
?>
<?php echo $this->render('part/header.php'); ?>
<?php $this->title = '予約履歴(詳細)' ?>
<div class="clearfix"></div>
<div class="container-fluid booking-history" style="max-width: 1024px; margin: 0; margin-top: 10px">
    <?php if ($cancelled) : ?>
        <div class="shared-alert">
            <?php echo Yii::t('frontend', 'This reservation has been successfully cancelled.') ?>
        </div>
    <?php endif ?>
    <div class="clearfix"></div>
    <div class="shared-section-title">
        予約情報一覧 詳細
    </div>
    <div class="clearfix"></div>
    <?php echo $this->render('part/detail-item', [
        'item' => $item,
        'product_coupons' => $product_coupons,
        'granted_points' => $granted_points
        ]) ?>

    <div class="clearfix"></div>
    <div class="row">
        <div class="col-xs-6">
            <a href="<?php echo Url::to(['bookinghistory']) ?>" title="予約履歴" class="btn btn-default btn-pink btn-empty">
                ◀戻る
            </a>
        </div>
        <?php if ($show_cancel_button) : ?>
            <div class="col-xs-6">
                <button type="button" class="btn btn-default btn-pink btn-empty pull-right" onclick="javascript:$('#booking-cancel').modal();">
                    予約をキャンセルする
                </button>
            </div>
        <?php endif ?>
    </div>
    <?php if ($show_cancel_button) : ?>
        <?php echo $this->render('part/cancel-form', ['item' => $item, 'rejectable' => $rejectable]); ?>
    <?php endif ?>
</div>
<div class="row shared-toppad-lg"></div>
