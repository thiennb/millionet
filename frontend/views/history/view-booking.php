<?php

use yii\helpers\Html;
use yii\bootstrap\Modal;

/* @var $this yii\web\View */
/* @var $model common\models\Booking */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Bookings', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="booking-view">

    <table class="table-store-detail table table-striped table-bordered detail-view">
        <tbody>
            <tr>
                <th><?= Yii::t('backend', 'Store Information') ?></th>
                <td>
                    <div class='row row-inline'>
                        <div class='col-mid-3'>店舗名:</div>
                        <div class='col-mid-3'>
                            <?= Html::a($modelStore->name, ['/store-detail', 'id' => $modelStore->id]) ?>
                        </div>
                    </div> 
                    <div class='row row-inline table-bordered'>
                        <div class='col-mid-3'>店舗住所:</div>
                        <div class='col-mid-3'> 
                            <?= $modelStore->address ?>
                        </div>
                    </div>
                    <?php if (!empty($modelStore->longitude) && !empty($modelStore->latitude)) { ?>
                        <div class="row row-inline table-bordered">
                            <iframe width="425" 
                                    height="350" 
                                    frameborder="0" 
                                    scrolling="no" 
                                    marginheight="0" 
                                    marginwidth="0" 
                                    src="https://maps.google.com/maps?f=q&source=s_q&hl=en&geocode=&q=<?= str_replace(" ", "+", $modelStore->address) ?>&ie=UTF8&hq=&t=m&z=14&ll=<?= $modelStore->latitude ?>,<?= $modelStore->longitude ?>&output=embed">
                            </iframe>
                        </div>
                    <?php } ?>
                </td>
            </tr>
            <tr>
                <th><?= Yii::t('backend', 'Telephone') ?></th>
                <td> <?= $modelStore->tel ?> </td>
            </tr>

        </tbody>
    </table>

    <table class="table table-striped table-bordered detail-view">
        <tbody>
            <?php
            if (!empty($model->bookingProduct)) {
                $count = 0;
                foreach ($model->bookingProduct as $key => $product) {
                    if (!empty($product->product)) {
                        ?>
                        <tr>
                            <th> <?= Yii::t('backend', 'Product') . ( ++$count) ?></th>
                            <td>
                                <div class="row" ><span class="btn common-button-submit"><?= $product->product->category->name ?></span></div>
                                <div class="row" ><?= $product->product->name ?></div>

                            </td>
                            <td>
                                <?= '¥' . $product->unit_price ?>
                            </td>
                        </tr>
                        <?php
                    }
                }
            }
            ?>
            <?php
            if (!empty($model->bookingProduct)) {
                $count = 0;
                foreach ($model->bookingProduct as $key => $product) {
                    if (!empty($product->option)) {
                        ?>
                        <tr>
                            <th> <?= Yii::t('backend', 'Option') . ( ++$count) ?></th>
                            <td>
                                <div class="row" ><?= $product->option->name . ": " . $product->product->name ?></div>

                            </td>
                            <td>
                                <?= '¥' . $product->unit_price ?>
                            </td>
                        </tr>
                        <?php
                    }
                }
            }
            ?>

            <?php
            //02 : スタッフ選択 (Select staff) 
            if ($modelStore->booking_resources == common\components\Constants::SELECT_STAFF) {
                if (!empty($model->staff)) {
                    ?>
                    <tr>
                        <th> <?= Yii::t('backend', 'Staff') ?></th>
                        <td>
                            <?= $model->staff->name ?>
                        </td>
                        <?php
                        if (isset($model->staff->assignProduct)) {
                            if ($model->staff->assignProduct->assign_fee_flag) {
                                ?>
                                <td>
                                    <?= '¥' . $model->staff->assignProduct->unit_price ?>
                                </td>
                                <?php
                            }
                        }
                        ?>
                    </tr>
                    <?php
                }
            //01 : 席タイプ選択 (Select seat type)   
            } else if ($modelStore->booking_resources == common\components\Constants::SELECT_SEAT_TYPE) {
                if (!empty($model->seat)) {
                    ?>
                    <tr>
                        <th> <?= Yii::t('backend', 'Seat') ?></th>
                        <td>
                            <?= $model->seat->name ?>
                        </td>
                    </tr>
                    <?php
                }
            }
            ?>
            <tr>
                <th><?= Yii::t('backend', 'Point Use') ?></th>
                <td> <?= empty($model->point_use) ? "0P" : $model->point_use . "P" ?> </td>
            </tr>
            <tr>
                <th><?= Yii::t('backend', 'Booking Total') ?></th>
                <td> <span> 
                        <?= empty($model->booking_total) ? "¥0" : "¥" . $model->booking_total ?>  
                    </span> 
                    <?php
                    $countPoint = 0;
                    if (!empty($model->bookingCoupon)) {
                        foreach ($model->bookingCoupon as $coupon) {
                            $countPoint += isset($coupon->coupon->grant_point) ? $coupon->coupon->grant_point : 0;
                        }
                    }
                    ?>
                    <span>
                        <?= "(" . $countPoint . "P 付与)" ?>  
                    </span>
                </td>

            </tr>

            <tr>
                <th><?= Yii::t('backend', 'Cancel deadline') ?></th>
                <td>  <?= $model->end_time ?></td>
            </tr>

        </tbody>
    </table>
    <div class="form-group">
        <?= Html::a(Yii::t('backend', 'Return'), ['index'], ['class' => 'btn common-button-default btn-default']) ?>
        <?php
        //01': 仮予約, '02': 予約中
        if ($model->status === '01' || $model->status === '02') {
            echo Html::button(Yii::t('backend', 'Cancel booking'), [
                'class' => 'btn common-button-submit',
                'data' => [
                    'toggle' => 'modal',
                    'target' => '#cancelModal',
                ],
            ]);
        }
        ?>
    </div> 

    <?php
    Modal::begin([
        'id' => 'cancelModal',
        'size' => 'SIZE_LARGE',
        'header' => '<b>' . Yii::t('backend', 'Confirm Store') . '</b>',
        'footer' =>
        ((false) ? Html::button(Yii::t('backend', 'Close'), ['class' => 'btn common-button-default', 'data-dismiss' => "modal", 'id' => 'btn-close']) : Html::button(Yii::t('backend', 'Close'), ['class' => 'btn common-button-default', 'data-dismiss' => "modal", 'id' => 'btn-close']) . Html::a(Yii::t('backend', 'Save'), ['cancel-booking', 'id' => $model->id], ['class' => 'btn common-button-submit'])),
        'footerOptions' => ['class' => 'modal-footer text-center'],
    ]);
    ?>
    <?php
    if (false) {
        echo Yii::t('backend', 'vi qua ki han cancel, xin call den cua hang de dc cancel.');
        ?>
        <div class="row">
            店舗電話番号:
        </div>
        <div class="row">
            <h2><?= $modelStore->tel ?></h2>  
        </div>

        <?php
    } else {
        echo Yii::t('backend', 'BAN CHAC CHAN MUON CANCEL BOOKING?');
    }
    ?>
    <?php
    Modal::end();
    ?>
</div>
