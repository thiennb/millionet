<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\grid\GridView;
use yii\widgets\LinkPager;
use yii\helpers\Url;
use yii\jui\DatePicker;
/* @var $this yii\web\View */
/* @var $searchModel common\models\MasterNoticeMemberSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->title = 'チャージ履歴(詳細)';
?>
<?php $this->registerCSSFile('@web/css/history.css',['position' => \yii\web\View::POS_HEAD]);?>
<style>
    tr.limit{ display:none;
}
a.btn.btn-primary{
    background-color: white;
    color :#FF69B4;
}
.common-button-submit:hover {
    background-color: #FF69B4;
}
#loadMore:hover{
    cursor: pointer;
}
</style>
<script>
$(document).ready(function () {
    size_li = $("tr.limit").size();
    limit = 10;
    $('tr.limit:lt('+limit+')').show();
    $('#loadMore').click(function () {
        limit= (limit+10 <= size_li) ? limit+10 : size_li;
    $('tr.limit:lt('+limit+')').show();
    });
});
</script>
<div class="clearfix"></div>
<div class="shared-common-container">
    <!--<div class="shared-section-title">
        <?= $this->title ?>
    </div>-->
    <div class="shared-section-title">
        <?= Yii::t('frontend', 'Charge Title') ?>
    </div>
    <div class="clearfix"></div>
    <div class="col-xs-12 black-bordered-box">
        <div class="row" style="margin:5px">
            <?php echo $dataProvider ? $dataProvider['0']['name'] : '' ; ?>
        </div>

        <div class="row" style="margin:5% 0 5% 0;text-align:center; font-size:150%; word-spacing: 15px;">
            <?= Yii::t('frontend','Balance') ?> <span style="font-size: 1.7em;color:<?= (number_format($chargeBalance['charge_balance']) != 0 ) ? '#FF69B4' : 'black';?>;font-weight: bold"><?php echo number_format(($chargeBalance) ? $chargeBalance['charge_balance'] : 0  ) ?></span> <?= Yii::t('frontend','Money Japanese') ?>
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="row shared-toppad-lg"></div>
    <div class="row">
        <div class="col-xs-12">
            <table class="table table-bordered table-responsive">
                <thead>
                    <tr>
                        <th> <?= Yii::t('frontend','Date') ?></th>
                        <th> <?= Yii::t('frontend','Content') ?></th>
                        <th><?= Yii::t('frontend','Balance Type') ?></th>
                    </tr>
                </thead>
                <tbody>
                <?php if ($dataProvider) { ?>
                <?php foreach ($dataProvider as $result) : ?>
                    <tr class="limit">
                        <td><?= $result['process_date'].' : '. $result['process_time'] ?></td>
                        <td><?= ($result['process_type'] == 0) ? Yii::t('frontend','Plus Charge') : Yii::t('frontend','Sub Charge')  ?></td>
                        <td><?= ($result['process_type'] == 0) ? '+'.$result['process_money'] .' '.Yii::t('frontend','Payment'): '-'.$result['process_money'] .''.Yii::t('frontend','Payment');  ?></td>
                    </tr>
                <?php endforeach ; ?>
                <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
    <div class="row shared-toppad-lg"></div>
    <div class="row">
        <div class="col-xs-6">
            <a href="<?php echo Url::base(TRUE).'/history/chargehistory'?>" style="max-width: 200px" class="btn btn-pink btn-empty btn-default pull-left">◀<?= Yii::t('frontend','Back'); ?></a>
        </div>
        <div class="col-xs-6">
            <div class="pull-right" id ="show_more">
                 <p style="color:#3c8dbc" class="pull-right" id ="loadMore" >▶<?= yii::t('frontend','View More') ?></p>
            </div>
        </div>
    </div>
</div>
<div class="clearfix"></div>
