<?php

use yii\helpers\Html;
use yii\helpers\Url;
use common\models\BookingSearch;
?> 
<style>

</style>
<?php $customer_id = \common\models\MasterCustomer::findOne(['user_id' => \Yii::$app->user->identity->id])->id;
//echo "<pre>";var_dump($customer_id);die;
 ?>
<div class="col-md-12">
    <div class="row" style="border: 2px solid #8c8989;margin-top:1%;padding: 1.5em;">
        <div class="form-group ">
            <?= Yii::t('backend', 'Name Store') ?> : <span> <?php echo  $model->store_name; ?></span>
            <div class="pull-right">
                    <button style="background:#FF69B4" class="btn btn-large"><span style="color:white;">
                    <?php if ($model->status == 0) {
                        echo Yii::t('frontend','Status Pending');
                        }else if ($model->status == 1) {
                            echo Yii::t('frontend','Status Booking');
                        } else if ($model->status == 2) {
                            echo Yii::t('frontend','Status End');
                        }else if ($model->status ==3) {
                            echo Yii::t('frontend','Status Cancel');
                        }else {
                            echo Yii::t('frontend','Status Cancel');
                        }
                    ?>    
                    </span>
                    </button>
            </div>
        </div>
        <div class="related-post">
        </div>
        <div class="form-group">
            <div class="row">
                <div class="col-md-4">
                    <?= Yii::t('frontend', 'Booking Date') ?> : <span><?php  echo  $model->booking_date.'  '.$model->start_time; ?></span>
                </div>
                <div class="col-md-4" style="margin-top:5%;margin-left:-33.5%">
                    <?= Yii::t('frontend', 'Product Booking') ?> : <span><?php  echo  $model->product_name;?></span><br>
                    <?php  echo   ($model->title) ? $model->title : '' ; ?><br>
                      <?php  echo  ($model->option_name) ? $model->option_name : '' ; ?>
                </div>
                <div class="col-md-4 pull-right">
                  <?php  $total_booking = $model->booking_total ? $model->booking_total :0; ?>
                    <span class="text"><?= Yii::t('frontend','Price Use') ?> : <span style="font-size: 1.7em;color:#FF69B4;font-weight: bold"><?php  echo '¥'. number_format($total_booking); ?></stpan>
                </div>
                <div class="col-md-4 pull-right" id ="right" style="margin:2.5% 0 0 18%">
                    <span class="text"><?= Yii::t('frontend','Point Booking')?> :
                        <?php  echo ($model->point_conversion) ? number_format($model->point_conversion).'P': 0 .' P'; ?>
                </div>
                <div class="col-md-2"  style="padding-left: -8em;">
                </div>
            </div>
        </div>
         <div class="form-group pull-right ">
            <a href="<?php   echo Url::base(TRUE) . '/history/bookingdetail?id='.$model->product_booking_id ?>">
                 <?= Html::submitButton(Yii::t('backend', '詳細を見る'), ['class' => 'btn common-button-submit', 'id' => 'btn-submit']) ?>
            </a>
        </div>
    </div>
</div>
 