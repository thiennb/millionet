<?php

use yii\helpers\Html;
use yii\helpers\Url;
?>
<div class="clearfix"></div>
<div class="col-md-12">
    <div class="row" style="border: 2px solid #8c8989;margin-top:1%;padding: 1.5em;">
        <div class="form-group ">
            <span><?php echo ($model->name) ? $model->name : ''; ?></span>
        </div>
        <div class="related-post">
        </div>
        <div class="form-group">
            <div class="row">
                <div class="col-md-5">
                    <?php $point =  $model->total_point ? $model->total_point : 0 ;  ?>
                    <?= Yii::t('frontend', 'Point Can Use') ?> :
                    <span class ="point" style="font-size: 1.7em;color:<?= ($point != 0) ? '#FF69B4' : 'black'; ?>;font-weight: bold"><?php echo number_format($point); ?></span>
                    <?= Yii::t('frontend','Point') ?>

                </div>
            </div>
        </div>
        <div class="form-group pull-right ">
            <span class='prev_bt'>▶</span><a href="<?php  echo Url::base(TRUE).'/history/pointdetail?store_code='.$model->store_code?>" style="color: #3d3d3d;"><?= Yii::t('frontend', 'View History Point') ?></a>
        </div>
    </div>
</div>
 <div class="clearfix"></div>
