<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\grid\GridView;
use yii\widgets\LinkPager;
use yii\helpers\Url;
use yii\jui\DatePicker;
/* @var $this yii\web\View */
/* @var $searchModel common\models\MasterNoticeMemberSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->title = Yii::t('frontend', 'Point Title Detail');
$this->registerCSSFile('@web/css/history.css',['position' => \yii\web\View::POS_HEAD]);
?>
<style>
tr.limit{ display:none;
}
a.btn.btn-primary{
    background-color: white;
    color :#FF69B4;
}
.common-button-submit:hover {
    background-color: #FF69B4;
}
#loadMore:hover{
    cursor: pointer;
}
</style>
<script>
$(document).ready(function () {
    size_li = $("tr.limit").size();
    limit = 10;
    $('tr.limit:lt('+limit+')').show();
    $('#loadMore').click(function () {
        limit= (limit+10 <= size_li) ? limit+10 : size_li;
    $('tr.limit:lt('+limit+')').show();
    });
});
</script>
<div class="clearfix"></div>
<div class="shared-common-container">
    <div class="shared-section-title">
        <?php echo Yii::t('frontend', 'Point Title Detail') ?>
    </div>
    <div class="clearfix"></div>
    <div class="black-bordered-box">
        <?php echo ($storeInfo) ? $storeInfo['name'] : '' ; ?>
        <div class="clearfix">

        </div>
        <div class="row" style="margin:10% 0 10% 0;text-align:center; font-size:150%; word-spacing: 15px;">
            <?php $total_point = ($getPoint) ? $getPoint['total_point'] : '0' ; ?>
             <?= Yii::t('frontend', 'Point Can Use') ?> <span class ="point_total" style="font-size: 1.7em;color:<?= ($total_point != 0) ? '#FF69B4' : 'black'; ?>;font-weight: bold"><?php echo number_format($total_point); ?></span> <?= Yii::t('frontend', 'Point') ?>
        </div>
    </div>
    <div class="row shared-toppad-lg"></div>
    <div class="row shared-toppad-lg"></div>
    <table class="table table-bordered table-responsive">
        <thead>
            <th class="col-xs-3">
                <?= Yii::t('frontend', 'Date') ?>
            </th>
            <th class="col-xs-3">
                <?= Yii::t('frontend', 'Content') ?>
            </th>
            <th colspan="2" class="col-xs-6">
                <?= Yii::t('frontend', 'Type Point') ?>
            </th>
        </thead>
        <tbody>
            <?php if ($dataProvider) { ?>
            <?php foreach ($dataProvider as $result) : ?>
                <tr class="limit">
                    <td class="col-xs-4"><?= $result['process_date'].' : '.$result['process_time']?></td>
                    <td class="col-xs-4">
                        <?php
                            if ($result['process_type'] == 0 ) { echo Yii::t('frontend','Grant');
                            }else if($result['process_type'] == 1) { echo Yii::t('frontend','Use');
                            }else if ($result['process_type'] == 2) { echo Yii::t('frontend','Plus');
                            }else { echo Yii::t('frontend','Sub');  }
                        ?>
                    </td>
                    <td class="col-xs-2">
                        <?php echo ($result['process_type'] == 0 || $result['process_type'] == 2 ) ? '「+'.$result['process_point'].'」' : "" ?>
                    </td>
                    <td class="col-xs-2">
                        <?php echo ($result['process_type'] == 1 || $result['process_type'] == 3 ) ? '「-'.$result['process_point'].'」' : "" ?>
                    </td>
                </tr>
            <?php endforeach ; ?>
            <?php } ?>
        </tbody>
    </table>
    <div class="clearfix"></div>
    <div class="row shared-toppad-lg"></div>
    <div class="row shared-toppad-lg"></div>
    <div class="row">
        <div class="col-xs-6">
            <a href="<?php echo Url::base(TRUE).'/history/pointhistory'?>" class="btn btn-pink btn-empty btn-default pull-left">◀<?= Yii::t('frontend','Back'); ?></a>
        </div>
        <div class="col-xs-6" id ="show_more">
           <p style="color:#3c8dbc" class="pull-right" id ="loadMore" >▶<?= yii::t('frontend','View More') ?></p>
        </div>
    </div>
</div>
<div class="clearfix"></div>
