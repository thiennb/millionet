<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\grid\GridView;
use yii\widgets\LinkPager;
use yii\helpers\Url;
use yii\jui\DatePicker;
/* @var $this yii\web\View */
/* @var $searchModel common\models\MasterNoticeMemberSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->title = Yii::t('backend', '回数券情報詳細');
$this->registerCSSFile('@web/css/history.css',['position' => \yii\web\View::POS_HEAD]);
?>
<style type="text/css">
    tr.limit{ display:none;
}
a.btn.btn-primary{
    background-color: white;
    color :#FF69B4;
}
.common-button-submit:hover {
    background-color: #FF69B4;
}
#loadMore:hover{
    cursor: pointer;
}
</style>
<script>
$(document).ready(function () {
    size_li = $("tr.limit").size();
    limit = 10;
    $('tr.limit:lt('+limit+')').show();
    $('#loadMore').click(function () {
        limit= (limit+10 <= size_li) ? limit+10 : size_li;
    $('tr.limit:lt('+limit+')').show();
    });
});
</script>
<div class="clearfix"></div>
<div class="shared-common-container">
    <div class="shared-section-title">
        <?= Yii::t('backend', '回数券情報詳細') ?>
    </div>
    <div class="clearfix"></div>
    <div class="black-bordered-box">
        <?= ($storeName) ? $storeName['name'] : ''; ?>
        <div class="clearfix"></div>
        <?= ($ticketInfo) ? $ticketInfo['ticket_name'] : ''; ?>
        <div class="clearfix"></div>
        <div class="row" style="margin:10% 0 10% 0;text-align:center; font-size:150%; word-spacing: 15px;">
            <?= Yii::t('frontend','Ticket Plus') ?><span class ="point_total" style="font-size: 1.7em;color:<?= ($ticketInfo['ticket_balance'] != 0) ? '#FF69B4' : 'black'; ?>;font-weight: bold"><?php echo number_format($ticketInfo['ticket_balance']); ?></span><?= Yii::t('frontend','Ticket Count') ?>
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="row shared-toppad-lg"></div>
    <div class="row shared-toppad-lg"></div>
    <table class="table table-bordered table-responsive">
        <thead>
            <th class="col-xs-4">
                <?= Yii::t('frontend', 'Date') ?>
            </th>
            <th class="col-xs-4">
                <?= Yii::t('frontend', 'Content') ?>
            </th>
            <th class="col-xs-4">
                <?= Yii::t('frontend','Ticket Pay Ment') ?>
            </th>
        </thead>
        <tbody>
            <?php if ($dataProvider) { ?>
            <?php foreach ($dataProvider as $result) : ?>
                <tr class="limit">
                    <td class="col-xs-4"><?php echo $result['process_date'] ?></td>
                    <td class="col-xs-4"><?php  echo ($result['process_type'] == 0) ? "購入" : "使用" ;?></td>
                    <td class="col-xs-4"><?php  echo ($result['process_type'] == 1) ? '-' .$result['process_number'] : '+' .$result['process_number'] ?></td>
                </tr>
            <?php endforeach ; ?>
            <?php } ?>
        </tbody>
    </table>
    <div class="row shared-toppad-lg"></div>
    <div class="row">
        <div class="col-xs-6">
            <a href="<?php echo Url::base(TRUE).'/history/tickethistory'?>" style="max-width: 200px" class="btn btn-pink btn-empty btn-default pull-left">◀<?= Yii::t('frontend','Back'); ?></a>
        </div>
        <div class="col-xs-6">
            <div class="pull-right" id ="show_more">
                 <p style="color:#3c8dbc" class="pull-right" id ="loadMore" >▶<?= yii::t('frontend','View More') ?></p>
            </div>
        </div>
    </div>
</div>
<div class="clearfix"></div>
