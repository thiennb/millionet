<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\grid\GridView;
use yii\widgets\LinkPager;
use yii\helpers\Url;
use yii\jui\DatePicker;
use yii\widgets\ListView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\MasterNoticeMemberSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->title = '購入情報一覧';
?>
<style type="text/css">
  div.clear-padding {
        margin: -17px 0 0 -13px;
        width: 124%
    }
    ul.pagination {
        margin-left:56%;
    }
    div.empty {
        width: 35em;
        /* padding: 2em; */
        padding-top: 2em;
        padding-left: 11em;
        background: whitesmoke;
        /* width: 20em; */
        height: 5em;
        font-size: 130%;
        /* margin-left: 16%; */
        float: left;
    }
    .pagination > li:first-child > a, .pagination > li:first-child > span {
        border :none;
    }
    .pagination > li:last-child > a, .pagination > li:last-child > span {
        border :none;
    }
    .pagination>li>a {
        background: white;
    }
    .pagination > li > a:hover {
        background-color: white !important;
    }
    .hiden.show2 > a {
        width: 95px !important;
    }
    input[type=checkbox] + label:before {
    font-family: FontAwesome;
    display: inline-block;
    font-size: 19px;
}

div#right.col-md4.pull-right {
 margin-top: 5% ;
}

.text_checkbox {
    padding-left: 1%;
    padding-right: 4%;
}

</style>
<?php
 $form = ActiveForm::begin([
                'method' => 'get',
                'enableClientValidation' => false,
    ]);
?>
<div class="row">
    <div class="col-lg-12 col-md-12">
        <div class="box box-info box-solid">
            <div class="box-header with-border">
                <h4 class="text-title"><b><?= $this->title ?></b></h4>
            </div>
            <div class="box-body content">
                <div class="col-md-12">
                    <div class="box-header with-border common-box-h4 col-md-4">
                        <h4 class="text-title"><b><?= Yii::t('backend', 'Coupon List') ?></b></h4>
                    </div>
                </div>
                <div class="col-md-5">
                    <form action="<?php echo Url::base(true) . '/history/bookinghistory' ?>" method="GET" id="form">
                        <div class="row">
                            <div class="col-md-8">
                                <div class="col-md-1 label-margin" style="width: 6em;">
                                    <?= Yii::t('backend', 'Store By') ?>
                                </div>
                                <div class="col-md-9">
                                    <select name="store_name" class="form-control">
                                        <?php if ($stores) { ?>
                                            <option value="0">Choose</option>
                                            <?php foreach ($stores as $store) : ?>
                                                <option <?php
                                                if (Yii::$app->request->get('store_name') == $store->id) {
                                                    echo "selected ='selected'";
                                                }
                                                ?> value="<?php echo $store->id ?>"><?php echo $store->name; ?></option>
                                                }
                                            <?php endforeach; ?>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row" style="margin-top: 1.5em;">
                            <div class="col-md-12">
                                <div class="col-md-1 label-margin" style="width: 6em;">
                                    <?= Yii::t('backend', 'Purchase Date') ?>
                                </div>
                                <div class="col-md-4" >
                                    <div class="form-group">
                                        <div class='input-group date'>
                                           <?= $form->field($model, 'start_date', [
                                                    'options' => ['class' => 'form-control'],
                                                    'template' => '<div class="clear-padding">{input}{error}</div>'
                                                ])->widget(DatePicker::classname(), [
                                                    'language' => 'ja',
                                                    'dateFormat' => 'yyyy/MM/dd',
                                                    'clientOptions' => [
                                                        "changeMonth" => true,
                                                        "changeYear" => true,
                                                        "yearRange" => "1900:+0"
                                                    ],
                                                ])->textInput(['maxlength' => 10,
                                                    'name' =>'start_date',
                                                    'value' => (!empty(Yii::$app->request->get('start_date')) ?Yii::$app->request->get('start_date') : ''),
                                                    'class' => 'form-control'
                                                ])
                                            ?>
                                            <span class="input-group-addon">
                                                <span class="glyphicon glyphicon-calendar"></span>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-1 label-margin label-center date_"> ～ </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <div class='input-group date'>
                                          <?= $form->field($model, 'end_date', [
                                                    'options' => ['class' => 'form-control'],
                                                    'template' => '<div class="clear-padding">{input}{error}</div>'
                                                ])->widget(DatePicker::classname(), [
                                                    'language' => 'ja',
                                                    'dateFormat' => 'yyyy/MM/dd',
                                                    'clientOptions' => [
                                                        "changeMonth" => true,
                                                        "changeYear" => true,
                                                        "yearRange" => "1900:+0"
                                                    ],
                                                ])->textInput(['maxlength' => 10,
                                                    'name' =>'end_date',
                                                    'value' => (!empty(Yii::$app->request->get('end_date')) ?Yii::$app->request->get('end_date') : ''),
                                                    'class' => 'form-control'
                                                ])
                                            ?>
                                            <span class="input-group-addon">
                                                <span class="glyphicon glyphicon-calendar"></span>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row" style="margin-top: 1.5em;">
                            <div class="col-md-12">
                                <div class="col-md-1 label-margin" style="width: 6em;">
                                    <?= Yii::t('backend', 'Purchase Date') ?>
                                </div>
                                <div class="col-md-5" >
                                    <div class="form-group" style="width:200%;margin-top:2%">
                                            <?php $params = Yii::$app->request->get('status'); ?>
                                            <input type="checkbox" id="checkbox1" value="01" <?= (!empty($params) && in_array("01", $params)) ? "checked" : '' ;?> name="status[]"/>
                                            <label for="checkbox2" class="text_checkbox"><?= Yii::t('frontend','Status Pending'); ?></label>
                                            <input type="checkbox" id="checkbox2" value="02"  <?= (!empty($params) && in_array("02", $params)) ? "checked" : '' ;?>  name="status[]"/>
                                            <label for="checkbox2" class="text_checkbox"><?= Yii::t('frontend','Status Booking') ;?></label>
                                            <input type="checkbox" id="checkbox3" value="03"  <?= (!empty($params) && in_array("03", $params)) ? "checked" : '' ;?>  name="status[]"/>
                                            <label for="checkbox2" class="text_checkbox"><?= Yii::t('frontend','Status End'); ?></label>
                                            <input type="checkbox" id="checkbox4" value="04"  <?= (!empty($params) && in_array("04", $params)) ? "checked" : '' ;?> name="status[]"/>
                                            <label for="checkbox2" class="text_checkbox"><?= Yii::t('frontend','Status Cancel'); ?></label>
                                    </div>
                                </div>
                            </div>
                        </div>
                </div>
                <div class="col-md-2" style="padding-top: 2em;height: 7em;padding-left: 5em;background: whitesmoke;">

                    <?= Html::submitButton('<i class="fa fa-search" aria-hidden="true"></i>  検索', ['name' => 'submit', 'class' => 'btn common-button-submit btn_search_byhistory', 'style' => '', 'id' => 'submit']) ?>

                </div>
                </form>
                <div class="col-md-12">
                    <div class="col-md-12">
                            <div class="box-header_buy with-border common-box-h4 col-md-4">
                                <h4 class="text-title"><b><?= Yii::t('backend', 'Search Results') ?></b></h4>
                            </div>
                        </div>
                    <div class="col-md-7">
                        <?php
                        if ( $dataProvider) {
                            echo ListView::widget([
                                'dataProvider' => $dataProvider,
                                'summary' => false,
                                'summaryOptions' => ['class' => 'common-clr-fl-right'],
                                'pager' => [
                                    'class' => 'common\components\CustomizeLinkPager',
                                    'options' => ['class' => 'pagination '],
                                ],
                                'layout' => "{pager}{summary}{items}",
                                'itemView' => '_items_booking'
                            ]);
                        }
                        ?>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
