<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\grid\GridView;
use yii\widgets\LinkPager;
use yii\helpers\Url;
use yii\jui\DatePicker;
use yii\widgets\ListView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\MasterNoticeMemberSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->title = '購入情報一覧';
?>
<?php $this->registerCSSFile('@web/css/history.css',['position' => \yii\web\View::POS_HEAD]);?>
<div class="shared-common-container">
    <div class="shared-section-title">
        <?php echo Yii::t('frontend', 'Buyhistory Title') ?>
    </div>
    <div class="clearfix"></div>
    <div class="row shared-toppad-lg"></div>
    <?php $form = ActiveForm::begin([
           'method' => 'get',
           'enableClientValidation' => false,
           'action' => Url::home() . Yii::$app->controller->id . '/buyhistory',
           'options' => [
               'class' => 'history-search-form'
           ]
       ]); ?>
       <div class="col-md-9">
           <div class="row">
               <div class="col-md-2 label-margin">
                   <?= Yii::t('backend', 'Store By') ?>
               </div>
               <div class="col-md-10">
                   <select name="store_name" class="form-control" style="float: left">
                       <?php if ($stores) { ?>
                           <option value="0"><?= Yii::t('frontend','Choose All')?></option>
                           <?php foreach ($stores as $store) : ?>
                               <option <?php
                               if (Yii::$app->request->get('store_name') == $store->id) { echo "selected ='selected'";}
                               ?> value="<?php echo $store->id ?>"><?php echo $store->name; ?></option>
                               }
                           <?php endforeach; ?>
                       <?php } ?>
                   </select>
               </div>
           </div>
           <div class="row shared-toppad-lg"></div>
           <div class="row">
               <div class="col-md-2 label-margin">
                   <?= Yii::t('backend', 'Purchase Date') ?>
               </div>
               <div class="col-md-4 col-md-4-5" >
                   <div class="form-group">
                       <div class='input-group date'>
                       <?= $form->field($model, 'start_date', [
                           'options' => ['class' => 'form-control'],
                           'template' => '<div class="clear-padding">{input}{error}</div>'
                       ])->widget(DatePicker::classname(), [
                           'language' => 'ja',
                           'dateFormat' => 'yyyy/MM/dd',
                           'clientOptions' => [
                               "changeMonth" => true,
                               "changeYear" => true,
                               "yearRange" => "1900:+0"
                           ],
                       ])->textInput(['maxlength' => 10,
                           'name' =>'start_date',
                           'value' => (!empty(Yii::$app->request->get('start_date')) ?Yii::$app->request->get('start_date') : ''),
                           'class' => 'form-control'
                       ])
                   ?>
                           <span class="input-group-addon">
                               <span class="glyphicon glyphicon-calendar"></span>
                           </span>
                       </div>
                   </div>
               </div>
               <div class="col-md-1 label-margin label-center between-date"> ～ </div>
               <div class="col-md-4 col-md-4-5">
                   <div class="form-group">
                       <div class='input-group date'>
                       <?= $form->field($model, 'end_date', [
                           'options' => ['class' => 'form-control'],
                           'template' => '<div class="clear-padding">{input}{error}</div>'
                       ])->widget(DatePicker::classname(), [
                           'language' => 'ja',
                           'dateFormat' => 'yyyy/MM/dd',
                           'clientOptions' => [
                               "changeMonth" => true,
                               "changeYear" => true,
                               "yearRange" => "1900:+0"
                           ],
                       ])->textInput(['maxlength' => 10,
                           'name' =>'end_date',
                           'value' => (!empty(Yii::$app->request->get('end_date')) ?Yii::$app->request->get('end_date') : ''),
                           'class' => 'form-control'
                       ])
                   ?>
                           <span class="input-group-addon">
                               <span class="glyphicon glyphicon-calendar"></span>
                           </span>
                       </div>
                   </div>
               </div>
           </div>
       </div>
       <div class="col-md-3">
            <div class="col-xs-12 text-center" style="background: whitesmoke;padding-top: 35px;height: 100px;">
                <button type="submit" class="common-button-submit btn_search_byhistory">
                    <i class="fa fa-search"></i> 検索
                </button>
           </div>
       </div>
   <?php ActiveForm::end() ?>
   <div class="clearfix"></div>
   <div class="row shared-toppad-lg"></div>
   <div class="clearfix"></div>
   <div class="shared-section-title">
       <?= Yii::t('backend', 'Search Results') ?>
   </div>
   <div class="clearfix"></div>
   <?php if($dataProvider->totalCount > 0) { ?>
       <div class="row shared-toppad-sm"></div>
      <?php
      if ( $dataProvider) {
          echo ListView::widget([
              'dataProvider' => $dataProvider,
              'summary' => false,
              'summaryOptions' => ['class' => 'common-clr-fl-right'],
              'pager' => [
                  'class' => 'common\components\CustomizeLinkPager',
                  'options' => ['class' => 'pagination '],
              ],
              'layout' => "{pager}{summary}{items}",
              'itemView' => '_search_buyhistory'
          ]);
      }
      ?>
  <?php } else {?>
      <div class="no_result_search_history">
          <?php echo Yii::t('frontend','No Result Buy History') ?>
      </div>
  <?php } ?>
</div>
<div class="clearfix"></div>
<script>
    (function ($) {
        $('.date').each(function () {
            $(this).children('.input-group-addon').click(function () {
                $(this).datepicker('show');
            });
        });
    })(jQuery);
</script>
