<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use  \yii\jui\DatePicker;
use common\components\Constants;
use common\models\MasterStore;

/* @var $this yii\web\View */
/* @var $model backend\models\ChargeHistorySearch */
/* @var $form yii\widgets\ActiveForm */

$fieldOptions1 = [
    'options' => ['class' => 'row row-inline'],
    'template' => "<div class='col-md-2'>{label}</div><div class='col-md-10'>{input}\n{hint}\n{error}</div>\n"
];

$fieldOptions2 = [
    'options' => ['class' => 'col-md-6'],
    'template' => "<div class='col-md-4 no-padding'>{label}</div><div class='col-md-8'>{input}\n{hint}\n{error}</div>\n"
];

?>
<?php
    $form = ActiveForm::begin([
                'action' => ['charge '],
                'method' => 'get',
                'enableClientValidation' => false,
    ]);
    ?>

<div class="charge -search col-md-9">

    <?= $form->field($model, 'store_id', $fieldOptions1)->dropDownList(MasterStore::getListStoreFrontend(), ['prompt' => Yii::t('backend', 'Select All')]) ?>
    
    <div class="row row-inline">
        <div class="col-md-2">
            <?=
            $form->field($model, 'history_date_from', [
                'options' => ['class' => 'col-md-12 no-padding'],
                'template' => "<label class='mws-form-label'>{label}</label>"])
            ?>
        </div>
        <div class="col-md-10">
            <?=
            $form->field($model, 'history_date_from', [
                'options' => ['class' => 'col-md-5 no-padding'],
                'template' => '<div class="clear-padding">{input}{error}</div>'
            ])->widget(DatePicker::classname(), [
                'language' => 'ja',
                'dateFormat' => 'yyyy/MM/dd',
                'clientOptions' => [
                    "changeMonth" => true,
                    "changeYear" => true,
                    "yearRange" => "1900:+0"
                ],
            ])->textInput(['maxlength' => true])
            ?>
            <div class="text-center col-md-2">～</div>
            <?=
            $form->field($model, 'history_date_to', [
                'options' => ['class' => 'col-md-5 no-padding'],
                'template' => '<div class="clear-padding">{input}{error}</div>'
            ])->widget(DatePicker::classname(), [
                'language' => 'ja',
                'dateFormat' => 'yyyy/MM/dd',
                'clientOptions' => [
                    "changeMonth" => true,
                    "changeYear" => true,
                    "yearRange" => "1900:+0"
                ],
            ])->textInput(['maxlength' => true])
            ?>
        </div>
    </div>
</div>


<div class="col-md-3">
    <div class="form-group">
        <?= Html::submitButton(Yii::t('backend', 'Search'), ['class' => 'btn common-button-submit']) ?>
    </div>
</div>
<?php ActiveForm::end(); ?>
