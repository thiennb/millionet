<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\grid\GridView;
use yii\widgets\LinkPager;
use yii\helpers\Url;
use yii\jui\DatePicker;
use yii\widgets\ListView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\MasterNoticeMemberSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->title = Yii::t('frontend', 'Point Breach Crum');
?>
<?php $this->registerCSSFile('@web/css/history.css',['position' => \yii\web\View::POS_HEAD]);?>
<div class="clearfix"></div>
<div class="shared-common-container">
    <div class="shared-section-title">
        <?php echo Yii::t('frontend', 'Point Title') ?>
    </div>
    <div class="clearfix"></div>
    <div class="row">
        <div class="clearfix"></div>
        <div class="col-xs-12">
            <?php if ($dataProvider->totalCount > 0) { ?>
            <div class="row shared-toppad-lg"></div>
            <?php

            if ( $dataProvider) {
                echo ListView::widget([
                    'dataProvider' => $dataProvider,
                    'summary' => false,
                    'summaryOptions' => ['class' => 'common-clr-fl-right'],
                    'pager' => [
                        'class' => 'common\components\CustomizeLinkPager',
                        'options' => ['class' => 'pagination '],
                    ],
                    'layout' => "{pager}{summary}{items}",
                    'itemView' => '_items_point'
                ]);
            }
            ?>
            <?php } else { ?>
             <div class="no_result_search_history">
                    <?= Yii::t('frontend','No Result Common') ?>
            </div>
            <?php } ?>
        </div>
    </div>
</div>
<div class="clearfix"></div>
