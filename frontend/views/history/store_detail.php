<?php
use yii\helpers\Html;
use yii\helpers\Url;
use common\components\Util;
$urlImage = ($baseUrl) ? $baseUrl.'/api/web/uploads/' : Url::base(TRUE) ;
$this->title = Yii::t('frontend', 'Meta Store');
?>
<?php $this->registerCSSFile('@web/css/history.css',['position' => \yii\web\View::POS_HEAD]); ?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/flexslider/2.6.3/jquery.flexslider.js"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/flexslider/2.6.3/flexslider.css" rel="stylesheet" type="text/css" media="screen">
<link href="<?php echo Url::base(TRUE).'/frontend/web/css/slider.css'?>" rel="stylesheet" type="text/css" media="screen">
<div class="clearfix"></div>
<div class="shared-common-container store-detail">
    <h4 style="font-size: 25px"><?php echo ($store) ? $store['name'] : ''; ?></h4>
    <h5 style="font-size: 20px"><?php echo $store ? $store['name_kana'] : '' ?></h5>
    <div class="row">
        <div class="col-md-7">
    		<div id="slider" class="flexslider">
    			<ul class="slides" style="width:300px;">
    		    	<?php for ($i = 1; $i < 6; $i++): ?>
    		    	    <?php echo ($store['image' . $i] != '') ? '<li><img src="'.Util::getUrlImage($store['image' . $i]).'" style="height=100px;width=100px;"></li>' : NULL;?>
    		    	<?php endfor; ?>
    			</ul>
    		</div>

    		<div id="carousel" class="flexslider">
    			<ul class="slides">
                    <?php for ($i = 1; $i < 6; $i++): ?>
    		    	    <?php echo ($store['image' . $i] != '') ? '<li><img src="'.Util::getUrlImage($store['image' . $i]).'" style="height=50px;width=20%;"></li>' : NULL;?>
    		    	<?php endfor; ?>
    			</ul>
    		</div>
    	</div>
    	<!-- END DISPLAY IMAGES STORE-->
    	<div class="col-md-5">
    		<div class="form-group">
    			<?= Yii::t('frontend','Introduce') ?> : <?php echo ($store) ? $store['introduce'] : ''; ?>

    		</div>
    		<div class="form-group">
                <a href="<?php echo Url::to(['/booking-order/index','storeId' => Yii::$app->request->get('id')])  ?>" class="btn btn-default" style="color: #fff">空席確認・予約する</a>
    		</div>
    	</div>
    </div>
    <div class="row shared-toppad-lg"></div>
    <div class="shared-section-title">
        スタッフ情報
    </div>
    <div class="row shared-toppad-lg"></div>
    <div class="clearfix"></div>
    <div class="col-xs-12">
        <?php foreach ($staff_list as $id => $staff) : ?>
            <?php if($id === 0) : ?>
                <div class="row" style="margin-bottom: 10px;">
            <?php elseif($id % 4 === 0) : ?>
                </div><div class="clearfix"></div><div class="row">
            <?php endif ?>
            <div class="col-xs-6 col-sm-3">
                <div class="staff_image" data-toggle="modal" data-target="#staff-panel-<?php echo $staff['id'] ?>">
                    <img src="<?php echo Util::getUrlImage($staff->avatar) ?>" class="img-responsive" />
                </div>
                <p>
                    <?= Html::encode($staff->name) ?>
                </p>
                <p>
                    <?= $staff->position ?> （歴<?php echo $staff->career ?>年）
                </p>
                <p>
                    <span class="introduce"><?= $staff->catch ?>
                        【<a href="#" data-toggle="modal" data-target="#staff-panel-<?php echo $staff['id'] ?>">
                        <?php echo Yii::t('frontend', 'Detail') ?></a>】</span>
                </p>
            </div>
            <?php if ($id === count($staff_list) - 1) : ?>
                </div>
            <?php endif ?>
         <?php endforeach ?>
    </div>
    <div class="clearfix"></div>
    <div class="row shared-toppad-lg"></div>
    <div class="shared-section-title">
        店舗データ
    </div>
    <div class="row shared-toppad-sm"></div>
    <div class="row" style="margin: 0">
        <table class="table table-responsive table-bordered">
            <tr>
                <th class="col-md-1" style="width:13.1%" id="top_border"><?= Yii::t('frontend','Store Phone');?></th>
                <?php
                $tel = ($store) ? $store['tel'] : '' ;
                $tel_formart = ($tel) ? sprintf("%s-%s-%s",substr($tel, 0, 3),substr($tel, 3, 3),substr($tel, 6)) : '';

            ?>
                <td class="col-md-4"><?= $tel_formart; ?>
                </td>
            </tr>
            <tr>
                <th class="col-md-1"><?= Yii::t('frontend','Store Home Page');?></th>
                <td class="col-md-4"><a id="url_store" target="_blank" href="<?php echo $store['website']; ?>"><?php echo ($store) ? $store['website'] : ''; ?></a></td>
            </tr>
             <tr>
                <th class="col-md-1"><?= Yii::t('frontend','Store Address');?></th>
                <td class="col-md-1"><?php echo ($store) ? $store['address'] : '' ; ?></td>
            </tr>
            <tr>
                <th class="col-md-1"><?= Yii::t('frontend','Store Direction');?></th>
                <td class="col-md-1"><?php echo ($store) ? $store['directions'] : '' ;?></td>
            </tr>
            <tr>
                <th class="col-md-1"><?= Yii::t('frontend','Store Hour');?></th>
                <td class="col-md-1"><?php echo ($store) ? $store['time_open'].' ～ '.$store['time_close'] : '' ; ?></td>
            </tr>
            <tr>
                <th class="col-md-1"><?= Yii::t('frontend','Store Holiday');?></th>
                <td class="col-md-1"><?php echo ($store) ? $store['regular_holiday'] : '' ; ?></td>
            </tr>
        </table>
    </div>
    <div class="row">
        <div class="col-md-2">
           <a href="<?php echo Url::base(TRUE).'/history/storehistory'?>" style="width : 120%;margin: 10% 0 0 -3%" class="btn btn-pink btn-empty btn-default pull-left">◀<?= Yii::t('frontend','Back'); ?></a>
       </div>
    </div>
</div>
<div class="clearfix"></div>
<div class="row shared-toppad-lg"></div>
<div class="clearfix"></div>
<!-- end info name -->
<?php echo $this->render('/booking-order/part/staff-panels', ['staffs' => $staff_list, 'staff_item_keys' => $staff_item_keys]) ?>



<!--MODEL STAFF INFO -->

<!-- END MODEL STAFF INFO -->
<script type="text/javascript" charset="utf-8">
jQuery.fn.centerModal = function () {
    $(this).modal();
};

$(window).load(function() {
  	$('#carousel').flexslider({
	    animation: "slide",
	    controlNav: false,
	    animationLoop: false,
	    slideshow: false,
	    itemWidth: 200,
	    itemMargin: 5,
	    asNavFor: '#slider',
        nextText: '',
        prevText : ''
  	});

  	$('#slider').flexslider({
	    animation: "slide",
	    controlNav: false,
	    animationLoop: false,
	    slideshow: false,
	    sync: "#carousel",
        nextText : '',
        prevText : ''
  	});

 //  	var  a = $(".introduce").text().length;
    //
 //  	console.log(a);
 //  	if (a > 20) {
    //
 //  		$(".form-group3").css("overflow","hidden");
 //  		$(".form-group3").css("overflow","hidden");
 //  		$("#read_more").show();
 //  	}
});
</script>
