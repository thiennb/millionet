<?php

use yii\helpers\Html;
use yii\widgets\ListView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('backend', 'List Booking Infomation');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row">
    <div class="col-lg-12 col-md-12">
        <div class="box box-info box-solid">
            <div class="box-header with-border">
                <h4 class="text-title"><b><?= $this->title ?></b></h4>
            </div>
            <div class="box-body content">
                <div class="col-md-12">
                    <div class="common-box">
                        <div class="row">
                            <div class="box-header with-border common-box-h4 col-md-6">
                                <h4 class="text-title"><b><?= Yii::t('backend', 'List Booking Infomation') ?></b></h4>
                            </div>
                        </div>

                        <div class="box-body content">
                            <div class="col-md-7">
                                <?php echo $this->render('_search-booking', ['model' => $searchModel]); ?>                                
                            </div>
                        </div>
                    </div>
                    <?php
                    Pjax::begin();
                    ?>    
                    <div class="common-box">
                        <div class="row">
                            <div class="box-header with-border common-box-h4 col-md-10">
                                <h4 class="text-title"><b><?= Yii::t('backend', 'Search result') ?></b></h4>
                            </div>
                        </div>
                        <div class="box-body content">
                            <div class="col-md-6">
                                <?php Pjax::begin(); ?>    
                                <?=
                                ListView::widget([
                                    'dataProvider' => $dataProvider,
                                    'layout' => "{pager}{summary}{items}",
                                    'summaryOptions' => ['class' => 'common-clr-fl-right'],
                                    'summary' => false,
                                    'pager' => [
                                        'class' => 'common\components\LinkPagerMillionet',
                                        'options' => ['class' => 'pagination '],
                                    ],
                                    'options' => ['class' => 'list-view'],
                                    'itemOptions' => ['class' => 'table table-striped table-bordered text-center',],
                                    'itemView' => '_items_booking',
                                ])
                                ?>
                            </div>
                        </div>
                    </div>
                    <?php Pjax::end(); ?>
                </div>
            </div>
        </div>
    </div>
</div>
