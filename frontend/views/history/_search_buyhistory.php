<?php

use yii\helpers\Html;
use yii\helpers\Url;
use common\models\MstOrderSearch;
?>

<div class="col-xs-12">
    <div class="row" style="border: 2px solid #8c8989;margin-top:1%;padding: 1.5em;">
        <div class="form-group ">
            <span><?php echo ($model->name) ? $model->name : ''; ?></span>
        </div>
        <div class="related-post">
        </div>
        <div class="form-group">
            <div class="row">
                <div class="col-md-8 ">
                    <?= Yii::t('frontend', 'Date Store') ?> : <span><?php  echo ($model->process_date) ?  $model->process_date . ' ' . $model->process_time : ''; ?></span>
                </div>
                <div class="col-md-4">
                    <?php  $total = ($model->total) ? $model->total : '0';?>
                    <?= Yii::t('backend', 'Payment Amount') ?> <span style="font-size: 1.7em;color:#FF69B4;font-weight: bold">
                    <?php  echo '¥'.number_format($total) ; ?>
                    </span>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="row" style="height: 60px;">
            <?php $check_home_lfg = ($model->order_code) ?  MstOrderSearch::getHome_flgbyBooking($model->booking_id) : '0' ?>
            <?php $check_gilt = ($model->order_code) ? MstOrderSearch::getGilf_flgByCode($model->order_code,$model->company_id) : '0' ?>
            <?php  if (($check_gilt) && in_array("1", $check_gilt)) { ?>
                <i class="fa fa-gift" aria-hidden="true" style="color:#f5057d;font-size: 4em;margin-left: 0.3em;" ></i>
            <?php  } ?>
            <?php   if (($check_home_lfg) && $check_home_lfg['to_home_delivery_flg'] == 1) { ?>
                <i class="fa fa-truck" aria-hidden="true" style="color: #3eb2c5;font-size: 4em;"></i>
            <?php  } ?>
        </div>
        <div class="clearfix"></div>
        <div class="pull-right">
            <a href="<?php  echo Url::base(TRUE) . '/history/buydetail?order_code=' . $model->order_code ?>">
                 <?= Html::submitButton(Yii::t('backend', '詳細を見る'), ['class' => 'btn common-button-submit', 'id' => 'btn-submit']) ?>
            </a>
        </div>
    </div>
</div>
