<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\grid\GridView;
use yii\widgets\LinkPager;
use yii\helpers\Url;
use yii\jui\DatePicker;
use yii\widgets\ListView;
/* @var $this yii\web\View */
/* @var $searchModel common\models\MasterNoticeMemberSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->title = Yii::t('backend', 'お気に入り店舗一覧');
?>
<?php $this->registerCSSFile('@web/css/history.css',['position' => \yii\web\View::POS_HEAD]);?>
<div class="shared-common-container">
    <div class="shared-section-title">
       <?= Yii::t('backend', 'List Store') ?>
    </div>
    <div class="clearfix"></div>
    <?php if ($dataProvider->totalCount > 0) { ?>
       <?php
            if ( $dataProvider) {
                echo ListView::widget([
                    'dataProvider' => $dataProvider,
                    'summary' => false,
                    'summaryOptions' => ['class' => 'common-clr-fl-right'],
                    'pager' => [
                        'class' => 'common\components\CustomizeLinkPager',
                        'options' => ['class' => 'pagination '],
                    ],
                    'layout' => "{pager}{summary}{items}",
                    'itemView' => 'view_store'
                ]);
            }
        ?>
    <?php } else { ?>
        <div class="no_result_search_history">
            <?= Yii::t('frontend','No Result Search Point') ?>
        </div>
    <?php } ?>
</div>
<div class="row shared-toppad-lg"></div>
