<?php

use yii\helpers\Html;
use yii\helpers\Url;
?>
<div class="clearfix"></div>
<div class="col-md-12">
    <div class="row" style="border: 2px solid #8c8989;margin-top:1%;padding: 1.5em;">
        <div class="form-group ">
            <span><?php echo ($model['store_name']) ? $model['store_name'] : ''; ?></span>
        </div>
        <div class="related-post">
        </div>
        <div class="form-group">
            <div class="row">
                <div class="col-md-4">
                    <?= Yii::t('frontend', 'Date Time') ?> : <span><?= ($model['process_date']) ? $model['process_date'].'   '.$model['process_time'] : ''; ?></span>
                </div>
                 <div class="col-md-6">
                   <?= Yii::t('frontend', 'Charge Balance') ?>
                </div>
                <div class="col-md-2">
                    <span class="text" style="font-size: 1.7em;color:<?= (number_format($model['charge_balance']) != 0 ) ? '#FF69B4' : 'black'; ?>;font-weight: bold;"><?php echo number_format($model['charge_balance']); ?><?= Yii::t('frontend','Payment'); ?> </span>
                </div>
            </div>
        </div>
        <div class="form-group pull-right ">
            <span class='prev_bt'>▶</span><a href="<?php echo Url::base(TRUE).'/history/chargedetail?store_code='.$model['store_code'].'&id='.$model['id']?>" style="color: #3d3d3d;"><?= Yii::t('frontend', 'View Charge Detail') ?></a>
        </div>
        <div class="clearfix"></div>
    </div>
</div>
 <div class="clearfix"></div>
