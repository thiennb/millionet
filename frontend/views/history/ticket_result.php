<?php

use yii\helpers\Html;
use yii\helpers\Url;
use common\models\MasterTicketSearch;
?>
<div class="col-xs-12">
    <div class="row" style="border: 2px solid #8c8989;margin-top:1%;padding: 1.5em;">
        <div class="form-group ">
            <span><?php  echo ($model['store_name']) ? $model['store_name'] : ''; ?></span>
        </div>
        <div class="related-post">
        </div>
        <div class="form-group">
            <div class="row">
                <div class="col-md-4">
                    <?= Yii::t('frontend', 'Ticket Name') ?> : <span><?php  echo ($model['ticket_name']) ? $model['ticket_name']: ''; ?></span>
                </div>
                 <div class="col-md-6" style="padding-left: 8em;">
                   <?= Yii::t('frontend', 'Ticket Balance') ?>
                </div>
                <div class="col-md-2">
                    <?php $ticket_balance = ($model['ticket_balance']) ? $model['ticket_balance'] : '0'; ?>
                    <span style="font-size: 1.7em;color:<?php  echo ($ticket_balance != 0 )  ? '#FF69B4' : 'black';?>;font-weight: bold"><?php echo ($ticket_balance) ? $ticket_balance .Yii::t('frontend','Ticket Blance') : "0".Yii::t('frontend','Ticket Blance'); ?> </span>
                </div>
            </div>
        </div>
        <div class="form-group pull-right ">
            <span class='prev_bt'>▶</span><a href="<?php  echo Url::base(true).'/history/ticketdetail?code='.$model['ticket_jan_code'].'&store_id='.$model['store_id_d'].'&id='.$model['id']?>" style="color: #3d3d3d;"><?= Yii::t('frontend', 'View History Ticket') ?></a>
        </div>
    </div>
</div>
