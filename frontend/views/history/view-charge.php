<?php

use yii\helpers\Html;
use yii\bootstrap\Modal;

/* @var $this yii\web\View */
/* @var $model common\models\Booking */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Bookings', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="charge-view">

    <table class="table-store-detail table table-striped table-bordered detail-view">
        <tbody>
            <tr>
                <th><?= Yii::t('backend', 'Store Information') ?></th>
                <td>
                    <div class='row row-inline'>
                        <div class='col-mid-3'>店舗名:</div>
                        <div class='col-mid-3'>
                            <?= Html::a($modelStore->name, ['/store-detail', 'id' => $modelStore->id]) ?>
                        </div>
                    </div> 
                    <div class='row row-inline table-bordered'>
                        <div class='col-mid-3'>店舗住所:</div>
                        <div class='col-mid-3'> 
                            <?= $modelStore->address ?>
                        </div>
                    </div>
                    <?php if (!empty($modelStore->longitude) && !empty($modelStore->latitude)) { ?>
                        <div class="row row-inline table-bordered">
                            <iframe width="425" 
                                    height="350" 
                                    frameborder="0" 
                                    scrolling="no" 
                                    marginheight="0" 
                                    marginwidth="0" 
                                    src="https://maps.google.com/maps?f=q&source=s_q&hl=en&geocode=&q=<?= str_replace(" ", "+", $modelStore->address) ?>&ie=UTF8&hq=&t=m&z=14&ll=<?= $modelStore->latitude ?>,<?= $modelStore->longitude ?>&output=embed">
                            </iframe>
                        </div>
                    <?php } ?>
                </td>
            </tr>

        </tbody>
    </table>

    <table class="table table-striped table-bordered detail-view">
        <tbody>
            <tr>
                <th><?= Yii::t('backend', 'Purchased Product') ?></th>
                <td> <?=   Html::a(Yii::t('backend', 'Detail of purchased product'), '#') ?> </td>
            </tr>
            <tr>
                <th><?= Yii::t('backend', 'Purchased money') ?></th>
                <td> <span> 
                        <?= empty($model->process_money) ? "¥0" : "¥" . $model->process_money ?>  
                    </span> 
                    
                    <span class="btn btn-primary ">
                        <?= "TYPE CHARGE" ?>  
                    </span>
                </td>

            </tr>

        </tbody>
    </table>
    

    <?php
    Modal::begin([
        'id' => 'cancelModal',
        'size' => 'SIZE_LARGE',
        'header' => '<b>' . Yii::t('backend', 'Confirm Store') . '</b>',
        'footer' =>
        ((false) ? Html::button(Yii::t('backend', 'Close'), ['class' => 'btn common-button-default', 'data-dismiss' => "modal", 'id' => 'btn-close']) : Html::button(Yii::t('backend', 'Close'), ['class' => 'btn common-button-default', 'data-dismiss' => "modal", 'id' => 'btn-close']) . Html::a(Yii::t('backend', 'Save'), ['cancel-booking', 'id' => $model->id], ['class' => 'btn common-button-submit'])),
        'footerOptions' => ['class' => 'modal-footer text-center'],
    ]);
    ?>
    <?php
    if (false) {
        echo Yii::t('backend', 'vi qua ki han cancel, xin call den cua hang de dc cancel.');
        ?>
        <div class="row">
            店舗電話番号:
        </div>
        <div class="row">
            <h2><?= $modelStore->tel ?></h2>  
        </div>

        <?php
    } else {
        echo Yii::t('backend', 'BAN CHAC CHAN MUON CANCEL BOOKING?');
    }
    ?>
    <?php
    Modal::end();
    ?>
</div>
