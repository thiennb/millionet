<?php
use yii\helpers\Html;

/* @var $this \yii\web\View */
/* @var $content string */
?>

<header class="main-header">

    <a href="<?php echo Yii::$app->homeUrl ?>" class="logo" style="margin: 0; padding: 0 !important">
        <img src="/api/uploads/logo05.jpg" style="    height: 80px; width: 100%" alt="Logo">
    </a>

    <nav class="navbar navbar-static-top navbar-custom" role="navigation">

        <!--<a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>-->

        <div class="navbar-custom-menu">            
            <ul class="nav navbar-nav" style="position: absolute; right: 0; bottom: 0;"> <!-- !css -->                
                <?php if (Yii::$app->user->isGuest) : ?>
<!--                    <li class="custom-li"><a href="/site/login" class="pull-right">ログイン</a></li>-->
                <?php else : ?>
                    <li class="custom-li"><a href="/site/logout" class="pull-right"><i class="fa fa-sign-out" aria-hidden="true"></i> ログアウト</a></li>
                <?php endif ?>
            </ul>
        </div>
    </nav>
</header>
