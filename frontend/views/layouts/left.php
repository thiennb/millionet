<?php if (!Yii::$app->user->isGuest) : ?><aside class="main-sidebar">

    <section class="sidebar">
      <?php $custormer  =common\models\MasterCustomer::findOne(['user_id'=>Yii::$app->user->id]) ; 
            $customer_id = (count($custormer) >0 )?$custormer->id :0;
      ?>
        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu'],
                'items' => [
                    ['label'=>Yii::t('frontend', 'Top page'), 'icon'=>'fa fa-home', 'url'=> ['/top/index']],
                    ['label'=>Yii::t('frontend', 'Booking info'), 'icon'=>'fa fa-calendar-check-o', 'url'=> ['history/bookinghistory']],
                    [
                        'label' => Yii::t('frontend', 'History info'),
                        'icon' => 'fa fa-clock-o',
                        'url' => '#',
                        'items' => [
                            ['label' => Yii::t('frontend', 'Point History'), 'options' => ['class' => 'pad-left'], 'url' => ['/history/pointhistory']],
                            ['label' => Yii::t('frontend', 'Store History'), 'options' => ['class' => 'pad-left'], 'url' => ['/history/buyhistory']],
                            ['label' => Yii::t('frontend', 'Ticket History'), 'options' => ['class' => 'pad-left'], 'url' => ['/history/tickethistory']],
                            ['label' => Yii::t('frontend', 'Charge History'), 'options' => ['class' => 'pad-left'], 'url' => ['/history/chargehistory']]
                        ],
                    ],
                    ['label'=>Yii::t('frontend', 'Favorite Store'), 'icon'=>'fa fa-heart', 'url'=> ['/history/storehistory']],
                    ['label'=>Yii::t('frontend', 'Customer Info'), 'icon'=>'fa fa-cog', 'url'=> '#',
                        'items' => [
                            ['label' => Yii::t('frontend', 'Rank Info'), 'options' => ['class' => 'pad-left'], 'url' => ['/member/rank-info']],
                            ['label' => Yii::t('frontend', 'Update Register Content'), 'options' => ['class' => 'pad-left'],'visible' => !Yii::$app->user->isGuest, 'url' => ['/member/update?id='. $customer_id]],
                        ],
                    ],
                    /*[
                        'label' => Yii::t('backend', 'Booking History'),
                        'icon' => 'fa fa-users',
                        'url' => '#',
                        'items' => [
                            ['label' => Yii::t('backend', 'Booking History'), 'icon' => 'fa fa-list', 'url' => ['/booking-order?storeId=1']],
                        ],
                    ],*/
                    //['label' => 'Menu Yii2', 'options' => ['class' => 'header']],
                ],
            ]
        ) ?>

    </section>

</aside><?php endif ?>
