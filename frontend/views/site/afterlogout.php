<?php use yii\helpers\Html;
    $this->title = 'ログアウトしました';
?>
<div class="row">
    <div class="col-md-4 col-md-offset-4">
        <div class="box box-info box-solid">
            <div class="box-body content">
                <div class="col-md-12">
                    <div class="common-box col-md-12">
                        <div class="col-md-12">
                             <div class="row">
                                <div class="box-header with-border common-box-h4 col-md-8">
                                    <h4 class="text-title"><b>ログアウトしました</b></h4>
                                </div>
                            </div>
                            <div class="row row-inline"><div class="form-group"></div></div>
                            <div class="row">
                                <p>ログアウトし、ログイン情報を消去しました。</p>
                                <div class="row row-inline"><div class="form-group"></div></div>
                                <p>他人に見られないようにするためには、ブラウザを閉じてください。</p>
                                <p class="help-text">※このブラウザを開いたままだと、ブラウザのバックボタンで前の画面が表示出来る可能性があります。</p>
                                <div class="row row-inline"><div class="form-group"></div></div>
                                <div class="col-xs-12 text-center">
                                    <?= Html::a('別のアカウントでログイン', 'login', ['class' => 'btn cm-btn-submit']) ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
