<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use frontend\assets\LoginAsset;
use dmstr\widgets\Alert;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */
LoginAsset::register($this);
$this->title = Yii::t('backend','Login screen');
$this->registerCssFile('@web/css/member.css', ['depends' => [yii\bootstrap\BootstrapAsset::className()]]);
$this->registerCssFile('@web/css/common.css', ['depends' => [yii\bootstrap\BootstrapAsset::className()]]);
$this->registerJsFile('@web/js/member.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile('@web/js/common-js.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
?>


<div class="col-md-12 login-box-body login-box-custom position-relative clear-padding-login-box-body">
    <div class="login-logo position-relative">
        <a href="#"> <?= Yii::t('backend', 'Sign In') ?></a>
    </div>
    <div class="col-md-12 position-relative">
        <div class="col-md-3 col-sm-12"></div>
        <div class="col-md-3 col-sm-12 ">
            <div class="row login-box login-box-custom">
                <div class="login-box-body">
                    <div class="login-logo">
                        <a href="#"> <?= Yii::t('backend', 'Sign In') ?></a>
                    </div>
                    <div class="form-group"><?= Alert::widget() ?></div>
                    <?php
                    $form = ActiveForm::begin(['id' => 'login-form',
                                'enableClientValidation' => false,
                                'fieldClass' => 'justinvoelker\awesomebootstrapcheckbox\ActiveField']);
                    ?>
                    <div class="col-md-12 field-loginform-password clear-padding">
                        <label class="control-label" for="loginform-phone"><?= Yii::t('backend','Email') ?></label>
                    </div>
                    <?=
                        $form->field($model, 'email',[
                            'template'  => '<div class="col-md-12 col-md-12 col-sm-12  clear-padding">{input}</div>'
                        ])
                        ->textInput(['maxlength'=>TRUE, 'class'=>'form-control text-left'])
                    ?>
                    <div class="col-md-12 field-loginform-password clear-padding">
                        <div class="form-group"></div>
                        <label class="control-label" for="loginform-password"><?= Yii::t('backend','Password') ?></label>
                    </div>
                    <?=
                        $form->field($model, 'password',[
                            'template'  =>  '{input}'
                        ])
                        ->passwordInput(['maxlength'=>TRUE])->label(FALSE)
                    ?>
                    <?=
                        $form->field($model, 'rememberMe')
                        ->checkbox()
                    ?>
                    <?= Html::submitButton(Yii::t('backend', 'Sign In'), ['class' => 'btn common-button-submit', 'name' => 'login-button']) ?>
                    <?php ActiveForm::end(); ?>
                </div>            
            </div>   
            <div class="col-md-12">
                <label>パスワードを忘れた方は<a href="/site/request-password-reset" style="text-decoration: underline;color: red;"><span style="color: red;">こちら</span></a></label>
            </div>
        </div>
        <div class="col-md-3 col-sm-12">
            <div class="login-box login-box-custom">
                <div class="login-box-body">
                    <div class="login-logo">
                        <a href="#"> <?= Yii::t('backend', 'If you are not a member yet') ?></a>
                    </div>
                    <div class="content"></div>
                    <?= Html::a(Yii::t('backend', 'Join Free'),['/member'], ['class' => 'btn login-box-a', 'name' => 'login-button']) ?>
                    <div class="content"></div>
                    <?= Html::a(Yii::t('backend', 'Registered in the Google information'),['/member/step1-email/'], ['class' => 'btn login-box-a', 'name' => 'login-button']) ?>
                </div>
            </div>
        </div>
    <div class="col-md-3 col-sm-12"></div>
    </div>
    <div class="login-logo position-relative clear-marging">
        <p>&nbsp;</p>
    </div>
</div><!-- /.login-box -->