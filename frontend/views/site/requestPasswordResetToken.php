<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\PasswordResetRequestForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use dmstr\widgets\Alert;
use frontend\assets\LoginAsset;

LoginAsset::register($this);
$this->registerCssFile('@web/css/member.css', ['depends' => [yii\bootstrap\BootstrapAsset::className()]]);
//$this->registerCssFile('@web/css/login-frontend.css', ['depends' => [yii\bootstrap\BootstrapAsset::className()]]);
$this->registerCssFile('@web/css/common.css', ['depends' => [yii\bootstrap\BootstrapAsset::className()]]);
$this->registerJsFile('@web/js/resetpassword.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
//$this->registerJsFile('@web/js/common-js.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->title = Yii::t('frontend', 'Reissue password');
?>

<div class="col-xs-12 login-box-body position-relative clear-padding-login-box-body login-frontend">
    <div class="col-xs-12 position-relative">
        <div class="col-xs-12 col-sm-12"></div>
        <div class="col-xs-12 col-sm-12 ">
            <div class="row">
                <div class="col-xs-4 col-xs-offset-4">
                    <div class="form-group"><?= Alert::widget() ?></div>
                    <?php
                    $form = ActiveForm::begin(['id' => 'request-password-reset-form',
                                'enableClientValidation' => false,
                                'fieldClass' => 'justinvoelker\awesomebootstrapcheckbox\ActiveField']);
                    ?>
                    <div class="row">
                        <div class="box-header with-border common-box-h4 col-xs-6">
                            <h4 class="text-title"><b><?= Yii::t('frontend', 'Reissue password') ?></b></h4>
                        </div>
                    </div>
                    <div class="row form-group"></div>
                    <div class="row form-group">
                        <?= Yii::t('frontend','Please enter your registered name and phone number.') ?>
                    </div>
                    <div class="row row-inline">
                        <div class="col-xs-3 bg_header div-horizontal">
                            <label class='mws-form-label'><?= Yii::t('backend','Name'); ?></label>
                            <span class="required-text">必須</span>
                        </div>
                        <div class="col-xs-9 clear-form-group">
                            <?= $form->field($model, 'first_name',[
                                'template' => "<div class='col-xs-2 clear-label-requir'>{label}</div>"
                                            . '<div class="col-xs-4">{input}{error}</div>'
                            ])->textInput(['maxlength' => true]) ?>
                            <?= $form->field($model, 'last_name',[
                                'template' => "<div class='col-xs-2 clear-label-requir'>{label}</div>"
                                            . '<div class="col-xs-4">{input}{error}</div>'
                            ])->textInput(['maxlength' => true]) ?>
                            <div class="div-margin"></div>
                            <?= $form->field($model, 'first_name_kana',[
                                'template' => "<div class='col-xs-2 clear-label-requir'>{label}</div>"
                                            . '<div class="col-xs-4">{input}{error}</div>'
                            ])->textInput(['maxlength' => true]) ?>
                            <?= $form->field($model, 'last_name_kana',[
                                'template' => "<div class='col-xs-2 clear-label-requir'>{label}</div>"
                                            . '<div class="col-xs-4">{input}{error}'
                                            . '</div>'
                            ])->textInput(['maxlength' => true]) ?>
                        </div>
                    </div>
                    <div class="row row-inline">
                        <div class="col-xs-3 bg_header div-horizontal">
                            <label class='mws-form-label'><?= Yii::t('backend','Phone'); ?></label>
                            <span class="required-text">必須</span>
                        </div>
                        <div class="col-xs-9 clear-form-group">
                            <?= $form->field($model, 'phone_1',[
                                'template' => '<div class="col-xs-3">{input}'
                                                . '{error}'
                                            . '</div>'
                            ])->textInput(['maxlength' => true,'class'=>'form-control text-right input-number']) ?>
                            <?= $form->field($model, 'phone_2',[
                                'template' => '<div class="col-xs-1 text-center"><i class="fa fa-minus" aria-hidden="true"></i></div>'
                                            . '<div class="col-xs-3">{input}'
                                                . '{error}'
                                            . '</div>'
                            ])->textInput(['maxlength' => true,'class'=>'form-control text-right input-number']) ?>
                            <?= $form->field($model, 'phone_3',[
                                'template' => '<div class="col-xs-1 text-center"><i class="fa fa-minus" aria-hidden="true"></i></div>'
                                            . '<div class="col-xs-3">{input}'
                                                . '{error}'
                                            . '</div>'
                            ])->textInput(['maxlength' => true,'class'=>'form-control text-right input-number']) ?>
                        </div>
                    </div>
                    <div class="row form-group"></div>
                    <div class="row">
                        <div class="form-group text-left">
                            <?= Html::a('<i class="fa fa-caret-left" aria-hidden="true" style="color:#FF69B4;">&nbsp</i>'.Yii::t('backend', 'Return'), ['login'], ['class' => 'btn common-back']) ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group text-center">
                            <?= Html::submitButton(Yii::t('frontend', 'Next page'), ['class' => 'btn cm-btn-submit'])
                            ?>
                        </div>
                    </div>
                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
    <div class="col-xs-4 col-sm-12"></div>
    </div>
    <div class="login-logo position-relative clear-marging">
        <p>&nbsp;</p>
    </div>
</div><!-- /.login-box -->
