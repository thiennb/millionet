<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ResetPasswordForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use dmstr\widgets\Alert;
use frontend\assets\LoginAsset;

LoginAsset::register($this);
$this->registerCssFile('@web/css/member.css', ['depends' => [yii\bootstrap\BootstrapAsset::className()]]);
$this->registerCssFile('@web/css/common.css', ['depends' => [yii\bootstrap\BootstrapAsset::className()]]);
$this->registerJsFile('@web/js/member.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile('@web/js/common-js.js', ['depends' => [\yii\web\JqueryAsset::className()]]);

$this->title = Yii::t('frontend','Reissue password');
?>

<div class="col-xs-12 login-box-body position-relative clear-padding-login-box-body login-frontend">
    <div class="col-xs-12 position-relative">
        <div class="col-xs-12 col-sm-12"></div>
        <div class="col-xs-12 col-sm-12 ">
            <div class="row">
                <div class="col-xs-4 col-xs-offset-4">
                    <div class="form-group"><?= Alert::widget() ?></div>
                    <?php
                    $form = ActiveForm::begin(['id' => 'request-password-reset-form',
                                'enableClientValidation' => false,
                                'fieldClass' => 'justinvoelker\awesomebootstrapcheckbox\ActiveField']);
                    ?>
                    <div class="row">
                        <div class="box-header with-border common-box-h4 col-xs-6">
                            <h4 class="text-title"><b><?= $this->title ?></b></h4>
                        </div>
                    </div>
                    <div class="row form-group"></div>
                    <div class="row form-group">
                        <div class="col-xs-12 text-center">
                            <?= Yii::t('frontend','Please set new password and go "change"') ?>
                        </div>
                    </div>
                    <div class="row row-inline">
                        <div class="col-md-3 bg_header div-horizontal">
                            <label class='mws-form-label'><?= Yii::t('frontend','New Password'); ?></label>
                        </div>
                        <div class="col-md-9 clear-form-group">
                            <?= $form->field($model, 'password',[
                                'template' => '<div class="col-md-12">{input}'.Yii::t('backend','Please enter in the 6 to 20-digit alphanumeric').'{error}</div>'
                            ])->passwordInput(['maxlength' => true]) ?>
                            <div class="div-margin"></div>
                            <?= $form->field($model, 'password_re',[
                                'template' => '<div class="col-md-12">{input}'.Yii::t('backend','For confirmation, please enter the other over the degree of').'{error}</div>'
                            ])->passwordInput(['maxlength' => true]) ?>
                        </div>
                    </div>
                    <div class="row form-group"></div>
                    <div class="row">
                        <div class="form-group text-center">
                            <?= Html::submitButton(Yii::t('frontend', 'Update'), ['class' => 'btn cm-btn-submit'])
                            ?>
                        </div>
                    </div>
                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
    <div class="col-xs-4 col-sm-12"></div>
    </div>
    <div class="login-logo position-relative clear-marging">
        <p>&nbsp;</p>
    </div>
</div><!-- /.login-box -->