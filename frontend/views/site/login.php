<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use frontend\assets\LoginAsset;
use dmstr\widgets\Alert;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */
LoginAsset::register($this);
$this->title = Yii::t('backend','Login screen');
$this->registerCssFile('@web/css/member.css', ['depends' => [yii\bootstrap\BootstrapAsset::className()]]);
$this->registerCssFile('@web/css/common.css', ['depends' => [yii\bootstrap\BootstrapAsset::className()]]);
$this->registerCssFile('@web/css/login-frontend.css', ['depends' => [yii\bootstrap\BootstrapAsset::className()]]);
$this->registerJsFile('@web/js/member.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile('@web/js/common-js.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
?>


<div class="login-box-body position-relative clear-padding-login-box-body container login-frontend">
    <!--<div class="login-logo position-relative">
        <a href="#"> <?= Yii::t('backend', 'Sign In') ?></a>
    </div>-->
    <div class="col-md-12 position-relative">
        <div class="col-md-6 col-lg-5 col-lg-offset-1 col-sm-12 ">
            <div class="row login-box login-box-custom">
                <div class="login-box-body">
                    <div class="login-logo">
                        <a href="#"> <?= Yii::t('backend', 'Sign In') ?></a>
                    </div>
                    <div class="form-group"><?= Alert::widget() ?></div>
                    <?php
                    $form = ActiveForm::begin(['id' => 'login-form',
                                'enableClientValidation' => false,
                                'fieldClass' => 'justinvoelker\awesomebootstrapcheckbox\ActiveField']);
                    ?>
                    <div class="col-md-12 field-loginform-password clear-padding">
                        <label class="control-label" for="loginform-phone"><?= Yii::t('backend','User login') ?></label>
                    </div>
                    <?=
                        $form->field($model, 'phone_1',[
                            'template'  => '<div class="col-md-3 clear-padding">{input}</div>'
                        ])
                        ->textInput(['maxlength'=>TRUE, 'class'=>'form-control input-number text-right'])
                    ?>

                    <?=
                        $form->field($model, 'phone_2',[
                            'template'  => '<div class="col-125 hidden-xs hidden-sm col-md-1 col-sm-1 div-horizontal-all padding-20"><i class="fa fa-minus" aria-hidden="true"></i></div>'
                            . '<div class="col-md-3 clear-padding">{input}</div>'
                        ])
                        ->textInput(['maxlength'=>TRUE, 'class'=>'form-control input-number text-right'])->label(FALSE)
                    ?>

                    <?=
                        $form->field($model, 'phone_3',[
                            'template'  => '<div class="col-125  hidden-xs hidden-sm col-md-1 col-sm-1 div-horizontal-all padding-20"><i class="fa fa-minus" aria-hidden="true"></i></div>'
                            . '<div class="col-md-3 clear-padding">{input}</div>'
                        ])
                        ->textInput(['maxlength'=>TRUE, 'class'=>'form-control input-number text-right'])->label(FALSE)
                    ?>
                    <div class="col-md-12 field-loginform-password clear-padding">
                        <div class="form-group"></div>
                        <label class="control-label" for="loginform-password"><?= Yii::t('backend','Password') ?></label>
                    </div>
                    <?=
                        $form->field($model, 'password',[
                            'template'  =>  '{input}{error}'
                        ])
                        ->passwordInput(['maxlength'=>TRUE])->label(FALSE)
                    ?>
                    <?=
                        $form->field($model, 'rememberMe')
                        ->checkbox()
                    ?>
                    <?= Html::submitButton(Yii::t('backend', 'Sign In'), ['class' => 'btn cm-btn-submit', 'name' => 'login-button', 'style' => 'font-size: 16px']) ?>
                    <?php ActiveForm::end(); ?>
                </div>
            </div>
            <div class="col-md-12">
                <label>パスワードを忘れた方は<a href="/site/request-password-reset" style="text-decoration: underline;color: red;"><span style="color: red;">こちら</span></a></label>
            </div>
        </div>
        <div class="col-md-6 col-lg-5 col-sm-12">
            <div class="login-box login-box-custom">
                <div class="login-box-body">
                    <div class="login-logo">
                        <a href="#"> <?= Yii::t('backend', 'If you are not a member yet') ?></a>
                    </div>
                    <div class="content" style="min-height: 120px"></div>
                    <?php //echo Html::a(Yii::t('backend', 'Join Free'),['/member'], ['class' => 'btn login-box-a', 'name' => 'login-button']) ?>
                    <div class="content" style="min-height: 100px"></div>
                    <?php echo Html::a(Yii::t('backend', 'Join Free'),['/member/step1-email/'], ['class' => 'btn cm-btn-submit', 'name' => 'login-button', 'style'=> 'width: 100%; height: 40px; font-size: 16px; line-height: 30px;'])// echo Html::a(Yii::t('backend', 'Registered in the Google information'),['/member/step1-email/'], ['class' => 'btn login-box-a', 'name' => 'login-button']) ?>
                </div>
            </div>
        </div>
    </div>
    <div class="login-logo position-relative clear-marging">
        <p>&nbsp;</p>
    </div>
</div><!-- /.login-box -->
