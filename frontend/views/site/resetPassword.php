<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ResetPasswordForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use dmstr\widgets\Alert;
use frontend\assets\LoginAsset;

LoginAsset::register($this);
$this->registerCssFile('@web/css/member.css', ['depends' => [yii\bootstrap\BootstrapAsset::className()]]);
$this->registerCssFile('@web/css/common.css', ['depends' => [yii\bootstrap\BootstrapAsset::className()]]);
$this->registerJsFile('@web/js/member.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile('@web/js/common-js.js', ['depends' => [\yii\web\JqueryAsset::className()]]);

$this->title = 'Reset password';
?>

<div class="col-md-12 login-box-body login-box-custom position-relative clear-padding-login-box-body">
    <div class="login-logo position-relative">
        <a href="#"> <?= Yii::t('backend', $this->title) ?></a>
    </div>
    <div class="col-md-12 position-relative">
        <div class="col-md-4 col-sm-12"></div>
        <div class="col-md-4 col-sm-12 ">
            <div class="row login-box login-box-custom">
                <div class="login-box-body">
                    <div class="login-logo">
                        <a href="#"> <?= Yii::t('backend', $this->title) ?></a>
                    </div>
                    <div class="form-group"><?= Alert::widget() ?></div>
                    <?php
                    $form = ActiveForm::begin(['id' => 'request-password-reset-form',
                                'enableClientValidation' => false,
                                'fieldClass' => 'justinvoelker\awesomebootstrapcheckbox\ActiveField']);
                    ?>
                    <div class="col-md-12 field-loginform-password clear-padding">
                        <label class="control-label" for="loginform-phone"><?= Yii::t('backend','Please choose your new password:') ?></label>
                    </div>
                    <?=
                        $form->field($model, 'password',[
                            'template'  => '{input}'
                        ])
                        ->passwordInput(['maxlength'=>TRUE, 'class'=>'form-control','autofocus' => true])
                    ?>
                    <?= Html::submitButton(Yii::t('backend', 'Submit'), ['class' => 'btn common-button-submit', 'name' => 'sendmail-button']) ?>
                    <?php ActiveForm::end(); ?>
                </div>            
            </div>
        </div>
    <div class="col-md-4 col-sm-12"></div>
    </div>
    <div class="login-logo position-relative clear-marging">
        <p>&nbsp;</p>
    </div>
</div><!-- /.login-box -->