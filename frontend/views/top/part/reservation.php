<?php
use yii\helpers\Html;
use common\models\BookingBusiness;
use common\models\BookingSeat;
use yii\helpers\Url;
//use Carbon\Carbon;

/* @var $item \common\models\Booking */
$booking_statuses = common\components\Constants::LIST_BOOKING_STATUS;
?>
<div class="shared-box search-item">
	<header role="heading">
		<div class="store-name">
			<?php echo HTML::encode($item->storeMaster->name) ?>

			<div class="fake-btn pull-right booking-status-<?php echo $item->status ?>">
				<?php echo $booking_statuses[$item->status] ?>
			</div>
		</div>
	</header>
	<div class="clearfix"></div>
	<div class="row" role="article">
		<div class="col-sm-6">
			<span class="item-key"><?= Yii::t('frontend','Date Time Top Booking')?>:</span> <?php echo strtr($item->booking_date, '-', '/') ?>
		</div>
		<div class="col-sm-6">
			<span class="item-key"><?= Yii::t('frontend','Money Top Booking') ?>:</span> <span class="item-price">¥<?php echo BookingBusiness::formatMoney($item->booking_total) ?></span>
		</div>

		<div class="clearfix"></div>
		<div class="col-sm-6">
			<span class="item-key"><?= Yii::t('frontend','Product and Option Top Booking') ?>:</span>
			<ul class="list-group list-no-border">
				<?php foreach ($product_coupons as $product_coupon) : $mode = key_exists('display_condition', $product_coupon->attributes) ? 'coupon' : 'product' ?>
					<?php if (key_exists('display_condition', $product_coupon->attributes)){
						$mode = 'coupon';
					}
					elseif ($product_coupon->assign_fee_flg === '1') {
						$mode = 'product'; //old : 'staff';
					}
					else {
						$mode = 'product';
					}
					 ?>
					<li class="list-group-item">
						<?php echo $this->render('reservation-content', [
							'mode' => $mode,
							'item' => $product_coupon,
							'staff'=> $item->staff
						]) ?>
					</li>
				<?php endforeach ?>
			</ul>
		</div>
		<div class="col-sm-6">
			<span class="item-key"><?= Yii::t('frontend','Point Top Booking');?>:</span> <?php echo BookingBusiness::formatPoint(BookingBusiness::getPointFromMoney($item->booking_total, $customer_store)) ?>P
		</div>

		<div class="clearfix"></div>
		<div class="col-sm-6">
			<?php if ($item->storeMaster->booking_resources == '01'): ?>
                <span class="item-key"><?= Yii::t('backend','Seat') ?>:</span>
                <?php $booking_seats = BookingSeat::findFrontEnd()->where(['booking_id' => $item->id])->all() ?>
                <?php foreach ($booking_seats as $booking_seat): ?>
                    <?php if ($booking_seat->seat): ?>
                        <?php echo $booking_seat->seat->name ?><br>
                    <?php endif; ?>
                <?php endforeach; ?>
			<?php else : ?>
                <span class="item-key"><?= Yii::t('frontend','Staff') ?>:</span>
                <?php if ($item->staff): ?>
                    <?php echo $item->staff->name ?>
                <?php endif; ?>
            <?php endif ?>
		</div>
		<div class="col-sm-6">
			<span class="item-key"><?= Yii::t('frontend','Cancel Date') ?>:</span> <?php echo $item->cancelOffset->format('Y-m-d H:i:s'); ?>
		</div>
	</div>
	<div class="clearfix"></div>
	<div class="row shared-toppad-lg">
		<div class="col-xs-12">
			<a href="<?php echo Url::to(['/history/bookingdetail', 'booking_id' => $item->id, 'page' => Yii::$app->request->get('page')]) ?>" class="btn btn-default pull-right">
					<?= Yii::t('frontend','View Booking Top Detail') ?>
			</a>
		</div>
	</div>
</div>
