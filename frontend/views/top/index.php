<?php

use yii\helpers\Html;
use yii\helpers\Url;
use common\models\BookingSearch;
use common\models\BookingBusiness;
use common\models\MstProduct;
use common\models\MasterRank;
use common\components\Util;

$this->title = Yii::t('frontend', 'Meta Store');

frontend\assets\OwlCarouselAsset::register($this);
?>
<?php $this->registerCSSFile('@web/css/history.css',['position' => \yii\web\View::POS_HEAD]);?>
<?php $this->registerCssFile('@web/css/bookinghistory.css', ['depends' => [
    yii\bootstrap\BootstrapAsset::className(),
    'justinvoelker\awesomebootstrapcheckbox\Asset'
]]); ?>
<?php // $rank_setting = MasterRank::findRankbyCompanyId($store['company_id']);  ?>
<div class="clearfix"></div>
<div class="shared-common-container top-page booking-history">
    <div class="row">
        <!-- RANK AND BOOKING -->
        <div class="col-sm-8">
            <!-- RANK INFO -->
            <div class="shared-section-title">
                <?php echo Yii::t('frontend', 'Top Rank Title') ?>
            </div>
            <div class="clearfix"></div>
            <div class="row shared-toppad-lg">

            </div>
            <div class="row">
                <div class="col-xs-12">
                    <?php if ($dataProvider) : ?>
                        <div class="owl-carousel">
                        <?php foreach ($dataProvider as $result) : ?>
                        <?php $rank_setting = MasterRank::findRankbyCompanyId($result['company_id']);  ?>
                            <div class="item">
                                <div class="store-img">
                                    <img src="<?php echo \common\components\Util::getUrlImage($result['image1']) ?>" class="img-responsive">
                                </div>
                                <div class="row shared-toppad-sm"></div>
                                <div>
                                    <?php echo $result['name'] ?>
                                </div>
                                <div>
                                    <?= Yii::t('frontend', 'WHERE') ?>
                                    <?php if($rank_setting['rank_apply_type'] == '00'): ?>
                                            <?php $completeRankCondition = Util::getCompleteRankCondition( $result['rank_id'],$result['total_point'],$result['number_visit'],$result['total_amount'], $rank_setting) ?>
                                            <?php if (key_exists('text_only_condtion', $completeRankCondition)) : ?>
                                                <?php echo $completeRankCondition['text_only_condtion']; ?>
                                            <?php endif; ?>
                                        <?php else: ?>
                                            <?php  $rankUpInfo = Yii::$app->user->identity->customer->getRankInfoInMonth(Yii::$app->user->identity->customer->id, $result['store_id']); ?>
                                            <?php $completeRankCondition = Util::getCompleteRankCondition( $result['rank_id'],$rankUpInfo['total_point'],$rankUpInfo['number_visit'],$rankUpInfo['total_amount'], $rank_setting) ?>
                                            <?php if (key_exists('text_only_condtion', $completeRankCondition)) : ?>
                                                <?php echo $completeRankCondition['text_only_condtion']; ?>
                                            <?php endif; ?>
                                        <?php endif; ?>

                                    <?= Yii::t('frontend', 'RANK') ?>
                                </div>
                                <div>
                                    <a href="<?= Url::base(TRUE).'/member/rank-info-detail?id='.$result['id']?>"><?= Yii::t('frontend', 'REDIRECT RANK INFO'); ?></a>
                                </div>
                                <div class="row shared-toppad-sm"></div>
                            </div>
                        <?php endforeach ?>
                        </div>
                    <?php else : ?>
                        <div class="no_result_search_history"><?= Yii::t('frontend', 'No Result Rank Top') ?></div>
                    <?php endif ?>
                </div>
            </div>
            <!-- END RANK INFO -->
            <!-- BOOKING -->
            <div class="row shared-toppad-lg">

            </div>
            <div class="shared-section-title">
                <?= Yii::t('frontend','Title Booking Top') ?>
            </div>
            <?php if ($dataBooking) : ?>
                <?php foreach ($dataBooking as $id => $item) : ?>
                    <?php echo $this->render('part/reservation', [
                        'item' => $item,
                        'product_coupons' => $product_coupons[$id],
                        'customer_store' => $customer_store[$item['store_id']]
                            ]) ?>
                <?php endforeach ?>
            <?php else : ?>
                <div class="cleafix"></div>
                <div class="row shared-toppad-lg"></div>
                <div class="no_result_search_history"><?=Yii::t('frontend','No Result Booking Top') ?></div>
            <?php endif ?>
            <!-- END BOOKING -->
            <div class="cleafix"></div>
            <div class="row shared-toppad-lg"></div>
        </div>
        <!-- END RANK AND BOOKING -->
        <div class="col-sm-4">
            <div class="col-xs-12 top-notices">
                <div class="row top-notice-title">
                    <h4><?= Yii::t('frontend', 'Title Notice') ?></h4>
                </div>
                <?php if ($notice) { ?>
                    <?php $stt = 0; ?>
                    <?php foreach ($notice as $result_notice) : ?>
                        <?php $stt ++; ?>
                                <?php if ($stt < 6) { ?>
                            <div class="row" style="border-bottom : 1px dotted;margin-top:10px">
                                <div class="form-group" style="padding-left:5px">
                                    <?php echo $result_notice['send_date'] ? $result_notice['send_date'] . ' ' . $result_notice['send_time'] : ''; ?>
                                </div>
                                <div class="form-group" style="padding-left:5px">
                                    <?php echo $result_notice['title'] ? $result_notice['title'] : ''; ?>
                                </div>
                                <div class="form-group" style="padding-left:5px">
                                    <?php echo $result_notice['content'] ? $result_notice['content'] : ''; ?>
                                </div>
                                <div class="form-group pull-right">
                            <?= Yii::t('frontend', 'View Notice Detail'); ?>
                                </div>
                            </div>
                    <?php } ?>
                    <?php endforeach; ?>
                    <div class="row" style= "height:50px; background :whitesmoke;text-align :center">
                        <h4><?= Yii::t('frontend', 'View History Notice') ?></h4>
                    </div>
                <?php } else { ?>
                <div style="padding: 15px;text-align:center";><?= Yii::t('frontend', 'No Result Notice Top') ?></div>
                <?php } ?>
            </div>
        </div>
    </div>
</div>
<div class="clearfix"></div>
<div class="shared-toppad-lg row"></div>
<script>
    (function ($) {
        $('.owl-carousel').owlCarousel({
            margin:20,
            nav:true,
            navText: ['<i class="fa fa-caret-left"></i>', '<i class="fa fa-caret-right"></i>'],
            responsive : {
                0:{
                    items:1
                },
                900:{
                    items:2
                },
                1200: {
                    items:3
                }
            }
        })
        $(".top-page .owl-carousel .item").css("height", "280px");
        $(".top-page .owl-carousel .item").css("max-height", "280px");
    })(jQuery);
</script>
