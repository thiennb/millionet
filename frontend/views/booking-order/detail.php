<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use common\models\CustomerStore;
use common\models\BookingBusiness;


/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = "理美容 POS システム";

frontend\assets\BookingOrderAsset::register($this);
?>
<?php $colspan = empty($seat) ? 2 : 3; ?>
<div class="shared-common-container booking-container">
    <div class="row">
	<div class="col-xs-12" id="header"></div>
            <div class="col-xs-12">
                <?php echo $this->render('_nav', [
                        'step' => 3, 'urls' => []
                ]) ?>
	    </div>
            <div class="reserveWrapper booking-order-custom">
            <div class="reserveContents">
		<?php $request = Yii::$app->request; ?>
		<?php $form = ActiveForm::begin(['id' => 'need-js-validation', 'enableClientValidation' => false, 'fieldConfig' => [
	        'options' => [
	            'tag' => false,
		        ],
		    ]]); ?>
                <div class="common-box">
                    <div class="box-header with-border common-box-h4 col-xs-12" style="margin-top: 0; margin-bottom: 0">
                        <h4 class="text-title"><b><?= Yii::t('frontend', 'Please provide more information') ?></b></h4>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <span class="booking-help-text"><?= $form->errorSummary($model); ?></span>
                    </div>
                </div>
                <table cellspacing="0" class="wFull">
                    <tbody>
                        <tr>
                            <th>
                                <?php echo Yii::t('frontend', 'Booking name') ?>
                            </th>
                            <td class="vaThT">
                                <?php echo HTML::encode($user->first_name . $user->last_name) ?>
                            </td>
                        </tr>
                        <tr>
                            <th>
                                <?php echo Yii::t('frontend', 'Phone number') ?></div>
                            </th>
                            <td class="vaThT">
                                <?php echo HTML::encode($user->mobile) ?>
                            </td>
                        </tr>
                        <tr>
                            <th>
                                <div class="dibBL vaM n iconMustEmpty mR10">&nbsp;</div><div class="dibBL vaM w105">
                                    <?php echo Yii::t('frontend', 'Demand') ?>
                                    <br>
                                    <span class="n" style="margin-left: -10px">（<?php echo Yii::t('frontend', '200 zenkaku characters') ?>）</span>
                                </div>
                            </th>
                            <td class="vaThT">
                                <?php echo $form->field($model, 'demand')->textarea(['rows' => '4', 'maxlength' => '200'])->label(false); ?>
                            </td>
                        </tr>
                                <!-- Question with question option -->
                        <?php foreach($checklists as $qKey => $checklist): ?>
                            <!-- HTML when type is (質問事項) -->
                        <?php if($checklist->type == 2): ?>
                        <tr class="check-row">
                            <th>
                                <div style="word-break: break-all">
                                    <?php echo $checklist->question->question_content; ?> <span class="booking-help-text">※</span>
                                </div>
                                <?php if($checklist->question->option == 2): ?>
                                <i style="font-size: 12px;">(<?php echo Yii::t('frontend', 'Max choice') ?>: <?php echo $checklist->question->max_choice ?>)</i>
                                <?php endif; ?>
                            </th>
                            <td class="vaThT" data-maxchoice="<?php echo $checklist->question->max_choice ?>">
                                <!-- HTML when option is 一つのみ選択 -->
                                <?php if($checklist->question->option == 1): ?>
                                    <?php foreach ($checklist->question->questionAnswers as $aKey => $answer): ?>
                                    <div>
                                        <input type="radio" id="one_<?php echo $checklist->id.'_'.$answer->id ?>" name="checklist[one][<?php echo $checklist->id ?>]" value="<?php echo $answer->id ?>"/>
                                        <label for="one_<?php echo $checklist->id.'_'.$answer->id ?>"><?php echo $answer->content ?></label>
                                    </div>
                                    <?php endforeach; ?>
                                <!-- HTML when option is 複数選択 -->
                                <?php elseif($checklist->question->option == 2): ?>
                                    <?php foreach ($checklist->question->questionAnswers as $aKey => $answer): ?>
                                    <div>
                                        <input type="checkbox" class="max-choice-checkbox" id="multi_<?php echo $checklist->id.'_'.$answer->id ?>" name="checklist[multi][<?php echo $checklist->id ?>][<?php echo $answer->id ?>]"/>
                                        <label for="multi_<?php echo $checklist->id.'_'.$answer->id ?>"><?php echo $answer->content ?></label>
                                    </div>
                                    <?php endforeach; ?>
                                <!-- HTML when option is フリー入力 -->
                                <?php else: ?>
                                    <textarea class="form-control freedom-text" name="checklist[freedom][<?php echo $checklist->id ?>]"></textarea>
                                <?php endif; ?>
                            </td>
                        </tr>
                        <?php
                        // Clear this checklist
                        unset($checklists[$qKey]);
                        endif;?>
                        <?php endforeach; ?>
                        <!-- End Question with question option -->
                    </tbody>
                </table>

                <!-- HTML for checklist when option is (注意事項) -->
                <?php if(count($checklists) > 0): ?>
                <div class="">
                    <div class="common-box">
                        <div class="box-header with-border common-box-h4 col-xs-12">
                            <h4 class="text-title"><b><?= Yii::t('frontend', 'Checklist') ?></b></h4>
                        </div>
                    </div>
                </div>
                <table cellspacing="0" class="wFull">
                    <tbody>
                        <?php foreach($checklists as $qKey => $checklist): ?>
                        <?php if($checklist->type == 1): ?>
                        <tr>
                            <th>
                                <div style="word-break: break-all">
                                    <?php echo $checklist->question->notice_content; ?>
                                    <span class="booking-help-text">※</span>
                                </div>
                            </th>
                            <td class="vaThT">
                                <div><input type="checkbox" id="agree_<?php echo $checklist->id ?>" name="checklist[agree][<?php echo $checklist->id ?>]" class="agree-checkbox"/> <label for="agree_<?php echo $checklist->id ?>"><?php echo Yii::t('frontend', 'Confirmed') ?></label></div>
                            </td>
                        </tr>
                        <?php endif; ?>
                        <?php endforeach; ?>
                    </tbody>
                </table>
                <?php endif; ?>
                <!-- End HTML for checklist with 注意事項 option -->

                <div class="">
                    <div class="common-box">
                        <div class="box-header with-border common-box-h4 col-xs-12">
                            <h4 class="text-title"><b><?= Yii::t('frontend', 'Please set number of points you want to spend') ?></b></h4>
                        </div>
                    </div>
                </div>
                <?php echo $this->render('_error') ?>
                <table cellspacing="0" class="wFull fgThNml pCellV5H5 mT10">
                    <tbody id="load-order-detail">

                    </tbody>
                    <tbody>
                        <tr>
                            <th>
                                <?php echo Yii::t('frontend', 'Using points') ?>
                            </th>
                            <td class="vaThT" colspan="<?php echo $colspan ?>">
                                <div class="mT5">
                                    <input type=hidden id="total-point" value="<?php echo $total_point ?>">
                                    <?php echo $form->field($model, 'point_use', [
                                                'template' => '{input}' . ' &nbsp;P (' . Yii::t('frontend', 'Current points') . '<span id="booking-point-total" class="mL5">' . BookingBusiness::formatPoint($total_point) . '</span> P)'. '{error}'
                                                ])->label(false)->input('text', [ 'id' => 'booking-points', 'class' => 'tfFR w110 imeOff']) ?>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th><?php echo Yii::t('frontend', 'You will need to pay') ?></th>
                            <td colspan="<?php echo $colspan ?>">
                                <span id="booking-need-to-pay">￥0</span> <span class="booking-help-text">※来店時のメニュー変更により、実際のお支払い金額と異なる可能性があります</span>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <div class="submitArea2">
                        <div>
                                <input type="submit" value="予約内容を確認する" class="bS confirmL db offL mHA pointerCursor btn disabled btn-submit" title="予約内容を確認する">
                        </div>
                </div>
				<?php ActiveForm::end(); ?>
            </div>
        </div>

        <div class="col-xs-12">
            <?php if (isset($_SERVER['HTTP_REFERER']) && strpos($_SERVER['HTTP_REFERER'], 'seat?') !== false): ?>
                <?php echo $this->render('_back_button', [
                        'url' => [
                                'seat',
                                'storeId',
                                'couponId',
                                'products',
                                'executeTime',
                                'date',
                                'capacity'
                        ]
                ]) ?>
            <?php else : ?>
                <?php echo $this->render('_back_button', [
                        'url' => '#',
                        'click' => 'javascript:history.back()'
                ]) ?>
            <?php endif; ?>

        </div>
    </div>
</div>
