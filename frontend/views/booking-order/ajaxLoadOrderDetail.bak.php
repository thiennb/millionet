<?php
use yii\helpers\Html;
use common\models\PointSetting;
?>
<?php $idx = -1; $extra_product_idx = -1; $len = count($price_table); $coupon_idx = 0; $product_idx = 0; ?>
<?php foreach ($items as $item): $coupon = $item; ?>
	<?php if (key_exists('listcate', $item)) : ?>
		<tr>
			<th><?php echo Yii::t('frontend', 'Coupon') . ' ' . ($coupon_idx += 1) ?> </th>
			<td class="w478">
				<?php
					$delimiter = array("{",",","}","\"");
					$replace = str_replace($delimiter, $delimiter[0], $coupon["listcate"]);
					$explode = explode($delimiter[0], $replace);

					$cate = array_values(array_filter(array_unique($explode, SORT_REGULAR)));
				?>
				<ul class="couponMenuIcons mT5 fs10 cFix">
					<?php foreach ($cate as $cate_name) : ?>
                                            <?php if ($cate_name !== 'NULL') : ?>
                                                <li class="couponMenuIcon mB5"><?php echo HTML::encode($cate_name) ?></li>
                                            <?php endif ?>
					<?php endforeach ?>
				</ul>
				<?php echo HTML::encode($coupon["name"]) ?>
			</td>
			<td class="w80">
				<div class="dibBL vaM w80"><?php if ($idx < $len) { echo $price_table[$idx += 1]; } else {echo '0';} ?></div>
			</td>
		</tr>
	<?php elseif (key_exists('avatar', $item)) : $astaff = $item; ?>
		<tr>
			<th><?php echo Yii::t('frontend', 'Staff') ?></th>
			<td class="w478">
				<?php echo HTML::encode($astaff["name"]) ?>
			</td>
			<td class="w80">
				<div class="dibBL vaM w80"><?php if ($idx < $len) { echo $price_table[$idx += 1]; }  else {echo '0';} ?></div>
			</td>
		</tr>
	<?php else : $product = $item; ?>
		<?php if ($product_no_option_count > 0) : $product_no_option_count -= 1; ?>
			<tr>
				<th><?php echo Yii::t('frontend', 'Product') . ' ' . ($product_idx += 1)?></th>
				<td class="w478">
					<ul class="couponMenuIcons mT5 fs10 cFix">
						<li class="couponMenuIcon mB5"><?php echo HTML::encode($product['category_name']) ?></li>
					</ul>
					<?php echo HTML::encode($product["name"]) ?>
				</td>
				<td class="w80">
					<div class="dibBL vaM w80"><?php if ($idx < $len) { echo $price_table[$idx += 1]; } else {echo '0';}  ?></div>
				</td>
			</tr>
		<?php elseif (isset($options[$extra_product_idx += 1])) : $option = $options[$extra_product_idx]; ?>
			<tr>
				<th><?php echo HTML::encode($option->name) ?>
				<td class="w478">
					<ul class="couponMenuIcons mT5 fs10 cFix">
						<li class="couponMenuIcon mB5"><?php echo HTML::encode($item['category_name']) ?></li>
					</ul>
					<?php echo HTML::encode($item["name"]) ?>
				</td>
				<td class="w80">
					<div class="dibBL vaM w80"><?php if ($idx < $len) { echo $price_table[$idx += 1]; } else {echo '0';}  ?></div>
				</td>
			</tr>
		<?php endif ?>
	<?php endif ?>
<?php endforeach; ?>
<tr>
	<th><?php echo Yii::t('frontend', 'Total amount') ?></th>
	<td colspan="2">
		￥<span id="booking-money"><?php echo Yii::$app->formatter->asDecimal((float)$total_price, 0) ?></span>
		<span class="booking-help-text">※クーポンのご利用の場合は、割引後の金額になります。</span>
	</td>
</tr>
<?php
$pointSetting = PointSetting::find()->orderBy(['id' => SORT_DESC])->one();
?>
<input type=hidden id="point-yen" value="<?php echo $pointSetting->point_conversion_yen ?>">
<input type=hidden id="point-p" value="<?php echo $pointSetting->point_conversion_p ?>">
<input type=hidden id="total-money" value="<?php echo $total_price ?>">
