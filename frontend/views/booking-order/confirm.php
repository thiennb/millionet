<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use common\models\CustomerStore;
use Carbon\Carbon;


/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

frontend\assets\BookingOrderAsset::register($this);
$this->title = "理美容 POS システム";
$price_idx = 0; $price_length = count($price_table);
?>
<div class="shared-common-container booking-container">
	<div class="row">
	    <div class="col-xs-12" id="header"></div>
		<div class="col-xs-12">
			<?php echo $this->render('_nav', [
				'step' => 4, 'urls' => []
			]) ?>
	    </div>

		<div class="col-xs-12" style="padding-top: 10px;">
			<div class="common-box">
				<div class="box-header with-border common-box-h4">
					<h4 class="text-title"><b><?= Yii::t('frontend', 'Please confirm your booking order') ?></b></h4>
				</div>
			</div>

			<div class="row">
				<span class="booking-help-text">※まだ予約は完了しておりません。最終確認をし「予約を確定する」ボタンを押してください。</span>
			</div>
		</div>

		<div class="reserveWrapper booking-order-custom" style="padding-top: 0">
		<div class="reserveContents">
		<?php $request = Yii::$app->request; ?>
		<?php $form = ActiveForm::begin(['id' => 'bt_reserveActionForm', 'enableClientValidation' => false, 'fieldConfig' => [
			'options' => [
				'tag' => false,
				],
			]]); ?>
				<?php if (count($formModel->errors)) : ?>
					<div class="booking-error show">
				<?php endif ?>
					<?php $request = Yii::$app->request ?>
					<?php // echo $form->field($formModel, 'demand')->hiddenInput()->label(false) ?>
					<?php // echo $form->field($formModel, 'point_use')->hiddenInput()->label(false) ?>
					<?php echo $form->field($formModel, 'storeId')->hiddenInput(['value' => $request->get('storeId')])->label(false) ?>
					<?php echo $form->field($formModel, 'products')->hiddenInput(['value' => $request->get('products')])->label(false) ?>
					<?php echo $form->field($formModel, 'coupons')->hiddenInput(['value' => $request->get('coupons')])->label(false) ?>
					<?php echo $form->field($formModel, 'options')->hiddenInput(['value' => $request->get('options')])->label(false) ?>
					<?php echo $form->field($formModel, 'staff')->hiddenInput(['value' => $request->get('staff')])->label(false) ?>
					<?php echo $form->field($formModel, 'date')->hiddenInput(['value' => $request->get('date')])->label(false) ?>
                    <?php echo $form->field($formModel, 'seat')->hiddenInput(['value' => $request->get('seat')])->label(false) ?>
                    <?php echo $form->field($formModel, 'capacity')->hiddenInput(['value' => $request->get('capacity')])->label(false) ?>
				<?php if (count($formModel->errors)) : ?>
					</div>
				<?php endif ?>
				<?php echo HTML::hiddenInput('_action', 'confirm') ?>
			<table cellspacing="0" class="wFull">
				<tbody>
					<tr>
						<th>
							<?php echo Yii::t('frontend', 'Booking date time') ?>
						</th>
						<td class="vaThT">
							<?php $booking_date_time = Carbon::createFromTimestamp($request->get('date'));
								echo $booking_date_time->format('m 月 d 日 (');
								echo Yii::t('frontend', $booking_date_time->format('D'));
								echo $booking_date_time->format(') H:i') ?>
						</td>
					</tr>
					<tr>
						<th>
							<?php echo Yii::t('frontend', 'Total time') ?>
						</th>
						<td class="vaThT">
							<?php $hours = floor($total_time / 60); $minutes = $total_time % 60; ?>
							<?php if ($hours > 0 ) { echo $hours . '時間 '; } ?>
							<?php if ($minutes > 0 ) { echo $minutes . '分'; } ?>
						</td>
					</tr>
				</tbody>
			</table>

			<table cellspacing="0" class="wFull" style="margin-top: 10px">
				<tbody>
					<?php echo $this->render('ajaxLoadOrderDetail', [
						'items' => $items,
						'price_table' => $price_table,
						'total_price' => $total_price,
						'total_point' => 0,
						'options' => $options,
						'product_no_option_count' => $product_no_option_count,
                        'pointSetting' => $pointSetting,
                        'seat' => $seat,
                        'capacity' => $request->get('capacity')
					]); ?>
                    <?php $colspan = empty($seat) ? 2 : 3 ?>
					<tr>
						<th>
							<?php echo Yii::t('frontend', 'Using points') ?>
						</th>
						<td class="vaThT" colspan="<?php echo $colspan ?>">
							<div class="mT5">
								<?php echo $point ?> (<?php echo Yii::t('frontend', 'Current points') ?> <?php echo $total_point ?> P)
								<!--<p class="fs10 mT2">※入力された電話番号は会員情報に反映されます。</p>-->
							</div>
						</td>
					</tr>
					<tr>
						<th>
							<?php echo Yii::t('frontend', 'You will have to pay') ?>
						</th>
						<td class="vaThT" colspan="<?php echo $colspan ?>">
							<span id="booking-money">￥<?php echo Yii::$app->formatter->asDecimal((float)$final_price, 0) ?></span>
							<span class="booking-help-text">※来店時のメニュー変更により、実際のお支払い金額と異なる可能性があります</span>
						</td>
					</tr>
				</tbody>
			</table>

			<div class="col-xs-12" style="padding: 15px 15px 5px 0px;">
				<span style="font-size: 16px">今回のご来店により <?php echo $point_gained ?> ポイント加算予定です。</span>
				<span class="booking-help-text">※来店時のメニュー変更により、加算ポイントが異なる可能性があります</span>
			</div>

			<table cellspacing="0" class="wFull" style="margin-top: 10px">
				<tbody>
					<tr>
						<th>
							<?php echo Yii::t('frontend', 'Booking name') ?>
						</th>
						<td class="vaThT">
							<?php echo HTML::encode($customer->first_name . $customer->last_name) ?>
						</td>
						<th class="left-bordered">
							<?php echo Yii::t('frontend', 'Phone number') ?>
						</th>
						<td class="vaThT">
							<?php echo HTML::encode($customer->mobile) ?>
						</td>
					</tr>

					<tr>
						<th>
							<?php echo Yii::t('frontend', 'Demand') ?>
						</th>
						<td class="vaThT" colspan="3">
							<?php echo HTML::encode($demand) ?>
						</td>
					</tr>
				</tbody>
			</table>
                        <?php if(count($checklists) > 0): ?>
                        <table cellspacing="0" class="wFull" style="margin-top: 10px">
                            <tbody>
                            <?php foreach($checklists as $checklist): ?>
                            <!-- HTML for checklist -->
                                <tr>
                                    <?php if($checklist->type == 2): ?>
                                    <th>
                                        <div style="word-break: break-all">
                                            <?php echo $checklist->question->question_content; ?>
                                        </div>
                                        <?php if($checklist->question->option == 2): ?>
                                        <i style="font-size: 12px;">(<?php echo Yii::t('frontend', 'Max choice') ?>: <?php echo $checklist->question->max_choice ?>)</i>
                                        <?php endif; ?>
                                    </th>
                                    <td class="vaThT" data-maxchoice="<?php echo $checklist->question->max_choice ?>">
                                        <!-- HTML when option is 一つのみ選択 -->
                                        <?php if($checklist->question->option == 1): ?>
                                            <?php foreach ($checklist->questionAnswers as $aKey => $answer): ?>
                                            <div>
                                                <input type="radio" disabled="true" checked/>
                                                <label><?php echo $answer->content ?></label>
                                            </div>
                                            <?php endforeach; ?>
                                        <!-- HTML when option is 複数選択 -->
                                        <?php elseif($checklist->option == 2): ?>
                                            <?php foreach ($checklist->questionAnswers as $aKey => $answer): ?>
                                            <div>
                                                <input type="checkbox" disabled="true" checked/>
                                                <label><?php echo $answer->content ?></label>
                                            </div>
                                            <?php endforeach; ?>
                                        <!-- HTML when option is フリー入力 -->
                                        <?php else: ?>
                                            <div style="word-break: break-all"><?php echo $checklist->questionAnswers; ?></div>
                                        <?php endif; ?>
                                    </td>
                                    <?php elseif($checklist->type == 1): ?>
                                    <th>
                                        <div style="word-break: break-all">
                                            <?php echo $checklist->question->notice_content; ?>
                                        </div>
                                    </th>
                                    <td class="vaThT">
                                        <div><input type="checkbox" disabled="true" checked/> <label><?php echo Yii::t('frontend', 'Confirmed') ?></label></div>
                                    </td>
                                    <?php endif; ?>
                                </tr>
                            <!-- End HTML for checklist -->
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                        <?php endif; ?>

			<div class="submitArea2">
				<div>
					<input type="submit" name="confirm" value="予約を確定する" class="bS confirmL db offL mHA pointerCursor" title="予約内容を確認する">
				</div>
			</div>
		<?php ActiveForm::end() ?>
		</div>
		</div>

		<div class="col-xs-12">
			<?php echo $this->render('_back_button', [
				'url' => '#',
				'click' => 'javascript:history.back()'
			]) ?>
		</div>
	</div>
</div>
