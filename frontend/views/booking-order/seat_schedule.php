<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use Carbon\Carbon;

/* @var $this yii\web\View */
/* @var $searchModel common\models\BookingSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

frontend\assets\BookingOrderAsset::register($this);
$this->title = "理美容 POS システム";
// Build urls
$request = Yii::$app->request;

$baseQuery  = 'storeId=' . $request->get('storeId') . '&coupons=' . $request->get('coupons') . '&executeTime=' . $request->get('executeTime') . '&products=' . $request->get('products') . '&options=' . $request->get('options');

// 2 tab urls
$toStaffURL = 'seat?' . $baseQuery;
$toCalendarURL = 'seatschedule?' . $baseQuery;

// base url for buttons
$baseURL = $toStaffURL . '&capacity=' . $request->get('capacity') . '&seat=' . $request->get('seat') .  '&date=';

$time_left = $time_open;
$time_right = $time_open;
$time_center = $time_open;
?>
<div class="shared-common-container booking-container">
	<div class="row">
    	<div class="col-xs-12" id="header">
    	</div>
    	<div class="col-xs-12">
    		<?php echo $this->render('_nav', [
    			'step' => 2, 'urls' => []
    		]) ?>
        </div>
    	<div class="col-xs-12">
    		<table cellspacing="0" class="wFull bdCell pCell5 mT20">
    			<tbody>
    				<tr>
    					<th class="w130 fgRbrown bgLGray2 taC " style="width: 20em;">所要時間合計</th>
    					<td id="timeallCoupon">
    						<span class="fgRbrown pT5 b"><?php echo $timeString ?></span>
    					</td>
    				</tr>
    			</tbody>
    		</table>
    	</div>
        <div class="col-xs-12">
            <div class="common-box">
                <div class="box-header with-border common-box-h4 col-xs-12">
                    <h4 class="text-title"><b><?= Yii::t('frontend', 'Please select date time') ?></b></h4>
                </div>
            </div>
        </div>
        <div class="col-xs-12">
            <div class="mT10 pr">
                <ul class="couponTypeFilter cFix">
                    <li>
                        <?= Html::a(Yii::t('frontend', 'Store availability'), $toCalendarURL, ['class' => 'btnCouponTypeFilter isCr btn_timedate_store']) ?>
                    </li>
                    <li>
                        <?php echo Html::a('席タイプ別空き状況', $toStaffURL, ['class' => 'btnCouponTypeFilter jscCouponType btn_styleList']) ?>
                    </li>
                </ul>
            </div>
        </div>
        <div class="col-xs-12" style=" margin: 1em 0 0 0">
            <div id="jsRsvCdTbl" class="ReserveConditionTable underTabContents">
                <div class="coverTable">
                    <div class="whiteTable2">
                        <table id="calendarM" class="innerTable taC nowrap" style="visibility: hidden" cellpadding="0" cellspacing="0">
                            <tbody>
                                <tr id="headCal"></tr>
                                <tr id="dayRow"></tr>
                                <tr id="valRow">
                                    <?php if ( $count > 0 ) : ?>

                                    <!-- Begin Time Left -->
                                    <th class="innerCell">
                                        <table cellpadding="0" cellspacing="0" class="moreInnerTable vaT">
                                            <tbody>
                                                <?php for ($i = 0; $i <= $count; $i++) :?>
                                                        <?php if ($i == 0) : ?>
                                                            <tr><th class="timeCell"><p class="hourR"><?= $time_open ?></p></th></tr>
                                                            <tr><th class="separate"></th></tr>
                                                        <?php else : ?>
                                                            <?php $time_left = date('H:i', strtotime("+30 minutes", strtotime($time_left))); ?>
                                                            <tr><th class="timeCell"><p class="hourR"><?= $time_left ?></p></th></tr>
                                                            <tr><th class="separate"></th></tr>
                                                        <?php endif; ?>
                                                <?php endfor; ?>
                                            </tbody>
                                        </table>
                                    </th>
                                    <!-- End Time Left -->

                                    <!-- Begin Time Body -->
                                    <?php foreach ($model as $day) :?>
                                        <?php if (isset($store_schedule[$day['date']])) : ?>
                                            <?php
                                                $current_schedule = $store_schedule[$day['date']];
                                                $is_holiday = $current_schedule['work_flg'] === '0';
                                                $open_timestamp = Carbon::createFromFormat('Y-m-d H:i', $day['date'] . ' ' . $current_schedule['start_time'])->timestamp;
                                                $close_timestamp = Carbon::createFromFormat('Y-m-d H:i', $day['date'] . ' ' . $current_schedule['end_time'])->timestamp;
                                            ?>
                                        <?php else : ?>
                                            <?php
                                                $is_holiday = false;
                                                $open_timestamp = Carbon::createFromFormat('Y-m-d H:i', $day['date'] . ' ' . $store->time_open)->timestamp;
                                                $close_timestamp = Carbon::createFromFormat('Y-m-d H:i', $day['date'] . ' ' . $store->time_close)->timestamp;

                                            ?>
                                        <?php endif ?>
                                        <?php $real_count = ($close_timestamp - $open_timestamp) / 1800; ?>

                                        <?php $time_center = $time_open;?>
                                        <th class="innerCell">
                                            <table cellpadding="0" cellspacing="0" class="moreInnerTable" data-date="<?= $day['date'] ?>">
                                                <tbody>
                                                    <?php $current_timestamp = time(); ?>
                                                    <?php $invalid_block_html = '
                                                        <td class="openColor" data-time=""><p class="scheduleR"><span class="bS db reserveImpossible offL">×</span></p></td>
                                                        <tr><th class="separate"></th></tr>'; ?>
                                                    <?php $tel_block_html = '
                                                        <td class="closeColor" data-time=""><p class="scheduleR"><span class="bS db reserveTel offL">TEL</span></p></td>
                                                        <tr><th class="separate"></th></tr>'; ?>
                                                    <?php for ($i = 0; $i <= $count; $i++) :?>
                                                        <?php if ($i == 0) : ?>
                                                            <?php $block_date_time = $day['date'] . ' ' . $time_open. ':00' ?>
                                                            <?php $block_timestamp = Carbon::createFromFormat('Y-m-d H:i:s', $block_date_time)->timestamp ?>
                                                            <?php if ($is_holiday || $block_timestamp < $current_timestamp || $block_timestamp < $open_timestamp || $block_timestamp > $close_timestamp || $i > $real_count - $number_row || $seat_count === 0) : ?>
                                                                <?php echo $invalid_block_html ?>
                                                            <?php elseif ($block_timestamp < $current_timestamp + $reservation_possible_seconds): ?>
                                                                <?php echo $tel_block_html ?>
                                                            <?php elseif ( !isset($schedule[$block_date_time]) || count($schedule[$block_date_time]) < $seat_count ) : ?>
                                                                <td class="openColor" data-time="<?= $time_center ?>"><p class="scheduleR"><a href="
                                                                    <?php echo $baseURL . $block_timestamp ?>" title="◎" class="bS db reserveImmediately offL vaT">◎</a></p></td>
                                                                <tr><th class="separate"></th></tr>
                                                            <?php else : ?>
                                                                <?php $found_seat = false ?>
                                                                <?php foreach ($seats as $seat): ?>
                                                                    <?php $schedule = $seat->shifts ?>
                                                                    <?php if ( !isset($schedule[$block_date_time]) || count($schedule[$block_date_time]) < $seat_count ) : ?>
                                                                        <?php $found_seat = true ?>
                                                                    <?php endif ?>
                                                                <?php endforeach; ?>
                                                                <?php if ( $found_seat ) : ?>
                                                                    <td class="openColor" data-time="<?= $time_center ?>"><p class="scheduleR"><a href="
                                                                        <?php echo $baseURL . $block_timestamp ?>" title="◎" class="bS db reserveImmediately offL vaT">◎</a></p></td>
                                                                    <tr><th class="separate"></th></tr>
                                                                <?php else: ?>
                                                                    <?php echo $invalid_block_html ?>
                                                                <?php endif; ?>
                                                            <?php endif; ?>
                                                        <?php else : ?>
                                                            <?php $time_center = date('H:i:00', strtotime("+30 minutes", strtotime($time_center))); ?>
                                                            <?php $block_date_time = $day['date'] . ' ' . $time_center ?>
                                                            <?php $block_timestamp = Carbon::createFromFormat('Y-m-d H:i:s', $block_date_time)->timestamp ?>
                                                            <?php if ($is_holiday || $block_timestamp < $current_timestamp || $block_timestamp < $open_timestamp || $block_timestamp > $close_timestamp || $i > $real_count - $number_row || $seat_count === 0) : ?>
                                                                <?php echo $invalid_block_html ?>
                                                            <?php elseif ($block_timestamp < $current_timestamp + $reservation_possible_seconds): ?>
                                                                <?php echo $tel_block_html ?>
                                                            <?php else : ?>
                                                                <?php $found_seat = false ?>
                                                                <?php foreach ($seats as $seat): ?>
                                                                    <?php $schedule = $seat->shifts ?>
                                                                    <?php if ( !isset($schedule[$block_date_time]) || count($schedule[$block_date_time]) < $seat_count ) : ?>
                                                                        <?php $found_seat = true ?>
                                                                    <?php endif ?>
                                                                <?php endforeach; ?>
                                                                <?php if ( $found_seat ) : ?>
                                                                    <td class="openColor" data-time="<?= $time_center ?>"><p class="scheduleR"><a href="
                                                                        <?php echo $baseURL . $block_timestamp ?>" title="◎" class="bS db reserveImmediately offL vaT">◎</a></p></td>
                                                                    <tr><th class="separate"></th></tr>
                                                                <?php else: ?>
                                                                    <?php echo $invalid_block_html ?>
                                                                <?php endif; ?>
                                                            <?php endif; ?>
                                                        <?php endif; ?>
                                                    <?php endfor; ?>
                                                </tbody>
                                            </table>
                                        </th>
                                    <?php endforeach; ?>
                                    <!-- End Time Body -->

                                    <!-- Begin Time Right -->
                                    <td valign="top">
                                        <table cellpadding="0" cellspacing="0" class="moreInnerTable vaT">
                                            <tbody>
                                                <?php for ($i = 0; $i <= $count; $i++) :?>
                                                        <?php if ($i == 0) : ?>
                                                            <tr><th class="timeCell"><p class="hourR"><?= $time_open ?></p></th></tr>
                                                            <tr><th class="separate"></th></tr>
                                                        <?php else : ?>
                                                            <?php $time_right = date('H:i', strtotime("+30 minutes", strtotime($time_right))); ?>
                                                            <tr><th class="timeCell"><p class="hourR"><?= $time_right ?></p></th></tr>
                                                            <tr><th class="separate"></th></tr>
                                                        <?php endif; ?>
                                                <?php endfor; ?>
                                            </tbody>
                                        </table>
                                    </td>
                                    <!-- End Time Right -->

                                    <?php else : ?>
                                        <td colspan="16"><?= Yii::t('frontend', 'Not Found') ?></td>
                                    <?php endif; ?>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

    	<div class="col-xs-12">
    		<?php /*echo $this->render('_back_button', [
    			'url' => [
    				'aftercouponoption',
    				'storeId',
    				'coupons',
    				'products'
    			]
    		])*/ ?>
    		<?php echo $this->render('_back_button', [
    			'url' => [
    				Yii::$app->request->get('noOption') ? 'index' : 'options',
    				'storeId',
    				'coupons',
    				'products',
    				'options'
    			],
    			'extends' => [
    				'addMenu' => 1
    			]
    		]) ?>
    	</div>
    </div>
</div>
<!--<tr><td class="openColor"><p class="scheduleR"><a href="
    <?php //echo $baseURL . Carbon::createFromFormat('Y/m/d H:i', $day['date'] .' '. $h . ':'. $s1)->timestamp ?>" title="◎" class="bS db reserveImmediately offL vaT">◎</a></p></td>
    <tr><th class="separate"></th></tr>
    <td class="closeColor"><p class="scheduleR"><span class="bS db reserveTel offL">TEL</span></p></td>
    <tr><th class="separate"></th></tr>
    <td class="closeColor"><p class="scheduleR"><span class="bS db reserveImpossible offL">×</span></p></td>
    <tr><th class="separate"></th></tr>-->
<script>
var selectedDay = "<?= $setting['date'] ?>";
var week = "<?= $setting['week'] ?>";
var time_execute = "<?= $setting['time_execute'] ?>";
var store_id = "<?= $setting['store_id'] ?>";
var coupons = "<?= $request->get('coupons') ?>";
var products = "<?= $request->get('products') ?>";
var staff = "<?= $request->get('staff') ?>";
var options = "<?php echo $request->get('options') ?>";
</script>

<?php $this->registerCSSFile('@web/css/salon_header.css',['position' => \yii\web\View::POS_HEAD]);?>
<?php $this->registerCSSFile('@web/css/choose_menu.css',['position' => \yii\web\View::POS_HEAD]);?>
<?php $this->registerJsFile('@web/js/salonschedule.js',['depends' => [\yii\web\JqueryAsset::className()]]); ?>
