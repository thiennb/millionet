<?php use yii\helpers\Html; ?>
<?php use common\components\Util; ?>
<?php foreach ($staffs as $item): ?>
    <div class="modal-container">
        <div class="modal fade shared-modal staff-panel" id="staff-panel-<?php echo $item->id ?>" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-body">
                  <div class="row">
                      <div class="col-sm-3">
                          <div class="staff-panel-avatar">
                              <img src="<?php echo Util::getUrlImage($item->avatar) ?>" alt="スタッフ写真" />
                          </div>
                      </div>
                      <div class="col-sm-9">
                          <p class="staff-panel-catch">
                              <?php echo Html::encode($item->catch) ?>
                          </p>
                          <p>
                              <?php echo Html::encode($item->introduction) ?>
                          </p>
                      </div>
                  </div>
                  <div class="row staff-gallery">
                      <div class="col-xs-12">
                          <?php if (!empty($item->image1)): ?>
                              <figure class="shared-figure col-xs-6">
                                  <img class="staff-panel-small-img" src="<?php echo Util::getUrlImage($item->image1) ?>" alt="紹介写真1" />
                                  <figcaption><?php echo Html::encode($item->image1_note) ?></figcaption>
                              </figure>
                          <?php endif; ?>

                          <?php if (!empty($item->image2)): ?>
                              <figure class="shared-figure col-xs-6">
                                  <img class="staff-panel-small-img" src="<?php echo Util::getUrlImage($item->image2) ?>" alt="紹介写真2" />
                                  <figcaption><?php echo Html::encode($item->image2_note) ?></figcaption>
                              </figure>
                          <?php endif; ?>
                      </div>
                  </div>
                  <div class="clearfix"></div>
                  <div class="row">
                      <div class="col-xs-12">
                          <?php if (count($staff_item_keys)) : ?>
                              <table class="table table-bordered">
                                  <?php foreach ($staff_item_keys as $idx => $key): ?>
                                      <tr>
                                          <th class="col-sm-4">
                                              <?php echo Html::encode($key) ?>
                                          </th>
                                          <td>
                                              <?php echo Html::encode($item['item_option_' . $idx]) ?>
                                          </td>
                                      </tr>
                                  <?php endforeach; ?>
                              </table>
                          <?php endif ?>
                      </div>
                  </div>
              </div>
            </div>
            <button type="button" class="btn-close" data-dismiss="modal" name="button">
                <i class="fa fa-times"></i>
            </button>
          </div>
        </div>
    </div>
<?php endforeach; ?>
<script>
    $('.modal-container').appendTo("body");
</script>
