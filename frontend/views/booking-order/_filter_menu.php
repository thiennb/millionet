<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use common\models\MasterNotice;
use common\components\Constants;
use yii\jui\DatePicker;

/* @var $this yii\web\View */
/* @var $model common\models\Booking */
/* @var $form yii\widgets\ActiveForm */
?>
<table class="couponFilter">
    <tbody>
        <tr>
            <td class="couponFilterInner">
                <div class="">
                    <ul class="couponFilterList booking-category-filter">

                        <?php
							$all_text = [];

                            foreach ($searchModelCategory as $key => $value) {
                                if($value["coupon_count"] == 0){
                                    echo '<li>
                                            <input type="checkbox" name="category"  value="'.$value["id"].'" id="menuCategory'.$value["id"].'" class="basicCheck filter-no-coupons filter-category">
                                            <label for="menuCategory'.$value["id"].'">'.HTML::encode($value["name"]).'</label>
                                          </li>';
                                }else{
									 $all_text[] = $value["name"];
                                     echo '<li>
                                            <input type="checkbox" name="category"  value="'.$value["id"].'" id="menuCategory'.$value["id"].'" class="basicCheck filter-category" data-name="'.$value["name"].'">
                                            <label for="menuCategory'.$value["id"].'">'.HTML::encode($value["name"]).'</label>
                                          </li>';
                                }
                            }
                        ?>

                    </ul>

                </div>
            </td>
            <td class="couponFilterSubmit">
                <input type="button" value="絞り込む" class="btnCssLGray btnCouponFilterSubmit" id="btn-filter">
            </td>
        </tr>
    </tbody>
</table>
<div class="" style="margin-top: 20px">
    <div class="common-box">
        <div class="box-header with-border common-box-h4">
            <h4 class="text-title"><b><?= Yii::t('frontend', 'Choose Menu') ?></b></h4>
        </div>
    </div>
</div>
