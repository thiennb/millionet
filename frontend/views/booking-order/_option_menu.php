<?php
use yii\helpers\Html;
?>

<table cellspacing="0" class="rsvSelectTbl tableFrame jscAddMenuSelect">
    <tbody class="option-menu-table">
        <tr>
            <th class="p8 taC">メニュー</th>
            <th class="p8 w100 taC">料金</th>
            <th class="p8 w110 taC">所要時間 (目安)</th>
        </tr>
		<?php $showed_products = []; ?>
        <?php foreach ($options as $option_id => $products) : ?>
			<?php if (!count($products)) continue; ?>
			<tr>
				<td colspan="3" class="pr-option"><span class="b"><?php echo HTML::encode($option_names[$option_id]) ?></span></td>
			</tr>
			<tr id="jsMenuWrap<?php echo $option_id; ?>" class="pr">
				<td colspan="3" class="pr-name">
					<label for="menu<?php echo $option_id; ?>-1" class="radio">
						<input type="radio" name="menu_options[<?php echo $option_id ?>]" id="menu<?php echo $option_id; ?>-1" value="-1" class="rsvSelectMenuChkbox cbF select-option"
						checked="checked">
						<?php echo Yii::t('frontend', 'No product selected') ?>
					</label>
				</td>
			</tr>
			<?php foreach ($products as $product_id => $product) : ?>
				<tr id="jsMenuWrap<?php echo $product_id; ?>" class="pr">
					<td class="pr-name">
						<div class="pr-category">
							<ul class="couponMenuIcons fs10 cFix">
								<?php if ($product['category_name']) : ?>
									<li class="couponMenuIcon mB5"><?php echo HTML::encode($product['category_name']) ?></li>
								<?php endif ?>
							</ul>
						</div>
						<label for="menu<?php echo $product_id; ?>" class="radio">
							<input type="radio" name="menu_options[<?php echo $product['option_id'] ?>]" id="menu<?php echo $product_id; ?>" value="<?php echo $product['option_id'] ?>-<?php echo $product['id'];?>" class="rsvSelectMenuChkbox cbF select-option">
							<?php echo HTML::encode($product['name']); ?>
						</label>
					</td>
					<td class="pr-price">
						¥<?php echo Yii::$app->formatter->asDecimal((float) $price_table[$product['id']]['total_price'], 0) ?>
					</td>
					<td class="pr-time">
						<?php echo (int)$product['time_require']; ?> 分
					</td>
				</tr>
			<?php endforeach; ?>
        <?php endforeach ?>
    </tbody>
</table>
