<?php
use yii\helpers\Html;
$idx = 0;
?>
<?php foreach ($options as $option_id => $option) : ?>
	<?php if(count($option)) : ?>
		<div class="common-box" style="padding-top: 10px">
			<div class="box-header with-border common-box-h4 col-xs-12">
				<h4 class="text-title">
					<b><?php echo $option_info[$option_id]['name'] ?></b>
				</h4>
			</div>
			<?php if ($option_info[$option_id]['type'] == 1) : $products = $option; ?>
				<table cellspacing="0" class="rsvSelectTbl tableFrame jscAddMenuSelect">
					<tbody class="option-menu-table">
						<tr>
							<th class="p8 taC">メニュー</th>
							<th class="p8 w100 taC">料金</th>
							<th class="p8 w110 taC">所要時間 (目安)</th>
						</tr>
						<tr id="jsMenuWrap<?php echo $option_id ?>" class="pr">
							<td colspan="3" class="pr-name">
								<label for="menu-<?php echo $idx ?>-<?php echo $option_id ?>-1" class="radio">
									<input type="radio" name="menu_options-<?php echo $idx ?>-<?php echo $option_id ?>" id="menu-<?php echo $idx ?>-<?php echo $option_id ?>-1" value="-1" class="rsvSelectMenuChkbox cbF select-option"
									checked="checked">
									<?php echo Yii::t('frontend', 'No product selected') ?>
								</label>
							</td>
						</tr>
						<?php foreach ($products as $product_id => $product): ?>
							<tr id="jsMenuWrap<?php echo $option_id ?>" class="pr">
								<td class="pr-name">
									<div class="pr-category">
										<ul class="couponMenuIcons fs10 cFix">
											<?php if ($product['category_name']) : ?>
												<li class="couponMenuIcon mB5"><?php echo HTML::encode($product['category_name']) ?></li>
											<?php endif ?>
										</ul>
									</div>
									<label for="menu-<?php echo $idx ?>-<?php echo $option_id ?>-<?php echo $product_id ?>" class="radio">
										<input type="radio" name="menu_options-<?php echo $idx ?>-<?php echo $option_id ?>" id="menu-<?php echo $idx ?>-<?php echo $option_id ?>-<?php echo $product_id ?>" value="<?php echo $option_id ?>-<?php echo $product_id ?>-<?php echo $option_info[$option_id]['search_by'] ?>" class="rsvSelectMenuChkbox cbF select-option">
										<?php echo HTML::encode($product['name']) ?>

										<input type="hidden" class="option-name" name="name" value="<?php echo $product['name'] ?>">
										<input type="hidden" class="option-time" name="name" value="<?php echo (int)$product['time_require'] ?>">
										<input type="hidden" class="option-price" name="name" value="<?php echo (float)$price_table[$product_id]['total_price'] ?>">
									</label>
								</td>
								<td class="pr-price">
									¥<?php echo Yii::$app->formatter->asDecimal((float) $price_table[$product_id]['total_price'], 0) ?>
								</td>
								<td class="pr-time">
									<?php echo (int)$product['time_require']; ?> 分
								</td>
							</tr>
						<?php endforeach; ?>
					</tbody>
				</table>
			<?php else : ?>
				<?php foreach ($option as $search_by => $products) : $idx++; ?>
					<div class="option-search-by">
						<?php echo $search_by_names[$option_id][$search_by] ?>
					</div>
					<table cellspacing="0" class="rsvSelectTbl tableFrame jscAddMenuSelect">
					    <tbody class="option-menu-table">
					        <tr>
					            <th class="p8 taC">メニュー</th>
					            <th class="p8 w100 taC">料金</th>
					            <th class="p8 w110 taC">所要時間 (目安)</th>
					        </tr>
							<tr id="jsMenuWrap<?php echo $option_id ?>" class="pr">
								<td colspan="3" class="pr-name">
									<label for="menu-<?php echo $idx ?>-<?php echo $option_id ?>-1" class="radio">
										<input type="radio" name="menu_options-<?php echo $idx ?>-<?php echo $option_id ?>" id="menu-<?php echo $idx ?>-<?php echo $option_id ?>-1" value="-1" class="rsvSelectMenuChkbox cbF select-option"
										checked="checked">
										<?php echo Yii::t('frontend', 'No product selected') ?>
									</label>
								</td>
							</tr>
							<?php foreach ($products as $product_id => $product): ?>
								<tr id="jsMenuWrap<?php echo $option_id ?>" class="pr">
									<td class="pr-name">
										<div class="pr-category">
											<ul class="couponMenuIcons fs10 cFix">
												<?php if ($product['category_name']) : ?>
													<li class="couponMenuIcon mB5"><?php echo HTML::encode($product['category_name']) ?></li>
												<?php endif ?>
											</ul>
										</div>
										<label for="menu-<?php echo $idx ?>-<?php echo $option_id ?>-<?php echo $product_id ?>" class="radio">
											<input type="radio" name="menu_options-<?php echo $idx ?>-<?php echo $option_id ?>" id="menu-<?php echo $idx ?>-<?php echo $option_id ?>-<?php echo $product_id ?>" value="<?php echo $option_id ?>-<?php echo $product_id ?>-<?php echo $search_by ?>" class="rsvSelectMenuChkbox cbF select-option">
											<?php echo HTML::encode($product['name']) ?>

											<input type="hidden" class="option-name" name="name" value="<?php echo $product['name'] ?>">
											<input type="hidden" class="option-time" name="name" value="<?php echo (int)$product['time_require'] ?>">
											<input type="hidden" class="option-price" name="name" value="<?php echo (float)$price_table[$product_id]['total_price'] ?>">
										</label>
									</td>
									<td class="pr-price">
										¥<?php echo Yii::$app->formatter->asDecimal((float) $price_table[$product_id]['total_price'], 0) ?>
									</td>
									<td class="pr-time">
										<?php echo (int)$product['time_require']; ?> 分
									</td>
								</tr>
							<?php endforeach; ?>
						</tbody>
					</table>
				<?php endforeach ?>
			<?php endif ?>
		</div>
	<?php endif ?>
<?php endforeach; exit; ?>
