<div>
    <div class="mT10 pr">
        <ul class="couponTypeFilter cFix booking-type-selection">
            <?php if($type === 'products') : ?>
				<li>
                    <button class="btnCouponTypeFilter filter-mode" value="coupons">
                        <?php echo Yii::t('frontend', 'Choose coupon') ?>
					</button>
                </li>
                <li>
                    <button class="btnCouponTypeFilter isCr" disabled="disabled">
                        <?php echo Yii::t('frontend', 'Choose product') ?>
					</button>
                </li>
			<?php else : ?>
				<li>
                    <button class="btnCouponTypeFilter isCr" disabled="disabled">
                        <?php echo Yii::t('frontend', 'Choose coupon') ?>
					</button>
                </li>
                <li>
                    <button class="btnCouponTypeFilter filter-mode" value="products">
                        <?php echo Yii::t('frontend', 'Choose product') ?>
					</button>
                </li>
			<?php endif ?>
        </ul>

		<div class="booking-pagination">
			<?php if ($current_page == 1 || $total_page == 0) : ?>
				<span class="prev-page" style="display: none">
					◀ <?php echo Yii::t('frontend', 'Prev page') ?>
				</span>
			<?php else : ?>
				<span class="prev-page enabled">
					◀ <?php echo Yii::t('frontend', 'Prev page') ?>
				</span>
			<?php endif; ?>
			<span class="current-page">
				<?php echo $current_page ?>
			</span> /
			<span class="total-page">
				<?php echo $total_page ?>
			</span>
			<?php echo Yii::t('frontend', 'Current page') ?>
			<?php if ($current_page >= $total_page) : ?>
				<span class="next-page" style="display: none">
					<?php echo Yii::t('frontend', 'Next page') ?> ▶
				</span>
			<?php else : ?>
				<span class="next-page enabled">
					<?php echo Yii::t('frontend', 'Next page') ?> ▶
				</span>
			<?php endif; ?>
		</div>
    </div
</div>
