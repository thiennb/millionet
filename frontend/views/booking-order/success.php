<?php $this->title = "理美容 POS システム";
frontend\assets\BookingOrderAsset::register($this);
?>
<div class="shared-common-container booking-container">
    <div class="row">
        <div class="col-xs-12" id="header">
            <!-- LOAD STORE INFORMATION -->
        </div>

        <div class="col-xs-12">
            <?php
            echo $this->render('_nav', [
                'step' => 5, 'urls' => []
            ])
            ?>
        </div>

        <div class="col-xs-12">
            <div class="booking-order-success col-xs-12">
                <div class="col-xs-6">
                    <div class="common-box">
                        <div class="box-header with-border common-box-h4">
                            <h4 class="text-title"><b><?= Yii::t('frontend', 'Booking success') ?></b></h4>
                        </div>
                    </div>
                </div>

                <div class="col-xs-12">
                    <div class="col-xs-12">
                        <?php echo Yii::t('frontend', 'Thank you for completing your booking process') ?>
                        <br>
                        <?php echo Yii::t('frontend', 'Please use app or this site\'s Booking history page to view your booking history') ?>
                    </div>
                </div>

                <div class="col-xs-6" style="margin-top: 20px">
                    <div class="common-box">
                        <div class="box-header with-border common-box-h4">
                            <h4 class="text-title"><b><?= Yii::t('frontend', 'Recommended apps') ?></b></h4>
                        </div>
                    </div>
                </div>

                <div class="col-xs-12">
                    <div class="col-xs-12">
                        <?php echo Yii::t('frontend', 'This store has distributed free useful apps. Hot news and limited coupons only available there. <br> Don\'t miss them out!') ?>
                        <div class="clearfix" style="margin-top: 20px"></div>
                        <div class="col-xs-6">
                            <button type="button" class="btn btn-custom" style="float: right">
                                <img src="/img/google-button.svg" alt="" />
                            </button>
                        </div>
                        <div class="col-xs-6">
                            <button type="button" class="btn btn-custom">
                                <img src="/img/apple-button.svg" alt="" />
                            </button>
                        </div>
                    </div>
                </div>

                <div class="col-xs-12" style="margin-top: 30px; text-align: center">
                    <a href="<?php echo \yii\helpers\Url::to(['top/index/']) ?>" class="btn btn-red" style="margin: 0 auto; max-width: 200px">
                        <?php echo Yii::t('frontend', 'To My Page') ?>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
