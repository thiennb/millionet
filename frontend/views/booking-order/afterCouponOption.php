	<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

frontend\assets\BookingOrderAsset::register($this);
$this->title = "理美容 POS システム";
?>
<div class="shared-common-container booking-container">
	<div class="row">
	    <div class="col-xs-12" id="header">
	    </div>
		<div class="col-xs-12">
			<?php echo $this->render('_nav', [
				'step' => 1, 'urls' => []
			]) ?>
	    </div>

		<div class="col-xs-12">
			<div class="mT50" id="load-option-menu">
			</div>
		</div>

		<div class="col-xs-12" style="padding-top:20px;">
	        <div class="common-box">
	            <div class="box-header with-border common-box-h4 col-xs-12">
	                <h4 class="text-title">
	                    <b><?= Yii::t('frontend', 'Please confirm the chosen menus') ?></b></h4>
	            </div>
	        </div>
	    </div>

	    <div id="load-confirm-menu">

	    </div>

		<div class="col-xs-12">
			<?php echo $this->render('_back_button', [
				'url' => [
					'index',
					'storeId',
					'products',
					'coupons',
					//'options'
				]
			]) ?>
		</div>
	</div>
</div>
