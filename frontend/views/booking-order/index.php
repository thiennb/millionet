<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $searchModel common\models\BookingSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

frontend\assets\BookingOrderAsset::register($this);
$this->title = "理美容 POS システム";
?>

<div class="shared-common-container booking-container">

	<div class="row">
	    <div class="col-xs-12" id="header">
			<!-- LOAD STORE INFORMATION -->
	    </div>
	    <div class="col-xs-12">
			<?php echo $this->render('_nav', [
				'step' => 1, 'urls' => []
			]) ?>
	    </div>
        <div class="row shared-toppad-sm"></div>
        <div class="clearfix"></div>
        <?php if (Yii::$app->session->getFlash('reservation_error')): ?>
            <div class="col-xs-12">
                <div class="booking-error" role="alert" id="booking-error2" style="display: block;">
                    <button type="button" class="close btn-close-alert" style="display: block;" data-dismiss="alert" aria-label="Close">×</button>
                	<label for="booking-error"><?php echo Yii::$app->session->getFlash('reservation_error') ?></label>
                </div>
            </div>
        <?php endif; ?>
        <div class="clearfix"></div>
	    <div class="col-xs-12">
	        <div class="common-box">
	            <div class="box-header with-border common-box-h4">
	                <h4 class="text-title"><b><?= Yii::t('frontend', 'Filter menu') ?></b></h4>
	            </div>
	        </div>
	    </div>

	    <div class="col-xs-12" id="list-filters">
	    </div>

		<div class="col-xs-12">
			<div id="couponTableData"></div>
		</div>

		<div class="col-xs-12">
			<?php echo $this->render('_back_button', [
				'url' => [
					'/'
				]
			]) ?>
		</div>
	</div>
</div>
