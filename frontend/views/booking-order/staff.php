<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use common\components\Util;
use Carbon\Carbon;

/* @var $this yii\web\View */
/* @var $available_staff Array */
// name, avatar, position, career
// introduction ???

$this->title = "理美容 POS システム";

frontend\assets\BookingOrderAsset::register($this);

// Build urls
$request = Yii::$app->request;
$date = $request->get('date');

$baseQuery = 'storeId=' . $request->get('storeId') . '&coupons=' . $request->get('coupons') . '&executeTime=' . $request->get('executeTime') . '&products=' . $request->get('products') . '&options=' . $request->get('options');

// 2 tab urls
$toStaffURL = 'staff?' . $baseQuery;
$toCalendarURL = 'salonschedule?' . $baseQuery;

if ($date) {
    $noStaffURL = 'detail?' . $baseQuery . '&noStaff=1&date=' . $date;
} else {
    $noStaffURL = 'salonschedule?' . $baseQuery . '&noStaff=1&date=' . $date;
}

?>
<div class="shared-common-container booking-container">
    <div class="row">
        <div class="col-xs-12" id="header">
        </div>
        <div class="col-xs-12">
            <!-- BOOKINGS STEPS -->
            <?php
            echo $this->render('_nav', [
                'step' => 2, 'urls' => []
            ])
            ?>
            <!-- END BOOKINGS STEPS -->
            <table cellspacing="0" class="wFull bdCell pCell5 mT20">
                <tbody>
                    <tr>
                        <th class="w130 fgRbrown bgLGray2 taC " style="width: 20em;"><?php echo Yii::t('frontend', 'Total duration (approximate)') ?></th>
                        <td>
                            <span class="fgRbrown pT5 b"><?php echo $timeString ?></span>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="clearfix"></div>
        <div class="col-xs-12" style="margin-top: 15px;">
            <div class="common-box">
                <div class="box-header with-border common-box-h4 col-xs-12">
                    <h4 class="text-title" style="margin-top: 10px;margin-bottom: 10px;"><b>
                        <?php if ($date): ?>
                            <?= Yii::t('frontend', 'Please select staff') ?>
                        <?php else : ?>
                            ご要望があればスタイリストを選択してください
                        <?php endif; ?>
                    </b></h4>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
        <?php if (!$date): ?>
            <div class="col-xs-12">
                <div class="mT10 pr">
                    <ul class="couponTypeFilter cFix">
                        <li>
                            <?= Html::a(Yii::t('frontend', 'Store availability'), $toCalendarURL, ['class' => 'btnCouponTypeFilter btn_timedate_store']) ?>
                        </li>
                        <li>
                            <?= Html::a(Yii::t('frontend', 'Staff availability'), $toStaffURL, ['class' => 'btnCouponTypeFilter isCr jscCouponType btn_styleList']) ?>
                        </li>
                    </ul>
                </div>
            </div>
        <?php endif; ?>
        <div class="clearfix"></div>
        <div class="col-xs-12">
            <?php if ($date): ?>
                <div class="col-xs-12">
                    <span class="arrow-left">指名なしで予約する</span>
                </div>
                <div class="col-xs-12" style="margin-top: 10px;">
                    <?= Html::a('指名なしで次へ', $noStaffURL, ['class' => 'btnCouponTypeFilter btn-pink']) ?>
                </div>
            <?php endif; ?>
        </div>
        <div class="col-xs-12">
            <?php if ($date): ?>
                <div class="col-xs-12" style="margin-top: 10px;">
                    <span class="arrow-left">指名して予約する</span>
                </div>
            <?php endif; ?>
        </div>
        <div class="">
            <div class="pH10">
                <div class="pB30 staff-schedule-list oh underTabContents" style="padding-top: 15px;">
                    <table cellpadding="0" cellspacing="0">
                        <tbody>
                            <?php for ($i = 0; $i < count($available_staff); $i++) : ?>
                                <?php
                                $new_row = $i % 6 === 0;
                                $staff = $available_staff[$i];
                                ?>
                                <?php
                                if ($new_row) {
                                    $current = 0;
                                    echo "<tr>";
                                } $current++;
                                ?>
                            <td class="bdGrayR w148 col-xs-3" valign="top">
                                <div class="w117 mHA">
                                    <div class="staff-avatar" style="cursor: pointer" data-toggle="modal" data-target="#staff-panel-<?php echo $staff['id'] ?>">
                                        <img src="<?php echo Util::getUrlImage($staff['avatar']) ?>" class="bdImgGray w115 h153" oncontextmenu="return false;">
                                    </div>
                                    <p class="mT5 staff-name"><?php echo HTML::encode($staff['name']) ?></p>
                                    <p class="staff-position"><?php echo HTML::encode($staff['position']) ?></p>
                                    <p class="staff-career">（歴<?php echo HTML::encode($staff['career']) ?>年）</p>
                                    <p class="staff-introduction">
                                        <?php echo HTML::encode($staff['catch']) ?>【<a href="#" data-toggle="modal" data-target="#staff-panel-<?php echo $staff['id'] ?>"><?php echo Yii::t('frontend', 'Detail') ?></a>】
                                    </p>
                                    <div class="clearfix"></div>
                                        <?php
                                        if (!isset($price_table[$staff['assign_product_id']])) {
                                            $price_table[$staff['assign_product_id']] = 0;
                                        }
                                        ?>
                                    <p class="staff-price">指名料 : <span class="fgPink">¥<?php echo Yii::$app->formatter->asDecimal((float) $price_table[$staff['assign_product_id']]["total_price"], 0); ?></span></p>
                                    <p>
                                        <?php if ($date): ?>
                                            <?= Html::a('指名して次へ', $baseURL . $staff['id'], ['class' => 'btnCouponTypeFilter btn-staff btn-pink']) ?>
                                        <?php else :?>
                                            <?= Html::a('空き状況を⾒る', $baseURL . $staff['id'], ['class' => 'btnCouponTypeFilter btn-staff btn-pink']) ?>
                                        <?php endif; ?>
                                    </p>
                                </div>
                            </td>
                            <?php
                            if ($current == 6) {
                                echo "</tr>";
                            }
                            ?>
                        <?php endfor ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <?php
        echo $this->render('_back_button', [
            'url' => '#',
            'click' => 'javascript:history.back()'
        ])
        ?>
    </div>
</div>
<?php echo $this->render('part/staff-panels', ['staffs' => $staff_detail, 'staff_item_keys' => $staff_item_keys]) ?>
