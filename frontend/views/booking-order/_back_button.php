<?php
	if (is_array($url)) {
		$finalURL = $url[0];
		$request = Yii::$app->request;

		array_shift($url);

		if (count($url)) {
			$finalURL .= '?' . $url[0] . '=' . $request->get($url[0]);
		}

		array_shift($url);

		foreach ($url as $key) {
			$finalURL .= '&' . $key . '=' . $request->get($key);
		}

		if(isset($extends) && is_array($extends)) {
			foreach ($extends as $key => $value) {
				$finalURL .= '&' . $key . '=' . $value;
			}
		}
	}
	else {
		$finalURL = $url;
	}
?>
<div class="clearfix"></div>
<div class="row" style="margin: 2em 1em 1em 1em">
	<a href="<?php echo $finalURL ?>" id="CT02" class="btnCouponTypeFilter jscCouponType pull-left" style="    padding: 5px;" <?php if(isset($click)) echo 'onclick="' . $click .'";' ?>><?php echo Yii::t('frontend', 'Back') ?></a>
    <a href="#" class="pull-right" onclick="javascript:window.scrollTo(0, $('#header').offset().top)">▲ ページトップへ</a>
</div>
