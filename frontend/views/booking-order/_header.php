<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\Booking */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="rsvSalonHeader">
	<div class="salon-nameWrap cFix">
		<a href="<?php echo Url::home() . Yii::$app->controller->id ?>/index?storeId=<?php echo $search["id"] ?>"><h2 class="salon-name"><?php echo HTML::encode($search["name"]); ?></h2></a>
		<p class="fl"><?php echo HTML::encode($search["name_kana"]) ?></p>
	</div>
</div>
