<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use common\components\Util;
use Carbon\Carbon;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $items Array */
// name, avatar, position, career
// introduction ???

$this->title = "理美容 POS システム";

frontend\assets\BookingOrderAsset::register($this);

// Build urls
$request = Yii::$app->request;
$date = $request->get('date');

$baseQuery = 'storeId=' . $request->get('storeId') . '&coupons=' . $request->get('coupons') . '&executeTime=' . $request->get('executeTime') . '&products=' . $request->get('products') . '&options=' . $request->get('options');

// 2 tab urls
$toStaffURL = 'seat?' . $baseQuery;
$toCalendarURL = 'seatschedule?' . $baseQuery;

$noStaffURL = 'detail?' . $baseQuery . '&noStaff=1&date=' . $request->get('date') . '&seat=0&capacity=' . $request->get('capacity');
?>
<div class="shared-common-container booking-container">
    <?php $form = ActiveForm::begin([
        'action' => $fullURL,
        'method' => 'get',
        'enableClientValidation' => false,
        'fieldConfig' => [
            'options' => [
                'tag' => false,
            ],
        ],
        ]) ?>
    <div class="row">
        <div class="col-xs-12" id="header">
        </div>
        <div class="col-xs-12">
            <!-- BOOKINGS STEPS -->
            <?php
            echo $this->render('_nav', [
                'step' => 2, 'urls' => []
            ])
            ?>
            <!-- END BOOKINGS STEPS -->
            <table cellspacing="0" class="wFull bdCell pCell5 mT20">
                <tbody>
                    <tr>
                        <th class="w130 fgRbrown bgLGray2 taC " style="width: 20em;">所要時間合計</th>
                        <td>
                            <span class="fgRbrown pT5 b"><?php echo $timeString ?></span>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="clearfix"></div>
        <?php if (!$date): ?>
            <div class="col-xs-12" style="margin-top: 15px;">
                <div class="mT10 pr">
                    <ul class="couponTypeFilter cFix">
                        <li>
                            <?= Html::a(Yii::t('frontend', 'Store availability'), $toCalendarURL, ['class' => 'btnCouponTypeFilter btn_timedate_store']) ?>
                        </li>
                        <li>
                            <?php echo Html::a('席タイプ別空き状況', $toStaffURL, ['class' => 'btnCouponTypeFilter isCr jscCouponType btn_styleList']) ?>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="clearfix"></div>
        <?php endif ?>
        <div class="col-xs-12" style="margin-top: 10px;">
            <div class="common-box">
                <div class="box-header with-border common-box-h4 col-xs-12">
                    <h4 class="text-title"><b>予約人数を選択してください</b></h4>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
        <?php $fields = [
            'storeId',
            'coupons',
            'executeTime',
            'products',
            'options',
            'date'
            ] ?>
        <?php foreach ($fields as $field): ?>
            <?php echo $form->field($formModel, $field)->hiddenInput(['name' => $field])->error(false)->label(false) ?>
        <?php endforeach; ?>
        <div class="col-xs-12">
            <div class="row">
                <div class="col-sm-4">
                    <div class="shared-toppad-sm"></div>
                    <?php echo $form->field($formModel, 'capacity')->dropdownList($seat_capacity_list, ['class' => 'form-control', 'name' => 'capacity'])->label(false) ?>
                </div>
                <div class="col-sm-8">
                    <div class="shared-toppad-sm"></div>
                    <button type="submit" class="btnCouponTypeFilter btn-pink">席を検索</button>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="col-xs-12" style="margin-top: 10px;">
            <div class="common-box">
                <div class="box-header with-border common-box-h4 col-xs-12">
                    <h4 class="text-title"><b>席タイプを選択してください</b></h4>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
        <?php if ($date): ?>
            <div class="col-xs-12">
                <div class="col-xs-12">
                    <span class="arrow-left">指名なしで予約する</span>
                    <p>
                        <?php echo Html::submitButton('指名なしで次へ', [
                            'name' => 'seat',
                            'value' => 0,
                            'class' => 'btnCouponTypeFilter btn-staff btn-pink',
                            'style' => 'width: 120px'
                            ]) ?>
                    </p>
                </div>
            </div>
        <?php endif ?>
        <div class="clearfix"></div>
        <div class="col-xs-12">
            <div class="col-xs-12" style="margin-top: 10px;">
                <span class="arrow-left">指名して予約する</span>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="">
            <div class="pH10">
                <div class="pB30 staff-schedule-list oh underTabContents" style="padding-top: 15px;">
                    <table cellpadding="0" cellspacing="0">
                        <tbody>
                            <?php $i = 0; ?>
                            <?php foreach ($items as $seat_id => $item): ?>
                                <?php
                                $new_row = $i % 6 === 0;
                                ?>
                                <?php
                                if ($new_row) {
                                    $current = 0;
                                    echo "<tr>";
                                } $current++;
                                ?>
                            <td class="bdGrayR w148 col-xs-3" valign="top">
                                <div class="w117 mHA">
                                    <div class="staff-avatar" style="cursor: pointer" data-toggle="modal" data-target="#staff-panel-<?php echo $item['id'] ?>">
                                        <img src="<?php echo Util::getUrlImage($item['image1']) ?>" class="bdImgGray w115 h153" oncontextmenu="return false;">
                                    </div>
                                    <p class="mT5 staff-name" style="font-size: 14px">
                                        <span style="color: #FF69B4"><?php echo HTML::encode($item['name']) ?></span>
                                        (最大<?php echo $item['capacity_max'] ?>人)
                                    </p>
                                    <p class="staff-introduction">
                                        【<a href="#" data-toggle="modal" data-target="#staff-panel-<?php echo $item['id'] ?>"><?php echo Yii::t('frontend', 'Detail') ?></a>】
                                    </p>
                                    <div class="clearfix"></div>
                                    <p>
                                        <?php echo Html::submitButton('指名して次へ', [
                                            'name' => 'seat',
                                            'value' => $item['id'],
                                            'class' => 'btnCouponTypeFilter btn-staff btn-pink',
                                            'style' => 'width: 120px'
                                            ]) ?>
                                    </p>
                                    <div class="clearfix"></div>
                                </div>
                            </td>
                            <?php
                            if ($current == 6) {
                                echo "</tr>";
                            }
                            ?>
                            <?php $i++; ?>
                        <?php endforeach ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <?php echo $this->render('_back_button', [
            'url' => [
                'seatschedule',
                'storeId',
                'coupons',
                'products',
                'options',
                'executeTime'
            ]
        ])
        ?>
    </div>
    <?php ActiveForm::end() ?>
</div>
<?php echo $this->render('part/seat-panels', ['items' => $items, 'seat_item_keys' => $seat_item_keys]) ?>
