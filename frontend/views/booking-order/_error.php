<div class="clearfix"></div>
<div class="booking-error" role="alert" id="booking-error">
    <button type="button" class="close btn-close-alert" style="display: block;" data-dismiss="alert" aria-label="Close">&times;</button>
	<label for="booking-error">クーポンまたは商品を選択してください。</label>
</div>
<div class="clearfix"></div>
