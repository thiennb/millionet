<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use common\components\Util;
use Carbon\Carbon;
use common\models\BookingBusiness;

/* @var $model common\models\MasterStaffSearch */
/* @var $form yii\widgets\ActiveForm */
?>
<?php
echo $this->render('_pagination', [
    'current_page' => $current_page,
    'total_page' => $total_page,
    'type' => $type
])
?>
<?php $className = $type === 'products' ? 'product' : 'coupon'; ?>
<?php echo $this->render('_error') ?>
<?php foreach ($search as $key => $value) { ?>
    <div class="clearfix"></div>
    <div class="coupon <?php if ($className === 'coupon') { echo 'coupon-benefit-' . $value['benefits_content']; } ?> <?php if ($type == 'coupons' && $value['combine_with_other_coupon_flg'] != '1') echo 'coupon-non-combinable'; ?>" id="<?php echo $className ?>-<?php echo $value['id'] ?>">
        <table summary="summary" cellspacing="1" class="mT2 wFull bgLGray2 fs10 couponTable">
            <tbody>
                <tr>
                    <!-- ITEM CHECKBOX -->
                    <td class="couponLabelCT01 booking-check-label">
                        <?php
                        echo '
                            <input type="checkbox" name="coupon" couponId="' . $value["id"] . '" value="' . $value["id"] . '" id="menuCoupon' . $key . '" class="basicCheck ' . $className . ' select-booking-' . ($type === 'products' ? 'product' : 'coupon') . '">
                            <label for="menuCoupon' . $key . '"></label>
                            <input name="time_execute" id="time_execute' . $key . '" type="hidden" value="' . $value['time_require'] . '">
                        ';
                        ?>
                    </td>
                    <!-- END ITEM CHECKBOX -->
                    <!-- ITEM MAIN INFO -->
                    <td class="bgWhite booking-list-item-info">
                        <div class="col-xs-12 coupon-categories">
                            <div class="">
                                <div class="">
                                    <ul class="couponMenuIcons fs10 cFix">
                                        <?php if ($type === 'products') : if ($value->category) : ?>
                                            <li class="couponMenuIcon mB5">
                                                <?php echo HTML::encode($value->category->name) ?>
                                            </li>
                                            <?php
                                            endif;
                                        else :
                                            $delimiter = array("{", ",", "}", "\"");
                                            $replace = str_replace($delimiter, $delimiter[0], $value["listcate"]);
                                            $explode = explode($delimiter[0], $replace);

                                            $cate = array_values(array_filter(array_unique($explode, SORT_REGULAR)));

                                            foreach ($cate as $name) :
                                                if ($name !== 'NULL') :
                                            ?>
                                                <li class="couponMenuIcon mB5">
                                                    <?php echo HTML::encode($name) ?>
                                                </li>
                                            <?php endif; endforeach;
                                        endif ?>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <?php
                            // get execute time string
                            $execute_hours = round($value['time_require'] / 60);
                            $execute_minutes = $value['time_require'] % 60;
                            if ($type === 'products') {
                                $image = HTML::encode($value['image1']);
                            } else {
                                $image = HTML::encode($value['image']);
                            }
                            ?>
                            <div class="row" style="padding-top: 15px">
                                <div class="col-xs-12">
                                    <div class="b row coupon-title-wrap">
                                        <div class="col-xs-9 coupon-title text-align-left"><?php echo HTML::encode($value["name"]); ?></div>
                                        <div class="col-xs-3 oh wwbw taR text-align-right">
                                            <span class="fs16 fgPink usingPointDel"><?php
                                            if (isset($price_table[$value['id']])) {
                                                $price_item = $price_table[$value['id']];

                                                echo BookingBusiness::displayMoney($price_item);
                                            } else
                                                echo '¥' . 0;
                                            ?>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="">
                                    <div class="col-xs-2">
                                        <img src="<?php echo Util::getUrlImage($image); ?>" width="150" alt="" class="img-fluid f-width">
                                    </div>
                                    <div class="col-xs-10">
                                        <div class="fgGray text-align-left coupon-description"><?php echo $value["description"] ?></div>
                                        <?php if (isset($value['display_condition'])) : $display_conditions = common\components\Constants::DISPLAY_CONDITION; ?>
                                            <dl class="text-align-left coupon-info">
                                            <dt class="fgPink"><?php echo Yii::t('frontend', 'Displaying condition') ?> : </dt><dd class="oh fgGray wbba">
                                                <?php echo (key_exists($value['display_condition'], $display_conditions)) ? $display_conditions[$value['display_condition']] : '' ?>
                                            </dd>
                                            <br>
                                            <dt class="fgPink"><?php echo Yii::t('frontend', 'Applying condition') ?> : </dt><dd class="oh fgGray wbba">
                                                <?php echo $value['apply_condition'] == 1 ? '対象者限定' : '全員' ?>
                                            </dd>
                                            <br>
                                            <dt class="fgPink"><?php echo Yii::t('frontend', 'Expire date') ?> : </dt><dd class="oh fgGray wbba"><?php
                                                if (!empty($value['expire_date'])) {
                                                    $expire_date = Carbon::createFromFormat('Y-m-d', $value['expire_date']);
                                                    echo $expire_date->year . '年' . $expire_date->month . '月' . $expire_date->day . '日まで';
                                                }
                                                ?></dd>
                                            </dl>
                                        <?php endif ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </td>
                    <!-- END ITEM MAIN INFO -->
                    <!-- ITEM BOOKING BUTTON -->
                    <td class="bgLLGray2 vaM w170 col-xs-3">
                        <div class=" pV10">
                            <div class="">
                                <?php $mode = isset($value['listcate']) ? 'coupon' : 'product' ?>
                                <?php $msg = 'Add ' . $mode . ' to booking menu'; ?>
                                <?= Html::submitButton(Yii::t('frontend', $msg), ['class' => 'btn btn1H50 btnWlines w150 mHA fs13 disabled btn_submitCoupon_check', 'disabled' => 'disabled', 'id' => "submit-" . $className . '-' . $value["id"]]) ?>
                            </div>
                        </div>
                    </td>
                    <!-- END ITEM BOOKING BUTTON -->
                </tr>
            </tbody>
        </table>
    </div>
    <div class="clearfix"></div>
            <?php } ?>
            <?php
            $result = count($search);
            if ($result > 0) {
                ?>
    <div class="">
        <div class=" pV10">
            <div class="form-group">
    <?= Html::submitButton(Yii::t('frontend', 'To next page with this content'), ['class' => 'btn common-button-submit mHA fs13 btn_sumitCoupon']) ?>
            </div>
        </div>
    </div>
<?php } else { ?>
    <div class="nothing-found"></div>
<?php } ?>
<script>
<?php if (isset($non_combinable) && $non_combinable === true): ?>
    bookingCombinable = false;
<?php else : ?>
    bookingCombinable = true;
<?php endif ?>
<?php if (isset($selected_set_coupons)): ?>
    bookingSetCoupons = <?php echo sprintf('[%s];', implode(',', $selected_set_coupons)) ?>
<?php else : ?>
    bookingSetCoupons = [];
<?php endif ?>
</script>
