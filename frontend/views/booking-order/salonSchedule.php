<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use Carbon\Carbon;

/* @var $this yii\web\View */
/* @var $searchModel common\models\BookingSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

frontend\assets\BookingOrderAsset::register($this);
$this->title = "理美容 POS システム";
// Build urls
$request = Yii::$app->request;

$baseQuery  = 'storeId=' . $request->get('storeId') . '&coupons=' . $request->get('coupons') . '&executeTime=' . $request->get('executeTime') . '&products=' . $request->get('products') . '&options=' . $request->get('options');

// 2 tab urls
$toStaffURL = 'staff?' . $baseQuery;
$toCalendarURL = 'salonschedule?' . $baseQuery;

// base url for buttons
$baseURL = $toStaffURL . '&date=';

if (!empty($request->get('staff'))) {
    $baseURL = 'detail?' . $baseQuery . '&staff=' . $request->get('staff') . '&date=';
}

if ($request->get('noStaff')) {
    $baseURL = 'detail?' . $baseQuery . '&noStaff=' . $request->get('noStaff') . '&date=';
}

$time_left = $time_open;
$time_right = $time_open;
$time_center = $time_open;
?>
<div class="shared-common-container booking-container">
	<div class="row">
    	<div class="col-xs-12" id="header">
    	</div>
    	<div class="col-xs-12">
    		<?php echo $this->render('_nav', [
    			'step' => 2, 'urls' => []
    		]) ?>
        </div>
    	<div class="col-xs-12">
    		<table cellspacing="0" class="wFull bdCell pCell5 mT20">
    			<tbody>
    				<tr>
    					<th class="w130 fgRbrown bgLGray2 taC " style="width: 20em;"><?php echo Yii::t('frontend', 'Total duration (approximate)') ?></th>
    					<td id="timeallCoupon">
    						<span class="fgRbrown pT5 b"><?php echo $timeString ?></span>
    					</td>
    				</tr>
    			</tbody>
    		</table>
    	</div>
        <div class="col-xs-12">
            <div class="common-box">
                <div class="box-header with-border common-box-h4 col-xs-12">
                    <h4 class="text-title"><b><?= Yii::t('frontend', 'Please select date time') ?></b></h4>
                </div>
            </div>
        </div>
        <div class="col-xs-12">
            <div class="mT10 pr">
                <ul class="couponTypeFilter cFix">
                    <li>
                        <?= Html::a(Yii::t('frontend', 'Store availability'), $toCalendarURL, ['class' => 'btnCouponTypeFilter isCr btn_timedate_store']) ?>
                    </li>
                    <li>
                        <?= Html::a(Yii::t('frontend', 'Staff availability'), $toStaffURL, ['class' => 'btnCouponTypeFilter jscCouponType btn_styleList']) ?>
                    </li>
                </ul>
            </div>
        </div>
        <div class="col-xs-12" style=" margin: 1em 0 0 0">
            <div id="jsRsvCdTbl" class="ReserveConditionTable underTabContents">
                <div class="coverTable">
                    <div class="whiteTable2">
                        <table id="calendarM" class="innerTable taC nowrap" style="visibility: hidden" cellpadding="0" cellspacing="0">
                            <tbody>
                                <tr id="headCal"></tr>
                                <tr id="dayRow"></tr>
                                <tr id="valRow">
                                    <?php if ( $count > 0 ) : ?>

                                    <!-- Begin Time Left -->
                                    <th class="innerCell">
                                        <table cellpadding="0" cellspacing="0" class="moreInnerTable vaT">
                                            <tbody>
                                                <?php for ($i = 0; $i <= $count; $i++) :?>
                                                        <?php if ($i == 0) : ?>
                                                            <tr><th class="timeCell"><p class="hourR"><?= $time_open ?></p></th></tr>
                                                            <tr><th class="separate"></th></tr>
                                                        <?php else : ?>
                                                            <?php $time_left = date('H:i', strtotime("+30 minutes", strtotime($time_left))); ?>
                                                            <tr><th class="timeCell"><p class="hourR"><?= $time_left ?></p></th></tr>
                                                            <tr><th class="separate"></th></tr>
                                                        <?php endif; ?>
                                                <?php endfor; ?>
                                            </tbody>
                                        </table>
                                    </th>
                                    <!-- End Time Left -->

                                    <!-- Begin Time Body -->
                                    <?php foreach ($model as $day) :?>
                                        <?php $store_open_timestamp = Carbon::createFromFormat('Y-m-d H:i', $day['date'] . ' ' . $store->time_open)->timestamp; ?>
                                        <?php if (isset($store_schedule[$day['date']])) : ?>
                                            <?php
                                                $current_schedule = $store_schedule[$day['date']];
                                                $is_holiday = $current_schedule['work_flg'] === '0';
                                                $open_timestamp = Carbon::createFromFormat('Y-m-d H:i', $day['date'] . ' ' . $current_schedule['start_time'])->timestamp;
                                                $close_timestamp = Carbon::createFromFormat('Y-m-d H:i', $day['date'] . ' ' . $current_schedule['end_time'])->timestamp;
                                            ?>
                                        <?php else : ?>
                                            <?php
                                                $is_holiday = false;
                                                $open_timestamp = $store_open_timestamp;
                                                $close_timestamp = Carbon::createFromFormat('Y-m-d H:i', $day['date'] . ' ' . $store->time_close)->timestamp;

                                            ?>
                                        <?php endif ?>
                                        <?php $real_count = ($close_timestamp - $open_timestamp + $store_open_timestamp) / 1800; ?>

                                        <?php $time_center = $time_open;?>
                                        <th class="innerCell">
                                            <table cellpadding="0" cellspacing="0" class="moreInnerTable" data-date="<?= $day['date'] ?>">
                                                <tbody>
                                                    <?php for ($i = 0; $i <= $count; $i++) :?>
                                                        <?php if ($i == 0) : ?>
                                                            <?php $block_date_time = $day['date'] . ' ' . $time_open. ':00' ?>
                                                            <?php $block_timestamp = Carbon::createFromFormat('Y-m-d H:i:s', $block_date_time)->timestamp ?>
                                                            <?php if ( $is_holiday || $block_timestamp < $open_timestamp || $block_timestamp > $close_timestamp || $i > $real_count - $number_row) : ?>
                                                                <td class="openColor" data-time=""><p class="scheduleR"><span class="bS db reserveImpossible offL">×</span></p></td>
                                                                <tr><th class="separate"></th></tr>
                                                            <?php elseif ( isset($day[$time_open.':00']) && $day[$time_open.':00'] == 1 ) : ?>
                                                                <td class="openColor" data-time="<?= $day[$time_open.':00'] ?>"><p class="scheduleR"><a href="
                                                                    <?php echo $baseURL . Carbon::createFromFormat('Y-m-d H:i', $day['date'] .' '.$time_open)->timestamp ?>" title="◎" class="bS db reserveImmediately offL vaT">◎</a></p></td>
                                                                <tr><th class="separate"></th></tr>
                                                            <?php elseif ( isset($day[$time_open.':00']) && $day[$time_open.':00'] == 2 ) : ?>
                                                                <td class="closeColor" data-time="<?= (isset($day[$time_center])) ? $day[$time_center] : '' ?>"><p class="scheduleR"><span class="bS db reserveTel offL">TEL</span></p></td>
                                                                <tr><th class="separate"></th></tr>
                                                            <?php else: ?>
                                                                <td class="openColor" data-time=""><p class="scheduleR"><span class="bS db reserveImpossible offL">×</span></p></td>
                                                                <tr><th class="separate"></th></tr>
                                                            <?php endif; ?>
                                                        <?php else : ?>
                                                            <?php $time_center = date('H:i:00', strtotime("+30 minutes", strtotime($time_center))); ?>
                                                            <?php $block_date_time = $day['date'] . ' ' . $time_center ?>
                                                            <?php $block_timestamp = Carbon::createFromFormat('Y-m-d H:i:s', $block_date_time)->timestamp ?>
                                                            <?php if ( $is_holiday || $block_timestamp < $open_timestamp || $block_timestamp > $close_timestamp || $i > $real_count - $number_row) : ?>
                                                                <td class="openColor" data-time=""><p class="scheduleR"><span class="bS db reserveImpossible offL">x</span></p></td>
                                                                <tr><th class="separate"></th></tr>
                                                            <?php elseif ( isset($day[$time_center]) && $day[$time_center] == 1 ) : ?>
                                                                <td class="openColor" data-time="<?= $day[$time_center] ?>"><p class="scheduleR"><a href="
                                                                    <?php echo $baseURL . Carbon::createFromFormat('Y-m-d H:i:s', $day['date'] .' '. $time_center)->timestamp ?>" title="◎" class="bS db reserveImmediately offL vaT">◎</a></p></td>
                                                                <tr><th class="separate"></th></tr>
                                                            <?php elseif ( isset($day[$time_center]) && $day[$time_center] == 2 ) : ?>
                                                                <td class="closeColor" data-time="<?= $day[$time_center] ?>"><p class="scheduleR"><span class="bS db reserveTel offL">TEL</span></p></td>
                                                                <tr><th class="separate"></th></tr>
                                                            <?php else: ?>
                                                                <td class="openColor" data-time=""><p class="scheduleR"><span class="bS db reserveImpossible offL">×</span></p></td>
                                                                <tr><th class="separate"></th></tr>
                                                            <?php endif; ?>
                                                        <?php endif; ?>
                                                    <?php endfor; ?>
                                                </tbody>
                                            </table>
                                        </th>
                                    <?php endforeach; ?>
                                    <!-- End Time Body -->

                                    <!-- Begin Time Right -->
                                    <td valign="top">
                                        <table cellpadding="0" cellspacing="0" class="moreInnerTable vaT">
                                            <tbody>
                                                <?php for ($i = 0; $i <= $count; $i++) :?>
                                                        <?php if ($i == 0) : ?>
                                                            <tr><th class="timeCell"><p class="hourR"><?= $time_open ?></p></th></tr>
                                                            <tr><th class="separate"></th></tr>
                                                        <?php else : ?>
                                                            <?php $time_right = date('H:i', strtotime("+30 minutes", strtotime($time_right))); ?>
                                                            <tr><th class="timeCell"><p class="hourR"><?= $time_right ?></p></th></tr>
                                                            <tr><th class="separate"></th></tr>
                                                        <?php endif; ?>
                                                <?php endfor; ?>
                                            </tbody>
                                        </table>
                                    </td>
                                    <!-- End Time Right -->

                                    <?php else : ?>
                                        <td colspan="16"><?= Yii::t('frontend', 'Not Found') ?></td>
                                    <?php endif; ?>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

    	<div class="col-xs-12">
    		<?php /*echo $this->render('_back_button', [
    			'url' => [
    				'aftercouponoption',
    				'storeId',
    				'coupons',
    				'products'
    			]
    		])*/ ?>
    		<?php echo $this->render('_back_button', [
    			'url' => [
    				Yii::$app->request->get('noOption') ? 'index' : 'options',
    				'storeId',
    				'coupons',
    				'products',
    				'options'
    			],
    			'extends' => [
    				'addMenu' => 1
    			]
    		]) ?>
    	</div>
    </div>
</div>
<!--<tr><td class="openColor"><p class="scheduleR"><a href="
    <?php //echo $baseURL . Carbon::createFromFormat('Y-m-d H:i', $day['date'] .' '. $h . ':'. $s1)->timestamp ?>" title="◎" class="bS db reserveImmediately offL vaT">◎</a></p></td>
    <tr><th class="separate"></th></tr>
    <td class="closeColor"><p class="scheduleR"><span class="bS db reserveTel offL">TEL</span></p></td>
    <tr><th class="separate"></th></tr>
    <td class="closeColor"><p class="scheduleR"><span class="bS db reserveImpossible offL">×</span></p></td>
    <tr><th class="separate"></th></tr>-->
<script>
var selectedDay = "<?= $setting['date'] ?>";
var week = "<?= $setting['week'] ?>";
var time_execute = "<?= $setting['time_execute'] ?>";
var store_id = "<?= $setting['store_id'] ?>";
var coupons = "<?= $request->get('coupons') ?>";
var products = "<?= $request->get('products') ?>";
var staff = "<?= $request->get('staff') ?>";
var options = "<?php echo $request->get('options') ?>";
</script>

<?php $this->registerCSSFile('@web/css/salon_header.css',['position' => \yii\web\View::POS_HEAD]);?>
<?php $this->registerCSSFile('@web/css/choose_menu.css',['position' => \yii\web\View::POS_HEAD]);?>
<?php $this->registerJsFile('@web/js/salonschedule.js',['depends' => [\yii\web\JqueryAsset::className()]]); ?>
