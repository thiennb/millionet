<!-- BOOKINGS STEPS -->
<?php
	$texts = [
		'placeholder',
		Yii::t('frontend', 'Select coupon'),
		Yii::t('frontend', 'Select date time'),
		Yii::t('frontend', 'Provide information'),
		Yii::t('frontend', 'Confirm booking'),
		Yii::t('frontend', 'Finish booking')
	];

	$request = Yii::$app->request;

	/*if ($step !== 4) {
		$baseQuery = '?storeId=' . $request->get('storeId') .
			'&couponId=' . $request->get('couponId') .
			'&date=' . $request->get('date') .
			'&executeTime=' . $request->get('executeTime') .
			'&products=' . $request->get('products') .
			'&staff=' . $request->get('staff');
	}
	else {
		$post = $request->post('FrontFormBookingOrder');
		$baseParams = [];

		unset($post['demand']);
		unset($post['isFirstTime']);
		unset($post['points']);

		// confirm page on has post data
		foreach ($post as $key => $value) {
			$baseParams[] = $key . '=' . $value;
		}

		$baseQuery = '?' . implode('&', $baseParams);

		//var_dump($baseQuery); exit;
	}*/
	$baseQuery = '?storeId=' . $request->get('storeId') .
		'&couponId=' . $request->get('couponId') .
		'&date=' . $request->get('date') .
		'&executeTime=' . $request->get('executeTime') .
		'&products=' . $request->get('products') .
		'&staff=' . $request->get('staff');

	$passive2 = $step > 2 ? 'passive' : '';//($step < 2 && empty($request->get('executeTime'))) || $step === 5 ? 'passive' : '';

    if ($step === 2) {
        $passive2 .= ' active';
    }

	$passive3 = $step > 3 ? 'passive' : '';//($step < 3 && empty($request->get('staff'))) || $step === 5  ? 'passive' : '';

	$passive1 = $step !== 1  ? 'passive' : 'active'; //$step === 5  ? 'passive' : '';

	$passive4 = $step > 4  ? 'passive' : '';

	$passive5 = $step > 5  ? 'passive' : '';
?>
<ol class="rsvStepList cFix step-list step-<?php echo $step ?>">
	<li class="rslStep1 <?php echo $passive1 ?>">
		<span>STEP1</span>
		<?php echo $texts[1] ?>
	</li>
	<li class="rslStep2 <?php echo $passive2 ?>">
		<span>STEP2</span>
		<?php echo $texts[2] ?>
	</li>
	<li class="rslStep3 <?php echo $passive3 ?>">
		<span>STEP3</span>
		<?php echo $texts[3] ?>
	</li>
	<li class="rslStep4 <?php echo $passive4 ?>">
		<span>STEP4</span>
		<?php echo $texts[4] ?>
	</li>
	<li class="rslStep5 <?php echo $passive5 ?>">
		<?php echo $texts[5] ?>
	</li>
</ol>
<!-- END BOOKINGS STEPS -->
