<?php

use yii\helpers\Html;
use Carbon\Carbon;
use common\models\BookingBusiness;

$display_conditions = common\components\Constants::DISPLAY_CONDITION;
?>
<div class="col-xs-12">
    <div class="mT20">
        <table cellspacing="0" class="jscCalculatedTable tableFrame rsvSelectedTbl">
            <tbody style="background: white;" id="confirm_menu_content">
                <tr>
                    <th colspan="1" class="rsvSelectedHead taC">選択済メニュー</th>
                    <th class="w100 taC">料金</th>
                    <th class="w110 taC">所要時間(目安)</th>
                </tr>
                <?php
                foreach ($searchConfirmmenu as $idx => $value) {
                    $type = (key_exists('display_condition', $value)) ? 'coupon' : 'product';
                    ?>
                    <tr>
                        <td class="rsvSelectedCoupon">
                            <ul class="couponMenuIcons fs10 cFix">
                                <?php if (isset($value["listcate"])) : ?>
                                    <?php
                                    $delimiter = array("{", ",", "}", "\"");
                                    $replace = str_replace($delimiter, $delimiter[0], $value["listcate"]);
                                    $explode = explode($delimiter[0], $replace);

                                    $cate = array_values(array_filter(array_unique($explode, SORT_REGULAR)));
                                    foreach ($cate as $c) :
                                        ?>
                                        <?php if ($c !== 'NULL') : ?>
                                            <li class="couponMenuIcon mB5"><?php echo HTML::encode($c) ?></li>
                                        <?php endif ?>
                                    <?php endforeach ?>
                                <?php elseif ($value['category_name']) : ?>
                                    <li class="couponMenuIcon mB5"><?php echo HTML::encode($value['category_name']) ?></li>
                                <?php endif ?>
                            </ul>
                            <p class="b fgDGray pT5 wbba"><?php echo HTML::encode($value['name']); ?></p>
                            <p class="pT10 wbba"><?php echo $value['description'] ?></p>
                            <?php if ($type === 'coupon') : ?>
                                <dl class="mT5 fs10 wbba">
                                    <dt class="di fgPink">提示条件：</dt><dd class="di mR10 pR10 fgGray bdLGrayR">
                                        <?php echo (key_exists($value['display_condition'], $display_conditions)) ? $display_conditions[$value['display_condition']] : '' ?>
                                    </dd>
                                    <dt class="di fgPink">利用条件：</dt><dd class="di mR10 pR10 fgGray bdLGrayR">
                                        <?php echo $value['apply_condition'] == 1 ? '対象者限定' : '全員' ?>
                                    </dd>
                                    <dt class="di fgPink">有効期限：</dt><dd class="di mR20 fgGray">
                                        <?php
                                        if (!empty($value['expire_date'])) {
                                            sscanf($value['expire_date'], '%d-%d-%d', $year, $month, $day);
                                            echo $year . '年' . $month . '月' . $day . '日まで';
                                        }
                                        ?>
                                    </dd>
                                </dl>
                            <?php endif ?>
                        </td>
                        <td class="w100 wbba taC">
                            <?php $price_table = ($type === 'coupon') ? $coupon_price_table : $product_price_table; ?>
                            <?php echo BookingBusiness::displayMoney($price_table[$value['id']]) ?>
                        </td>
                        <td class="w110 wbba jsRsvMenuTimes taC" ><?php echo (int) $value["time_require"]; ?> 分</td>
                    </tr>
                <?php } ?>

            </tbody>
            <tbody id="confirm_menu_addon"></tbody>
            <tbody id="confirm_menu_total">
                <tr>
                    <th class="taR" colspan="2"><?php echo Yii::t('frontend', 'Total execute time') ?></th>
                    <td class="rsvSelectedTotalTime jscTotalTime taC totaltime" id="total-time">
                        <?php
                        if ($hour > 0) {
                            echo $hour . '時間';
                        }

                        if ($minute > 0) {
                            echo $minute . '分';
                        }
                        ?>
                    </td>
                </tr>
            </tbody>
        </table>
        <div class="pV10">
            <div class="shared-alert-info no-option-alert">オプションの商品をまだ選択していません。</div>
        </div>
        <div class=" pV10">
            <div class="form-group">
                <?= Html::submitButton(Yii::t('frontend', 'To next page with this content'), ['class' => 'btn common-button-submit mHA fs13 btn_to_time_schedule']) ?>
            </div>
        </div>
    </div>
</div>
<input type="hidden" id="base-time" value="<?php echo $total_time ?>">
