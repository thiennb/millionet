<?php

namespace common\components;

use Yii;
use yii\db\BaseActiveRecord;
use yii\behaviors\AttributeBehavior;
use common\components\Util;


class CompanyIdBehavior extends AttributeBehavior
{
    /**
     * @var string the attribute that will receive current user ID value
     * Set this property to false if you do not want to record the creator ID.
     */
    public $company_id = 'company_id';
    /**
     * @var string the attribute that will receive current user ID value
     * Set this property to false if you do not want to record the updater ID.
     */

    /**
     * @inheritdoc
     *
     * In case, when the property is `null`, the value of `Yii::$app->user->id` will be used as the value.
     */
    public $value;

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        if (empty($this->attributes)) {
            $this->attributes = [
                BaseActiveRecord::EVENT_BEFORE_INSERT => $this->company_id,
                BaseActiveRecord::EVENT_BEFORE_UPDATE => $this->company_id,
            ];
        }
    }

    /**
     * @inheritdoc
     *
     * In case, when the [[value]] property is `null`, the value of `Yii::$app->user->id` will be used as the value.
     */
    protected function getValue($event)
    {
        if ($this->value === null && \Yii::$app->controllerNamespace != "api\controllers") {
            
            $company_id = Util::getCookiesCompanyId();
            if(empty($company_id)){
                $store_id = Yii::$app->session->get('store_id_rg');
                $store = \common\models\MasterStore::findOne($store_id);
                if(!empty($store)){
                    Util::setCookiesCompanyId($store->company_id);
                }else{
                    Util::setCookiesCompanyId(\common\models\Company::find()->one()->id);
                }
                
            }
            return  Util::getCookiesCompanyId();
        }
        
        return parent::getValue($event);
    }
}