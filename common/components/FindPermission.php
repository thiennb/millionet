<?php

namespace common\components;
use Yii;
use common\components\Util;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class FindPermission {

    public function getPermissionCondition($table_name, $store_id_flg = true, $store_code_flg = false, $order_code_flg = false) {
        $condition = [];
        array_push($condition, ['or', [$table_name . '.del_flg' => '0'], [$table_name . '.del_flg' => null]]);
        if (\Yii::$app->controllerNamespace != "backend\controllers") {
            return array_merge(['AND'], $condition);
        }
        $role = self::getRole();
        if ($store_id_flg) {
            if ($role->permission_id == Constants::STORE_MANAGER_ROLE || $role->permission_id == Constants::STAFF_ROLE) {
                array_push($condition, [$table_name . '.store_id' => $role->store_id]);
            }
        } elseif ($store_code_flg) {
            if ($role->permission_id == Constants::STORE_MANAGER_ROLE || $role->permission_id == Constants::STAFF_ROLE) {
                $store_code = \common\models\MasterStore::findOne($role->store_id);
                array_push($condition, [$table_name . '.store_code' => $store_code->store_code]);
            }
        } elseif ($order_code_flg) {
            if ($role->permission_id == Constants::STORE_MANAGER_ROLE || $role->permission_id == Constants::STAFF_ROLE) {
                $store_code = \common\models\MasterStore::findOne($role->store_id);
                array_push($condition, ['left(' . $table_name . '.order_code, 5)' => $store_code->store_code]);
            }
        }

        return array_merge(['AND'], $condition);
    }

    public static function permissionBeforeAction($condition = false, $store_id = null) {
        if (!$condition) {
            throw new \yii\web\BadRequestHttpException( Yii::t("backend", 'The requested page is not permissed') );
        }
        if (!is_null($store_id)) {
            $role = self::getRole();
            if ($role->permission_id == Constants::STORE_MANAGER_ROLE || $role->permission_id == Constants::STAFF_ROLE) {
                if ($role->store_id != $store_id) {
                    throw new \yii\web\BadRequestHttpException(Yii::t("backend", "Don't update orther store"));
                }
            } else {
               // $company = \common\models\CompanyStore::findOne(['store_id' => $store_id]);
                $company = \common\models\Company::find()->innerJoin('mst_store', 'company.id = mst_store.company_id')->where(['mst_store.id' => $store_id])->one();

                if(is_null($company->id) || $company->id != Util::getCookiesCompanyId()){
                    throw new \yii\web\BadRequestHttpException(Yii::t("backend", "Please do not update other companies"));
                }
            }
        }
    }

    public static function getRole() {
        $role = \Yii::$app->user->identity;
        if (is_null($role)) {
            throw new \yii\web\BadRequestHttpException(Yii::t("backend", 'Please login'));
        }
        return $role;
    }

}
