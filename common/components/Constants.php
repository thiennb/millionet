<?php

namespace common\components;

/**
 * Description of Constant
 *
 * @author thiennb
 */
use Yii;

class Constants {

    const WORK_HOUR = [
        "08:00" => "08:00",
        "08:30" => "08:30",
        "09:00" => "09:00",
        "09:30" => "09:30",
        "10:00" => "10:00",
        "10:30" => "10:30",
        "11:00" => "11:00",
        "11:30" => "11:30",
        "12:00" => "12:00",
        "12:30" => "12:30",
        "13:00" => "13:00",
        "13:30" => "13:30",
        "14:00" => "14:00",
        "14:30" => "14:30",
        "15:00" => "15:00",
        "15:30" => "15:30",
        "16:00" => "16:00",
        "16:30" => "16:30",
        "17:00" => "17:00",
        "17:30" => "17:30",
        "18:00" => "18:00",
        "18:30" => "18:30",
        "19:00" => "19:00",
        "19:30" => "19:30",
        "20:00" => "20:00",
        "20:30" => "20:30",
        "21:00" => "21:00",
        "21:30" => "21:30",
        "22:00" => "22:00",
    ];
    const LIST_TYPE = [
        0 => '通常',
        1 => '予約時',
        2 => '予約○時間前',
        3 => '会計○時間後',
        4 => '会員登録時',
        5 => '会員登録○時間後',
        6 => '会員登録時',
        7 => '来店時',
    ];
    const LIST_OBJECTS = [
        0 => 'Object 1',
        1 => 'Object 2',
        2 => 'Object 3',
        3 => 'Object 4',
    ];
    const LIST_DILIVERY_AREA = [
        0 => '半径 100メートル',
        1 => '半径 500メートル',
        2 => '半径 1000 メートル',
        3 => '指定なし',
    ];
    const LIST_SEX = [
        0 => "男性",
        1 => '女性',
    ];
    const LIST_BOOKING_STATUS = [
        '01' => '仮予約',
        '02' => '予約中',
        '03' => '完了',
        '04' => 'キャンセル',
        '05' => '予約拒否',
        '06' => 'キャンセル' // cancel by customer
    ];
    const LIST_BOOKING_STATUS_KANRI = [
        '01' => '仮予約',
        '02' => '予約中',
        '03' => '完了',
        '04' => '店舗キャンセル',
        '05' => '予約拒否',
        '06' => '会員キャンセル '// cancel by customer
    ];
    const LIST_BOOKING_METHOD = [
        '01' => '電話',
        '02' => '直接来店',
        '03' => '予約サイト',
        '04' => 'その他',
    ];
    const LIST_CONDITION_VISIT = [
        1 => '以上',
        2 => '以下',
        3 => '一致',
    ];
    const LIST_CONDITION_LAST_VISIT = [
        1 => '以前',
        2 => '以降',
        3 => '一致',
    ];
    const LIST_NEWS_TRANSMISION = [
        1 => '送信可',
        0 => '送信不可',
    ];
    const LIST_PERMISSION = [
        0 => "運用管理者",
        1 => '企業管理者',
        2 => '店舗管理者',
        3 => '店舗スタッフ',
    ];
    const LIST_TAX_INTERNAL_FOREIGN = [
        '00' => '内税',
        '01' => '外税',
        '02' => '非課税',
    ];
    const LIST_SHOW = [
        1 => "表示",
        0 => '非表示',
    ];
    const LIST_SMOKING = [
        0 => "喫煙可", //Smoking 
        1 => "禁煙", // No Smoking 
    ];
    const CAST_REGISTER = [
        0 => "対象商品を自動読込",
        1 => "読込済対象商品に適用する",
    ];
    const COUPON_DISPLAY = [
        0 => "会員アプリのみ",
        1 => "レジのみ",
        2 => "両方",
        3 => "表示しない",
    ];
    const COMBINATION = [
        0 => "併用可能",
        1 => "併用不可",
    ];
    const BENEFIT_CONTENTS = [
        '00' => '値引',
        '01' => '割引',
        '02' => 'セット価格',
        '03' => '食べ飲み放題',
    ];
    const SIZE = [
        0 => '大',
        1 => '中',
        2 => '小',
    ];
    const POSITIONS = [
        0 => '画像上',
        1 => '画像下',
    ];
    const RANK = [
        '00' => 'ノーマル',
        '01' => 'ブロンズ',
        '02' => 'シルバー',
        '03' => 'ゴールド',
        '04' => 'ブラック',
    ];
    const STAFF = [
        0 => 'Staff 1',
        1 => 'Staff 2',
        2 => 'Staff 3',
        3 => 'Staff 4',
    ];
    const CAREFUL_CHECK = [
        '1' => '要注意チェック会員を含む',
        '0' => '要注意チェック会員を含まない',
    ];
    const LIST_CONDITION_RANK = [
        '00' => "すべて達成",
        '01' => "2つ以上達成",
        '02' => "1つ以上達成"
    ];
    const LIST_STATUS_ENJOY_LOTTERY = [
        '00' => "発表前",
        '01' => "発表済み"
    ];
    const LIST_SHOW_COUPON = [
        0 => '会員アプリ',
        1 => 'レシート出力',
    ];
    const DISTANCE_TO_SHOP = [
        0 => '100',
        1 => '500',
        2 => '1000',
    ];
    const DELIVERY_STATUS = [
        '01' => "配信待ち",
        '02' => "配信済み",
        '03' => "配信失敗",
        '04' => "配信停止",
    ];
    //'01' => "配信待ち",(DOING)
    const DOING_DELIVERY_STATUS = '01';
    const LIST_RANKUP_TYPE = [
        "00" => "即時ランクアップ",
        '01' => "翌月ランクアップ"
    ];
    const JAN_TYPE_CUSTORMER = '20';
    const JAN_TYPE_PRODUCT = '21';
    const JAN_TYPE_TICKET = '23';
    const JAN_TYPE_COUPON = '22';
    const CHECK_JAN_PRODUCT = [20,22,23,24,25,26,27,28,29];
    // Datpdt 19/09/2016 start
    const LIST_BENEFIT_CONTENT = [
        '00' => '値引',
        '01' => '割引',
        '02' => 'セット価格',
        '03' => '食べ飲み放題'
    ];
    // Datpdt 19/09/2016 end

    const LIST_OPTION_SELECT = [
        0 => "商品ごとにオプションを選択する",
        1 => "会計全体でオプションを選択する"
    ];
    // Datpdt 21/09/2016 start
    const LIST_OPTION_SELECT_TIME = [
        "00:00" => "00:00",
        "00:30" => "00:30",
        "01:00" => "01:00",
        "01:30" => "01:30",
        "02:00" => "02:00",
        "02:30" => "02:30",
        "03:00" => "03:00",
        "03:30" => "03:30",
        "04:00" => "04:00",
        "04:30" => "04:30",
        "05:00" => "05:00",
        "05:30" => "05:30",
        "06:00" => "06:00",
        "06:30" => "06:30",
        "07:00" => "07:00",
        "07:30" => "07:30",
        "08:00" => "08:00",
        "08:30" => "08:30",
        "09:00" => "09:00",
        "09:30" => "09:30",
        "10:00" => "10:00",
        "10:30" => "10:30",
        "11:00" => "11:00",
        "11:30" => "11:30",
        "12:00" => "12:00",
        "12:30" => "12:30",
        "13:00" => "13:00",
        "13:30" => "13:30",
        "14:00" => "14:00",
        "14:30" => "14:30",
        "15:00" => "15:00",
        "15:30" => "15:30",
        "16:00" => "16:00",
        "16:30" => "16:30",
        "17:00" => "17:00",
        "17:30" => "17:30",
        "18:00" => "18:00",
        "18:30" => "18:30",
        "19:00" => "19:00",
        "19:30" => "19:30",
        "20:00" => "20:00",
        "20:30" => "20:30",
        "21:00" => "21:00",
        "21:30" => "21:30",
        "22:00" => "22:00",
        "22:30" => "22:30",
        "23:00" => "23:00",
        "23:30" => "23:30",
    ];
    const LIST_SHOW_BOOKING = [
        "01" => "席タイプ選択",
        "02" => "スタッフ選択",
        "00" => "どちらも選択しない",
    ];
    const LIST_SHOW_STORE = [
        1 => "表示する",
        0 => '表示しない',
    ];
    const POINT_HISTORY = [
        //"0" => "すべて",
        "0" => "付与",
        "1" => "使用",
        "2" => "増加",
        "3" => "減少",
    ];
    const Print_Receipt = [
        "00" => "予約サイト・会員アプリ",
        "01" => " レシート出力",
    ];
    // Datpdt 21/09/2016 end    
    // Lylm 12/09/2016 start
    const LIST_OPTION_SELECT_POINT_GRANT = [
        1 => "付与する",
        0 => "付与しない",
    ];
    const LIST_BOOKING_MENTHOD = [
        "01" => "電話",
        "02" => "直接来店",
        "03" => "予約サイト",
        "04" => "その他",
    ];
    const DISPLAY_CONDITION = [
        "01" => "予約時",
        "02" => "入店時",
        "03" => "着席時",
        "04" => "注文時",
        "05" => "受付時",
        "06" => "会計時",
        "07" => "予約時＆入店時",
        "08" => "予約時＆着席時",
        "09" => "予約時＆注文時",
        "10" => "予約時＆受付時",
        "11" => "予約時＆会計時",
        "12" => "入店時＆着席時",
        "13" => "入店時＆注文時",
        "14" => "入店時＆受付時",
        "15" => "入店時＆会計時",
        "16" => "着席時＆注文時",
        "17" => "着席時＆受付時",
        "18" => "着席時＆会計時",
        "19" => "注文時＆受付時",
        "20" => "注文時＆会計時",
        "21" => "受付時＆会計時",
    ];
    const WORK_DAY = [
        "1" => "月",
        "2" => "火",
        "3" => "水",
        "4" => "木",
        "5" => "金",
        "6" => "土",
        "0" => "日"
    ];
    const TYPE_CHECKLIST = [
        "1" => "注意事項",
        "2" => "質問事項",
    ];
    const DISPLAY_FLG = [
        "0" => "表示する",
        "1" => "表示しない",
    ];
    const CHOSSE_OPTION = [
        "1" => "一つのみ選択",
        "2" => "複数選択",
        "3" => "フリー入力",
    ];
    //HoangNQ 
    //予約時リソース選択 : booking_resources in mst_store
    //01 : 席タイプ選択 (Select seat type)
    const SELECT_SEAT_TYPE = "01";
    //02 : スタッフ選択 (Select staff)
    const SELECT_STAFF = "02";
    //00 : どちらも選択しない (None)
    const SELECT_NONE = "00";
    //Auto Send Nitoce
    //01: 予約時 
    const PUSH_TIMMING_AT_BOOKING = "01";
    //02: 予約...時間前 
    const PUSH_TIMMING_BEFORE_BOOKING = "02";
    //03: 会員登録時 ( Khi dang ki member)
    const PUSH_TIMMING_AT_REGISTER = "03";
    //04: 会員登録から (Sau khi dang ki)
    const PUSH_TIMMING_AFTER_REGISTER = "04";
    //05: 会計から (Sau khi thanh toan)
    const PUSH_TIMMING_AFTER_CHARGE = "05";
    //06: 店舗から (Den gan cua tiem cach xxx met)
    const PUSH_TIMMING_NEAR_STORE = "06";
    //VIEW CONSTANT CSS
    const DIV = 'div';
    const OPTION_DIV_CLASS_12 = ['class' => 'col-md-12'];
    const OPTION_DIV_SEARCH_LABEL = ['class' => 'col-lg-3 col-md-4 label-margin label_widget_notice_ct'];
    const OPTION_DIV_MIDDLE_LABEL = ['class' => 'col-md-1 label-margin label-center separator'];
    const OPTION_DIV_SEARCH_CONTENT = ['class' => 'col-lg-3 col-md-4 input_widget_notice_ct'];
    
    //Perminssion role
    
    //0 => "運用管理者",
    const APP_MANAGER_ROLE = '0';
    //1 => '企業管理者',
    const COMPANY_MANAGER_ROLE = '1';
    //2 => '店舗管理者',
    const STORE_MANAGER_ROLE = '2';
    //3 => '店舗スタッフ',
    const STAFF_ROLE = '3';
    
    const POS = [
        "1" => "POS 使用",
        "0" => "POS 不使用",
    ];
    
    const ROUND_DOW = [
        "0" => "無し",
        "1" => "有り",
    ];
    
    const ROUND_PROCESS = [
        "0" => "四捨五入",
        "1" => "切捨て",
    ];
     
    const GOLD_TICKET_SUPPLY_FLG = [
        "0" => "しない",
        "1" => "する",
    ];
    
    const GOLD_TICKET_SUPPLY_METHOD = [
        "1" => "自動発行",
        "2" => "任意発行",
    ];
    
    const EXPIRATION_DATE_FLG = [
        "0" => "無期限",
        "1" => "指定する",
    ];
    
    const POINT_POS = [
        "1" => "使用",
        "2" => "増加",
        "3" => "減少",
    ];
    const CHARGE_POS = [
        "1" => "使用",
        "2" => "チャージ",
    ];
}
