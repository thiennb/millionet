<?php

namespace common\components;

use common\models\MasterStore;
use common\models\MasterTypeSeat;
use common\models\MstProduct;

class CommonCheckExistModel {

    /**
     * Finds the Store model exists based on its primary key value.
     * If the model is not found, return false.
     * @param integer $id_store
     * @return true
     */
    static function checkStoreByExists($id_store) {
        if (($model = MasterStore::findOne($id_store)) !== null) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Check Products model exists based on its primary key value.
     * If the model is not exists, return false.
     * @param integer $id_product
     * @return true
     */
    static function CheckProductsExists($id_product) {
        if (!empty($id_product)) {
            $check_exist_model = new CommonCheckExistModel;
            //$check_value_product = substr($this->option_hidden, -1);
            if ($check_exist_model->endsWith($id_product, ",")) {
                $id_product = rtrim($id_product, ",");
                $count_array_products = count(explode(",", $id_product));
            } else {
                $count_array_products = count(explode(",", $id_product));
            }

            if ((new MstProduct())->getCountProductsExists($id_product) != $count_array_products) {
                return false;
            } else {
                return true;
            }
        } else {
            return true;
        }
    }

    /**
     * Check Type Seat model exists based on its primary key value.
     * If the model is not exists, return false.
     * @param integer $id_type_seat
     * @return true
     */
    static function CheckTypeSeatExists($id_type_seat) {
        if (($model = MasterTypeSeat::findOne($id_type_seat)) !== null) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Check Products have special character at the end.
     * If the Products do not have special character at the end, return false.
     * @param string $products
     * @param string $needle
     * @return true
     */
    public function endsWith($products, $needle) {
        // search forward starting from end minus needle length characters
        return $needle === "" || (($temp = strlen($products) - strlen($needle)) >= 0 && strpos($products, $needle, $temp) !== false);
    }

}
