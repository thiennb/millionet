<?php

namespace common\components;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Util
 *
 * @author thanhlbk
 */
use Yii;
use common\components\Constants;
use common\models\MstProduct;
use yii\web\UploadedFile;
use common\models\MasterCoupon;
use common\models\SiteSetting;
use yii\helpers\Url;

class Util {

    public static function getUrlImage($image) {
        if (!$image) {
            return Yii::$app->urlManagerApi->createUrl('/uploads') . '/' . 'no-image.jpg';
        }

        if (strpos($image, 'base64') !== false) {
            return $image;
        }

        $path_to_image_file = Yii::getAlias("@uploadPath/$image");

        if (file_exists($path_to_image_file)) {
            return Yii::$app->urlManagerApi->createUrl('/uploads') . '/' . $image;
        } else {
            return Yii::$app->urlManagerApi->createUrl('/uploads') . '/' . 'no-image.jpg';
        }
    }

    public static function uploadFile($file, $fileName) {
        if ($file) {
            $uploadPath = \Yii::getAlias('@uploadPath');
            $file->saveAs(Yii::getAlias($uploadPath . '/' . $fileName));
            return true;
        }
        return false;
    }

    /**
     * @author rikkei
     * save uri to season
     * d($var);
     */
    public static function setSeasonImage($model, $field) {
        $session = Yii::$app->session;

        $file = UploadedFile::getInstance($model, $field);

//      Util::dd($file) ;
//        $model->image1 = $src;


        if (!empty($file) != null) {
            // Read image path, convert to base64 encoding
            $imageData = base64_encode(file_get_contents($file->tempName));
            // Format the image SRC:  data:{mime};base64,{data};
            $src = 'data: ' . $file->type . ';base64,' . $imageData;
            //create new ss
            $session->set($field, $src);

            return $src;
        }

        if (empty($file) && $session->has($field) != null) {
            //return ss
            return $session->get($field);
        }

        //no image no ss
        return null;
    }

    /**
     * @author rikkei
     * save image from uri
     * d($var);
     */
    public static function saveImageUri($field, $filename = null) {
        $session = Yii::$app->session;
        if (empty($session->has($field))) {
            return false;
        }
        $src = $session->get($field);
        $imgData = str_replace(' ', '+', $src);
        $imgData = substr($imgData, strpos($imgData, ",") + 1);
        $imgData = base64_decode($imgData);
        // Path where the image is going to be saved

        $pos = strpos($src, ';');

        $type = explode('/', substr($src, 0, $pos))[1];
//      if( empty($filename ) ){
//      $filename = Yii::$app->security->generateRandomString();
//      }
        $filePath = \Yii::getAlias('@uploadPath') . DIRECTORY_SEPARATOR . $filename . '.' . $type;

        // Write $imgData into the image file
        $file = fopen($filePath, 'w');
        fwrite($file, $imgData);
        fclose($file);

        //no image no ss
        return $filename . '.' . $type;
    }

    public static function deleteFile($fileName) {
        $uploadPath = \Yii::getAlias('@uploadPath');
        @unlink(Yii::getAlias($uploadPath . '/' . $fileName));
    }

    /**
     * @author thiennb
     * Debug function
     * d($var);
     */
    static function d($var, $caller = null) {
        if (!isset($caller)) {
            $caller = array_shift(debug_backtrace(1));
        }
        echo '<code>File: ' . $caller['file'] . ' / Line: ' . $caller['line'] . '</code>';
        echo '<pre>';
        \yii\helpers\VarDumper::dump($var, 10, true);
        echo '</pre>';
    }

    /**
     * @author thiennb
     * Debug function with die() after
     * dd($var);
     */
    static function dd($var) {
        $data = debug_backtrace(1);
        $caller = array_shift($data);
        self::d($var, $caller);
        die();
    }

    /**
     * @author thiennb
     * gen jan_code
     * return 13 number digit
     */
    static function genDigitCheck($code = '210000000000') {

        $oddoutput = [];
        $evenoutput = [];
        $sumEvent = $sumOdd = $sum = 0;
        for ($counter = 1; $counter <= strlen($code); $counter++) {
            if ($counter % 2 == 0) {
                $evenoutput[] = $code[$counter - 1];
            } else {
                $oddoutput[] = $code[$counter - 1];
            }
        }
        // get sum event number
        foreach ($evenoutput as $event) {
            $sumEvent += $event;
        }
        $sumEvent = $sumEvent * 3;
        // get sum odd number
        foreach ($oddoutput as $odd) {
            $sumOdd += $odd;
        }

        // get last digit of sum
        $last = substr($sumEvent + $sumOdd, -1);
        // sub 10
        return ((int) $last == 0) ? '0' : (string) (10 - (int) $last);
    }

    /**
     * @author thiennb
     * gen jan_code
     * return 13 number digit
     */
    static function genJanCodeAuto($type = '20') {

        if ($type == Constants::JAN_TYPE_CUSTORMER) {
            $max_jan = \common\models\MasterCustomer::find(false)->select('customer_jan_code')->max('customer_jan_code');
            if (is_null($max_jan))
                return '200000000000' . self::genDigitCheck('200000000000');
            $jan = (real) substr($max_jan, 0, 12) + 1;

            return $jan . self::genDigitCheck((string) $jan);
        }
        if ($type == Constants::JAN_TYPE_PRODUCT) {
//            $sql = "Select nextval('product_jan_code')";
//            $command = Yii::app()->db->createCommand($sql);
            $next_code = SiteSetting::findOne(['type' => '01', 'key_1' => '01', 'key_2' => '02']);
            $next_jan_code = (string) ((real) 210000000000 + (real) $next_code->value_1);
            while (true) {
                $check_jan_code = $next_jan_code . self::genDigitCheck((string) $next_jan_code);
                $result = MstProduct::findOne(['jan_code' => $check_jan_code]);
                if (empty($result->jan_code)) {
                    $next_value = (real) (substr($check_jan_code, 0, 12) + 1) - (real) 210000000000;
                    SiteSetting::updateAll(['value_1' => $next_value], ['type' => '01', 'key_1' => '01', 'key_2' => '02']);
                    return $check_jan_code;
                }
                $next_jan_code = (real) $next_jan_code + 1;
            }

            return $jan . self::genDigitCheck((string) $jan);
        }
        if ($type == Constants::JAN_TYPE_TICKET) {
            $max_jan = \common\models\MasterTicket::find()->select('ticket_jan_code')->max('ticket_jan_code');
            if (is_null($max_jan))
                return '230000000000' . self::genDigitCheck('230000000000');
            $jan = (real) substr($max_jan, 0, 12) + 1;
            return $jan . self::genDigitCheck((string) $jan);
        }

        if ($type == Constants::JAN_TYPE_COUPON) {
            $max_jan = \common\models\MasterCoupon::find()->select('coupon_jan_code')->max('coupon_jan_code');
            if (is_null($max_jan))
                return '220000000000' . self::genDigitCheck('220000000000');
            $jan = (real) substr($max_jan, 0, 12) + 1;
            return $jan . self::genDigitCheck((string) $jan);
        }
    }

    /**
     * check jan_code
     * return 13 number digit
     */
    static function checkJanCode($type = '21', $jan_code = '210000000000000') {
        if ($type == Constants::JAN_TYPE_PRODUCT) {
            $check_type = substr($jan_code, 0, 2);
            if ($check_type != Constants::JAN_TYPE_PRODUCT) {
                return FALSE;
            }
            $code = substr($jan_code, 0, 12);
            $digit = substr($jan_code, 12, 1);
            $digit_check = self::genDigitCheck($code);
            return ($digit_check == $digit) ? TRUE : FALSE;
        }
    }
    
    /**
     * Check the product jan code unsatisfactorily conditions within 20 to 30 except 21
     * If don't unsatisfactorily return false
     * @param string $type
     * @param string $jan_code
     * @return true
     */
    static function checkJanCodeProduct($type = '21', $jan_code = '210000000000000') {
        if ($type == Constants::JAN_TYPE_PRODUCT) {
            $check_type = substr($jan_code, 0, 2);
            $arr_check_jan_product = Constants::CHECK_JAN_PRODUCT;
            foreach ($arr_check_jan_product as $value){
                if($value == intval($check_type)){
                   return FALSE; 
                }
            }
            $code = substr($jan_code, 0, 12);
            $digit = substr($jan_code, 12, 1);
            $digit_check = self::genDigitCheck($code);
            return ($digit_check == $digit) ? TRUE : FALSE;
        }
    }

    function checkJanCodeTicket($type = '23', $jan_code = '2300000000009') {
        if ($type == Constants::JAN_TYPE_TICKET) {
            $check_type = substr($jan_code, 0, 2);
            if ($check_type != Constants::JAN_TYPE_TICKET) {
                return FALSE;
            }
            $code = substr($jan_code, 0, 12);
            $digit = substr($jan_code, 12, 1);
            $digit_check = self::genDigitCheck($code);
            return ($digit_check == $digit) ? TRUE : FALSE;
        }
    }

    static function checkJanCodeCustomer($type = '20', $jan_code = '200000000000005') {
        if ($type == Constants::JAN_TYPE_CUSTORMER) {
            $check_type = substr($jan_code, 0, 2);
            if ($check_type != Constants::JAN_TYPE_CUSTORMER) {
                return FALSE;
            }
            $code = substr($jan_code, 0, 12);
            $digit = substr($jan_code, 12, 1);
            $digit_check = self::genDigitCheck($code);
            return ($digit_check == $digit) ? TRUE : FALSE;
        }
    }

    /**
     * gen array time with step
     * return array
     */
    public static function genArrayWorkHours($startHour, $endHour, $stepMinute = 30) {
        $startTime = new \DateTime('2010-01-01 ' . $startHour);
        $endTime = new \DateTime('2010-01-01 ' . $endHour);
        $timeStep = $stepMinute;
        $timeArray = array();

        while ($startTime <= $endTime) {
            $timeArray[$startTime->format('H:i')] = $startTime->format('H:i');
            $startTime->add(new \DateInterval('PT' . $timeStep . 'M'));
        }
        return $timeArray;
    }

    public static function dateFormatString($date) {

        $string = date('Y/m/d', strtotime($date) ). "(" . Constants::WORK_DAY[date('w', strtotime($date))] . ")";
        return $string;
    }

    /**
     * get index of array value
     * return index number
     */
    public static function getIndexArray($array, $value) {

        $count = 1;
        $array = array_keys($array);
        // TODO
        $index = 1;
        foreach ($array as $item) {
            if ($value == $item) {
                $index = $count;
                break;
            } else {
                $count++;
            }
        }
        return $index;
    }

    public static function truncateString($string, $length, $pad = '...') {
        return strlen($string) > $length ? substr($string, 0, $length - strlen($pad)) . $pad : $string;
    }

    public static function getCompleteRankCondition($rankId = null, $totalPoint = null, $numberVisit = null, $totalMoney = null, $rankSetting = null) {
        if ($rankSetting != null) {
            $condition = '3'; $rankNext = '00';
            switch($rankSetting['rank_condition']){
                case '00' : $condition = '3'; break;
                case '01' : $condition = '2'; break;
                case '02' : $condition = '1'; break;
                default: break;
            }
            
            $PointRequire = 0; $numVisitRequire = 0; $totalMoneyRequire = 0;
            if (empty($rankId)) {
                $rankId = '00';
            }
            
            $rankNext = (int)$rankId < 4 ? ($rankSetting['black_status'] == '1' ? '04': $rankNext) : $rankNext;
            $rankNext = (int)$rankId < 3 ? ($rankSetting['gold_status'] == '1' ? '03': $rankNext) : $rankNext;
            $rankNext = (int)$rankId < 2 ? ($rankSetting['silver_status'] == '1' ? '02': $rankNext) : $rankNext;
            $rankNext = (int)$rankId < 1 ? ($rankSetting['bronze_status'] == '1' ? '01': $rankNext) : $rankNext;
            switch ($rankNext) {
                case '01':
                    $PointRequire = $rankSetting["bronze_point_total"];
                    $numVisitRequire = $rankSetting["bronze_time_visit"];
                    $totalMoneyRequire = $rankSetting["bronze_use_money"];
                    break;
                case '02':
                    $PointRequire = $rankSetting["sliver_point_total"];
                    $numVisitRequire = $rankSetting["sliver_time_visit"];
                    $totalMoneyRequire = $rankSetting["sliver_use_money"];
                    break;
                case '03':
                    $PointRequire = $rankSetting["gold_point_total"];
                    $numVisitRequire = $rankSetting["gold_time_visit"];
                    $totalMoneyRequire = $rankSetting["gold_use_money"];
                    break;
                case '04':
                    $PointRequire = $rankSetting["black_point_total"];
                    $numVisitRequire = $rankSetting["black_time_visit"];
                    $totalMoneyRequire = $rankSetting["black_use_money"];
                    break;
                    defaul: break;
            }

            $result = 0;
            $conditionRequire = ['pointCondition'=>$PointRequire, 'numVisitCondition'=> $numVisitRequire,'useMoneyCondition'=>$totalMoneyRequire];
            if ($rankNext != '00') {
                if (((int) $totalPoint >= (int) $PointRequire) && ((int)$PointRequire > 0))
                    $result ++;
                if (((int) $numberVisit >= (int) $numVisitRequire) && ((int)$numVisitRequire > 0))
                    $result ++;
                if (((float) $totalMoney >= (float) $totalMoneyRequire) && ((int)$totalMoneyRequire > 0))
                    $result ++;
                
                $text = '<span>'.Yii::t("frontend","Rank Up Condition").'</span> <span>'.$result.'/'.$condition.'</span> <span>'.Yii::t("frontend","Completed").'</span>';
                $text_only_condtion = $result.'/'.$condition;
                return array('text'=> $text, 'text_only_condtion'=> $text_only_condtion, 'condition'=> $result, 'next'=>$rankNext, 'condition_require' => $conditionRequire);
            }            
        }

        return array('text'=> '', 'condition'=> 0, 'next'=>'00', 
            'condition_require' => ['pointCondition'=>0, 'numVisitCondition'=> 0,'useMoneyCondition'=>0]);
    }

    /*
    public static function  setCookiesCompanyCode($company_code) {
        $cookies = Yii::$app->response->cookies;
        $cookies->add(new \yii\web\Cookie([
                    'name'  => 'code_company_cookie',
                    'value' => $company_code,
                    'expire'=> time() + (365 * 24 * 60 * 60)
                ]));
    }
    
    public static function  getCookiesCompanyCode() {
        $company_code =null;
        if (\Yii::$app->controllerNamespace == "backend\controllers") {
         
         $cookies = Yii::$app->request->cookies;
         if($cookies->has('code_company_cookie')){
         $company_code = $cookies->get('code_company_cookie')->value;
         }  else {
            \Yii::$app->user->logout();
            \Yii::$app->getResponse()->redirect(Url::to('/'));
         }
        } 
        return $company_code;
    }
    */
    
    public static function  setCookiesCompanyId($company_id) {
        if (\Yii::$app->controllerNamespace == "backend\controllers") {
            $cookies = Yii::$app->response->cookies;
            $cookies->add(new \yii\web\Cookie([
                        'name'  => 'id_company_cookie_backend',
                        'value' => $company_id,
                        'expire'=> time() + (365 * 24 * 60 * 60)
                    ]));
        }else if (\Yii::$app->controllerNamespace == "frontend\controllers") {
            $cookies = Yii::$app->response->cookies;
            $cookies->add(new \yii\web\Cookie([
                        'name'  => 'id_company_cookie_frontend',
                        'value' => $company_id,
                        'expire'=> time() + (365 * 24 * 60 * 60)
                    ]));
        }
    }
    
    public static function  getCookiesCompanyId() {
        $company_id =null;
        if (\Yii::$app->controllerNamespace == "backend\controllers") {
         
            $cookies = Yii::$app->request->cookies;
            if($cookies->has('id_company_cookie_backend')){
                $company_id = $cookies->get('id_company_cookie_backend')->value;
            }  else {
               \Yii::$app->user->logout();
               \Yii::$app->getResponse()->redirect(Url::to('/'));
            }
        }else if(\Yii::$app->controllerNamespace == "frontend\controllers"){
            $cookies = Yii::$app->request->cookies;
            if($cookies->has('id_company_cookie_frontend')){
                $company_id = $cookies->get('id_company_cookie_frontend')->value;
            }  else {
               \Yii::$app->user->logout();
               \Yii::$app->getResponse()->redirect(Url::to('/'));
            }
        } 
        return $company_id;
    }
    
    public static function formatPhoneNumber ($phone_number) {
        return strlen($phone_number) > 7 ? substr($phone_number, 0, 3) . '-' . substr($phone_number, 3, 3) . '-' . substr($phone_number, 6) : $phone_number;
    }
    
    public static function getTaxProduct($taxRateId = ''){
        $taxRate = 0;
        if($taxRateId != ''){
            $taxDetail = \common\models\TaxDetail::find()
                ->where(['tax_id' => $taxRateId])
                ->andWhere(['<=','start_date', date('Y-m-d')])
                ->andWhere(['>=','end_date', date('Y-m-d')])
                ->andWhere(['del_flg'=> '0'])
                ->one();

            if(!empty($taxDetail)){
                $taxRate = $taxDetail['rate'];
            }                
        }
        
        return $taxRate;
    }
}
