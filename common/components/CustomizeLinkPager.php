<?php

namespace common\components;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of LinkPagerMillionet
 *
 * @author Administrator
 */
use Yii;
use yii\base\InvalidConfigException;
use yii\helpers\Html;
use yii\base\Widget;
use yii\data\Pagination;

class CustomizeLinkPager extends \yii\widgets\LinkPager {

    //put your code here
    public $prevPageLabel = "<span class='prev-page disabled'><span class='prev_bt'>◀</span> 前へ</span>";
    public $nextPageLabel = "<span class='next-page disabled'>次へ <span class='prev_bt'>▶</span></span>";
    public $pageCssClass = 'hiden';
    public $activePageCssClass = 'active show2';

    public function init() {
        parent::init();
    }

    protected function renderPageButtons() {
        $pageCount = $this->pagination->getPageCount();
        if ($pageCount < 2 && $this->hideOnSinglePage) {
            //return ''; // HungLQ show on 1 page
        }

        $buttons = [];
        $currentPage = $this->pagination->getPage();

        // first page
        $firstPageLabel = $this->firstPageLabel === true ? '1' : $this->firstPageLabel;
        if ($firstPageLabel !== false) {
            $buttons[] = $this->renderPageButton($firstPageLabel, 0, $this->firstPageCssClass, $currentPage <= 0, false);
        }
        // prev page
        if ($currentPage > 0) {
            if ($this->prevPageLabel !== false) {
                if (($page = $currentPage - 1) < 0) {
                    $page = 0;
                }
            $buttons[] = $this->renderPageButton($this->prevPageLabel, $page, $this->prevPageCssClass, $currentPage <= 0, false);
            }
        }
        

        // internal pages
        $pageCount = $this->pagination->getPageCount();
        $beginPage = 0;
        $endPage = $pageCount - 1;
        //list($beginPage, $endPage) = $this->getPageRange();
//        for ($i = $beginPage; $i <= $endPage; ++$i) {
//            $buttons[] = $this->renderPageButton($i + 1, $i, null, false, $i == $currentPage);
//        }
        for ($i = $beginPage; $i <= $endPage; ++$i) {
            $buttons[] = $this->renderPageButton($i + 1 . ' / ' . (string) ($endPage + 1).'ページ', $i, null, false, $i == $currentPage);
        }


        // next page
        if (($pageCount-1) != $currentPage ) {
            if ($this->nextPageLabel !== false) {
                if (($page = $currentPage + 1) >= $pageCount - 1) {
                    $page = $pageCount - 1;
                }
                $buttons[] = $this->renderPageButton($this->nextPageLabel, $page, $this->nextPageCssClass, $currentPage >= $pageCount - 1, false);
            }
        }
        // last page
        $lastPageLabel = $this->lastPageLabel === true ? $pageCount : $this->lastPageLabel;
        if ($lastPageLabel !== false) {
            $buttons[] = $this->renderPageButton($lastPageLabel, $pageCount - 1, $this->lastPageCssClass, $currentPage >= $pageCount - 1, false);
        }

        return Html::tag('ul', implode("\n", $buttons), $this->options);
    }

}
