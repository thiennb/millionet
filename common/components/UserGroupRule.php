<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace common\components;

use Yii;
use yii\rbac\Rule;
use common\models\User;

/**
 * Checks if user group matches
 */
class UserGroupRule extends Rule
{
    public $name = 'userGroup';

    public function execute($user, $item, $params)
    {
        if (!Yii::$app->user->isGuest) {
            $group = Yii::$app->user->identity->group;
            if ($item->name === 'admin') {
                    return $group == User::GROUP_ADMIN;
            } elseif ($item->name === 'staff') {
                return $group == User::GROUP_ADMIN || $group == User::GROUP_STAFF;
            }elseif ($item->name === 'user') {
                return $group == User::GROUP_ADMIN || $group == User::GROUP_STAFF || $group == User::GROUP_USER;
            }
        }
        return false;
    }
}
