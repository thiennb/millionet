<?php

namespace common\components;

use Yii;
use yii\db\BaseActiveRecord;
use yii\behaviors\AttributeBehavior;


class RegisterFromBehavior extends AttributeBehavior
{
    /**
     * @var string the attribute that will receive current user ID value
     * Set this property to false if you do not want to record the creator ID.
     */
    public $createdFromAttribute = 'register_kbn';
    public $createdFromAttributeUpdate = 'update_kbn';
    /**
     * @var string the attribute that will receive current user ID value
     * Set this property to false if you do not want to record the updater ID.
     */

    /**
     * @inheritdoc
     *
     * In case, when the property is `null`, the value of `Yii::$app->user->id` will be used as the value.
     */
    public $value;
    const  FROM_BACKEND = '1';
    const  FROM_FRONTEND = '2';
    const  FROM_API = '3';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        if (empty($this->attributes)) {
            $this->attributes = [
                BaseActiveRecord::EVENT_BEFORE_INSERT => $this->createdFromAttribute,
                BaseActiveRecord::EVENT_BEFORE_UPDATE => $this->createdFromAttributeUpdate,
            ];
        }
    }

    /**
     * @inheritdoc
     *
     * In case, when the [[value]] property is `null`, the value of `Yii::$app->user->id` will be used as the value.
     */
    protected function getValue($event)
    {
        if ($this->value === null) {
          
          
            
            if (\Yii::$app->controllerNamespace == "backend\controllers") {
            
             return self::FROM_BACKEND;
            } 
            if (\Yii::$app->controllerNamespace == "frontend\controllers") {
            
             return self::FROM_FRONTEND;
            } 
            if (\Yii::$app->controllerNamespace == "api\controllers") {
            
             return self::FROM_API;
            } 
            
            return null;
        }

        return parent::getValue($event);
    }
}