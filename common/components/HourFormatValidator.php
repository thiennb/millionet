<?php

    /* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace common\components;
use Yii;
use yii\validators\Validator;

class HourFormatValidator extends Validator
{
    public function validateAttribute($model, $attribute)
    {
        if (!preg_match('/(2[0-4]|[01][1-9]|00|10):([0-5][0-9])/', $model->$attribute)) {
             $this->addError($model,$attribute, Yii::t('backend', 'Time format is incorrect.'));
        }
        
    }
}