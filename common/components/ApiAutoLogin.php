<?php

namespace common\components;
 
use Yii;
use yii\base\Component;
use common\models\User;
use api\models\ApiLoginForm;
use api\models\ApiSupport;
 
class ApiAutoLogin extends \yii\base\Behavior
{
  //put your code here
  public function events() {
    return [
    \yii\web\Application::EVENT_BEFORE_ACTION => 'checkAutoLogin'
    ];
    
  }
 
  public function checkAutoLogin(){
    
      $session = \Yii::$app->session;
      $token = \Yii::$app->request->headers->get('Authorization');
            
      if($token == null){
          $token = $session->get('token_param');
      }
      if($token != null && \Yii::$app->user->isGuest){
      $login = new ApiLoginForm();

      $tokenObject = ApiSupport::isValidApiToken($token);
      $user = User::find()->where(['id' => $tokenObject->id])->one();
      $login->login($user, 3600 * 24 * 30);
      }
      return true;
  }
}

