<?php
return [
   'bootstrap' => array('gon','LanguageSwitcher'),
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
  'language' => 'ja',
  "name" =>'理美容 POS システム',
  'timeZone' => 'Asia/Tokyo',
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
       'gon' => array(
            'class' => 'ijackua\gon\GonComponent',
            'jsVariableName' => 'gon',
//            'globalData' => ['g1' => 1, 'g2' => '2'],
            'showEmptyVar' => true,
        ),
      'LanguageSwitcher' => [
            'class' => 'common\components\LanguageSwitcher',
        ],
      'i18n' => [
            'translations' => [
                'yii' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    
                    'basePath' => '@approot/messages'
                ],
                '*' => [
                        'class' => 'yii\i18n\PhpMessageSource',
                        'basePath' => '@approot/messages',
                    ]
                ]
        ],
        'formatter' => [
              'dateFormat' => 'yyyy/MM/dd',
              'decimalSeparator' => ',',
              'nullDisplay' => '',
              
         ],
       
    ],
    
];
