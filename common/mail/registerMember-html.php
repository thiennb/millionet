<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user common\models\User */
$resetLink = Yii::$app->urlManager->createAbsoluteUrl(['site/']);
?>
<div class="password-reset">
    <p>Hello <?= Html::encode($model->last_name.$model->first_name) ?>,</p>

    <p>You complete register account in Millionet System</p>
    <p>This is your information:</p>
    <p>Your phone number: <?= $model->phone_1.$model->phone_2.$model->phone_3 ?></p>
    <p>Your password:<?= $model->password ?></p>
    <p><?= Html::a(Html::encode($resetLink), $resetLink) ?></p>
</div>
