<?php

use yii\helpers\Html;
/* @var $this yii\web\View */
/* @var $user common\models\User */
$nextStep = Yii::$app->urlManager->createAbsoluteUrl(['member/step2']);
?>
Dear,

    You had registered account in Millionet System by this email.
    This is your key confirm: <?= $model->key_confirm_again ?>
    Please use this key and continually register member.
    <p><?= Html::a(Html::encode($nextStep), $nextStep) ?></p>