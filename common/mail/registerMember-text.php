<?php

use yii\helpers\Html;
/* @var $this yii\web\View */
/* @var $user common\models\User */
$resetLink = Yii::$app->urlManager->createAbsoluteUrl(['site/']);
?>
Hello <?= Html::encode($model->last_name.$model->first_name) ?>,

    You complete register account in Millionet System
    This is your information:
    Your phone number: <?= $model->phone_1.$model->phone_2.$model->phone_3 ?>
    Your password:<?= $model->password ?>
    <p><?= Html::a(Html::encode($resetLink), $resetLink) ?></p>