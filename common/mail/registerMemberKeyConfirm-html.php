<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user common\models\User */
$nextStep = Yii::$app->urlManager->createAbsoluteUrl(['member/step2']);
?>
<div class="register-member-keyconfirm">
    <p>Dear,</p>

    <p>You had registered account in Millionet System by this email.</p>
    <p>This is your key confirm: <?= $model->key_confirm_again ?></p>
    <p>Please use this key and continually register member.</p>
    <p><?= Html::a(Html::encode($nextStep), $nextStep) ?></p>
</div>
