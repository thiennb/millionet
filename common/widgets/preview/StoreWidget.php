<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace common\widgets\preview;
use yii\bootstrap\Widget;
/**
 * Description of Preview
 *
 * @author thanhlbk
 */
class StoreWidget extends Widget{
    public $data;
    public $model;
    public $form;


    public function init() {
        parent::init();
        ob_start();
    }
    
    public function run() {
        $content = ob_get_clean();
        return $this->render('_store', [
                'output' => $this->data, 
                'model' => $this->model,
                'form' => $this->form,
            ]);
    }
}
