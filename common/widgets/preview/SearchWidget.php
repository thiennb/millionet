<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace common\widgets\preview;
use yii\bootstrap\Widget;
/**
 * Description of Preview
 *
 * @author thanhlbk
 */
class SearchWidget extends Widget{
    public $data;
    public $formId;
    public $formAction;
    public $btnBack;
    public $btnSubmit;
    public $searchName;
    public $model;


    public function init() {
        parent::init();
        ob_start();
    }
    
    public function run() {
        $content = ob_get_clean();
        $formId  = isset($this->formId) ? $this->formId : 'search-w0';
        return $this->render('_search_widget', [
                'data' => $this->data, 
                'formId' => $formId,
                'formAction' => $this->formAction,
                'btnBack' => $this->btnBack,
                'btnSubmit' => $this->btnSubmit,
                'searchName' => $this->searchName,
                'model' => $this->model,
            ]);
    }
}
