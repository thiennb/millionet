<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$lowerModelName = strtolower($modelName);
$idPreview = "preview2-" . $lowerModelName;
$fields = array();
?>
<div class="form-horizontal">
    <?php
    foreach ($data as $key => $item) {
        ?>
        <div class="form-group">
            <?php if (!empty($item['label'])) { ?>
                <label class="col-sm-4"><?= $item['label']; ?></label>
                <div class="col-sm-8">
                    <?php
                    $field = array();
                    $idField = $lowerModelName . '-' . $key;
                    if (isset($item['model'])) {
                        //orther model
                        $idField = strtolower($item['model']) . '-' . $key;
                    }
                    $idFieldPreview = "preview2-" . $idField;
                    $type = "";
                    if ($item['type'] == 'image' || $item['type'] == 'input') {
                        ?>
                        <div class="<?= $idFieldPreview ?>"></div>
                        <?php
                    } else {
                        ?>
                        <span id="<?= $idFieldPreview ?>"></span>
                        <?php
                    }

                    if ($item['type'] == 'input_coupon') {
                        ?>
                        <div class="<?= $idFieldPreview ?>"></div>
                        <?php
                    } else {
                        ?>
                        <span id="<?= $idFieldPreview ?>"></span>
                        <?php
                    }

                    $field['id'] = $idField;
                    $field['idPreview'] = $idFieldPreview;
                    $field['type'] = $item['type'];
                    if (isset($item['groupWith'])) {
                        $field['groupWith'] = $lowerModelName . '-' . $item['groupWith'];
                    }
                    if (isset($item['groupWithSeat'])) {
                        $field['groupWithSeat'] = $lowerModelName . '-' . $item['groupWithSeat'];
                    }

                    if (isset($item['show_coupon_check'])) {

                        $field['show_coupon_check'] = true;
                    }

                    $fields[] = $field;
                    ?>

                </div>
            <?php } else { ?>
                <div class="col-sm-12">
                    <?php if ($item['type'] == 'table') { ?>
                        <?php
                        $field = array();
                        $idField = $lowerModelName . '-' . $key;
                        $idFieldPreview = "preview2-" . $idField;
                        $type = "";
                        $field['id'] = $idField;
                        $field['header'] = $item['header'];
                        $field['table'] = $item['table'];
                        $field['idPreview'] = $idFieldPreview;
                        $field['type'] = $item['type'];
                        if (isset($item['groupWith'])) {
                            $field['groupWith'] = $lowerModelName . '-' . $item['groupWith'];
                        }
                        if (isset($item['groupWithSeat'])) {
                            $field['groupWithSeat'] = $lowerModelName . '-' . $item['groupWithSeat'];
                        }
                      
                        $fields[] = $field;
                        ?>
                        <table id="<?= $idFieldPreview ?>" border='1' class="col-sm-12"></table>
                    <?php } ?>
                    <?php if ($item['type'] == 'table-normal') { ?>
                        <?php
                        $field = array();
                        $idField = $lowerModelName . '-' . $key;
                        $idFieldPreview = "preview2-" . $idField;
                        $type = "";
                        $field['id'] = $idField;
                        $field['column'] = $item['column'];
                        $field['table'] = $item['table'];
                        $field['idPreview'] = $idFieldPreview;
                        $field['type'] = $item['type'];
                        if (isset($item['groupWith'])) {
                            $field['groupWith'] = $lowerModelName . '-' . $item['groupWith'];
                        }
                        if (isset($item['groupWithSeat'])) {
                            $field['groupWithSeat'] = $lowerModelName . '-' . $item['groupWithSeat'];
                        }

                        $fields[] = $field;
                        ?>
                        <table id="<?= $idFieldPreview ?>" border='1' class="col-sm-12"></table>
                    <?php } ?>
                </div>
            <?php } ?>
        </div>
        <?php
    }
    ?>

</div>

<script>

    function previewDialog2(notDelay){
        //fix for IE
        if(!notDelay){
            notDelay = true;
        }
        if (notDelay == false) {
            //if notDelay == false delay ultil have image
            var checkExist = setInterval(function () {
                //check until exit 
                if ($('.file-initial-thumbs').find('img').length > 0) {
                    previewDialog2();
                    clearInterval(checkExist);
                }
            }, 100);
        }
        //hot fix for coupon

<?php
foreach ($fields as $item) {
    if ($item['type'] == 'input_coupon') {
        isset($item['groupWith']) ? $group = " ' ' + $('#{$item['groupWith']}').val()" : $group = " ''";
        isset($item['groupWithSeat']) ? $groupSeat = " ' ' + $('#{$item['groupWithSeat']}').val()" : $groupSeat = " ''";
       
        ?>
                var $sText = $('#<?= $item['id'] ?>').val() + <?= $group ?>;
                //$('#<?= $item['idPreview'] ?>').text($('#<?= $item['id'] ?>').val() + <?= $group ?>);

                $('.<?= $item['idPreview'] ?>').html('').append('<span>' + $sText + '</span>');

                if (<?= $groupSeat ?> !== "") {
                    $('#<?= $item['idPreview'] ?>').text($('#<?= $item['id'] ?>').val() + "     ～     " + <?= $groupSeat ?>);
                }

        <?php
    } else if ($item['type'] == 'input') {
        isset($item['groupWith']) ? $group = " ' ' + $('#{$item['groupWith']}').val()" : $group = " ''";
        isset($item['groupWithSeat']) ? $groupSeat = " ' ' + $('#{$item['groupWithSeat']}').val()" : $groupSeat = " ''";
        ?>                
                $('#<?= $item['idPreview'] ?>').text($('#<?= $item['id'] ?>').val() + <?= $group ?>);
                
                if (<?= $groupSeat ?> !== "") {
                    $('#<?= $item['idPreview'] ?>').text($('#<?= $item['id'] ?>').val() + "     ～     " + <?= $groupSeat ?>);
                }

        <?php
    } else if ($item['type'] == 'select') {
        ?>
                if ($('#<?= $item['id'] ?>  option:selected').val()) {
                    $('#<?= $item['idPreview'] ?>').text($('#<?= $item['id'] ?>  option:selected').text());
                } else {
                    $('#<?= $item['idPreview'] ?>').text("");
                }
        <?php
    } else if ($item['type'] == 'image') {
        ?>
                var $src_img = $('.<?= 'field-' . $item['id'] ?>').find('img').clone();
                $src_img.attr('width', '200px').attr('height', '200px');

                //                        if($src_img == '' || $src_img == null){
                //                            $src_img = '/api/uploads/no-image.jpg';
                //                        }
                //                        $('.<?= $item['idPreview'] ?>').attr('src', $src_img);
                if ($src_img == '' || $src_img == null || $src_img.attr('src') == '/api/uploads/no-image.jpg') {
                    $('.<?= $item['idPreview'] ?>').html('');
                } else {
                    $('.<?= $item['idPreview'] ?>').html('').append($src_img);
                }

        <?php
    } else if ($item['type'] == 'radio') {
        ?>
                $('#<?= $item['idPreview'] ?>').text($('#<?= $item['id'] ?> input:checked').parent().text());


        <?php
    } else if ($item['type'] == 'radio_customer') {
        ?>
                var $radio_customer_render = $('#radio_customer_value_' + $('#masternotice-type_send:checked').val()).text();
                if($('#masternotice-type_send:checked').val() == '1'){
                    $radio_customer_render = $('#masternotice-send_date').val() + "  " + $('#masternotice-send_time').val();
                }
                $('#<?= $item['idPreview'] ?>').text($radio_customer_render);
        <?php
    } else if ($item['type'] == 'checkbox') {
        if (isset($item['show_coupon_check']) && $item['show_coupon_check'] == true) {
            ?>
                    //================================
                    // Check Show Coupon Datpdt start
                    //================================
                    var value_show_coupon = $('input[name="MasterCoupon[show_coupon][]"]:checked').map(function () {

                        return $(this).val();

                    return $(this).val();

                });
                if (value_show_coupon.length >= 2) {
                    //alert(value_show_coupon.length);
                        var index, len, value = null;

                        for (index = 0, len = value_show_coupon.length; index < len; ++index) {
                            if (value_show_coupon[index] == '00') {
                                value = "予約サイト・会員アプリ";
                            } else {
                                value += "   ~   レシート出力";
                            }
                        }
                        $('#<?= $item['idPreview'] ?>').text(value);
                    } else {
                        if (value_show_coupon[0] == '00') {
                            $('#<?= $item['idPreview'] ?>').text("予約サイト・会員アプリ");
                        } else {
                            $('#<?= $item['idPreview'] ?>').text("レシート出力");
                        }
                    }


        <?php } else { ?>
                    if ($('#<?= $item['id'] ?>').prop('checked')) {
                        $('#<?= $item['idPreview'] ?>').text("チェック");
                    } else {
                        $('#<?= $item['idPreview'] ?>').text("なし");
                    }
                    //================================
                    // Check Show Coupon Datpdt end
                    //================================
        <?php } ?>
        <?php
    } else if ($item['type'] == 'table') {
        ?>
                var $render_tabel = "";
                $render_tabel += "<tr>";
                $('.<?= $item['header'] ?>').each(function () {
                    $render_tabel += "<th class='div-border text-center'>" + $(this).text() + "</th>";
                })
                $render_tabel += "</tr>";
                var $body_table = $('table.<?= $item['table'] ?>').find('tr:not(:eq(0))');
                $body_table.each(function () {
                    var $reduced_tax_flg;
                    if ($(this).find('td').first().find('input[type="checkbox"]').prop('checked') == true) {
                        $reduced_tax_flg = $('.<?= $item['header'] ?>').first().text()
                    } else {
                        $reduced_tax_flg = ''
                    }
                    var $rate = $(this).find('td:nth-child(2)').find('div').find('input').val();
                    var $start_date = $(this).find('td:nth-child(4)').find('div').find('input').val();
                    var $end_date = $(this).find('td:nth-child(6)').find('div').find('input').val();
                    if ($reduced_tax_flg != '' || $rate != '' || $start_date != '' || $end_date != '') {
                        $render_tabel += "<tr>";
                        $render_tabel += "<td class='div-border text-center col-sm-3'>" + $reduced_tax_flg + "</td>";
                        $render_tabel += "<td class='div-border text-center col-sm-3'>" + $rate
                                + $(this).find('td:nth-child(3)').text() + "</td>";
                        $render_tabel += "<td class='div-border text-center col-sm-6'>" + $start_date + '&nbsp;&nbsp;~&nbsp;&nbsp; ' + $end_date + "</td>";
                        $render_tabel += "</tr>";
                    }
                })
                $('#<?= $item['idPreview'] ?>').html($render_tabel);
        <?php
    } else if ($item['type'] == 'table-normal') {
        ?>
                var $i = 0;
                var $col = <?= $item['column'] ?>;
                $col = ($col) ? $col.split(",") : $col;
                var $col_num = $col.length;
                var $header = $('#<?= $item['table'] ?>').find('tr:eq(0)').find('th');
                var $header_arr = [];
                $header.each(function () {
                    $header_arr.push($(this).text());
                })
                var $render_tabel = "<table>";
                $render_tabel += "<tr>";
                //render header
                for ($i = 0; $i < $col_num; $i++) {
                    $render_tabel += "<th>" + $header_arr[$i] + "</th>";
                }
                $render_tabel += "</tr>";
                //render body
                var $body_tr = $('#<?= $item['table'] ?>').find('tr:not(:eq(0))');
                $body_tr.each(function () {
                    $render_tabel += "<tr>";
                    var $body_td = $(this).find('td');
                    var $body_td_arr = [];
                    var $no_td = null;
                    $body_td.each(function () {
                        $no_td = ($(this).find('div')) ? $(this).find('div').text() : null;
                        $body_td_arr.push($(this).text());
                    })
                    if ($no_td) {
                        $render_tabel += "<td colspan=" + $col_num + ">" + ($no_td) + "</td>";
                    } else {
                        for ($i = 0; $i < $col_num; $i++) {
                            var $td_render = ($body_td_arr[$i]).replace(/&/g, "&amp;").replace(/>/g, "&gt;").replace(/</g, "&lt;").replace(/"/g, "&quot;");
                            $render_tabel += ($body_td_arr[$i]) ? "<td>" + (($td_render == '') ? " " : $td_render) + "</td>" : '<td></td>';
                        }
                    }
                    $render_tabel += "</tr>";
                })
                $render_tabel += "</table>";
                $('#<?= $item['idPreview'] ?>').html($render_tabel);
        <?php
    }
}
?>


    }

    $(function () {
        $(document).on('click', '#<?= $idBtnConfirm ?>', function () {
            if ($(".kv-error-close").length > 0) {
                return false;
            }
            previewDialog2();
            return  true;
        });
        //set defaulse validate prevent submit if not click btnSubmit
        var validated = 0 ;
        $(document).on('click', '#<?= $btnSubmit ?>', function () {
            validated = 1;
        });
        //can submit if validated != 0 
        $('#<?= $formId ?>').on('beforeSubmit', function (event) {
            if(validated == 0 ) return false;
        });
        //show modal if all validate
        $('#<?= $formId ?>').on('afterValidate', function (event, attribute, messages) {
           
            if ($(this).find('.has-error').length == 0 ) {
               
                $('#<?= $modalId ?>').modal("show");
                
            }
        });
    });
</script>
