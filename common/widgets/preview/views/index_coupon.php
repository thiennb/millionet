<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$lowerModelName = strtolower($modelName);
$idPreview = "preview2-" . $lowerModelName;
$fields = array();
?>
<div class="col-md-12">
    <div class="col-md-8 preview_coupon preview_coupon_pv">
        <div class='mastercoupon-title_top'>

        </div>
        <div class='mastercoupon-introduction_1_top'>

        </div>
        <div class='mastercoupon-introduction_2_top'>

        </div>
        <div class='mastercoupon-introduction_3_top'>

        </div>
        <div class='mastercoupon-image'>

        </div>
        <div class='mastercoupon-title_bottom' style="">

        </div>
        <div class='mastercoupon-introduction_1_bottom'>

        </div>
        <div class='mastercoupon-introduction_2_bottom'>

        </div>
        <div class='mastercoupon-introduction_3_bottom'>

        </div>
        <div class='expire_date'>
            <div class="col-md-3 expiration_date_tt" style="margin-left: 1.5em;">
            </div>
            <div class="col-md-4 expire_date_vl">

            </div>
            <div class="col-md-4 expire_date_tl">

            </div>
        </div>
        <div class='col-md-11 tl' style="text-align: left;margin-left: 1.5em;font-weight: bold;">
        </div>
        <div class='col-md-11' style="margin-left: 2em;">
            <div class="related-post">
            </div>
        </div>
        <div class='col-md-12 jan_code_preview' style="height: 6em;">

        </div>
    </div>
</div>

<script>

    function previewDialog2(notDelay){
        //fix for IE
        if(!notDelay){
            notDelay = true;
        }
        if (notDelay == false) {
            //if notDelay == false delay ultil have image
            var checkExist = setInterval(function () {
                //check until exit 
                if ($('.file-initial-thumbs').find('img').length > 0) {
                    previewDialog2();
                    clearInterval(checkExist);
                }
            }, 100);
        }
    }
    $(document).on('click', '#btn-confirm-preview', function () {
        previewCoupon();
    });

    function previewCoupon() {
        // ============================
        // Get Font Size Start
        // ============================
        var title_font_size, introduction_font_size_1, introduction_font_size_2, introduction_font_size_3;
        title_font_size = $('#mastercoupon-title_font_size').val();
        introduction_font_size_1 = $('#mastercoupon-introduction_font_size_1').val();
        introduction_font_size_2 = $('#mastercoupon-introduction_font_size_2').val();
        introduction_font_size_3 = $('#mastercoupon-introduction_font_size_3').val();
        // ============================
        // Get Font Size End
        // ============================

        // ============================
        // Get Position Start
        // ============================
        var title_postion, introduction_postion_1, introduction_postion_2, introduction_postion_3;
        title_postion = $('#mastercoupon-title_postion').val();
        introduction_postion_1 = $('#mastercoupon-introduction_postion_1').val();
        introduction_postion_2 = $('#mastercoupon-introduction_postion_2').val();
        introduction_postion_3 = $('#mastercoupon-introduction_postion_3').val();
        // ============================
        // Get Position End
        // ============================

        // ============================
        // Get Check Barcode Start
        // ============================
        var vl_display_barcode_flg;
        var display_barcode_flg = $('#mastercoupon-display_barcode_flg').prop('checked');
        if (display_barcode_flg == true) {
            vl_display_barcode_flg = 1;
        } else {
            vl_display_barcode_flg = 0;
        }
        // ============================
        // Get Check Barcode End
        // ============================

        // ============================
        // Get combine_with_other_coupon_flg
        // ============================
        var vl_combine_with_other_coupon_flg;
        var combine_with_other_coupon_flg = $('#mastercoupon-combine_with_other_coupon_flg').prop('checked');
        if (combine_with_other_coupon_flg == true) {
            vl_combine_with_other_coupon_flg = 1;
        } else {
            vl_combine_with_other_coupon_flg = 0;
        }
        // ============================
        // Get combine_with_other_coupon_flg
        // ============================

        // ============================
        // Get mastercoupon-expire_date Start
        // ============================
        var expire_date;
        expire_date = $('#mastercoupon-expire_date').val();
        // ============================
        // Get mastercoupon-expire_date End
        // ============================ 

        // ============================
        // Get mastercoupon-expire_auto_set Start
        // ============================
        var vl_expire_auto_set;
        var expire_auto_set = $('#mastercoupon-expire_auto_set').prop('checked');
        if (expire_auto_set == true) {
            vl_expire_auto_set = 1;
        } else {
            vl_expire_auto_set = 0;
        }
        // ============================
        // Get mastercoupon-expire_auto_set End
        // ============================

        // ============================
        // Get Jan Code Start
        // ============================
        var jan_code;
        jan_code = $('#mastercoupon-jan_code_before_create').val();
        // ============================
        // Get Jan Code End
        // ============================

        // ============================
        // Value mastercoupon-title start
        // ============================
        var vl_mastercoupon_title, vl_mastercoupon_introduction_1, vl_mastercoupon_introduction_2, vl_mastercoupon_introduction_3;
        vl_mastercoupon_title = $('#mastercoupon-title').val();
        vl_mastercoupon_introduction_1 = $('#mastercoupon-introduction_1').val();
        vl_mastercoupon_introduction_2 = $('#mastercoupon-introduction_2').val();
        vl_mastercoupon_introduction_3 = $('#mastercoupon-introduction_3').val();
        // ============================
        // Value mastercoupon-title End
        // ============================
        var html_append_image = '',
                html_append_title = '',
                html_append_introduction_1 = '',
                html_append_introduction_2 = '',
                html_append_introduction_3 = '',
                html_append_jan_code = '';

        var obj = {};
        var src = $('.kv-file-content img').attr('src');
//        alert(src);
        // Add Image start
        if (src !== '/api/uploads/no-image.jpg') {
            html_append_image = '<div class="mastercoupon-image">'
                    + '<div class="col-md-12">'
                    + '<img src=' + src + ' alt="" width="230px" height="230px">'
                    + '</div>'
                    + '</div>';
            $('.mastercoupon-image').html('').append(html_append_image);
        }
        // Add Image end
        // Add title top and bottom start
        if (vl_mastercoupon_title.length !== 0) {
            //alert(vl_mastercoupon_title);
            html_append_title = '<div class="col-md-12 title_preview_coupon">'
                    + '<div class="col-md-7">'
                    + '<p>★★★★★★★★★★★★★★★★★★★★★★★★★</p>'
                    + '</div>'
                    + '</div>'
                    + '<div class="col-md-12">'
                    + '<div class="col-md-12 title" style="text-align: left;">'
                    + '<p>' + vl_mastercoupon_title + '</p>'
                    + '</div>'
                    + '</div>'
                    + '<div class="col-md-12">'
                    + '<div class="col-md-7">'
                    + '<p>★★★★★★★★★★★★★★★★★★★★★★★★★</p>'
                    + '</div>'
                    + '</div>';

            $('.mastercoupon-title_top').html('').append(html_append_title);
            $('.mastercoupon-title_bottom').html('').append(html_append_title);
            if (title_font_size == 0) {
                $(".title").removeClass("font_nomal").addClass("font_large");
            } else if (title_font_size == 1) {
                $(".title").removeClass("font_large").addClass("font_nomal");
            } else if (title_font_size == 2) {
                $(".title").removeClass("font_large").removeClass("font_nomal");
            }
        }
        // Add title top and bottom end

        // Add introduction_1 top and bottom start
        html_append_introduction_1 = '<div class="col-md-12 introduction_1 introduction" >'
                + '<p>' + vl_mastercoupon_introduction_1 + '</p>'
                + '</div>';
        $('.mastercoupon-introduction_1_top').html('').append(html_append_introduction_1);
        $('.mastercoupon-introduction_1_bottom').html('').append(html_append_introduction_1);

        if (introduction_font_size_1 == 0) {
            $(".introduction_1").removeClass("font_nomal").addClass("font_large");
        } else if (introduction_font_size_1 == 1) {
            $(".introduction_1").removeClass("font_large").addClass("font_nomal");
        } else if (introduction_font_size_1 == 2) {
            $(".introduction_1").removeClass("font_large").removeClass("font_nomal");
        }
        // Add introduction_1 top and bottom end

        // Add introduction_2 top and bottom start
        html_append_introduction_2 = '<div class="col-md-11 introduction_2 introduction">'
                + '<p>' + vl_mastercoupon_introduction_2 + '</p>'
                + '</div>';
        $('.mastercoupon-introduction_2_top').html('').append(html_append_introduction_2);
        $('.mastercoupon-introduction_2_bottom').html('').append(html_append_introduction_2);
        if (introduction_font_size_2 == 0) {
            $(".introduction_2").removeClass("font_nomal").addClass("font_large");
        } else if (introduction_font_size_2 == 1) {
            $(".introduction_2").removeClass("font_large").addClass("font_nomal");
        } else if (introduction_font_size_2 == 2) {
            $(".introduction_2").removeClass("font_large").removeClass("font_nomal");
        }
        // Add introduction_2 top and bottom end

        // Add introduction_3 top and bottom start
        html_append_introduction_3 = '<div class="col-md-11 introduction_3 introduction">'
                + '<p>' + vl_mastercoupon_introduction_3 + '</p>'
                + '</div>';
        $('.mastercoupon-introduction_3_top').html('').append(html_append_introduction_3);
        $('.mastercoupon-introduction_3_bottom').html('').append(html_append_introduction_3);
        if (introduction_font_size_3 == 0) {
            $(".introduction_3").removeClass("font_nomal").addClass("font_large");
        } else if (introduction_font_size_3 == 1) {
            $(".introduction_3").removeClass("font_large").addClass("font_nomal");
        } else if (introduction_font_size_3 == 2) {
            $(".introduction_3").removeClass("font_large").removeClass("font_nomal");
        }
        // Add introduction_3 top and bottom end

        // Add jan_code_preview start
        html_append_jan_code = '<img src="http://' + window.location.hostname + '/api/general/barcode/' + jan_code + '.jpg" alt="" >';
        $('.jan_code_preview').html('').append(html_append_jan_code);
        // Add jan_code_preview end

        // Check display bar code
        checkDisplayBarcode(vl_display_barcode_flg, vl_expire_auto_set, expire_date, vl_combine_with_other_coupon_flg);

        // **********************************
        // Check display Start
        // **********************************
        // case : 0
        if (title_postion == 0 && introduction_postion_1 == 0 && introduction_postion_2 == 0 && introduction_postion_3 == 0)
        {
            // Top
            $('.mastercoupon-title_top').show();
            $('.mastercoupon-introduction_1_top').show();
            $('.mastercoupon-introduction_2_top').show();
            $('.mastercoupon-introduction_3_top').show();
            // Bottom
            $('.mastercoupon-title_bottom').hide();
            $('.mastercoupon-introduction_1_bottom').hide();
            $('.mastercoupon-introduction_2_bottom').hide();
            $('.mastercoupon-introduction_3_bottom').hide();
        }
        // case : 1
        if (title_postion == 0 && introduction_postion_1 == 0 && introduction_postion_2 == 0 && introduction_postion_3 == 1)
        {
            // Top
            $('.mastercoupon-title_top').show();
            $('.mastercoupon-introduction_1_top').show();
            $('.mastercoupon-introduction_2_top').show();
            $('.mastercoupon-introduction_3_top').hide();
            // Bottom
            $('.mastercoupon-title_bottom').hide();
            $('.mastercoupon-introduction_1_bottom').hide();
            $('.mastercoupon-introduction_2_bottom').hide();
            $('.mastercoupon-introduction_3_bottom').show();
        }
        // case : 2
        if (title_postion == 0 && introduction_postion_1 == 0 && introduction_postion_2 == 1 && introduction_postion_3 == 0)
        {
            // Top
            $('.mastercoupon-title_top').show();
            $('.mastercoupon-introduction_1_top').show();
            $('.mastercoupon-introduction_2_top').hide();
            $('.mastercoupon-introduction_3_top').show();
            // Bottom
            $('.mastercoupon-title_bottom').hide();
            $('.mastercoupon-introduction_1_bottom').hide();
            $('.mastercoupon-introduction_2_bottom').show();
            $('.mastercoupon-introduction_3_bottom').hide();
        }
        // case : 3
        if (title_postion == 0 && introduction_postion_1 == 0 && introduction_postion_2 == 1 && introduction_postion_3 == 1)
        {
            // Top
            $('.mastercoupon-title_top').show();
            $('.mastercoupon-introduction_1_top').show();
            $('.mastercoupon-introduction_2_top').hide();
            $('.mastercoupon-introduction_3_top').hide();
            // Bottom
            $('.mastercoupon-title_bottom').hide();
            $('.mastercoupon-introduction_1_bottom').hide();
            $('.mastercoupon-introduction_2_bottom').show();
            $('.mastercoupon-introduction_3_bottom').show();
        }
        // case : 4
        if (title_postion == 0 && introduction_postion_1 == 1 && introduction_postion_2 == 0 && introduction_postion_3 == 0)
        {
            // Top
            $('.mastercoupon-title_top').show();
            $('.mastercoupon-introduction_1_top').hide();
            $('.mastercoupon-introduction_2_top').show();
            $('.mastercoupon-introduction_3_top').show();
            // Bottom
            $('.mastercoupon-title_bottom').hide();
            $('.mastercoupon-introduction_1_bottom').show();
            $('.mastercoupon-introduction_2_bottom').hide();
            $('.mastercoupon-introduction_3_bottom').hide();
        }
        // case : 5
        if (title_postion == 0 && introduction_postion_1 == 1 && introduction_postion_2 == 0 && introduction_postion_3 == 1)
        {
            // Top
            $('.mastercoupon-title_top').show();
            $('.mastercoupon-introduction_1_top').hide();
            $('.mastercoupon-introduction_2_top').show();
            $('.mastercoupon-introduction_3_top').hide();
            // Bottom
            $('.mastercoupon-title_bottom').hide();
            $('.mastercoupon-introduction_1_bottom').show();
            $('.mastercoupon-introduction_2_bottom').hide();
            $('.mastercoupon-introduction_3_bottom').show();
        }
        // case : 6
        if (title_postion == 0 && introduction_postion_1 == 1 && introduction_postion_2 == 1 && introduction_postion_3 == 0)
        {
            // Top
            $('.mastercoupon-title_top').show();
            $('.mastercoupon-introduction_1_top').hide();
            $('.mastercoupon-introduction_2_top').hide();
            $('.mastercoupon-introduction_3_top').show();
            // Bottom
            $('.mastercoupon-title_bottom').hide();
            $('.mastercoupon-introduction_1_bottom').show();
            $('.mastercoupon-introduction_2_bottom').show();
            $('.mastercoupon-introduction_3_bottom').hide();
        }
        // case : 7
        if (title_postion == 0 && introduction_postion_1 == 1 && introduction_postion_2 == 1 && introduction_postion_3 == 1)
        {
            // Top
            $('.mastercoupon-title_top').show();
            $('.mastercoupon-introduction_1_top').hide();
            $('.mastercoupon-introduction_2_top').hide();
            $('.mastercoupon-introduction_3_top').hide();
            // Bottom
            $('.mastercoupon-title_bottom').hide();
            $('.mastercoupon-introduction_1_bottom').show();
            $('.mastercoupon-introduction_2_bottom').show();
            $('.mastercoupon-introduction_3_bottom').show();
        }
        // case : 8
        if (title_postion == 1 && introduction_postion_1 == 0 && introduction_postion_2 == 0 && introduction_postion_3 == 0)
        {
            // Top
            $('.mastercoupon-title_top').hide();
            $('.mastercoupon-introduction_1_top').show();
            $('.mastercoupon-introduction_2_top').show();
            $('.mastercoupon-introduction_3_top').show();
            // Bottom
            $('.mastercoupon-title_bottom').show();
            $('.mastercoupon-introduction_1_bottom').hide();
            $('.mastercoupon-introduction_2_bottom').hide();
            $('.mastercoupon-introduction_3_bottom').hide();
        }
        // case : 9
        if (title_postion == 1 && introduction_postion_1 == 0 && introduction_postion_2 == 0 && introduction_postion_3 == 1)
        {
            // Top
            $('.mastercoupon-title_top').hide();
            $('.mastercoupon-introduction_1_top').show();
            $('.mastercoupon-introduction_2_top').show();
            $('.mastercoupon-introduction_3_top').hide();
            // Bottom
            $('.mastercoupon-title_bottom').show();
            $('.mastercoupon-introduction_1_bottom').hide();
            $('.mastercoupon-introduction_2_bottom').hide();
            $('.mastercoupon-introduction_3_bottom').show();
        }
        // case : 10
        if (title_postion == 1 && introduction_postion_1 == 0 && introduction_postion_2 == 1 && introduction_postion_3 == 0)
        {
            // Top
            $('.mastercoupon-title_top').hide();
            $('.mastercoupon-introduction_1_top').show();
            $('.mastercoupon-introduction_2_top').hide();
            $('.mastercoupon-introduction_3_top').show();
            // Bottom
            $('.mastercoupon-title_bottom').show();
            $('.mastercoupon-introduction_1_bottom').hide();
            $('.mastercoupon-introduction_2_bottom').show();
            $('.mastercoupon-introduction_3_bottom').hide();
        }
        // case : 11
        if (title_postion == 1 && introduction_postion_1 == 0 && introduction_postion_2 == 1 && introduction_postion_3 == 1)
        {
            // Top
            $('.mastercoupon-title_top').hide();
            $('.mastercoupon-introduction_1_top').show();
            $('.mastercoupon-introduction_2_top').hide();
            $('.mastercoupon-introduction_3_top').hide();
            // Bottom
            $('.mastercoupon-title_bottom').show();
            $('.mastercoupon-introduction_1_bottom').hide();
            $('.mastercoupon-introduction_2_bottom').show();
            $('.mastercoupon-introduction_3_bottom').show();
        }
        // case : 12
        if (title_postion == 1 && introduction_postion_1 == 1 && introduction_postion_2 == 0 && introduction_postion_3 == 0)
        {
            // Top
            $('.mastercoupon-title_top').hide();
            $('.mastercoupon-introduction_1_top').hide();
            $('.mastercoupon-introduction_2_top').show();
            $('.mastercoupon-introduction_3_top').show();
            // Bottom
            $('.mastercoupon-title_bottom').show();
            $('.mastercoupon-introduction_1_bottom').show();
            $('.mastercoupon-introduction_2_bottom').hide();
            $('.mastercoupon-introduction_3_bottom').hide();
        }
        // case : 13
        if (title_postion == 1 && introduction_postion_1 == 1 && introduction_postion_2 == 0 && introduction_postion_3 == 1)
        {
            // Top
            $('.mastercoupon-title_top').hide();
            $('.mastercoupon-introduction_1_top').hide();
            $('.mastercoupon-introduction_2_top').show();
            $('.mastercoupon-introduction_3_top').hide();
            // Bottom
            $('.mastercoupon-title_bottom').show();
            $('.mastercoupon-introduction_1_bottom').show();
            $('.mastercoupon-introduction_2_bottom').hide();
            $('.mastercoupon-introduction_3_bottom').show();
        }
        // case : 14
        if (title_postion == 1 && introduction_postion_1 == 1 && introduction_postion_2 == 1 && introduction_postion_3 == 0)
        {
            // Top
            $('.mastercoupon-title_top').hide();
            $('.mastercoupon-introduction_1_top').hide();
            $('.mastercoupon-introduction_2_top').hide();
            $('.mastercoupon-introduction_3_top').show();
            // Bottom
            $('.mastercoupon-title_bottom').show();
            $('.mastercoupon-introduction_1_bottom').show();
            $('.mastercoupon-introduction_2_bottom').show();
            $('.mastercoupon-introduction_3_bottom').hide();
        }
        // case : 15
        if (title_postion == 1 && introduction_postion_1 == 1 && introduction_postion_2 == 1 && introduction_postion_3 == 1)
        {
            // Top
            $('.mastercoupon-title_top').hide();
            $('.mastercoupon-introduction_1_top').hide();
            $('.mastercoupon-introduction_2_top').hide();
            $('.mastercoupon-introduction_3_top').hide();
            // Bottom
            $('.mastercoupon-title_bottom').show();
            $('.mastercoupon-introduction_1_bottom').show();
            $('.mastercoupon-introduction_2_bottom').show();
            $('.mastercoupon-introduction_3_bottom').show();
        }
        // **********************************
        // Check display End
        // **********************************

    }
    function checkDisplayBarcode(vl_display_barcode_flg, vl_expire_auto_set, expire_date, vl_combine_with_other_coupon_flg) {
        // Check display bar code
        if (vl_display_barcode_flg == 1) {
            $('.jan_code_preview').show();
            $('.related-post').show();
            //$('.tl').hide();
            if (vl_combine_with_other_coupon_flg != 1) {
                $('.tl').show();
                $('.tl').html('').append('<p>※他のクーポンと併用できません。<p>');
            } else {
                $('.tl').hide();
            }
        } else {
            $('.related-post').hide();
            $('.jan_code_preview').hide();
            //$('.tl').hide();
            if (vl_combine_with_other_coupon_flg != 1) {
                $('.tl').show();
                $('.tl').html('').append('<p>※他のクーポンと併用できません。<p>');
            } else {
                $('.tl').hide();
            }
        }

        if (vl_expire_auto_set != 1 && expire_date != null)
        {
            $('.expiration_date_tt').html('').append('<p>有効期限<p>');
            $('.expire_date_vl').html('').append('<p>' + expire_date + '<p>');
            $('.expire_date_tl').html('').append('<p>まで<p>');
            if (vl_combine_with_other_coupon_flg != 1) {
                $('.tl').show();
                $('.tl').html('').append('<p>※他のクーポンと併用できません。<p>');
            }else {
                $('.tl').hide();
            }
            //$('.tl').hide();
        }

        if (expire_date.length == 0)
        {
            $('.expiration_date_tt').html('').append('<p>有効期限<p>');
            $('.expire_date_vl').html('').append('<p>無期限<p>');
            $('.expire_date_tl').html('');
            if (vl_combine_with_other_coupon_flg != 1) {
                $('.tl').show();
                $('.tl').html('').append('<p>※他のクーポンと併用できません。<p>');
            }else {
                $('.tl').hide();
            }
            //$('.tl').hide();
        }
        if (vl_expire_auto_set == 1) {
            $('.expiration_date_tt').html('').append('<p>有効期限<p>');
            $('.expire_date_vl').html('').append('<p>' + getDateTime() + '<p>');
            $('.expire_date_tl').html('').append('<p>末日まで<p>');
            if (vl_combine_with_other_coupon_flg != 1) {
                $('.tl').show();
                $('.tl').html('').append('<p>※他のクーポンと併用できません。<p>');
            }else {
                $('.tl').hide();
            }
            //$('.tl').hide();
        }
//        if (vl_combine_with_other_coupon_flg == 1 && vl_display_barcode_flg == 1 && vl_expire_auto_set == 1) {
//            $('.expiration_date_tt').html('').append('<p>有効期限<p>');
//            $('.expire_date_vl').html('').append('<p>' + getDateTime() + '<p>');
//            $('.expire_date_tl').html('').append('<p>末日まで<p>');
//            $('.tl').show();
//            $('.tl').html('').append('<p>※他のクーポンと併用できません。<p>');
//            $('.jan_code_preview').hide();
//            $('.related-post').hide();
//
//        }
    }
    function getDateTime() {
        var currentdate = new Date();

        var datetime = currentdate.getFullYear() + "年"
                + (currentdate.getMonth() + 1) + "月";

        return datetime;
    }
    $(function () {
        $(document).on('click', '#<?= $idBtnConfirm ?>', function () {
            if ($(".kv-error-close").length > 0) {
                return false;
            }
            previewDialog2();
            return  true;
        });
        //set defaulse validate prevent submit if not click btnSubmit
        var validated = 0;
        $(document).on('click', '#<?= $btnSubmit ?>', function () {
            validated = 1;
        });
        //can submit if validated != 0 
        $('#<?= $formId ?>').on('beforeSubmit', function (event) {
            if (validated == 0)
                return false;
        });
        //show modal if all validate
        $('#<?= $formId ?>').on('afterValidate', function (event, attribute, messages) {

            if ($(this).find('.has-error').length == 0) {

                $('#<?= $modalId ?>').modal("show");

            }
        });
    });
</script>
