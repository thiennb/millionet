<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$lowerModelName = strtolower($modelName);
$idPreview = "preview-" . $lowerModelName;
$fields = array();
?>
<div class="form-horizontal">
    <?php
    foreach ($data as $key => $item) {
        ?>
        <div class="form-group">
            <?php if (!empty($item['label']) && $key != 'question_priview') { ?>
                <label class="col-sm-6"><?= $item['label']; ?></label>
                <div class="col-sm-6">
                    <?php
                    $field = array();
                    $idField = $lowerModelName . '-' . $key;
                    if (isset($item['model'])) {
                        //orther model
                        $idField = strtolower($item['model']) . '-' . $key;
                    }
                    $idFieldPreview = "preview-" . $idField;
                    $type = "";
                    if ($item['type'] == 'image' || $item['type'] == 'input') {
                        if (isset($item['area'])) {
                            ?>
                            <p style="white-space:pre-wrap;" id="<?= $idFieldPreview ?>"></p>
                        <?php } else { ?>
                            <div class="<?= $idFieldPreview ?>"></div>
                            <?php
                        }
                    } else {
                        ?>
                        <span id="<?= $idFieldPreview ?>"></span>
                        <?php
                    }

                    if ($item['type'] == 'input_coupon') {
                        ?>
                        <div class="<?= $idFieldPreview ?>"></div>
                        <?php
                    } else {
                        ?>
                        <span id="<?= $idFieldPreview ?>"></span>
                        <?php
                    }

                    $field['id'] = $idField;
                    $field['idPreview'] = $idFieldPreview;
                    $field['type'] = $item['type'];
                    if (isset($item['groupWith'])) {
                        $field['groupWith'] = $lowerModelName . '-' . $item['groupWith'];
                    }
                    if (isset($item['groupWithSeat'])) {
                        $field['groupWithSeat'] = $lowerModelName . '-' . $item['groupWithSeat'];
                    }

                    if (isset($item['show_coupon_check'])) {

                        $field['show_coupon_check'] = true;
                    }

                    if (isset($item['confirm_booking'])) {

                        $field['confirm_booking'] = true;
                    }

                    // Show (store) start
                    if (isset($item['show_in_booking_flg_check'])) {

                        $field['show_in_booking_flg_check'] = true;
                    }

                    if (isset($item['show_in_notice_flg_check'])) {

                        $field['show_in_notice_flg_check'] = true;
                    }

                    if (isset($item['show_in_coupon_flg_check'])) {

                        $field['show_in_coupon_flg_check'] = true;
                    }

                    if (isset($item['show_in_product_category_flg_check'])) {

                        $field['show_in_product_category_flg_check'] = true;
                    }

                    if (isset($item['show_in_my_page_flg_check'])) {

                        $field['show_in_my_page_flg_check'] = true;
                    }

                    if (isset($item['show_in_staff_flg_check'])) {

                        $field['show_in_staff_flg_check'] = true;
                    }
                    // Show (store) end

                    $fields[] = $field;
                    ?>

                </div>
            <?php } else { ?>
                <div id="priview-quetion">
                    <div id="table_checklist_type1">
                        <div class="col-md-11 table_checklist_preview_box">
                            <div class="col-md-2 table_checklist_preview_option_box_title">注意事項</div>
                            <div class="col-md-10 table_checklist_preview_option">
                                <div id="append_checklist_type1">

                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="table_checklist_type2">
                        <div class="col-md-11 table_checklist_preview_option_box">
                            <div class="col-md-2 table_checklist_preview_option_box_title">質問事項</div>
                            <div class="col-md-10 table_checklist_preview_option">
                                <div id="table_checklist_option1">

                                </div>
                                <div id="table_checklist_option2">

                                </div> 
                                <div id="table_checklist_option3">

                                </div> 
                            </div>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </div>
        <?php
    }
    ?>

</div>

<script>

    function previewDialog(notDelay){
        
        //fix for IE
        if(!notDelay){
           
            notDelay = true;
        }

        if (notDelay == false) {
            //if notDelay == false delay ultil have image
            var checkExist = setInterval(function () {
                //check until exit 
                if ($('.file-initial-thumbs').find('img').length > 0) {
                    previewDialog();
                    clearInterval(checkExist);
                }
            }, 100);
        }
        //hot fix for coupon

<?php
foreach ($fields as $item) {
    if ($item['type'] == 'input_coupon') {
        isset($item['groupWith']) ? $group = " ' ' + $('#{$item['groupWith']}').val()" : $group = " ''";
        isset($item['groupWithSeat']) ? $groupSeat = " ' ' + $('#{$item['groupWithSeat']}').val()" : $groupSeat = " ''";
        ?>
                var $sText = $('#<?= $item['id'] ?>').val() + <?= $group ?>;
                //$('#<?= $item['idPreview'] ?>').text($('#<?= $item['id'] ?>').val() + <?= $group ?>);

                $('.<?= $item['idPreview'] ?>').html('').append('<span>' + $sText + '</span>');

                if (<?= $groupSeat ?> !== "") {
                    $('#<?= $item['idPreview'] ?>').text($('#<?= $item['id'] ?>').val() + "     ～     " + <?= $groupSeat ?>);
                }




        <?php
    } else if ($item['type'] == 'input') {
        isset($item['groupWith']) ? $group = " ' ' + $('#{$item['groupWith']}').val()" : $group = " ''";
        isset($item['groupWithSeat']) ? $groupSeat = " ' ' + $('#{$item['groupWithSeat']}').val()" : $groupSeat = " ''";
        ?>
                $('#<?= $item['idPreview'] ?>').text($('#<?= $item['id'] ?>').val() + <?= $group ?>);

                if (<?= $groupSeat ?> !== "") {
                    $('#<?= $item['idPreview'] ?>').text($('#<?= $item['id'] ?>').val() + "     ～     " + <?= $groupSeat ?>);
                }

        <?php
    } else if ($item['type'] == 'hidden') {
        isset($item['groupWith']) ? $group = " ' ' + $('#{$item['groupWith']}').val()" : $group = " ''";
        isset($item['groupWithSeat']) ? $groupSeat = " ' ' + $('#{$item['groupWithSeat']}').val()" : $groupSeat = " ''";
        ?>
                $('#<?= $item['idPreview'] ?>').html($('#<?= $item['id'] ?>').val() + <?= $group ?>);

                if (<?= $groupSeat ?> !== "") {
                    $('#<?= $item['idPreview'] ?>').html($('#<?= $item['id'] ?>').val() + "     ～     " + <?= $groupSeat ?>);
                }


        <?php
    } else if ($item['type'] == 'select') {
        ?>
                if ($('#<?= $item['id'] ?>  option:selected').val()) {
                    $('#<?= $item['idPreview'] ?>').text($('#<?= $item['id'] ?>  option:selected').text());
                } else {
                    $('#<?= $item['idPreview'] ?>').text("");
                }
        <?php
    } else if ($item['type'] == 'image') {
        ?>
                var $src_img = $('.<?= 'field-' . $item['id'] ?>').find('img').clone();
                $src_img.attr('width', '200px').attr('height', '200px');

                //                        if($src_img == '' || $src_img == null){
                //                            $src_img = '/api/uploads/no-image.jpg';
                //                        }
                //                        $('.<?= $item['idPreview'] ?>').attr('src', $src_img);
                if ($src_img == '' || $src_img == null || $src_img.attr('src') == '/api/uploads/no-image.jpg') {
                    $('.<?= $item['idPreview'] ?>').html('');
                } else {
                    $('.<?= $item['idPreview'] ?>').html('').append($src_img);
                }

        <?php
    } else if ($item['type'] == 'radio') {
        ?>
                $('#<?= $item['idPreview'] ?>').text($('#<?= $item['id'] ?> input:checked').parent().text());


        <?php
    } else if ($item['type'] == 'radio_customer') {
        ?>
                var $radio_customer_render = $('#radio_customer_value_' + $('#masternotice-type_send:checked').val()).text();
                if ($('#masternotice-type_send:checked').val() == '1') {
                    $radio_customer_render = $('#masternotice-send_date').val() + "  " + $('#masternotice-send_time').val();
                }
                $('#<?= $item['idPreview'] ?>').text($radio_customer_render);
        <?php
    } else if ($item['type'] == 'text_confirm_booking') {
        ?>
                var value_check_list = $('#value_textarea_qs').val();

                if (value_check_list.length != 0) {

                    var myArray = JSON.parse(value_check_list);

                    if (myArray.length == 0) {
                        $('#<?= $item['idPreview'] ?>').text("なし");
                    } else {
                        $('#<?= $item['idPreview'] ?>').text("あり");
                    }

                } else {
                    $('#<?= $item['idPreview'] ?>').text("なし");
                }
        <?php
    } else if ($item['type'] == 'checkbox_store') {
        // Check Show (store) start
        if (isset($item['show_in_booking_flg_check']) && $item['show_in_booking_flg_check'] == true) {
            ?>


                    var value_show_in_booking_flg = $('input[name="MasterStore[show_in_booking_flg]"]:checked').map(function () {
                        return $(this).val();
                    });

                    if (value_show_in_booking_flg[0] == '0') {
                        $('#<?= $item['idPreview'] ?>').text("表示しない");
                    } else if (value_show_in_booking_flg[0] == '1') {
                        $('#<?= $item['idPreview'] ?>').text("表示する");
                    }

            <?php
        }
        ?>
        <?php
        if (isset($item['show_in_notice_flg_check']) && $item['show_in_notice_flg_check'] == true) {
            ?>
                    var value_show_in_notice_flg_check = $('input[name="MasterStore[show_in_notice_flg]"]:checked').map(function () {
                        return $(this).val();
                    });

                    if (value_show_in_notice_flg_check[0] == '0') {
                        $('#<?= $item['idPreview'] ?>').text("表示しない");
                    } else if (value_show_in_notice_flg_check[0] == '1') {
                        $('#<?= $item['idPreview'] ?>').text("表示する");
                    }
            <?php
        }
        ?>
        <?php
        if (isset($item['show_in_coupon_flg_check']) && $item['show_in_coupon_flg_check'] == true) {
            ?>
                    var value_show_in_coupon_flg_check = $('input[name="MasterStore[show_in_coupon_flg]"]:checked').map(function () {
                        return $(this).val();
                    });

                    if (value_show_in_coupon_flg_check[0] == '0') {
                        $('#<?= $item['idPreview'] ?>').text("表示しない");
                    } else if (value_show_in_coupon_flg_check[0] == '1') {
                        $('#<?= $item['idPreview'] ?>').text("表示する");
                    }
            <?php
        }
        ?>
        <?php
        if (isset($item['show_in_product_category_flg_check']) && $item['show_in_product_category_flg_check'] == true) {
            ?>
                    var value_show_in_product_category_flg_check = $('input[name="MasterStore[show_in_product_category_flg]"]:checked').map(function () {
                        return $(this).val();
                    });

                    if (value_show_in_product_category_flg_check[0] == '0') {
                        $('#<?= $item['idPreview'] ?>').text("表示しない");
                    } else if (value_show_in_product_category_flg_check[0] == '1') {
                        $('#<?= $item['idPreview'] ?>').text("表示する");
                    }
            <?php
        }
        ?>
        <?php
        if (isset($item['show_in_my_page_flg_check']) && $item['show_in_my_page_flg_check'] == true) {
            ?>
                    var value_show_in_my_page_flg_check = $('input[name="MasterStore[show_in_my_page_flg]"]:checked').map(function () {
                        return $(this).val();
                    });

                    if (value_show_in_my_page_flg_check[0] == '0') {
                        $('#<?= $item['idPreview'] ?>').text("表示しない");
                    } else if (value_show_in_my_page_flg_check[0] == '1') {
                        $('#<?= $item['idPreview'] ?>').text("表示する");
                    }
            <?php
        }
        ?>

        <?php
        if (isset($item['show_in_staff_flg_check']) && $item['show_in_staff_flg_check'] == true) {
            ?>
                    var value_show_in_staff_flg_check = $('input[name="MasterStore[show_in_staff_flg]"]:checked').map(function () {
                        return $(this).val();
                    });

                    if (value_show_in_staff_flg_check[0] == '0') {
                        $('#<?= $item['idPreview'] ?>').text("表示しない");
                    } else if (value_show_in_staff_flg_check[0] == '1') {
                        $('#<?= $item['idPreview'] ?>').text("表示する");
                    }
            <?php
        }
        ?>

        <?php
    } else if ($item['type'] == 'checkbox') {
        ?>
        <?php
        if (isset($item['show_coupon_check']) && $item['show_coupon_check'] == true) {
            ?>
                    //================================
                    // Check Show Coupon Datpdt start
                    //================================
                    var value_show_coupon = $('input[name="MasterCoupon[show_coupon][]"]:checked').map(function () {
                        return $(this).val();
                    });
                    //alert(value_show_coupon.length);
                    if (value_show_coupon.length >= 2) {
                        //alert(value_show_coupon.length);
                        var index, len, value = null;

                        for (index = 0, len = value_show_coupon.length; index < len; ++index) {
                            if (value_show_coupon[index] == '00') {
                                value = "予約サイト・会員アプリ";
                            } else {
                                value += "   ,   レシート出力";
                            }
                        }
                        $('#<?= $item['idPreview'] ?>').text(value);
                    }
                    if (value_show_coupon.length == 1) {
                        if (value_show_coupon[0] == '00') {
                            $('#<?= $item['idPreview'] ?>').text("予約サイト・会員アプリ");
                        }
                        if (value_show_coupon[0] == '01') {
                            $('#<?= $item['idPreview'] ?>').text("レシート出力");
                        }
                    }
                    if (value_show_coupon.length == 0) {

                        $('#<?= $item['idPreview'] ?>').text("");
                    }

        <?php } else { ?>
                    if ($('#<?= $item['id'] ?>').prop('checked')) {
                        $('#<?= $item['idPreview'] ?>').text("あり");
                    } else {
                        $('#<?= $item['idPreview'] ?>').text("なし");
                    }
                    //================================
                    // Check Show Coupon Datpdt end
                    //================================
        <?php } ?>
        <?php
    } else if ($item['type'] == 'table') {
        ?>
                var $render_tabel = "";
                $render_tabel += "<tr>";
                $('.<?= $item['header'] ?>').each(function () {
                    $render_tabel += "<th class='div-border text-center'>" + $(this).text() + "</th>";
                })
                $render_tabel += "</tr>";
                var $body_table = $('table.<?= $item['table'] ?>').find('tr:not(:eq(0))');
                $body_table.each(function () {
                    var $reduced_tax_flg;
                    if ($(this).find('td').first().find('input[type="checkbox"]').prop('checked') == true) {
                        $reduced_tax_flg = $('.<?= $item['header'] ?>').first().text()
                    } else {
                        $reduced_tax_flg = ''
                    }
                    var $rate = $(this).find('td:nth-child(2)').find('div').find('input').val();
                    var $start_date = $(this).find('td:nth-child(4)').find('div').find('input').val();
                    var $end_date = $(this).find('td:nth-child(6)').find('div').find('input').val();
                    if ($reduced_tax_flg != '' || $rate != '' || $start_date != '' || $end_date != '') {
                        $render_tabel += "<tr>";
                        $render_tabel += "<td class='div-border text-center col-sm-3'>" + $reduced_tax_flg + "</td>";
                        $render_tabel += "<td class='div-border text-center col-sm-3'>" + $rate
                                + $(this).find('td:nth-child(3)').text() + "</td>";
                        $render_tabel += "<td class='div-border text-center col-sm-6'>" + $start_date + '&nbsp;&nbsp;~&nbsp;&nbsp; ' + $end_date + "</td>";
                        $render_tabel += "</tr>";
                    }
                })
                $('#<?= $item['idPreview'] ?>').html($render_tabel);
        <?php
    } else if ($item['type'] == 'table-normal') {
        ?>
                var $i = 0;
                var $col = <?= $item['column'] ?>;
                $col = ($col) ? $col.split(",") : $col;
                var $col_num = $col.length;
                var $header = $('#<?= $item['table'] ?>').find('tr:eq(0)').find('th');
                var $header_arr = [];
                $header.each(function () {
                    $header_arr.push($(this).text());
                })
                var $render_tabel = "<table>";
                $render_tabel += "<tr>";
                //render header
                for ($i = 0; $i < $col_num; $i++) {
                    $render_tabel += "<th>" + $header_arr[$i] + "</th>";
                }
                $render_tabel += "</tr>";
                //render body
                var $body_tr = $('#<?= $item['table'] ?>').find('tr:not(:eq(0))');
                $body_tr.each(function () {
                    $render_tabel += "<tr>";
                    var $body_td = $(this).find('td');
                    var $body_td_arr = [];
                    var $no_td = null;
                    $body_td.each(function () {
                        $no_td = ($(this).find('div')) ? $(this).find('div').text() : null;
                        $body_td_arr.push($(this).text());
                    })
                    if ($no_td) {
                        $render_tabel += "<td colspan=" + $col_num + ">" + ($no_td) + "</td>";
                    } else {
                        for ($i = 0; $i < $col_num; $i++) {
                            var $td_render = ($body_td_arr[$i]).replace(/&/g, "&amp;").replace(/>/g, "&gt;").replace(/</g, "&lt;").replace(/"/g, "&quot;");
                            $render_tabel += ($body_td_arr[$i]) ? "<td>" + (($td_render == '') ? " " : $td_render) + "</td>" : '<td></td>';
                        }
                    }
                    $render_tabel += "</tr>";
                })
                $render_tabel += "</table>";
                $('#<?= $item['idPreview'] ?>').html($render_tabel);
        <?php
    }
}
?>
        // Priview Check List
        var value_check_list = $('#value_textarea_qs').val();
        $('#table_checklist_type1').hide();
        $('#table_checklist_type2').hide();
        $('#priview-quetion').hide();
        //alert(value_check_list);
        if (value_check_list.length != 0) {
           
            var myArray = JSON.parse(value_check_list);
            $('#append_checklist_type1').html('');
            //$('#table_checklist_type2').html('');
            $('#priview-quetion').show();
            var i = 0;
            $('#table_checklist_option1').html('');
            $('#table_checklist_option2').html('');
            $('#table_checklist_option3').html('');
            for (i = 0; i < myArray.length; i++) {
                
                if (myArray[i] != null) {
                    
                    if (myArray[i]['type_question'] == '1') {
                        if (myArray[i]['notice_content'] != '' && myArray[i]['notice_content'] != 'no_data') {
                            $('#table_checklist_type1').show();
                            $('#append_checklist_type1').append('<div class="col-md-12"  style="border-top: 1px solid #ddd;min-height: 5em;padding-top: 1em;">' + myArray[i]['notice_content'] + '</div>');
                        }
                    } else if (myArray[i]['type_question'] == '2') {

                        $('#table_checklist_type2').show();
                        // Option 1
                        if (myArray[i]['option'] == '1') {
                            //$('#table_checklist_option1').html('');
                            $('#table_checklist_option1').append('<div class="col-md-12"  style="border-top: 1px solid #ddd;min-height: 5em;padding-top: 1em;">' + myArray[i]['question_content'] + '</div>');
                            var res = myArray[i]['answer'];
                            var index;
                            var html_radio = '', j = 0, k = 1;

                            for (index = 0; index < res.length; index++) {
                                if (res[index] != 'no_data') {
                                    if (j == 0) {
                                        html_radio += '<input type="radio" checked="checked" disabled> A' + k + ' ' + res[index] + '<br>';
                                        j++;
                                    } else {
                                        html_radio += '<input type="radio" disabled> A' + k + ' ' + res[index] + '<br>';
                                    }
                                }
                            }

                            $('#table_checklist_option1').append('<div class="col-md-12"  style="border-top: 1px solid #ddd;min-height: 5em;padding-top: 1em;">' + html_radio + '</div>');
                        }
                        // Option 2
                        if (myArray[i]['option'] == '2') {
                            //$('#table_checklist_option2').html('');
                            $('#table_checklist_option2').append('<div class="col-md-12"  style="border-top: 1px solid #ddd;min-height: 5em;padding-top: 1em;">' + myArray[i]['question_content'] + '（最大回答数' + myArray[i]['CountAsw'] + '）</div>');
                            var res = myArray[i]['answer'];
                            var html_checkbox = '', k = 1;
                            var index;
                            
                            for (index = 0; index < res.length; index++) {
                                if (res[index] != 'no_data') {
                                    html_checkbox += '<input type="checkbox" style="margin-left: 0.5em;" disabled> A' + k + ' ' + res[index]+ '<br>';
                                    k++;
                                }
                            }
                            $('#table_checklist_option2').append('<div class="col-md-12"  style="border-top: 1px solid #ddd;min-height: 5em;padding-top: 1em;">' + html_checkbox + '</div>');
                        }
                        // Option 3
                        if (myArray[i]['option'] == '3'  && myArray[i]['question_content'] != 'no_data') {
                                                     
                            $('#table_checklist_option3').append('<div class="col-md-12"  style="border-top: 1px solid #ddd;min-height: 5em;padding-top: 1em;">' + myArray[i]['question_content'] + '</div>');
                            $('#table_checklist_option3').append('<div class="col-md-12 table_checklist_preview_option_3"  style=""><div class="col-md-1 font_label">A</div><div class="col-md-11 table_checklist_preview_option_3_area"><textarea class="form-control area_question" maxlength="300" rows="3" style="border-radius: 0.7em;" disabled></textarea></div></div>');
                        }
                    }
                }
            }
        }
    }

    $(function () {
        $(document).on('click', '#<?= $idBtnConfirm ?>', function () {
            if ($(".kv-error-close").length > 0) {
                return false;
            }
            previewDialog();
            return  true;
        });
        //set defaulse validate prevent submit if not click btnSubmit
        var validated = 0;
        $(document).on('click', '#<?= $btnSubmit ?>', function () {
            validated = 1;
        });
        //can submit if validated != 0 
        $('#<?= $formId ?>').on('beforeSubmit', function (event) {
            if (validated == 0)
                return false;
        });
        //show modal if all validate
        $('#<?= $formId ?>').on('afterValidate', function (event, attribute, messages) {

            if ($(this).find('.has-error').length == 0) {

                $('#<?= $modalId ?>').modal("show");

            }
        });
    });
</script>
