<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$lowerModelName = strtolower($modelName);
$idPreview = "preview-" . $lowerModelName;
$lowerModelName_1 = strtolower($modelName_1);
$idPreview_1 = "preview-" . $lowerModelName_1;
$fields = array();
?>
<div class="form-horizontal">
    <?php
    foreach ($data as $key => $item) {
        ?>
        <div class="form-group col-sm-12">
            <?php if (!empty($item['label'])) { ?>
                <div class="col-sm-4">
                    <label><?= $item['label']; ?></label>
                </div>
                <?php if($item['type'] == 'input_mult'){ ?>
                    <div class="col-sm-4">
                        <?php
                        $field = array();
                        $idField = $lowerModelName . '-' . $key;
                        $idField_1 = $lowerModelName_1 . '-' . $key;
                        if (isset($item['model'])) {
                            //orther model
                            $idField = strtolower($item['model']) . '-' . $key;
                        }
                        $idFieldPreview = "preview-" . $idField;
                        if($item['type'] == 'input_mult'){
                            $idFieldPreview = "preview-" . $lowerModelName.'-'. split(',', $key)[0];
                            $idFieldPreview_1 = "preview-" . $lowerModelName_1.'-'. split(',', $key)[0];
                        }
                        $type = "";
                        if ($item['type'] == 'image' || $item['type'] == 'input'|| $item['type'] == 'input_mult') {
                            ?>
                            <div class="<?= $idFieldPreview ?>"></div>
                            <?php
                        } else {
                            ?>
                            <span id="<?= $idFieldPreview ?>"></span>
                            <?php
                        }

                        if ($item['type'] == 'input_coupon') {
                            ?>
                            <div class="<?= $idFieldPreview ?>"></div>
                            <?php
                        } else {
                            ?>
                            <span id="<?= $idFieldPreview ?>"></span>
                            <?php
                        }

                        $field['id'] = $idField;
                        $field['id_1'] = $idField_1;
                        $field['idPreview'] = $idFieldPreview;
                        $field['type'] = $item['type'];
                        $field['syb'] = !empty($item['syb'])?$item['syb']:'';
                        $field['key']   = $key;
    //                    $field['syb'] = '';
                        if (isset($item['groupWith'])) {
                            $field['groupWith'] = $lowerModelName . '-' . $item['groupWith'];
                        }
                        if (isset($item['groupWithSeat'])) {
                            $field['groupWithSeat'] = $lowerModelName . '-' . $item['groupWithSeat'];
                        }

                        if (isset($item['show_coupon_check'])) {

                            $field['show_coupon_check'] = true;
                        }

                        $fields[] = $field;
                        ?>

                    </div>
                    <div class="col-sm-2" id='black_list_flg'>
                        <i class="fa fa-exclamation-triangle fa-3x"  style="color:red" aria-hidden="true"></i>
                    </div>
                    <div class="col-sm-2 no-padding" id="to_home_delivery_flg">
                        <span class="to_home_delivery_flg">宅配</span>
                    </div>
                <?php }else { ?>
                    <div class="col-sm-8">
                        <?php
                        $field = array();
                        $idField = $lowerModelName . '-' . $key;
                        $idField_1 = $lowerModelName_1 . '-' . $key;
                        if (isset($item['model'])) {
                            //orther model
                            $idField = strtolower($item['model']) . '-' . $key;
                        }
                        $idFieldPreview = "preview-" . $idField;
                        if($item['type'] == 'input_mult'){
                            $idFieldPreview = "preview-" . $lowerModelName.'-'. split(',', $key)[0];
                            $idFieldPreview_1 = "preview-" . $lowerModelName_1.'-'. split(',', $key)[0];
                        }
                        $type = "";
                        if ($item['type'] == 'image' || $item['type'] == 'input'|| $item['type'] == 'input_mult') {
                            ?>
                            <div class="<?= $idFieldPreview ?>"></div>
                            <?php
                        } else {
                            ?>
                            <span id="<?= $idFieldPreview ?>"></span>
                            <?php
                        }

                        if ($item['type'] == 'input_coupon') {
                            ?>
                            <div class="<?= $idFieldPreview ?>"></div>
                            <?php
                        } else {
                            ?>
                            <span id="<?= $idFieldPreview ?>"></span>
                            <?php
                        }

                        $field['id'] = $idField;
                        $field['id_1'] = $idField_1;
                        $field['idPreview'] = $idFieldPreview;
                        $field['type'] = $item['type'];
                        $field['syb'] = !empty($item['syb'])?$item['syb']:'';
                        $field['key']   = $key;
    //                    $field['syb'] = '';
                        if (isset($item['groupWith'])) {
                            $field['groupWith'] = $lowerModelName . '-' . $item['groupWith'];
                        }
                        if (isset($item['groupWithSeat'])) {
                            $field['groupWithSeat'] = $lowerModelName . '-' . $item['groupWithSeat'];
                        }

                        if (isset($item['show_coupon_check'])) {

                            $field['show_coupon_check'] = true;
                        }

                        $fields[] = $field;
                        ?>

                    </div>
                <?php } ?>
            <?php } ?>
        </div>
        <?php
    }
    ?>

</div>

<script>

    function previewDialog(notDelay){
        //fix for IE
        if(!notDelay){
            notDelay = true;
        }
        if (notDelay == false) {
            //if notDelay == false delay ultil have image
            var checkExist = setInterval(function () {
                //check until exit 
                if ($('.file-initial-thumbs').find('img').length > 0) {
                    previewDialog();
                    clearInterval(checkExist);
                }
            }, 100);
        }
        //hot fix for coupon

<?php
foreach ($fields as $item) {
    ?>
    <?php 
    if ($item['type'] == 'input') {
        isset($item['groupWith']) ? $group = " ' ' + $('#{$item['groupWith']}').val()" : $group = " ''";
        isset($item['groupWithSeat']) ? $groupSeat = " ' ' + $('#{$item['groupWithSeat']}').val()" : $groupSeat = " ''";
       
        ?>
                var $value = $('#<?= $item['id'] ?>').val()?$('#<?= $item['id'] ?>').val():$('#<?= $item['id_1'] ?>').val();
                if($value){
                    $('#<?= $item['idPreview'] ?>').text($value+ "<?= $item['syb']?>" + <?= $group ?>);
                }else{
                    $('#<?= $item['idPreview'] ?>').text("");
                }

                if (<?= $groupSeat ?> !== "") {
                    $('#<?= $item['idPreview'] ?>').text($value + "<?= $item['syb']?>" + "     ～     " + <?= $groupSeat ?>);
                }

    <?php 
    
    } else if ($item['type'] == 'text_area') {
        isset($item['groupWith']) ? $group = " ' ' + $('#{$item['groupWith']}').val()" : $group = " ''";
        isset($item['groupWithSeat']) ? $groupSeat = " ' ' + $('#{$item['groupWithSeat']}').val()" : $groupSeat = " ''";
       
        ?>
                var $value = $('#<?= $item['id'] ?>').val()?$('#<?= $item['id'] ?>').val():$('#<?= $item['id_1'] ?>').val();
                $('#<?= $item['idPreview'] ?>').html('<div style="white-space: pre;">'+$value + <?= $group ?>+'</div>');

        <?php
    } else if ($item['type'] == 'hidden') {
        isset($item['groupWith']) ? $group = " ' ' + $('#{$item['groupWith']}').val()" : $group = " ''";
        isset($item['groupWithSeat']) ? $groupSeat = " ' ' + $('#{$item['groupWithSeat']}').val()" : $groupSeat = " ''";
        ?>
                $('#<?= $item['idPreview'] ?>').html($('#<?= $item['id'] ?>').val() + <?= $group ?>);

                if (<?= $groupSeat ?> !== "") {
                    $('#<?= $item['idPreview'] ?>').html($('#<?= $item['id'] ?>').val() + "     ～     " + <?= $groupSeat ?>);
                }
                
               
        <?php
    } else if ($item['type'] == 'input_mult') {
        isset($item['groupWith']) ? $group = " ' ' + $('#{$item['groupWith']}').val()" : $group = " ''";
        isset($item['groupWithSeat']) ? $groupSeat = " ' ' + $('#{$item['groupWithSeat']}').val()" : $groupSeat = " ''";
        ?>
                var $key = <?= '"'.$item['key'].'"' ?>;
                var $arr = $key.split(",");
                var $value = '';
                for($i = 0; $i<$arr.length;$i++){
                    var $id_name = <?= '"#'.$lowerModelName.'-"' ?>+$arr[$i];
                    var $id_name_1 = <?= '"#'.$lowerModelName.'-"' ?>+$arr[$i];
                    $value += $($id_name).val()?$($id_name).val():$($id_name).val();
                }
                if ($value) {
                    $('#<?= $item['idPreview'] ?>').text($value);
                } else {
                    $('#<?= $item['idPreview'] ?>').text("");
                }
                var $black_list_flg = $('#customerstore-black_list_flg');
                var $to_home_delivery_flg = $('#booking-to_home_delivery_flg');
                if($black_list_flg.is(':checked')){
                    $('#black_list_flg').show();
                }else{
                    $('#black_list_flg').hide();
                }
                if($to_home_delivery_flg.is(':checked')){
                    $('#to_home_delivery_flg').show();
                }else{
                    $('#to_home_delivery_flg').hide();
                }
        <?php
    }else if ($item['type'] == 'label') {
        isset($item['groupWith']) ? $group = " ' ' + $('#{$item['groupWith']}').val()" : $group = " ''";
        isset($item['groupWithSeat']) ? $groupSeat = " ' ' + $('#{$item['groupWithSeat']}').val()" : $groupSeat = " ''";
        ?>
                var $value = $('#preview-<?= $item['key'] ?>').html();
                if ($value) {
                    $('#<?= $item['idPreview'] ?>').html('<div style="white-space: pre;">'+$value.trim()+'</div>');
                } else {
                    $('#<?= $item['idPreview'] ?>').text("");
                }
        <?php
    }else if ($item['type'] == 'select') {
        ?>
                var $value = $('#<?= $item['id'] ?>  option:selected').text()?$('#<?= $item['id'] ?>  option:selected').text():$('#<?= $item['id_1'] ?>  option:selected').text();
                if ($value) {
                    $('#<?= $item['idPreview'] ?>').text($value);
                } else {
                    $('#<?= $item['idPreview'] ?>').text("");
                }
        <?php
    }else if ($item['type'] == 'radio') {
        ?>
                $('#<?= $item['idPreview'] ?>').text($('#<?= $item['id'] ?> input:checked').parent().text());


        <?php
    } else if ($item['type'] == 'checkbox') {
        if (isset($item['show_coupon_check']) && $item['show_coupon_check'] == true) {
            ?>
                    //================================
                    // Check Show Coupon Datpdt start
                    //================================
                    var value_show_coupon = $('input[name="MasterCoupon[show_coupon][]"]:checked').map(function () {

                        return $(this).val();

                        return $(this).val();

                    });
                    if (value_show_coupon.length >= 2) {
                        //alert(value_show_coupon.length);
                        var index, len, value = null;

                        for (index = 0, len = value_show_coupon.length; index < len; ++index) {
                            if (value_show_coupon[index] == '00') {
                                value = "予約サイト・会員アプリ";
                            } else {
                                value += "   ,   レシート出力";
                            }
                        }
                        $('#<?= $item['idPreview'] ?>').text(value);
                    } else {
                        if (value_show_coupon[0] == '00') {
                            $('#<?= $item['idPreview'] ?>').text("予約サイト・会員アプリ");
                        } else {
                            $('#<?= $item['idPreview'] ?>').text("レシート出力");
                        }
                    }


        <?php } else { ?>
                    if ($('#<?= $item['id'] ?>').prop('checked')) {
                        $('#<?= $item['idPreview'] ?>').text("あり");
                    } else {
                        $('#<?= $item['idPreview'] ?>').text("なし");
                    }
                    //================================
                    // Check Show Coupon Datpdt end
                    //================================
        <?php } ?>
        <?php
    } 
}
?>


    }

    $(function () {
        $(document).on('click', '#<?= $idBtnConfirm ?>', function () {
            if ($(".kv-error-close").length > 0) {
                return false;
            }
            previewDialog();
            return  true;
        });
        //set defaulse validate prevent submit if not click btnSubmit
        var validated = 0;
        $(document).on('click', '#<?= $btnSubmit ?>', function () {
            validated = 1;
        });
        //can submit if validated != 0 
        $('#<?= $formId ?>').on('beforeSubmit', function (event) {
            if (validated == 0)
                return false;
        });
         $('#<?= $formId ?>').on('beforeValidate', function (event, attribute, messages) {
           
            point_use = $("#booking-point_use").val();
            pathControler = 'get-preview-sum-price';
            $.ajax({
            url: pathControler,
            type: 'get',
            data: {
              point_use:point_use
            },
            success: function (data) {
//              console.log(data);
               $(".preview-mastercustomer-payment").text(data + '円');
                //
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(errorThrown);
            }
            });
        });
        //show modal if all validate
        $('#<?= $formId ?>').on('afterValidate', function (event, attribute, messages) {

            if ($(this).find('.has-error').length == 0) {
                $('.field-booking-productselect').hide();
                $('.field-booking-staffselect').hide();
                
                $('#<?= $modalId ?>').modal("show");

            }
        });
    });
</script>
