<?php

use yii\bootstrap\Html;
use yii\bootstrap\ActiveForm;
use yii\jui\DatePicker;
use common\components\Constants;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
 <?php

 echo Html::beginTag(Constants::DIV, ['class' => $searchName . '-search']);
 $form = ActiveForm::begin([
             'method' => 'get',
             'enableClientValidation' => FALSE,
             'action' => [$formAction],
 ]);

 foreach ($data as $key => $output) {
     $showCondition = isset($output['show-condition']) ? $output['show-condition'] : true;
     $doubleInput = isset($output['double-input']) ? true : false;
     $groupInput = isset($output['group-input']) ? $output['group-input'] : false;
     if ($showCondition) {
         echo Html::beginTag(Constants::DIV, Constants::OPTION_DIV_CLASS_12);
         echo Html::tag(Constants::DIV, $output['label'], Constants::OPTION_DIV_SEARCH_LABEL);
         if ($doubleInput) {
             switch ($output['type']) {
                 case 'vist-number':
                     //echo $form->field($model, $key)->dropDownList($output['drop-data'], isset($output['prompt']) ? ['prompt' => $output['prompt']] : [])->label(false);
                     break;
                 default :  // date-to-date
                     echo Html::beginTag(Constants::DIV, Constants::OPTION_DIV_SEARCH_CONTENT);
                     echo $form->field($model, $output['double-input']['input-1'], [
                     ])->widget(DatePicker::classname(), [
                         'language' => 'ja',
                         'dateFormat' => 'yyyy/MM/dd',
                         'clientOptions' => [
                             "changeMonth" => true,
                             "changeYear" => true,
                             "yearRange" => "1900:+0"
                         ],
                     ])->textInput(['maxlength' => 10])->label(false);
                     echo Html::endTag(Constants::DIV);
                     echo Html::tag(Constants::DIV, '～', Constants::OPTION_DIV_MIDDLE_LABEL);
                     echo Html::beginTag(Constants::DIV, Constants::OPTION_DIV_SEARCH_CONTENT);
                     echo $form->field($model, $output['double-input']['input-2'], [
                     ])->widget(DatePicker::classname(), [
                         'language' => 'ja',
                         'dateFormat' => 'yyyy/MM/dd',
                         'clientOptions' => [
                             "changeMonth" => true,
                             "changeYear" => true,
                             "yearRange" => "1900:+0"
                         ],
                     ])->textInput(['maxlength' => 10])->label(false);
                     echo Html::endTag(Constants::DIV);
                     break;
             }
         } else if ($groupInput) {
             
         } else {
             echo Html::beginTag(Constants::DIV, Constants::OPTION_DIV_SEARCH_CONTENT);
             switch ($output['type']) {
                 case 'dropdownlist':
                     echo $form->field($model, $key)->dropDownList($output['drop-data'], isset($output['prompt']) ? ['prompt' => $output['prompt']] : [])->label(false);
                     break;
                 case 'date':
                     //echo $form->field($model, $key)->dropDownList($output['drop-data'], isset($output['prompt']) ? ['prompt' => $output['prompt']] : [])->label(false);
                     break;
                 default :
                     echo $form->field($model, $key)->textInput(['maxlength' => true])->label(false);
                     break;
             }
             echo Html::endTag(Constants::DIV);
         }

         echo Html::endTag(Constants::DIV);
     }
 }

 echo Html::beginTag(Constants::DIV, ['class' => 'form-group']);
 echo Html::a(Yii::t('backend', 'Return'), [isset($btnBack) ? $btnBack['action'] : '/'], ['class' => 'btn common-button-default btn-default']);
 //if(isset($btnSubmit))
 echo Html::submitButton(Yii::t('backend', 'Search'), ['class' => 'btn common-button-submit']);
 echo Html::endTag(Constants::DIV);
 ActiveForm::end();
 echo Html::endTag(Constants::DIV);
 ?>
