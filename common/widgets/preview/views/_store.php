<?php

use yii\bootstrap\Html;
use common\components\Constants;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
 <?php
     $labelOption = isset($output['label-option']) ? $output['label-option'] : Constants::OPTION_DIV_SEARCH_LABEL;
     $contentOption = isset($output['content-option']) ? $output['content-option'] : Constants::OPTION_DIV_SEARCH_CONTENT;
     $showCondition = isset($output['show-condition']) ? $output['show-condition'] : true;
     $doubleInput = isset($output['double-input']) ? true : false;
     $groupInput = isset($output['group-input']) ? $output['group-input'] : false;
     if ($showCondition) {
         echo Html::beginTag(Constants::DIV, Constants::OPTION_DIV_CLASS_12);
         echo Html::tag(Constants::DIV, $output['label'], $labelOption);
             echo Html::beginTag(Constants::DIV, $contentOption);
                echo $form->field($model, $output['id'])->dropDownList($output['drop-data'], isset($output['prompt']) ? ['prompt' => $output['prompt']] : [])->label(false);
             echo Html::endTag(Constants::DIV);
         echo Html::endTag(Constants::DIV);
     }
 ?>
<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

