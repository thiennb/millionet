<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$lowerModelName = strtolower($modelName);
$idPreview = "preview2-" . $lowerModelName;
$fields = array();
?>
<div class="form-horizontal">
    <div class="form-group">
        <div class="col-sm-8 store_gift_preview">
            <?php
            foreach ($data as $key => $item) {
                ?>
                <?php if (!empty($item['label'])) { ?>

                    <?php
                    $field = array();
                    $idField = $lowerModelName . '-' . $key;
                    if (isset($item['model'])) {
                        //orther model
                        $idField = strtolower($item['model']) . '-' . $key;
                    }
                    $idFieldPreview = "preview2-" . $idField;
                    $type = "";

                    if ($item['type'] == 'date_time') {
                        ?>
                        <div class="<?= $idFieldPreview ?>" style="font-size: 1.3em;margin-bottom: 0.3em;"></div>
                        <?php
                    }
                    if ($item['type'] == 'image') {
                        ?>
                        <div class="<?= $idFieldPreview ?>"></div>
                        <?php
                    }
                    if ($item['type'] == 'input_description1') {
                        ?>
                        <div class="<?= $idFieldPreview ?>"  style="text-align: left;font-size: 1.3em;margin-top: 1em;"></div>
                        <?php
                    }
                    if ($item['type'] == 'input_description2') {
                        ?>
                        <div class="<?= $idFieldPreview ?>"  style="text-align: left;font-size: 1.3em;margin-top: 1em;"></div>
                        <?php
                    }
                    if ($item['type'] == 'radio_expiration_date_flg') {
                        ?>
                        <div class="<?= $idFieldPreview ?>"  style="font-weight: bold;text-align: left;font-size: 1.3em;margin-top: 1em;"></div>
                        <?php
                    }
                    if ($item['type'] == 'radio_member_no') {
                        ?>
                        <div class="<?= $idFieldPreview ?>"  style="font-weight: bold;text-align: left;font-size: 1.3em;margin-top: 1em;"></div>    
                        <?php
                    }
                    $field['id'] = $idField;
                    $field['idPreview'] = $idFieldPreview;
                    $field['type'] = $item['type'];
                    $fields[] = $field;
                    ?>


                <?php } ?>


                <?php
            }
            ?>
        </div>
    </div>
</div>

<script>

    function previewDialog2(notDelay){
        //fix for IE
        if(!notDelay){
            notDelay = true;
        }
        if (notDelay == false) {
            //if notDelay == false delay ultil have image
            var checkExist = setInterval(function () {
                //check until exit 
                if ($('.file-initial-thumbs').find('img').length > 0) {
                    previewDialog2();
                    clearInterval(checkExist);
                }
            }, 100);
        }
        //hot fix for coupon

<?php
foreach ($fields as $item) {
    if ($item['type'] == 'image') {
        ?>
                var $src_img = $('.<?= 'field-' . $item['id'] ?>').find('img').clone();
                $src_img.attr('width', '350px').attr('height', '350px');
                if ($src_img == '' || $src_img == null || $src_img.attr('src') == '/api/uploads/no-image.jpg') {
                    $('.<?= $item['idPreview'] ?>').html('');
                } else {
                    $('.<?= $item['idPreview'] ?>').html('').append($src_img);
                }

        <?php
    }
    if ($item['type'] == 'input_description1') {
        ?>
                var description1 = $('#masterstore-gold_ticket_description1').val();
                $('.<?= $item['idPreview'] ?>').html('').append(description1);
        <?php
    }
    if ($item['type'] == 'input_description2') {
        ?>
                var description2 = $('#masterstore-gold_ticket_description2').val();
                $('.<?= $item['idPreview'] ?>').html('').append(description2);
        <?php
    }
    if ($item['type'] == 'date_time') {
        ?>
                var datetime = getDateTime(0);
                $('.<?= $item['idPreview'] ?>').html('').append(datetime);
        <?php
    }
    if ($item['type'] == 'radio_expiration_date_flg') {
        ?>
                var check_radio = $("input[name='MasterStore[expiration_date_flg]']:checked").val();
                if (check_radio == 1) {
                    if ($('#masterstore-supply_year').val() != 0) {
                        var datetime = getDateTime($('#masterstore-supply_year').val());
                        $('.<?= $item['idPreview'] ?>').html('').append('有効期限 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp' + datetime);
                    } else {
                        $('.<?= $item['idPreview'] ?>').html('').append('有効期限 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;無期限');
                    }

                } else {
                    $('.<?= $item['idPreview'] ?>').html('').append('有効期限 無期限');
                }
        <?php
    }
    if ($item['type'] == 'radio_member_no') {
        ?>
          $('.<?= $item['idPreview'] ?>').html('').append("会員 No &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;***********0001");
        <?php
    }
}
?>
    }

    $(function () {
        $(document).on('click', '#<?= $idBtnConfirm ?>', function () {
            if ($(".kv-error-close").length > 0) {
                return false;
            }
            previewDialog2();
            return  true;
        });
        //set defaulse validate prevent submit if not click btnSubmit
        var validated = 0;
        $(document).on('click', '#<?= $btnSubmit ?>', function () {
            validated = 1;
        });
        //can submit if validated != 0 
        $('#<?= $formId ?>').on('beforeSubmit', function (event) {
            if (validated == 0)
                return false;
        });
        //show modal if all validate
        $('#<?= $formId ?>').on('afterValidate', function (event, attribute, messages) {

            if ($(this).find('.has-error').length == 0) {

                $('#<?= $modalId ?>').modal("show");

            }
        });
    });
    function getDateTime(year) {
        var currentdate = new Date();
        if (year == 0) {
            var datetime = currentdate.getFullYear() + " 年 "
                    + (currentdate.getMonth() + 1) + " 月 "
                    + currentdate.getDate() + " 日（金）"
                    + currentdate.getHours() + " 時 "
                    + currentdate.getMinutes() + " 分 ";
            return datetime;
        } else {
            var datetime = (currentdate.getFullYear() + parseInt(year)) + " 年 "
                    + (currentdate.getMonth() + 1) + " 月 "
                    + currentdate.getDate() + " 日 まで ";
            return datetime;
        }
    }

</script>
