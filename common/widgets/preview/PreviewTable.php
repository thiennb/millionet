<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace common\widgets\preview;
use yii\bootstrap\Widget;
/**
 * Description of Preview
 *
 * @author thanhlbk
 */
class PreviewTable extends Widget{
    public $data;
    public $modelName;
    public $idBtnConfirm;
    public $formId;
    public $btnClose;
    public $modalId;
    public $btnSubmit;
    
    public function init() {
        parent::init();
        ob_start();
    }
    
    public function run() {
        $content = ob_get_clean();
        $formId  = isset($this->formId) ? $this->formId : 'w0';
        return $this->render('index_table', [
                'data' => $this->data, 
                'modelName' => $this->modelName, 
                'idBtnConfirm' => $this->idBtnConfirm,
                'formId' => $formId,
                'btnClose' => $this->btnClose,
                'btnSubmit' => $this->btnSubmit,
                'modalId' => $this->modalId
            ]);
    }
}
