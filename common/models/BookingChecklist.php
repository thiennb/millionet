<?php

namespace common\models;

use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use yii2tech\ar\softdelete\SoftDeleteBehavior;
use Yii;

/**
 * This is the model class for table "booking_checklist".
 *
 * @property integer $id
 * @property integer $booking_id
 * @property integer $checklist_id
 * @property string $answer_list
 * @property string $del_flg
 * @property integer $created_at
 * @property integer $created_by
 * @property integer $updated_at
 * @property integer $updated_by
 */
class BookingChecklist extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'booking_checklist';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            //[['id', 'booking_id', 'checklist_id'], 'required'],
            [['booking_id', 'checklist_id'], 'required'],
            [['booking_id', 'checklist_id', 'created_at', 'created_by', 'updated_at', 'updated_by'], 'integer'],
            [['answer_list'], 'string', 'max' => 100],
            [['del_flg'], 'string', 'max' => 1],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend', 'ID'),
            'booking_id' => Yii::t('backend', 'Booking ID'),
            'checklist_id' => Yii::t('backend', 'Checklist ID'),
            'answer_list' => Yii::t('backend', 'Answer List'),
            'del_flg' => Yii::t('backend', 'Del Flg'),
            'created_at' => Yii::t('backend', 'Created At'),
            'created_by' => Yii::t('backend', 'Created By'),
            'updated_at' => Yii::t('backend', 'Updated At'),
            'updated_by' => Yii::t('backend', 'Updated By'),
        ];
    }
    
    public function behaviors() {
      return [
        TimestampBehavior::className(),
        BlameableBehavior::className(),
        'softDeleteBehavior' => [
          'class' => SoftDeleteBehavior::className(),
          'softDeleteAttributeValues' => [
            'del_flg' => true
          ],
        ]
      ];
    }
      public static function find($findAll = true) {
        if ($findAll)
            return parent::find()->where(['booking_checklist.del_flg' => '0']);
        else
            return parent::find();
    }
    

}
