<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use common\components\RegisterFromBehavior;
use yii2tech\ar\softdelete\SoftDeleteBehavior;
use Carbon\Carbon;
use yii\helpers\Html;
use common\components\Util;
use yii\db\ActiveRecord;
use yii\behaviors\AttributeBehavior;

/**
 * This is the model class for table "booking_product".
 *
 * @property integer $id
 * @property integer $booking_id
 * @property integer $product_id
 * @property integer $option_id
 * @property integer $quantity
 * @property string $unit_price
 * @property string $del_flg
 * @property integer $created_at
 * @property integer $created_by
 * @property integer $updated_at
 * @property integer $updated_by
 */
class BookingProduct extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public $store_name;
    public $booking_date;
    public $start_time;
    public $product_name;
    public $title;
    public $option_name;
    public $booking_total;
    public $point_conversion;
    public $product_booking_id;
    public $status;
    public static function tableName()
    {
        return 'booking_product';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['booking_id'], 'required'],
            [['booking_id', 'product_id', 'option_id', 'quantity', 'created_at', 'created_by', 'updated_at', 'updated_by'], 'integer'],
            [['unit_price'], 'number'],
            [['del_flg'], 'string', 'max' => 1],
        ];
    }

	public function behaviors() {
      return [
        TimestampBehavior::className(),
        BlameableBehavior::className(),
        RegisterFromBehavior::className(), 
        'softDeleteBehavior' => [
          'class' => SoftDeleteBehavior::className(),
          'softDeleteAttributeValues' => [
            'del_flg' => true
          ],
        ]
      ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'booking_id' => Yii::t('app', 'Booking ID'),
            'product_id' => Yii::t('app', 'Product ID'),
            'option_id' => Yii::t('app', 'Option ID'),
            'quantity' => Yii::t('app', 'Quantity'),
            'unit_price' => Yii::t('app', 'Unit Price'),
            'del_flg' => Yii::t('app', 'Del Flg'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'updated_by' => Yii::t('app', 'Updated By'),
        ];
    }
    public function getProduct()
    {
      return $this->hasOne(MstProduct::className(), ['id' => 'product_id']);
    }
    
    public function getOption()
    {
      return $this->hasOne(MasterOption::className(), ['id' => 'option_id']);
    }

    public static function find() {
        return parent::find()->where([BookingProduct::tableName() . '.del_flg' => '0']);
    }
}
