<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "system_message".
 *
 * @property integer $id
 * @property string $title
 * @property string $message
 * @property string $public_date
 * @property string $public_time
 * @property string $status
 * @property string $del_flg
 * @property integer $created_at
 * @property integer $created_by
 * @property integer $updated_at
 * @property integer $updated_by
 */
class MstSystemMessage extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'system_message';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'public_date', 'created_at', 'created_by', 'updated_at', 'updated_by'], 'required'],
            [['public_date'], 'safe'],
            [['created_at', 'created_by', 'updated_at', 'updated_by'], 'integer'],
            [['title'], 'string', 'max' => 100],
            [['message'], 'string', 'max' => 500],
            [['public_time'], 'string', 'max' => 5],
            [['status', 'del_flg'], 'string', 'max' => 1],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'message' => 'Message',
            'public_date' => 'Public Date',
            'public_time' => 'Public Time',
            'status' => 'Status',
            'del_flg' => 'Del Flg',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
        ];
    }
    
    public static function find() {
        return parent::find()->where(['or', ['del_flg' => '0'], ['del_flg' => null]]);
    }
}
