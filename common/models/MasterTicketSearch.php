<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\MasterTicket;
use yii\db\Query;
use common\components\Util;
//use  yii\db\Connection::createCommand();
/**
 * MasterTicketSearch represents the model behind the search form about `common\models\MasterTicket`.
 */
class MasterTicketSearch extends MasterTicket {

    /**
     * @inheritdoc
     */
    public $price_from;
    public $price_to;
    public $set_ticket_min;
    public $set_ticket_max;
    public $date_expiration_from;
    public $date_expiration_to;
    public $product_name;

    public function rules() {
        return [
            [['name', 'set_ticket', 'product_name', 'date_expiration', 'price', 'del_flg', 'created_by', 'updated_by', 'price_from', 'price_to',], 'safe'],
            [['id', 'store_id', 'created_at', 'updated_at', 'set_ticket_min', 'set_ticket_max'], 'integer'],
            [['price_from', 'price_to'], 'number', 'min' => 0, 'max' => 9999999999],
            [['price_from', 'price_to'], 'string', 'max' => 13],
            [['set_ticket_min', 'set_ticket_max'], 'number', 'min' => 0, 'max' => 99999],
            [['set_ticket_min', 'set_ticket_max'], 'string', 'max' => 6],
            [['date_expiration_from', 'date_expiration_to'], 'string', 'max' => 3],
            [['name'], 'string', 'max' => 100],
            ['price_from', 'checkPrice'],
            ['price_to', 'checkPriceTo'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function checkPrice($attribute, $params) {
        if ($this->price_from > 9999999999) {
            $this->addError('price_from', Yii::t('backend', "The ticket selling price must not be larger than 9,999,999,999."));
        }
    }

    public function checkPriceTo($attribute, $params) {

        if ($this->price_to > 9999999999) {
            $this->addError('price_to', Yii::t('backend', "The ticket selling price must not be larger than 9,999,999,999."));
        }
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
         $this->load($params);
        $query = MasterTicket::find()
                ->innerJoin('mst_store', 'mst_store.id = ticket.store_id')
                // Join : Company store
                ->innerJoin('company', 'company.id = mst_store.company_id');
                if(!empty($this->date_expiration_from) || !empty($this->date_expiration_to)){
                    $query->innerJoin('ticket_history', 'ticket_history.ticket_jan_code = ticket.ticket_jan_code');
                    $query->innerJoin('mst_order', 'mst_order.order_code = ticket_history.order_code');
                }
                $query->groupBy(MasterTicket::tableName() . '.id');
        // add conditions that should always apply here
        $cookies = Yii::$app->request->cookies;
        $company_id =  Util::getCookiesCompanyId();
        $query->andFilterWhere(['=', 'company.id', $company_id]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => false,
        ]);

       

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        $query->andFilterWhere(['=', MasterTicket::tableName() . '.store_id', $this->store_id]);
        if (!empty($this->product_name)) {
            $query->joinWith('productTicket');
            $query->innerJoin('mst_product', 'product_ticket.product_id = mst_product.id');
            $query->andFilterWhere(['like', 'mst_product.name', $this->product_name]);
            $query->andFilterWhere(['or', ['like', 'mst_product.name', $this->product_name], ['like', 'mst_product.name_kana', $this->product_name]]);
        }
        $query->andFilterWhere(['>=', 'set_ticket', $this->set_ticket_min]);
        $query->andFilterWhere(['<=', 'set_ticket', $this->set_ticket_max]);
        if(!empty($this->date_expiration_from)){
            $query->andFilterWhere(['>=', 'ticket.date_expiration', $this->date_expiration_from]);
        }
        if(!empty($this->date_expiration_from)){
            $query->andFilterWhere(['<=', 'ticket.date_expiration', $this->date_expiration_to]);
        }
        $query->andFilterWhere(['>=', 'price', $this->price_from]);
        $query->andFilterWhere(['<=', 'price', $this->price_to]);

        return $dataProvider;
    }

    public function filter($customerId) {
        $query = (new \yii\db\Query())->select(['ticket.id',
                    'ticket.store_id',
                    'ticket.name AS name_ticket',
                    'ticket.ticket_jan_code',
                    'ticket.date_expiration',
                    'ticket.created_at',
                    'mst_store.name',
                    'ticket_history.ticket_balance',
                    'booking.customer_id'])
                ->from('ticket')
                ->where(['booking.customer_id' => $customerId])
                ->innerJoin('mst_store', 'mst_store.id = ticket.store_id')
                ->innerJoin('booking', 'booking.store_id = ticket.store_id')
                ->leftJoin('ticket_history', 'ticket_history.ticket_jan_code = ticket.ticket_jan_code')
                ->orderBy(['ticket.created_at' => SORT_DESC])
                ->distinct();

        return $query;
    }

    public function searchHistory($params, $customer_jan_code = null) {
        $search_query = (new Query())
                ->select([
                    'ticket_history.ticket_jan_code',
                    'max(ticket.id) as id_ticket',
                    'max(ticket_history.created_at) as created_at_d',
                    'max(ticket.name) AS ticket_name',
                    'max(mst_store.id) as store_id_d',
                    'max(mst_store.name) as store_name',
                ])
                ->from(["ticket_history" => TicketHistory::tableName()])
                ->innerJoin(['ticket' => MasterTicket::tableName()], "ticket.ticket_jan_code = ticket_history.ticket_jan_code and ticket.store_id = ticket_history.store_id "
                        . "and ticket.del_flg = :zero")
                ->innerJoin(['mst_store' => MasterStore::tableName()], "mst_store.id = ticket_history.store_id "
                . "and mst_store.del_flg = :zero");

        if (!empty($params['store_name']) || !empty($params['start_date']) || !empty($params['end_date'])) {
            if ($params['store_name']) {
                $search_query->andFilterWhere(['=', TicketHistory::tableName() . '.store_id', $params['store_name']]);
            }
            if ($params['start_date']) {
                $search_query->andFilterWhere(['>=', MstOrder::tableName() . '.process_date', $params['start_date']]);
            }
            if ($params['end_date']) {
                $search_query->andFilterWhere(['<=', MstOrder::tableName() . '.process_date', $params['end_date']]);
            }
        }    
        if (isset($customer_jan_code)) {
            $search_query->innerJoin(MstOrder::tableName(), MstOrder::tableName() . '.order_code = ' . TicketHistory::tableName() . '.order_code');
            $search_query->andFilterWhere([MstOrder::tableName() . '.customer_jan_code' => $customer_jan_code]);
        }                        
        $search_query->groupBy(["ticket_history.ticket_jan_code"]);
        $list_query = (new Query())
                ->select([
                    'ticket_history.ticket_jan_code',
                    'created_at_d',
                    'ticket_history.ticket_balance',
                    'ticket_history.order_code',
                    'store_name',
                    'ticket_name',
                    'store_id_d',
                    'ticket_history.id AS id',
                ])
                ->from(["search" => $search_query])
                ->innerJoin(['ticket_history' => TicketHistory::tableName()], "ticket_history.created_at = search.created_at_d "
                . "and ticket_history.ticket_jan_code = search.ticket_jan_code");  
     
        
        $list_query->addParams([
            'zero' => '0'
        ]);
        $dataProvider = new ActiveDataProvider([
            'query' => $list_query,
            'pagination' => [
                'pageSizeLimit' => [1, 5],
            ],
            'sort' => false
        ]);

        return $dataProvider;
    }

    public function ticket_detail($code = null,$customer_jan_code = null) {

        $query = TicketHistory::find();
        $query->select([
            'ticket_history.process_number',
            'mst_order.process_date',
            'ticket_history.process_type',
        ]);

        $query->innerJoin(MstOrder::tableName(), MstOrder::tableName() . '.order_code = ' . TicketHistory::tableName() . '.order_code');
        $query->andFilterWhere([TicketHistory::tableName() . '.ticket_jan_code' => $code]);
        $query->andFilterWhere([MstOrder::tableName() . '.customer_jan_code' => $customer_jan_code]);
        $query->andFilterWhere([TicketHistory::tableName() . '.del_flg' => 0]);
        $data = $query->asArray()->all();
        return $data;
    }

    public function getinfoStore($store_id, $code) {
        $query = TicketHistory::find();
        $query->select([
            'mst_store.name AS store_name',
            'ticket.name AS ticket_name',
            'ticket_history.ticket_balance',
        ])->distinct();
        $query->innerJoin('mst_store', 'mst_store.store_code = left(ticket_history.order_code,5)');
        $query->innerJoin('ticket', 'ticket.ticket_jan_code = ticket_history.ticket_jan_code');
        $query->andFilterWhere([MasterStore::tableName() . '.id' => $store_id]);
        $query->andFilterWhere([TicketHistory::tableName() . '.ticket_jan_code' => $code]);
        return $query;
    }

    public function getStoreName($store_id = null) {
        $query = MasterStore::find()->select(['name']);
        $query->andFilterWhere([MasterStore::tableName() . '.id' => $store_id]);
        $data = $query->asArray()->one();
        return $data;
    }

    public function getTicketInfo($code = null, $id = null, $store_id = null ) {
        $query = TicketHistory::find();
        $query->select([
            'ticket_history.ticket_jan_code',
            'ticket.name AS ticket_name',
            'ticket_balance AS ticket_balance',
        ]);
        $query->innerJoin(MasterTicket::tableName(), MasterTicket::tableName() . '.ticket_jan_code = ' . TicketHistory::tableName() . '.ticket_jan_code');
        $query->andFilterWhere([TicketHistory::tableName() . '.ticket_jan_code' => $code]);
        $query->andFilterWhere([TicketHistory::tableName() . '.id' => $id]);
        $query->andFilterWhere([MasterTicket::tableName() . '.store_id' => $store_id]);
        $data = $query->asArray()->one();
        return $data;
    }

    public static function getTicketBalance($id) {
        $query = TicketHistory::find()->select(['ticket_balance']);
        $query->andFilterWhere([TicketHistory::tableName() . '.id' => $id]);
        $data = $query->asArray()->one();
        return $data;
    }

}
