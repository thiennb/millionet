<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii2tech\ar\softdelete\SoftDeleteBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the model class for table "mst_gift_receiver".
 *
 * @property integer $id
 * @property integer $gift_id
 * @property integer $product_id
 * @property integer $quantity
 * @property string $people_receiver_name
 * @property string $people_receiver_tel
 * @property string $people_receiver_postcode
 * @property string $people_receiver_address
 * @property string $del_flg
 * @property integer $created_by
 * @property integer $updated_by	
 * @property integer $created_at
 * @property integer $updated_at
 */
class MasterGiftReceiver extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'gift_receiver';
    }

    public $customer_id_new;

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            TimestampBehavior::className(),
            BlameableBehavior::className(),
            'softDeleteBehavior' => [
                'class' => SoftDeleteBehavior::className(),
                'softDeleteAttributeValues' => [
                    'del_flg' => true
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [

            [['customer_id'], 'safe'],
            [['gift_id', 'quantity', 'customer_id', 'customer_id_new', 'created_at', 'created_by', 'updated_at', 'updated_by'], 'integer'],
            [['del_flg'], 'string', 'max' => 1],
            [['quantity'], 'integer', 'min' => 1],
            ['quantity', 'maxQuantity'],
            [['gift_id', 'product_id', 'quantity'], 'required'],
        ];
    }

    public function maxQuantity() {
        if (!empty($this->product_id) && !empty($this->gift_id)) {
            if (!empty($this->customer_id)) {
                $customer_id = !empty($this->customer_id_new) ? $this->customer_id_new : $this->customer_id;
                if ($this->quantity > $this->getQuantityResidual($this->gift_id, $this->product_id, $customer_id)) {
                    $this->addError('quantity', \Yii::t('backend', 'Input value must not be greater than left value.'));
                }
            } else {
                if ($this->quantity > $this->getQuantityResidual($this->gift_id, $this->product_id)) {
                    $this->addError('quantity', \Yii::t('backend', 'Input value must not be greater than left value.'));
                }
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => Yii::t('app', 'ID'),
            'gift_id' => Yii::t('backend', 'Gift ID'),
            'product_id' => Yii::t('backend', 'Product'),
            'quantity' => Yii::t('backend', 'Quantity'),
            'customer_id' => Yii::t('app', 'Customer ID'),
            'del_flg' => Yii::t('app', 'Del Flg'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'updated_by' => Yii::t('app', 'Updated By'),
        ];
    }

    public static function getQuantityResidual($gift_id, $product_jan_code, $customer_id = null) {
        if ($customer_id !== null) {
            $sum = MasterGiftReceiver::find()->where(["gift_id" => $gift_id, "product_id" => $product_id, ["<>", "customer_id", $customer_id]])->sum('quantity');
        } else {
            $sum = MasterGiftReceiver::find()->where(["gift_id" => $gift_id, "product_id" => $product_id])->sum('quantity');
        }
        $detail = MasterGiftDetails::find()->andWhere(["AND", ["gift_id" => $gift_id], ["product_id" => $product_id]])->one();
        
        $total = $detail->quantity;
        return $total - (empty($sum) ? 0 : $sum);
    }

    public function getTotalQuantityResidual($gift_id) {
        $sum = MasterGiftReceiver::find()->andWhere(["gift_id" => $gift_id])->sum('quantity');
        $detail = MasterGiftDetails::find()->andWhere(["gift_id" => $gift_id])->sum('quantity');
        $total = isset($detail) ? $detail : 0;
        return $total - (empty($sum) ? 0 : $sum);
    }

    public static function find() {
        return parent::find()->where(['or', [MasterGiftReceiver::tableName() . '.del_flg' => '0'], [MasterGiftReceiver::tableName() . '.del_flg' => null]]);
    }
    
    /**
     * Get customer relation
     * @return hasOne
     */
    public function getCustomer(){
        return $this->hasOne(MasterCustomer::className(), ['id' => 'customer_id']);
    }
    
    /**
     * Get customer relation
     * @return hasOne
     */
    public function getProduct(){
        return $this->hasOne(MstProduct::className(), ['id' => 'product_id']);
    }
}
