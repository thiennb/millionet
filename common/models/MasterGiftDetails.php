<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii2tech\ar\softdelete\SoftDeleteBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the model class for table "gift_detail".
 *
 * @property integer $id
 * @property integer $gift_id
 * @property integer $product_id
 * @property integer $quantity
 * @property string $del_flg
 * @property integer $created_at
 * @property integer $created_by
 * @property integer $updated_at
 * @property integer $updated_by
 */
class MasterGiftDetails extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'gift_detail';
    }

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            TimestampBehavior::className(),
            BlameableBehavior::className(),
            'softDeleteBehavior' => [
                'class' => SoftDeleteBehavior::className(),
                'softDeleteAttributeValues' => [
                    'del_flg' => '1'
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['gift_id'], 'required'],
            [['gift_id', 'product_id', 'quantity', 'created_at', 'created_by', 'updated_at', 'updated_by'], 'integer'],
            [['del_flg'], 'string', 'max' => 1],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'gift_id' => 'Gift ID',
            'product_id' => 'Product ID',
            'quantity' => 'Quantity',
            'del_flg' => 'Del Flg',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
        ];
    }

    public function insertGiftDetailFromOrderDetail($list_product, $id) {
        $total_product = 0;
        foreach ($list_product as $value) {
            $order_detail = MstOrderDetail::find()->andWhere(['id' => $value])->one();
            $product_id = MstProduct::find()->andWhere(['jan_code' => $order_detail->jan_code])->one();
            $gift_detail_old =  MasterGiftDetails::find()->andWhere(['gift_id' => $id, 'product_id'=> $product_id->id])->one();
            if(empty($gift_detail_old)) {
                $newGiftDetail = new MasterGiftDetails();
                $newGiftDetail->gift_id = $id;
                $newGiftDetail->product_id = $product_id->id;
                $newGiftDetail->quantity = $order_detail->quantity;
                $newGiftDetail->save();
                $total_product +=  $newGiftDetail->quantity;
            }
        }
        return $total_product;
    }
    
    /**
     * Get customer relation
     * @return hasOne
     */
    public function getProduct(){
        return $this->hasOne(MstProduct::className(), ['id' => 'product_id']);
    }

}
