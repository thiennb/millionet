<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii2tech\ar\softdelete\SoftDeleteBehavior;
use yii\behaviors\BlameableBehavior;
use common\components\Util;
use common\components\Constants;

/**
 * This is the model class for table "pay_coupon".
 *
 * @property integer $id
 * @property integer $order_id
 * @property integer $coupon_id
 * @property string $del_flg
 * @property integer $created_at
 * @property integer $created_by
 * @property integer $updated_at
 * @property integer $updated_by
 */
class PayCoupon extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pay_coupon';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [[ 'order_code','coupon_jan_code'], 'required'],
            [[ 'coupon_jan_code', 'created_at', 'created_by', 'updated_at', 'updated_by'], 'safe'],
            [['del_flg'], 'string', 'max' => 1],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('frontend', 'ID'),
            'order_code' => Yii::t('frontend', 'Order Code'),
            'coupon_jan_code' => Yii::t('frontend', 'Coupon jancode'),
            'quantity' => Yii::t('frontend', 'Quantity'),
            'price_down_type' => Yii::t('frontend', 'Price Down Type'),
            'price_down_value' => Yii::t('frontend', 'Price Down Value'),
            'price_down_money' => Yii::t('frontend', 'Price Down Money'),
        ];
    }
    
    public function behaviors() {
        return [
            TimestampBehavior::className(),
            BlameableBehavior::className(),
            'softDeleteBehavior' => [
                'class' => SoftDeleteBehavior::className(),
                'softDeleteAttributeValues' => [
                    'del_flg' => '1'
                ],
            ],
        ];
    }
    
    public static function find()
    {
        return parent::find()->where([PayCoupon::tableName().'.del_flg' => '0']);
    }
    
    public function getCoupon () {
        return $this->hasOne(MasterCoupon::className(), ['coupon_jan_code' => 'coupon_jan_code']);
    }
}
