<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\MasterEnjoyLottery;
use common\components\Util;
use common\models\MstOrder;

/**
 * MasterEnjoyLotterySearch represents the model behind the search form about `common\models\MasterEnjoyLottery`.
 */
class MasterEnjoyLotterySearch extends MasterEnjoyLottery
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['store_id', 'number_bill_min','number_bill_max','winning_numbers'], 'integer', 'min' => 0],
            [['black_list_check'], 'string', 'max' => 1],
            [['status_winer'], 'string','max' => 2],
            [['start_winner_period_date', 'end_winner_period_date', 'start_register_period_date', 'end_register_period_date'], 'date', 'format' => 'php:Y/m/d'],
            ['end_winner_period_date', 'compare', 'compareAttribute' => 'start_winner_period_date', 'operator' => '>=', 'message' => Yii::t('backend', 'End date must be greater than Start date.'), 'skipOnEmpty' => TRUE],
            ['end_register_period_date', 'compare', 'compareAttribute' => 'start_register_period_date', 'operator' => '>=', 'message' => Yii::t('backend', 'End date must be greater than Start date.'), 'skipOnEmpty' => TRUE],
            ['number_bill_max', 'compare', 'compareAttribute' => 'number_bill_min', 'operator' => '>=', 'message' => Yii::t('backend', 'Max number must be greater than min number.'), 'skipOnEmpty' => TRUE],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MasterEnjoyLottery::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
             'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => false
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'black_list_check' => $this->black_list_check,
            'status_winer' => $this->status_winer,
        ]);
        
        if(!empty($this->store_id)){
            $query  ->andWhere(['or',['=','store_id', $this->store_id],['is', 'store_id' ,null]]);
        }

        $query  ->andFilterWhere(['>=', 'winner_announcement_date', $this->start_winner_period_date])
                ->andFilterWhere(['<=', 'winner_announcement_date', $this->end_winner_period_date])                
                ->andFilterWhere(['>=', 'register_date', $this->start_register_period_date])
                ->andFilterWhere(['<=', 'register_date', $this->end_register_period_date])             
                ->andFilterWhere(['>=', 'winning_numbers', $this->number_bill_min])
                ->andFilterWhere(['<=', 'winning_numbers', $this->number_bill_max]);

        return $dataProvider;
    }
    

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function searchCustomer(MasterEnjoyLottery $model = null)
    {
        $query = MstOrder::find()
                ->innerJoin('mst_customer','mst_order.customer_jan_code = mst_customer.customer_jan_code')
                ->innerJoin('mst_store','mst_store.store_code = SUBSTRING(mst_order.order_code,1,5)')
                ->innerJoin('customer_store','customer_store.store_id = mst_store.id and customer_store.customer_id = mst_customer.id')
                ->leftJoin('management_login','management_login.management_id = mst_order.order_management_id')
                ->leftJoin('mst_staff','mst_staff.management_login_id = management_login.id')
//                ->innerJoin('company_store', 'company_store.store_id = mst_store.id')
                ->andWhere(['mst_customer.del_flg' => '0'])
                ->andWhere(['mst_store.del_flg' => '0'])
                ->andWhere(['customer_store.del_flg' => '0'])
                ->andWhere(['management_login.del_flg' => '0'])
                ->andWhere(['mst_staff.del_flg' => '0']);
//                ->andWhere(['company_store.del_flg' => '0']);

        // add conditions that should always apply here
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => FALSE,
            'sort' => false
        ]);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
//        $company_code =  Util::getCookiesCompanyCode();
        $query->andWhere(['mst_customer.company_id' => Util::getCookiesCompanyId()]);
        if(!empty($model->winning_numbers) && (int)$model->winning_numbers > 0){
            $query->limit($model->winning_numbers);
        }else if((string)$model->winning_numbers == '0'){
            $query->andFilterWhere([
                'mst_order.id' => -1,
            ]);
        }
        if(!empty($model->rank_id)){
            $params = $model->rank_id;
            if(!is_array($params)){
                $params = explode(',', $params);
            }
            foreach ($params AS $index => $value)
                $params[$index] = (int)$value;
            $query->andFilterWhere(['IN','customer_store.rank_id', $model->rank_id]);
        }
        // grid filtering conditions
        $query->andFilterWhere([
            'mst_store.id' => $model->store_id,
            'mst_customer.sex' => $model->sex,
            'mst_customer.birth_date' => $model->birth_date,
            'customer_store.black_list_flg' => $model->black_list_check,
            'mst_customer.id' => $model->customer_id_hd,
        ]);

        $query  ->andFilterWhere(['>=', 'mst_order.process_date', $model->start_winner_period_date])
                ->andFilterWhere(['<=', 'mst_order.process_date', $model->end_winner_period_date])                
                ->andFilterWhere(['>=', 'mst_order.total', $model->earn_period_from_min])
                ->andFilterWhere(['<=', 'mst_order.total', $model->earn_period_from_max])             
                ->andFilterWhere(['>=', 'customer_store.number_visit', $model->visit_store_min])
                ->andFilterWhere(['<=', 'customer_store.number_visit', $model->visit_store_max]);
        $query  ->andFilterWhere(['or',['=','mst_staff.id',  $model->staff_id_1],
                                        ['=','mst_staff.id',  $model->staff_id_2],
                                        ['=','mst_staff.id',  $model->staff_id_3]]);
        $query->select([
            'mst_order.lotery',
            'mst_order.order_code',
            'mst_order.process_date',
            'mst_customer.first_name as customer_first_name',
            'mst_customer.last_name as customer_last_name',
            'customer_store.number_visit as customer_visit_number',
            'mst_order.total'
        ]);
        $query->groupBy([
            'mst_order.lotery',
            'mst_order.order_code',
            'mst_order.process_date',
            'mst_customer.first_name',
            'mst_customer.last_name',
            'customer_store.number_visit',
            'mst_order.total'
        ]);
        return $dataProvider;
    }
}
