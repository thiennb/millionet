<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii2tech\ar\softdelete\SoftDeleteBehavior;
use yii\behaviors\BlameableBehavior;
use yii\helpers\ArrayHelper;
use common\components\Constants;

/**
 * This is the model class for table "mst_type_seat".
 *
 * @property integer $id
 * @property string $name
 * @property string $introduce
 * @property string $image1
 * @property string $image2
 * @property string $image3
 * @property string $show
 * @property string $del_flg
 * @property string $created_by
 * @property string $updated_by
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $capacity_max
 * @property integer $capacity_min
 * @property array shifts
 * @property array detailedSchedule
 * @property array capacity
 */
class MasterTypeSeat extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public $tmp_image1;
    public $tmp_image2;
    public $tmp_image3;
    public $hidden_image1;
    public $hidden_image2;
    public $hidden_image3;
    public $schedule;
    public $shifts;
    public $smoking_flg;
    public $_capacity;

    public static function tableName() {
        return 'mst_type_seat';
    }

    public function behaviors() {
        return [
            TimestampBehavior::className(),
            BlameableBehavior::className(),
            'softDeleteBehavior' => [
                'class' => SoftDeleteBehavior::className(),
                'softDeleteAttributeValues' => [
                    'del_flg' => '1'
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['introduce'], 'string', 'max' => 500],
            [['name'], 'required'],
            [['created_at', 'updated_at', 'store_id', 'created_by', 'updated_by', 'created_at', 'updated_at'], 'integer'],
            [['name'], 'string', 'max' => 20],
            [['image1', 'image2', 'image3'], 'string', 'max' => 100],
            [['show_flg', 'del_flg'], 'string', 'max' => 1],
            [['hidden_image3', 'hidden_image2', 'hidden_image1'], 'safe'],
            [['store_id'], 'required', 'when' => function() {
            $role = \common\components\FindPermission::getRole();
            return ($role->permission_id == Constants::STORE_MANAGER_ROLE || $role->permission_id == Constants::STAFF_ROLE);
        }],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => Yii::t('backend', 'ID'),
            'store_id' => Yii::t('backend', 'Store Id'),
            'name' => Yii::t('backend', 'Name Type Seat'),
            'introduce' => Yii::t('backend', 'Introduce'),
            'image1' => Yii::t('backend', 'Image1'),
            'image2' => Yii::t('backend', 'Image2'),
            'image3' => Yii::t('backend', 'Image3'),
            'show_flg' => Yii::t('backend', 'Show'),
            'del_flg' => Yii::t('backend', 'Del Flg'),
            'created_by' => Yii::t('backend', 'Created By'),
            'updated_by' => Yii::t('backend', 'Updated By'),
            'created_at' => Yii::t('backend', 'Created At'),
            'updated_at' => Yii::t('backend', 'Updated At'),
        ];
    }

    public function listShow() {
        return array(
            1 => \Yii::t("app", "表示"),
            0 => \Yii::t("app", "非表示"),
        );
    }

    public function getListTypeSeat() {
        return ArrayHelper::map(MasterTypeSeat::find()->all(), 'id', 'name');
    }

    public static function listTypeSeatByStore($shopId = null) {
        $seat_flag = \common\components\Constants::SELECT_SEAT_TYPE;
        if ($shopId != null) {
            return ArrayHelper::map(MasterTypeSeat::find()
                                    ->innerJoin(MasterStore::tableName(), 'mst_store.id = ' . MasterTypeSeat::tableName() . '.store_id')
                                    ->andWhere(['and', ['=', MasterTypeSeat::tableName() . '.store_id', $shopId], ['=', 'booking_resources', $seat_flag]])
                                    ->orderBy(['name' => SORT_ASC])->all(), 'id', 'name');
        }
        return [];
    }

    public function checkDeleteSeat($id) {
        $check = 0;
        $check = MasterTypeSeat::find()->join('INNER JOIN', 'mst_seat', 'mst_type_seat.id = mst_seat.type_seat_id')->andWhere(['mst_type_seat.id' => $id])->count();
        return ($check > 0) ? FALSE : TRUE;
    }

    public static function find() {
        return parent::find()->where((new \common\components\FindPermission)->getPermissionCondition(self::tableName(), true));
    }

    public static function findFrontEnd() {
        return parent::find()
                        ->where(['or', [self::tableName() . '.del_flg' => null], [self::tableName() . '.del_flg' => '0']])
                        ->andWhere([self::tableName() . '.show_flg' => '1']);
    }

    public function getSeats() {
        $seat_search = $this->hasMany(MasterSeat::className(), ['type_seat_id' => 'id'])
                ->andWhere(['or', [MasterSeat::tableName() . '.del_flg' => '0'], [MasterSeat::tableName() . '.del_flg' => null]])
                ->andWhere([MasterSeat::tableName() . '.show_flg' => '1'])
                ->andFilterWhere([MasterSeat::tableName() . '.smoking_flg' => $this->smoking_flg]);

        return $seat_search;
    }

    public function getCapacity() {
        $capacity_search = (new \yii\db\Query())
                ->select([
                    self::tableName() . '.id',
                    'min' => "min(" . MasterSeat::tableName() . ".capacity_min)",
                    'max' => "sum(" . MasterSeat::tableName() . ".capacity_max)",
                    'total_seat_count' => "count(" . MasterSeat::tableName() . ".id)"
                ])
                ->from(self::tableName())
                ->innerJoin(MasterSeat::tableName(), MasterSeat::tableName() . '.type_seat_id = ' . self::tableName() . '.id '
                        . 'and (' . MasterSeat::tableName() . ".del_flg = '0' or " . MasterSeat::tableName() . '.del_flg = null) '
                        . 'and ' . MasterSeat::tableName() . ".show_flg = " . self::tableName() . ".show_flg")
                ->where([self::tableName() . '.id' => $this->id])
                ->andWhere(['or', [self::tableName() . '.del_flg' => null], [self::tableName() . '.del_flg' => '0']])
                ->andWhere([self::tableName() . '.show_flg' => '1'])
                ->andFilterWhere([MasterSeat::tableName() . '.smoking_flg' => $this->smoking_flg])
                ->groupBy([self::tableName() . '.id']);
        return $capacity_search->one();
    }

    /**
     * array list staff 
     */
    public static function listTypeSeat($storeId = null) {
        if ($storeId != null) {
            return ArrayHelper::map(MasterTypeSeat::find()->andWhere(['store_id' => $storeId])->orderBy(['name' => SORT_ASC])->all(), 'id', 'name');
        }
        return ArrayHelper::map(MasterTypeSeat::find()->orderBy(['name' => SORT_ASC])->all(), 'id', 'name');
    }

    /**
     * Get a ready-to-use detailed schedule for type_seats
     * @param type $params
     *   'from_date' => '2016-12-01',
     *   'to_date' => '2016-12-01',
     *   'from_time' => '10:00',
     *   'to_time' => '10:00',
     *   'smoking_flg' => 0,
     *   'store_id' => 7,
     *   'id' => 11,
     *   'type_seat_id' => 1,
     *   'capacity' => 8, // khoang giua capacity_min va capacity_max // so nguoi yeu cau
     *   'capacity_min' => 2,
     *   'capacity_max' => 10
     * @return array
     * date-time => seat_list
     */
    public function getDetailedSchedule($params = []) {
        return MasterSeatSearch::getDetailedTypeSchedule($this->id, $params);
    }

    public function getCapacity_min() {
        if (!$this->_capacity) {
            $this->_capacity = $this->capacity;
        }
        
        return $this->_capacity['min'];
    }
    
    public function getCapacity_max() {
        if (!$this->_capacity) {
            $this->_capacity = $this->capacity;
        }
        
        return $this->_capacity['max'];
    }
}
