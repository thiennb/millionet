<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii2tech\ar\softdelete\SoftDeleteBehavior;
use yii\behaviors\BlameableBehavior;
/**
 * This is the model class for table "mst_enjoy_lottery".
 *
 * @property integer $id
 * @property integer $winner_announcement_date
 * @property integer $winner_announcement_hour
 * @property integer $winner_announcement_minute
 * @property integer $winning_numbers
 * @property string $period_start
 * @property string $period_end
 * @property string $earn_period_from
 * @property string $earn_period_to
 * @property string $period_coming_times_from
 * @property string $period_coming_times_to
 * @property string $register_date_from
 * @property string $register_date_to
 * @property integer $rank_id
 * @property integer $sex
 * @property integer $birthday_year
 * @property integer $birthday_month
 * @property integer $birthday_day
 * @property integer $black_list_check
 * @property integer $member_specified
 * @property integer $staff_1
 * @property integer $staff_2
 * @property integer $staff_3
 * @property string $del_flg
 * @property string $status
 * @property string $people_created
 * @property string $people_updated
 * @property string $count_winner
 * @property integer $created_at
 * @property integer $updated_at
 */
class MasterEnjoyLottery extends \yii\db\ActiveRecord
{
    public $start_register_period_date;
    public $end_register_period_date;
    public $number_bill_min;
    public $number_bill_max;
    public $customer_id_hd;
    public $count_winner;


    const BEFORE_PUBLISH = '00';
    const AFTER_PUBLISH = '01';
    const NO_CAREFULNESS_CHECK = '00';
    const CAREFULNESS_CHECK = '01';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'enjoy_lottery';
    }

    /**
     * @inheritdoc
     */
    
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),            
            BlameableBehavior::className(),
            'softDeleteBehavior' => [
                'class' => SoftDeleteBehavior::className(),
                'softDeleteAttributeValues' => [
                    'del_flg' => '1'
                ],
            ],
        ];
    }
    
    public function rules()
    {
        return [
            ['winner_announcement_date', 'required'],
            [['store_id', 'earn_period_from_max','earn_period_from_min','winning_numbers','visit_store_max','visit_store_min'], 'integer', 'min' => 0],
            [['black_list_check'], 'string', 'max' => 1],
            [['status_winer'], 'string','max' => 2],
            [['winner_announcement_time'],'string','max' => 5],
            [['winner_announcement_date','start_winner_period_date','end_winner_period_date','birth_date'], 'date', 'format' => 'yyyy/MM/dd'],
            ['birth_date', 'compare', 'compareValue' => '1900/01/01', 'operator' => '>=', 'message' => Yii::t('yii', '{attribute} must be greater than "{compareValueOrAttribute}".'), 'skipOnEmpty' => true],
            ['end_winner_period_date', 'compare', 'compareAttribute' => 'start_winner_period_date', 'operator' => '>=', 'message' => Yii::t('backend', 'End date must be greater than Start date.'), 'skipOnEmpty' => true],
            ['winner_announcement_date', 'compare', 'compareValue' => date('Y/m/d'), 'operator' => '>=', 'message' => Yii::t('backend', "Please select date time great than now date")],
            ['earn_period_from_max', 'compare', 'compareAttribute' => 'earn_period_from_min', 'operator' => '>=', 'message' => Yii::t('backend', 'Max number must be greater than min number.'), 'skipOnEmpty' => true],
            ['visit_store_max', 'compare', 'compareAttribute' => 'visit_store_min', 'operator' => '>=', 'message' => Yii::t('backend', 'Max number must be greater than min number.'), 'skipOnEmpty' => true],
            [['sex','staff_id_1','staff_id_2','staff_id_3'], 'default', 'value' => null],
            [['black_list_check','rank_id','customer_id_hd','register_date','count_winner'],'safe'],
            ['winner_announcement_time', 'compare', 'compareValue' => date('H:i'), 'operator' => '>=', 'message' => Yii::t('backend', "Please select date time great than now date"),
                'when' => function ($model) {                    
                    return ($model->winner_announcement_date == date('Y/m/d'));
                }
            ],
            ['count_winner', 'check_count_winner'],
        ];
    }

    /**
     * @inheritdoc
     */
    
    public static function find()
    {
        return parent::find()->where((new \common\components\FindPermission)->getPermissionCondition(self::tableName(), true));
    }
    
    public function attributeLabels()
    {
        return [
            'store_id' => Yii::t('backend', 'Store Ticket'),
            'start_winner_period_date' => Yii::t('backend', 'Period Start List Search'),
            'end_winner_period_date' => Yii::t('backend', 'Period Start List Search'),
            'number_bill_min' => Yii::t('backend', 'Winning Numbers Index'),
            'number_bill_max' => Yii::t('backend', 'Winning Numbers Index'),
            'black_list_check' => Yii::t('backend', 'Careful Check Index'),
            'status_winer'=> Yii::t('backend', 'Status lottery'),
            'start_register_period_date'=>Yii::t('backend','Register date from'),
            'end_register_period_date'=>Yii::t('backend','Register date from'),
            'member_specified'  =>  Yii::t('backend','Member Specified'),
            'winner_announcement_date'  =>  Yii::t('backend','Winner Announcement Date'),
            'winning_numbers'  =>  Yii::t('backend','Winning Numbers'),
            'earn_period_from_min'  =>  Yii::t('backend','Earn Period From'),
            'earn_period_from_max'  =>  Yii::t('backend','Earn Period From'),
            'visit_store_min'   =>  Yii::t('backend','Period Coming Times From'),
            'visit_store_max'   =>  Yii::t('backend','Period Coming Times From'),
            'rank_id'   =>  Yii::t('backend','Rank'),
            'sex'   =>  Yii::t('backend','Sex'),
            'birth_date'   =>  Yii::t('backend','Birth Date'),
            'staff_id_1'   =>  Yii::t('backend','Register staff'),
            'staff_id_2'   =>  Yii::t('backend','Register staff'),
            'staff_id_3'   =>  Yii::t('backend','Register staff'),
        ];
    }
    
    public function beforeSave($insert)
   {
        if (parent::beforeSave($insert)) {
            (!empty($this->rank_id) && is_array($this->rank_id))?$this->rank_id = implode(',',$this->rank_id):$this->rank_id;
            $this->member_specified = $this->customer_id_hd;
            (empty($this->register_date))?$this->register_date = date('Y/m/d'):$this->register_date;
            return true;
        } else {
            return false;
        }
   }
   
    /**
     * Check email
     * @param string $attribute, $params
     * @return error 
     * @throws 
     */
    public function check_count_winner($attribute, $params) {
        if ($this->$attribute === '0' || empty($this->$attribute)) {
            $this->addError('count_winner', Yii::t('backend', "Registration is possible only when the winning number is one or more."));
        }
    }
}
