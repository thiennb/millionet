<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\ChargeHistory;
use yii\validators\CompareValidator;
use common\components\Util;
use yii\db\Query;

/**
 * ChargeHistorySearch represents the model behind the search form about `common\models\ChargeHistory`.
 */
class ChargeHistorySearch extends ChargeHistory {

    public $sex;
    public $register_store_id;
    public $register_staff_id;
    public $name;
    public $customer_jan_code;
    public $birth_date_from;
    public $birth_date_to;
    public $charge_history;
    public $rank;
    public $number_visit;
    public $history_date_from;
    public $history_date_to;
    public $charge_amount_from;
    public $charge_amount_to;
    public $number_visit_min;
    public $number_visit_max;
    public $store_id;

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['input_money', 'process_money', 'charge_amount_from', 'charge_amount_to', 'charge_balance'], 'number', 'max' => 9999999999, 'min' => 0],
            [['input_money', 'process_money', 'charge_amount_from', 'charge_amount_to', 'charge_balance'], 'string', 'max' => 13],
            [['id', 'created_at', 'updated_at', 'charge_balance', 'register_store_id', 'register_staff_id', 'number_visit_min', 'number_visit_max'], 'integer'],
            [['name', 'store_id', 'sex', 'process_time', 'number_visit_min', 'number_visit_max', 'process_type', 'history_date_from', 'history_date_to', 'del_flg', 'birth_date_from', 'birth_date_to', 'rank', 'customer_jan_code', 'process_date'], 'safe'],
            [['name'], 'string', 'max' => 80],
            [['number_visit_min', 'number_visit_max'], 'integer', 'max' => 99999, 'min' => 0],
            [['number_visit_min', 'number_visit_max'], 'string', 'max' => 6],
            [['birth_date_from', 'birth_date_to', 'history_date_from', 'history_date_to'], 'date', 'format' => 'php:Y/m/d'],
            [['customer_jan_code'], 'string', 'max' => 13],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     * @customer_id int $customer_id
     *
     * @return ActiveDataProvider
     */
    public function chargeHistorySearch($params, $customer_id) {
        $query = ChargeHistory::find();
        $query->joinWith('store', true, 'INNER JOIN');
        $query->joinWith('customerMaster', true, 'INNER JOIN');
        $query->leftJoin('customer_store', "charge_history.customer_id = customer_store.customer_id AND mst_store.id = customer_store.store_id AND customer_store.del_flg = '0'");
        // Join : Company store
        $query->innerJoin('company', 'company.id = mst_store.company_id');
       // $cookies = Yii::$app->request->cookies;
        $company_id =  Util::getCookiesCompanyId();
        $query->andFilterWhere([
            'company.id' => $company_id,
        ]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query->with('customerMaster'),
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => false,
        ]);
        if (isset($customer_id)) {
            $query->andFilterWhere(['charge_history.customer_id' => $customer_id]);
            return $dataProvider;
        }
        // add conditions that should always apply here
        $this->load($params);
        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere(['=', MasterStore::tableName() . '.id', $this->register_store_id]);
        if ($this->register_staff_id != '') {
            $query->andFilterWhere(['=', 'charge_history.staff_management_id', $this->register_staff_id]);
            /*
           $command = Yii::$app->getDb()->createCommand(
                                    "SELECT 
                                        max(ct.order_code) as oder_code,
                                        mst_customer.id as id_customer
                                     FROM 
                                     (SELECT max(mst_order.order_code) as order_code,mst_order.customer_jan_code FROM mst_order GROUP BY mst_order.customer_jan_code)
                                     ct
                                     INNER JOIN mst_customer
                                     ON mst_customer.customer_jan_code = ct.customer_jan_code
                                     INNER JOIN customer_store
                                     ON customer_store.customer_id = mst_customer.id
                                     INNER JOIN mst_order
                                     ON mst_order.customer_jan_code = mst_customer.customer_jan_code
                                     WHERE
                                     customer_store.store_id = :idStore
                                     AND
                                     mst_order.reji_management_id = :idLastStaff
                                     AND
                                     mst_order.del_flg = '0'
                                     GROUP BY
                                      mst_customer.id"
            );

            $id_last_staff = (new BookingSearch)->converIdLastStaff(intval($this->register_staff_id));
            $id_store = $this->register_store_id;
            
            $command->bindParam(':idLastStaff', $id_last_staff)
                    ->bindParam(':idStore', $id_store);
            
           
            $result = $command->queryAll();

            if (count($result) > 0) {
                $i = 0;
                $result_seach = null;
                foreach ($result as $value) {
                    $id_customer = $value['id_customer'];
                    if ($i == 0) {
                        $result_seach = $id_customer;
                    } else {
                        $result_seach .= "," . $id_customer;
                    }
                    $i++;
                }
                $array_cover = explode(',', $result_seach);

                $query->andFilterWhere(['charge_history.customer_id' => $array_cover]);
            } else {

                $query->andFilterWhere(['=', 'charge_history.created_at', 0]);
            }
            */
        }
        $query->andFilterWhere(['>=', 'charge_history.process_date', $this->history_date_from]);
        $query->andFilterWhere(['<=', 'charge_history.process_date', $this->history_date_to]);
        $query->andFilterWhere(['>=', 'charge_history.process_money', $this->charge_amount_from]);
        $query->andFilterWhere(['<=', 'charge_history.process_money', $this->charge_amount_to]);
        $query->andFilterWhere(['=', 'customer_jan_code', $this->customer_jan_code]);
        $query->andFilterWhere(['or', ['like', 'concat(first_name,last_name) ', $this->name], ['like', 'concat(first_name_kana,last_name_kana) ', $this->name]]);
        $query->andFilterWhere(['sex' => $this->sex]);
        $query->andFilterWhere(['=', 'customer_store.rank_id', $this->rank]);
        $query->andFilterWhere(['>=', 'customer_store.number_visit', $this->number_visit_min]);
        $query->andFilterWhere(['<=', 'customer_store.number_visit', $this->number_visit_max]);
        $query->andFilterWhere(['>=', 'birth_date', $this->birth_date_from]);
        $query->andFilterWhere(['<=', 'birth_date', $this->birth_date_to]);
        $query->groupBy(ChargeHistory::tableName() . '.id');

        return $dataProvider;
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     * @customer_id int $customer_id
     * 
     * 
     * @return ActiveDataProvider
     */
    public function searchHistory($params, $customer_id = null) {
        $query = ChargeHistory::find();
        // add conditions that should always apply here
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 5,
            ],
            'sort' => false
        ]);
        $this->load($params);
        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        $query->andFilterWhere(['=', 'customer_id', $customer_id]);
        if (!empty($this->history_date_from))
            $query->andFilterWhere(['>=', 'charge_history.process_date', $this->history_date_from]);
        if (!empty($this->history_date_to))
            $query->andFilterWhere(['<=', 'charge_history.process_date', $this->history_date_to]);
        if (!empty($this->store_id)) {
            $query->leftJoin(MasterStore::tableName(), 'charge_history.store_code = mst_store.store_code');
            $query->andFilterWhere(['=', MasterStore::tableName() . '.id', $this->store_id]);
        }
        $query->groupBy('charge_history.id');
        return $dataProvider;
    }

   
    public function searchChargehistory($customerId = null, $company_id)
    {
        $search_query = (new Query())
                        ->select([
                            'charge_history.store_code',
                            'max(charge_history.process_date) as process_date_d',
                            'max(mst_store.name) as name',
                        ])
                        ->from(["charge_history" => ChargeHistory::tableName()])
                        ->innerJoin(['mst_store' => MasterStore::tableName()], "mst_store.store_code = charge_history.store_code "
                                . "and mst_store.del_flg = :zero");
        $search_query->andFilterWhere([ChargeHistory::tableName() . '.customer_id' => $customerId]);
        $search_query->andFilterWhere([ChargeHistory::tableName() . '.company_id' => $company_id]);       
        $search_query->groupBy(["charge_history.store_code"]) ;
        $search_query->orderBy([
            'process_date_d' => SORT_DESC
        ]);
        $list_query = (new Query())
                        ->select([
                            'charge_history.store_code',
                            'charge_history.id',
                            'process_date_d',
                            'process_time',
                            'name',
                            'charge_balance',
                        ])
                ->from(["search" => $search_query])
                ->innerJoin(['charge_history' => ChargeHistory::tableName()], "charge_history.process_date = search.process_date_d "
                    . "and charge_history.store_code = search.store_code");
            
        $list_query->addParams([
            'zero' => '0'
            ]);
        $dataProvider = new ActiveDataProvider([
            'query' => $list_query,
            'pagination' => [
                'pageSizeLimit' => [1,5],
            ],
            'sort' => false
        ]);
        return $dataProvider;
    }


    public function chargeDetail($customerId,$store_code = null,$company_id = null)
    {
        $query = ChargeHistory::find();
        $query->select([
                        'charge_history.process_date',
                        'charge_history.process_time',
                        'charge_history.process_money',
                        'mst_store.name',
                        'charge_history.process_type',
                    ]);
        $query->innerJoin('mst_store', 'charge_history.store_code = mst_store.store_code') ; 
        $query->andFilterWhere(['=', 'charge_history.store_code', $store_code]);
        $query->andFilterWhere(['=', 'charge_history.customer_id', $customerId]);
        $query->andFilterWhere(['=', 'charge_history.company_id', $company_id]);
        $query->andFilterWhere(['=', 'charge_history.del_flg',0]);
        $query->orderBy([
            'charge_history.process_date' => SORT_DESC,
            'charge_history.process_time' => SORT_DESC
        ]);

        return $query;
    }

    public function getCharge_Balance($customer_id , $id, $company_id)
    {
        if ($id) {
            $query = ChargeHistory::find()->select([
                'charge_history.customer_id',
                'charge_history.id',
                'charge_history.charge_balance'
            ]);
            $query->andFilterWhere(['=', ChargeHistory::tableName() . '.id', $id]);
            $query->andFilterWhere(['=', ChargeHistory::tableName() . '.customer_id', $customer_id]);
            $query->andFilterWhere(['=', ChargeHistory::tableName() . '.company_id', $company_id]);
            $data = $query->asArray()->one();
            return $data;      
        }
    }
    
    public static function searchLastestChargeHistory ($customers, $company_id) {
        $latest_date_query = (new Query())
                ->select([
                    'customer_id',
                    'store_code',
                    'process_date' => 'MAX(process_date)'
                ])
                ->from(self::tableName())
                ->where(['or', ['del_flg' => '0'], ['del_flg' => null]])
                ->andWhere(['customer_id' => $customers])
                ->andWhere(['company_id' => $company_id])
                ->groupBy(['customer_id', 'store_code']);
        
        $latest_date_time_query = (new Query())
                ->select([
                    'c.customer_id',
                    'c.store_code',
                    'c.process_date',
                    'process_time' => 'MAX(c.process_time)'
                ])
                ->from(['s' => $latest_date_query])
                ->innerJoin(['c' => self::tableName()], 'c.customer_id = s.customer_id '
                        . 'and c.store_code = s.store_code '
                        . 'and c.process_date = s.process_date')

                ->groupBy([
                    'c.customer_id',
                    'c.store_code',
                    'c.process_date'
                ]);
        
        return $latest_date_time_query;
    }
}
