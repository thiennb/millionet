<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii2tech\ar\softdelete\SoftDeleteBehavior;
use yii\helpers\ArrayHelper;
use yii\validators\RequiredValidator;
use yii\validators\NumberValidator;
use yii\validators\DateValidator;
use yii\behaviors\BlameableBehavior;
use common\models\TaxDetail;
use common\models\MstProduct;
use common\components\Util;
use common\components\CompanyIdBehavior;

/**
 * This is the model class for table "mst_tax".
 *
 * @property integer $id
 * @property string $code
 * @property string $name
 * @property double $rate
 * @property string $start_date
 * @property string $end_date
 * @property string $del_flg
 * @property integer $created_at
 * @property integer $updated_at
 * @property array $mutil_rate
 *
 * @property MstProduct[] $mstProducts
 */
class MasterTax extends \yii\db\ActiveRecord {

    public $mutil_rate;
    public $rate;
    public $start_date;
    public $end_date;
    public $detail_id;
    public $error_tax_rate;
    public $id_tax;

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            TimestampBehavior::className(),
            BlameableBehavior::className(),
            CompanyIdBehavior::className(),
            'softDeleteBehavior' => [
                'class' => SoftDeleteBehavior::className(),
                'softDeleteAttributeValues' => [
                    'del_flg' => '1'
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'mst_tax';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['name'], 'required'],
            [['name'], 'unique',
                'targetClass' => 'common\models\MasterTax',
                'when' => function ($model, $attribute) {
            return $model->{$attribute} !== $model->getOldAttribute($attribute);
        },
            ],
            [['name'], 'string', 'max' => 20],
            [['rate'], 'integer', 'min' => 0,],
            [['detail_id'], 'integer'],
            [['mutil_rate'], 'checkMultipleInput'],
            [['start_date', 'end_date', 'detail_id', 'error_tax_rate', 'id_tax', 'company_id'], 'safe'],
            ['name', 'validateName', 'on' => 'create'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => Yii::t('backend', 'ID'),
            'code' => Yii::t('backend', 'Code'),
            'name' => Yii::t('backend', 'Name Tax'),
            'rate' => Yii::t('backend', 'Rate'),
            'start_date' => Yii::t('backend', 'Start Date'),
            'end_date' => Yii::t('backend', 'End Date'),
            'del_flg' => Yii::t('backend', 'Del Flg'),
            'created_at' => Yii::t('backend', 'Created At'),
            'updated_at' => Yii::t('backend', 'Updated At'),
        ];
    }

    /**
     * add condition search del_flg = 0
     * @param 
     * @return 
     * @throws 
     */
    public static function find() {
        return parent::find()->where(['mst_tax.del_flg' => '0']);
    }

    /**
     * add condition search del_flg = 0
     * @param 
     * @return array list tax: array[code=>name]
     * @throws 
     */
    public static function listTax() {
        $company_id = Util::getCookiesCompanyId();
        return ArrayHelper::map(self::find()->innerJoin('company', 'company.id = mst_tax.company_id')->andFilterWhere(['mst_tax.del_flg' => '0', 'company.id' => $company_id])->groupBy('mst_tax.id,mst_tax.name')->select(['mst_tax.id', 'mst_tax.name'])->orderBy('name')->all(), 'id', 'name');
    }

    /**
     * Check data input in multiple rate of tax
     * @param string $attribute
     * @return error 
     * @throws 
     */
    public function checkMultipleInput($attribute) {
        //create validate required
        $requiredValidator = new RequiredValidator();
        //create validate number
        $numberValidator = new NumberValidator();
        //create validate date
        $dateValidator = new DateValidator();
        $mutil_rate = $this->$attribute;
        //check set tax rate
        $check_tax_rate = false;
        $check_today_exists_in_rows = false;

        //validate for each value of attribute
        foreach ($mutil_rate as $index => $row) {
            if ($row['reduced_tax_flg'] == "0" && $row['rate'] == "" && $row['start_date'] == "" && $row['end_date'] == "") {
                continue;
            }
            //rate required
            $error = null;
            $requiredValidator->validate($row['rate'], $error);
            if (!empty($error)) {
                $key = $attribute . '[' . $index . '][rate]';
                $this->addError($key, $error);
            }
            //rate number
            $error = null;
            $numberValidator->min = 0;
            $numberValidator->max = 999.999;
            $numberValidator->validate($row['rate'], $error);
            if (!empty($error)) {
                $key = $attribute . '[' . $index . '][rate]';
                $this->addError($key, $error);
            }
            //check rate min
            if ($numberValidator->min > (int) $row['rate']) {
                $key = $attribute . '[' . $index . '][rate]';
                $attribute_label = Yii::t('backend', 'Tax Rate');
                $this->addError($key, Yii::t('yii', '{attribute} must be no less than {min}.', [
                            'attribute' => $attribute_label,
                            'min' => $numberValidator->min
                ]));
            }
            //check rate max
            if ($numberValidator->max < (int) $row['rate']) {
                $key = $attribute . '[' . $index . '][rate]';
                $attribute_label = Yii::t('backend', 'Tax Rate');
                $this->addError($key, Yii::t('yii', '{attribute} must be no greater than {max}.', [
                            'attribute' => $attribute_label,
                            'max' => $numberValidator->max
                ]));
            }
            //start_date required
            $error = null;
            $requiredValidator->validate($row['start_date'], $error);
            if (!empty($error)) {
                $key = $attribute . '[' . $index . '][start_date]';
                $this->addError($key, $error);
            }
            //start_date date
            $error = null;
            $dateValidator->format = 'yyyy/MM/dd';
            $dateValidator->validate($row['start_date'], $error);
            if (!empty($error)) {
                $key = $attribute . '[' . $index . '][start_date]';
                $this->addError($key, $error);
            }
            //end_date required
            $error = null;
            $requiredValidator->validate($row['end_date'], $error);
            if (!empty($error)) {
                $key = $attribute . '[' . $index . '][end_date]';
                $this->addError($key, $error);
            }
            //end_date date
            $error = null;
            $dateValidator->format = 'yyyy/MM/dd';
            $dateValidator->validate($row['end_date'], $error);
            if (!empty($error)) {
                $key = $attribute . '[' . $index . '][end_date]';
                $this->addError($key, $error);
            }
            //check start date  < end date
            if (!empty($row['start_date']) && !empty($row['end_date']) && $row['start_date'] > $row['end_date']) {
                $key = $attribute . '[' . $index . '][end_date]';
                $this->addError($key, Yii::t('backend', "End date must be greater than Start date."));
            }

            for ($i = 0; $i < ($index); $i++) {
                if (!empty($row['start_date']) && !empty($row['end_date']) && (
                        (($mutil_rate[$i]['start_date'] <= $row['start_date']) && ($mutil_rate[$i]['end_date'] >= $row['start_date'])) ||
                        (($mutil_rate[$i]['start_date'] <= $row['end_date']) && ($mutil_rate[$i]['end_date'] >= $row['end_date']))
                        )) {
                    $key = $attribute . '[' . $index . '][start_date]';
                    $this->addError($key, Yii::t('backend', "Entered date is within the date range."));
                }
            }
            $check_tax_rate = true;

            if (!empty($row['start_date']) && !empty($row['end_date'])) {
                $start_date_timestamp = \Carbon\Carbon::createFromFormat('Y/m/d', $row['start_date'])->timestamp;
                $end_date_timestamp = \Carbon\Carbon::createFromFormat('Y/m/d', $row['end_date'])->timestamp;
                $current_date_timestamp = \Carbon\Carbon::createFromFormat('Y/m/d', date('Y/m/d'))->timestamp;

                if ($start_date_timestamp <= $current_date_timestamp && $end_date_timestamp >= $current_date_timestamp) {
                    $check_today_exists_in_rows = true;
                }
            }
        }

        if (!$check_today_exists_in_rows) {
            $this->addError('error_tax_rate', Yii::t('backend', "Please enter a time period including today."));
        }

        //check have tax rate
        if (!$check_tax_rate) {
            $this->addError('error_tax_rate', Yii::t('backend', "Please enter a tax rate to one or more."));
        }
    }

    /**
     * Check name of tax
     * @param string $attribute, $params
     * @return error 
     * @throws 
     */
    public function validateName($attribute, $params) {
        //check name exists in database
        $check = MasterTax::find()->where([ 'name' => $this->$attribute])->exists();
        //if exists name
        if ($check) {
            $key = $attribute;
            //add message error
            $this->addError($key, Yii::t('backend', "Sory Name was been used."));
        }
    }

    /**
     * Get next code of tax
     * @param 
     * @return new code of tax, code will not duplicate 
     * @throws 
     */
    public function getNextCode() {
        //get max id in database
        $old_code = MasterTax::find()->max('id');
        //if no data return 1 or return $old_code +1
        return empty($old_code) ? 1 : $old_code + 1;
    }

    /**
     * save tax with multiple rate
     * @param 
     * @return 
     * @throws 
     */
    public function createMultilRate() {
        $model = new MasterTax();
        $model->name = $this->name;
        $model->save();
        //save detail
        foreach ($this->mutil_rate as $key => $val) {
            if ($val['reduced_tax_flg'] == "0" && $val['rate'] == "" && $val['start_date'] == "" && $val['end_date'] == "") {
                continue;
            }
            $detail = new TaxDetail();
            $detail->tax_id = $model->id;
            $detail->rate = $val['rate'];
            $detail->reduced_tax_flg = $val['reduced_tax_flg'];
            $detail->start_date = $val['start_date'];
            $detail->end_date = $val['end_date'];
            $detail->save();
        }
    }

    /**
     * save tax with multiple rate
     * @param 
     * @return 
     * @throws 
     */
    public function updateMultilRate($id) {
        TaxDetail::updateAll(['del_flg' => '1', 'updated_at' => strtotime("now")], ['tax_id' => $id]);
        //save detail
        foreach ($this->mutil_rate as $key => $val) {
            if ($val['reduced_tax_flg'] == "0" && $val['rate'] == "" && $val['start_date'] == "" && $val['end_date'] == "") {
                continue;
            }
            $detail = new TaxDetail();
            $detail->tax_id = $id;
            $detail->rate = $val['rate'];
            $detail->reduced_tax_flg = $val['reduced_tax_flg'];
            $detail->start_date = $val['start_date'];
            $detail->end_date = $val['end_date'];
            $detail->save();
        }
    }

    /**
     * Get all rate of tax when update
     * @param 
     * @return 
     * @throws 
     */
    public static function initMultilRate($tax_id) {
        //get all record of tax
        $multi_rate_tmp = TaxDetail::find()->where(['tax_id' => $tax_id, 'del_flg' => '0'])->asArray()->all();
        foreach ($multi_rate_tmp as $key => $val) {
            //convert date
            $multi_rate_tmp[$key]['start_date'] = date('Y/m/d', strtotime($val['start_date']));
            $multi_rate_tmp[$key]['end_date'] = date('Y/m/d', strtotime($val['end_date']));
        }
        return $multi_rate_tmp;
    }

    public function checkDelete($id) {
        $check = 0;
        $check = self::find()->join("INNER JOIN", 'tax_detail', 'tax_detail.tax_id = mst_tax.id')
                        ->join("INNER JOIN", 'mst_product', 'mst_product.tax_rate_id = mst_tax.id')->where(['tax_detail.id' => $id])->count();
        return ($check > 0) ? FALSE : TRUE;
    }

}
