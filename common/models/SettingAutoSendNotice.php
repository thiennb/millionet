<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii2tech\ar\softdelete\SoftDeleteBehavior;
use yii\behaviors\BlameableBehavior;
use common\components\Constants;

/**
 * This is the model class for table "mst_setting_auto_send_notice".
 *
 * @property integer $id
 * @property string $status_send
 * @property string $push_timming
 * @property integer $hour_before_booking
 * @property integer $hour_after_register_member
 * @property integer $hour_after_accounting
 * @property integer $distance_to_shop
 * @property string $customer_jan_code
 * @property string $first_name
 * @property string $last_name
 * @property string $first_name_kana
 * @property string $last_name_kana
 * @property string $sex
 * @property string $start_birth_date
 * @property string $end_birth_date
 * @property string $start_visit_date
 * @property string $end_visit_date
 * @property integer $max_visit_number
 * @property integer $min_visit_number
 * @property string $rank_id
 * @property integer $last_staff_id
 * @property string $black_list_flg
 * @property string $del_flg
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $notice_id
 */
class SettingAutoSendNotice extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'setting_auto_send_notice';
    }

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            TimestampBehavior::className(),
            BlameableBehavior::className(),
            'softDeleteBehavior' => [
                'class' => SoftDeleteBehavior::className(),
                'softDeleteAttributeValues' => [
                    'del_flg' => '1'
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['hour_before_booking', 'hour_after_register_member', 'hour_after_accounting', 'distance_to_shop', 'max_visit_number', 'min_visit_number', 'last_staff_id', 'store_id', 'notice_id'], 'integer'],
            [['status_send', 'push_timming', 'rank_id'], 'string', 'max' => 2],
            [['sex', 'black_list_flg', 'del_flg'], 'string', 'max' => 1],
            [['customer_jan_code'], 'string', 'max' => 13],
            ['end_birth_date', 'compare', 'compareAttribute' => 'start_birth_date', 'operator' => '>=', 'message' => Yii::t('backend', 'End date must be greater than Start date.'), 'skipOnEmpty' => TRUE],
            ['end_visit_date', 'compare', 'compareAttribute' => 'start_visit_date', 'operator' => '>=', 'message' => Yii::t('backend', 'End date must be greater than Start date.'), 'skipOnEmpty' => TRUE],
            ['max_visit_number', 'compare', 'compareAttribute' => 'min_visit_number', 'operator' => '>=', 'message' => Yii::t('backend', 'Max number must be greater than min number.'), 'skipOnEmpty' => TRUE],
            [['first_name', 'last_name', 'first_name_kana', 'last_name_kana'], 'string', 'max' => 40],
            [['start_birth_date', 'end_birth_date', 'start_visit_date', 'end_visit_date'], 'string', 'max' => 10],
            [['start_birth_date', 'end_birth_date', 'start_visit_date', 'end_visit_date'], 'date', 'format' => 'php:Y/m/d'],
            [['min_visit_number', 'max_visit_number'], 'integer', 'max' => 99999, 'min' => 0],
            [['min_visit_number', 'max_visit_number'], 'string', 'max' => 6],
            [['first_name_kana', 'last_name_kana'], 'match', 'pattern' => '/^[ァ-ヴー]+$/u', 'message' => Yii::t('backend', '{attribute} invalid characters full size only')],
            [['first_name_kana', 'last_name_kana', 'rank_id', 'customer_jan_code', 'first_name', 'last_name', 'status_send', 'push_timming', 'sex'], 'default', 'value' => null],
            ['hour_before_booking', 'required', 'when' => function($model) {
                    return $model->push_timming == Constants::PUSH_TIMMING_BEFORE_BOOKING;
                }],
            ['hour_after_register_member', 'required', 'when' => function($model) {
                    return $model->push_timming == Constants::PUSH_TIMMING_AFTER_REGISTER;
                }],
            ['hour_after_accounting', 'required', 'when' => function($model) {
                    return $model->push_timming == Constants::PUSH_TIMMING_AFTER_CHARGE;
                }],
            ['distance_to_shop', 'required', 'when' => function($model) {
                    return $model->push_timming == Constants::PUSH_TIMMING_NEAR_STORE;
                }],
            [['hour_after_register_member', 'hour_before_booking', 'hour_after_accounting', 'distance_to_shop'], 'number', 'min' => 0, 'max' => 24],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => Yii::t('backend', 'ID'),
            'status_send' => Yii::t('backend', 'Status Send'),
            'push_timming' => Yii::t('backend', 'Deliver Time'),
            'hour_before_booking' => Yii::t('backend', 'Hour Before Booking'),
            'hour_after_register_member' => Yii::t('backend', 'Hour After Register Member'),
            'hour_after_accounting' => Yii::t('backend', 'Hour After Accounting'),
            'distance_to_shop' => Yii::t('backend', 'Distance To Shop'),
            'customer_jan_code' => Yii::t('backend', 'Membership card number'),
            'first_name' => Yii::t('backend', 'First name'),
            'last_name' => Yii::t('backend', 'Last name'),
            'first_name_kana' => Yii::t('backend', 'First name kana'),
            'last_name_kana' => Yii::t('backend', 'Last name kana'),
            'sex' => Yii::t('backend', 'Sex'),
            'start_birth_date' => Yii::t('backend', 'Birthday'),
            'end_birth_date' => Yii::t('backend', 'Birthday'),
            'start_visit_date' => Yii::t('backend', 'Last visit time'),
            'end_visit_date' => Yii::t('backend', 'Last visit time'),
            'max_visit_number' => Yii::t('backend', 'Visit number'),
            'min_visit_number' => Yii::t('backend', 'Visit number'),
            'rank_id' => Yii::t('backend', 'Customers rank'),
            'last_staff_id' => Yii::t('backend', 'Last time staff'),
            'black_list_flg' => Yii::t('backend', 'black_list'),
            'del_flg' => Yii::t('backend', 'Del Flg'),
            'created_at' => Yii::t('backend', 'Created At'),
            'updated_at' => Yii::t('backend', 'Updated At'),
            'created_by' => Yii::t('backend', 'Created By'),
            'updated_by' => Yii::t('backend', 'Updated By'),
        ];
    }

    
    
    public static function find($findAll = true) {
        if ($findAll)
            return parent::find()->where(['setting_auto_send_notice.del_flg' => '0']);
        else
            return parent::find();
    }

    /**
     * Process before save into database
     * @return none
     */
    public function beforeSave($insert) {
        if (parent::beforeSave($insert)) {
            $this->status_send = (!empty($this->status_send)) ? $this->status_send : '00';
            return true;
        } else {
            return false;
        }
    }

    public function getStaff() {
        return $this->hasOne(MasterStaff::className(), ['id' => 'last_staff_id']);
    }

    public function getStore() {
        return $this->hasOne(MasterStore::className(), ['id' => 'store_id']);
    }
    
}
