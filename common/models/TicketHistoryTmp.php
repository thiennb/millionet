<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace common\models;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii2tech\ar\softdelete\SoftDeleteBehavior;
use yii\behaviors\BlameableBehavior;
use common\components\Util;
use common\components\Constants;

/**
 * Description of TicketHistoryTmp
 *
 * @author rikei
 */
class TicketHistoryTmp extends \yii\db\ActiveRecord{
    //put your code here
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ticket_history_tmp';
    }
    
    /**
     * @inheritdoc
     */
    public $store_name;
    public $name;
    public $ticket_name;
    public function rules()
    {
        return [
            [['process_type'], 'required'],
            [[ 'created_at', 'created_by', 'updated_at', 'updated_by','store_id'], 'safe'],
            [['ticket_balance', 'process_number'], 'number'],
            [['process_type', 'del_flg'], 'string', 'max' => 1],
        ];
    }
    public function behaviors() {
        return [
            TimestampBehavior::className(),
            BlameableBehavior::className(),
            'softDeleteBehavior' => [
                'class' => SoftDeleteBehavior::className(),
                'softDeleteAttributeValues' => [
                    'del_flg' => '1'
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'store_id' => 'Store Id',
            'ticket_balance' => 'Ticket Balance',
            'process_number' => 'Process Number',
            'process_type' => 'Process Type',
            'del_flg' => 'Del Flg',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
        ];
    }
    
    public function getTicket()
    {   
      return $this->hasOne(MasterTicket::className(), ['ticket_jan_code' => 'ticket_jan_code']);
    }
    
//    public function getStore()
//    {   
//      return $this->hasOne(MasterStore::className(), ['store_code' => 'SUBSTRING(order_code, 1, 5)']);
//    }
    
    public function getStaff()
    {   
      return $this->hasOne(MasterCustomer::className(), ['id' => 'customer_id']);
    }
    
    public function getCustomer()
    {   
      return $this->hasOne(MasterCustomer::className(), ['customer_jan_code' => 'customer_jan_code']);
    }
    
    public function getOrder()
    {   
      return $this->hasOne(MstOrderTmp::className(), ['order_code' => 'order_code']);
    }
    
    public static function find()
    {
        return parent::find()->where((new \common\components\FindPermission)->getPermissionCondition(self::tableName(), false, false, true));
    }

    public function getmasterTicket()
    {
        return $this->hasOne(MasterTicket::className(), ['ticket_jan_code' => 'ticket_jan_code']);
    }
    
    public function getTicketBalance($ticketJanCode = null, $customerJancode = null){
        if($ticketJanCode !== null && $customerJancode !== null){
            $query = new \yii\db\Query();
            $result = $query->select(['ticket_balance', 'process_type'])
                    ->from('ticket_history_tmp')
                    ->innerJoin('mst_order_tmp', 'ticket_history_tmp.order_code = mst_order_tmp.order_code')
                    ->where(['ticket_history_tmp.ticket_jan_code'=>$ticketJanCode])->andWhere(['ticket_history_tmp.del_flg'=> '0'])
                    ->andWhere(['mst_order_tmp.customer_jan_code'=> $customerJancode])->andWhere(['mst_order_tmp.del_flg'=> '0'])
                    ->orderBy('ticket_history_tmp.updated_at DESC, ticket_history_tmp.process_type DESC')->one();
            
            return $result;
        }
        
        return null;
    }
    
    public function getBalanceTicketForUpdateCustomerStore($customerId = null, $storeCode = null){
        if($customerId !== null && $storeCode !== null){

            $connection = Yii::$app->db;
            $query = $connection->createCommand("SELECT DISTINCT TRIM(ticket.name) as name,ticket.id,ticket.ticket_jan_code
                FROM mst_order_tmp INNER JOIN ticket_history_tmp ON mst_order_tmp.order_code = ticket_history_tmp.order_code
                INNER JOIN ticket ON ticket_history_tmp.ticket_jan_code = ticket.ticket_jan_code
                WHERE mst_order_tmp.customer_jan_code = (select customer_jan_code from mst_customer where id =" . $customerId . ")
                AND mst_order_tmp.del_flg = '0'
                AND substring(mst_order_tmp.order_code from 1 for 5) = '".$storeCode."'
                AND ticket_history_tmp.del_flg = '0'
                AND ticket.del_flg = '0'
                ORDER BY name");
            $results = $query->queryAll();
            $count = 0;
            foreach($results as $ticket){
                $ticketHistory = $connection->createCommand("SELECT ticket_balance FROM ticket_history_tmp WHERE ticket_jan_code = '".$ticket['ticket_jan_code']."'
                        AND ticket_history_tmp.del_flg = '0'
                        ORDER BY updated_at DESC LIMIT 1")->queryAll();
                
                if(!empty($ticketHistory)){
                    if($ticketHistory[0]['ticket_balance'] > 0){
                        $count++;
                    }
                }
            }
            
            return $count;
        }
        
        return 0;
    }
    
    
    public static function getTicketOfCustomerInStore($customerId = null, $store_id = null){
        if($customerId !== null && $store_id !== null){
            
            $store = MasterStore::findOne($store_id);
            if(empty($store)) return [];
            $connection = Yii::$app->db;
            $query = $connection->createCommand("SELECT DISTINCT TRIM(ticket.name) as name,ticket.id,ticket.ticket_jan_code, ticket.price
                FROM mst_order_tmp INNER JOIN ticket_history_tmp ON mst_order_tmp.order_code = ticket_history_tmp.order_code
                INNER JOIN ticket ON ticket_history_tmp.ticket_jan_code = ticket.ticket_jan_code
                WHERE mst_order_tmp.customer_jan_code = (select customer_jan_code from mst_customer where id =" . $customerId . ")
                AND mst_order_tmp.del_flg = '0'
                AND substring(mst_order_tmp.order_code from 1 for 5) = '".$store->store_code."'
                AND ticket_history_tmp.del_flg = '0'
                AND ticket.del_flg = '0'
                ORDER BY name");
            $results = $query->queryAll();
            foreach($results as $key => $ticket){
                $ticketHistory = $connection->createCommand("SELECT ticket_balance FROM ticket_history_tmp WHERE ticket_jan_code = '".$ticket['ticket_jan_code']."'
                        AND ticket_history_tmp.del_flg = '0'
                        ORDER BY updated_at DESC LIMIT 1")->queryAll();
                
                $results[$key]['ticket_balance'] = $ticketHistory[0]['ticket_balance'];
                $list_product = \yii\helpers\ArrayHelper::map(MstProduct::find()->innerJoin('product_ticket','mst_product.id = product_ticket.product_id')->andWhere(['product_ticket.ticket_id' => $results[$key]['id']])->select(['mst_product.id','mst_product.jan_code'])->all(), 'id', 'jan_code');

                $list_product_tmp = implode(',', $list_product);
                $results[$key]['list_product'] = $list_product_tmp;
            }
            
            return $results;
        }
        
        return [];
    }
}
