<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\MasterSettingAutoSendNotice;

/**
 * MasterSettingAutoSendNoticeSearch represents the model behind the search form about `common\models\MasterSettingAutoSendNotice`.
 */
class MasterSettingAutoSendNoticeSearch extends MasterSettingAutoSendNotice
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'hour_before_booking', 'hour_after_register_member', 'hour_after_accounting', 'distance_to_shop', 'max_visit_number', 'min_visit_number', 'last_staff_id', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['status_send', 'push_timming', 'membership_card_number', 'first_name', 'last_name', 'first_name__kana', 'last_name_kana', 'sex', 'start_birth_date', 'end_birth_date', 'start_visit_date', 'end_visit_date', 'rank_member', 'black_list', 'del_flg'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MasterSettingAutoSendNotice::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'hour_before_booking' => $this->hour_before_booking,
            'hour_after_register_member' => $this->hour_after_register_member,
            'hour_after_accounting' => $this->hour_after_accounting,
            'distance_to_shop' => $this->distance_to_shop,
            'start_birth_date' => $this->start_birth_date,
            'end_birth_date' => $this->end_birth_date,
            'start_visit_date' => $this->start_visit_date,
            'end_visit_date' => $this->end_visit_date,
            'max_visit_number' => $this->max_visit_number,
            'min_visit_number' => $this->min_visit_number,
            'last_staff_id' => $this->last_staff_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'status_send', $this->status_send])
            ->andFilterWhere(['like', 'push_timming', $this->push_timming])
            ->andFilterWhere(['like', 'membership_card_number', $this->membership_card_number])
            ->andFilterWhere(['like', 'first_name', $this->first_name])
            ->andFilterWhere(['like', 'last_name', $this->last_name])
            ->andFilterWhere(['like', 'first_name__kana', $this->first_name__kana])
            ->andFilterWhere(['like', 'last_name_kana', $this->last_name_kana])
            ->andFilterWhere(['like', 'sex', $this->sex])
            ->andFilterWhere(['like', 'rank_member', $this->rank_member])
            ->andFilterWhere(['like', 'black_list', $this->black_list])
            ->andFilterWhere(['like', 'del_flg', $this->del_flg]);

        return $dataProvider;
    }
}
