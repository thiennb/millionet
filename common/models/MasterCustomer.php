<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii2tech\ar\softdelete\SoftDeleteBehavior;
use yii\behaviors\BlameableBehavior;
use common\components\Util;
use common\models\MasterStore;
use common\components\Constants;
use common\components\RegisterFromBehavior;
use common\components\CompanyIdBehavior;

/**
 * This is the model class for table "mst_customer".
 *
 * @property integer $id
 * @property string $first_name
 * @property string $last_name
 * @property string $first_name_kana
 * @property string $last_name_kana
 * @property string $name_kana
 * @property string $birth_date
 * @property integer $sex
 * @property string $address
 * @property string $post_code
 * @property string $tel
 * @property string $mobile
 * @property string $email
 * @property string $memo
 * @property integer $point
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $del_flg
 * @property integer $number_visit
 * @property integer $last_visit_date
 * @property integer $first_visit_date
 * @property double $total_amount
 * @property integer $reservation_number
 * @property integer $tickets_balance
 * @property integer $charge_balance
 * @property string $black_list_flg
 * @property string $news_transmision_flg
 * @property interger $last_staff_id
 * @property interger $register_store_id
 * @property interger $register_staff_id
 * @property string $register_kbn 
 */
class MasterCustomer extends \yii\db\ActiveRecord {

    private $_attributes = [];
    public $process_date;
    public $number_visit;
    public $last_visit_st;
    public $id_store;
    public $store_name;
    public $news_transmision_flg;
    public $product_id;
    public $quantity;
    public $last_visit_date;
    public $rank_id;
    public $id_hd_customer;
    public $order_code_customer;
    public $total_point_history;
    public $total_money_history;
    public $total_num_visit;

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'mst_customer';
    }

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            TimestampBehavior::className(),
            BlameableBehavior::className(),
            CompanyIdBehavior::className(),
            'softDeleteBehavior' => [
                'class' => SoftDeleteBehavior::className(),
                'softDeleteAttributeValues' => [
                    'del_flg' => true
                ],
            ],
            RegisterFromBehavior::className()
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['sex', 'birth_date', 'product_id', 'quantity', 'first_name', 'last_name', 'first_name_kana', 'last_name_kana', 'visitCondition', 'lastVisitDateCondition', 'rank_id',
            'last_visit_st', 'news_transmision_flg', 'last_time_staff', 'number_visit_min', 'number_visit_max', 'last_staff_id', 'register_store_id', 'register_staff_id', 'number_visit', 'last_visit_date', 'first_visit_date', 'tickets_balance', 'charge_balance', 'id_store', 'store_name', 'process_date', 'id', 'id_hd_customer', 'address2', 'customer_jan_code','register_kbn'], 'safe'],
            [['sex', 'created_at', 'updated_at'], 'integer'],
            [['memo'], 'string', 'max' => 500],
            [['first_name', 'last_name', 'first_name_kana', 'last_name_kana', 'mobile'], 'required', 'except' => 'giftinsert'],
            [['first_name', 'last_name', 'first_name_kana', 'last_name_kana'], 'string', 'max' => 40],
            [['customer_jan_code'], 'string', 'max' => 13],
            [['address', 'address2'], 'string', 'max' => 240],
            [['post_code'], 'string', 'length' => 7, 'min' => 7],
            [['tel', 'mobile','birth_date'], 'string', 'max' => 17],
            [['birth_date'], 'string', 'max' => 10],
            [['tel', 'mobile'], 'double', 'message' => Yii::t('backend', '{attribute} phone is not valid')],
            [['email'], 'string', 'max' => 60],
            [['email'], 'checkEmail'],
            [['birth_date', 'last_visit_date'], 'date', 'format' => 'php:Y/m/d'],
            [['del_flg'], 'string', 'max' => 1],
            [['point', 'number_visit', 'user_id'], 'default', 'value' => 0],
            //[ 'customer_jan_code', 'match', 'not' => true, 'pattern' => '/[^a-zA-Z0-9]/', 'message' => Yii::t('backend', '{attribute} invalid characters membership')],
            [['first_name', 'last_name'], 'match', 'not' => true, 'pattern' => '/[\'\/~`\!@#\$%\^&\*\(\)_\-\+=\{\}\[\]\|;:"\<\>,\.\?\\\]/', 'message' => Yii::t('backend', '{attribute} invalid characters')],
            [['first_name_kana', 'last_name_kana'], 'match', 'pattern' => '/^[ァ-ヴー]+$/u', 'message' => Yii::t('backend', '{attribute} invalid characters full size only')],
            // Check Exit : Phone , Email
//            [['mobile'], 'checkPhoneExit'],
//            [['email'], 'checkEmailExit'],
            [['email', 'mobile', 'customer_jan_code'], 'unique',
                'targetClass' => 'common\models\MasterCustomer',
                'when' => function ($model, $attribute) {
                return $model->{$attribute} !== $model->getOldAttribute($attribute);
            },
            ],
            [['customer_jan_code'], 'checkJanCodeFormat'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => Yii::t('backend', 'ID'),
            'name' => Yii::t('backend', 'Name'),
            'name_kana' => Yii::t('backend', 'Name kana'),
            'birth_date' => Yii::t('backend', 'Birthday'),
            'memorial_date' => Yii::t('backend', 'Memorial Date'),
            'sex' => Yii::t('backend', 'Sex'),
            'address' => Yii::t('backend', 'Address1'),
            'address2' => Yii::t("backend", "Address2"),
            'post_code' => Yii::t('backend', 'PostCode'),
            'tel' => Yii::t('backend', 'Tel'),
            'mobile' => Yii::t('backend', 'Phone'),
            'email' => Yii::t('backend', 'Email'),
            'memo' => Yii::t('backend', 'Memo'),
            'point' => Yii::t('backend', 'Point'),
            'created_at' => Yii::t('backend', 'Created At'),
            'updated_at' => Yii::t('backend', 'Updated At'),
            'del_flg' => Yii::t('backend', 'Del Flg'),
            'number_visit' => Yii::t('backend', 'Visit number'),
            'last_visit_date' => Yii::t('backend', 'Last visit date'),
            'first_visit_date' => Yii::t('backend', 'First visit date'),
            'total_amount' => Yii::t('backend', 'Total Amount'),
            'reservation_number' => Yii::t('backend', 'Reservation number'),
            'tickets_balance' => Yii::t('backend', 'Tickets balance'),
            'charge_balance' => Yii::t('backend', 'Charge balance'),
            'rank' => Yii::t('backend', 'Customers rank'),
            'first_name' => Yii::t('backend', "First name"),
            'last_name' => Yii::t('backend', "Last name"),
            'first_name_kana' => Yii::t('backend', "First name kana"),
            'last_name_kana' => Yii::t('backend', "Last name kana"),
            'customer_jan_code' => Yii::t("backend", "Membership card number"),
            'last_staff_id' => Yii::t("backend", "Last time staff"),
            'number_visit_min' => Yii::t('backend', 'Visit number'),
            'number_visit_max' => Yii::t('backend', 'Visit number'),
            'birth_date_from' => Yii::t("backend", "Birthday"),
            'birth_date_to' => Yii::t("backend", "Birthday"),
            'last_visit_from' => Yii::t("backend", "Last visit time"),
            'last_visit_to' => Yii::t("backend", "Last visit time"),
            'register_store_id' => Yii::t("backend", "Register store"),
            'register_staff_id' => Yii::t("backend", "Register staff"),
            'store_name' => Yii::t("backend", "Store Id"),
            'product_id' => Yii::t("backend", "Name"),
            'quantity' => Yii::t("backend", "Quantity"),
            'date_history_from' => Yii::t("backend", "Date History"),
            'date_history_to' => Yii::t("backend", "Date History"),
            'total_point_from' => Yii::t("backend", 'Total Point'),
            'total_point_to' => Yii::t("backend", 'Total Point'),
            'number_visit_min' => Yii::t("backend", 'Visit number'),
            'number_visit_max' => Yii::t("backend", 'Visit number'),
            'ticket_name' => Yii::t("backend", 'Name Ticket'),
            'product_name' => Yii::t("backend", 'Product Name'),
            'store_id' => Yii::t('backend', 'Store Go'),
            'customer_name' => Yii::t('backend', 'Name Customer'),
            'visit_date_to' => Yii::t('backend', 'Date Go'),
            'visit_date_from' => Yii::t('backend', 'Date Go'),
            'order_code' => Yii::t('backend', 'Order Code'),
            'last_staff_id' => Yii::t('backend', "Last time staff"),
            'seat_id' => Yii::t('backend', "Seat Use"),
            'list_category' => Yii::t('backend', 'Product Category'),
        ];
    }

    public static function find($findAll = true) {
        if ($findAll)
            if(\Yii::$app->controllerNamespace != "api\controllers"){
                return parent::find()->where(['or', ['mst_customer.del_flg' => '0'], ['mst_customer.del_flg' => null]])->andWhere(['mst_customer.company_id' => Util::getCookiesCompanyId()]);
            }else{
                return parent::find()->where(['or', ['mst_customer.del_flg' => '0'], ['mst_customer.del_flg' => null]]);
            }
        else
            return parent::find();
    }
    
    public static function findFrontEnd() {
        return parent::find()->where(['or', ['mst_customer.del_flg' => '0'], ['mst_customer.del_flg' => null]]);
    }
    
    public static function findByStore() {
        // permission HoangNQ start
        $query = self::find()->innerJoin('customer_store', 'customer_store.customer_id = mst_customer.id')
                ->andWhere(['or', ['mst_customer.del_flg' => '0'], ['mst_customer.del_flg' => null]])
                ->andWhere(['mst_customer.company_id' => Util::getCookiesCompanyId()]);
        $store_id = \Yii::$app->user->identity->store_id;
        $role = \common\components\FindPermission::getRole();
        if($role->permission_id == Constants::STORE_MANAGER_ROLE || $role->permission_id == Constants::STAFF_ROLE){
            $query->andWhere(['customer_store.store_id' => $store_id]);
        }             
        // permission HoangNQ start
        return $query;
        
    }

    public function beforeSave($insert) {
        if (parent::beforeSave($insert)) {

            if ($this->customer_jan_code == null || empty($this->customer_jan_code)) {
                if ($this->isNewRecord) {
                    $this->customer_jan_code = Util::genJanCodeAuto(\common\components\Constants::JAN_TYPE_CUSTORMER);
                }
            }
            return true;
        } else {
            return false;
        }
    }

    public function attributes() {
        return array_merge(parent::attributes(), array_keys($this->_attributes));
    }

    public function __get($name) {
        if (array_key_exists($name, $this->_attributes))
            return $this->_attributes[$name];

        try {
            return parent::__get($name);
        } catch (\Exception $e) {
            return false;
        }
    }

    public function __set($name, $value) {
        try {
            parent::__set($name, $value);
        } catch (\Exception $e) {
            $this->_attributes[$name] = $value;
        }
    }
    public function getName(){
     
      return $this->first_name . $this->last_name;
    }

    /**
     * Get rank
     */
    public function getCustomerPoint() {
//        return $this->hasMany(CustomerPoint::className(), ['customer_id' => 'id']);
        //hard code one shop
        return $this->hasOne(CustomerStore::className(), ['customer_id' => 'id']);
    }

    public function getPointHistory() {
//        return $this->hasMany(CustomerPoint::className(), ['customer_id' => 'id']);
        //hard code one shop
        return $this->hasOne(PointHistory::className(), ['customer_id' => 'id']);
    }

    /**
     * Get Store register
     */
    public function getStore() {
//        return $this->hasMany(CustomerPoint::className(), ['customer_id' => 'id']);
        //hard code one shop
        return $this->hasOne(MasterStore::className(), ['id' => 'register_store_id']);
    }

    /**
     * Get image black list 
     */
    public function getImageBlackList() {
        $model = CustomerStore::find()->andWhere(['customer_id'=> $this->id , 'store_id' => $this->register_store_id])->one();
        if(count($model) == 1 && $model->black_list_flg == "1"){
          return '<i class="fa fa-exclamation-triangle fa-2x"  style="color:red" aria-hidden="true"></i>';
        }else{
          return '';
        }
    }

    /**
     * Check email
     * @param string $attribute, $params
     * @return error 
     * @throws 
     */
    public function checkEmail($attribute, $params) {

        $check = preg_match(
           '/^[A-z]+[.A-z0-9_\-]+[@][A-z0-9_\-]+([.][A-z0-9_\-]+)+[A-z.]{2,4}$/', $this->email    
        );

        //check valid email
        if (!$check) {
            $key = $attribute;
            //add message error
            $this->addError($key, Yii::t('backend', "email not valid"));
        }
        
    }

    /**
     * Check can delete custormer
     * 
     * @return bool 
     * @throws 
     */
    public static function checkDelete($id) {
        $check = 0;
        $check = Booking::find()->andWhere(['customer_id' => $id])->count();
        $check += MasterGiftReceiver::find()->andWhere(['customer_id' => $id])->count();
        $check += MasterGift::find()->andWhere(['customer_id' => $id])->count();
        $check += NoticeCustomer::find()->andWhere(['customer_id' => $id])->count();
        //$check += CustomerPoint::find()->andWhere(['customer_id'=>$id])->count();
        $check += ChargeHistory::find()->andWhere(['customer_id' => $id])->count();
        $check += PointHistory::find()->andWhere(['customer_id' => $id])->count();

        return ($check > 0) ? FALSE : TRUE;
    }

    public function getCustomer() {
        return $this->hasOne(MasterStaff::className(), ['id' => 'register_staff_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomerStore() {
        return $this->hasMany(CustomerStore::className(), ['customer_id' => 'id']);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder() {
        return $this->hasMany(MstOrder::className(), ['customer_jan_code' => 'customer_jan_code']);
    }

    /**
     * Finds the MasterCustomer model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return MasterCustomer the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function findModel($id) {
        if (($model = CustomerStore::findOne($id)) !== null) {
            return $model;
        } else {
            throw new \yii\web\NotFoundHttpException('The requested page does not exist.');
        }
    }

    public static function findModelCustomer($id) {
        if (($model = MasterCustomer::findOne($id)) !== null) {
            return $model;
        } else {
            throw new \yii\web\NotFoundHttpException('The requested page does not exist.');
        }
    }

    // ======================
    // == Check Phone start
    // ======================
    public function checkPhoneExit($attribute, $params) {
        if (!empty($this->mobile)) {
            $check = 0;
            $check = MasterCustomer::find()->andWhere(['mobile' => $this->mobile])->count();
            if ($check > 0) {
                $key = $attribute;
                $this->addError($key, Yii::t('backend', "Mobile Exit"));
            }
        }
    }

    // ======================
    // == Check Phone end
    // ======================    
    // ======================
    // == Check Email start
    // ======================
    public function checkEmailExit($attribute, $params) {
        if (!empty($this->email)) {
            $check = 0;
            $check = MasterCustomer::find()->andWhere(['email' => $this->email])->count();
            if ($check > 0) {
                $key = $attribute;
                $this->addError($key, Yii::t('backend', "Email Exit"));
            }
        }
    }

    // ======================
    // == Check Email end
    // ======================
    // ======================
    // == Check JANCODE start
    // ======================
    public function checkJanCodeFormat($attribute, $params) {
        
        $check_jan_code = Util::checkJanCodeCustomer(Constants::JAN_TYPE_CUSTORMER,$this->customer_jan_code);
        
        if (!$check_jan_code || strlen($this->customer_jan_code) < 13) {
            $key = $attribute;
            $this->addError($key, Yii::t('backend', "JAN_Code invalid, please try again."));
        }
             
    }

    // ======================
    // == Check JANCODE end
    // ======================
    
    // Select count customer
    public function selectCountCustomer() {
            
        $count_customer = self::find()->count();
        
        return $count_customer;
    } 
    
    public function getUser()
    {
      return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
    
    public function getRankInfoInMonth($customerId = null, $storeId = null){

        $firstDate = (new \DateTime('first day of this month'))->format('Y-m-d');
        $store = MasterStore::StoreDetail($storeId)->one();
        $customer = MasterCustomer::find()->where(['id'=>$customerId, 'del_flg'=>'0'])->one();
        
        $totalPoint = 0; $totalMoney = 0; $totalNumVisit = 0;
        
        if($customer !== null && $store !== null){
            $pointHistory = PointHistory::find()
                    ->where(['BETWEEN','process_date', $firstDate, date('Y-m-d')])
                    ->andWhere(['customer_id'=>$customerId])
                    ->andWhere(['del_flg'=>'0'])
                    ->andWhere(['store_code'=>$store['store_code']])->all();

            foreach($pointHistory as $point){
                if($point['process_type'] == '0' || $point['process_type'] == '2'){
                    $totalPoint += $point['process_point'];
                }

                if($point['process_type'] == '1' || $point['process_type'] == '3'){
                    $totalPoint -= $point['process_point'];
                }
            }

            $orders = MstOrder::find()
                    ->where(['BETWEEN','process_date', $firstDate, date('Y-m-d')])
                    ->andWhere(['customer_jan_code'=>$customer['customer_jan_code']])
                    ->andWhere(['del_flg'=>'0'])
                    ->andWhere(['left(order_code,5)'=>$store['store_code']])->all();

            foreach($orders as $order){
                $totalMoney += $order['total'];
            }

            $totalNumVisit = count(MstOrder::find()
                    ->where(['BETWEEN','process_date', $firstDate, date('Y-m-d')])
                    ->andWhere(['customer_jan_code'=>$customer['customer_jan_code']])
                    ->andWhere(['del_flg'=>'0'])
                    ->andWhere(['left(order_code,5)'=>$store['store_code']])->all());
        }
        return array('total_point'=> $totalPoint, 'total_amount'=> $totalMoney, 'number_visit' => $totalNumVisit);
    }
    
    public function getCustomerDetail($customerId = null){
        $query = $detailOrder = (new \yii\db\Query())->select(['*'])->from('mst_customer');
        $query->where(['id' => $customerId]);
        $query->andWhere(['mst_customer.del_flg' => 0]);
        return $query->one();
    }

    public function getCompanyId($customer_id)
    {
        $customer = self::findOne($customer_id);
//        $query->select([
//            'mst_store.company_id'
//        ]);
//        $query->innerJoin('mst_store', 'mst_store.id = customer_store.store_id') ;
//        $query->andFilterWhere([CustomerStore::tableName() . '.customer_id' => $customer_id]);
//        $data = $query->asArray()->one();
        return $customer->company_id;
    }
  }
