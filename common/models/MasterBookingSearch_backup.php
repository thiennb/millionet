<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Booking;

/**
 * MasterBookingSearch represents the model behind the search form about `common\models\MasterBooking`.
 */
class MasterBookingSearch_backup extends Booking
{
    /**
     * @inheritdoc
     */
    //product_category
    public $list_category, $product_name;
    //table: mst_customer
    public $birth_date_from, $birth_date_to;
    public $customer_first_name, $customer_last_name, $sex, $rank, $membership_card_number, $mobile;
    //table: customer_store
    public $number_visit_min, $number_visit_max;
    public $last_visit_date_from, $last_visit_date_to;
    //table: booking
    public $booking_time_min, $booking_time_max;
    //table:
    public $last_seat_id, $last_staff_id;
    public $seat_type, $service_staff_id;
    
    public function rules() {
        return [
            [['id', 'customer_id', 'booking_time_max', 'booking_time_min', 'last_seat_id', 'seat_type', 'service_staff_id', 'store_id', 'staff_id', 'seat_id', 'status', 'action', 'created_at', 'updated_at'], 'integer'],
            [['memo', 'list_category', 'product_name', 'start_time', 'end_time', 'booking_date', 'demand', 'del_flg'], 'safe'],
            [['booking_date_from', 'booking_date_to', 'birth_date_to', 'birth_date_from', 'last_visit_date_from', 'last_visit_date_to'], 'date', 'format' => 'php:Y/m/d'],
            [['customer_first_name', 'customer_last_name', 'sex', 'rank', 'mobile', 'membership_card_number', 'last_staff_id'], 'safe'],
            [['product_name'], 'string', 'max' => 100],
            [['membership_card_number'], 'string', 'max' => 20],
            [['mobile'], 'string', 'max' => 11],
            [['mobile'], 'integer', 'min'=> 0],
        ];
    }


    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */

   public function search($params) {
        $query = Booking::find();
        // add conditions that should always apply here
        //join booking product
        $query->innerJoin(BookingProduct::tableName(), BookingProduct::tableName() . '.booking_id = ' . Booking::tableName() . '.id');
        $query->innerJoin(MstProduct::tableName(), BookingProduct::tableName() . '.product_id = ' . MstProduct::tableName() . '.id');
        $dataProvider = new ActiveDataProvider([
            'query' => $query->with("storeMaster")->with("bookingProduct")->with("masterCustomer")->with("seat")->with("staff"),
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => false
        ]);
        $this->load($params);
        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        // grid filtering conditions
        if (!empty($this->membership_card_number) || !empty($this->customer_first_name) || !empty($this->customer_last_name) || !empty($this->sex) || !empty($this->birth_date_from) || !empty($this->birth_date_to) || !empty($this->mobile)) {
            $query->innerJoin(MasterCustomer::tableName(), MasterCustomer::tableName() . '.id = ' . Booking::tableName() . '.customer_id');
            $query->andWhere(['mst_customer.company_id' => \common\components\Util::getCookiesCompanyId()]);
            if (!empty($this->membership_card_number))
                $query->andFilterWhere(['=', MasterCustomer::tableName() . '.membership_card_number', $this->membership_card_number]);
            if (!empty($this->customer_first_name)) {
                $query->andFilterWhere(['or', ['like', MasterCustomer::tableName() . '.first_name', $this->customer_first_name], ['like', MasterCustomer::tableName() . '.first_name_kana', $this->customer_first_name]]);
            }
            if (!empty($this->customer__last_name)) {
                $query->andFilterWhere(['or', ['like', MasterCustomer::tableName() . '.last_name', $this->customer_last_name], ['like', MasterCustomer::tableName() . '.last_name_kana', $this->customer_last_name]]);
            }
            if (!empty($this->sex))
                $query->andFilterWhere([MasterCustomer::tableName() . '.sex' => $this->sex]);
            if (!empty($this->mobile))
                $query->andFilterWhere([MasterCustomer::tableName() . '.mobile' => $this->mobile]);

            if (!empty($this->birth_date_from))
                $query->andFilterWhere(['>=', MasterCustomer::tableName() . '.birth_date', $this->birth_date_from]);
            if (!empty($this->birth_date_to))
                $query->andFilterWhere(['<=', MasterCustomer::tableName() . '.birth_date', $this->birth_date_to]);
        }
        if (!empty($this->product_name)) {
            if (!empty($this->product_name))
                $query->andFilterWhere(['or', ['like', MstProduct::tableName() . '.name', $this->product_name], ['like', MstProduct::tableName() . '.name_kana', $this->product_name]]);
            
        }
        if (!empty($this->list_category)) {
            foreach ($this->list_category as $cate_id) {
                $product = MstProduct::find()->where([MstProduct::tableName() . '.category_id' => $cate_id])->select('id');
                $count_booking_id = BookingProduct::find()->select("booking_id")->where(['product_id' => $product])->groupBy("booking_id")->having([">", "COUNT(booking_id)", 0]);
                $query->andFilterWhere(['booking_id' => $count_booking_id]);
            }
        }
        if (!empty($this->rank) || !empty($this->last_visit_date_from) || !empty($this->last_visit_date_to) || !empty($this->last_staff_id)) {
            $query->innerJoin(CustomerStore::tableName(), CustomerStore::tableName() . '.customer_id = ' . Booking::tableName() . '.customer_id' . ' AND ' . CustomerStore::tableName() . '.store_id = ' . Booking::tableName() . '.store_id');
            if (!empty($this->rank))
                $query->andFilterWhere(['=', CustomerStore::tableName() . '.rank_id', $this->rank]);
            if (!empty($this->last_visit_date_from))
                $query->andFilterWhere(['>=', CustomerStore::tableName() . '.last_visit_date', $this->last_visit_date_from]);
            if (!empty($this->last_visit_date_to))
                $query->andFilterWhere(['<=', CustomerStore::tableName() . '.last_visit_date', $this->last_visit_date_to]);
        }
        if (!empty($this->booking_date_from))
            $query->andFilterWhere(['>=', 'booking_date', $this->booking_date_from]);
        if (!empty($this->booking_date_to))
            $query->andFilterWhere(['<=', 'booking_date', $this->booking_date_to]);
        if (!empty($this->store_id))
            $query->andFilterWhere(['=', Booking::tableName() . '.store_id', $this->store_id]);
        if (!empty($this->status))
            $query->andFilterWhere(['=', Booking::tableName() . '.status', $this->status]);
        if (!empty($this->booking_time_min))
            $query->andFilterWhere(['>=', '(select COUNT(*) FROM booking as book1 where booking.customer_id = book1.customer_id)', $this->booking_time_min]);
        if (!empty($this->booking_time_max))
            $query->andFilterWhere(['<=', '(select COUNT(*) FROM booking as book1 where booking.customer_id = book1.customer_id)', $this->booking_time_max]);
        return $dataProvider;
    }
}
