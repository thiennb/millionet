<?php

namespace common\models;

use Yii;
use yii\helpers\ArrayHelper;
use Carbon\Carbon;

/**
 * This is the model class for table "booking".
 *
 * @property integer $id
 * @property string $booking_code
 * @property integer $customer_id
 * @property integer $store_id
 * @property string $booking_date
 * @property string $memo
 * @property string $memo_manager
 * @property integer $staff_id
 * @property integer $seat_id
 * @property string $start_time
 * @property string $end_time
 * @property string $status
 * @property string $action
 * @property string $demand
 * @property integer $number_of_people
 * @property string $booking_price
 * @property string $booking_discount
 * @property string $booking_total
 * @property string $point_use
 * @property string $booking_method
 * @property string $to_home_delivery_flg
 * @property string $del_flg
 * @property integer $created_at
 * @property integer $created_by
 * @property integer $updated_at
 * @property integer $updated_by
 */
class ApiFormBooking extends \yii\db\ActiveRecord {

    public $storeId;
    public $products;
    public $coupons;
    public $options;
    public $staff;
    public $seat;
    public $date;
    public $capacity;
    static $mode = 'input';

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'booking';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['capacity', 'seat'], 'required', 'on' => ['restaurant']],
            [['storeId', 'products', 'coupons', 'options', 'staff', 'seat', 'date', 'capacity'], 'safe'],
            ['storeId', 'validateInput']
        ];
        /* return [
          [['booking_code', 'customer_id', 'store_id', 'created_at', 'created_by', 'updated_at', 'updated_by'], 'required'],
          [['customer_id', 'store_id', 'staff_id', 'seat_id', 'number_of_people', 'created_at', 'created_by', 'updated_at', 'updated_by'], 'integer'],
          [['booking_date'], 'safe'],
          [['booking_price', 'booking_discount', 'booking_total', 'point_use'], 'number'],
          [['booking_code'], 'string', 'max' => 10],
          [['memo', 'memo_manager'], 'string', 'max' => 500],
          [['start_time', 'end_time'], 'string', 'max' => 5],
          [['status', 'action', 'booking_method'], 'string', 'max' => 2],
          [['demand'], 'string', 'max' => 240],
          [['to_home_delivery_flg', 'del_flg'], 'string', 'max' => 1],
          ]; */
    }

    public function validateInput($attribute, $params) {
        // check coupons and products format
        if ((!empty($this->coupons) && !preg_match('/^\d+(,\d+){0,}$/', $this->coupons)) || (!empty($this->products) && !preg_match('/^\d+(,\d+){0,}$/', $this->products)) || (!empty($this->options) && !preg_match("/^\d+\-\d+\-\d+(,\d+\-\d+\-\d+){0,}$/", $this->options))) {
            $this->addError($attribute, Yii::t('frontend', 'Invalid coupon(s) or product(s)'));
        }

        $product_ids = array_filter(explode(',', $this->products));
        // DUE TO COUPON_PRODUCT_ID
        $option_list = array_filter(explode(',', $this->options));
        $option_count = count($option_list);
        if ($option_count > 0) {
            foreach ($option_list as $option) {
                $explode = explode('-', $option);
                $product_id = $explode[1];
                $option_id = $explode[0];
                $search_by = $explode[2];

                $product_ids[] = $product_id;
            }
        }

        $total_time = 0;

        $coupons = BookingSearch::searchCouponsById($this->coupons, $this->storeId);
        $products = BookingSearch::searchProductsById(implode(',', $product_ids), $this->storeId);

        if (empty($coupons) && empty($products)) {
            $this->addError($attribute, Yii::t('frontend', 'Invalid coupon(s) or product(s)'));
        } else {
            $total_time += array_sum(ArrayHelper::getColumn($coupons, 'time_require'));
            $total_time += array_sum(ArrayHelper::getColumn($products, 'time_require'));

            if (empty($products)) {
                $benefits = ArrayHelper::getColumn($coupons, 'benefits_content');

                if (array_search('02', $benefits) === false && array_search('03', $benefits) === false) {
                    $this->addError($attribute, Yii::t('frontend', 'Please select at least one product or set coupon'));
                }
            }

            $combinations = ArrayHelper::getColumn($coupons, 'combine_with_other_coupon_flg');

            if (array_search('1', $combinations) !== false && (array_search('', $combinations) !== false || array_search('0', $combinations) !== false)) {
                $this->addError($attribute, Yii::t('frontend', 'These coupons can not be chosen with each other'));
            }
        }

        $number_row = 60 * $total_time / 1800;

        $execute_time = $number_row * 1800 / 60;

        $hours = floor($execute_time / 60);
        $minutes = $execute_time % 60;

        if ($hours < 10) {
            $hours = '0' . $hours;
        }

        if ($minutes < 10) {
            $minutes = '0' . $minutes;
        }

        $execute_time = sprintf('%s:%s:00', (string) $hours, (string) $minutes);

        $date = Carbon::createFromTimestamp($this->date);
        $ymd = $date->format('Y-m-d');
        $hi = $date->format('H:i');
        $date_time = $ymd . ' ' . $hi . ':00';

        $store = MasterStore::find()->where(['id' => $this->storeId])->one();

        if (!$store) {
            return false;
        }

        $invalid_store_schedule = !BookingBusiness::checkStoreSchedule($store->id, $ymd, $hi, $number_row * 1800 / 60);

        //====== SALON =========
        if ($store['booking_resources'] === '01') {
            if ($invalid_store_schedule) {
                $this->addError($attribute, '座席数が不足しています');
            }

            /* @var $type_seat \common\models\MasterTypeSeat */
            if ($this->seat) {
                $type_seats = MasterTypeSeat::findFrontEnd()
                        ->where(['id' => $this->seat])
                        ->all();
            } else {
                $type_seats = MasterTypeSeat::findFrontEnd()
                        ->where(['store_id' => $this->storeId])
                        ->all();
            }
            $found_seat = false;
            foreach ($type_seats as $type_seat) {
                $shift = $type_seat->getDetailedSchedule([
                    'from_date' => $ymd,
                    'to_date' => $ymd,
                    'from_time' => $hi,
                    'to_time' => $hi
                ]);

                if (isset($shift[$date_time])) {
                    $capacity_min = $shift[$date_time]['capacity_min'];
                    $capacity_max = $shift[$date_time]['capacity_max'];
                } else {
                    $capacity_min = $type_seat['capacity_min'];
                    $capacity_max = $type_seat['capacity_max'];
                }

                if ($this->capacity >= $capacity_min && $this->capacity <= $capacity_max) {
                    $found_seat = true;
                }
            }

            $search_date_time = $date->format('Y-m-d') . ' ' . $date->format('H:i:00');

            if (!$found_seat) {
                $this->addError($attribute, '座席数が不足しています');
            }
        } else {
            if ($invalid_store_schedule) {
                $this->addError($attribute, Yii::t('frontend', 'Invalid staff or schedule requested'));
            }

            $found_staff = false;

            $staff_list = BookingSearch::searchStaffByDateTime($this->storeId, $number_row, $execute_time, $date->format('Y-m-d'), $date->format('Y-m-d'), $store->reservations_possible_time, $date->format('H:i:00'));

            if ($this->staff != 0) {
                foreach ($staff_list as $staff) {
                    if ($staff['id'] == $this->staff) {
                        $found_staff = true;
                    }
                }
            } elseif (count($staff_list) > 0) {
                $found_staff = true;
            }

            if (!$found_staff) {
                $this->addError($attribute, Yii::t('frontend', 'Invalid staff or schedule requested'));
            }
        }

        return true;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'booking_code' => 'Booking Code',
            'customer_id' => 'Customer ID',
            'store_id' => 'Store ID',
            'booking_date' => 'Booking Date',
            'memo' => 'Memo',
            'memo_manager' => 'Memo Manager',
            'staff_id' => 'Staff ID',
            'seat_id' => 'Seat ID',
            'start_time' => 'Start Time',
            'end_time' => 'End Time',
            'status' => 'Status',
            'action' => 'Action',
            'demand' => 'Demand',
            'number_of_people' => 'Number Of People',
            'booking_price' => 'Booking Price',
            'booking_discount' => 'Booking Discount',
            'booking_total' => 'Booking Total',
            'point_use' => 'Point Use',
            'booking_method' => 'Booking Method',
            'to_home_delivery_flg' => 'To Home Delivery Flg',
            'del_flg' => 'Del Flg',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
            'capacity' => Yii::t('frontend', 'Capacity'),
        ];
    }

}
