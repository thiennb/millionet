<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use yii2tech\ar\softdelete\SoftDeleteBehavior;

/**
 * This is the model class for table "tax_detail".
 *
 * @property integer $id
 * @property integer $tax_id
 * @property string $rate
 * @property string $start_date
 * @property string $end_date
 * @property string $del_flg
 * @property integer $created_at
 * @property integer $created_by
 * @property integer $updated_at
 * @property integer $updated_by
 * @property integer $reduced_tax_flg
 */
class TaxDetail extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tax_detail';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),            
            BlameableBehavior::className(),
            'softDeleteBehavior' => [
                'class' => SoftDeleteBehavior::className(),
                'softDeleteAttributeValues' => [
                    'del_flg' => '1'
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tax_id', 'created_at', 'created_by', 'updated_at', 'updated_by'], 'integer'],
            [['rate'], 'number'],
            [['start_date', 'end_date'], 'required'],
            [['start_date', 'end_date','reduced_tax_flg'], 'safe'],
            [['del_flg'], 'string', 'max' => 1],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'tax_id' => Yii::t('app', 'Tax ID'),
            'rate' => Yii::t('app', 'Rate'),
            'start_date' => Yii::t('app', 'Start Date'),
            'end_date' => Yii::t('app', 'End Date'),
            'del_flg' => Yii::t('app', 'Del Flg'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'reduced_tax_flg' => Yii::t('app', 'Reduced Tax Flag'),
        ];
    }
            
    public static function find()
    {
        return parent::find()->where(['tax_detail.del_flg'=>'0']);
    }
}
