<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use yii2tech\ar\softdelete\SoftDeleteBehavior;
use common\components\Util;
/**
 * This is the model class for table "point_setting".
 *
 * @property integer $id
 * @property string $grant_yen_per_point
 * @property string $point_conversion_p
 * @property string $point_conversion_yen
 * @property string $grant_point_visit_flg
 * @property string $del_flg
 * @property integer $created_at
 * @property integer $created_by
 * @property integer $updated_at
 * @property integer $updated_by
 */
class PointSetting extends \yii\db\ActiveRecord {

     /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            TimestampBehavior::className(),
            BlameableBehavior::className(),
            'softDeleteBehavior' => [
                'class' => SoftDeleteBehavior::className(),
                'softDeleteAttributeValues' => [
                    'del_flg' => '1'
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'point_setting';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['grant_yen_per_point', 'point_conversion_yen'], 'number'],
            [['created_at', 'created_by', 'updated_at', 'updated_by','company_id'], 'safe'],
            [['created_at', 'created_by', 'updated_at', 'updated_by'], 'integer'],
            [['grant_point_visit_flg'], 'required'],
            [['grant_point_visit_flg', 'del_flg'], 'string', 'max' => 1],
            [['point_conversion_p'], 'checkPoint'],
            [['grant_yen_per_point'], 'checkPerPoint'],
            [['point_conversion_yen'], 'checkConversionPoint'],
            [['grant_yen_per_point'], 'required', 'message' => Yii::t('backend', 'The purchase amount will not be blank')],
            [['point_conversion_p'], 'required', 'message' => Yii::t('backend', 'Point can not be empty.')],
            [['point_conversion_yen'], 'required', 'message' => Yii::t('backend', 'Circle can not be empty.')],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'grant_yen_per_point' => Yii::t('backend', 'Grant Yen Per Point'),
            'point_conversion_p' => Yii::t('backend', 'Point Conversion P'),
            'point_conversion_yen' => Yii::t('backend', 'Point Conversion Yen'),
            'grant_point_visit_flg' => Yii::t('backend', 'Grant Point Visit Flg'),
            'del_flg' => Yii::t('backend', 'Del Flg'),
            'created_at' => Yii::t('backend', 'Created At'),
            'created_by' => Yii::t('backend', 'Created By'),
            'updated_at' => Yii::t('backend', 'Updated At'),
            'updated_by' => Yii::t('backend', 'Updated By'),
        ];
    }

    public static function find() {
        return parent::find(['del_flg' => 0]);
    }

    public static function getPointSetting($company_id = false) {
        if($company_id === false) {
            $company_id =  Util::getCookiesCompanyId();
        }
        
        return PointSetting::find()->andFilterWhere([self::tableName() . '.del_flg' => '0', self::tableName() . '.company_id'=>$company_id])->one();
    }

    // **************************************** //
    // Check Point
    // **************************************** //
    public function checkPoint($attribute, $params) {
        $point = $this->point_conversion_p;
        if ($point > 9999999.999) {
            $key = $attribute;
            $this->addError($key, Yii::t('backend', "Please enter only from 0 to 9999999.999 point setting"));
        }
        if ($point < 0 || $point == 0) {
            $key = $attribute;
            $this->addError($key, Yii::t('backend', "Discount Poin of the format is not correct."));
        }
    }

    public function checkPerPoint($attribute, $params) {
        $per_point = $this->grant_yen_per_point;
        if ($per_point < 0 || $per_point == 0) {
            $key = $attribute;
            $this->addError($key, Yii::t('backend', "Please enter greater than 0 cyclotomic purchase."));
        }
    }

    public function checkConversionPoint($attribute, $params) {
        $conversion_point = $this->point_conversion_yen;
        if ($conversion_point < 0 || $conversion_point == 0) {
            $key = $attribute;
            $this->addError($key, Yii::t('backend', "Circle, please input greater than 0."));
        }
    }

    // **************************************** //
    // Check Point
    // **************************************** //
}
