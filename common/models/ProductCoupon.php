<?php

namespace common\models;
use common\models\MstProduct;
use common\models\ProductOption;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use yii2tech\ar\softdelete\SoftDeleteBehavior;
use Yii;

/**
 * This is the model class for table "product_coupon".
 *
 * @property integer $id
 * @property integer $coupon_id
 * @property integer $product_id
 * @property string $del_flg
 * @property integer $created_at
 * @property integer $created_by
 * @property integer $updated_at
 * @property integer $updated_by
 */
class ProductCoupon extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product_coupon';
    }
    
    
    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            TimestampBehavior::className(),
            BlameableBehavior::className(),
            'softDeleteBehavior' => [
                'class' => SoftDeleteBehavior::className(),
                'softDeleteAttributeValues' => [
                    'del_flg' => '1'
                ],
            ],
        ];
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['coupon_id', 'product_id'], 'required'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend', 'ID'),
            'coupon_id' => Yii::t('backend', 'Coupon ID'),
            'product_id' => Yii::t('backend', 'Product ID'),
            'del_flg' => Yii::t('backend', 'Del Flg'),
            'created_at' => Yii::t('backend', 'Created At'),
            'created_by' => Yii::t('backend', 'Created By'),
            'updated_at' => Yii::t('backend', 'Updated At'),
            'updated_by' => Yii::t('backend', 'Updated By'),
        ];
    }
    
    /**
     * @inheritdoc
     */
    public function getProduct()
    {
        return $this->hasMany(MstProduct::className(), ['id' => 'product_id']);
    }
    
    /**
     * @get option by product
     */
    public function getOption()
    {
        return $this->hasMany(ProductOption::className(), ['product_id' => 'product_id']);
    }
    
    /**
     * @get all product in coupon
     */
//    public function getAllProduct()
//    {
//        return $this->hasMany(MstProduct::className(), ['id' => 'product_id']);
//    }
    
     public static function find()
    {
        return parent::find()->where(['product_coupon.del_flg' => '0']);
    }
    
}