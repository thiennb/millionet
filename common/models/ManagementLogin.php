<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use yii2tech\ar\softdelete\SoftDeleteBehavior;

/**
 * This is the model class for table "management_login".
 *
 * @property integer $id
 * @property string $management_id
 * @property string $auth_key
 * @property string $password_hash
 * @property string $password_reset_token
 * @property integer $status
 * @property string $del_flg
 * @property integer $created_at
 * @property integer $created_by
 * @property integer $updated_at
 * @property integer $updated_by
 */
class ManagementLogin extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'management_login';
    }
    
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),         
            BlameableBehavior::className(),
            'softDeleteBehavior' => [
                'class' => SoftDeleteBehavior::className(),
                'softDeleteAttributeValues' => [
                    'del_flg' => '1'
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['password_hash'], 'required'],
            [['status', 'created_at', 'created_by', 'updated_at', 'updated_by'], 'integer'],
            [['management_id'], 'string', 'max' => 5],
            [['auth_key'], 'string', 'max' => 32],
            [['password_hash', 'password_reset_token'], 'string', 'max' => 255],
            [['del_flg'], 'string', 'max' => 1],
            [['management_id'], 'unique'],
            [['password_reset_token'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'management_id' => Yii::t('app', 'Management ID'),
            'auth_key' => Yii::t('app', 'Auth Key'),
            'password_hash' => Yii::t('app', 'Password Hash'),
            'password_reset_token' => Yii::t('app', 'Password Reset Token'),
            'status' => Yii::t('app', 'Status'),
            'del_flg' => Yii::t('app', 'Del Flg'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'updated_by' => Yii::t('app', 'Updated By'),
        ];
    }
    public function generateManagementId(){
        $code = self::find(false)->max('management_id');
        $code = (!isset($code)||empty($code))?0: $code;
        $code = (int)$code + 1;
        $count = strlen((string)$code);
        switch($count){
            case '1': $code = '0000'.$code; break;
            case '2': $code = '000'.$code; break;
            case '3': $code = '00'.$code; break;
            case '4': $code = '0'.$code; break;
            case '5': break;
       }
       
       return (string)$code;
       
    }

    public static function find($findAll = true)
    {
        if($findAll)
            return parent::find()->where(['management_login.del_flg'=>'0']);
        else
            return parent::find();
    }
    
    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        try {
            return Yii::$app->security->validatePassword($password, $this->password_hash);
        }
        catch(\Exception $e) {
            return false;
        }
    }
    
    public function getStaff() {
        return $this->hasOne(MasterStaff::className(), ['management_login_id' => 'id']);
    }
}
