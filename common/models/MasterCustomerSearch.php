<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\MasterCustomer;
use yii\data\SqlDataProvider;
use common\components\Constants;
use common\components\Util;
use common\models\BookingSearch;

/**
 * MasterCustomerSearch represents the model behind the search form about `common\models\MasterCustomer`.
 *
 * @property string birth_date_from */
class MasterCustomerSearch extends MasterCustomer {

    /**
     * @inheritdoc
     */
    public $name;
    public $birth_date_from;
    public $birth_date_to;
    public $last_visit_from;
    public $last_visit_to;
    public $total_point;
    public $total_point_from;
    public $total_point_to;
    public $process_type;
    public $process_date;
    public $rank;
    public $date_history_from;
    public $date_history_to;
    public $number_visit_max, $number_visit_min;
    public $master_customer, $ticket_name, $product_name, $visit_date_to, $visit_date_from;
    public $order_code, $customer_name;

    public function rules() {
        return [
            [['sex', 'master_customer', 'ticket_name', 'product_name', 'name', 'name_kana', 'birth_date', 'address', 'post_code', 'tel', 'mobile', 'email', 'memo', 'del_flg', 'visitCondition', 'lastVisitDateCondition', 'birth_date_from', 'birth_date_to', 'last_visit_to', 'last_visit_from', 'rank', 'customer_jan_code', 'first_name', 'last_name', 'last_staff_id', 'total_point', 'total_point_from', 'total_point_to', 'point_history', 'process_type', 'date_history_from', 'date_history_to', 'process_date'], 'safe'],
            [['customer_name', 'list_category', 'seat_id', 'order_code', 'store_id'], 'safe'],
            [['customer_jan_code'], 'string', 'max' => 13],
            [['name'], 'string', 'max' => 80],
            [['sex', 'number_visit_min', 'number_visit_max', 'register_store_id', 'rank', 'last_staff_id', 'register_staff_id'], 'safe'],
            [['birth_date_from', 'birth_date_to', 'date_history_from', 'date_history_to', 'visit_date_to', 'visit_date_from', 'last_visit_from', 'last_visit_to'], 'date', 'format' => 'php:Y/m/d'],
            [['total_point_from', 'total_point_to'], 'number', 'max' => 9999999.999, 'min' => 0],
            [['total_point_from', 'total_point_to'], 'string', 'max' => 11],
            [['number_visit_max', 'number_visit_min'], 'integer', 'max' => 99999, 'min' => 0],
            [['number_visit_max', 'number_visit_min'], 'string', 'max' => 6],
            [['product_name'], 'string', 'max' => 100],
            [['birth_date_from', 'birth_date_to', 'last_visit_from', 'last_visit_to'], 'date', 'format' => 'php:Y/m/d'],
            [['number_visit_min'], 'checkNumberVisit'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search() {

        $query = MasterCustomer::findByStore()
//                ->innerJoin('customer_store', 'customer_store.customer_id = mst_customer.id')
                ->leftJoin('mst_store', 'mst_store.id = customer_store.store_id')
                ->leftJoin('mst_order', 'mst_order.customer_jan_code = mst_customer.customer_jan_code');
        // Join : Company store
//                ->innerJoin('company', 'company.id = mst_store.company_id');
//        $company_code = Util::getCookiesCompanyCode();
        $query->andWhere(['mst_customer.company_id' => Util::getCookiesCompanyId()]);

        $query->select([
            'COUNT(mst_order.id) AS number_visit',
            'mst_customer.first_name',
            'mst_customer.last_name',
            'mst_customer.customer_jan_code',
            'mst_store.name as store_name',
            'customer_store.id as id_store',
            //'customer_store.number_visit as number_visit',
            'customer_store.last_visit_date as last_visit_st',
            'customer_store.rank_id',
        ]);

        $query->groupBy([
            'mst_customer.first_name',
            'mst_customer.last_name',
            'mst_customer.customer_jan_code',
            'mst_store.name',
            'customer_store.id',
            'customer_store.number_visit',
            'customer_store.last_visit_date',
            'customer_store.rank_id',
        ]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => false,
        ]);

        $query->orderBy(['(mst_customer.first_name,mst_customer.last_name)' => SORT_ASC]);
        //$this->load($params);
//        var_dump($dataProvider->getModels());

        return $dataProvider;
    }
    
    public function searchGet($params) {
        $query = MasterCustomer::findByStore()
//                ->innerJoin('customer_store', 'customer_store.customer_id = mst_customer.id')
                ->leftJoin('mst_store', 'mst_store.id = customer_store.store_id')
                ->leftJoin('mst_order', 'mst_order.customer_jan_code = mst_customer.customer_jan_code');
        // Join : Company store
//                ->innerJoin('company', 'company.id = mst_store.company_id');
        // add conditions that should always apply here
//        $company_code = Util::getCookiesCompanyCode();
        $query->andWhere(['mst_customer.company_id' => Util::getCookiesCompanyId()]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => false,
        ]);

        $this->load($params);

        if (!$this->validate()) {

            return $dataProvider;
        }

        // grid filtering conditions
        if (!($this->last_staff_id == '')) {

            $command = Yii::$app->getDb()->createCommand(
                        "SELECT 
                           max(ct.order_code) as oder_code,
                           mst_customer.id
                        FROM 
			(SELECT max(mst_order.order_code) as order_code,mst_order.customer_jan_code FROM mst_order GROUP BY mst_order.customer_jan_code)
                        ct
                        INNER JOIN mst_customer
                        ON mst_customer.customer_jan_code = ct.customer_jan_code
                        INNER JOIN customer_store
                        ON customer_store.customer_id = mst_customer.id
                        INNER JOIN mst_order
                        ON ct.customer_jan_code = mst_order.customer_jan_code
                        WHERE
                        customer_store.store_id = :idStore
                        AND
                        mst_order.order_management_id = :idLastStaff
                        AND
                        mst_order.order_code = ct.order_code
                        AND
                        mst_order.del_flg = '0'
                        GROUP BY
                        mst_customer.id
                        "
            );

            $id_store = intval($this->register_store_id);
            $id_last_staff = (new BookingSearch)->converIdLastStaff(intval($this->last_staff_id));

            $command->bindParam(':idStore', $id_store)
                    ->bindParam(':idLastStaff', $id_last_staff);

            $result = $command->queryAll();
            
            if (count($result) > 0) {
                $i = 0;
                $result_seach = null;
                foreach ($result as $value) {
                    $customer_id = $value['id'];
                    if ($i == 0) {
                        $result_seach = $customer_id;
                    } else {
                        $result_seach .= "," . $customer_id;
                    }
                    $i++;
                }
                $array_cover = explode(',', $result_seach);

                $query->andFilterWhere(['mst_customer.id' => $array_cover]);
            } else {
                $query->andFilterWhere(['=', 'mst_customer.created_at', 0]);
            }
        }


        $query->andFilterWhere([
            'mst_customer.sex' => $this->sex,
            'customer_store.store_id' => $this->register_store_id,
            'customer_store.rank_id' => $this->rank,
        ]);
        $query->andFilterWhere(['like', 'mst_customer.customer_jan_code', $this->customer_jan_code])
                ->andFilterWhere([
                    'OR',
                    ['LIKE', "concat(mst_customer.first_name_kana,'',mst_customer.last_name_kana)", $this->name],
                    ['LIKE', "concat(mst_customer.first_name,'',mst_customer.last_name)", $this->name],
                    ['LIKE', "mst_customer.first_name", $this->name],
                    ['LIKE', "mst_customer.last_name", $this->name],
                    ['LIKE', "mst_customer.first_name_kana", $this->name],
                    ['LIKE', "mst_customer.last_name_kana", $this->name],
                ])
                ->andFilterWhere(['>=', 'customer_store.number_visit', $this->number_visit_min])
                ->andFilterWhere(['<=', 'customer_store.number_visit', $this->number_visit_max])
                ->andFilterWhere(['>=', 'mst_customer.birth_date', $this->birth_date_from])
                ->andFilterWhere(['<=', 'mst_customer.birth_date', $this->birth_date_to])
                ->andFilterWhere(['>=', 'customer_store.last_visit_date', $this->last_visit_from])
                ->andFilterWhere(['<=', 'customer_store.last_visit_date', $this->last_visit_to]);

        $query->select([
            'COUNT(mst_order.id) AS number_visit',
            'mst_customer.first_name',
            'mst_customer.last_name',
            'mst_customer.customer_jan_code',
            'mst_store.name as store_name',
            'customer_store.id as id_store',
            //'customer_store.number_visit as number_visit',
            'customer_store.last_visit_date as last_visit_st',
            'customer_store.rank_id',
        ]);

        $query->groupBy([
            'mst_customer.first_name',
            'mst_customer.last_name',
            'mst_customer.customer_jan_code',
            'mst_store.name',
            'customer_store.id',
            'customer_store.number_visit',
            'customer_store.last_visit_date',
            'customer_store.rank_id',
        ]);

        $query->orderBy(['(mst_customer.first_name,mst_customer.last_name)' => SORT_ASC]);
        return $dataProvider;
    }

    public function searchPointHistory($params, $customer_id = null) {
        $query = PointHistory::find();
        $query->joinWith('store', true, 'INNER JOIN');
        $query->joinWith('customerMaster', true, 'INNER JOIN');
        $query->leftJoin('customer_store', "point_history.customer_id = customer_store.customer_id AND mst_store.id = customer_store.store_id AND customer_store.del_flg = '0'");
        // Join : Company store
        $query->innerJoin('company', 'company.id = mst_store.company_id');
        //$cookies = Yii::$app->request->cookies;
        $company_id = Util::getCookiesCompanyId();
        $query->andFilterWhere([
            'company.id' => $company_id,
        ]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => false,
        ]);
        if (isset($customer_id)) {
            $query->andFilterWhere(['point_history.customer_id' => $customer_id]);
            return $dataProvider;
        }
        // add conditions that should always apply here
        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        //担当者
        if ($this->register_staff_id != '') {
            $query->andFilterWhere(['point_history.staff_management_id' => $this->register_staff_id]);
            /*
            $command = Yii::$app->getDb()->createCommand(
                                    "SELECT
					mst_order.order_code,
					mst_order.customer_jan_code,
					mst_customer.id as id_customer
                                    FROM 
                                    point_history
                                    INNER JOIN mst_customer
                                    ON mst_customer.id = point_history.customer_id
                                    INNER JOIN customer_store
                                    ON customer_store.customer_id = mst_customer.id
                                    INNER JOIN mst_order
                                    ON mst_order.customer_jan_code = mst_customer.customer_jan_code
                                    WHERE
                                    customer_store.store_id = :idStore
                                    AND
                                    mst_order.reji_management_id = :idLastStaff
                                    GROUP BY
                                    mst_order.order_code,
                                    mst_order.customer_jan_code,
                                    mst_customer.id "
            );

            $id_last_staff = (new BookingSearch)->converIdLastStaff($this->register_staff_id);

            $id_store = $this->register_store_id;
            $command->bindParam(':idLastStaff', $id_last_staff)
                    ->bindParam(':idStore', $id_store);

            $result = $command->queryAll();

            if (count($result) > 0) {
                $i = 0;
                $result_seach = null;
                foreach ($result as $value) {
                    $id_customer = $value['id_customer'];
                    if ($i == 0) {
                        $result_seach = $id_customer;
                    } else {
                        $result_seach .= "," . $id_customer;
                    }
                    $i++;
                }
                $array_cover = explode(',', $result_seach);

                $query->andFilterWhere(['point_history.customer_id' => $array_cover]);
            } else {

                $query->andFilterWhere(['=', 'point_history.created_at', 0]);
            }
             */
        }
        $query->andFilterWhere([
            //店舗
            'mst_store.id' => $this->register_store_id,
        ]);
        //履歴日付
        $query->andFilterWhere(['>=', 'point_history.process_date', $this->date_history_from]);
        $query->andFilterWhere(['<=', 'point_history.process_date', $this->date_history_to]);
        //ポイント区分
        $query->andFilterWhere(['=', 'point_history.process_type', $this->process_type]);
        //変動ポイント数
        $query->andFilterWhere(['>=', 'process_point', $this->total_point_from]);
        $query->andFilterWhere(['<=', 'process_point', $this->total_point_to]);

        //会員証番号, 顧客名, 性別
        $query->andFilterWhere(['or', ['like', 'concat(first_name,last_name) ', $this->name], ['like', 'concat(first_name_kana,last_name_kana) ', $this->name]])
                ->andFilterWhere(['like', 'customer_jan_code', $this->customer_jan_code])->andFilterWhere(['sex' => $this->sex]);
        //生年月日
        $query->andFilterWhere(['>=', 'birth_date', $this->birth_date_from]);
        $query->andFilterWhere(['<=', 'birth_date', $this->birth_date_to]);
        //顧客ランク

        $query->andFilterWhere(['rank_id' => $this->rank]);
        //来店回数
        $query->andFilterWhere(['>=', 'number_visit', $this->number_visit_min]);
        $query->andFilterWhere(['<=', 'number_visit', $this->number_visit_max]);
        $query->groupBy(PointHistory::tableName() . '.id');

        return $dataProvider;
    }

    public function searchTicketHistory($params, $customer_id = null) {
        $query = TicketHistory::find();
        $query->joinWith('ticket', true, 'INNER JOIN');
        //$query->joinWith('order', true, 'INNER JOIN');
        $query->joinWith([
            'order' => function (\yii\db\ActiveQuery $query) {
                $query->joinWith('masterCustomer', true, 'INNER JOIN');
            }
                ], true, 'INNER JOIN');
        $query->innerJoin('mst_store', "SUBSTRING(" . MstOrder::tableName() . ".order_code, 1, 5) = mst_store.store_code AND mst_store.del_flg = '0'");
        $query->leftJoin("customer_store", "mst_customer.id = customer_store.customer_id AND mst_store.id = customer_store.store_id AND customer_store.del_flg = '0'");
        // Join : Company store
        $query->innerJoin('company', 'company.id = mst_store.company_id');
//        $company_code = Util::getCookiesCompanyCode();
        $query->andWhere(['mst_customer.company_id' => Util::getCookiesCompanyId()]);
        $dataProvider = new ActiveDataProvider([
            'query' => $query->with("order"),
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => false,
        ]);
        if (isset($customer_id)) {
            $query->andFilterWhere(['mst_customer.id' => $customer_id]);
            return $dataProvider;
        }
        // add conditions that should always apply here
        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        // grid filtering conditions
        // grid filtering conditions
        $query->andFilterWhere(['mst_store.id' => $this->register_store_id]);
        if ($this->register_staff_id != '') {
            
            $command = Yii::$app->getDb()->createCommand(
                                    "SELECT
                                      mst_order.order_code
                                     FROM
                                     mst_order
                                     INNER JOIN mst_customer
                                     ON mst_customer.customer_jan_code = mst_order.customer_jan_code
                                     INNER JOIN customer_store
                                     ON customer_store.customer_id = mst_customer.id
                                     WHERE
                                     customer_store.store_id = :idStore
                                     AND
                                     mst_order.reji_management_id = :idLastStaff
                                     AND
                                     mst_order.del_flg = '0'"
            );

            $id_last_staff = (new BookingSearch)->converIdLastStaff(intval($this->register_staff_id));
            $id_store = $this->register_store_id;
            
            $command->bindParam(':idLastStaff', $id_last_staff)
                    ->bindParam(':idStore', $id_store);
            
           
            $result = $command->queryAll();

            if (count($result) > 0) {
                $i = 0;
                $result_seach = null;
                foreach ($result as $value) {
                    $oder_code_sl = $value['order_code'];
                    if ($i == 0) {
                        $result_seach = $oder_code_sl;
                    } else {
                        $result_seach .= "," . $oder_code_sl;
                    }
                    $i++;
                }
                $array_cover = explode(',', $result_seach);

                $query->andFilterWhere(['ticket_history.order_code' => $array_cover]);
            } else {
                $query->andFilterWhere(['=', 'ticket_history.created_at', 0]);
            }
        }
        if (!empty($this->date_history_from)) {
            $query->andFilterWhere(['>=', 'mst_order.process_date', $this->date_history_from]);
        }
        if (!empty($this->date_history_to)) {
            $query->andFilterWhere(['<=', 'mst_order.process_date', $this->date_history_to]);
        }
        $query->andFilterWhere(['like', 'ticket.name', $this->ticket_name]);
        if (!empty($this->product_name)) {
            $query->leftJoin(ProductTicket::tableName(), ProductTicket::tableName() . '.ticket_id = ' . MasterTicket::tableName() . '.id');
            $query->leftJoin(MstProduct::tableName(), MstProduct::tableName() . '.id = ' . ProductTicket::tableName() . '.product_id');
            $query->andFilterWhere(['or', ['like', 'mst_product.name', $this->product_name], ['like', 'mst_product.name_kana', $this->product_name]]);
        }
        // search membership_card_number, name, sex, rank, birth_date in table mst_customer 
        $query->andFilterWhere(['=', 'mst_customer.customer_jan_code', $this->customer_jan_code]);
        $query->andFilterWhere(['or', ['like', 'concat(first_name,last_name) ', $this->name], ['like', 'concat(first_name_kana,last_name_kana) ', $this->name]]);
        $query->andFilterWhere(['mst_customer.sex' => $this->sex]);
        $query->andFilterWhere(['=', 'customer_store.rank_id', $this->rank]);
        $query->andFilterWhere(['>=', 'birth_date', $this->birth_date_from]);
        $query->andFilterWhere(['<=', 'birth_date', $this->birth_date_to]);
        $query->andFilterWhere(['>=', 'number_visit', $this->number_visit_min]);
        $query->andFilterWhere(['<=', 'number_visit', $this->number_visit_max]);
        $query->groupBy(TicketHistory::tableName() . '.id');
        return $dataProvider;
    }

    public function searchVisitHistory($params, $customer_id = null) {
        $query = MstOrder::find();
        $query->joinWith('masterCustomer', true, 'INNER JOIN');
        $query->innerJoin('mst_store', "SUBSTRING(" . MstOrder::tableName() . ".order_code, 1, 5) = mst_store.store_code AND mst_store.del_flg = '0'");
        $query->leftJoin("customer_store", "mst_customer.id = customer_store.customer_id AND mst_store.id = customer_store.store_id AND customer_store.del_flg = '0'");
        // Join : Company store
        $query->innerJoin('company', 'company.id = mst_store.company_id');
//        $company_code = Util::getCookiesCompanyCode();
        $query->innerJoin('management_login', 'mst_order.order_management_id = management_login.management_id');
        
        $query->innerJoin('mst_staff', 'mst_staff.management_login_id = management_login.id');
        
        $query->andWhere(['mst_customer.company_id' => Util::getCookiesCompanyId()]);
        
        $query->select([
            'mst_order.*',
            'mst_staff.name as name_staff'
        ]);
        
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => false,
        ]);

        if (isset($customer_id)) {
            $query->andFilterWhere(['mst_customer.id' => $customer_id]);
            return $dataProvider;
        }
        // add conditions that should always apply here
        $this->load($params);
        //return $dataProvider;
        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        // grid filtering conditions
        //$query->join('left join', 'mst_customer', 'mst_order.customer_jan_code = mst_customer.customer_jan_code');
        $query->andFilterWhere(['=', MasterCustomer::tableName() . '.customer_jan_code', $this->customer_jan_code]);
        $query->andFilterWhere(['or', ['like', 'concat(first_name,last_name) ', $this->customer_name], ['like', 'concat(first_name_kana,last_name_kana) ', $this->customer_name]]);
        //$query->andFilterWhere(['or', ['like', MasterCustomer::tableName() . '.last_name', $this->customer_last_name], ['like', MasterCustomer::tableName() . '.last_name_kana', $this->customer_last_name]]);
        $query->andFilterWhere([MasterCustomer::tableName() . '.sex' => $this->sex]);
        $query->andFilterWhere([MasterCustomer::tableName() . '.mobile' => $this->mobile]);
        $query->andFilterWhere(['>=', MasterCustomer::tableName() . '.birth_date', $this->birth_date_from]);
        $query->andFilterWhere(['<=', MasterCustomer::tableName() . '.birth_date', $this->birth_date_to]);
        //$query->andFilterWhere(['=', CustomerStore::tableName() . '.rank_id', $this->rank]);
        $query->andFilterWhere(['=', 'customer_store.rank_id', $this->rank]);
        $query->andFilterWhere(['>=', CustomerStore::tableName() . '.number_visit', $this->number_visit_min]);
        $query->andFilterWhere(['<=', CustomerStore::tableName() . '.number_visit', $this->number_visit_max]);

        if (!empty($this->list_category)) {
            foreach ($this->list_category as $cate_id) {
                $product = MstProduct::find()->andWhere([MstProduct::tableName() . '.category_id' => $cate_id])->select('jan_code');
                $count_id = MstOrderDetail::find()->select("order_code")->andWhere(['jan_code' => $product])->groupBy("order_code")->having([">", "COUNT(order_code)", 0]);
                $query->andFilterWhere(['mst_order.order_code' => $count_id]);
            }
        }
        if (!empty($this->product_name)) {
            $query->leftJoin(MstOrderDetail::tableName(), MstOrderDetail::tableName() . '.order_code = ' . MstOrder::tableName() . '.order_code');
            $query->leftJoin(MstProduct::tableName() . ' as product1', MstOrderDetail::tableName() . '.jan_code = ' . 'product1.jan_code');
            $query->andFilterWhere(['or', ['like', 'product1.name', $this->product_name], ['like', 'product1.name_kana', $this->product_name]]);
        }
        $query->andFilterWhere(['=', MstOrder::tableName() . '.order_code', $this->order_code]);
        $query->andFilterWhere(['>=', MstOrder::tableName() . '.process_date', $this->visit_date_from]);
        $query->andFilterWhere(['<=', MstOrder::tableName() . '.process_date', $this->visit_date_to]);
        if (!empty($this->seat_id)) {
            //$query->andFilterWhere(['=', MstOrder::tableName() . '.order_code', $this->order_code]);
        }
        if (!($this->last_staff_id == '')) {
            $id_last_staff = (new BookingSearch)->converIdLastStaff($this->last_staff_id);
            $query->andFilterWhere(['=', 'mst_order.order_management_id', $id_last_staff]);
//              var_dump($id_last_staff);
//              exit();
        }
        $query->andFilterWhere(['=', 'mst_store.id', $this->register_store_id]);
        $query->groupBy([MstOrder::tableName() . '.id','mst_staff.name']);
        return $dataProvider;
    }

    public function searchCustomer($params) {
        $query = MasterCustomer::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => false
        ]);

//        if (!($this->load($params) && $this->validate())) {
//            return $dataProvider;
//        }
        $this->load($params);

        $query->andWhere(['mst_customer.company_id' => Util::getCookiesCompanyId()]);
        $query->andFilterWhere(['or', ['like', 'concat(first_name,last_name)', $this->first_name . $this->last_name], ['like', 'concat(first_name_kana,last_name_kana)', $this->first_name_kana . $this->last_name_kana]])
                ->andFilterWhere(['like', 'mobile', $this->mobile]);

        return $dataProvider->getModels();
    }

    public function searchCustomerInGift($params) {
        $this->load($params);
        $query = MasterCustomer::find();
        $query->andFilterWhere(['like', 'first_name', $params['first_name']]);
        $query->andFilterWhere(['like', 'last_name', $params['last_name']]);
        $query->andFilterWhere(['like', 'first_name_kana', $params['first_name_kana']]);
        $query->andFilterWhere(['like', 'last_name_kana', $params['last_name_kana']]);
        $query->andFilterWhere(['like', 'post_code', $params['post_code']]);
        $query->andFilterWhere(['like', 'address', $params['address']]);
        $query->andFilterWhere(['like', 'mobile', $params['mobile']]);
        return $query->all();
    }
    
    /*
     * Validate number_visit_min and number_visit_max
     * If number_visit_min > number_visit_max return error message
     * @return
     * 
    */
    public function checkNumberVisit($attribute, $params){
        if(!empty($this->number_visit_max)){
            if($this->number_visit_min > $this->number_visit_max){
                 $this->addError('number_visit_max', Yii::t('backend', "Exit number, please specify the number since the start of the number."));
            }
        }
    }

}
