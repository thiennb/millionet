<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use yii2tech\ar\softdelete\SoftDeleteBehavior;

/**
 * This is the model class for table "question".
 *
 * @property integer $id
 * @property string $option
 * @property integer $max_choice
 * @property string $notice_content
 * @property string $question_content
 * @property integer $question_answer_id
 * @property string $del_flg
 * @property integer $created_at
 * @property integer $created_by
 * @property integer $updated_at
 * @property integer $updated_by
 */
class Question extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'question';
    }
    
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),            
            BlameableBehavior::className(),
            'softDeleteBehavior' => [
                'class' => SoftDeleteBehavior::className(),
                'softDeleteAttributeValues' => [
                    'del_flg' => '1'
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            //[['max_choice', 'question_answer_id', 'created_at', 'created_by', 'updated_at', 'updated_by'], 'integer'],
           //[['question_answer_id', 'created_at', 'created_by', 'updated_at', 'updated_by'], 'required'],
            [['option', 'del_flg'], 'string', 'max' => 1],
            [['notice_content'], 'string', 'max' => 300],
            [['question_content'], 'string', 'max' => 200],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('frontend', 'ID'),
            'option' => Yii::t('frontend', 'Option'),
            'max_choice' => Yii::t('frontend', 'Max Choice'),
            'notice_content' => Yii::t('frontend', 'Notice Content'),
            'question_content' => Yii::t('frontend', 'Question Content'),
            'question_answer_id' => Yii::t('frontend', 'Question Answer ID'),
            'del_flg' => Yii::t('frontend', 'Del Flg'),
            'created_at' => Yii::t('frontend', 'Created At'),
            'created_by' => Yii::t('frontend', 'Created By'),
            'updated_at' => Yii::t('frontend', 'Updated At'),
            'updated_by' => Yii::t('frontend', 'Updated By'),
        ];
    }
    
    /**
     * Get question answers relative
     * @return List of QuestionAnswer
     */
    public function getQuestionAnswers(){
        return $this->hasMany(\common\models\QuestionAnswer::className(), ['question_id' => 'id'])
                ->where(['question_answer.del_flg' => 0]);
    }
}
