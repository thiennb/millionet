<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\MstOrder;
use common\models\MstOrderDetail;
use common\components\Util;
/**
 * MstOrderSearch represents the model behind the search form about `common\models\MstOrder`.
 */
class MstOrderSearch extends MstOrder {

    /**
     * @inheritdoc
     */
    public $store_id, $process_date_from, $process_date_to, $product_name, $customer_first_name, $customer_last_name;

    public function rules() {
        return [
            [['id', 'booking_id', 'created_at', 'created_by', 'updated_at', 'updated_by', 'store_id'], 'integer'],
            [['order_code', 'process_date', 'process_time', 'del_flg', 'store_id', 'product_name'], 'safe'],
            [['point_use', 'discount_percent', 'discount_value', 'discount_coupon', 'detail_money', 'subtotal', 'total', 'tax', 'point_add', 'money_credit', 'money_ticket', 'money_other', 'money_receive', 'money_refund'], 'number'],
            [['process_date_from', 'process_date_to'], 'date', 'format' => 'php:Y/m/d'],
            [['customer_first_name', 'customer_last_name'], 'string', 'max' => 40],
                //['process_date_from', 'compare', 'compareAttribute' => 'process_date_to', 'operator' => '<=', 'enableClientValidation' => false],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = MstOrder::find()
                ->innerJoin('mst_store', 'mst_store.store_code = SUBSTRING(mst_order.order_code,1,5)')
                // Join : Company store
                ->innerJoin('company', 'company.id = mst_store.company_id');
        $company_id = Util::getCookiesCompanyId();
        $query->andFilterWhere([
            'company.id' => $company_id,
        ]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => false,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        if (!empty($this->store_id)) {
            $store = MasterStore::findOne($this->store_id);
            $query->andFilterWhere(['=', "SUBSTRING(" . MstOrder::tableName() . ".order_code, 1, 5)", $store->store_code]);
        }
        $query->andFilterWhere([MstOrder::tableName() . '.order_code' => $this->order_code]);
        $query->andFilterWhere(['>=', 'process_date', $this->process_date_from]);
        $query->andFilterWhere(['<=', 'process_date', $this->process_date_to]);
        if (!empty($this->customer_first_name) || !empty($this->customer_last_name)) {
            $query->joinWith('masterCustomer', true, 'INNER JOIN');
            $query->andFilterWhere(['or', ['like', 'first_name', $this->customer_first_name], ['like', 'first_name_kana', $this->customer_first_name]]);
            $query->andFilterWhere(['or', ['like', 'last_name', $this->customer_last_name], ['like', 'last_name_kana', $this->customer_last_name]]);
        }
        if (!empty($this->product_name)) {
            $query->joinWith([
                'masterOrderDetail' => function (\yii\db\ActiveQuery $query) {
                    $query->joinWith('masterProduct', true, 'INNER JOIN');
                }
                    ], true, 'INNER JOIN');
            $query->andFilterWhere(['or', ['like', MstProduct::tableName() . '.name', $this->product_name], ['like', MstProduct::tableName() . '.name_kana', $this->product_name]]);
        }
        $query->groupBy(MstOrder::tableName() . '.id');
        return $dataProvider;
    }


 public function searchHistory($params,$store_code = null, $company_id = null, $customer_jan_code )
    {
        $query = MstOrder::find();
        // add conditions that should always apply here
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
              'pageSize' => 5,
            ],
            'sort' => false
        ]);
        $this->load($params);
        if (!$this->validate()) {
            return $dataProvider;
        }
        $query->select([
                    'mst_order.id AS order_id',
                    'mst_order.order_code',
                    'mst_order.process_date AS process_date',
                    'mst_order.process_time AS process_time',
                    'mst_order.total AS total',
                    'mst_store.name AS name' ,
                    'mst_order.booking_id',
                    'mst_order.company_id',
                    ]);
        $query->innerJoin('mst_store', 'mst_store.store_code = left(mst_order.order_code,5) and mst_store.company_id = mst_order.company_id') ;
        $query->innerJoin('order_detail', 'order_detail.order_code = mst_order.order_code and mst_order.company_id = order_detail.company_id') ;
        if (!empty($params['store_name']) || !empty($params['start_date']) || !empty($params['end_date'])) {
            if ($params['store_name']) {
                if (isset($store_code)) {
                    $query->andwhere(['left(mst_order.order_code,5)' => $store_code]);
                    $query->andwhere(['mst_order.company_id' => $company_id]);
                }
            }
            if ($params['start_date']) 
                $query->andFilterWhere(['>=', MstOrder::tableName() . '.process_date', $params['start_date']]);

            if ($params['end_date'])
                $query->andFilterWhere(['<=', MstOrder::tableName() . '.process_date', $params['end_date']]);   
        }
        if (isset($company_id)) {
            $query->andFilterWhere([MstOrder::tableName() . '.company_id' => $company_id]);
            $query->andFilterWhere([MstOrder::tableName() . '.customer_jan_code' => $customer_jan_code]);
        }
        $query->groupBy([
                    'mst_order.id',
                    'mst_order.order_code',
                    'mst_order.process_date',
                    'mst_order.process_time',
                    'mst_order.total',
                    'mst_store.name' ,
                    'mst_order.booking_id',
                    'mst_order.company_id',
            ]);
        $query->orderBy([
            'mst_order.process_date' => SORT_DESC,
            'mst_order.process_time' => SORT_DESC,
        ]); 
        return $dataProvider;
    }

    public function getMoneyType($order_code, $company_id) {
        $query = MstOrder::find();
        $query->select([
                        'mst_order.money_cash',
                        'mst_order.point_use',
                        'mst_order.money_credit',
                        'mst_order.charge_use',
                        'mst_order.money_ticket',
                        'mst_order.money_other',
                        ])->distinct();
        $query->where(['mst_order.order_code' => $order_code]);
        $query->andWhere(['mst_order.company_id' => $company_id]);
        $query->andWhere(['mst_order.del_flg' => 0]);
        return $query ;
    }

    public static function getHome_flgbyBooking ($id)
    {
        if ($id) {
            $query = Booking::find()->select(['to_home_delivery_flg']);
            $query->andFilterWhere([Booking::tableName() . '.id' => $id]);
            $data = $query->asArray()->one();
            return $data ;
        }
    }

    public function getHome_lfg_detail($order_code,$company_id)
    {
        $query = Booking::find();
        $query->select([
            'to_home_delivery_flg'
        ]);
        $query->rightJoin('mst_order', 'mst_order.booking_id = booking.id') ;
        $query->andFilterWhere([MstOrder::tableName() . '.order_code' => $order_code]);
        $query->andFilterWhere([MstOrder::tableName() . '.company_id' => $company_id]);
        $query->andFilterWhere([Booking::tableName() . '.del_flg' => 0]);
        $data = $query->asArray()->one();
        return $data;
    }

    public function check_gift_flg($order_code,$company_id)
    {
        $query = MstOrderDetail::find()->select('gift_flg');
        $query->andFilterWhere([MstOrderDetail::tableName() . '.order_code' => $order_code]);
        $query->andFilterWhere([MstOrderDetail::tableName() . '.company_id' => $company_id]);
        $data = $query->asArray()->All();
        if ($data) {
            foreach ($data as $value) {
                $gift[] = $value['gift_flg'];
            } 
            return $gift;
        }
    }

    public static function getGilf_flgByCode ($order_code,$company_id)
    {
        $query = MstOrderDetail::find()->select('gift_flg');
        $query->andFilterWhere([MstOrderDetail::tableName() . '.order_code' => $order_code]);
        $query->andFilterWhere([MstOrderDetail::tableName() . '.company_id' => $company_id]);
        $data = $query->asArray()->All();
        if ($data) {
            foreach ($data as $value) {
                $gift[] = $value['gift_flg'];
            } 
            return $gift;
        }
    }

    public static function getOrderItems (MstOrder $order) {
        $detail = $order->masterOrderDetail;
        $coupon_jan_codes = [];
        $product_jan_codes = [];
        
        $items = [];
        
        foreach ($detail as $item) {
            $items = $item;
            if (substr($item->jan_code,0, 2) === '22') {
                $coupon_jan_codes[] = $item->jan_code;
            }
            else {
                $product_jan_codes[] = $item->jan_code;
            }
        }
        
        $coupon_search = MasterCoupon::findFrontEnd()
                ->andWhere([
                    'coupon_jan_code' => $coupon_jan_codes
                ])
                ->all();
        
        
        $product_search = MstProduct::findFrontEnd()
                ->andWhere([
                    'jan_code' => $product_jan_codes
                ])
                ->all();
        
        return array_merge($coupon_search, $product_search);
    }
    
    public function get_home_deliver_flg($order_code, $company_id)
    {
        $query = Booking::findFrontEnd();
        $query->innerJoin(MstOrder::tableName(), MstOrder::tableName() . '.booking_id = ' . Booking::tableName() . '.id') ;
        $query->andWhere([MstOrder::tableName() . '.order_code' => $order_code]);
        //$query->andWhere([MstOrder::tableName() . '.company_id' => $company_id]);
        $query->andWhere(['or', [Booking::tableName() . '.del_flg' => '0'], [Booking::tableName() . '.del_flg' => null]]);
        return $query->one();
    }
}

