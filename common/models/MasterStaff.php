<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii2tech\ar\softdelete\SoftDeleteBehavior;
use yii\helpers\ArrayHelper;
use yii\behaviors\BlameableBehavior;
use common\models\MasterStore;
use common\models\StaffSchedule;
use common\models\Booking;
use common\models\MasterCustomer;
use common\models\ManagementLogin;
use common\models\CustomerStore;
use yii\db\Query;

/**
 * This is the model class for table "mst_staff".
 *
 * @property integer $id
 * @property string $name
 * @property string $short_name
 * @property string $position
 * @property integer $career
 * @property string $sex
 * @property integer $permission_id
 * @property string $email
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $del_flg
 * @property string $avatar
 * @property string $image1
 * @property string $image1_note
 * @property string $image2
 * @property string $image2_note
 * @property string $link_web
 * @property string $memo
 * @property string $show_flg
 * @property string $item_option_1
 * @property string $item_option_2
 * @property string $item_option_3
 * @property string $item_option_4
 * @property string $item_option_5
 * @property string $link_title_1
 * @property string $link_url_1
 * @property string $link_icon_1
 * @property string $link_title_2
 * @property string $link_url_2
 * @property string $link_icon_2
 * @property string $link_title_3
 * @property string $link_url_3
 * @property string $link_icon_3
 * @property string $note
 * @property string $show
 */
class MasterStaff extends \yii\db\ActiveRecord implements \yii\web\IdentityInterface {

    public $file_avatar;
    public $file_image1;
    public $file_image2;
    public $file_icon_1;
    public $file_icon_2;
    public $file_icon_3;
    public $password;
    public $password_new;
    public $password_new_re;
    public $hidden_avatar;
    public $hidden_image1;
    public $hidden_image2;
    public $hidden_icon1;
    public $hidden_icon2;
    public $hidden_icon3;
    public $id_staff_hd;

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'mst_staff';
    }

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            TimestampBehavior::className(),
            BlameableBehavior::className(),
            'softDeleteBehavior' => [
                'class' => SoftDeleteBehavior::className(),
                'softDeleteAttributeValues' => [
                    'del_flg' => '1'
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [

            [['name', 'permission_id', 'store_id'], 'required'],
//            [['password'], 'required', 'on' => 'update'],
            [['password_new', 'password_new_re'], 'required', 'on' => 'create'],
//            [['email'], 'unique', 'on' => 'create'],
            [['email'], 'string', 'max' => 60],
            [['email'], 'checkEmail'],
//            [['email'], 'unique',
//                'targetClass' => 'common\models\MasterStaff', 
//                'when' => function ($model, $attribute) {
//                    return $model->{$attribute} !== $model->getOldAttribute($attribute);
//                },
//            ],
            [['permission_id', 'management_login_id', 'store_id', 'assign_product_id'], 'integer'],
            [['sex', 'show_flg', 'del_flg'], 'string', 'max' => 1],
            [['image1_note', 'image2_note', 'memo'], 'string', 'max' => 500],
            [['name', 'avatar', 'image1', 'image2'], 'string', 'max' => 80],
            [['short_name'], 'string', 'max' => 5],
            [['position'], 'string', 'max' => 20],
            [['career'], 'number', 'max' => 100, 'min' => 0],
            [['career'], 'string', 'max' => 4],
            [['file_avatar', 'file_image1', 'file_image2'], 'file', 'extensions' => 'png, jpg, jpeg, gif', 'skipOnEmpty' => true, 'maxSize' => 1024 * 1024 * 2],
            [['file_icon_1', 'file_icon_2', 'file_icon_3'], 'file', 'extensions' => 'png, jpg, jpeg, gif', 'skipOnEmpty' => true, 'maxSize' => 1024 * 1024 * 2],
            [['item_option_1', 'item_option_2', 'item_option_3', 'item_option_4', 'item_option_5'], 'string', 'max' => 100],
            [['link_title_1', 'link_title_2', 'link_title_3'], 'string', 'max' => 100],
            [['link_url_1', 'link_url_2', 'link_url_3'], 'url'],
            [['link_url_1', 'link_url_2', 'link_url_3'], 'string', 'max' => 300],
            ['password_new_re', 'compare', 'compareAttribute' => 'password_new', 'skipOnEmpty' => false,],
            [['password_new_re', 'password_new', 'password'], 'string', 'min' => 6, 'max' => 20],
            [['password_new_re', 'password_new', 'password'], 'match', 'pattern' => "/^([a-zA-Z0-9!@#$%^&*]+)$/u", 'message' => Yii::t('backend', 'Please enter alphanumeric symbols.')],
            [['hidden_avatar', 'hidden_image1', 'hidden_image2', 'hidden_icon1', 'hidden_icon2', 'hidden_icon3'], 'safe'],
            [['name', 'short_name'], 'match', 'not' => true, 'pattern' => '/[\'\/~`\!@#\$%\^&\*\(\)_\-\+=\{\}\[\]\|;:"\<\>,\.\?\\\]/', 'message' => Yii::t('backend', '{attribute} invalid characters')],
            [['link_url_1'], 'url', 'defaultScheme' => ''],
            [['link_url_2'], 'url', 'defaultScheme' => ''],
            [['link_url_3'], 'url', 'defaultScheme' => ''],
            //['password', 'compare', 'compareAttribute' => 'password_new', 'message' => Yii::t('backend', "Passwords don't match")],
            [['id_staff_hd'], 'safe'],
            [['password'], 'checkPassWord', 'on' => 'update', 'skipOnEmpty' => false],
            [['catch', 'introduction'], 'safe'],
            [['catch'], 'string', 'max' => 25],
            [['introduction'], 'string', 'max' => 150],
            [['short_name'], 'required'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => Yii::t('backend', 'ID'),
            'management_login_id' => Yii::t('backend', 'Management ID'),
            'name' => Yii::t('backend', 'Name'),
            'short_name' => Yii::t('backend', 'Short name'),
            'sex' => Yii::t('backend', 'Sex'),
            'permission_id' => Yii::t('backend', 'Permission'),
            'email' => Yii::t('backend', 'Email'),
            'created_at' => Yii::t('backend', 'Created At'),
            'updated_at' => Yii::t('backend', 'Updated At'),
            'del_flg' => Yii::t('backend', 'Del Flg'),
            'avatar' => Yii::t('backend', 'Staff avatar'),
            'file_avatar' => Yii::t('backend', 'Staff avatar'),
            'image1' => Yii::t('backend', 'Home Photo 1'),
            'file_image1' => Yii::t('backend', 'Home Photo 1'),
            'image1_note' => Yii::t('backend', 'Home Photo 1 Comment'),
            'image2' => Yii::t('backend', 'Home Photo 2'),
            'file_image2' => Yii::t('backend', 'Home Photo 2'),
            'image2_note' => Yii::t('backend', 'Home Photo 2 Comment'),
            'link_web' => Yii::t('backend', 'Link Web 1'),
            'memo' => Yii::t('backend', 'Any item'),
            'show_flg' => Yii::t('backend', 'Show'),
            'password' => Yii::t('backend', 'Password'),
            'password_new' => ($this->scenario == 'create' ) ? Yii::t('backend', 'New Password') : Yii::t('backend', 'Update New Password'),
            'password_new_re' => ($this->scenario == 'create') ? Yii::t('backend', 'New Password Reinput') : Yii::t('backend', 'Update New Password Reinput'),
            'item_option_1' => Yii::t('backend', 'Item OPtion 1'),
            'item_option_2' => Yii::t('backend', 'Item OPtion 2'),
            'item_option_3' => Yii::t('backend', 'Item OPtion 3'),
            'item_option_4' => Yii::t('backend', 'Item OPtion 4'),
            'item_option_5' => Yii::t('backend', 'Item OPtion 5'),
            'link_title_1' => Yii::t('backend', 'Link 1 title'),
            'link_url_1' => Yii::t('backend', 'Link 1URL'),
            'link_icon_1' => Yii::t('backend', 'Link 1 icon'),
            'link_title_2' => Yii::t('backend', 'Link 2 title'),
            'link_url_2' => Yii::t('backend', 'Link 2URL'),
            'link_icon_2' => Yii::t('backend', 'Link 2 icon'),
            'link_title_3' => Yii::t('backend', 'Link 3 title'),
            'link_url_3' => Yii::t('backend', 'Link 3URL'),
            'link_icon_3' => Yii::t('backend', 'Link 3 icon'),
            'note' => Yii::t('backend', 'Any item 1'),
            'show' => Yii::t('backend', 'Show'),
            'store_id' => Yii::t('backend', 'Affiliation store'),
            'assign_product_id' => Yii::t('backend', 'Assign fee'),
            'career' => Yii::t('backend', 'career'),
            'catch' => Yii::t('backend', 'Catch'),
            'introduction' => Yii::t('backend', 'Introduction'),
        ];
    }

    public static function find() {
        return parent::find()->where(['or', [MasterStaff::tableName() . '.del_flg' => '0'], [MasterStaff::tableName() . '.del_flg' => null]]);
    }

    public static function findFrontEnd() {
        return parent::find()->where(['or', [MasterStaff::tableName() . '.del_flg' => '0'], [MasterStaff::tableName() . '.del_flg' => null]]);
    }

    public static function findByStore() {
        return self::find()->where((new \common\components\FindPermission)->getPermissionCondition(self::tableName(), true));
    }

    public function getStore() {
        return $this->hasOne(\common\models\MasterStore::className(), ['id' => 'store_id']);
    }

    public function getAssignProduct() {
        return $this->hasOne(\common\models\MstProduct::className(), ['id' => 'assign_product_id']);
    }

    /**
     * array list staff 
     */
    public static function listStaff($shopId = null) {
        if ($shopId != null) {
            return ArrayHelper::map(MasterStaff::find()->innerJoin(ManagementLogin::tableName(), 'management_login.id = mst_staff.management_login_id')->andWhere(['store_id' => $shopId])->orderBy(['name' => SORT_ASC])->all(), 'id', 'name');
        }
        return ArrayHelper::map(MasterStaff::find()->orderBy(['name' => SORT_ASC])->all(), 'id', 'name');
    }

    public function listStaffByStore($shopId = null) {
        $staff_flag = \common\components\Constants::SELECT_STAFF;
        if ($shopId != null) {
            $search_query = (new Query())
                            ->select([
                                "id" => "m.management_id",
                                "name" => "s.name"
                            ])
                            ->from(["s" => MasterStaff::tableName()])
                            ->innerJoin(['m' => ManagementLogin::tableName()], "m.id = s.management_login_id")
                            ->innerJoin(['st' => MasterStore::tableName()], "st.id = s.store_id ")
                            ->andWhere(['and', ['=', 's.store_id', $shopId], ['=', 'booking_resources', $staff_flag]])
                            ->orderBy(['name' => SORT_ASC])->all();

            $result = $search_query;

            return ArrayHelper::map($result, 'id', 'name');
        }
        return [];
    }

    public function listStaffByStoreCT($shopId = null) {
        $staff_flag = \common\components\Constants::SELECT_STAFF;
        if ($shopId != null) {
            $search_query = (new Query())
                            ->select([
                                "id" => "s.id",
                                "name" => "s.name"
                            ])
                            ->from(["s" => MasterStaff::tableName()])
                            ->innerJoin(['m' => ManagementLogin::tableName()], "m.id = s.management_login_id")
                            ->innerJoin(['st' => MasterStore::tableName()], "st.id = s.store_id ")
                            ->andWhere(['and', ['=', 's.store_id', $shopId], ['=', 'booking_resources', $staff_flag]])
                            ->orderBy(['name' => SORT_ASC])->all();

            $result = $search_query;

            return ArrayHelper::map($result, 'id', 'name');
        }
        return [];
    }

    public function getRoleByManagementLoginId($id) {
        return MasterStaff::find()->where(['management_login_id' => $id])->select('store_id, permission_id')->one();
    }

    public function saveManagementId($password) {
        $management_login = new ManagementLogin();
        $management_login->management_id = $management_login->generateManagementId();
        $management_login->password_hash = Yii::$app->security->generatePasswordHash($password);
        $management_login->save();
        $this->management_login_id = $management_login->id;
    }

    public function saveManagementIdInit($password) {
        $management_login = new ManagementLogin();
        $management_login->detachBehaviors();
        $management_login->management_id = $management_login->generateManagementId();
        $management_login->password_hash = Yii::$app->security->generatePasswordHash($password);
        $management_login->created_at = 1;
        $management_login->created_by = 1;
        $management_login->updated_at = 1;
        $management_login->updated_by = 1;
        $management_login->save();
        $this->management_login_id = $management_login->id;
    }

    public function updatePasswordManagementLogin($password) {
        $management_login = ManagementLogin::findOne($this->management_login_id);
        if (isset($management_login)) {
            $management_login->password_hash = Yii::$app->security->generatePasswordHash($password);
        }
        $management_login->save();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getManagementLogin() {
        return $this->hasOne(ManagementLogin::className(), ['id' => 'management_login_id']);
    }

    /**
     * Check email
     * @param string $attribute, $params
     * @return error 
     * @throws 
     */
    public function checkEmail($attribute, $params) {
        //check valid email
        $check = preg_match(
                '/^[A-z]+[.A-z0-9_\-]+[@][A-z0-9_\-]+([.][A-z0-9_\-]+)+[A-z.]{2,4}$/', $this->$attribute
        );

        if (!$check) {
            $key = $attribute;
            //add message error
            $this->addError($key, Yii::t('backend', "email not valid"));
        }
    }

    public function checkDelete($id) {
        $check = 0;
        $check += Booking::find()->andWhere(['staff_id' => $id])->count();
        $check += StaffSchedule::find()->andWhere(['staff_id' => $id])->count();
        $check += MasterCustomer::find()->andWhere(['register_staff_id' => $id])->count();
        $check += CustomerStore::find()->andWhere(['last_staff_id' => $id])->count();
        return ($check > 0) ? FALSE : TRUE;
    }

    public static function getListStaff() {
        return ArrayHelper::map(MasterStaff::find()->all(), 'id', 'name');
    }

    /**
     * Check email
     * @param string $attribute, $params
     * @return error 
     * @throws 
     */
    public function checkPassWord($attribute, $params) {
        //get management login
        $id = $this->id_staff_hd;
        $permission_id = Yii::$app->user->identity->permission_id;
        $model_old = !empty($id) ? self::findOne($id) : null;
        if (!empty($model_old)) {
            if (empty($this->password) && (Yii::$app->user->identity->permission_id >= $model_old->permission_id)) {
                //add message error
                $this->addError($attribute, Yii::t('yii', '{attribute} cannot be blank.', ['attribute' => Yii::t('backend', 'Password')]));
            }
        }
        $manage_login = ManagementLogin::findOne($this->management_login_id);
        if (isset($manage_login) && !empty($this->password)) {
            if (!$manage_login->validatePassword($this->password)) {
                $this->addError($attribute, Yii::t('backend', "Input string is not currently password match."));
            }
        }
    }

    /**
     * array list last staff 
     */
    public static function listLastStaff($shopId = null) {
        if ($shopId != null) {
            return ArrayHelper::map(self::find()->join('INNER JOIN', 'customer_store', 'customer_store.last_staff_id = mst_staff.id')
                                    ->join('INNER JOIN', 'mst_customer', 'mst_customer.id = customer_store.customer_id')
                                    ->andWhere(['mst_staff.store_id' => $shopId, 'company_id' => \common\components\Util::getCookiesCompanyId()])
                                    ->orderBy(['mst_staff.name' => SORT_ASC])->all(), 'id', 'name');
        }
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password) {
        return $this->managementLogin->validatePassword($password);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id) {
        return self::findOne(['id' => $id]);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null) {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * @inheritdoc
     */
    public function getId() {
        return $this->getPrimaryKey();
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey() {
        return $this->managementLogin->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey) {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByManagementId($username) {
        $management = ManagementLogin::findOne(['management_id' => $username, 'status' => User::STATUS_ACTIVE]);

        if ($management) {
            return self::findOne(['management_login_id' => $management->id]);
        }
    }

}
