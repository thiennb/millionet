<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use yii2tech\ar\softdelete\SoftDeleteBehavior;

/**
 * This is the model class for table "checklist".
 *
 * @property integer $id
 * @property string $type
 * @property string $display_flg
 * @property integer $question_id
 * @property string $del_flg
 * @property integer $created_at
 * @property integer $created_by
 * @property integer $updated_at
 * @property integer $updated_by
 */
class Checklist extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'checklist';
    }

     /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),            
            BlameableBehavior::className(),
            'softDeleteBehavior' => [
                'class' => SoftDeleteBehavior::className(),
                'softDeleteAttributeValues' => [
                    'del_flg' => '1'
                ],
            ],
        ];
    }
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            //[['question_id'], 'required'],
            [['question_id', 'created_at', 'created_by', 'updated_at', 'updated_by'], 'integer'],
            [['type', 'display_flg', 'del_flg'], 'string', 'max' => 1],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('frontend', 'ID'),
            'type' => Yii::t('frontend', 'Type'),
            'display_flg' => Yii::t('frontend', 'Display Flg'),
            'question_id' => Yii::t('frontend', 'Question ID'),
            'del_flg' => Yii::t('frontend', 'Del Flg'),
            'created_at' => Yii::t('frontend', 'Created At'),
            'created_by' => Yii::t('frontend', 'Created By'),
            'updated_at' => Yii::t('frontend', 'Updated At'),
            'updated_by' => Yii::t('frontend', 'Updated By'),
        ];
    }
    
    /**
     * Get checklist by store
     * @param int $store_id
     * @return Lists of checklist
     */
    public static function getChecklistByStore($store_id){
        return Checklist::find()
               ->leftJoin('store_checklist', 'store_checklist.checklist_id = checklist.id')
               ->where(['store_checklist.store_id' => $store_id])
               ->andWhere(['store_checklist.del_flg' => 0])
               ->andWhere(['checklist.del_flg' => 0])
               ->andWhere(['checklist.display_flg' => 0])
               ->all();
    }
    
    /**
     * Get questions relative
     * 
     * @return Question object
     */
    public function getQuestion(){
        return $this->hasOne(\common\models\Question::className(), ['id' => 'question_id'])
                ->where(['question.del_flg' => 0]);
    }
}
