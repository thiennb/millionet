<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use yii2tech\ar\softdelete\SoftDeleteBehavior;

/**
 * This is the model class for table "mst_order".
 *
 * @property integer $id
 * @property string $order_code
 * @property string $process_date
 * @property string $process_time
 * @property integer $booking_id
 * @property string $point_use
 * @property string $change_use
 * @property string $discount_percent
 * @property string $discount_value
 * @property string $discount_coupon
 * @property string $detail_money
 * @property string $subtotal
 * @property string $total
 * @property string $tax
 * @property string $point_add
 * @property string $money_credit
 * @property string $money_ticket
 * @property string $money_other
 * @property string $money_receive
 * @property string $money_refund
 * @property string $del_flg
 * @property integer $created_at
 * @property integer $created_by
 * @property integer $updated_at
 * @property integer $updated_by
 */
class MstOrder extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public $name;
    public $to_home_delivery_flg;
    public $gift_flg;
    public $start_date;
    public $end_date;
    public $customer_first_name;
    public $customer_last_name;
    public $customer_visit_number;
    public $name_staff;
    public $gift_quantity;
    
    public static function tableName()
    {
        return 'mst_order';
    }
    
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),            
            BlameableBehavior::className(),
            'softDeleteBehavior' => [
                'class' => SoftDeleteBehavior::className(),
                'softDeleteAttributeValues' => [
                    'del_flg' => '1'
                ],
            ],
        ];
    }
    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['process_date','name_staff'], 'safe'],
            [['booking_id'], 'required'],
            [['booking_id', 'created_at', 'created_by', 'updated_at', 'updated_by'], 'integer'],
            [['point_use', 'discount_percent', 'discount_value', 'discount_coupon', 'detail_money', 'subtotal', 'total', 'tax', 'point_add', 'money_credit', 'money_ticket', 'money_other', 'money_receive', 'money_refund'], 'number'],
            [['order_code'], 'string', 'max' => 16],
            [['process_time'], 'string', 'max' => 5],
            [['del_flg'], 'string', 'max' => 1],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'process_time' => 'Process Time',
            'company_id'    => 'Company Id',
            'booking_id' => 'Booking ID',
            'point_use' => 'Point Use',
            'change_use' => 'Change Use',
            'discount_percent' => 'Discount Percent',
            'discount_value' => 'Discount Value',
            'discount_coupon' => 'Discount Coupon',
            'detail_money' => 'Detail Money',
            'subtotal' => 'Subtotal',
            'total' => 'Total',
            'tax' => 'Tax',
            'point_add' => 'Point Add',
            'money_credit' => 'Money Credit',
            'money_ticket' => 'Money Ticket',
            'money_other' => 'Money Other',
            'money_receive' => 'Money Receive',
            'money_refund' => 'Money Refund',
            'del_flg' => 'Del Flg',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
            'order_code' =>  Yii::t('backend', 'Order Code'),
            'store_id' =>  Yii::t('backend', 'Store Id'),
            'process_date_from' => Yii::t('backend', 'Date'),
            'process_date_to' => Yii::t('backend', 'Date'),
            'process_date' => Yii::t('backend', 'Date'),
            'customer_name' => Yii::t('backend', 'People Send'),
            'customer_jan_code' =>Yii::t('backend', 'Customer Jan Code'),
            'product_name' => Yii::t('backend', 'Product name'),
            'point_add' => Yii::t('backend','Point Add'),  
            'visit_date_from' => Yii::t('backend', 'People Send'),
        ];
    }
    
    /**
     * Get master order detail
     * @return type
     */
    public function getMasterOrderDetail() {
        return $this->hasMany(MstOrderDetail::className(), ['order_code' => 'order_code']);
    }
    
    /**
     *  Get gift order detail
     */
    public function getGiftOrderDetail() {
        return $this->hasMany(MstOrderDetail::className(), ['order_code' => 'order_code'])->where(['gift_flg' => 1]);
    }
    
    public function getBooking() {
        return $this->hasOne(Booking::className(), ['id' => 'booking_id'])->with('masterCustomer');
    }
    
    public function getStore() {
        return $this->hasOne(MasterStore::className(), ['id' => 'store_id']);
    }
    
    public function getMasterCustomer() {
        return $this->hasOne(MasterCustomer::className(), ['customer_jan_code' => 'customer_jan_code']);
    }
    
    public function getManagement()
    {   
      return $this->hasOne(ManagementLogin::className(), ['management_id' => 'order_management_id']);
    }
    public function getRejiManagement()
    {   
      return $this->hasOne(ManagementLogin::className(), ['management_id' => 'reji_management_id']);
    }
    public static function find() {
        
        return parent::find()->where((new \common\components\FindPermission)->getPermissionCondition(self::tableName(), false, false, true));
    }

    public function filterDetail($order_code, $company_id = null)
    {
        $query = (new \yii\db\Query())->select(['mst_order.id as orderId',
                                                'mst_order.total',
                                                'mst_order.order_code',
                                                'mst_store.id',
                                                'order_detail.gift_flg',
                                                'mst_store.name',
                                                'mst_store.address',
                                                'mst_store.latitude',
                                                'mst_store.longitude',
                                                'mst_store.website',
                                                'booking.to_home_delivery_flg',
                                                'mst_order.point_use'])
                                        ->from('mst_order')
                                        ->innerJoin('mst_store', 'mst_store.store_code = left(mst_order.order_code,5)')
                                        ->leftJoin('booking', 'booking.store_id = mst_store.id')
                                        ->innerJoin('order_detail', 'order_detail.order_code = mst_order.order_code')
                                        ->where(['mst_order.order_code' => $order_code])
                                        ->andWhere(['mst_order.company_id' => $company_id])
                                        ->one();
        return $query ;
    }
    
    public function findTotalPeopleDateStaff($storeCode = null){
        $totalPeople = 0;
        $listaddedJancode = [];
        if($storeCode !== null){
            $orders = $this->find()->where(['process_date'=> date('Y-m-d'), 'left(mst_order.order_code,5)'=>$storeCode, 'del_flg'=> '0'])->all();
            
            if(!empty($orders)){
                foreach($orders as $order){
                    if(trim($order['customer_jan_code']) == ''){
                        $totalPeople++;
                    }else{
                        if(!in_array($order['customer_jan_code'], $listaddedJancode)){
                            $totalPeople++;
                            array_push($listaddedJancode, $order['customer_jan_code']);
                        }
                    }
                }
            }
        }
        
        return $totalPeople;
    }
    
    public function findtotalPeoPleMonthStaff($storeCode = null){
        $totalPeople = 0;
        $listaddedJancode = [];
        if($storeCode !== null){
            $orders = $this->find()->where(['EXTRACT( month from process_date)'=> date('m'), 'left(order_code,5)'=> $storeCode, 'del_flg'=> '0'])->all();
            if(!empty($orders)){
                foreach($orders as $order){
                    if(trim($order['customer_jan_code']) == ''){
                        $totalPeople++;
                    }else{
                        if(!in_array($order['customer_jan_code'], $listaddedJancode)){
                            $totalPeople++;
                            array_push($listaddedJancode, $order['customer_jan_code']);
                        }
                    }
                }
            }
        }
        
        return $totalPeople;
    }
    
    public function findAllPeopleDateStaff($storeCode = null){
        if($storeCode !== null){
            return $this->find()->where(['process_date' => date('Y-m-d'), 'left(order_code,5)'=>$storeCode, 'del_flg' => '0'])->all();
        }
        
        return null;
    }
    
    public function findTotalAwayPeopleDateStaff($storeCode = null, $awayDate = null, $awayGimmickDate = null){
        if($storeCode !== null && $awayDate !== null && $awayGimmickDate != null){

            $dateAway = ''.date('Y-m-d', strtotime('-'.$awayDate.' day', strtotime(date('Y-m-d'))));
            $dateGimmickAway = ''.date('Y-m-d', strtotime('-'.$awayGimmickDate.' day', strtotime(date('Y-m-d'))));
            $subQuery = $this->find()->select('customer_jan_code')
                    ->andWhere(['left(order_code,5)' => $storeCode])
                    ->andWhere(['between', 'process_date', $dateAway, date('Y-m-d') ])
                    ->andWhere(['del_flg'=>'0'])->all();
            $customerJanCode = array();
            foreach($subQuery as $order){
                if(trim($order['customer_jan_code']) != ''){
                    if(!in_array($order['customer_jan_code'], $customerJanCode))
                        array_push($customerJanCode, $order['customer_jan_code']);
                }
            }

            $query = $this->find()->where(['left(order_code,5)' => $storeCode])
                                ->andWhere(['between','process_date', $dateGimmickAway, $dateAway])
                                ->andWhere(['del_flg'=>'0']);

            if(!empty($customerJanCode)){
                $query = $query->andWhere(['not in', 'customer_jan_code', $customerJanCode]);
            }
            return $query->count('DISTINCT(customer_jan_code)');
        }
        
        return 0;
    }
    
    public function findTotalAwayGimmickPeopleDateStaff($storeCode = null, $awayGimmickDate = null){
        if($storeCode !== null && $awayGimmickDate !== null){
            $totalPeople = $this->findTotalAwayPeopleDateStaff($storeCode, $awayGimmickDate, $awayGimmickDate+2);
            return $totalPeople;
        }
        
        return 0;
    }
    
    public function findTotalMoneyByHour($year = null, $month = null, $day = null, $timeStart = null, $timeEnd = null, $storeCode = null){
        if($storeCode !== null){
            $orders = (new \yii\db\Query())
                    ->select([
                        'mst_order.total',
                        'mst_order.money_credit',
                        'mst_order.customer_jan_code',
                        'mst_order.money_ticket',
                        'mst_order.money_other',
                        'mst_order.money_cash',
                        'mst_order.point_use_money',
                        'mst_order.process_date',
                        'mst_order.process_time',
                        'SUM(order_detail.quantity) as quantity'])
                    ->from('mst_order')
                    ->where(['left(mst_order.order_code,5)'=>$storeCode])
                    ->innerJoin('order_detail', 'order_detail.order_code = mst_order.order_code')
                    ->andWhere(['mst_order.del_flg'=>'0'])
                    ->andWhere(['mst_order.process_date'=> $year.'-'.sprintf('%02s',$month).'-'.sprintf('%02s',$day)])
                    ->groupBy(['mst_order.order_code', 
                        'mst_order.money_credit', 
                        'mst_order.customer_jan_code', 
                        'mst_order.money_ticket', 
                        'mst_order.money_other', 
                        'mst_order.money_cash',
                        'mst_order.point_use_money',
                        'mst_order.process_date',
                        'mst_order.process_time',
                        'mst_order.total'])
                    ->all();
            $hourStart = strtotime($year.'-'.$month.'-'.$day.' '.$timeStart.':00');
            $hourEnd = strtotime($year.'-'.$month.'-'.$day.' '.$timeEnd.':00');

            $total_money = 0;
            $count_order = 0;
            $quantity = 0;
            $num_customer = 0;
            $custom_jan_code = array();
            if(!empty($orders)){

                foreach($orders as $order){
                    $timeOrder = strtotime($order['process_date'].' '.$order['process_time'].':00');
                    if($timeOrder > $hourStart && $timeOrder <= $hourEnd){
                        $total_money += $order['total'];
                        $count_order ++;
                        $quantity += $order['quantity'];
                        
                        if(trim($order['customer_jan_code']) == ''){
                            $num_customer++;
                        }else{
                            if(!in_array($order['customer_jan_code'], $custom_jan_code)){
                                $num_customer++;
                                array_push($custom_jan_code, $order['customer_jan_code']);
                            }
                        }
                    }
                }

                return array(
                    'time' => $timeEnd,
                    'time_mark' => $timeEnd,
                    'total_money'=> $total_money, 
                    'count'=> $count_order, 
                    'money_per_order' => $count_order !== 0 ? $total_money/$count_order : 0,
                    'num_customer' => $num_customer,
                    'money_per_customer' => $num_customer !== 0 ? $total_money/$num_customer : 0,
                    'quantity'=>$quantity);
            }
        }
        
        return null;
    }
    
    public function findTotalMoneyByDate($year = null, $month = null,$storeCode){
        if($storeCode !== null){
            $revenues = array();
            $daysInMonth = cal_days_in_month(CAL_GREGORIAN, $month, $year);
            for($day = 1; $day <= $daysInMonth ; $day++){
                $datetime = $year.'-'.sprintf('%02s',$month).'-'.sprintf('%02s',$day);

                $orders = (new \yii\db\Query())
                    ->select([
                        'mst_order.total',
                        'mst_order.money_credit',
                        'mst_order.customer_jan_code',
                        'mst_order.money_ticket',
                        'mst_order.money_other',
                        'mst_order.money_cash',
                        'mst_order.point_use_money',
                        'mst_order.process_date',
                        'mst_order.process_time',
                        'SUM(order_detail.quantity) as quantity'])
                    ->from('mst_order')
                    ->where(['left(mst_order.order_code,5)'=>$storeCode])
                    ->innerJoin('order_detail', 'order_detail.order_code = mst_order.order_code')
                    ->andWhere(['mst_order.del_flg'=>'0'])
                    ->andWhere(['mst_order.process_date'=> $datetime])
                    ->groupBy(['mst_order.order_code', 
                        'mst_order.money_credit', 
                        'mst_order.customer_jan_code', 
                        'mst_order.money_ticket', 
                        'mst_order.money_other', 
                        'mst_order.money_cash',
                        'mst_order.point_use_money',
                        'mst_order.process_date',
                        'mst_order.process_time',
                        'mst_order.total'])
                        ->all();
                $total_money = 0;
                $count_order = 0;
                $quantity = 0;
                $num_customer = 0;
                $custom_jan_code = array();
                
                if(!empty($orders)){

                    foreach($orders as $order){
                        $total_money += $order['total'];
                        $count_order++;
                        $quantity += $order['quantity'];
                        if(trim($order['customer_jan_code']) == ''){
                            $num_customer++;
                        }else{
                            if(!in_array($order['customer_jan_code'], $custom_jan_code)){
                                $num_customer++;
                                array_push($custom_jan_code, $order['customer_jan_code']);
                            }
                        }
                    }                    
                }

                array_push($revenues ,array(
                        'time' => $datetime,
                        'time_mark' => sprintf('%02s',$day),
                        'total_money'=> $total_money, 
                        'count'=> $count_order, 
                        'money_per_order' => $count_order !== 0 ? $total_money/$count_order : 0,
                        'num_customer' => $num_customer,
                        'money_per_customer' => $num_customer !== 0 ? $total_money/$num_customer : 0,
                        'quantity'=>$quantity));
            }
            return $revenues;
        }
        return null;
    }
    
    public function findTotalMoneyByMonth($year = null, $storeCode){
        if($storeCode !== null){
            $revenues = array();
            
            for($month = 1; $month <= 12 ; $month++){

                $orders = (new \yii\db\Query())
                    ->select([
                        'mst_order.total',
                        'mst_order.money_credit',
                        'mst_order.customer_jan_code',
                        'mst_order.money_ticket',
                        'mst_order.money_other',
                        'mst_order.money_cash',
                        'mst_order.point_use_money',
                        'mst_order.process_date',
                        'mst_order.process_time',
                        'SUM(order_detail.quantity) as quantity'])
                    ->from('mst_order')
                    ->where(['left(mst_order.order_code,5)'=>$storeCode])
                    ->innerJoin('order_detail', 'order_detail.order_code = mst_order.order_code')
                    ->andWhere(['mst_order.del_flg'=>'0'])
                    ->andWhere(['EXTRACT(YEAR FROM mst_order.process_date)'=> $year])
                    ->andWhere(['EXTRACT(MONTH FROM mst_order.process_date)'=> $month])
                    ->groupBy(['mst_order.order_code', 
                        'mst_order.money_credit', 
                        'mst_order.customer_jan_code', 
                        'mst_order.money_ticket', 
                        'mst_order.money_other', 
                        'mst_order.money_cash',
                        'mst_order.point_use_money',
                        'mst_order.process_date',
                        'mst_order.process_time',
                        'mst_order.total'])
                        ->all();
                $total_money = 0;
                $count_order = 0;
                $quantity = 0;
                $num_customer = 0;
                $custom_jan_code = array();
                
                if(!empty($orders)){

                    foreach($orders as $order){
                        $total_money += $order['total'];
                        $count_order ++;
                        $quantity += $order['quantity'];
                        if(trim($order['customer_jan_code']) == ''){
                            $num_customer++;
                        }else{
                            if(!in_array($order['customer_jan_code'], $custom_jan_code)){
                                $num_customer++;
                                array_push($custom_jan_code, $order['customer_jan_code']);
                            }
                        }
                    }                    
                }
                array_push($revenues ,array(
                        'time' => $month,
                        'time_mark' => sprintf($month.Yii::t('backend', 'Month')),
                        'total_money'=> $total_money, 
                        'count'=> $count_order, 
                        'money_per_order' => $count_order !== 0 ? $total_money/$count_order : 0,
                        'num_customer' => $num_customer,
                        'money_per_customer' => $num_customer !== 0 ? $total_money/$num_customer : 0,
                        'quantity'=>$quantity));
            }
            return $revenues;
        }
        return null;
    }
    
    function findTotalMoneyByYear($storeCode){
        if($storeCode !== null){
            $revenues = array();
            $mindate = (new \yii\db\Query())
                    ->select(['min(mst_order.process_date) as min_date'])
                    ->from('mst_order')
                    ->where(['left(mst_order.order_code,5)'=>$storeCode])->one();

            if(!empty($mindate)){
                $mindate = $mindate['min_date'];
                $minYear = (int)date('Y', strtotime($mindate));
                $currentYear = (int)date('Y');
                
                for($minYear; $minYear <= $currentYear; $minYear++){
                    $orders = (new \yii\db\Query())
                    ->select([
                        'mst_order.total',
                        'mst_order.money_credit',
                        'mst_order.customer_jan_code',
                        'mst_order.money_ticket',
                        'mst_order.money_other',
                        'mst_order.money_cash',
                        'mst_order.point_use_money',
                        'mst_order.process_date',
                        'mst_order.process_time',
                        'SUM(order_detail.quantity) as quantity'])
                    ->from('mst_order')
                    ->where(['left(mst_order.order_code,5)'=>$storeCode])
                    ->innerJoin('order_detail', 'order_detail.order_code = mst_order.order_code')
                    ->andWhere(['mst_order.del_flg'=>'0'])
                    ->andWhere(['EXTRACT(YEAR FROM mst_order.process_date)'=> $minYear])
                    ->groupBy(['mst_order.order_code', 
                        'mst_order.money_credit', 
                        'mst_order.customer_jan_code', 
                        'mst_order.money_ticket', 
                        'mst_order.money_other', 
                        'mst_order.money_cash',
                        'mst_order.point_use_money',
                        'mst_order.process_date',
                        'mst_order.process_time',
                        'mst_order.total'])
                        ->all();
                    $total_money = 0;
                    $count_order = 0;
                    $quantity = 0;
                    $num_customer = 0;
                    $custom_jan_code = array();
                    if(!empty($orders)){

                        foreach($orders as $order){
                            $total_money += $order['total'];
                            $count_order ++;
                            $quantity += $order['quantity'];
                            if(trim($order['customer_jan_code']) == ''){
                                $num_customer++;
                            }else{
                                if(!in_array($order['customer_jan_code'], $custom_jan_code)){
                                    $num_customer++;
                                    array_push($custom_jan_code, $order['customer_jan_code']);
                                }
                            }
                        } 
                        
                        array_push($revenues ,array(
                            'time' => $minYear,
                            'time_mark' => $minYear,
                            'total_money'=> $total_money, 
                            'count'=> $count_order, 
                            'money_per_order' => $count_order !== 0 ? $total_money/$count_order : 0,
                            'num_customer' => $num_customer,
                            'money_per_customer' => $num_customer !== 0 ? $total_money/$num_customer : 0,
                            'quantity'=>$quantity));
                    }
                    
                }
                return $revenues;
            }
        }
        return null;
    }
    
    
    public function findTotalMoneyDateStaff($storeCode = null){
        if($storeCode !== null){
            $orders = (new \yii\db\Query())
                    ->select([
                        'mst_order.total',
                        'mst_order.money_credit',
                        'mst_order.customer_jan_code',
                        'mst_order.money_ticket',
                        'mst_order.money_other',
                        'mst_order.money_cash',
                        'mst_order.point_use_money',
                        'mst_order.process_date',
                        'mst_order.process_time'])
                    ->from('mst_order')
                    ->where(['left(mst_order.order_code,5)'=>$storeCode])                    
                    ->andWhere(['mst_order.del_flg'=>'0'])
                    ->andWhere(['mst_order.process_date'=> date('Y-m-d')])                    
                        ->all();
            
            if(!empty($orders)){
                $totalMoney = 0;
                
                foreach($orders as $order){
                    $totalMoney += $order['total'];
                }
                
                return $totalMoney;
            }
        }
        
        return 0;
    }
    
    public function findtotalMoneyMonthStaff($storeCode = null){
        if($storeCode !== null){
            
            $orders = (new \yii\db\Query())
                    ->select([
                        'mst_order.total',
                        'mst_order.money_credit',
                        'mst_order.customer_jan_code',
                        'mst_order.money_ticket',
                        'mst_order.money_other',
                        'mst_order.money_cash',
                        'mst_order.point_use_money',
                        'mst_order.process_date',
                        'mst_order.process_time'])
                    ->from('mst_order')
                    ->where(['left(mst_order.order_code,5)'=>$storeCode])                    
                    ->andWhere(['mst_order.del_flg'=>'0'])
                    ->andWhere(['EXTRACT(month FROM (process_date))'=> date('m')])                    
                        ->all();
            
            if(!empty($orders)){
                $totalMoney = 0;
                
                foreach($orders as $order){
                    $totalMoney += $order['total'];
                }
                
                return $totalMoney;
            }
        }
        
        return 0;
    }
    //function for Order Tmp
    // generate booking code
    public function genOrderCode($storeId = null) {
        try{
            if($storeId === null){
                throw new Exception("Dont have this Store Id");
            }
            
            $store = MasterStore::find()->where(['id'=>$storeId])->one();
            if(empty($store)){
                throw new Exception("Cannot find Store");
            }

            $storeCode = $store['store_code'];            
            $subquery = MstOrder::find()->select('order_code')->where(['left(mst_order.order_code,5)'=>$storeCode]);
            $listOrderCode = $subquery->all();
            $listCode = array();
            foreach($listOrderCode as $order){
                $listCode[] = $order['order_code'];
            }

            $order_code = MstOrder::find()->select('order_code')->where(['order_code'=> $listCode])->max('order_code');
            //0000500000000001
            if($order_code == null){
              return $storeCode.'00000000001';
            }
            $order_code = $order_code + 1;
            $order_code = str_pad($order_code, 16, '0', STR_PAD_LEFT);

            return $order_code;

            
        }catch(\yii\base\Exception $ex){
            return null;
        }
    }
    
    public function findAllMoneyDateStaff($storeCode = null){
        if($storeCode !== null){
            $orders = (new \yii\db\Query())
                    ->select([
                        'mst_order.total',
                        'mst_order.money_credit',
                        'mst_order.customer_jan_code',
                        'mst_order.money_ticket',
                        'mst_order.money_other',
                        'mst_order.money_cash',
                        'mst_order.point_use_money',
                        'mst_order.process_date',
                        'mst_order.process_time'])
                    ->from('mst_order')
                    ->where(['left(mst_order.order_code,5)'=>$storeCode])                    
                    ->andWhere(['mst_order.del_flg'=>'0'])
                    ->andWhere(['mst_order.process_date'=> date('Y-m-d')])                    
                        ->all();
            return $orders;
        }
        
        return null;
    }
    
    public function getPayCoupons() {
        return $this->hasMany(PayCoupon::className(), ['order_code' => 'order_code']);
    }
}
