<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "booking".
 *
 * @property integer $id
 * @property integer $coupon_id
 * @property integer $customer_id
 * @property integer $shop_id
 * @property string $memo
 * @property string $memo2
 * @property string $memo3
 * @property integer $staff_id
 * @property string $fax
 * @property string $memo1
 * @property string $start_time
 * @property string $end_time
 * @property integer $seat_id
 * @property integer $menu_coupon_id
 * @property integer $status
 * @property integer $action
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $booking_date
 * @property string $demand
 * @property integer $black_list
 * @property string $del_flg
 */
class MstBookingArrivalHistory extends \yii\db\ActiveRecord
{
    // Booking Date
    public $booking_arrival_date_from;
    public $booking_arrival_date_to;
      // Chairs (Waiting QA)
    public $chairs;
    // Category Product
    public $category;
    // Name Product
    public $purchased_product_name;
    // Car Number Customer
    public $membership_card_number;
    // Name Customer
    public $name_customer;
    // Sex Customer
    public $sex;
    // Rank Customer (Waiting QA)
    public $rank_customer; 
    // Visit Customer
    public $visit;
    public $number_visit_below;
    public $number_visit_above;
    
    // Birth Date
    public $birth_date_from;
    public $birth_date_to;
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'booking';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['coupon_id', 'customer_id', 'shop_id', 'created_at', 'updated_at'], 'required'],
            [['coupon_id', 'customer_id', 'shop_id', 'staff_id', 'seat_id', 'menu_coupon_id', 'status', 'action', 'created_at', 'updated_at', 'black_list'], 'integer'],
            [['booking_date','booking_arrival_date_from','booking_arrival_date_to','chairs','category',
                'purchased_product_name','membership_card_number','name_customer','sex','rank_customer','number_visit_below','number_visit_above'
                ,'booking_date_from','booking_date_to'], 'safe'],
            [['memo', 'memo2', 'memo3', 'memo1'], 'string', 'max' => 60],
            [['fax'], 'string', 'max' => 12],
            [['start_time', 'end_time'], 'string', 'max' => 5],
            [['demand'], 'string', 'max' => 300],
            [['del_flg'], 'string', 'max' => 1],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend', 'ID'),
            'coupon_id' => Yii::t('backend', 'Coupon ID'),
            'customer_id' => Yii::t('backend', 'Customer ID'),
            'shop_id' => Yii::t('backend', 'Shop ID'),
            'memo' => Yii::t('backend', 'Memo'),
            'memo2' => Yii::t('backend', 'Memo2'),
            'memo3' => Yii::t('backend', 'Memo3'),
            'staff_id' => Yii::t('backend', 'Staff ID In Charge'),
            'fax' => Yii::t('backend', 'Fax'),
            'memo1' => Yii::t('backend', 'Memo1'),
            'start_time' => Yii::t('backend', 'Start Time'),
            'end_time' => Yii::t('backend', 'End Time'),
            'seat_id' => Yii::t('backend', 'Seat ID'),
            'menu_coupon_id' => Yii::t('backend', 'Menu Coupon ID'),
            'status' => Yii::t('backend', 'Status'),
            'action' => Yii::t('backend', 'Action'),
            'created_at' => Yii::t('backend', 'Created At'),
            'updated_at' => Yii::t('backend', 'Updated At'),
            'booking_date' => Yii::t('backend', 'Booking Date'),
            'demand' => Yii::t('backend', 'Demand'),
            'black_list' => Yii::t('backend', 'Black List'),
            'del_flg' => Yii::t('backend', 'Del Flg'),
            'booking_arrival_date_from' => Yii::t('backend', 'Booking Arrival Date From'),
            'booking_arrival_date_to' => Yii::t('backend', 'Booking Arrival Date To'),
            'chairs' => Yii::t('backend', 'Seat Use'),
            'category' => Yii::t('backend', 'Category'), 
            'purchased_product_name' => Yii::t('backend', 'Purchased Product Name'),
            'membership_card_number' => Yii::t('backend', 'Membership Card Number'),
            'name_customer' => Yii::t('backend', 'Name Customer'),
            'sex' => Yii::t('backend', 'Sex'),
            'rank_customer' => Yii::t('backend', 'Rank Customer'),
            'number_visit_below' => Yii::t('backend', 'Number Visit Below'),
            'number_visit_above' => Yii::t('backend', 'Number Visit Above'),
            'birth_date_from' => Yii::t('backend', 'Birth Date From'),
            'birth_date_to' => Yii::t('backend', 'Birth Date To'),
        ];
    }
}
