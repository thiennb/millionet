<?php

namespace common\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "post_code".
 *
 * @property integer $id
 * @property string $nation_code
 * @property string $old_post_code
 * @property string $post_code
 * @property string $country_kana_name
 * @property string $city_kana_name
 * @property string $area_kana_name
 * @property string $country_name
 * @property string $city_name
 * @property string $area_name
 * @property string $display_control_1
 * @property string $display_control_2
 * @property string $display_control_3
 * @property string $display_control_4
 * @property string $display_control_5
 * @property string $display_control_6
 */
class PostCode extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'post_code';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nation_code', 'old_post_code', 'post_code'], 'string', 'max' => 10],
            [['country_kana_name', 'city_kana_name', 'area_kana_name', 'country_name', 'city_name', 'area_name'], 'string', 'max' => 100],
            [['display_control_1', 'display_control_2', 'display_control_3', 'display_control_4', 'display_control_5', 'display_control_6'], 'string', 'max' => 1],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend', 'ID'),
            'nation_code' => Yii::t('backend', 'Nation Code'),
            'old_post_code' => Yii::t('backend', 'Old Post Code'),
            'post_code' => Yii::t('backend', 'Post Code'),
            'country_kana_name' => Yii::t('backend', 'Country Kana Name'),
            'city_kana_name' => Yii::t('backend', 'City Kana Name'),
            'area_kana_name' => Yii::t('backend', 'Area Kana Name'),
            'country_name' => Yii::t('backend', 'Country Name'),
            'city_name' => Yii::t('backend', 'City Name'),
            'area_name' => Yii::t('backend', 'Area Name'),
            'display_control_1' => Yii::t('backend', 'Display Control 1'),
            'display_control_2' => Yii::t('backend', 'Display Control 2'),
            'display_control_3' => Yii::t('backend', 'Display Control 3'),
            'display_control_4' => Yii::t('backend', 'Display Control 4'),
            'display_control_5' => Yii::t('backend', 'Display Control 5'),
            'display_control_6' => Yii::t('backend', 'Display Control 6'),
        ];
    }
    
    public static function getAddressName($code){
        
        $model = self::findOne(['post_code'=>$code]);
        if(count($model)>0) return str_replace (' ','',$model->country_name.$model->city_name.$model->area_name);

        return false;

    }

    
    public static function getListPostCode(){        
        return ArrayHelper::map(self::find()->all(), 'post_code', 'country_name');
    }
    
    public static function getListPrefecture(){        
        return ArrayHelper::map(self::find()->select(['country_name','country_name'])->groupBy(['country_name'])->all(), 'country_name', 'country_name');
    }
  }
