<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "customer_point".
 *
 * @property integer $id
 * @property integer $customer_id
 * @property integer $store_id
 * @property string $total_point
 * @property string $total_amount
 * @property string $rank_id
 * @property string $del_flg
 * @property integer $created_at
 * @property integer $created_by
 * @property integer $updated_at
 * @property integer $updated_by
 */
class CustomerPoint extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'customer_store';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['customer_id', 'store_id', 'created_at', 'created_by', 'updated_at', 'updated_by'], 'required'],
            [['customer_id', 'store_id', 'created_at', 'created_by', 'updated_at', 'updated_by'], 'integer'],
            [['total_point', 'total_amount'], 'number'],
            [['rank_id'], 'string', 'max' => 2],
            [['del_flg'], 'string', 'max' => 1],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('frontend', 'ID'),
            'customer_id' => Yii::t('frontend', 'Customer ID'),
            'store_id' => Yii::t('frontend', 'Store ID'),
            'total_point' => Yii::t('frontend', 'Total Point'),
            'total_amount' => Yii::t('frontend', 'Total Amount'),
            'rank_id' => Yii::t('frontend', 'Rank ID'),
            'del_flg' => Yii::t('frontend', 'Del Flg'),
            'created_at' => Yii::t('frontend', 'Created At'),
            'created_by' => Yii::t('frontend', 'Created By'),
            'updated_at' => Yii::t('frontend', 'Updated At'),
            'updated_by' => Yii::t('frontend', 'Updated By'),
        ];
    }
    
     public static function find()
    {
        return parent::find()->where(['customer_store.del_flg'=>'0']);
    }
}
