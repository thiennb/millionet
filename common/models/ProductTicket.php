<?php

namespace common\models;

use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use yii2tech\ar\softdelete\SoftDeleteBehavior;
use Yii;

/**
 * This is the model class for table "product_ticket".
 *
 * @property integer $id
 * @property integer $ticket_id
 * @property integer $product_id
 * @property string $del_flg
 * @property integer $created_at
 * @property integer $created_by
 * @property integer $updated_at
 * @property integer $updated_by
 */
class ProductTicket extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'product_ticket';
    }

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            TimestampBehavior::className(),
            BlameableBehavior::className(),
            'softDeleteBehavior' => [
                'class' => SoftDeleteBehavior::className(),
                'softDeleteAttributeValues' => [
                    'del_flg' => '1'
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['ticket_id', 'product_id'], 'required'],
            [['ticket_id', 'product_id', 'created_at', 'created_by', 'updated_at', 'updated_by'], 'integer'],
            [['del_flg'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => Yii::t('app', 'ID'),
            'ticket_id' => Yii::t('app', 'Ticket ID'),
            'product_id' => Yii::t('app', 'Product ID'),
            'del_flg' => Yii::t('app', 'Del Flg'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'updated_by' => Yii::t('app', 'Updated By'),
        ];
    }

    public function getProduct() {
        return $this->hasOne(MstProduct::className(), ['id' => 'product_id']);
    }

    public static function find($findAll = true) {
        if ($findAll)
            return parent::find()->where(['product_ticket.del_flg' => '0']);
        else
            return parent::find();
    }

}
