<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "booking_checklist".
 *
 * @property integer $id
 * @property integer $booking_id
 * @property integer $checklist_id
 * @property string $answer_list
 * @property string $del_flg
 * @property integer $created_at
 * @property integer $created_by
 * @property integer $updated_at
 * @property integer $updated_by
 */
class BookingChecklistView extends \yii\db\ActiveRecord
{
    public $check_list_id;
    public $type;
    public $display_flg;
    public $option;
    public $max_choice;
    public $notice_content;
    public $question_content;
    public $answer_content;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'booking_checklist';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
        ];
    }
}
