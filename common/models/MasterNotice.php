<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use yii2tech\ar\softdelete\SoftDeleteBehavior;
use common\models\SettingAutoSendNotice;

/**
 * This is the model class for table "mst_notice".
 *
 * @property integer $id
 * @property string $send_date
 * @property string $title
 * @property string $content
 * @property string $del_flg
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $type_send
 */
class MasterNotice extends \yii\db\ActiveRecord {

    //check schedule send notice: immediately or select date
    public $type_send;
    //check schedule send notice: immediately or select date
    public $type_setting;
    //status send notice
    public $status_send;
    //name of customer
    public $name;

    const PUSH_AUTOMATIC = "1";
    const PUSH_BY_HAND = "0";
    const STATUS_SEND_CANCEL = "04";
    const STATUS_SEND_DOING = "01";
    const STATUS_SEND_DONE = "02";
    const STATUS_SEND_ERROR = "03";

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'mst_notice';
    }

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            TimestampBehavior::className(),
            BlameableBehavior::className(),
            'softDeleteBehavior' => [
                'class' => SoftDeleteBehavior::className(),
                'softDeleteAttributeValues' => [
                    'del_flg' => '1'
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['type_send', 'title', 'content'], 'required'],
            [['send_date'], 'date', 'format' => 'php:Y/m/d', 'message' => Yii::t('backend', "Date format don't match")],
//            ['send_date', 'compare', 'compareValue' => date('Y/m/d'), 'operator' => '>=', 'message' => Yii::t('backend', "Please select date time great than now date"), 'when' => function($model) {
//                    if($model->send_date == date('Y/m/d'))
//                        return true;$model->type_send == '1' && $model->send_time < date('h:i');
//                    return $model->type_send == '1';
//                }],
            ['send_date', 'checkDateTime'],
            [['send_date', 'send_time'], 'required', 'when' => function ($model) {
                return $model->type_send == '1';
            }],
            [['created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['title'], 'string', 'max' => 100],
            [['content'], 'string', 'max' => 500],
            [['del_flg'], 'string', 'max' => 1],
            [['status_send'], 'string', 'max' => 2],
            [['send_time'], 'string', 'max' => 5],
            [['name'], 'safe'],
        ];
    }
    
    // ======================
    // == Check Date Time Send start
    // ======================
    public function checkDateTime($attribute, $params) {
        if($this->type_send == '1'){
            if($this->send_date < date('Y/m/d'))
                $this->addError($attribute, Yii::t('backend', "Please select date time great than now date"));
            else if($this->send_date == date('Y/m/d') && $this->send_time < date('h:i')){
                $this->addError('send_time', Yii::t('backend', "Please select date time great than now date"));
            }
            
        }
    }

    // ======================
    // == Check Date Time Send end
    // ======================
    
    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => Yii::t('backend', 'ID'),
            'send_date' => Yii::t('backend', 'Delivery start date time'),
            'start_send_date' => Yii::t('backend', 'Delivery start date time'),
            'end_send_date' => Yii::t('backend', 'Delivery start date time'),
            'send_time' => Yii::t('backend', 'Delivery start date time'),
            'title' => Yii::t('backend', 'Title'),
            'content' => Yii::t('backend', 'Note'),
            'del_flg' => Yii::t('backend', 'Del Flg'),
            'created_at' => Yii::t('backend', 'Created At'),
            'updated_at' => Yii::t('backend', 'Updated At'),
            'created_by' => Yii::t('backend', 'Created By'),
            'updated_by' => Yii::t('backend', 'Updated By'),
            'type_send' => Yii::t('backend', 'Delivery start date'),
        ];
    }

    public static function find() {
        return parent::find()->where(['mst_notice.del_flg' => '0']);
    }

    /**
     * Save schedule send notice for customer
     * @param integer $id
     * @return mixed
     */
    public function sendNotice($id, $store_id) {
        $notice_customer = new NoticeCustomer();
        $notice_customer->customer_id = $id;
        $notice_customer->notice_id = $this->id;
        $notice_customer->status_send = \common\components\Constants::DOING_DELIVERY_STATUS;
        $notice_customer->store_id = $store_id;
        $date = $this->type_send == 0 ? date('Y-m-d') : $this->send_date;
        $time = $this->type_send == 0 ? date('H:i') : $this->send_time;
        $notice_customer->push_date = strtotime($date . " " .$time. ":00");
        $notice_customer->save();
    }

    /**
     * Save schedule send notice for multiple customer
     * @return mixed
     */
    public function saveNoticeMulti($arr_customer) {
        foreach ($arr_customer as $val) {
            $this->sendNotice($val["customer_id"], $val["store_id"]);
        }
    }

    /**
     * Setting Notice use for send auto to customer
     * @param integer $id
     * @return mixed
     */
    public function settingSendAutoNotice($id, $arr_customer) {
        $setting = SettingAutoSendNotice::findOne($id);
        $setting->notice_id = $this->id;
        $setting->save();
        foreach ($arr_customer as $val) {
            $notice_customer = new NoticeCustomer();
            $notice_customer->notice_id = $this->id;
            $notice_customer->customer_id = $val;
            $notice_customer->save();
        }
    }

    public function beforeSave($insert) {
        if (parent::beforeSave($insert)) {
            $this->send_date = $this->type_send == 0 ? date('Y-m-d') : $this->send_date;
            $this->send_time = $this->type_send == 0 ? date('H:i') : $this->send_time;
            return true;
        } else {
            return false;
        }
    }

}
