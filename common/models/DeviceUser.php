<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "device_user".
 *
 * @property integer $id
 * @property string $device_token
 * @property integer $user_id
 * @property integer $store_id
 * @property string $del_flg
 * @property integer $created_at
 * @property integer $created_by
 * @property integer $updated_at
 * @property integer $updated_by
 */
class DeviceUser extends \yii\db\ActiveRecord
{
        /**
         * @inheritdoc
         */
        public static function tableName()
        {
                return 'device_user';
        }

        /**
         * @inheritdoc
         */
        public function rules()
        {
                return [
                    [['device_token', 'store_id', 'created_at', 'created_by', 'updated_at', 'updated_by'], 'required'],
                    [['user_id', 'store_id', 'created_at', 'created_by', 'updated_at', 'updated_by'], 'integer'],
                    [['device_token'], 'string', 'max' => 255],
                    [['del_flg'], 'string', 'max' => 1],
                ];
        }

        /**
         * @inheritdoc
         */
        public function attributeLabels()
        {
                return [
                    'id' => 'ID',
                    'device_token' => 'Device Token',
                    'user_id' => 'User ID',
                    'store_id' => 'Store ID',
                    'del_flg' => 'Del Flg',
                    'created_at' => 'Created At',
                    'created_by' => 'Created By',
                    'updated_at' => 'Updated At',
                    'updated_by' => 'Updated By',
                ];
        }
        
        /*
         * Set device token
         * 
         * @return json
         */
        public static function  setDeviceToken($device_token, $store_id, $user_id = null){
            $device = static::getDeviceUser($device_token, $store_id, $user_id);

            if($device){
                return $device;
            }
            
            $date = strtotime(date('Y-m-d H:i:s'));

            $device = new DeviceUser();
            $device->created_at = $date;
            $device->updated_at = $date;
            $device->device_token = $device_token;
            $device->store_id = $store_id;
            $device->user_id = $user_id;
            $device->created_by = 0;
            $device->updated_by = 0;
            $device->notice_received_flg = 1;             
            $device->coupon_received_flg = 0;

            $device->save(false);

            return $device;
        }
        
        /*
         * Is exist device token method
         * 
         * @return json
         */
        public static function getDeviceUser($device_token, $store_id, $user_id = null){
                $device = static::find()->where(['store_id' => $store_id])
                                ->andWhere(['device_token' => $device_token])
                                ->andWhere(['user_id' => $user_id]);
                
                return $device ->one();
        }
        
        /*
         * Set enable for coupon
         * 
         * @rturn true/false
         */
        public static function setNoticeReceivedFlg($store_id, $user_id, $flg){
                \Yii::$app->db->createCommand()->update(
                        'device_user',
                        ['notice_received_flg' => ($flg === true || $flg === 'true' || $flg == '1') ? 1 : 0],
                        "user_id = $user_id AND store_id = $store_id")
                        ->execute();
                
                return true;
        }
        
        /*
         * Set enable for coupon
         * 
         * @rturn true/false
         */
        public static function setCouponReceivedFlg($store_id, $user_id, $flg){
                \Yii::$app->db->createCommand()->update(
                        'device_user',
                        ['coupon_received_flg' => ($flg === true || $flg === 'true' || $flg == '1') ? 1 : 0],
                        "user_id = $user_id AND store_id = $store_id")
                        ->execute();
                
                return true;
        }
}
