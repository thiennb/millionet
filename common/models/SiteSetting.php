<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii2tech\ar\softdelete\SoftDeleteBehavior;
use yii\behaviors\BlameableBehavior;
use common\components\Constants;

/**
 * This is the model class for table "mst_setting_auto_send_notice".
 *
 * @property integer $id
 * @property string $status_send
 * @property string $push_timming
 * @property integer $hour_before_booking
 * @property integer $hour_after_register_member
 * @property integer $hour_after_accounting
 * @property integer $distance_to_shop
 * @property string $customer_jan_code
 * @property string $first_name
 * @property string $last_name
 * @property string $first_name_kana
 * @property string $last_name_kana
 * @property string $sex
 * @property string $start_birth_date
 * @property string $end_birth_date
 * @property string $start_visit_date
 * @property string $end_visit_date
 * @property integer $max_visit_number
 * @property integer $min_visit_number
 * @property string $rank_id
 * @property integer $last_staff_id
 * @property string $black_list_flg
 * @property string $del_flg
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $notice_id
 */
class SiteSetting extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'site_setting';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            
        ];
    }

    public static function find() {
        return parent::find()->where(['site_setting.del_flg' => '0']);
    }

}
