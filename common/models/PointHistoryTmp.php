<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\PointHistoryTmp;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use yii2tech\ar\softdelete\SoftDeleteBehavior;
/**
 * This is the model class for table "point_history".
 *
 * @property integer $id
 * @property integer $customer_id
 * @property string $process_date
 * @property string $process_time
 * @property string $process_type
 * @property string $process_point
 * @property string $del_flg
 * @property integer $created_at
 * @property integer $created_by
 * @property integer $updated_at
 * @property integer $updated_by
 */
class PointHistoryTmp extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public $name;
    public static function tableName()
    {
        return 'point_history_tmp';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['customer_id'], 'required'],
            [['customer_id', 'created_at', 'created_by', 'updated_at', 'updated_by'], 'integer'],
            [['process_date'], 'safe'],
            [['process_point'], 'number'],
            [['process_time'], 'string', 'max' => 5],
            [['process_type', 'del_flg'], 'string', 'max' => 1],
        ];
    }
    
    public function behaviors() {
        return [
            TimestampBehavior::className(),
            BlameableBehavior::className(),
            'softDeleteBehavior' => [
                'class' => SoftDeleteBehavior::className(),
                'softDeleteAttributeValues' => [
                    'del_flg' => '1'
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'customer_id' => Yii::t('app', 'Customer ID'),
            'process_date' => Yii::t('app', 'Process Date'),
            'process_time' => Yii::t('app', 'Process Time'),
            'process_type' => Yii::t('app', 'Process Type'),
            'process_point' => Yii::t('app', 'Process Point'),
            'del_flg' => Yii::t('app', 'Del Flg'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'updated_by' => Yii::t('app', 'Updated By'),
        ];
    }
    
    public static function find()
    {
        
        return parent::find()->where((new \common\components\FindPermission)->getPermissionCondition(self::tableName(), false, true));
    }
    
    public function getCustomerMaster() {
        return $this->hasOne(MasterCustomer::className(), ['id' => 'customer_id']);
    }
    
    public function getManagement() {
        return $this->hasOne(ManagementLogin::className(), ['management_id' =>  'staff_management_id']);
    }
    
    public function getStore()
    {   
      return $this->hasOne(MasterStore::className(), ['store_code' => 'store_code']);
    }


    public function searchPoint($customerId = null, $company_id = null)
    {
        $query = CustomerStore::find();
        // add conditions that should always apply here
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 5,
            ],
            'sort' => false
        ]);
        
        $query->select([
                        'customer_store_tmp.store_id',
                        'customer_store_tmp.customer_id',
                        'customer_store_tmp.total_point',
                        'customer_store_tmp.created_at',
                        'mst_store.name',
                        'mst_store.store_code'
                        ])->distinct();
        $query->innerJoin('mst_store', 'mst_store.id = customer_store_tmp.store_id') ; 
        $query->innerJoin('point_history_tmp', 'mst_store.store_code = point_history_tmp.store_code and mst_store.company_id = point_history_tmp.company_id') ; 
        $query->andFilterWhere(['=', 'customer_store_tmp.customer_id', $customerId]);
        $query->andFilterWhere(['=', 'point_history_tmp.customer_id', $customerId]);
        $query->andFilterWhere(['=', 'point_history_tmp.company_id', $company_id]);
        $query->andFilterWhere(['=', 'customer_store_tmp.del_flg',0]);
        $query->orderBy(['customer_store_tmp.created_at' => SORT_DESC]);   
        return $dataProvider ;
    }



    public function HistoryDetail($store_code = null, $customer_id = null, $company_id = null)
    {
        $query = PointHistoryTmp::find();
        $query->innerJoin('mst_store', 'mst_store.store_code = point_history_tmp.store_code') ; 
        $query->innerJoin('customer_store_tmp', 'customer_store_tmp.store_id = mst_store.id') ; 
        $query->select([
                        'point_history_tmp.id',
                        'point_history_tmp.customer_id',
                        'point_history_tmp.store_code',
                        'point_history_tmp.process_time',
                        'point_history_tmp.process_date',
                        'point_history_tmp.process_type',
                        'point_history_tmp.process_point',
                        'point_history_tmp.created_at',
                        'mst_store.name',
                        'customer_store_tmp.total_point',

                        ]);
        if (isset($store_code)) {
            $query->andFilterWhere(['=', 'point_history_tmp.store_code',$store_code]);
        }
        if (isset($customer_id)) {
            $query->andFilterWhere(['=', 'point_history_tmp.customer_id',$customer_id]);
        }
        if (isset($company_id)) {
            $query->andFilterWhere(['=', 'point_history_tmp.company_id',$company_id]);
        }
        $query->andFilterWhere(['=', 'point_history_tmp.del_flg',0]);
        $query->orderBy([
            'point_history_tmp.process_date' => SORT_DESC,
            'point_history_tmp.process_time' => SORT_DESC,
        ]); 
        return $query;
    }

    public function getInfoStore($store_code = null, $company_id = null)
    {
        $query = MasterStore::find();
        $query->select([
                        'mst_store.id',
                        'mst_store.name',
                        ]);
        if (isset($store_code)) {
            $query->andFilterWhere(['=', 'mst_store.store_code', $store_code]);
            $query->andFilterWhere(['=', 'mst_store.company_id', $company_id]);
        }
        $query->andFilterWhere(['=', 'mst_store.del_flg',0]);
        return $query;
    }

    public function getPoint($store_id = null,$customer_id = null)
    {
        $query = CustomerStoreTmp::find();
        $query->select([
                        'customer_store_tmp.total_point',
                        ]);
        if (isset($store_id)) {
            $query->andFilterWhere(['=', 'customer_store_tmp.store_id', $store_id]);
        }
          if (isset($customer_id)) {
            $query->andFilterWhere(['=', 'customer_store_tmp.customer_id', $customer_id]);
        }
        $query->andFilterWhere(['=', 'customer_store_tmp.del_flg',0]);
        return $query;
    }
}
