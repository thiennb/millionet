<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\MasterStore;
use yii\helpers\ArrayHelper;
use common\components\Util;
/**
 * StoreSearch represents the model behind the search form about `common\models\Store`.
 */
class StoreSearch extends MasterStore {

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['id', 'regular_holiday', 'booking_resources', 'created_at', 'updated_at'], 'integer'],
            [['tel'], 'integer', 'message' => Yii::t('backend', 'Phone Is Not Valid')],
            [['name', 'address', 'directions', 'website', 'time_open', 'time_close', 'introduce', 'image1', 'image2', 'image3', 'image4', 'image5', 'reservations_possible_time', 'store_item_1', 'store_item_2', 'store_item_3', 'store_item_4', 'store_item_5', 'store_item_6', 'store_item_7', 'store_item_8', 'store_item_9', 'store_item_10', 'created_by', 'updated_by', 'post_code'], 'safe'],
            [['show_flg'], 'boolean'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = MasterStore::find()
                // Join : Company store
                //->innerJoin('company_store', 'company_store.store_id = mst_store.id')
                // Join : Company
                ->innerJoin('company', 'company.id = mst_store.company_id');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => false
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'regular_holiday' => $this->regular_holiday,
            'mst_store.tel' => $this->tel,
            'show_flg' => $this->show_flg,
            'booking_resources' => $this->booking_resources,
        ]);
        $company_id =  Util::getCookiesCompanyId();
        $query->andFilterWhere(['like', 'mst_store.name', $this->name])
                ->andFilterWhere(['like', 'mst_store.address', $this->address])
                ->andFilterWhere(['like', 'directions', $this->directions])
                ->andFilterWhere(['like', 'website', $this->website])
                ->andFilterWhere(['like', 'time_open', $this->time_open])
                ->andFilterWhere(['like', 'time_close', $this->time_close])
                ->andFilterWhere(['like', 'introduce', $this->introduce])
                ->andFilterWhere(['like', 'image1', $this->image1])
                ->andFilterWhere(['like', 'image2', $this->image2])
                ->andFilterWhere(['like', 'image3', $this->image3])
                ->andFilterWhere(['like', 'image4', $this->image4])
                ->andFilterWhere(['like', 'image5', $this->image5])
                ->andFilterWhere(['like', 'booking_resources', $this->booking_resources])
                ->andFilterWhere(['like', 'reservations_possible_time', $this->reservations_possible_time])
                ->andFilterWhere(['like', 'store_item_1', $this->store_item_1])
                ->andFilterWhere(['like', 'store_item_2', $this->store_item_2])
                ->andFilterWhere(['like', 'store_item_3', $this->store_item_3])
                ->andFilterWhere(['like', 'store_item_4', $this->store_item_4])
                ->andFilterWhere(['like', 'store_item_5', $this->store_item_5])
                ->andFilterWhere(['like', 'store_item_6', $this->store_item_6])
                ->andFilterWhere(['like', 'store_item_7', $this->store_item_7])
                ->andFilterWhere(['like', 'store_item_8', $this->store_item_8])
                ->andFilterWhere(['like', 'store_item_9', $this->store_item_9])
                ->andFilterWhere(['like', 'staff_item_1', $this->staff_item_1])
                ->andFilterWhere(['like', 'staff_item_2', $this->staff_item_2])
                ->andFilterWhere(['like', 'staff_item_3', $this->staff_item_3])
                ->andFilterWhere(['like', 'staff_item_4', $this->staff_item_4])
                ->andFilterWhere(['like', 'show_in_booking_flg', $this->show_in_booking_flg])
                ->andFilterWhere(['like', 'show_in_notice_flg', $this->show_in_notice_flg])
                ->andFilterWhere(['like', 'show_in_coupon_flg', $this->show_in_coupon_flg])
                ->andFilterWhere(['like', 'show_in_product_category_flg', $this->show_in_product_category_flg])
                ->andFilterWhere(['like', 'show_in_my_page_flg', $this->show_in_my_page_flg])
                ->andFilterWhere(['like', 'show_in_staff_flg', $this->show_in_staff_flg])
                ->andFilterWhere(['like', 'created_by', $this->created_by])
                ->andFilterWhere(['like', 'updated_by', $this->updated_by])
                ->andFilterWhere(['like', 'mst_store.post_code', $this->post_code])
                ->andFilterWhere(['=', 'company.id', $company_id]);

        $query->orderBy(['mst_store.name' => SORT_ASC]);
        $query->groupBy(['mst_store.id']);

        return $dataProvider;
    }

    // Datpdt 29/09/2016 start
    /**
     * @return \yii\db\ActiveQuery
     */
    public static function listStore() {
        return ArrayHelper::map(self::find()->all(), 'id', 'name');
    }

    // Datpdt 29/09/2016 end
}
