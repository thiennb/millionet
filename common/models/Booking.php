<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use common\components\RegisterFromBehavior;
use yii2tech\ar\softdelete\SoftDeleteBehavior;
use Carbon\Carbon;
use yii\helpers\Html;
use common\components\Util;
use yii\db\ActiveRecord;
use yii\behaviors\AttributeBehavior;

/**
 * This is the model class for table "booking".
 *
 * @property integer $id
 * @property string $booking_code
 * @property integer $customer_id
 * @property integer $store_id
 * @property string $booking_date
 * @property string $memo
 * @property string $memo_manager
 * @property integer $staff_id
 * @property string $start_time
 * @property string $end_time
 * @property string $status
 * @property string $action
 * @property string $demand
 * @property integer $number_of_people
 * @property string $booking_price
 * @property string $booking_discount
 * @property string $booking_total
 * @property string $to_home_delivery_flg
 * @property string $customer_specify
 * @property string $del_flg
 * @property integer $created_at
 * @property integer $created_by
 * @property integer $updated_at
 * @property integer $updated_by
 * @property integer $point_use 
 */
class Booking extends \yii\db\ActiveRecord
{
    const BOOKING_STATUS_PENDING  = "01" ;
    const BOOKING_STATUS_APPROVE  = "02" ;
    const BOOKING_STATUS_FINISH   = "03" ;
    const BOOKING_STATUS_CANCEL   = "04" ;
    const BOOKING_STATUS_DENY     = "05" ;
    const BOOKING_STATUS_CANCEL_BY_CUSTOMER     = "06" ;
    const BOOKING_ABOVE     = 1 ;
    const BOOKING_BELOW     = 2 ;
    const BOOKING_EQUAL     = 3 ;

    public $full_name;
    public $phone_contact;
    public $product_name;
    public $booking_date_from;
    public $booking_date_to;
    public $productSelect;
    public $staffSelect;
    public $couponSelect;
    public $store_name, $first_name, $last_name, $booking_resources;
    public $name;
    public $booking_id;
    public $store;
    public $product;
    public $option;
    public $start_date;
    public $end_date;
    public $booking_type;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'booking';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [[ 'customer_id', 'store_id'], 'required'],
         
            [['customer_id', 'store_id', 'staff_id', 'number_of_people', 'created_at', 'created_by', 'updated_at', 'updated_by'], 'integer'],
            [['booking_date','booking_method','to_home_delivery_flg','booking_code','memo','couponSelect','productSelect','staffSelect','customer_specify'], 'safe'],
           [['booking_price', 'booking_discount', 'booking_total' ,'point_use'], 'number','numberPattern' => '/^[0-9]+(,[0-9]+)*$/'],
            [['point_use'], 'number', 'max' => 9999999.999, 'min' => 0,'numberPattern' => '/^[0-9]+(,[0-9]+)*$/'],
            [['point_use'], 'string', 'max' => 9],
            [['booking_code'], 'string', 'max' => 20],
            [['memo', 'memo_manager'], 'string', 'max' => 500],
            [['start_time', 'end_time'], 'string', 'max' => 5],
            [['status', 'action'], 'string', 'max' => 2],
            [['demand'], 'string', 'max' => 240],
            [['to_home_delivery_flg', 'del_flg'], 'string', 'max' => 1],
            [['phone_contact'], 'checkPhoneExit'],
            [['point_use'],'checkPoindValid','except'=> 'statusBooking'],
            [[ 'productSelect'], 'required','on'=>['bookingAdmin','bookingSeatAdmin'] ,'message'=>'商品またはクーポンを選択してください。'],
            [[ 'staffSelect'], 'required','on'=>'bookingAdmin' ,'message'=> '日時・スタッフを選択してください。'],
            [[ 'staffSelect'], 'required','on'=>'bookingSeatAdmin' ,'message'=> '日時・席タイプを選択してください。'],
        ];
    }
    public function behaviors() {
      return [
        TimestampBehavior::className(),
        BlameableBehavior::className(),
        RegisterFromBehavior::className(),  
        'softDeleteBehavior' => [
          'class' => SoftDeleteBehavior::className(),
          'softDeleteAttributeValues' => [
            'del_flg' => true
          ],
        ]
      ];
    }
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'booking_code' => Yii::t('backend', 'Booking Code') ,
            'customer_id' => 'Customer ID',
            
            'booking_date' => 'Booking Date',
            'memo' => 'Memo',
            'memo_manager' => 'Memo Manager',
            'staff_id' => 'Staff ID',
            'seat_id' => 'Seat ID',
            'start_time' => 'Start Time',
            'end_time' => 'End Time',
            'status' => 'Status',
            'action' => 'Action',
            'demand' => 'Demand',
            'number_of_people' => 'Number Of People',
            'booking_price' => 'Booking Price',
            'booking_discount' => 'Booking Discount',
            'booking_total' => 'Booking Total',

            'del_flg' => 'Del Flg',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
            'booking_method' => Yii::t('backend', 'Booking Method'),
            'to_home_delivery_flg' => Yii::t('backend', 'Home delivery'),
            'booking_date_from' => Yii::t('backend', 'Booking Date'),
            'booking_date_to' => Yii::t('backend', 'Booking Date'),
            'store_id' => Yii::t('backend', 'Select a store'),
            'customer_jan_code' => Yii::t('backend', 'Code Membership'),
            'customer_name' => Yii::t('backend', 'Name Customer'),
            'rank' => Yii::t('backend', 'Rank Customer'),
            'number_visit_min' => Yii::t('backend', 'Visit number'),
            'number_visit_max' => Yii::t('backend', 'Visit number'),
            'birth_date_from' => Yii::t("backend", "Birthday"),
            'birth_date_to' => Yii::t("backend", "Birthday"),
            'history_date_from' => Yii::t('backend', 'Process Date'),
            'history_date_to' => Yii::t('backend', 'Process Date'),
            'product_name' => Yii::t('backend', 'Name Product Booking'),
            'list_catogory' => Yii::t('backend', 'Category'),
            'last_visit_date_from' => Yii::t('backend', 'Last visit time'),
            'last_visit_date_to' => Yii::t('backend', 'Last visit time'),
            'last_staff_id' => Yii::t('backend', 'Last time staff'),
            'list_category' => Yii::t('backend', 'Product Category'),
            'customer_first_name' => Yii::t('backend', 'First name'),
            'customer_last_name' => Yii::t('backend', 'Last name'),
            'mobile' => Yii::t('backend', 'Phone'),
            'booking_time_min' => Yii::t('backend', 'Booking Times'),
            'booking_time_max' => Yii::t('backend', 'Booking Times'),
            'last_seat_id' => Yii::t('backend', 'Last Seat'),
            'service_staff_id' => Yii::t('backend', 'Service staff'),
            'seat_type' => Yii::t('backend', 'Type Seat Name'),
            'status' => Yii::t('backend', 'Booking status'),
            'point_use' => Yii::t('backend', 'Point use'),
            'demand' => Yii::t('backend', 'Demand'),
            'memo_manager' => Yii::t('backend', 'Memo manager'),
            'sex' => Yii::t('backend','Sex'),
            'productSelect' => '商品またはクーポンを選択してくだいさい。',
            'staffSelect' => '日時・スタッフをを選択してくだいさい。',
            'customer_specify' => \Yii::t('backend','Customer specified')
        ];
    }
    /**
     * @inheritdoc
     * @return MstBookingQuery the active query used by this AR class.
     */
//    public static function find()
//    {
//        return new MstBookingQuery(get_called_class());
//    }
    public static function find($noDel = true)
    {
        if($noDel){
            
            return parent::find()->where((new \common\components\FindPermission)->getPermissionCondition(Booking::tableName(), true));
        }
        return parent::find();
    }
//    public function beforeSave($insert)
//    {
//        if (parent::beforeSave($insert)) {
//            // Place your custom code here
//          $this->booking_date = strtotime($this->booking_date);
//
//            return true;
//        } else {
//            return false;
//        }
//    }

    public function getmasterStore() {
        return $this->hasMany(MasterStore::className(), ['id' => 'store_id']);
    }
    public function getImageBlackList()
    {
        $model = CustomerStore::find()->andWhere(['customer_id'=> $this->customer_id , 'store_id' => $this->store_id])->one();
        if(count($model) == 1 && $model->black_list_flg == "1"){
          return '<i class="fa fa-exclamation-triangle fa-2x"  style="color:red" aria-hidden="true"></i>';
        }else{
          return '';
        }

    }
    public function getTempStatus()
    {
        $model = CustomerStore::find()->andWhere(['customer_id'=> $this->customer_id , 'store_id' => $this->store_id])->one();
        if($this->status== Booking::BOOKING_STATUS_PENDING){
          return '<span class="temp-status"> 仮 </span>';
        }else{
          return '';
        }

    }
    public function getMasterCustomer()
    {
        return $this->hasOne(MasterCustomer::className(), ['id' => 'customer_id']);
    }
    
    public function getCustomerStore()
    {
        return $this->hasOne(CustomerStore::className(), ['customer_id' => 'customer_id'], ['store_id' => 'store_id']);
    }
    
    public function getOrder()
    {
        return $this->hasOne(MstOrder::className(), ['booking_id' => 'id']);
    }

    public function getStaff()
    {
        return $this->hasOne(MasterStaff::className(), ['id' => 'staff_id']);
    }

    public function getSeat()
    {
        return $this->hasOne(MasterSeat::className(), ['id' => 'seat_id']);
    }
    
    public function getListSeat()
    {
        return $this->hasMany(BookingSeat::className(), ['booking_id' => 'id'])->andWhere(['or', [ 'booking_seat.del_flg' => '0'], ['booking_seat.del_flg' => null]]);
    }

    public function getStoreMaster() {
        return $this->hasOne(MasterStore::className(), ['id' => 'store_id']);
    }

    public function getBookingProduct() {
        return $this->hasMany(BookingProduct::className(), ['booking_id' => 'id']);
    }
    
    public function getBookingCoupon() {
        return $this->hasMany(BookingCoupon::className(), ['booking_id' => 'id']);
    }
    
    public function getStoreCancelTime(){
        return $this->hasOne(MasterStore::className(), ['id' => 'store_id']);
    }
    public function beforeSave($insert) {
        if (parent::beforeSave($insert)) {

            if ($this->isNewRecord)
                $this->booking_code = $this->genBookingCode();
            return true;
        } else {
            return false;
        }
    }
    // generate booking code
    public function genBookingCode () {
      $booking_code = (int) Booking::find(false)->select('booking_code')->max('booking_code');
      if($booking_code == null){
        return '0000000001';
      }
      $booking_code = $booking_code + 1;
      $booking_code = str_pad($booking_code, 10, '0', STR_PAD_LEFT);

      return $booking_code;
    }
    
    // ======================
    // == Check Phone
    // ======================
    public function checkPhoneExit($attribute, $params) {
        if(!empty($this->mobile)){
            $check = 0;
            $check = MasterCustomer::find()->andWhere(['mobile' => $this->mobile])->count();
            if($check > 0){
                $key = $attribute;
                $this->addError($key, Yii::t('backend', "Mobile Exit"));
            }
        }
    }
    //validate point
    public function checkPoindValid($attribute, $params) {
        $model = CustomerStore::findOne(['customer_id'=>  $this->customer_id,'store_id'=>  $this->store_id]);
        $key = $attribute;
        
        if((count($model) == 0 && $this->point_use >0 )|| (!empty($this->point_use) && (int)str_replace(',', '', $this->point_use) > (int)$model->total_point)  ){

            $this->addError($key, Yii::t('backend', "Point too many"));
            
        }
        if (\Yii::$app->controllerNamespace == "backend\controllers"&& \Yii::$app->controller->action->id !='order-checkout' ) {
            $sumPrice = Yii::$app->runAction('booking/get-preview-sum-price',['point_use'=> $this->point_use]);
            if($sumPrice < 0){
                $this->addError($key, Yii::t('backend', 'Total Price must large than 0 .Please enter point use again'));
            }
        }
        
        
        
    }
    
    public  function getMoneyFromPoint() {
        if(!empty($this->point_use) ){
            $money = BookingBusiness::getMoneyFromPoint((int)str_replace(',', '', $this->point_use) );
           
            return $money;
        }
        return 0 ;
    }
    
    // Select count booking
    public function selectCountBooking() {
            
        $count_booking = self::find()->count();
        
        return $count_booking;
            
    }
    /*
     * Function get Count Booking of Store in Date for Staff
     * @param storeId : int
     */
     
    public function getBookingDateStaff($storeId = null){
        if($storeId !== null){
            return $this->find()->where(['store_id'=> $storeId, 'booking_date'=> date('Y-m-d')])->andWhere(['!=','status', '01'])->all();
        }
        
        return 0;
    }
    
    /**
     * @return \Carbon\Carbon
     */
    public function getCancelOffset() {
        return $this->getBookingTime()->subMinutes((int)$this->storeMaster->cancel_possible_time);
    }
    
    public static function findFrontEnd() {
        return parent::find(['or', [self::tableName() . '.del_flg' => '0'], [self::tableName() . '.del_flg' => null]]);
    }
    
    public function getBookingTime() {
        return Carbon::createFromFormat('Y-m-d H:i', $this->booking_date . ' ' . $this->start_time);
    }
    
    public function countBookingDeliveryHome($bookings = null){
        if($bookings !== null){
            $count = 0;
            foreach($bookings as $booking){
                if(isset($booking["to_home_delivery_flg"]))
                {    
                    if($booking["to_home_delivery_flg"] == "1")
                        $count++;
                }else{
                    break;
                }
            }
            
            return $count;
        }
        
        return 0;
    } 
}
