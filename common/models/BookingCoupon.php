<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use common\components\RegisterFromBehavior;
use yii2tech\ar\softdelete\SoftDeleteBehavior;
use Carbon\Carbon;
use yii\helpers\Html;
use common\components\Util;
use yii\db\ActiveRecord;
use yii\behaviors\AttributeBehavior;

/**
 * This is the model class for table "booking_coupon".
 *
 * @property integer $id
 * @property integer $booking_id
 * @property integer $coupon_id
 * @property integer $quantity
 * @property string $unit_price
 * @property string $del_flg
 * @property integer $created_at
 * @property integer $created_by
 * @property integer $updated_at
 * @property integer $updated_by
 */
class BookingCoupon extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'booking_coupon';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['booking_id', 'coupon_id'], 'required'],
            [['booking_id', 'coupon_id', 'quantity', 'created_at', 'created_by', 'updated_at', 'updated_by'], 'integer'],
            [['unit_price'], 'number'],
            [['del_flg'], 'string', 'max' => 1],
        ];
    }

    public function behaviors() {
      return [
        TimestampBehavior::className(),
        BlameableBehavior::className(),
        RegisterFromBehavior::className(), 
        'softDeleteBehavior' => [
          'class' => SoftDeleteBehavior::className(),
          'softDeleteAttributeValues' => [
            'del_flg' => true
          ],
        ]
      ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => Yii::t('frontend', 'ID'),
            'booking_id' => Yii::t('frontend', 'Booking ID'),
            'coupon_id' => Yii::t('frontend', 'Coupon ID'),
            'quantity' => Yii::t('frontend', 'Quantity'),
            'unit_price' => Yii::t('frontend', 'Unit Price'),
            'del_flg' => Yii::t('frontend', 'Del Flg'),
            'created_at' => Yii::t('frontend', 'Created At'),
            'created_by' => Yii::t('frontend', 'Created By'),
            'updated_at' => Yii::t('frontend', 'Updated At'),
            'updated_by' => Yii::t('frontend', 'Updated By'),
        ];
    }

    public static function find() {
        return parent::find()->where([BookingCoupon::tableName().'.del_flg' => '0']);
    }
    
    public function getCoupon()
    {
        return $this->hasOne(MasterCoupon::className(), ['id' => 'coupon_id']);
    }
}
