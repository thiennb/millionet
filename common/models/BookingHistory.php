<?php

namespace common\models;

use \yii\db\Query;
use yii\data\ActiveDataProvider;

class BookingHistory extends BookingSearch {

    public function searchHistory($params, $customer = false) {
        $query = Booking::find();
        // add conditions that should always apply here
        //join booking product
        $query->innerJoin(MasterStore::tableName(), Booking::tableName() . '.store_id=' . MasterStore::tableName() . '.id'
                . ' and ' . MasterStore::tableName() . '.del_flg = :zero');
        /* $query->leftJoin(BookingCoupon::tableName(),
          BookingCoupon::tableName() . '.booking_id=' . Booking::tableName() . '.id'
          . ' and ' . BookingCoupon::tableName() . '.del_flg = :zero');
          $query->leftJoin(MasterCoupon::tableName(),
          BookingCoupon::tableName() . '.coupon_id = ' . MasterCoupon::tableName() . '.id'
          . ' and ' . MasterCoupon::tableName() . '.del_flg = :zero');
          $query->leftJoin(BookingProduct::tableName(),
          BookingProduct::tableName() . '.booking_id = ' . Booking::tableName() . '.id'
          . ' and ' . BookingProduct::tableName() . '.del_flg = :zero');
          $query->leftJoin(MstProduct::tableName(),
          BookingProduct::tableName() . '.product_id = ' . MstProduct::tableName() . '.id'
          . ' and ' . MstProduct::tableName() . '.del_flg = :zero'); */
        $query->addParams([
            'zero' => '0'
        ]);
        $query->orderBy([
            'booking_date' => SORT_ASC,
            'start_time' => SORT_ASC
        ]);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 3,
            ],
            'sort' => false
        ]);
        $this->load($params);
        if (!$this->validate() || $customer === null) {
            // uncomment the following line if you do not want to return any records when validation fails
            $query->where('0=1');
            return $dataProvider;
        }
        if ($customer !== false) {
            $query->andFilterWhere(['=', 'customer_id', $customer->id]);
        }
        
        if (!empty($this->booking_date_from)) {
            $query->andFilterWhere(['>=', 'booking_date', $this->booking_date_from]);
        }
        
        if (!empty($this->booking_date_to)) {
            $query->andFilterWhere(['<=', 'booking_date', $this->booking_date_to]);
        }
        
        if (!empty($this->store_id)) {
            $query->andFilterWhere(['=', Booking::tableName() . '.store_id', $this->store_id]);
        }
        
        $booking_statuses = \common\components\Constants::LIST_BOOKING_STATUS;
        array_pop($booking_statuses);
        array_pop($booking_statuses);
        
        if (empty($this->list_status)) {
            $this->list_status = array_keys($booking_statuses);
        }
        
        if (array_search('04', $this->list_status) !== false) {
            $this->list_status[] = '06';
        }
        //================
        $arrSts = [];
        foreach ($this->list_status as $sts) {
            $arrSts[] = [Booking::tableName() . '.status' => $sts];
        }
        $query->andFilterWhere(array_merge(['OR'], $arrSts));
        $query->orderBy([
            'created_at' => SORT_DESC
        ]);
        //===============
        $query->groupBy('booking.id');
        return $dataProvider;
    }

    public static function searchProducts($booking_id) {
        $query = MstProduct::findFrontEnd()
                ->select([
                    MstProduct::tableName() . '.*',
                    'option_name' => MasterOption::tableName() . '.name',
                    'total_price' => BookingProduct::tableName() . '.unit_price'
                ])
                ->innerJoin(BookingProduct::tableName(), BookingProduct::tableName() . '.booking_id = :bookingId'
                        . ' and ' . BookingProduct::tableName() . '.product_id = ' . MstProduct::tableName() . '.id'
                        . ' and ' . BookingProduct::tableName() . '.del_flg = :zero'
                )
                ->leftJoin(MasterOption::tableName(), MasterOption::tableName() . '.id = ' . BookingProduct::tableName() . '.option_id');
        $query->addParams([
            'zero' => '0',
            'bookingId' => $booking_id
        ]);

        $query->orderBy([
            MstProduct::tableName() . '.assign_fee_flg' => SORT_ASC,
            BookingProduct::tableName() . '.option_id' => SORT_DESC
        ]);

        return $query;
    }

    public static function searchCoupons($booking_id) {
        $query = MasterCoupon::findFrontEnd()
                ->select([
                    MasterCoupon::tableName() . '.*',
                    'total_price' => BookingCoupon::tableName() . '.unit_price'
                ])
                ->innerJoin(BookingCoupon::tableName(), BookingCoupon::tableName() . '.booking_id = :bookingId'
                        . ' and ' . BookingCoupon::tableName() . '.coupon_id = ' .  MasterCoupon::tableName() . '.id'
                        . ' and ' . BookingCoupon::tableName() . '.del_flg = :zero');
        $query->addParams([
            'zero' => '0',
            'bookingId' => $booking_id
        ]);

        return $query;
    }
}
