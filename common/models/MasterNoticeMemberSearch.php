<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\MasterCustomer;

/**
 * MasterNoticeMemberSearch represents the model behind the search form about `common\models\MasterNoticeMember`.
 */
class MasterNoticeMemberSearch extends MasterNoticeMember {

    
    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['customer_jan_code', 'first_name', 'last_name', 'first_name_kana', 'last_name_kana',
            'sex', 'start_birth_date', 'end_birth_date', 'start_visit_date', 'end_visit_date', 'max_visit_number',
            'min_visit_number', 'rank_id', 'last_staff', 'black_list', 'store_id'], 'safe'],
            //--------------------------------
            [['start_birth_date', 'end_birth_date', 'start_visit_date', 'end_visit_date'], 'date', 'format' => 'php:Y/m/d'],
            //['end_birth_date', 'compare', 'compareAttribute' => 'start_birth_date', 'operator' => '>=', 'message' => Yii::t('backend', "End date must great than Start date"), 'skipOnEmpty' => TRUE],
            //['end_visit_date', 'compare', 'compareAttribute' => 'start_visit_date', 'operator' => '>=', 'message' => Yii::t('backend', "End date must great than Start date"), 'skipOnEmpty' => TRUE],
            //['max_visit_number', 'compare', 'compareAttribute' => 'min_visit_number', 'operator' => '>=', 'message' => Yii::t('backend', "End date must great than Start date"), 'skipOnEmpty' => TRUE],
            [['first_name_kana', 'last_name_kana'], 'match', 'pattern' => '/^[ァ-ヴー]+$/u', 'message' => Yii::t('backend', '{attribute} invalid characters full size only')],
            [['first_name', 'last_name'], 'match', 'not' => true, 'pattern' => '/[\'\/~`\!@#\$%\^&\*\(\)_\-\+=\{\}\[\]\|;:"\<\>,\.\?\\\]/', 'message' => Yii::t('backend', '{attribute} invalid characters')],
            //---------------------------------
            [['first_name', 'last_name', 'first_name_kana', 'last_name_kana'], 'string', 'max' => 40],
            [['customer_jan_code'], 'string', 'max' => 13],
            [['min_visit_number', 'max_visit_number'], 'string', 'max' => 6],
            [['min_visit_number', 'max_visit_number'], 'integer', 'max' => 99999]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'customer_jan_code' => Yii::t('backend', 'Membership card number'),
            'first_name' => Yii::t('backend', 'First name'),
            'last_name' => Yii::t('backend', 'Last name'),
            'first_name_kana' => Yii::t('backend', 'First name kana'),
            'last_name_kana' => Yii::t('backend', 'Last name kana'),
            'sex' => Yii::t('backend', 'Sex'),
            'start_birth_date' => Yii::t('backend', 'start_birth_date'),
            'end_birth_date' => Yii::t('backend', 'end_birth_date'),
            'start_visit_date' => Yii::t('backend', 'Last visit time'),
            'end_visit_date' => Yii::t('backend', 'Last visit time'),
            'max_visit_number' => Yii::t('backend', 'Visit number'),
            'min_visit_number' => Yii::t('backend', 'Visit number'),
            'rank_id' => Yii::t('backend', 'Customers rank'),
            'last_staff' => Yii::t('backend', 'Last time staff'),
            'black_list' => Yii::t('backend', 'black_list'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = CustomerStore::find();
        $query->joinWith('customer', true, 'INNER JOIN');
        //$query->innerJoin('mst_customer', 'mst_customer.id = customer_store.customer_id');
        // add conditions that should always apply here
        $query->andWhere(['mst_customer.company_id' => \common\components\Util::getCookiesCompanyId()]);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => false,
            'sort' => false
        ]);
        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        if (!($this->last_staff == '')) {
            $command = "(SELECT mst_order.order_management_id staff_id FROM mst_order WHERE customer_jan_code = 
                        (SELECT customer_jan_code FROM mst_customer WHERE id = customer_store.customer_id) ORDER BY SUBSTRING(mst_order.order_code,6,11) DESC LIMIT 1)";

            $query->andWhere([
                'IN',$command, $this->last_staff
            ]);
        }
        $query->andFilterWhere([
            'mst_customer.sex' => $this->sex,
            'customer_store.rank_id' => $this->rank_id,
            //'customer_store.last_staff_id' => $this->last_staff,
            'customer_store.store_id' => $this->store_id,
        ]);
        if ($this->black_list == 1) {
            $query->andWhere(['OR', ['customer_store.black_list_flg' => '0'], ['customer_store.black_list_flg' => null]]);
        }

        $query->andFilterWhere(['=', 'mst_customer.customer_jan_code', $this->customer_jan_code])
                ->andFilterWhere(['like', 'mst_customer.first_name', $this->first_name])
                ->andFilterWhere(['like', 'mst_customer.last_name', $this->last_name])
                ->andFilterWhere(['like', 'mst_customer.first_name_kana', $this->first_name_kana])
                ->andFilterWhere(['like', 'mst_customer.last_name_kana', $this->last_name_kana])
                ->andFilterWhere(['>=', 'mst_customer.birth_date', $this->start_birth_date])
                ->andFilterWhere(['<=', 'mst_customer.birth_date', $this->end_birth_date])
                ->andFilterWhere(['>=', 'customer_store.last_visit_date', $this->start_visit_date])
                ->andFilterWhere(['<=', 'customer_store.last_visit_date', $this->end_visit_date])
                ->andFilterWhere(['>=', 'customer_store.number_visit', $this->min_visit_number])
                ->andFilterWhere(['<=', 'customer_store.number_visit', $this->max_visit_number]);
        $query->orderBy(['customer_store.last_visit_date' => 'ASC']);
        return $dataProvider;
    }

}
