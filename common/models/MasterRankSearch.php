<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\MasterRank;

/**
 * MasterRankSearch represents the model behind the search form about `common\models\MasterRank`.
 */
class MasterRankSearch extends MasterRank
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'rank_apply_time', 'bronze_time_visit', 'bronze_point_total', 'sliver_time_visit', 'sliver_point_total', 'gold_time_visit', 'gold_point_total', 'black_time_visit', 'black_point_total', 'created_at', 'created_by', 'updated_at', 'updated_by'], 'integer'],
            [['rank_condition', 'del_flg'], 'safe'],
            [['bronze_point_rate', 'bronze_use_money', 'sliver_point_rate', 'sliver_use_money', 'gold_point_rate', 'gold_use_money', 'black_point_rate', 'black_use_money'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MasterRank::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'rank_apply_time' => $this->rank_apply_time,
            'bronze_point_rate' => $this->bronze_point_rate,
            'bronze_time_visit' => $this->bronze_time_visit,
            'bronze_point_total' => $this->bronze_point_total,
            'bronze_use_money' => $this->bronze_use_money,
            'sliver_point_rate' => $this->sliver_point_rate,
            'sliver_time_visit' => $this->sliver_time_visit,
            'sliver_point_total' => $this->sliver_point_total,
            'sliver_use_money' => $this->sliver_use_money,
            'gold_point_rate' => $this->gold_point_rate,
            'gold_time_visit' => $this->gold_time_visit,
            'gold_point_total' => $this->gold_point_total,
            'gold_use_money' => $this->gold_use_money,
            'black_point_rate' => $this->black_point_rate,
            'black_time_visit' => $this->black_time_visit,
            'black_point_total' => $this->black_point_total,
            'black_use_money' => $this->black_use_money,
            'created_at' => $this->created_at,
            'created_by' => $this->created_by,
            'updated_at' => $this->updated_at,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'rank_condition', $this->rank_condition])
            ->andFilterWhere(['like', 'del_flg', $this->del_flg]);

        return $dataProvider;
    }
}
