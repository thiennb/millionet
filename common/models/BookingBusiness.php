<?php

namespace common\models;

use Yii;
use Carbon\Carbon;
use yii\data\ActiveDataProvider;
use common\models\Booking;
use common\models\MasterNotice;
use common\models\MasterCustomer;
use common\models\CustomerStore;
use common\models\SettingAutoSendNotice;
use common\models\PointSetting;
use yii\helpers\ArrayHelper;
use yii\db\Query;
use yii\db\Expression;

/**
 * This is the model class for table "booking".
 *
 */
class BookingBusiness extends \yii\base\Model {

    const PUSH_TIMING_BOOKING = '01';
    const PUSH_TIMING_BOOKING_BEFORE = '02';
    const PUSH_TIMING_REGISTER_MEMBER = '03';
    const PUSH_TIMING_REGISTER_MEMBER_AFTER = '04';
    const PUSH_TIMING_PAY_AFTER = '05';
    const PUSH_TIMING_DISTANCE = '06';

    /**
     * Coupon types in price_table
     */
    const COUPON_TYPE_DISCOUNT_ALL_YEN = 1;
    const COUPON_TYPE_DISCOUNT_SUBSET_YEN = 3;
    const COUPON_TYPE_DISCOUNT_ALL_PERCENT = 2;
    const COUPON_TYPE_DISCOUNT_SUBSET_PERCENT = 4;
    const COUPON_TYPE_PRICE_SET = 5;
    const COUPON_TYPE_PRICE_SET_NONTAXED = 6;
    const COUPON_TYPE_DRINK_EAT = 7;
    const COUPON_TYPE_DRINK_EAT_NONTAXED = 8;
    const NOTICE_STATUS_ENABLE = '00';
    const NOTICE_STATUS_DISABLE = '01';

    /**
     * function register notion for query
     * 
     * @return mixed
     */
    public static function PushMessage($status = '01', $idParam1 = null, $idParam2 = null) {
        //return true if not have param
        if ($idParam1 == null) {
            return true;
        }
        $query = SettingAutoSendNotice::find();
        $modelTemp = new SettingAutoSendNotice();
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => false,
            ],
            'sort' => false
        ]);
        $notice_id = $customer_id = $store_id = $push_date = 0;

        $push_date = Carbon::now();
        switch ($status) {
            case self::PUSH_TIMING_BOOKING :
                //case is notice when booking
                //$idParam1 is bookingId

                $modelBooking = Booking::findOne($idParam1);
                $model = MasterCustomer::findOne($modelBooking->customer_id);
                $modelCustormerStore = CustomerStore::find()
                        ->where(['customer_id' => $model->id, 'store_id' => $model->register_store_id])
                        ->one();
                $customer_id = $model->id;
                $store_id = $modelBooking->store_id;
                $params = self::convertModelToArray($model, $modelCustormerStore, $modelBooking);
                $query->andWhere(['push_timming' => self::PUSH_TIMING_BOOKING]);

                break;
            case self::PUSH_TIMING_BOOKING_BEFORE :
                //case is notice when booking
                //$idParam1 is bookingId

                $modelBooking = Booking::findOne($idParam1);
                $model = MasterCustomer::findOne($modelBooking->customer_id);
                $modelCustormerStore = CustomerStore::find()
                        ->where(['customer_id' => $model->id, 'store_id' => $model->register_store_id])
                        ->one();
                $customer_id = $model->id;
                $store_id = $modelBooking->store_id;
                $params = self::convertModelToArray($model, $modelCustormerStore, $modelBooking);
                $query->andWhere(['push_timming' => self::PUSH_TIMING_BOOKING_BEFORE]);
                $push_date = Carbon::parse($modelBooking->booking_date . ' ' . $modelBooking->start_time);

                break;
            case self::PUSH_TIMING_REGISTER_MEMBER_AFTER :
                //case is notice when regidter member
                //$idParam1 is custormer id 
                $model = MasterCustomer::findOne($idParam1);
                $modelCustormerStore = CustomerStore::find()
                        ->where(['customer_id' => $model->id, 'store_id' => $model->register_store_id])
                        ->one();
                $params = self::convertModelToArray($model, $modelCustormerStore);
                $query->andWhere(['push_timming' => self::PUSH_TIMING_REGISTER_MEMBER_AFTER]);
                $customer_id = $model->id;
                $store_id = $model->register_store_id;

                break;
            case self::PUSH_TIMING_REGISTER_MEMBER :
                //case is notice when regidter member
                //$idParam1 is custormer id 
                $model = MasterCustomer::findOne($idParam1);
                $modelCustormerStore = CustomerStore::find()
                        ->where(['customer_id' => $model->id, 'store_id' => $model->register_store_id])
                        ->one();
                $params = self::convertModelToArray($model, $modelCustormerStore);
                $query->andWhere(['push_timming' => self::PUSH_TIMING_REGISTER_MEMBER]);
                $customer_id = $model->id;
                $store_id = $model->register_store_id;
                break;
            default:
                break;
        }
//                        \common\components\Util::d($params);
        //filter by param 
        foreach ($params as $key => $value) {
            if ($modelTemp->hasAttribute($key) && $key != 'black_list_flg') {
                $query->andWhere(['or', ['=', $key, empty($value) ? null : $value], ['is', $key, null]]);
            }
            if ($key == 'birth_date') {
                $query->andWhere(['or', ['<=', 'start_birth_date', $value], ['is', 'start_birth_date', null]])
                        ->andWhere(['or', ['>=', 'end_birth_date', $value], ['is', 'end_birth_date', null]]);
            }
            if ($key == 'last_visit_date') {
                $query->andWhere(['or', ['<=', 'start_visit_date', $value], ['is', 'start_visit_date', null]])
                        ->andWhere(['or', ['>=', 'end_visit_date', $value], ['is', 'end_visit_date', null]]);
            }
            if ($key == 'number_visit') {
                $query->andWhere(['or', ['>=', 'min_visit_number', $value], ['is', 'min_visit_number', null]])
                        ->andWhere(['or', ['<=', 'max_visit_number', $value], ['is', 'max_visit_number', null]]);
            }
            if ($key == 'black_list_flg' && $value == '1') {
                $query->andWhere(['or', ['=', 'black_list_flg', $value], ['is', 'black_list_flg', null]]);
            }
            if ($key == 'last_staff_id' && !empty($value)) {
                $command = "(SELECT order_detail.order_management_id staff_id FROM mst_order WHERE customer_jan_code = 
                        (SELECT customer_jan_code FROM mst_customer WHERE id = customer_store.customer_id) ORDER BY SUBSTRING(mst_order.order_code,6,11) DESC LIMIT 1)";
                $query->andWhere([
                    'IN', $command, $value
                ]);
            }
            //TODO : tim theo last_staff_id
        }
        $query->andWhere(['status_send' => self::NOTICE_STATUS_ENABLE]);
        //get result from query
        $result = $dataProvider->getModels();
//        \common\components\Util::d($result);
        if (!empty($result)) {
            foreach ($result as $modelNotice) {
                $noticeCustormer = new NoticeCustomer();
                $noticeCustormer->notice_id = $modelNotice->notice_id;
                $noticeCustormer->customer_id = $customer_id;
                $noticeCustormer->store_id = $store_id;
                $noticeCustormer->status_send = MasterNotice::STATUS_SEND_DOING;
                switch ($status) {
                    case self::PUSH_TIMING_BOOKING_BEFORE:
                        $noticeCustormer->push_date = $push_date->subHour($modelNotice->hour_before_booking)->timestamp;
                        break;
                    case self::PUSH_TIMING_REGISTER_MEMBER_AFTER:
                        $noticeCustormer->push_date = $push_date->addHour($modelNotice->hour_before_booking)->timestamp;
                        break;
                    default:
                        $noticeCustormer->push_date = $push_date->timestamp;
                        break;
                }
                $noticeCustormer->save();
            }
        }
//        \common\components\Util::d($query);
        return true;
    }

    public static function convertModelToArray($model, $attributes = null) {
        $result = [];
        if (!is_array($attributes)) {
            // validating multiple models
            $models = func_get_args();
            $attributes = null;
        } else {
            $models = [$model];
        }
        /* @var $model Model */
        foreach ($models as $model) {

            if (isset($model->attributes)) {
                $result = array_merge($result, $model->attributes);
            }
        }
        unset($result['created_at']);
        unset($result['updated_at']);
        unset($result['created_by']);
        unset($result['updated_by']);
        unset($result['del_flg']);
        unset($result['id']);
        return $result;
    }

    /**
     * 
     * @param string $coupons  list of ids separated by commas
     * @param string $products list of ids separated by commas
     * @param string $options  list of ids separated by commas, format option_id/product_id/option_product_id
     * @param string $date     Y-m-d
     * @return array
     *  coupons     array list of coupons : mode = coupon, id, name, quantity, type, products, unit_price, total_price
     *  products    array list of products: mode = product|staff, id, name, quantity, assign_fee_flg, unit_price, total_price
     *  options     array list of options : mode = option, option_id, product_id, option_product_id, quantity, name, unit_price, total_price
     *  total_price integer final price
     *  total_taxes
     *  total_external_taxes
     *  sub_total_price
     */
    public static function getPriceTable($coupons = '', $products = '', $options = '', $date = '') {
        $coupon_ids = self::validateIds($coupons) ? array_filter(explode(',', $coupons)) : [];
        $product_ids = self::validateIds($products) ? array_filter(explode(',', $products)) : [];
        $option_ids = self::validateOptionIds($options) ? array_filter(explode(',', $options)) : [];
        /*
         * @var $product_temp_prices array
         * cache temporary prices to discount yen before percent
         * merged for products and options
         */
        $product_temp_prices = [];
        /*
         * @var $all_yen_coupon_lists array
         * defer list of all-yen coupon list
         */
        $all_yen_coupon_list = [];
        /*
         * @var $all_percent_coupon_list array
         * defer list of all-% coupon list
         */
        $all_percent_coupon_list = [];
        /*
         * @var $temp_tax_rates
         * hold all tax money to add last
         */
        $temp_tax_money = [];
        /*
         * Treat coupons like products
         * @var $coupon_temp_prices
         */
        $coupon_temp_prices = [];
        $temp_external_tax_money = [];
        /*
         * @var $subset_percent_discount_list
         * hold all subset percent discount amounts
         * 99% A + 1% A = 100% A ( A => 0 )
         */
        $subset_percent_discount_list = [];

        if (empty($date)) {
            $date = date('Y-m-d');
        }

        /** query price tables * */
        $search_product_ids = $product_ids;
        foreach ($option_ids as $option_product_searchby) {
            $ex = explode('/', $option_product_searchby);
            $search_product_ids[] = $ex[1];
        }
        $product_price_table = ArrayHelper::index(count($search_product_ids) ? self::getProductPriceTable(implode(',', $search_product_ids), $date)->all() : [], 'id');
        $coupon_price_table = ArrayHelper::index((count($coupon_ids)) ? self::getCouponPriceTable(implode(',', $coupon_ids), $date)->orderBy(['type' => SORT_ASC])->all() : [], 'id');

        /* build product list */
        $product_list = [];
        foreach ($product_ids as $product_id) {
            // quantity implementation
            if (isset($product_list[$product_id])) {
                $product_temp_prices[$product_id] += $product_list[$product_id]['unit_price'];
                $product_list[$product_id]['quantity'] += 1;
                continue;
            }

            $product_list_item = [];
            $product_list_item['id'] = $product_id;
            $product_list_item['quantity'] = 1;
            $product_list_item['mode'] = 'product';
            if (isset($product_price_table[$product_id])) {
                $price_item = $product_price_table[$product_id];
                $product_list_item['unit_price'] = (float) $price_item['unit_price'];
                $product_list_item['total_price'] = (float) $price_item['total_price'];
                $product_list_item['assign_fee_flg'] = $price_item['assign_fee_flg'];
                $product_list_item['name'] = $price_item['name'];
                $product_list_item['mode'] = $price_item['assign_fee_flg'] == '1' ? 'staff' : 'product';
                $product_list_item['tax_rate'] = (int) $price_item['tax_rate'];
                $product_list_item['tax_display_method'] = $price_item['tax_display_method'];
            } else {
                $product_list_item['unit_price'] = 0;
                $product_list_item['total_price'] = 0;
                $product_list_item['assign_fee_flg'] = '0';
                $product_list_item['name'] = '';
                $product_list_item['tax_rate'] = 0;
                $product_list_item['tax_display_method'] = 'product';
            }

            $product_temp_prices[$product_id] = $product_list_item['unit_price']; // 25-11-2016 aHoang discount truoc roi tinh thue sau
            $product_list[$product_id] = $product_list_item;
            $temp_tax_rates[$product_id] = $product_list_item['tax_rate'];
        }
        /* end build product list */

        /* build option list */
        $option_list = [];
        foreach ($option_ids as $option_product_searchby) {
            $ex = explode('/', $option_product_searchby);
            $option_list_item = [];
            $option_list_item['mode'] = 'option';
            $option_list_item['option_id'] = $ex[0];
            $option_list_item['product_id'] = $ex[1];
            $option_list_item['option_product_id'] = $ex[2];
            $option_list_item['quantity'] = 1;

            if (isset($product_price_table[$ex[1]])) {
                $price_item = $product_price_table[$ex[1]];
                $option_list_item['unit_price'] = (float) $price_item['unit_price'];
                $option_list_item['total_price'] = (float) $price_item['total_price'];
                $option_list_item['name'] = MasterOption::findOne($ex[0])->name . ' : ' . $price_item['name'];
                $option_list_item['tax_rate'] = (int) $price_item['tax_rate'];
                $option_list_item['tax_display_method'] = $price_item['tax_display_method'];
            } else {
                $option_list_item['unit_price'] = 0;
                $option_list_item['total_price'] = 0;
                $option_list_item['name'] = '';
                $option_list_item['tax_rate'] = 0;
                $option_list_item['tax_display_method'] = null;
            }
            $option_list[$option_product_searchby] = $option_list_item;
            // merge options and products for discounting together
            $product_id = $option_list_item['product_id'];
            if (isset($product_temp_prices[$product_id])) {
                $product_temp_prices[$product_id] += $option_list_item['unit_price'];
            } else {
                $product_temp_prices[$product_id] = $option_list_item['unit_price'];
                //$product_list[$product_id] = $option_list_item['unit_price'];
            }
            $temp_tax_rates[$product_id] = $option_list_item['tax_rate'];
        }
        /* end build option list */
        /* build coupon list */
        $coupon_list = [];
        foreach ($coupon_price_table as $coupon_id => $coupon_price_item) {
            $coupon_list_item = [];
            $coupon_list_item['mode'] = 'coupon';
            $coupon_list_item['id'] = $coupon_id;
            $coupon_list_item['quantity'] = 1;
            if (!isset($coupon_price_table[$coupon_id])) {
                $coupon_list_item['unit_price'] = 0;
                $coupon_list_item['total_price'] = 0;
                //$coupon_list_item['type'] = '0';
                $coupon_list_item['name'] = '';
                $coupon_list_item['type'] = 0;
                $coupon_list_item['products'] = '';
                $coupon_list_item['tax_rate'] = 0;
            } else {
                $price_item = $coupon_price_table[$coupon_id];

                $coupon_list_item['name'] = $price_item['name'];
                $coupon_list_item['type'] = $price_item['type'];
                $coupon_list_item['products'] = $price_item['products'];
                $coupon_list_item['tax_rate'] = (int) $price_item['tax_rate'];
                // this coupon list is sorted by type
                // calculating processes :
                //  prepare unit prices (unit_price / quantity => TEMP_PRICE)
                //  defer allyen
                //  defer allpercent
                //  -subset yen to unit_price(TEMP_PRICE)
                //  -subset % to unit price(TEMP_PRICE)
                //  SUM
                //  -allyen for each product, seperated amount for each product is calculate by TEMP_PRICE / SUM ( saved to TEMP_PRICE )
                //  -allpercent to SUM
                //  calcalate tax for each product by its TEMP_PRICE
                //  SUM_TAX
                //  SUM_TEMP_PRICE + SUM_SET_COUPONS + SUM_DRINKEAT_COUPONS + SUM_TAX
                switch ($price_item['type']) {
                    case self::COUPON_TYPE_DISCOUNT_ALL_YEN :
                        $coupon_list_item['unit_price'] = (float) ('-' . $price_item['discount_yen']); // < 0
                        $coupon_list_item['total_price'] = $coupon_list_item['unit_price'];
                        $all_yen_coupon_list[] = $coupon_id;
                        break;
                    case self::COUPON_TYPE_DISCOUNT_ALL_PERCENT :
                        $coupon_list_item['unit_price'] = (float) ('-' . $price_item['discount_percent']); // < 0
                        $coupon_list_item['total_price'] = 0;
                        $all_percent_coupon_list[] = $coupon_id;
                        break;
                    case self::COUPON_TYPE_DISCOUNT_SUBSET_YEN :
                        $coupon_list_item['unit_price'] = (float) ('-' . $price_item['discount_yen']); // x < 0
                        $coupon_products = array_filter(explode(',', $coupon_list_item['products']));
                        $discount_total = 0;
                        // discount each product only once
                        foreach ($coupon_products as $product_id) {
                            if (isset($product_temp_prices[$product_id])) {
                                if (abs($coupon_list_item['unit_price']) > $product_temp_prices[$product_id]) {
                                    $discounted_amount = -$product_temp_prices[$product_id];
                                } else {
                                    $discounted_amount = $coupon_list_item['unit_price'];
                                }
                                $product_temp_prices[$product_id] += $discounted_amount;
                                $discount_total += $discounted_amount;
                            }
                        }
                        $coupon_list_item['total_price'] = $discount_total;
                        break;
                    case self::COUPON_TYPE_DISCOUNT_SUBSET_PERCENT :
                        $coupon_list_item['unit_price'] = (float) ('-' . $price_item['discount_percent']); // x < 0
                        $coupon_list_item['total_price'] = 0;
                        $coupon_products = array_filter(explode(',', $coupon_list_item['products']));
                        // discount each product many times
                        foreach ($coupon_products as $product_id) {
                            if (isset($product_temp_prices[$product_id])) {
                                if (!isset($subset_percent_discount_list[$product_id])) {
                                    $subset_percent_discount_list[$product_id] = [];
                                }

                                $subset_percent_discount_list[$product_id][$coupon_id] = $coupon_list_item['unit_price'];
                            }
                        }
                        break;
                    case self::COUPON_TYPE_PRICE_SET :
                    case self::COUPON_TYPE_PRICE_SET_NONTAXED :
                        $coupon_list_item['unit_price'] = (float) $price_item['discount_price_set'];
                        $coupon_list_item['total_price'] = $coupon_list_item['unit_price'];
                        $coupon_temp_prices[$coupon_id] = $coupon_list_item['unit_price'];
                        break;
                    /** !!!!!!! DISCOUNT THESE SHITS * */
                    case self::COUPON_TYPE_DRINK_EAT :
                    case self::COUPON_TYPE_DRINK_EAT_NONTAXED :
                        $coupon_list_item['unit_price'] = (float) $price_item['discount_drink_eat'];
                        $coupon_list_item['total_price'] = $coupon_list_item['unit_price'];
                        $coupon_temp_prices[$coupon_id] = $coupon_list_item['unit_price'];
                        break;
                    default :
                        $coupon_list_item['unit_price'] = 0;
                        $coupon_list_item['total_price'] = 0;
                        break;
                }
            }
            //$coupon_temp_prices[$coupon_id] = $coupon_list_item['total_price'];
            $coupon_list[$coupon_id] = $coupon_list_item;
        }
        /* end build coupon list */
        /* subset percent */
        foreach ($subset_percent_discount_list as $product_id => $subset_item) {
            $discounted_percent_sum = array_sum($subset_item);
            $discounted_amount = $discounted_percent_sum / 100 * $product_temp_prices[$product_id];
            if (abs($discounted_amount) > $product_temp_prices[$product_id]) {
                $discounted_amount = -$product_temp_prices[$product_id];
            }
            $product_temp_prices[$product_id] += $discounted_amount;
            foreach ($subset_item as $coupon_id => $coupon_unit_price) {
                $coupon_list[$coupon_id]['total_price'] = $coupon_unit_price / $discounted_percent_sum * $discounted_amount;
            }
        }
        /* end subset percent */
        /* calculate ratios */
        $temp_sum = array_sum($coupon_temp_prices) + array_sum($product_temp_prices);
        $product_price_ratios = [];
        $coupon_price_ratios = [];
        foreach ($product_temp_prices as $product_id => $temp_price) {
            if ($temp_sum == 0) {
                $product_price_ratios[$product_id] = 0;
            } else {
                $product_price_ratios[$product_id] = $temp_price / $temp_sum;
            }
        }
        foreach ($coupon_temp_prices as $coupon_id => $temp_price) {
            if ($temp_sum == 0) {
                $coupon_price_ratios[$coupon_id] = 0;
            } else {
                $coupon_price_ratios[$coupon_id] = $temp_price / $temp_sum; // 28.11
            }
        }
        /* end calculate ratios */
        /* apply all-yen coupons */
        foreach ($all_yen_coupon_list as $coupon_id) {
            $discounted_amount = $coupon_list[$coupon_id]['unit_price'];
            $temp_sum += $discounted_amount;
            foreach ($product_temp_prices as $product_id => $temp_price) {
                $product_discounted_amount = $product_price_ratios[$product_id] * $discounted_amount;
                $product_temp_prices[$product_id] += $product_discounted_amount;
            }
            // 28.11.2016
            foreach ($coupon_temp_prices as $coupon_id => $temp_price) {
                $product_discounted_amount = $coupon_price_ratios[$coupon_id] * $discounted_amount;
                $coupon_temp_prices[$coupon_id] += $product_discounted_amount;
            }
        }
        /* end apply all-yen coupons */
        /* apply all-% coupons */
        $discounted_amounts = []; // < 0
        foreach ($all_percent_coupon_list as $coupon_id) {
            $discounted_amounts[] = $coupon_list[$coupon_id]['unit_price'];
            $coupon_list[$coupon_id]['total_price'] = $coupon_list[$coupon_id]['unit_price'] * $temp_sum / 100;
        }
        $discounted_ratio = (1 + array_sum($discounted_amounts) / 100);
        $temp_sum *= $discounted_ratio;
        foreach ($product_temp_prices as $product_id => $temp_price) {
            $product_temp_prices[$product_id] *= $discounted_ratio;
        }
        foreach ($coupon_temp_prices as $coupon_id => $temp_price) {
            $coupon_temp_prices[$coupon_id] *= $discounted_ratio;
        }
        /* end apply all-yen coupons */
        /* recalculate product taxes */
        // set and drinkeat coupons taxes are calculated before because they dont have any discounts
        $taxes = [];
        foreach ($product_temp_prices as $product_id => $temp_price) {
            if (isset($product_list[$product_id])) {
                if ($product_list[$product_id]['tax_display_method'] === '01') {
                    $temp_external_tax_money[] = $temp_price * $product_list[$product_id]['tax_rate'] / 100;
                }
                $temp_tax_money[] = $temp_price * $product_list[$product_id]['tax_rate'] / 100;
                $taxes[] = $product_list[$product_id]['tax_rate'];
            } else {
                // fix missing option tax_display_method when using product_temp_prices for both options and products
                foreach ($option_list as $option_item) {
                    if ($option_item['product_id'] == $product_id) {
                        if ($option_item['tax_display_method'] === '01') {
                            $temp_external_tax_money[] = $temp_price * $option_item['tax_rate'] / 100;
                        }
                        $temp_tax_money[] = $temp_price * $option_item['tax_rate'] / 100;
                        $taxes[] = $option_item['tax_rate'];
                        break;
                    }
                }
            }
        }
        foreach ($coupon_temp_prices as $coupon_id => $temp_price) {
            if ($coupon_list[$coupon_id]['type'] === self::COUPON_TYPE_DRINK_EAT || $coupon_list[$coupon_id]['type'] === self::COUPON_TYPE_PRICE_SET) {
                $coupon_list[$coupon_id]['total_price'] = (1 + $coupon_list[$coupon_id]['tax_rate'] / 100) * $coupon_list[$coupon_id]['unit_price']; // tax for displaying
                $temp_external_tax_money[] = $temp_price * $coupon_list[$coupon_id]['tax_rate'] / 100; // tax based on order
            }
            $temp_tax_money[] = $temp_price * $coupon_list[$coupon_id]['tax_rate'] / 100;
            $taxes[] = $coupon_list[$coupon_id]['tax_rate'];
        }
        
        $total_external_tax = array_sum($temp_external_tax_money);
        $sub_total = array_sum($product_temp_prices) + array_sum($coupon_temp_prices);
        $total_price = $sub_total + $total_external_tax;

        $result = [];
        $result['total_price'] = round($total_price);
        $result['total_taxes'] = round(array_sum($temp_tax_money));
        $result['total_external_taxes'] = round($total_external_tax);
        $result['sub_total_price'] = round($sub_total);
        $result['coupons'] = $coupon_list;
        $result['products'] = $product_list;
        $result['options'] = $option_list;

        if ($result['total_price'] < 0) {
            $result['total_price'] = 0;
        }

        return $result;
    }

    /**
     * Query product price table
     * @param string|closure  $products
     * @param optional string $date
     * @return array|boolean
     *  - id   : product id
     *  - name : product name
     *  - assign_fee_flg 
     *  - unit_price
     *  - total_price
     */
    public static function getProductPriceTable($products, $date = '') {
        $filter_passed = is_callable($products);
        if (!$filter_passed && (empty($products) || !self::validateIds($products))) {
            return (new Query())->select('*')->from(Booking::tableName())->where('0=1');
        }

        if (empty($date)) {
            $date = date('Y-m-d');
        }

        $query = (new Query())->select([
                    'p.id',
                    'p.name',
                    'sum(p.unit_price) as unit_price',
                    'p.assign_fee_flg',
                    new Expression(
                            'case 
                                when p.tax_display_method = :external_flg and td.rate is not null then round(sum(p.unit_price * ( 1 + td.rate / 100 )))
                                else round(sum(p.unit_price))
                            end as total_price'
                    ),
                    'tax_rate' => 'coalesce(td.rate, 0)',
                    'p.tax_display_method'
                ])
                ->from(['p' => MstProduct::tableName()])
                ->leftJoin(['td' => TaxDetail::tableName()], 'td.tax_id = p.tax_rate_id '
                        . 'and td.del_flg = :zero '
                        . 'and :date between td.start_date '
                        . 'and td.end_date')
                ->where(['p.del_flg' => '0']);

        if (!$filter_passed) {
            $query->andWhere(['p.id' => explode(',', $products)]);
        } else {
            $products($query);
        }

        $query->groupBy(['p.id', 'td.rate']);

        $query->addParams([
            'zero' => '0',
            'external_flg' => '01',
            ':date' => $date
        ]);

        return $query;
    }

    /**
     * Query coupon price and/or benefits
     * 
     * @param          string|closure  $coupons a list of coupon id separated by commas
     * @param optional string          $date    date string in format Y-m-d
     * @return array|boolean
     *  - id   : coupon id
     *  - name : coupon title
     *  - tax_rate : tax rate (for type 5, 7)
     *  - type : use contants to detect
     *  - discount_yen
     *  - discount_percent
     *  - discount_price_set
     *  - discount_drink_eat
     */
    public static function getCouponPriceTable($coupons, $date = '') {
        $filter_passed = is_callable($coupons);
        if (!$filter_passed && (empty($coupons) || !self::validateIds($coupons))) {
            return (new Query())->select('*')->from(Booking::tableName())->where('0=1');
        }

        if (empty($date)) {
            $date = date('Y-m-d');
        }

        $search_query = (new Query())
                ->select([
                    "id" => "c.id",
                    "products" => "array_to_string(array_agg(p.id), ',')",
                    "tax_rate" => "coalesce(max(td.rate), 0)"
                ])
                ->from(["c" => MasterCoupon::tableName()])
                ->leftJoin(['pc' => ProductCoupon::tableName()], "pc.coupon_id = c.id "
                        . "and pc.del_flg = :zero")
                ->leftJoin(['p' => MstProduct::tableName()], "p.id = pc.product_id "
                        . "and p.store_id = c.store_id "
                        . "and p.del_flg = :zero")
                ->leftJoin(['td' => TaxDetail::tableName()], "td.tax_id = p.tax_rate_id "
                        . "and (:date between td.start_date and td.end_date) "
                        . "and td.del_flg = :zero")
                ->where("c.del_flg = :zero");

        if (!$filter_passed) {
            $search_query->andWhere("c.id in ($coupons)");
        } else {
            $coupons($search_query);
        }

        $search_query->groupBy(["c.id"]);

        $list_query = (new Query())
                ->select([
                    "c.id",
                    "name" => "c.title",
                    "search.products",
                    "search.tax_rate",
                    new Expression(
                            "case "
                            . "when c.benefits_content = '02' then c.discount_price_set_tax_type "
                            . "when c.benefits_content = '03' then c.discount_drink_eat_tax_type "
                            . "end as tax_display_method"
                    ),
                    //1 /* yen:all */
                    //2 /* %:all */
                    //3 /* yen:subset */
                    //4 /* %:subset */
                    //5 /* taxed */
                    //6 /* price_set */
                    //7 /* taxed */
                    //8 /* drink eat */
                    new Expression(
                            "case
                                when c.benefits_content = '00' and search.products = '' then 1
                                when c.benefits_content = '00' then 3
                                when c.benefits_content = '01' and search.products = '' then 2
                                when c.benefits_content = '01' then 4
                                when c.benefits_content = '02' and c.discount_price_set_tax_type = '01' then 5
                                when c.benefits_content = '02' then 6
                                when c.benefits_content = '03' and c.discount_drink_eat_tax_type = '01' then 7
                                when c.benefits_content = '03' then 8
                                else 0
                            end as type"
                    ),
                    "c.discount_yen",
                    "c.discount_percent",
                    "c.discount_price_set",
                    "c.discount_drink_eat"
                ])
                ->from(["search" => $search_query])
                ->innerJoin(['c' => MasterCoupon::tableName()], "c.id = search.id");

        $list_query->addParams([
            'date' => $date,
            'zero' => '0'
        ]);

        return $list_query;
    }

    /**
     * Validate id list separated by commas
     * @param string $idList
     * @return boolean
     */
    public static function validateIds($idList) {
        return \preg_match('/^\d+(,\d+){0,}$/', $idList);
    }

    /**
     * Validate option list separated by commas and slashes (%d/%d/%d)
     * @param string 
     * @return boolean
     */
    public static function validateOptionIds($options) {
        return preg_match('/^\d+\/\d+\/\d+(,\d+\/\d+\/\d+){0,}$/', $options);
    }

    /**
     * Format money with comma
     * @param mixed $money
     * @return string
     */
    public static function formatMoney($money) {
        return Yii::$app->formatter->asDecimal($money, 0);
    }

    /**
     * Money gained from point <direct conversion>
     * @param mixed $mixed_point number of points used
     * @param optional PointSetting $pointSetting
     * @return int
     */
    public static function getMoneyFromPoint($mixed_point, $pointSetting = false) {
        $point = (int) $mixed_point;

        if ($pointSetting === false) {
            $pointSetting = PointSetting::getPointSetting();
        }

        if ($pointSetting && $point) {
            return ($point * (int) ($pointSetting->point_conversion_yen)) / (int) ($pointSetting->point_conversion_p);
        }

        return 0;
    }

    /**
     * Point gained from money <2-step conversion>
     * @param type $mixed_money number of yen
     * @param CustomerStore $customerStore customer_store
     * @param optional PointSetting $pointSetting
     * @return int
     */
    public static function getPointFromMoney($mixed_money, CustomerStore $customerStore, $pointSetting = false) {
        $money = (int) $mixed_money;

        if ($pointSetting === false) {
            $pointSetting = PointSetting::getPointSetting($customerStore->store->company_id); // update 22.11.2016
        }
        //$company = CompanyStore::findOne(['store_id' => $customerStore->store_id]); // update 22.11.2016
        $rankSetting = null;
        $raw_point = 0;
        if ($pointSetting && $money) {
            $raw_point = ($money / (int) ($pointSetting->grant_yen_per_point)); //* (int) ($pointSetting->point_conversion_p)) / (int) ($pointSetting->point_conversion_yen);
            $rankSetting = MasterRank::findOne(['company_id' => $pointSetting->company_id]);
        }

        if ($raw_point > 0 && $rankSetting) {
            // default is normal rank
            $normal_point = round($raw_point * (int) $rankSetting->normal_point_rate);
            switch ($customerStore->rank_id) {
                /* bronze rank */
                case '01': return $rankSetting->bronze_status = '1' ? round($raw_point * (int) $rankSetting->bronze_point_rate) : $normal_point;
                /* silver rank */
                case '02': return $rankSetting->silver_status = '1' ? round($raw_point * (int) $rankSetting->sliver_point_rate) : $normal_point;
                /* gold rank */
                case '03': return $rankSetting->gold_status = '1' ? round($raw_point * (int) $rankSetting->gold_point_rate) : $normal_point;
                /* normal rank */
                default : return $normal_point;
            }
        }

        // no rank setting found
        return round($raw_point);
    }

    /**
     * Format point with comma
     * @param mixed $point
     * @return string
     */
    public static function formatPoint($point) {
        return Yii::$app->formatter->asDecimal($point, 0);
    }

    /**
     * Automatically display money from price_item ( received from static::getPriceTable )
     * @param type $price_item
     */
    public static function displayMoney($price_item) {
        if ($price_item['mode'] != 'coupon' || $price_item['type'] > 4) {
            return '¥' . self::formatMoney($price_item['total_price']);
        } elseif ($price_item['type'] == 1 || $price_item['type'] == 3) {
            return '¥' . self::formatMoney(abs($price_item['unit_price'])) . Yii::t('frontend', 'Off');
        } else {
            return abs($price_item['unit_price']) . '%' . Yii::t('frontend', 'Off');
        }
    }

    /**
     * Check booking suitable for store schedule / settings
     * @param integer $store_id
     * @param string $booking_date Y-m-d
     * @param string $start_time H:i
     * @param optional integer $execute_time minute(s)
     * @return boolean
     */
    public static function checkStoreSchedule($store_id, $booking_date, $start_time, $execute_time = 0) {
        try {
            // Find store
            $store = MasterStore::findFrontEnd()->where(['id' => $store_id])->one();

            if (!$store) {
                return false;
            }

            $booking_start_timestamp = Carbon::createFromFormat('Y-m-d H:i', $booking_date . ' ' . $start_time)->timestamp;
            $booking_end_timestamp = Carbon::createFromFormat('Y-m-d H:i', $booking_date . ' ' . $start_time)->addMinutes($execute_time)->timestamp;

            $store_schedule = StoreSchedule::find()->andWhere(['store_id' => $store_id, 'schedule_date' => $booking_date])->one();
            $invalid_store_schedule = false;

            $store_open_timestamp = Carbon::createFromFormat('Y-m-d H:i', $booking_date . ' ' . $store->time_open)->timestamp;
            $store_close_timestamp = Carbon::createFromFormat('Y-m-d H:i', $booking_date . ' ' . $store->time_close)->timestamp;
            // Check store schedule : higher priority
            if ($store_schedule) {
                $schedule_start_timestamp = Carbon::createFromFormat('Y-m-d H:i', $booking_date . ' ' . $store_schedule->start_time)->timestamp;
                $schedule_end_timestamp = Carbon::createFromFormat('Y-m-d H:i', $booking_date . ' ' . $store_schedule->end_time)->timestamp;
                if ($store_schedule['work_flg'] === '0' || $schedule_start_timestamp > $booking_start_timestamp || $schedule_end_timestamp < $booking_end_timestamp) {
                    $invalid_store_schedule = true;
                }
            }
            // Check store setting : lower priority
            else if ($store_open_timestamp > $booking_start_timestamp || $store_close_timestamp < $booking_end_timestamp) {
                $invalid_store_schedule = true;
            }

            return !$invalid_store_schedule;
        } catch (\Exception $e) {
            return false;
        }
    }
    
    public function getTotalMoneyOrder($productList = null, $adjustmentMoney = 0, $totalOutterTax = 0, $moneyFromPoint = 0, $chargeUse = 0, $chargeAddMoney = 0){
        $totalMoney = 0;
        
        $products = array();
        $coupons = array();
        $tickets = array();
        $points= array();
        $charge = array();
        $reduce = array();
        
        if(!empty($productList)){
            
            foreach($productList as $item){
                $jancode = substr($item['jancode'], 0,2);
                switch ($jancode){
                    case '21': array_push($products, $item); break;
                    case '22': array_push($coupons, $item); break;
                    case '23': array_push($tickets, $item); break;
                    case 'rd': array_push($reduce, $item); break;
                    case 'pt': array_push($points, $item); break;
                    case 'ch': array_push($charge, $item); break;
                }
            }
            
            foreach($products as $product){
                $totalMoney += $product['last_price'];
            }
            
            foreach($coupons as $coupon){
                if($coupon['benefits_content'] == '02' || $coupon['benefits_content'] == '03'){
                    $totalMoney += $coupon['last_price'];
                }
            }
            
            foreach($tickets as $ticket){
                if($ticket['status_ticket'] == 'buy'){
                    $totalMoney += $ticket['last_price'];
                }
            }
            $totalMoney = round($totalMoney + $totalOutterTax - $adjustmentMoney - $moneyFromPoint - $chargeUse + $chargeAddMoney);
        }
        
        return $totalMoney;
    }

}
