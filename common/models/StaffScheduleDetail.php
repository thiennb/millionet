<?php

namespace common\models;

use Yii;
use yii\db\Query;

/**
 * This is the model class for table "staff_schedule_detail".
 *
 * @property integer $id
 * @property integer $staff_id
 * @property string $store_id
 * @property string $booking_date
 * @property string $booking_time
 * @property string $booking_status
 * @property string $booking_code
 */
class StaffScheduleDetail extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    const BOOKING_STATUS_WORKING = "1";
    const BOOKING_STATUS_BUSY = "2";
    const BOOKING_STATUS_OFF = "3";
    public static function tableName()
    {
        return 'staff_schedule_detail';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['staff_id'], 'integer'],
            [['booking_date','booking_code','store_id'], 'safe'],
            [['booking_time'], 'string', 'max' => 8],
            [['booking_status'], 'string', 'max' => 1],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'staff_id' => 'Staff ID',
            'booking_date' => 'Booking Date',
            'booking_time' => 'Booking Time',
            'booking_status' => 'Booking Status',
        ];
    }

    public static function find()
    {
        return parent::find()->where(['booking_status' => 1]);
    }

//    public static function getStaffScheduleDetailBetweenDates($staff_id,$start_date,$end_date) {
//
//        $query = StaffScheduleDetail::find();
//        $query->where(["between", "booking_date", "'" .$start_date. "'","'" .$end_date. "'"]);
//        $query->andWhere('staff_id = :staff_id', [':staff_id' => $staff_id]);
//        $query->orderBy('booking_date','booking_time');
//
//        return $query->all();
//    }

    public static function getStaffScheduleDetailBetweenDates($staff_id,$number_row,$time_execute,$start_date,$end_date) {

        $connection = \Yii::$app->db;

        return $connection->createCommand("
                select
                            *
                    from (
                        select
                            ssd1.id,
                            ssd1.booking_date,
                            ssd1.booking_status,
                            case
                                when
                                    (
                                        select
                                                count(*)
                                        from
                                                staff_schedule_detail as ssd
                                        where
                                                ssd.booking_status = '1' and ssd.staff_id = $staff_id
                                        and
                                                ssd.booking_time between ssd1.booking_time and ssd1.booking_time + '".$time_execute."' - '00:30:00'
                                        and
                                                ssd1.booking_date = ssd.booking_date
                                    ) = ".$number_row."
                                then
                                        ssd1.booking_time
                                else
                                        null
                            end as booking_time
                        from
                                staff_schedule_detail as ssd1
                        where
                                booking_status = '1' and staff_id = $staff_id
                        and
                                booking_date between '".$start_date."' and '".$end_date."'
                        order by
                                booking_date,booking_time
                        ) as staff_schedule_detail
                    where
                            booking_time is not null
        ")->queryAll();
    }

    public static function checkBookingIsValid($staff_id,$time_booking,$time_execute,$start_date ,$booking_code = null) {

        $connection = \Yii::$app->db;

        sscanf($time_execute, "%d:%d:%d", $hours, $minutes, $seconds);
        $time_seconds = $hours * 3600 + $minutes * 60 + $seconds;
        $number_row = (int)ceil($time_seconds/1800);
         $row = $connection->createCommand("
                select
                            *
                    from (
                        select
                            ssd1.id,
                            ssd1.booking_date,
                            ssd1.booking_status,
                            case
                                when
                                    (
                                        select
                                                count(*)
                                        from
                                                staff_schedule_detail as ssd
                                        where
                                        ( ssd.booking_status = '1' or ( ssd.booking_status = '2' and ssd.booking_code = :booking_code ) ) 
                                        and ssd.staff_id = :staff_id
                                               
                                        and
                                                ssd.booking_time between ssd1.booking_time and ssd1.booking_time + :time_execute - '00:30:00'
                                        and
                                                ssd1.booking_date = ssd.booking_date
                                    ) = :number_row
                                then
                                        ssd1.booking_time
                                else
                                        null
                            end as booking_time
                        from
                                staff_schedule_detail as ssd1
                        where
                        ( booking_status = '1' or ( booking_status = '2' and booking_code = :booking_code ) ) and staff_id = :staff_id
                        and
                                booking_date = :start_date
                        and     booking_time = :time_booking
                        and
                                to_timestamp(booking_date||' '||booking_time,'YYYY/MM/DD HH24:MI:SS' ) > :now
                        order by
                                booking_date,booking_time
                        ) as staff_schedule_detail
                    where
                            booking_time is not null

        ",[
            ':time_execute' => $time_execute,
            ':staff_id'     => $staff_id,
            ':number_row'   => $number_row,
            ':start_date'   => $start_date ,
            ':time_booking' => $time_booking,
            ':booking_code' => $booking_code,
            ':now'          => date('Y-m-d H:i:s')
        ])->queryAll();

        return  count($row) > 0 ? "true" : "false";
    }   
}
