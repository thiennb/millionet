<?php
namespace common\models;

use Yii;

/**
 * Login form with company code
 */
class CompanyLoginForm extends LoginForm {
	public $company_code;

	/**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // username and password are both required
            [['username', 'password', 'company_code'], 'required'],
            // rememberMe must be a boolean value
            ['rememberMe', 'boolean'],
			// company_code existence is checked
			['company_code', 'validateCompanyCode'],
            // password is validated by validatePassword()
            ['password', 'validatePassword']
        ];
    }

	/**
	 * @inheritdoc
	 */
	public function attributeLabels () {
		return [
			'company_code' => Yii::t('backend', 'Company Code'),
			'username' => Yii::t('backend', 'Admin Login ID'),
			'password' => Yii::t('backend', 'Password'),
			'rememberMe' => Yii::t('backend', 'Save Login Information')
		];
	}

	/**
	 * @inheritdoc
	 */
	public function login()
    {
		// validate company code, user name and password
        if (/* $this->checkCompanyCode() && */ $this->validate()) {
            return Yii::$app->user->login($this->getUser(), $this->rememberMe ? 3600 * 24 * 30 : 0);
        } else {
            return false;
        }
    }

	/**
     * Validates the company_code
     * This method serves as the inline validation for company code.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
	public function validateCompanyCode ($attribute, $params) {
		if ($this->hasErrors()){
			return false;
		}

		if (!Company::find()->where(['company_code' => $this->company_code])->exists()) {
			$this->addError($attribute, Yii::t('backend', 'Company code doesn\'t exist'));
			return false;
		}
		else {
			return true;
		}
	}
}
