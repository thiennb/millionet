<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use yii2tech\ar\softdelete\SoftDeleteBehavior;
use common\models\MasterStaff;
use common\models\ProductCoupon;
use yii\helpers\ArrayHelper;
use common\models\MstProduct;
use common\components\Util;
use common\models\PayCoupon;
use common\models\BookingCoupon;
use common\components\Constants;

/**
 * This is the model class for table "mst_coupon".
 *
 * @property integer $id
 * @property string $name
 * @property integer $exp_date
 * @property string $memo
 * @property string $title
 * @property string $content
 * @property integer $start_date
 * @property integer $end_date
 * @property string $type_id
 * @property string $display_start_date
 * @property integer $show_coupon
 * @property string $code_membership
 * @property string $name_kana
 * @property integer $sex
 * @property string $birthday_from
 * @property string $birthday_to
 * @property string $last_visit_date_from
 * @property string $last_visit_date_to
 * @property integer $visit_number_min
 * @property integer $visit_number_max
 * @property integer $rank_id
 * @property integer $staff_id
 * @property integer $black_list_flg
 * @property integer $benefits_content
 * @property double $discount_yen
 * @property double $discount_percent
 * @property double $price_set
 * @property double $drink_eat
 * @property string $image
 * @property integer $grant_point
 * @property integer $title_font_size
 * @property integer $title_postion
 * @property string $introduction_1
 * @property integer $introduction_font_size_1
 * @property integer $introduction_postion_1
 * @property string $introduction_2
 * @property integer $introduction_font_size_2
 * @property integer $introduction_postion_2
 * @property string $introduction_3
 * @property integer $introduction_font_size_3
 * @property integer $introduction_postion_3
 * @property integer $combine_with_other_coupon_flg
 * @property string $del_flg
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $display_barcode_flg
 * @property string $coupon_deatails_description
 */
class MasterCoupon extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public $tmp_image;
    // Hiden Image For Delete
    public $hidden_image1;
    private $_attributes = [];
    //    public $firstName;
    //    public $lastName;
    //    public $firstNameKana;
    //    public $lastNameKana;
    // ====================================
    // Datpdt 19/09/2016 Search start
    public $coupon_date_from;
    public $coupon_date_to;
    public $booking_site_member_app;
    public $print_receipt;
    public $benefit_content;
    public $category;
    public $name_customer;
    public $name_product;
    public $introduction;
    public $display_start_date;
    //list product selected
    public $option;
    public $option_hidden;
    public $title_hidden;
    public $list_product;
    public $error_product_tax;
    public $jan_code_before_create;
    public $total_price;
    public $id_store_comon;
    public $_role;

    const BENEFITS_CONTENT_DISCOUNT_PRICE = '00';
    const BENEFITS_CONTENT_DISCOUNT_PERCENT = '01';
    const BENEFITS_CONTENT_DISCOUNT_SET_PRICE = '02';
    const BENEFITS_CONTENT_DISCOUNT_FREE = '03';

    // Datpdt 19/09/2016 Search end
    // ==================================== 
    // Datpdt 20/09/2016 Create start
    // Datpdt 20/09/2016 Create end
    public static function tableName() {
        return 'coupon';
    }

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            TimestampBehavior::className(),
            BlameableBehavior::className(),
            'softDeleteBehavior' => [
                'class' => SoftDeleteBehavior::className(),
                'softDeleteAttributeValues' => [
                    'del_flg' => '1'
                ],
            ],
        ];
    }

    public function rules() {
        return [
                [['visit_number_min', 'visit_number_max', 'sex', 'visit_number_max', 'title_font_size', 'title_postion', 'introduction_font_size_1', 'introduction_postion_1', 'introduction_font_size_2', 'introduction_postion_2', 'introduction_font_size_3', 'introduction_postion_3', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
                [['coupon_details_description'], 'string'],
                [['start_date', 'birthday_from', 'birthday_to', 'last_visit_date_from', 'last_visit_date_to',
            // Datpdt 19/09/2016 Search start    
            'coupon_date_from', 'coupon_date_to', 'booking_site_member_app', 'export_receipt', 'store_id', 'benefit_content', 'category',
            'name_product', 'introduction', 'start_date', 'name_customer',
            // Datpdt 20/09/2016 Create start                    
            'expire_date', 'print_receipt', 'booking_site_member_app', 'name_customer', 'option_hidden', 'option',
            'memo', 'content', 'name', 'name_kana', 'type_id', 'title', 'coupon_jan_code', 'show_coupon', 'rank_id', 'last_staff_id', 'discount_percent', 'discount_price_set', 'discount_price_set_tax_type', 'discount_drink_eat', 'discount_drink_eat_tax_type', 'combine_with_other_coupon_flg', 'display_barcode_flg', 'benefits_content', 'discount_yen', 'visit_number_min'
            , 'list_product', 'error_product_tax', 'display_condition', 'expire_auto_set', 'black_list_flg', 'jan_code_before_create', '_role', 'id_store_comon'], 'safe'],
            // Datpdt 20/09/2016 Create end             
            // Datpdt 19/09/2016 Search end
            // [['price_set', 'drink_eat'], 'number'],
            [['start_date'], 'required'],
            //[['name_kana', 'image'], 'string', 'max' => 100],
            [['title', 'introduction_1', 'introduction_2', 'introduction_3'], 'string', 'max' => 100],
            //[['type_id'], 'string', 'max' => 3],
            [['del_flg'], 'string', 'max' => 1],
                [['start_date', 'expire_date', 'birthday_from', 'birthday_to', 'last_visit_date_from', 'last_visit_date_to'], 'string', 'max' => 10],
            //[['code_membership','discount_yen','discount_price_set','discount_drink_eat'], 'integer', 'max' => 13],
            [['first_name', 'last_name', 'first_name_kana', 'last_name_kana'], 'string', 'max' => 40],
                [['visit_number_min', 'visit_number_max'], 'string', 'max' => 6],
            //[['discount_percent'], 'integer', 'max' => 3],
            //[['grant_point'], 'integer', 'max' => 11],
            [['title'], 'string', 'max' => 40],
                [['introduction_1', 'introduction_2', 'introduction_3'], 'string', 'max' => 100],
                [['coupon_details_description'], 'string', 'max' => 500],
            // Check Name
            //[['discount_yen'], 'required'],
            // [['first_name', 'last_name', 'first_name_kana', 'last_name_kana'], 'required', 'except' => 'giftinsert'],
            [['first_name', 'last_name'], 'match', 'not' => true, 'pattern' => '/[\'\/~`\!@#\$%\^&\*\(\)_\-\+=\{\}\[\]\|;:"\<\>,\.\?\\\]/', 'message' => Yii::t('backend', '{attribute} invalid characters')],
                [['first_name_kana', 'last_name_kana'], 'match', 'pattern' => '/^[ァ-ヴー]+$/u', 'message' => Yii::t('backend', '{attribute} invalid characters full size only')],
                [['expire_date', 'start_date', 'coupon_date_to', 'coupon_date_from', 'birthday_from', 'birthday_to', 'last_visit_date_from', 'last_visit_date_to'], 'date', 'format' => 'php:Y/m/d'],
                [['benefits_content'], 'checkDiscount'],
            // Check start date
            // Validate
            [['expire_date'], 'checkStartDate'],
                [['start_date'], 'checkLastDate'],
                [['birthday_from'], 'checkStartBirthDayFrom'],
                [['birthday_to'], 'checkStartBirthDay'],
                [['last_visit_date_to'], 'checkStartLastVisit'],
                [['visit_number_max'], 'checkStartVisitTime'],
                [['grant_point'], 'checkGrantPoint'],
                [['discount_percent'], 'checkDiscountPercent'],
            //[['error_product_tax'],'validateProduct'],
            // Check size image
            [['tmp_image', 'image'], 'file', 'extensions' => 'png, jpg, jpeg, gif', 'skipOnEmpty' => true, 'maxSize' => 1024 * 1024 * 2],
                ['code_membership', 'match', 'not' => true, 'pattern' => '/[^a-zA-Z0-9]/', 'message' => Yii::t('backend', '{attribute} invalid characters membership')],
                [['store_id'], 'required', 'when' => function() {
                    $role = \common\components\FindPermission::getRole();
                    return ($role->permission_id == Constants::STORE_MANAGER_ROLE || $role->permission_id == Constants::STAFF_ROLE);
                }],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => Yii::t('backend', 'ID'),
            'coupon_jan_code' => Yii::t('backend', 'Coupon Jan Code Tb'),
            'name' => Yii::t('backend', 'Name Coupon'),
            'exp_date' => Yii::t('backend', 'Exp Date'),
            'memo' => Yii::t('backend', 'Memo'),
            'title' => Yii::t('backend', 'Title Coupon'),
            'content' => Yii::t('backend', 'Content'),
            'start_date' => Yii::t('backend', 'Start Date'),
            'end_date' => Yii::t('backend', 'End Date'),
            'type_id' => Yii::t('backend', 'Type ID'),
            'start_date' => Yii::t('backend', 'Display Start Date'),
            'show_coupon' => Yii::t('backend', 'Show Coupon'),
            'code_membership' => Yii::t('backend', 'Code Membership'),
            'name_kana' => Yii::t('backend', 'Name Kana'),
            'sex' => Yii::t('backend', 'Sex Coupon'),
            'birthday_from' => Yii::t('backend', 'Birthday From'),
            'birthday_to' => Yii::t('backend', 'Birthday To'),
            'last_visit_date_from' => Yii::t('backend', 'Last Visit Date From'),
            'last_visit_date_to' => Yii::t('backend', 'Last Visit Date To'),
            'visit_number_min' => Yii::t('backend', 'Visit Number Min'),
            'visit_number_max' => Yii::t('backend', 'Visit Number Max'),
            'rank_id' => Yii::t('backend', 'Rank Customer'),
            'staff_id' => Yii::t('backend', 'Staff ID'),
            'black_list_flg' => Yii::t('backend', 'Black List Cuopon'),
            'benefits_content' => Yii::t('backend', 'Benefits Content Coupon Preview'),
            'discount_yen' => Yii::t('backend', 'Discount Circle'),
            'discount_percent' => Yii::t('backend', 'Discount Pull'),
            'price_set' => Yii::t('backend', 'Price Set'),
            'drink_eat' => Yii::t('backend', 'Drink Eat'),
            'image' => Yii::t('backend', 'Image Cuopon'),
            'grant_point' => Yii::t('backend', 'Grant Point Cuopon'),
            'title_font_size' => Yii::t('backend', 'Title Font Size'),
            'title_postion' => Yii::t('backend', 'Title Postion'),
            'introduction_1' => Yii::t('backend', 'Introduction 1'),
            'introduction_font_size_1' => Yii::t('backend', 'Introduction Font Size 1'),
            'introduction_postion_1' => Yii::t('backend', 'Introduction Postion 1'),
            'introduction_2' => Yii::t('backend', 'Introduction 2'),
            'introduction_font_size_2' => Yii::t('backend', 'Introduction Font Size 2'),
            'introduction_postion_2' => Yii::t('backend', 'Introduction Postion 2'),
            'introduction_3' => Yii::t('backend', 'Introduction 3'),
            'introduction_font_size_3' => Yii::t('backend', 'Introduction Font Size 3'),
            'introduction_postion_3' => Yii::t('backend', 'Introduction Postion 3'),
            'combine_with_other_coupon_flg' => Yii::t('backend', 'Other Coupon User'),
            'display_barcode_flg' => Yii::t('backend', 'Display Code'),
            'coupon_details_description' => Yii::t('backend', 'Coupon Deatails Description'),
            // Datpdt 19/09/2016 Search start
            'booking_site_member_app' => Yii::t('backend', 'Booking Site Member App'),
            'print_receipt' => Yii::t('backend', 'Print Receipt'),
            'store_id' => Yii::t('backend', 'Distributing Store'),
            'benefit_content' => Yii::t('backend', 'Benefit'),
            'display_start_date' => Yii::t('backend', 'Display Start Date'),
            // Datpdt 19/09/2016 Search end
            // Datpdt 19/09/2016 Create start
            'expire_date' => Yii::t('backend', 'Valid Period'),
            // Datpdt 19/09/2016 Create end            
            'del_flg' => Yii::t('backend', 'Del Flg'),
            'created_at' => Yii::t('backend', 'Created At'),
            'updated_at' => Yii::t('backend', 'Updated At'),
            'created_by' => Yii::t('backend', 'Created By'),
            'updated_by' => Yii::t('backend', 'Updated By'),
            'first_name' => Yii::t('backend', "First name"),
            'last_name' => Yii::t('backend', "Last name"),
            'first_name_kana' => Yii::t('backend', "First name kana"),
            'last_name_kana' => Yii::t('backend', "Last name kana"),
            'coupon_date_from' => Yii::t('backend', "Start Day From"),
            'coupon_date_to' => Yii::t('backend', "Start Day To"),
            'expire_auto_set' => Yii::t('backend', "Expire Auto Set"),
        ];
    }

    public static function find() {

        return parent::find()->andFilterWhere((new \common\components\FindPermission)->getPermissionCondition(self::tableName(), true));
    }

    public static function findFrontEnd() {

        return parent::find(['or', [self::tableName() . '.del_flg' => '0'], [self::tableName() . '.del_flg' => null]]);
    }

    public function beforeSave($insert) {
        if (parent::beforeSave($insert)) {
            if ($this->isNewRecord)
                $this->coupon_jan_code = Util::genJanCodeAuto(\common\components\Constants::JAN_TYPE_COUPON);
            return true;
        } else {
            return false;
        }
    }

    public function attributes() {
        return array_merge(parent::attributes(), array_keys($this->_attributes));
    }

    public function __get($name) {
        if (array_key_exists($name, $this->_attributes))
            return $this->_attributes[$name];

        try {
            return parent::__get($name);
        } catch (\Exception $e) {
            return false;
        }
    }

    public function __set($name, $value) {
        try {
            parent::__set($name, $value);
        } catch (\Exception $e) {
            $this->_attributes[$name] = $value;
        }
    }

    public function getListTypeStaff() {
        return ArrayHelper::map(MasterStaff::find()->all(), 'id', 'name');
    }

    public static function getListCoupon($storeId = null) {
        if ($storeId != null)
            return ArrayHelper::map(MasterCoupon::find()->andWhere(['store_id' => $storeId])->all(), 'id', 'title');
        return ArrayHelper::map(MasterCoupon::find()->all(), 'id', 'title');
    }

    /**
     * @get arr option from coupon id
     */
    public static function getArrOption($coupons) {

        $productOption = [];
        if (count($coupons) == 0)
            return [];
        foreach ($coupons as $couponId) {
            $products = self::findOne($couponId)->productsCoupon;
            //model ProductCoupon, get productsCoupon from coupon
            foreach ($products as $product) {


                //model ProductCoupon, get option from product
                if (count($product->option) >= 0) {
                    foreach ($product->option as $option) {
                        //model MstProductOption

                        if (!in_array($option->option_id, $productOption)) {
                            $productOption[] = $option->option_id;
                        }
                    }
                }
            }
        }
        return $productOption;
    }

    public static function getArrProduct($coupons) {

        $productsId = [];
        if (count($coupons) == 0)
            return [];
        foreach ($coupons as $couponId) {
            $products = self::findOne($couponId)->productsCoupon;
            //model ProductCoupon, get productsCoupon from coupon
            foreach ($products as $product) {
                if (!in_array($product->product_id, $productsId)) {
                    $productsId[] = $product->product_id;
                }

                //model ProductCoupon, get option from product
            }
        }
        return $productsId;
    }

    public function getProductsCoupon() {
        return $this->hasMany(ProductCoupon::className(), ['coupon_id' => 'id']);
    }

    //save into table product_coupon
    public function saveProductCoupon() {
        ProductCoupon::updateAll(['del_flg' => '1', 'updated_at' => strtotime("now")], ['coupon_id' => $this->id]);
        $product_arr = empty($this->option_hidden) ? "" : explode(",", $this->option_hidden);
        if (!empty($product_arr)) {
            foreach ($product_arr as $val) {
                $product_coupon = new ProductCoupon();
                $product_coupon->coupon_id = $this->id;
                $product_coupon->product_id = $val;
                $product_coupon->save();
            }
        }
    }

    // ==================================
    // Check Product tax
    // ==================================
    public function checkProductTaxDisplayMethod($list_product) {

        $command = Yii::$app->getDb()->createCommand(
                "SELECT 
                    COUNT(*) 
                FROM    
                (
                    SELECT
                        mst_tax.id
                    FROM
                      mst_product
                    INNER JOIN mst_tax
                    ON
                     mst_product.tax_rate_id = mst_tax.id
                    INNER JOIN tax_detail
                    ON
                    tax_detail.tax_id = mst_tax.id
                    WHERE
                      mst_product.id IN (" . $list_product . ")
                    GROUP BY
                      mst_tax.id
                )    
                as temp;"
        );
        if (preg_match('/^\d+(,\d+){0,}$/', $list_product)) {
            $result = $command->queryAll();
            return (int) $result[0]['count'];
        } else {
            return 0;
        }
    }

    public function checkProductDisplayMethod($list_product) {

        $command = Yii::$app->getDb()->createCommand(
                "SELECT 
                    COUNT(*)
                FROM    
                (
			SELECT
			DISTINCT
			  CASE   
			      WHEN tax_display_method = '02' THEN 2
			      ELSE 1  
			  END AS  tax_display_method
			FROM
			  mst_product
			WHERE
			  mst_product.id IN (" . $list_product . ")
		) as temp;"
        );

        if (preg_match('/^\d+(,\d+){0,}$/', $list_product)) {
            $result = $command->queryAll();
            return (int) $result[0]['count'];
        } else {
            return 0;
        }
    }

    public function checkProductDisplayMethod02($list_product) {

        $command = Yii::$app->getDb()->createCommand(
                "SELECT 
                    COUNT(*) 
                FROM    
                (
                    SELECT
                        mst_product.tax_display_method
                    FROM
                      mst_product
                    WHERE
                      mst_product.id IN (" . $list_product . ")
                    AND mst_product.tax_display_method = '02'
                )    
                as temp;"
        );
        if (preg_match('/^\d+(,\d+){0,}$/', $list_product)) {
            $result = $command->queryAll();
            return (int) $result[0]['count'];
        } else {
            return 0;
        }
    }

    // **************************************** //
    // Check Exit Id Of Table Coupon Other Table //
    // **************************************** //
    public function checkDelete($id, $coupon_jan_code) {

        $check = 0;
        //$check = ProductCoupon::find()->where(['coupon_id' => $id])->count();
        //$check = PayCoupon::find()->where(['coupon_id' => $id])->count();
        $check = BookingCoupon::find()->where(['coupon_id' => $id])->count();
        // Check exit table order_detail
        $check += MstOrderDetail::find()->where(['jan_code' => $coupon_jan_code])->count();

        return ($check > 0) ? FALSE : TRUE;
    }

    public function checkDiscount($attribute, $params) {

        if ($this->benefits_content == '00') {
            if (empty($this->discount_yen)) {
                $this->addError('discount_yen', Yii::t('backend', "Select But Not Input Discount"));
            }
            if (0 > $this->discount_yen) {
                $this->addError('discount_yen', Yii::t('backend', "Discount of the format is not correct."));
            }
        }
        if ($this->benefits_content == '01') {
            if (empty($this->discount_percent)) {
                $this->addError('discount_percent', Yii::t('backend', "Select But Not Input Discount Percent"));
            }
            if ($this->discount_percent < 0) {
                $this->addError('discount_percent', Yii::t('backend', "Discount Percent of the format is not correct."));
            }
            if ($this->discount_percent > 100) {
                $this->addError('discount_percent', Yii::t('backend', "Please enter only from 0 to 100."));
            }
        }
        if ($this->benefits_content == '02') {
            if (empty($this->discount_price_set)) {
                $this->addError('discount_price_set', Yii::t('backend', "Select But Not Input Discount Price Set"));
            }
            if (!empty($this->discount_price_set) && 0 > $this->discount_price_set) {
                $this->addError('discount_price_set', Yii::t('backend', "Set the price set of the format is not correct."));
            }

            $this->checkCountProduct();
        }
        if ($this->benefits_content == '03') {
            if (empty($this->discount_drink_eat)) {
                $this->addError('discount_drink_eat', Yii::t('backend', "Select But Not Input Discount Drink Eat"));
            }

            if (!empty($this->discount_drink_eat) && 0 > $this->discount_drink_eat) {
                $this->addError('discount_drink_eat', Yii::t('backend', "Eat all you can drink of the format is not correct."));
            }
            $this->checkCountProduct();
        }
        // ================================
        // Check Select Product Start
        // ================================
        if (strlen($this->option_hidden) == 0 && $this->benefits_content != '00' && $this->benefits_content != '01') {
            $this->addError('error_product_tax', Yii::t('backend', "Please select a commodity."));
            //$this->addError('error_product_tax', Yii::t('backend', "Set price,in the case of all you can drink,since it can not tax rate is to set the different products, please try again."));
        }
        // ================================
        // Check Select Product End
        // ================================
        // ================================
        // Check Tax Start
        // ================================
        if ($this->discount_drink_eat_tax_type == '01' || $this->discount_price_set_tax_type == '01' || $this->discount_drink_eat_tax_type == '00' || $this->discount_price_set_tax_type == '00') {

            $product_arr = empty($this->option_hidden) ? "" : explode(",", $this->option_hidden);

            $count_product_chosse = count($product_arr);

            if ($count_product_chosse >= 2) {

                $list_product_chosse = null;
                if (!empty($product_arr)) {
                    $check_product_arr = 0;
                    foreach ($product_arr as $key => $value) {
                        $check_product_arr ++;
                        if ($check_product_arr == 1) {
                            $list_product_chosse .= $value;
                        } else if ($value != null && $check_product_arr != 1) {
                            $list_product_chosse .= ("," . $value);
                        }
                    }
                }

                $result_check = $this->checkProductTaxDisplayMethod($list_product_chosse);
                $result_check_product = $this->checkProductDisplayMethod($list_product_chosse);

                if ($result_check != 1 || $result_check_product != 1) {
                    $this->addError('error_product_tax', Yii::t('backend', "Set price,in the case of all you can drink,since it can not tax rate is to set the different products, please try again."));
                }
            }
        }

        if ($this->discount_drink_eat_tax_type == '02' || $this->discount_price_set_tax_type == '02') {

            $product_arr = empty($this->option_hidden) ? "" : explode(",", $this->option_hidden);

            $count_product_chosse = count(array_filter($product_arr));

            if ($count_product_chosse >= 2) {

                $list_product_chosse = null;
                if (!empty($product_arr)) {
                    $check_product_arr = 0;
                    foreach ($product_arr as $key => $value) {
                        $check_product_arr ++;
                        if ($check_product_arr == 1) {
                            $list_product_chosse .= $value;
                        } else if ($value != null && $check_product_arr != 1) {
                            $list_product_chosse .= ("," . $value);
                        }
                    }
                }

                $result_check = $this->checkProductDisplayMethod02($list_product_chosse);

                if ($result_check != $count_product_chosse) {
                    $this->addError('error_product_tax', Yii::t('backend', "Set price,in the case of all you can drink,since it can not tax rate is to set the different products, please try again."));
                }
            }
        }

        //==============================
//        $expire_date = $this->expire_date;
//        if ($this->expire_auto_set == 0) {
//            if (empty($expire_date) || $expire_date == null) {
//                $this->addError('expire_date', Yii::t('backend', "expire_date cannot be blank."));
//            }
//        }
//        
//        if (empty($this->start_date) || $this->start_date == null) {
//                $this->addError('start_date', Yii::t('backend', 'start_date cannot be blank.'));
//        }
    }

    public function checkCountProduct() {
        $product_arr = empty($this->option_hidden) ? "" : explode(",", $this->option_hidden);

        $count_product_chosse = count($product_arr);
        if ($count_product_chosse < 2) {
            $this->addError('error_product_tax', Yii::t('backend', "Please select two or more goods."));
        }
    }

    // **************************************** //
    // Check Display Start Date And Valid Period
    // **************************************** //
    public function checkStartDate($attribute, $params) {
        $start_date = $this->start_date;
        $expire_date = $this->expire_date;
        if (!empty($expire_date)) {

            if ($start_date > $expire_date) {
                $key = $attribute;
            }
        }
    }

    public function checkLastDate($attribute, $params) {
        $start_date = $this->start_date;
        if (!empty($start_date)) {
            if ($this->expire_auto_set == 1) {
                $last_date = date("Y/m/t", strtotime("0 month"));
                if ($last_date < $start_date) {
                    $this->addError('start_date', Yii::t('backend', "Expiration date, please specify the date of the later display start"));
                }
            }
        }
    }

    // **************************************** //
    // Check Display BirthdayFROM And BirthdayTo
    // **************************************** //
    public function checkStartBirthDay($attribute, $params) {
        $birthday_from = $this->birthday_from;
        $birthday_to = $this->birthday_to;
        if (!empty($birthday_to)) {
            if ($birthday_from > $birthday_to) {
                $key = $attribute;
                $this->addError($key, Yii::t('backend', "End date must be greater than Start date."));
            }
        }
    }

    // **************************************** //
    // Check Display Last visit date TO And Last visit date From
    // **************************************** //
    public function checkStartLastVisit($attribute, $params) {
        $last_visit_date_from = $this->last_visit_date_from;
        $last_visit_date_to = $this->last_visit_date_to;
        if ($last_visit_date_from > $last_visit_date_to) {
            $key = $attribute;
            $this->addError($key, Yii::t('backend', "End date must be greater than Start date."));
        }
    }

    // **************************************** //
    // Check Display visit time TO And visit time From
    // **************************************** //
    public function checkStartVisitTime($attribute, $params) {
        $visit_number_min = $this->visit_number_min;
        $visit_number_max = $this->visit_number_max;
        if ($visit_number_min > $visit_number_max) {
            $key = $attribute;
            $this->addError($key, Yii::t('backend', "Exit number, please specify the number since the start of the number."));
        }
    }

    // **************************************** //
    // Check checkGrantPoint
    // **************************************** //
    public function checkGrantPoint($attribute, $params) {
        $grant_point = $this->grant_point;
        if ($grant_point > 9999999.999) {
            $key = $attribute;
            $this->addError($key, Yii::t('backend', "Please enter only from 0 to 9999999.999."));
        }
        if ($grant_point < 0) {
            $key = $attribute;
            $this->addError($key, Yii::t('backend', "Discount Poin of the format is not correct."));
        }
    }

    // **************************************** //
    // Check checkGrantPoint
    // **************************************** //
    public function validateProductTax() {

        $this->addError('error_product_tax', Yii::t('backend', "Set price,in the case of all you can drink,since it can not tax rate is to set the different products, please try again."));
    }

    public function validateProduct($attribute, $params) {
        if (strlen($this->option_hidden && $this->benefits_content != '00' && $this->benefits_content != '01') == 0) {
            $key = $attribute;
            $this->addError($key, Yii::t('backend', "Please select a commodity."));
        }
    }

    public function checkStartBirthDayFrom($attribute, $params) {
        $birthday_from = $this->birthday_from;
        $date_now = date("Y/m/d");
        if (!empty($birthday_from)) {
            if ($birthday_from > $date_now) {
                $key = $attribute;
                $this->addError($key, Yii::t('backend', "Date of birth date is incorrect."));
            }
        }
    }

    public function listCategories() {
        $product_categories = (new \yii\db\Query())->select(['category_id'])
                ->from(MstProduct::tableName())
                ->innerJoin(ProductCoupon::tableName(), ProductCoupon::tableName() . '.product_id = ' . MstProduct::tableName() . '.id'
                        . ' and ' . ProductCoupon::tableName() . '.del_flg = :zero'
                        . ' and ' . ProductCoupon::tableName() . '.coupon_id = :id')
                ->where([
                    MstProduct::tableName() . '.del_flg' => '0'
                ])
                ->addParams([
                    'id' => $this->id,
                    'zero' => '0'
                ])
                ->all();

        return MstCategoriesProduct::findFrontend()->andWhere(['id' => $product_categories]);
    }

    public function checkDiscountPercent() {
        if ($this->discount_percent > 99) {
            $this->addError('discount_percent', Yii::t('backend', "You can not enter more than 99."));
        }
    }

    public function getCouponByJancode($jancode = null, $store_id = null) {
        if ($jancode !== null && $store_id !== null) {
            $result = (new \yii\db\Query())
                    ->select(['*'])
                    ->from('coupon')
                    ->where(['coupon_jan_code' => $jancode])
                    ->andWhere(['<=', 'start_date', date('Y-m-d')])
                    ->andWhere(['>=', 'expire_date', date('Y-m-d')])
                    ->andWhere(['store_id' => $store_id])
                    ->andWhere(['del_flg' => '0'])
                    ->one();
            return $result;
        }

        return null;
    }

    public function getName() {
        return $this->first_name . ' ' . $this->last_name;
    }

    /**
     * Check Products model exists based on its primary key value.
     * If the model is not exists, return false.
     * @param integer $id
     * @return true
     */
    public function CheckProductsExists() {
        
        if (!empty($this->option_hidden)) {
            
            $check_value_option = substr($this->option_hidden, -1);
            if($check_value_option == ',')
            {
                $this->option_hidden = rtrim($this->option_hidden,",");
                $count_array_products = count(explode(",", $this->option_hidden));
            }else{
                $count_array_products = count(explode(",", $this->option_hidden));
            }

            if ((new MstProduct())->getCountProductsExists($this->option_hidden) != $count_array_products) {
                return false;
            } else {
                return true;
            }
            
        } else {
            return true;
        }
    }

}
