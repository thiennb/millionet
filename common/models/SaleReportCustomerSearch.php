<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\MasterSettingAutoSendNotice;

/**
 * SaleSearch represents the model behind the search form about `common\models\MasterOrder`.
 */
class SaleReportCustomerSearch extends MstOrder
{
    /**
     * @inheritdoc
     */
    public $store_id;
    public function rules()
    {
        return [
            [['process_date', 'store_id'], 'safe'],
            [['booking_id'], 'required'],
            [['booking_id', 'created_at', 'created_by', 'updated_at', 'updated_by'], 'integer'],
            [['point_use', 'change_use', 'discount_percent', 'discount_value', 'discount_coupon', 'detail_money', 'subtotal', 'total', 'tax', 'point_add', 'money_credit', 'money_ticket', 'money_other', 'money_receive', 'money_refund'], 'number'],
            [['order_code'], 'string', 'max' => 10],
            [['process_time'], 'string', 'max' => 5],
            [['del_flg'], 'string', 'max' => 1],
        ];
    }
}