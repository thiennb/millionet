<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii2tech\ar\softdelete\SoftDeleteBehavior;
use yii\behaviors\BlameableBehavior;
use common\components\Util;
use common\components\Constants;

/**
 * This is the model class for table "mst_money_input_output".
 *
 * @property integer $id
 * @property date $process_date
 * @property string $process_time
 * @property string $management_id
 * @property string $store_code
 * @property string $order_code
 * @property string $process_type
 * @property numeric $number_10000
 * @property numeric $number_5000
 * @property numeric $number_2000
 * @property numeric $number_1000
 * @property numeric $number_500
 * @property numeric $number_100
 * @property numeric $number_50
 * @property numeric $number_10
 * @property numeric $number_5
 * @property numeric $number_1
 * @property numeric $input_money
 * @property numeric $output_money
 * @property numeric $reji_money
 * @property numeric $money_cash
 * @property numeric $money_point
 * @property numeric $money_charge
 * @property numeric $money_credit
 * @property numeric $money_ticket
 * @property numeric $money_other
 * @property numeric $prepare_money
 * @property string $del_flg
 * @property integer $created_at
 * @property integer $created_by
 * @property integer $updated_at
 * @property integer $updated_by
 */
class MasterMoneyInputOutput extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'mst_money_input_output';
    }
    
    /**
     * @inheritdoc
     */
    public function rules() {
        return [];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => Yii::t('backend', 'ID'),
            'process_date' => Yii::t('backend', 'Process Date'),
            'process_time' => Yii::t('backend', 'Process Time'),
            'management_id' => Yii::t('backend', 'Management Id'),
            'store_code' => Yii::t('backend', 'Store Code'),
            'order_code' => Yii::t('backend', 'Order Code'),
            'process_type' => Yii::t('backend', 'Process Type'),
            'number_10000' => Yii::t('backend', 'Number 10000'),
            'number_5000' => Yii::t('backend', 'Number 5000'),
            'number_2000' => Yii::t('backend', 'Number 2000'),
            'number_1000' => Yii::t('backend', 'Number 1000'),
            'number_500' =>  Yii::t('backend','Number 500'),
            'number_100' =>  Yii::t('backend','Number 100'),
            'number_50'  =>  Yii::t('frontend','Number 50'),
            'number_10' => Yii::t('frontend','Number 10'),
            'number_5' =>  Yii::t('frontend','Number_5'),
            'number_1' =>     Yii::t('frontend','Number 1'),
            'input_money' =>     Yii::t('frontend','Input Money'),
            'output_money' =>     Yii::t('frontend','Output Money'),
            'reji_money' =>     Yii::t('frontend','Reji Money'),
            'money_cash' =>     Yii::t('frontend','Money Cash'),
            'money_point' =>     Yii::t('frontend','Money Point'),
            'money_charge' =>     Yii::t('frontend','Money Charge'),
            'money_credit' =>     Yii::t('frontend','Money Credit'),
            'money_ticket' =>     Yii::t('frontend','Money Ticket'),
            'money_other' =>     Yii::t('frontend','Money Other'),
            'prepare_money'   =>  Yii::t('frontend','Prepare Money'),
            'del_flg'   =>  Yii::t('frontend','Delete Flag'),
            'created_at'   =>  Yii::t('frontend','Created At'),
            'created_by'   =>  Yii::t('frontend','Created By'),
            'updated_at'   =>  Yii::t('frontend','Updated At'),
            'updated_by'   =>  Yii::t('frontend','Updated By'),
        ];
    }

    
    public static function find()
    {
        return parent::find()->where(['mst_money_input_output.del_flg'=>'0']);
    }
    
    /**
     * get total money of current date
     * @param storeCode(string) :  store code of User is Login
     */
    public function findTotalMoneyDateStaff($storeCode = null){
        if($storeCode !== null){
            $totalMoney = $this->find()->where(['mst_money_input_output.store_code'=>$storeCode, 'process_date'=>date('Y-m-d'), 'del_flg' => '0'])->sum('reji_money');
            if($totalMoney!== null)
                return $totalMoney;
        }
        
        return 0;
    }
    
    public function findtotalMoneyMonthStaff($storeCode = null){
        if($storeCode !== null){
            $totalMoney = $this->find()->where(['store_code'=>$storeCode, 'EXTRACT(month FROM (process_date))'=> date('m'), 'del_flg' => '0'])->sum('reji_money');
            if($totalMoney!== null)
                return $totalMoney;
        }
        
        return 0;
    }
    
    public function findAllMoneyDateStaff($storeCode = null){
        if($storeCode !== null){
            return $this->find()->where(['store_code'=>$storeCode, 'process_date'=>date('Y-m-d'), 'del_flg'=> '0'])->all();
        }
        
        return null;
    }
    
}
