<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use yii2tech\ar\softdelete\SoftDeleteBehavior;
use common\components\Util;

/**
 * This is the model class for table "company".
 *
 * @property integer $id
 * @property string $company_code
 * @property string $name
 * @property string $name_kana
 * @property string $tel
 * @property string $fax
 * @property string $email
 * @property string $home_page
 * @property string $post_code
 * @property string $address
 * @property string $del_flg
 * @property integer $created_at
 * @property integer $created_by
 * @property integer $updated_at
 * @property integer $updated_by
 * @property string $app_header_background_color 
 * @property string $app_content_background_color 
 * @property string $app_footer_background_color 
 * @property string $app_header_character_color 
 * @property string $app_content_character_color 
 * @property string $app_footer_character_color 
 */
class Company extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'company';
    }

    public function behaviors() {
        return [
            TimestampBehavior::className(),
            BlameableBehavior::className(),
            'softDeleteBehavior' => [
                'class' => SoftDeleteBehavior::className(),
                'softDeleteAttributeValues' => [
                    'del_flg' => true
                ],
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['company_code', 'name'], 'required'],
            [['created_at', 'created_by', 'updated_at', 'updated_by'], 'integer'],
            [['company_code'], 'string', 'max' => 20],
            [['name', 'name_kana'], 'string', 'max' => 100],
            [['name_kana'], 'match', 'pattern' => '/^[ァ-ヴー]+$/u', 'message' => Yii::t('backend', '{attribute} invalid characters full size only')],
            [['tel', 'fax'], 'number', 'min' => 17],
            [['post_code'], 'match','pattern' => '/^[0-9]$/', 'message' => Yii::t('backend', '{attribute} invalid characters company')],
            [['email'], 'string', 'max' => 60],
            [['home_page'], 'string', 'max' => 300],
            [['post_code'], 'string'],
            [['address', 'address2'], 'string', 'max' => 240],
            [['del_flg'], 'string', 'max' => 1],
            [['email'], 'checkEmail'],
            [['home_page'], 'url', 'defaultScheme' => ''],
            [['app_header_background_color', 'app_content_background_color', 'app_footer_background_color', 'app_header_character_color', 'app_content_character_color', 'app_footer_character_color', 'address2'], 'safe'],
            // validate company_code
            [ 'company_code', 'match', 'not' => true, 'pattern' => '/[^a-zA-Z0-9]/', 'message' => Yii::t('backend', '{attribute} invalid characters membership')],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => Yii::t('backend', 'ID'),
            'company_code' => Yii::t('backend', 'Company Code'),
            'name' => Yii::t('backend', 'Company Name'),
            'name_kana' => Yii::t('backend', 'Company Name Kana'),
            'tel' => Yii::t('backend', 'Phone'),
            'fax' => Yii::t('backend', 'Fax'),
            'email' => Yii::t('backend', 'Email'),
            'home_page' => Yii::t('backend', 'Homepage URL'),
            'post_code' => Yii::t('backend', 'PostCode'),
            'address' => Yii::t('backend', 'Address'),
            'del_flg' => Yii::t('backend', 'Del Flg'),
            'created_at' => Yii::t('backend', 'Created At'),
            'created_by' => Yii::t('backend', 'Created By'),
            'updated_at' => Yii::t('backend', 'Updated At'),
            'updated_by' => Yii::t('backend', 'ID'),
        ];
    }

    /**
     * Check email
     * @param string $attribute, $params
     * @return error 
     * @throws 
     */
    public function checkEmail($attribute, $params) {


        $check = preg_match(
                '/^[A-z]+[.A-z0-9_\-]+[@][A-z0-9_\-]+([.][A-z0-9_\-]+)+[A-z.]{2,4}$/', $this->$attribute
        );

        //check valid email
        if (!$check) {
            $key = $attribute;
            //add message error
            $this->addError($key, Yii::t('backend', "email not valid"));
        }
    }

    // Get list company by company code
    public function listCompany() {
        $company_id = Util::getCookiesCompanyId();

        return Company::find()->where(['company.del_flg' => '0', 'company.id' => $company_id])->one();
    }

    protected function findModel($id) {
        if (($model = Company::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
