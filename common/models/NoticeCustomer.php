<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use yii2tech\ar\softdelete\SoftDeleteBehavior;

/**
 * This is the model class for table "mst_notice".
 *
 * @property integer $id
 * @property integer $notice_id
 * @property integer $customer_id
 * @property string $del_flg
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 */
class NoticeCustomer extends \yii\db\ActiveRecord
{
    public $customer_jan_code;
    public $first_name;
    public $last_name;
    public $sex;
    public $birth_date;
    public $last_visit_date;
    public $number_visit;
    public $rank;
    public $last_staff_name;
    public $send_date, $send_time;
    public $title;
    public $content;
    public $store_name;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'notice_customer';
    }
        
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'TimestampBehavior' => [
                'class' => TimestampBehavior::className()
            ],
            BlameableBehavior::className(),
            'softDeleteBehavior' => [
                'class' => SoftDeleteBehavior::className(),
                'softDeleteAttributeValues' => [
                    'del_flg' => '1'
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['notice_id', 'customer_id'], 'required'],
            [['created_at', 'updated_at', 'updated_by', 'created_by', 'notice_id', 'customer_id','id'], 'integer'],
            ['updated_by','safe'],
            [['del_flg'], 'string', 'max' => 1],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend', 'ID'),
            'notice_id' => Yii::t('backend', 'Notice Id'),
            'customer_id' => Yii::t('backend', 'Customer Id'),
            'del_flg' => Yii::t('backend', 'Del Flg'),
            'created_at' => Yii::t('backend', 'Created At'),
            'updated_at' => Yii::t('backend', 'Updated At'),
            'created_by' => Yii::t('backend', 'Created By'),
            'updated_by' => Yii::t('backend', 'Updated By'),
            'sex' => Yii::t('backend', 'Sex'),
            'number_visit' => Yii::t('backend', 'Visit number'),
        ];
    }
    
    public static function find()
    {
        
        return parent::find()->where((new \common\components\FindPermission)->getPermissionCondition(self::tableName(), true));
    }
    
    /**
     * Update statust send notice
     * parameter integer $id
     * @inheritdoc
     */
    public function updateDeliverSchedule($id){
        $notice_customer = NoticeCustomer::findOne($id);
        NoticeCustomer::updateAll(['status_send' => MasterNotice::STATUS_SEND_CANCEL,'updated_at' => strtotime("now")],['notice_id' => $notice_customer->notice_id]);
    }
    
    /**
     * Update read flag
     * parameter integer $notice ID
     * parameter integer $customer ID
     * @inheritdoc
     */
    public static function getByNoticeId($noticeId, $customerId){
        $notice_customer = NoticeCustomer::find()->andwhere(['notice_id' => $noticeId])
                                ->andWhere(['customer_id' => $customerId]);
        return $notice_customer ->one();
    }
    
    /**
     * Update read flag
     * parameter integer $notice ID
     * parameter integer $customer ID
     * parameter integer $customer ID
     * @inheritdoc
     */
    public static function getByNoticeId_Customer_Store($noticeId, $customerId, $storeId){
        $notice_customer = NoticeCustomer::find()->andwhere(['notice_id' => $noticeId])
                                ->andWhere(['customer_id' => $customerId])
                                ->andWhere(['store_id' => $storeId]);
        return $notice_customer ->one();
    }
    
    public function getStore() {
        return $this->hasOne(MasterStore::className(), ['id' => 'store_id']);
    }
    
    public function getNotice() {
        return $this->hasOne(MasterNotice::className(), ['id' => 'notice_id']);
    }
    
    public function getCustomer() {
        return $this->hasOne(MasterCustomer::className(), ['id' => 'customer_id']);
    }
    public function getCustomerStore() {
        return $this->hasOne(CustomerStore::className(), ['customer_id' => 'customer_id'], ['store_id' => 'store_id']);
    }
    
    /**
     * Update read flag
     * parameter integer $notice ID
     * parameter integer $customer ID
     * @inheritdoc
     */
    public function updateReadFlag($noticeId, $customerId){
        $date = strtotime(date('Y-m-d H:i:s'));
//        $notice_customer = NoticeCustomer::getByNoticeId($noticeId, $customerId);
        NoticeCustomer::updateAll(['read_flg'=>'1', 'updated_at' => $date, 'updated_by' => $customerId],['and',['notice_id' => $noticeId], ['customer_id' => $customerId]]);
    }
}
