<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace common\models;
use Yii;
use yii\db\Expression;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use yii2tech\ar\softdelete\SoftDeleteBehavior;
/**
 * This is the model class for table "order_detail".
 *
 * @property integer $id
 * @property integer $order_id
 * @property string $jan_code
 * @property string $product_display_name
 * @property string $quantity
 * @property string $price
 * @property string $price_down_type
 * @property string $price_down_value
 * @property string $product_sub_total
 * @property string $product_total
 * @property string $product_tax
 * @property string $tax_display_method
 * @property string $gift_flg
 * @property string $del_flg
 * @property integer $created_at
 * @property integer $created_by
 * @property integer $updated_at
 * @property integer $updated_by
 */
class MstOrderDetailTmp extends \yii\db\ActiveRecord{
    /**
     * @inheritdoc
     */
    public $check_list;
    public $revenue, $total_revenue, $profit, $total_profit, $quantitys, $total_quantitys;
    public static function tableName()
    {
        return 'order_detail_tmp';
    }
    /**
     * @inheritdoc
     */
    
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),            
            BlameableBehavior::className(),
            'softDeleteBehavior' => [
                'class' => SoftDeleteBehavior::className(),
                'softDeleteAttributeValues' => [
                    'del_flg' => '1'
                ],
            ],
        ];
    }
    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['created_at', 'created_by', 'updated_at', 'updated_by'], 'integer'],
            [['quantity', 'price', 'price_down_value', 'product_sub_total', 'product_total', 'product_tax'], 'number'],
            [['jan_code'], 'string', 'max' => 13],
            [['check_list'], 'safe'],
            [['product_display_name'], 'string', 'max' => 100],
            [['price_down_type', 'gift_flg', 'del_flg'], 'string', 'max' => 1],
            [['tax_display_method'], 'string', 'max' => 2],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'company_id'    => 'Company Id',
            'order_code' => 'Order Code',
            'jan_code' => 'Jan Code',
            'product_display_name' => 'Product Display Name',
            'price' => 'Price',
            'price_down_type' => 'Price Down Type',
            'price_down_value' => 'Price Down Value',
            'product_sub_total' => 'Product Sub Total',
            'product_total' => 'Product Total',
            'product_tax' => 'Product Tax',
            'tax_display_method' => 'Tax Display Method',
            'gift_flg' => 'Gift Flg',
            'del_flg' => 'Del Flg',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
            'product_name' => Yii::t('backend', 'Product name'),
            'quantity' =>  Yii::t('backend', 'Quantity'),
        ];
    }
    
    public function getMasterProduct() {
        return $this->hasOne(MstProduct::className(), ['jan_code' => 'jan_code']);
    }
    
    public function getOrder() {
        return $this->hasOne(MstOrder::className(), ['order_code' => 'order_code']);
    }
    
    public function getManagement() {
        return $this->hasOne(ManagementLogin::className(), ['management_id' => 'product_management_id']);
    }

    public static function find() {
        return parent::find()->where(['or', [MstOrderDetail::tableName() . '.del_flg' => '0'], [MstOrderDetail::tableName() . '.del_flg' => null]]);
    }
    
    public function insertGiftDetailFromOrderDetail($list_product_id, $order_id) {
        if (!empty(MasterGift::findOne(['order_id' => $order_id]))) {
            $newGift = new MasterGift();
            $order = MstOrder::findOne(['id' => $order_id])->with("booking");
            $newGift->gift_code = MasterGift::createGiftCode(null);
            $newGift->gift_date = $order->process_date;
            $newGift->order_id = $order_id;
            $newGift->customer_id = $order->booking->customer_id;
        }
    }
    
    public function findProductSales($listCateogries = null, $startDate = '', $endDate = '', $storeId = null){
        if($listCateogries !== null && $storeId !== null){
            $store = MasterStore::find()->select(['id','name_kana','name','store_code', 'away_dates', 'away_gimmick_dates'])->where(['id' => $storeId])->one();
            
            if(!empty($store)){
                
                $listProduct = MstProduct::find()->select('jan_code')->where(['in', 'category_id', $listCateogries])->distinct()->all();
                if(!empty($listProduct)){
                    
                    $listJanCodeProduct = array();
                    $listOrderCode = array();
                    $listOrder = MstOrder::find()->select('order_code')
                            ->where(['BETWEEN', 'process_date', date('Y-m-d', strtotime($startDate)), date('Y-m-d', strtotime($endDate))])
                            ->andWhere(['left(order_code,5)'=>$store['store_code']])
                            ->andWhere(['del_flg'=>'0'])
                            ->distinct()->all();

                    if(!empty($listOrder)){
                        foreach($listProduct as $product){
                            if($product['jan_code'] !== '')
                                $listJanCodeProduct[] = $product['jan_code'];
                        }
                        foreach($listOrder as $order){
                            $listOrderCode[] = $order['order_code'];
                        }
                        
                        $listOrderDetail = MstOrderDetail::find()
                                ->select(['SUM(order_detail_tmp.quantity) as total_quantitys', 
                                    'SUM(order_detail_tmp.product_total) as total_revenue', 
                                    'SUM(order_detail_tmp.product_total - (order_detail.quantity * mst_product.cost)) as total_profit',
                                    'order_detail_tmp.jan_code'])
                                ->innerJoin('mst_product', 'order_detail_tmp.jan_code = mst_product.jan_code')
                                ->where(['in', 'order_detail_tmp.jan_code', $listJanCodeProduct])
                                ->andWhere(['in', 'order_detail_tmp.order_code', $listOrderCode])
                                ->andWhere(['mst_product.store_id'=>$storeId])
                                ->groupBy(['order_detail_tmp.jan_code']);
                        return $listOrderDetail;
                    }
                }                
            }
        }
        
        return null;
    }
    
    public function getSumRevenueQuantity($listCateogries = null, $startDate = '', $endDate = '', $storeId = null){
        if($listCateogries !== null && $storeId != null){
            $store = MasterStore::find()->select(['id','name_kana','name','store_code', 'away_dates', 'away_gimmick_dates'])->where(['id' => $storeId])->one();
            if(!empty($store)){

                $listProduct = MstProduct::find()->select('jan_code')->where(['in', 'category_id', $listCateogries])->distinct()->all();
                if(!empty($listProduct)){

                    $listJanCodeProduct = array();
                    $listOrderCode = array();
                    $listOrder = MstOrder::find()->select('order_code')
                            ->where(['BETWEEN', 'process_date', date('Y-m-d', strtotime($startDate)), date('Y-m-d', strtotime($endDate))])
                            ->andWhere(['left(order_code,5)'=>$store['store_code']])
                            ->andWhere(['del_flg'=>'0'])
                            ->distinct()->all();
                    if(!empty($listOrder)){
                        foreach($listProduct as $product){
                            if($product['jan_code'] !== '')
                                $listJanCodeProduct[] = $product['jan_code'];
                        }
                        foreach($listOrder as $order){
                            $listOrderCode[] = $order['order_code'];
                        }
                        
                        $sumInfo = MstOrderDetail::find()
                                ->select(['SUM(order_detail_tmp.quantity) as total_quantitys', 
                                    'SUM(order_detail_tmp.product_total) as total_revenue', 
                                    'SUM(order_detail_tmp.product_total - (order_detail.quantity * mst_product.cost)) as total_profit'])
                                ->innerJoin('mst_product', 'order_detail.jan_code = mst_product.jan_code')
                                ->where(['in', 'order_detail_tmp.jan_code', $listJanCodeProduct])
                                ->andWhere(['in', 'order_detail_tmp.order_code', $listOrderCode])
                                ->andWhere(['mst_product.store_id'=>$storeId])
                                ->all();
                        return $sumInfo;
                    }
                }                
            }
        }
        
        return null;
    }
}
