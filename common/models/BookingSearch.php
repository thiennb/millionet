<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\db\Query;
use Carbon\Carbon;
use yii\data\ActiveDataProvider;
use common\models\Booking;
use yii\db\Expression;
use common\components\Util;
use common\models\MasterCoupon;

/**
 * BookingSearch represents the model behind the search form about `common\models\Booking`.
 */
class BookingSearch extends Booking {

    const IDLIST_PATTERN = '/^\d+(,\d+){0,}$/';

    /**
     * @inheritdoc
     */
    //product_category
    public $list_category, $product_name;
    //table: mst_customer
    public $birth_date_from, $birth_date_to;
    public $customer_first_name, $customer_last_name, $sex, $rank, $customer_jan_code, $mobile;
    //table: customer_store
    public $number_visit_min, $number_visit_max;
    public $last_visit_date_from, $last_visit_date_to;
    //table: booking
    public $booking_time_min, $booking_time_max;
    //table:
    public $last_seat_id, $last_staff_id;
    public $seat_type, $service_staff_id;
    public $list_status;
    public $seat_group_id;

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['booking_time_max', 'booking_time_min', 'action', 'created_at', 'updated_at'], 'integer'],
            [['memo', 'list_status', 'list_category', 'product_name', 'start_time', 'end_time', 'booking_date', 'demand', 'service_staff_id', 'del_flg'], 'safe'],
            [['booking_date_from', 'booking_date_to', 'birth_date_to', 'birth_date_from', 'last_visit_date_from', 'last_visit_date_to'], 'date', 'format' => 'php:Y/m/d'],
            [['customer_first_name', 'customer_last_name', 'sex', 'rank', 'mobile', 'store_id', 'customer_jan_code', 'last_staff_id'], 'safe'],
            [['id', 'customer_id', 'last_seat_id', 'seat_type', 'staff_id', 'status',], 'safe'],
            [['product_name'], 'string', 'max' => 100],
            [['customer_jan_code'], 'string', 'max' => 13],
            [['mobile'], 'string', 'max' => 17],
            [['mobile'], 'integer', 'min' => 0],
            [['booking_time_max', 'booking_time_min'], 'string', 'max' => 6],
            [['customer_first_name', 'customer_last_name'], 'string', 'max' => 40],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params, $customer_id = null) {
        $query = Booking::find()
                // Join : Company store
//                ->innerJoin('company_store', 'company_store.store_id = booking.store_id');
//        $company_code = Util::getCookiesCompanyCode();
//        $query->andWhere([
//            'company_store.company_code' => $company_code,
//        ]);
                ->innerJoin('mst_store', 'mst_store.id = booking.store_id')
                ->innerJoin('company', 'company.id = mst_store.company_id');
                //->innerJoin('customer_store', 'customer_store.store_id = mst_store.id');
        
        $company_id = Util::getCookiesCompanyId();
        $query->andFilterWhere([
            'company.id' => $company_id,
        ]);

        $this->load($params);
        // if (!($this->service_staff_id == '') && !($this->store_id == "")) {
//            $query->joinWith([
//                'masterCustomer' => function (\yii\db\ActiveQuery $query) {
//                    $query->joinWith('order', true, 'INNER JOIN');
//                }
//            ], true, 'INNER JOIN');
        //   $query->joinWith('masterCustomer', true, 'INNER JOIN');
        //$query->innerJoin('mst_order', 'mst_order.booking_id = booking.id');
        //  } else {
        $query->joinWith('masterCustomer', true, 'INNER JOIN');
        //  }
        // $query->joinWith('storeMaster');
        $query->joinWith('customerStore');
        // add conditions that should always apply here
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => false
        ]);

        $query->andWhere(['mst_customer.company_id' => Util::getCookiesCompanyId()]);
        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            $query->where('0=1');
            return $dataProvider;
        }
        if (isset($customer_id)) {
            $query->andFilterWhere([Booking::tableName() . '.customer_id' => $customer_id]);
            //return $dataProvider; // HungLQ fix missing store name
        }

        // grid filtering conditions


        $query->andFilterWhere(['=', MasterCustomer::tableName() . '.customer_jan_code', $this->customer_jan_code]);
        $query->andFilterWhere(['or', ['like', MasterCustomer::tableName() . '.first_name', $this->customer_first_name], ['like', MasterCustomer::tableName() . '.first_name_kana', $this->customer_first_name]]);
        $query->andFilterWhere(['or', ['like', MasterCustomer::tableName() . '.last_name', $this->customer_last_name], ['like', MasterCustomer::tableName() . '.last_name_kana', $this->customer_last_name]]);
        $query->andFilterWhere([MasterCustomer::tableName() . '.sex' => $this->sex]);
        $query->andFilterWhere([MasterCustomer::tableName() . '.mobile' => $this->mobile]);
        $query->andFilterWhere(['>=', MasterCustomer::tableName() . '.birth_date', $this->birth_date_from]);
        $query->andFilterWhere(['<=', MasterCustomer::tableName() . '.birth_date', $this->birth_date_to]);
        if (!empty($this->product_name)) {
            $tmp_productname = strtolower($this->product_name);
            $query->joinWith([
                'bookingProduct' => function (\yii\db\ActiveQuery $query) {
                    $query->joinWith('product');
                }
            ]);
            $query->joinWith([
                'bookingCoupon' => function (\yii\db\ActiveQuery $query) {
                    $query->joinWith('coupon');
                }
            ]);
            $query->andFilterWhere(['or', ['like', 'LOWER(' . MstProduct::tableName() . '.name)', $tmp_productname], ['like', 'LOWER(' . MstProduct::tableName() . '.name_kana)', $tmp_productname], ['like', 'LOWER(title)', $tmp_productname]]);
        }
        if (!empty($this->list_category)) {
            foreach ($this->list_category as $cate_id) {
                //Do: Get list product_id have category_id = category_id user choose
                $product = MstProduct::find()->andWhere([MstProduct::tableName() . '.category_id' => $cate_id])->select('id');
                // HoangNQ 2016/10/27
                //DO: Find list coupon_id in have product_id in "list product_id"
                $product_coupon = ProductCoupon::find()->select(ProductCoupon::tableName() . ".coupon_id")->andWhere(['product_id' => $product])->groupBy(ProductCoupon::tableName() . ".coupon_id");
                //DO: Find list booking_id in have coupon_id in "list coupon_id"
                $booking_coupon = BookingCoupon::find()->select(BookingCoupon::tableName() . ".booking_id")->andWhere(['coupon_id' => $product_coupon])->groupBy(BookingCoupon::tableName() . ".booking_id");
                //DO: Find list booking_id in have product_id in "list product_id"
                $count_booking_id = BookingProduct::find()->select(BookingProduct::tableName() . ".booking_id")->andWhere(['product_id' => $product])->groupBy(BookingProduct::tableName() . ".booking_id")->having([">", "COUNT(" . BookingProduct::tableName() . ".booking_id)", 0]);
                //DO: Add conditions to query
                $query->andFilterWhere(['OR', [Booking::tableName() . '.id' => $booking_coupon], [Booking::tableName() . '.id' => $count_booking_id]]);
                //$query->andFilterWhere([Booking::tableName() . '.id' => $count_booking_id]);
                // HoangNQ 2016/10/27
            }
        }

        $query->andFilterWhere(['=', CustomerStore::tableName() . '.rank_id', $this->rank]);
        $query->andFilterWhere(['>=', CustomerStore::tableName() . '.last_visit_date', $this->last_visit_date_from]);
        $query->andFilterWhere(['<=', CustomerStore::tableName() . '.last_visit_date', $this->last_visit_date_to]);
        $query->andFilterWhere(['>=', 'booking_date', $this->booking_date_from]);
        $query->andFilterWhere(['<=', 'booking_date', $this->booking_date_to]);
        if (!($this->store_id == "")) {
            $query->andFilterWhere(['=', Booking::tableName() . '.store_id', $this->store_id]);
            //施術担当者 start
            if (!($this->service_staff_id == '') && !($this->last_staff_id == '')) {
                $query = $this->selectLastDateByOrderManagementId($this->store_id, $this->last_staff_id, $query, $this->service_staff_id, 0);
            } else if (!($this->service_staff_id == '') && ($this->last_staff_id == '')) {
                $query = $this->selectLastDateByOrderManagementId($this->store_id, $this->last_staff_id, $query, $this->service_staff_id, 1);
            } else if (!($this->last_staff_id == '' && ($this->service_staff_id == ''))) {
                $query = $this->selectLastDateByOrderManagementId($this->store_id, $this->last_staff_id, $query, $this->service_staff_id, 0);
            }
            //前回担当者 - end
            //前回利用席 - start
            //$query->andFilterWhere(['=', Booking::tableName() . ' .seat_group_id', $this->last_seat_id]);
            //前回利用席 - end
            // Search by seat_type(席タイプ) start
            if (!($this->seat_type == "")) {
                $query->innerJoin('booking', 'booking.id = booking_seat.booking_id');
                $query->andFilterWhere(['=', 'booking_seat.seat_id', $this->seat_type]);
            }
            //Search by seat_type(席タイプ) end
        }
        $query->andFilterWhere(['=', Booking::tableName() . '.status', $this->status]);
        $query->andFilterWhere(['>=', '(select COUNT(*) FROM booking as book1 where booking.customer_id = book1.customer_id)', $this->booking_time_min]);
        $query->andFilterWhere(['<=', '(select COUNT(*) FROM booking as book1 where booking.customer_id = book1.customer_id)', $this->booking_time_max]);
//        $query->andFilterWhere(['>=', 'customer_store.number_booking', $this->booking_time_min]);
//        $query->andFilterWhere(['<=', 'customer_store.number_booking', $this->booking_time_max]);
        
        //$query->groupBy('booking.id');
        $query->orderBy('booking.booking_date,booking.booking_code ASC');
        $query->select([
            'mst_store.name as store_name',
            'booking.booking_date',
            'booking.start_time',
            'mst_customer.first_name',
            'mst_customer.last_name',
            'booking.id',
            'booking.booking_code',
            'booking.booking_method',
            'mst_store.booking_resources',
            'booking.point_use',
            'booking.booking_total',
            'booking.status',
            'booking.staff_id',
        ]);
        if (!($this->seat_type == "")) {
            $query->select(['booking_seat.seat_id',]);
        }
        $query->groupBy([
            'mst_store.name',
            'booking.booking_date',
            'booking.start_time',
            'mst_customer.first_name',
            'mst_customer.last_name',
            'booking.id',
            'booking.booking_code',
            'booking.booking_method',
            'mst_store.booking_resources',
            'booking.point_use',
            'booking.booking_total',
            'booking.status',
            'booking.staff_id',
        ]);
        if (!($this->seat_type == "")) {
            $query->groupBy(['booking_seat.seat_id',]);
        }
        return $dataProvider;
    }

    public function searchHistory($params) {
        $query = Booking::find();
        // add conditions that should always apply here
        //join booking product
        $query->innerJoin(BookingProduct::tableName(), BookingProduct::tableName() . '.booking_id = ' . Booking::tableName() . '.id');
        $query->innerJoin(MstProduct::tableName(), BookingProduct::tableName() . '.product_id = ' . MstProduct::tableName() . '.id');
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 3,
            ],
            'sort' => false
        ]);
        $this->load($params);
        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        if (!empty($this->booking_date_from))
            $query->andFilterWhere(['>=', 'booking_date', $this->booking_date_from]);
        if (!empty($this->booking_date_to))
            $query->andFilterWhere(['<=', 'booking_date', $this->booking_date_to]);
        if (!empty($this->store_id))
            $query->andFilterWhere(['=', Booking::tableName() . '.store_id', $this->store_id]);
        if (!empty($this->list_status)) {
            $arrSts = [];
            foreach ($this->list_status as $sts) {
                $arrSts[] = [Booking::tableName() . '.status' => $sts];
            }
            $query->andFilterWhere(array_merge(['OR'], $arrSts));
        }
        $query->groupBy('booking.id');
        return $dataProvider;
    }

    public function searchProduct($category, $idStore, $countCategory) {
        if (!self::validateList($category)) {
            return [];
        }

        $command = Yii::$app->getDb()->createCommand(
                "SELECT
                CP.title as title,
                CP.id as id_coupon,
				CP.coupon_details_description as description,
                c.listcate,
                c.total,
                c.totaltime,
				:idStore as id_store
            FROM (
                SELECT
                        pco.coupon_id,count(*) count,SUM(p.unit_price) total,SUM(p.time_require) totaltime,array_agg(pc.name) listCate
                    FROM product_coupon pco
                    INNER JOIN mst_product p
                        ON pco.product_id = p.id
                    INNER JOIN mst_product_category pc
                        ON pc.id = p.category_id AND pc.del_flg = '0'
                    WHERE p.category_id IN ($category)
						AND p.del_flg = '0'
                        AND p.store_id = :idStore
                    GROUP BY pco.coupon_id
                    HAVING COUNT(*) = :countCategory
                    ) c
                INNER JOIN (
                    SELECT
                            pco.coupon_id ,count(*) count
                        FROM product_coupon pco
                        INNER JOIN mst_product p
                            ON pco.product_id = p.id
                        WHERE p.store_id = :idStore
                        GROUP BY pco.coupon_id
                        HAVING COUNT(*) = :countCategory
            ) d
            ON d.coupon_id = c.coupon_id
            INNER JOIN coupon CP
            ON CP.id = c.coupon_id
			WHERE CP.del_flg = '0'"
        );

        $command->bindParam(':idStore', $idStore)
                ->bindParam(':countCategory', $countCategory);

        return $result = $command->queryAll();
    }

    public static function searchCategoryByStoreId($idStore) {
        $command = Yii::$app->getDb()->createCommand(
                "SELECT
				cate.id,
				cate.name,
				count(c.id) as coupon_count
			FROM mst_product AS p
			INNER JOIN mst_product_category AS cate
				ON cate.id = p.category_id AND cate.del_flg = '0'
			LEFT JOIN product_coupon AS pc
				ON p.id = pc.product_id AND pc.del_flg = '0'
			LEFT JOIN coupon AS c
				ON c.id = pc.coupon_id
				AND (c.show_coupon = '00' OR c.show_coupon = '00,01')
				AND (c.start_date is null or start_date <= :now)
				AND (c.expire_date is null or :now <= expire_date)
				AND c.del_flg = '0'
			WHERE p.store_id = :idStore
				/*AND p.del_flg = '0'*/
				/*AND p.show_flg = '1'*/
			GROUP BY cate.id, cate.name
		");

        $command->bindParam(':idStore', $idStore);
        $now = date('Y-m-d');
        $command->bindParam(':now', $now);

        return $result = $command->queryAll();
    }

    public function searchTimeSalonsChedule($couponId, $idStore) {
        if (!self::validateList($couponId)) {
            return [];
        }

        $command = Yii::$app->getDb()->createCommand(
                "SELECT
                CP.title as title,
                CP.id as id_coupon,
                c.listcate,
                c.totaltime,
                :idStore as id_store
            FROM (SELECT
                        c.id,count(*) count, SUM(p.time_require) totaltime, array_agg(pca.name) listcate
                    FROM
                        coupon c
                    INNER JOIN
                        product_coupon pc
                    ON
                        c.id = pc.coupon_id
                    INNER JOIN
                        mst_product p
                    ON
                        p.id = pc.product_id
                    INNER JOIN
                        mst_product_category pca
                    ON
                        pca.id = p.category_id
                    WHERE
                        p.store_id = :idStore
                    	AND c.id IN ($couponId)
						AND p.del_flg = '0'
						AND c.del_flg = '0'
						AND pca.del_flg = '0'
                    GROUP BY
                        c.id
                    HAVING
                        count(*) > 0) c
            INNER JOIN coupon CP
                ON CP.id = c.id"
        );

        $command->bindParam(':idStore', $idStore);

        return $result = $command->queryAll();
    }

    // @ref StoreScheduleTemp::getSchuduleStoreTempBetweenDates
    /**
     * Search for store schedule by date time
     * @param  [type] $store_id                  [description]
     * @param  [type] $number_row                [description]
     * @param  [type] $time_execute              [description]
     * @param  [type] $start_date                [description]
     * @param  [type] $end_date                  [description]
     * @param  [type] $reservation_possible_time [description]
     * @param  mixed  $staff					 [description]
     * @return [type]                            [description]
     */
    public static function searchStoreScheduleBetweenDates($store_id, $number_row, $time_execute, $start_date, $end_date, $reservation_possible_time, $staff = null) {
        $number_row = ceil($number_row);
        $total_time = $number_row * 30;
        $h = floor($total_time / 60);
        $m = $total_time % 60;
        $time_execute = sprintf('%d:%d:00', $h, $m);
        if (!$reservation_possible_time) {
            $reservation_possible_time = 0;
        }

//        $command = "
//			select
//					*
//			from (
//				select
//					ssd1.id,
//					ssd1.staff_id,
//					ssd1.booking_date,
//					ssd1.store_id,
//					case
//						when to_timestamp(ssd1.booking_date||' '||(ssd1.booking_time),'YYYY/MM/DD HH24:MI:SS' ) > (to_timestamp(:reserveOffset, 'YYYY/MM/DD HH24:MI:SS'))
//						then 1 else 2
//					end as booking_status,
//					case
//						when
//							(
//								select
//										count(*)
//								from
//										staff_schedule_detail as ssd
//								where
//										ssd.booking_status = '1'
//								and
//										ssd.staff_id = ssd1.staff_id
//								and
//										ssd.store_id = ssd1.store_id
//								and
//										ssd.booking_time between ssd1.booking_time and ssd1.booking_time + :executeTime - '0:30:00'
//								and
//										ssd1.booking_date = ssd.booking_date
//
//							) = :numberRow
//						then
//								ssd1.booking_time
//						else
//								null
//					end as booking_time
//				from
//						staff_schedule_detail as ssd1
//				where
//						booking_status = '1' and store_id = :storeId
//				and
//						 booking_date between :startDate and :endDate
//				order by
//						staff_id,booking_date,booking_time
//				) as staff_schedule_detail
//			inner join mst_staff as staff
//				ON staff.id = staff_id
//				AND staff.del_flg = '0'
//				AND staff.show_flg = '1'
//			where
//				booking_time is not null
//			and
//				to_timestamp(booking_date||' '||booking_time,'YYYY/MM/DD HH24:MI:SS' ) > :now
//        ";
        
        $command = "
			select
					*
			from (
				select
					ssd1.id,
					ssd1.staff_id,
					ssd1.booking_date,
					ssd1.store_id,
					case
						when to_timestamp(ssd1.booking_date||' '||(ssd1.booking_time),'YYYY/MM/DD HH24:MI:SS' ) > (to_timestamp(:reserveOffset, 'YYYY/MM/DD HH24:MI:SS'))
						then 1 else 2
					end as booking_status,
					ssd1.booking_time
				from
						staff_schedule_detail as ssd1
				where
						booking_status = '1' and store_id = :storeId
                                and (
								select
										count(*)
								from
										staff_schedule_detail as ssd
								where
										ssd.booking_status = '1'
								and
										ssd.staff_id = ssd1.staff_id
								and
										ssd.store_id = ssd1.store_id
								and
										ssd.booking_time between ssd1.booking_time and ssd1.booking_time + :executeTime - '0:30:00'
								and
										ssd1.booking_date = ssd.booking_date

							) = :numberRow
				and
						 booking_date between :startDate and :endDate
				order by
						staff_id,booking_date,booking_time
				) as staff_schedule_detail
			inner join mst_staff as staff
				ON staff.id = staff_id
				AND staff.del_flg = '0'
				AND staff.show_flg = '1'
			where
				to_timestamp(booking_date||' '||booking_time,'YYYY/MM/DD HH24:MI:SS' ) > :now
        ";

        if (!empty($staff)) {
            $command .= " and staff_id=:staffId";
        }

        $command = Yii::$app->db->createCommand($command);
        $num_row_floor = floor($number_row);

        $command->bindParam(':endDate', $end_date);
        $command->bindParam(':startDate', $start_date);
        $command->bindParam(':storeId', $store_id);
        $command->bindParam(':executeTime', $time_execute);
        $command->bindParam(':numberRow', $num_row_floor);
        $now = Carbon::now();
        $formatNow = $now->format('Y-m-d H:i:s');
        $command->bindParam(':now', $formatNow);
        $addMinutes = $now->addMinutes($reservation_possible_time)->format('Y-m-d H:i:s');
        $command->bindParam(':reserveOffset', $addMinutes);

        if (!empty($staff)) {
            $command->bindParam(':staffId', $staff);
        }

        return $command->queryAll();
    }

    /**
     * Seach for distinct staff by date time
     * @param  [type]  $store_id                  [description]
     * @param  [type]  $number_row                [description]
     * @param  [type]  $time_execute              [description]
     * @param  [type]  $start_date                [description]
     * @param  [type]  $end_date                  [description]
     * @param  [type]  $reservation_possible_time [description]
     * @param  mixed $start_time                  [description]
     * @return [type]                             [description]
     */
    public static function searchStaffByDateTime($store_id, $number_row, $time_execute, $start_date, $end_date, $reservation_possible_time, $start_time = null) {
        $number_row = ceil($number_row);
        $total_time = $number_row * 30;
        $h = floor($total_time / 60);
        $m = $total_time % 60;
        $time_execute = sprintf('%d:%d:00', $h, $m);
        if (!$reservation_possible_time) {
            $reservation_possible_time = 0;
        }

//        $command = "
//	        select distinct(staff_id) as id,
//			staff.name,
//			staff.avatar,
//			staff.career,
//			staff.position, staff.introduction, staff.catch, staff.assign_product_id
//	        from (
//				select
//					*
//				from (
//					select
//						ssd1.id,
//						ssd1.staff_id,
//						ssd1.booking_date,
//						ssd1.store_id,
//						case
//							when to_timestamp(ssd1.booking_date||' '||(ssd1.booking_time),'YYYY/MM/DD HH24:MI:SS' ) > to_timestamp(:reserveOffset, 'YYYY/MM/DD HH24:MI:SS')
//							then 1 else 2
//						end as booking_status,
//						case
//							when
//								(
//									select
//											count(*)
//									from
//											staff_schedule_detail as ssd
//									where
//											ssd.booking_status = '1'
//									and
//											ssd.staff_id = ssd1.staff_id
//									and
//											ssd.store_id = ssd1.store_id
//									and
//											ssd.booking_time between ssd1.booking_time and ssd1.booking_time + :executeTime - '0:30:00'
//									and
//											ssd1.booking_date = ssd.booking_date
//
//								) = :numberRow
//							then
//									ssd1.booking_time
//							else
//									null
//						end as booking_time
//					from
//							staff_schedule_detail as ssd1
//					where
//							booking_status = '1' and store_id = :storeId
//					and
//							 booking_date between :startDate and :endDate
//					order by
//							staff_id,booking_date,booking_time
//					) as staff_schedule_detail
//				where
//						booking_time is not null
//				and
//						booking_status = 1
//				and
//						to_timestamp(booking_date||' '||booking_time,'YYYY/MM/DD HH24:MI:SS' ) > :now
//				) as store_schedule_temps
//			inner join mst_staff as staff
//				ON staff.id = staff_id
//				AND staff.del_flg = '0'
//				AND staff.show_flg = '1'
//        ";
        
        $command = "
	        select distinct(staff_id) as id,
			staff.name,
			staff.avatar,
			staff.career,
			staff.position, staff.introduction, staff.catch, staff.assign_product_id
	        from (
				select
					*
				from (
					select
						ssd1.id,
						ssd1.staff_id,
						ssd1.booking_date,
						ssd1.store_id,
						case
							when to_timestamp(ssd1.booking_date||' '||(ssd1.booking_time),'YYYY/MM/DD HH24:MI:SS' ) > to_timestamp(:reserveOffset, 'YYYY/MM/DD HH24:MI:SS')
							then 1 else 2
						end as booking_status,
						ssd1.booking_time
					from
							staff_schedule_detail as ssd1
					where
							booking_status = '1' and store_id = :storeId
                                        and (
									select
											count(*)
									from
											staff_schedule_detail as ssd
									where
											ssd.booking_status = '1'
									and
											ssd.staff_id = ssd1.staff_id
									and
											ssd.store_id = ssd1.store_id
									and
											ssd.booking_time between ssd1.booking_time and ssd1.booking_time + :executeTime - '0:30:00'
									and
											ssd1.booking_date = ssd.booking_date

								) = :numberRow
					and
							 booking_date between :startDate and :endDate
					order by
							staff_id,booking_date,booking_time
					) as staff_schedule_detail
				where
						booking_status = 1
				and
						to_timestamp(booking_date||' '||booking_time,'YYYY/MM/DD HH24:MI:SS' ) > :now
				) as store_schedule_temps
			inner join mst_staff as staff
				ON staff.id = staff_id
				AND staff.del_flg = '0'
				AND staff.show_flg = '1'
        ";

        if ($start_time !== null) {
            $command .= ' where booking_time = :startTime';
        }

        $command = Yii::$app->db->createCommand($command);

        $command->bindParam(':executeTime', $time_execute);
        $num_row_floor = floor($number_row);
        $command->bindParam(':numberRow', $num_row_floor);
        $command->bindParam(':storeId', $store_id);
        $command->bindParam(':startDate', $start_date);
        $command->bindParam(':endDate', $end_date);

        $now = Carbon::now();
        $formatNow = $now->format('Y-m-d H:i:s');
        $command->bindParam(':now', $formatNow);
        $addMinutes = $now->addMinutes($reservation_possible_time)->format('Y-m-d H:i:s');
        $command->bindParam(':reserveOffset', $addMinutes);

        if ($start_time !== null) {
            $command->bindParam(':startTime', $start_time);
        }

        return $command->queryAll();
    }

    /**
     * Search all coupons by coupon id list and store id
     * @param  string  $couponId id or a list of ids ( separated by commas )
     * @param  integer $storeId
     * @return array
     */
    public static function searchCouponsById($couponId, $storeId) {
        if (!self::validateList($couponId)) {
            return [];
        }

        $command = Yii::$app->db->createCommand(
                "SELECT
				CP.id,
				CP.title as name,
				c.listcate,
				case when CP.benefits_content = '00' or CP.benefits_content = '01' then 0
                                    else c.time_require
                                end as time_require,
				CP.coupon_details_description as description,
				CP.expire_date,
				CP.display_condition, CP.benefits_content, CP.combine_with_other_coupon_flg,
				CP.image,
				case when CP.code_membership is not null
					or CP.first_name is not null
					or CP.first_name_kana is not null
					or CP.last_name is not null
					or CP.last_name_kana is not null
					or CP.sex is not null
					or CP.birthday_from is not null
					or CP.birthday_to is not null
					or CP.last_visit_date_from is not null
					or CP.last_visit_date_to is not null
					or CP.visit_number_min is not null
					or CP.visit_number_max is not null
					or CP.rank_id is not null
					or CP.last_staff_id is not null
                                        or CP.store_id is not null
					then 1
					else 0
				end as apply_condition
			FROM (
			    SELECT
				c.id, array_agg(pca.name) as listcate, SUM(p.time_require) as time_require, c.title
			    FROM
				coupon c
			    LEFT JOIN product_coupon pc
			    	ON c.id = pc.coupon_id AND pc.del_flg = '0'
			    LEFT JOIN mst_product p
			    	ON p.id = pc.product_id AND p.del_flg = '0' AND p.store_id = c.store_id
			    LEFT JOIN mst_product_category pca
			    	ON pca.id = p.category_id
			    WHERE c.id IN ($couponId)
			    	AND c.store_id = :storeId
                                AND c.del_flg = '0'
			    GROUP BY
				c.id
			) c
			INNER JOIN coupon CP
			ON CP.id = c.id"
        );

        $command->bindParam('storeId', $storeId);

        return $command->queryAll();
    }

    public static function searchProductsById($productId, $storeId) {
        if (!self::validateList($productId)) {
            return [];
        }

        $command = Yii::$app->db->createCommand(
                "SELECT
				p.id,
				p.name,
				cate.name as category_name,
				p.time_require,
				p.description
			FROM
				mst_product as p
			LEFT JOIN mst_product_category as cate
				ON cate.id = p.category_id AND cate.del_flg = '0'
			WHERE p.store_id = :storeId
				AND p.id IN ($productId)
				AND p.del_flg = '0'
				AND p.show_flg = '1'"
        );

        $command->bindParam('storeId', $storeId);

        return $command->queryAll();
    }

    public static function searchStaffById($staffId, $storeId) {
        if (!self::validateList($staffId)) {
            return [];
        }

        $command = Yii::$app->db->createCommand(
                "SELECT
				staff.id,
				staff.name,
				staff.avatar,
				staff.career,
				staff.position,
				staff.introduction, staff.catch,
				staff.assign_product_id
			FROM
				mst_staff as staff
			/*LEFT JOIN mst_product as product
				ON product.id = staff.assign_product_id*/
			WHERE staff.store_id = :storeId
				AND staff.id IN ($staffId)
				AND staff.del_flg = '0'"
        );

        $command->bindParam('storeId', $storeId);

        return $command->queryAll();
    }

    private static function validateList($list) {
        return preg_match(self::IDLIST_PATTERN, $list);
    }

    /**
     * Get Coupon Price Table
     *
     * @param  			string $coupons list of coupon ids (separated by commas)
     * @param  optional string $date    a string of date Y-m-d, if omitted then current date is used
     * @return array of price
     * - id : coupon id
     * - no_discount_price : coupon's total price of products inside (taxed) without the discount condition
     * - tax_rate : the chosen tax rate ( nullable )
     * - total_price : discounted price, the final price
     */
    public function getCouponPriceTable($coupons = '', $date = '') {
        if (empty($coupons) || !self::validateList($coupons))
            return [];

        if ($date == '')
            $date = date('Y-m-d');
        // Fix mobile coupon price displaying
        return BookingBusiness::getPriceTable($coupons, '', '', $date)['coupons'];
    }

    /**
     * Get Product Price Table
     *
     * @param  			string $products list of product ids (separated by commas)
     * @param  optional string $date     a string of date Y-m-d, if omitted then current date is used
     * @return array of price
     * - id : product id
     * - unit_price : product's original unit price
     * - tax_rate : the chosen tax rate ( nullable )
     * - total_price : taxed price, the final price
     */
    public static function getProductPriceTable($products = '', $date = '') {
        if (empty($products) || !self::validateList($products))
            return [];

        if ($date == '')
            $date = date('Y-m-d');

        $command = "
            select
                p.id, name,
                p.unit_price,
                max(td.rate) as tax_rate,
                case when p.assign_fee_flg = '1' then 'staff'
                    else 'product'
                end as mode,
                case when p.tax_display_method = '01' and td.rate is not null then round(p.unit_price * ( 1 + td.rate / 100 ))
                        else round(p.unit_price)
                end as total_price
            from mst_product as p
            left join tax_detail as td
                on td.tax_id = p.tax_rate_id
                and td.del_flg = '0'
                and :date between td.start_date and td.end_date
            where p.id IN ($products)
                and p.del_flg = '0'
            group by p.id, p.unit_price, td.rate";

        $command = Yii::$app->db->createCommand($command);

        $command->bindParam(':date', $date);

        return $command->queryAll();
    }

    /**
     * Get all the options and their products base on coupons and store
     * @param  string  $coupons list of coupon ids
     * @param  integer $idStore  store id
     * @return array
     * - option_id
     * - option_name
     * - option_type
     * - search_by : product id used for searching
     * - search_by_name : product name used for searching
     * - id : product id (inside option)
     * - name : product name (inside option)
     * - time_require : product's time_require (inside option)
     * - category_name : product's category (inside option)
     */
    public static function searchOptionProductByIdCoupon($coupons) {
        if (!self::validateList($coupons)) {
            return [];
        }

        $command = Yii::$app->db->createCommand(
                "select
				searchOption.option_id,
				searchOption.option_name,
				searchOption.option_type,
				searchOption.product_id as search_by,
				searchOption.product_name as search_by_name,
				p.id,
				p.name,
				p.time_require,
				cate.name as category_name

			from
				(select
					o.id as option_id,
					o.name as option_name,
					o.type as option_type,
					p.id as product_id,
					p.name as product_name,
					c.store_id as store_id
				from product_coupon as pc
				inner join coupon as c
					on c.id = pc.coupon_id and c.del_flg = '0' /* no display condition */
				inner join mst_product as p
					on p.id = pc.product_id and p.del_flg = '0' and p.store_id = c.store_id /* products inside coupon, no display condition */
				/* search for options */
				inner join product_option as po
					on po.product_id = p.id and po.del_flg = '0'
				inner join mst_option as o
					on o.id = po.option_id and o.del_flg = '0' and o.store_id = c.store_id
				where c.id IN ($coupons) AND (c.benefits_content = '02' OR c.benefits_content = '03') 
				group by o.id, p.id, c.store_id order by o.id) as searchOption
			inner join product_option as po
				on po.option_id = searchOption.option_id and po.del_flg = '0'
			inner join mst_product as p
				on p.id = po.product_id and p.del_flg = '0' and p.show_flg = '1' /* need to be displayed */
				and p.id <> searchOption.product_id and p.store_id = searchOption.store_id
			left join mst_product_category as cate
				on cate.id = p.category_id and cate.del_flg = '0'
			order by option_id, search_by, p.name"
        );

        return $command->queryAll();

        /* $listQuery = (new Query())
          ->select([
          'id' => 'distinct(p.id)',
          'p.name',
          'p.time_require',
          'option_id' => 'o.id',
          'option_name' => 'o.name',
          'option_type' => 'o.type',
          'category_name' => 'cate.name',
          ])
          ->from(['po' => 'product_option'])
          ->innerJoin(['o' => 'mst_option'], 'o.id = po.option_id AND o.del_flg = :delFlg')
          ->innerJoin(['p' => 'mst_product'], 'p.id = po.product_id AND p.store_id = o.store_id AND p.del_flg = :delFlg AND p.show_flg = :showFlg')
          ->leftJoin(['cate' => 'mst_product_category'], 'cate.id = p.category_id AND cate.del_flg = :delFlg')
          ->where([
          'o.store_id' => $storeId
          ]); */
    }

    public static function searchOptionProductByIdProduct($products) {
        if (!self::validateList($products)) {
            return [];
        }

        $command = "SELECT
				o.id as option_id,
				o.name as option_name,
				o.type as option_type,
				search.product_id as search_by,
				search.name as search_by_name,
				p.id,
				p.name,
				p.time_require,
				cate.name as category_name
			FROM (
				SELECT
					po.product_id,
					po.option_id,
					p.name,
					p.store_id
				FROM product_option as po
				INNER JOIN mst_product as p
					ON p.id = po.product_id AND p.del_flg = '0'
				WHERE po.product_id IN ($products)
					AND po.del_flg = '0'
			) as search
			INNER JOIN mst_option as o
				ON o.id = search.option_id AND o.del_flg = '0'
			INNER JOIN product_option as po
				ON po.option_id = o.id AND po.del_flg = '0'
			INNER JOIN mst_product as p
				ON p.id = po.product_id AND p.del_flg = '0' AND p.id != search.product_id AND p.store_id = search.store_id
			LEFT JOIN mst_product_category as cate
				ON p.category_id = cate.id and cate.del_flg = '0' order by option_id, search_by, p.name
			";

        return Yii::$app->db->createCommand($command)->queryAll();
    }

    /**
     * Search coupons by a list of store (and/or categories)
     * @param  string 				 $stores     store id list, comma-separated
     * @param  [string] 			 $categories category id list, comma-separated
     * @param  [MasterCustomer|null] $customer	 search by customer condition 4 - 13
     * @return array
     * - id
     * - name (c.title)
     * - description
     * - time_require
     * - expire_date
     * - display_condition
     * - apply_condition
     * - image
     * -combine_with_other_coupon_flg : 1 true / 0 false 
     */
    public static function searchCouponsByStoreId($stores = '', $categories = '', MasterCustomer $customer = null, $isBackEnd = false) {
        $searchByCategories = false;
        //$categoryCondition = '';
        // check categories if specified
        if (strlen($categories)) {
            $searchByCategories = true;
            //$categoryCondition = ' and cate.id in (' . $categories . ')';

            if (!self::validateList($categories)) {
                return [];
            }
        }

        // check store id list
        if (!self::validateList($stores)) {
            return [];
        }

        $search_query = (new Query())->select([
                    'c.id',
                    'count' => 'count(*)',
                    'time_require' => 'sum(p.time_require)',
                    'listcate' => 'array_agg(distinct(cate.name))',
                    'listcate_with_id' => "array_agg(distinct(concat(cate.id,'||',cate.name)))",
                    'listcate_id' => 'array_agg(distinct(cate.id))',
                    'category_count' => 'count(distinct(cate.name))'
                ])
                ->from(['c' => 'coupon'])
                ->leftJoin('product_coupon as pc', 'c.id = pc.coupon_id and pc.del_flg = :nodel')
                ->leftJoin('mst_product as p', 'p.id = pc.product_id and p.del_flg = :nodel')
                ->leftJoin('mst_product_category as cate', 'cate.id = p.category_id and cate.del_flg = :nodel')
                ->where([
                    'c.store_id' => explode(',', $stores),
                    'c.del_flg' => '0'
                ])
                ->groupBy('c.id')
                ->having(['>', 'count(*)', 0]);

        $now = date('Y-m-d');

        $list_query = (new Query())->select([
                    'c.id',
                    'name' => 'c.title',
                    'description' => 'c.coupon_details_description',
                    'search.listcate',
                    'search.time_require',
                    'c.expire_date',
                    'c.display_condition',
                    'c.image', 'c.combine_with_other_coupon_flg', 'c.benefits_content',
                    new Expression(
                            "case when (c.code_membership is not null and c.code_membership <> '')
                            or (c.first_name is not null and c.first_name <> '')
                            or (c.first_name_kana is not null and c.first_name_kana <> '')
                            or (c.last_name is not null and c.last_name <> '')
                            or (c.last_name_kana is not null and c.last_name_kana <> '')
                            or (c.sex is not null and c.sex <> '')
                            or (c.birthday_from is not null)
                            or (c.birthday_to is not null)
                            or (c.last_visit_date_from is not null)
                            or (c.last_visit_date_to is not null)
                            or (c.visit_number_min is not null)
                            or (c.visit_number_max is not null)
                            or (c.rank_id is not null and c.rank_id <> '')
                            or (c.last_staff_id is not null)
                            or (c.store_id is not null)
                            then 1
                            else 0
                        end as apply_condition"
                    ),
                    /* add support to api */
                    'categories' => 'search.listcate_with_id',
                    'c.*'
                    /* end add support to api */
                ])
                ->from(['search' => $search_query])
                ->innerJoin('coupon as c', 'c.id = search.id')
                // all coupon showing conditions
//                ->where(['or', ['show_coupon' => '00'], ['show_coupon' => '00,01']])
                ->andWhere(['or', 'start_date is null', ['<=', 'start_date', $now]])
                ->andWhere(['or', 'expire_date is null', ['expire_auto_set' => '1'], ['>=', 'expire_date', $now]])
                ->addParams([
            'nodel' => '0'
        ]);
        if ($isBackEnd == false) {
            $list_query->andWhere(['or', ['show_coupon' => '00'], ['show_coupon' => '00,01']]);
        }


        if ($customer) {
            empty($customer->id) ? $customer->id = 0 : '';
            $customerStore = CustomerStore::findOne(['customer_id' => $customer->id, 'store_id' => $stores]);
            $customer->birth_date = trim($customer->birth_date) != null ? $customer->birth_date : null;
            if (!isset($customerStore)) {
                $customerStore = new CustomerStore();
            }

            // Get customer info                                
            $list_query
                    ->andWhere(['or',
                        ['c.sex' => $customer->sex],
                        ['c.sex' => null],
                        ['c.sex' => '']
                    ])
                    ->andWhere(['or',
                        ['c.birthday_from' => null],
                        ['<=', 'c.birthday_from', $customer->birth_date]
                    ])
                    ->andWhere(['or',
                        ['c.birthday_to' => null],
                        ['>=', 'c.birthday_to', $customer->birth_date]
                    ])
                    ->andWhere(['or',
                        ['c.code_membership' => $customer->customer_jan_code],
                        ['c.code_membership' => ''],
                        ['c.code_membership' => null]])
                    ->andWhere(['or',
                        ['c.first_name' => $customer->first_name],
                        ['c.first_name' => null],
                        ['c.first_name' => '']])
                    ->andWhere(['or',
                        ['c.last_name' => $customer->last_name],
                        ['c.last_name' => null],
                        ['c.last_name' => '']])
                    ->andWhere(['or',
                        ['c.first_name_kana' => $customer->first_name_kana],
                        ['c.first_name_kana' => null],
                        ['c.first_name_kana' => '']])
                    ->andWhere(['or',
                        ['c.last_name_kana' => $customer->last_name_kana],
                        ['c.last_name_kana' => null],
                        ['c.last_name_kana' => '']]);

            if ($customerStore) {
                $list_query
                        ->andWhere(['or',
                            ['<=', 'c.last_visit_date_from', $customerStore->last_visit_date],
                            ['c.last_visit_date_from' => null]
                        ])
                        ->andWhere(['or',
                            ['>=', 'c.last_visit_date_to', $customerStore->last_visit_date],
                            ['c.last_visit_date_to' => null]
                        ])
                        ->andWhere(['or',
                            ['<=', 'c.visit_number_min', $customerStore->number_visit],
                            ['c.visit_number_min' => null]
                        ])
                        ->andWhere(['or',
                            ['>=', 'c.visit_number_max', $customerStore->number_visit],
                            ['c.visit_number_max' => null]
                        ])
                        //9 配信店舗
                        ->andWhere(['or',
                            ['c.rank_id' => $customerStore->rank_id],
                            ['c.rank_id' => null],
                            ['c.rank_id' => '']
                ]);
                //12 要注意指定の 顧客は含まない a Phong bo
//                        ->andWhere(['or',
//                            ['c.last_staff_id' => $customerStore->last_staff_id],
//                            ['c.last_staff_id' => null]]);

                if ($customerStore->black_list_flg == '1') {
                    $list_query->andWhere(['<>', 'c.black_list_flg', '1']);
                }
            }
        }

        if ($searchByCategories) {
            $categories = explode(',', $categories);
            sort($categories);

            $list_query = $list_query->andWhere([
                'search.category_count' => count($categories),
                'search.listcate_id' => new Expression(sprintf('ARRAY[%s]', implode(',', $categories)))
            ]);
        }

        return $list_query;
    }

    /**
     * Search products by a list of store (and/or categories)
     * @param  string $stores     store id list, comma-separated
     * @param  string $categories category id list, comma-separated
     * @return array
     * - id
     * - name (c.title)
     * - description
     * - time_require
     * - expire_date
     * - display_condition
     * - apply_condition
     * - image
     */
    public function searchProductsByStoreId($stores = '', $categories = '') {
        $searchByCategories = false;
        //$categoryCondition = '';
        // check categories if specified
        if (strlen($categories)) {
            $searchByCategories = true;
            //$categoryCondition = ' and cate.id in (' . $categories . ')';

            if (!self::validateList($categories)) {
                return [];
            }
        }

        // check store id list
        if (!self::validateList($stores)) {
            return [];
        }

        $query = MstProduct::findFrontEnd()
                ->select(['id', 'name', 'time_require', 'description', 'image1', 'category_id'])
                ->with('category')
                ->andWhere(['store_id' => $stores])
                ->andWhere(['or', ['tel_booking_flg' => null], ['tel_booking_flg' => '0']])
                ->andWhere(['or', ['option_condition_flg' => null], ['option_condition_flg' => '0']])
                ->andWhere(['show_flg' => '1']);

        if ($searchByCategories) {
            $categories = explode(',', $categories);
            //sort($categories);
            $query = $query->andWhere(['category_id' => $categories]);
        }

        return $query;
    }

    public static function getRank($customer_id) {
        $query = CustomerStore::find();
        $query->select([
            'customer_store.rank_id',
            'rank.bronze_point_rate',
            'rank.sliver_point_rate',
            'rank.gold_point_rate',
            'rank.black_point_rate',
            'rank.id',
        ])->distinct();
        $query->innerJoin(MasterRank::tableName(), CustomerStore::tableName() . '.rank_id = ' . MasterRank::tableName() . '.rank_condition');
        $query->andFilterWhere(['!=', CustomerStore::tableName() . '.customer_id', $customer_id]);
        //$query->asArray()->all();
        return $query;
    }

    public function findId($id) {
        $findId = BookingProduct::find()->select(['booking_id'])->where(['id' => $id])->asArray()->one();
        $query = Booking::find()->select([
            'mst_store.name',
            'mst_store.longitude',
            'mst_store.latitude',
            'mst_store.address',
            'mst_store.tel'
        ]);
        $query->innerJoin(MasterStore::tableName(), MasterStore::tableName() . '.id = ' . Booking::tableName() . '.store_id');
        $query->where(['booking.id' => $findId['booking_id']]);
        return $query;
    }

    public function getProduct($id) {
        $findId = BookingProduct::find()->select(['booking_id'])->where(['id' => $id])->asArray()->one();
        $query = MstProduct::find()->select([
                    'booking.id',
                    'mst_product.name AS product_name',
                    'mst_product.category_id',
                        //  'booking_product.option_id',
                ])->distinct();
        $query->innerJoin(MasterStore::tableName(), MasterStore::tableName() . '.id = ' . MstProduct::tableName() . '.store_id');
        $query->innerJoin(Booking::tableName(), MasterStore::tableName() . '.id = ' . Booking::tableName() . '.store_id');
        $query->where(['booking.id' => $findId['booking_id']]);
        return $query;
    }

    public static function getStore($store_id) {
        $query = MasterStore::find();
        $query->andFilterWhere(['=', 'mst_store.id', $store_id]);
        $data = $query->one();
        return $data;
    }

    public static function getStaffById($staff_id) {
        $query = MasterStaff::find()->select(['name']);
        $query->andFilterWhere(['=', 'mst_staff.id', $staff_id]);
        $data = $query->one();
        return $data;
    }

    public static function getMoneyData($store_id, $customer_id) {
        $query = CustomerStore::find();
        $query->andFilterWhere(['=', 'customer_store.store_id', $store_id]);
        $query->andFilterWhere(['=', 'customer_store.customer_id', $customer_id]);
        $query->andFilterWhere(['=', 'customer_store.del_flg', 0]);
        $data = $query->one();
        return $data;
    }

    public static function getCancelBooking($booking_id = null, $customer_id = null) {
        $booking = Booking::find()
                ->andWhere(['id' => $booking_id])
                ->andWhere(['customer_id' => $customer_id])
                ->with(['storeMaster', 'staff'])
                ->one();
        $rejectable = ($booking->cancelOffset);
        return $rejectable;
    }

    public function selectLastDateByOrderManagementId($id_store, $last_staff_id, $query, $service_staff_id, $check_select) {

        if ($check_select == 0) {
            $command = Yii::$app->getDb()->createCommand(
                                    "SELECT 
                                    max(mst_order.order_code) as oder_code,
                                    mst_order.customer_jan_code
                                    FROM 
                                    (SELECT max(mst_order.order_code) as order_code,mst_order.customer_jan_code FROM mst_order GROUP BY mst_order.customer_jan_code)
                                    ct
                                    INNER JOIN mst_customer
                                    ON mst_customer.customer_jan_code = ct.customer_jan_code
                                    INNER JOIN booking
                                    ON mst_customer.id = booking.customer_id
                                    INNER JOIN mst_order
                                    ON mst_order.booking_id = booking.id 
                                    INNER JOIN customer_store
                                    ON customer_store.customer_id = mst_customer.id
                                    WHERE
                                    customer_store.store_id = :idStore
                                    AND
                                    mst_order.order_management_id = :idLastStaff
                                    AND 
                                    mst_order.order_management_id = :idServiceStaff
                                    AND
                                    mst_order.order_code = ct.order_code
                                    GROUP BY
                                    mst_order.customer_jan_code"
            );

            $id_last_staff = $this->converIdLastStaff(intval($last_staff_id));
            
            $command->bindParam(':idLastStaff', $id_last_staff)
                    ->bindParam(':idStore', $id_store);
            
            if ($service_staff_id != '') {
                $command->bindParam(':idServiceStaff', $this->converIdLastStaff(intval($service_staff_id)));
            } else {
                $command->bindParam(':idServiceStaff', $id_last_staff);
            }

            $result = $command->queryAll();

            $query->innerJoin('mst_order', 'mst_order.booking_id = booking.id');
            if (count($result) > 0) {
                $i = 0;
                $result_seach = null;
                foreach ($result as $value) {
                    $oder_code_sl = $value['oder_code'];
                    if ($i == 0) {
                        $result_seach = $oder_code_sl;
                    } else {
                        $result_seach .= "," . $oder_code_sl;
                    }
                    $i++;
                }
                $array_cover = explode(',', $result_seach);

                $query->andFilterWhere(['mst_order.order_code' => $array_cover]);

                return $query;
            } else {

                $query->andFilterWhere(['=', 'mst_order.created_at', 0]);
                return $query;
            }
        } else {
            $command = Yii::$app->getDb()->createCommand(
                    "SELECT 
                                    mst_order.order_code as oder_code,
                                    mst_order.customer_jan_code
                                    FROM 
                                    booking
                                    INNER JOIN mst_order
                                    ON mst_order.booking_id = booking.id 
                                    INNER JOIN mst_customer
                                    ON mst_customer.customer_jan_code = mst_order.customer_jan_code
                                    INNER JOIN customer_store
                                    ON customer_store.customer_id = mst_customer.id
                                    WHERE
                                    customer_store.store_id = :idStore
                                    AND
                                    mst_order.order_management_id = :idLastStaff "
            );

            $id_service_staff_id = $this->converIdLastStaff(intval($service_staff_id));
            
            $command->bindParam(':idLastStaff', $id_service_staff_id)
                    ->bindParam(':idStore', $id_store);

            $result = $command->queryAll();

            $query->innerJoin('mst_order', 'mst_order.booking_id = booking.id');
            if (count($result) > 0) {
                $i = 0;
                $result_seach = null;
                foreach ($result as $value) {
                    $oder_code_sl = $value['oder_code'];
                    if ($i == 0) {
                        $result_seach = $oder_code_sl;
                    } else {
                        $result_seach .= "," . $oder_code_sl;
                    }
                    $i++;
                }
                $array_cover = explode(',', $result_seach);

                $query->andFilterWhere(['mst_order.order_code' => $array_cover]);

                return $query;
            } else {

                $query->andFilterWhere(['=', 'mst_order.created_at', 0]);
                return $query;
            }
        }
    }

    public static function converIdLastStaff($id) {
        $result_staff_convert = 0;
        $id_last_staff_id = intval($id);
        if (strlen($id_last_staff_id) == 1) {
            $result_staff_convert = '0000' . $id_last_staff_id;
        } else if (strlen($id_last_staff_id) == 2) {
            $result_staff_convert = '000' . $id_last_staff_id;
        } else if (strlen($id_last_staff_id) == 3) {
            $result_staff_convert = '00' . $id_last_staff_id;
        } else if (strlen($id_last_staff_id) == 4) {
            $result_staff_convert = '0' . $id_last_staff_id;
        } else if (strlen($id_last_staff_id) == 5) {
            $result_staff_convert = $id_last_staff_id;
        }
        return $result_staff_convert;
    }

}
