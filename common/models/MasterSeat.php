<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii2tech\ar\softdelete\SoftDeleteBehavior;
use yii\behaviors\BlameableBehavior;
use yii\helpers\ArrayHelper;
use common\components\Constants;

/**
 * This is the model class for table "mst_seat".
 *
 * @property integer $id
 * @property string $store_id
 * @property string $name
 * @property integer $type_seat_id
 * @property integer $capacity_min
 * @property integer $capacity_max
 * @property string $smoking_flg
 * @property string $introduce
 * @property string $image1
 * @property string $image2
 * @property string $image3
 * @property string $show_flg
 * @property string $del_flg
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 */
class MasterSeat extends \yii\db\ActiveRecord
{
    public $schedule;
    public $shifts = [];
    public $type_seat_name;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'mst_seat';
    }

    /**
     * @inheritdoc
     */
    
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
            BlameableBehavior::className(),
            'softDeleteBehavior' => [
                'class' => SoftDeleteBehavior::className(),
                'softDeleteAttributeValues' => [
                    'del_flg' => '1'
                ],
            ],
        ];
    }
    
    public function rules()
    {
        return [
            [['type_seat_id', 'capacity_min', 'capacity_max', 'created_at', 'updated_at','store_id', 'created_by', 'updated_by'], 'integer'],
            [['introduce'], 'string'],
            [['type_seat_name'], 'safe'],
            [['name','type_seat_id','smoking_flg','capacity_min', 'capacity_max',], 'required'],
            [['name'], 'string', 'max' => 100],
            [['smoking_flg', 'show_flg', 'del_flg'], 'string', 'max' => 1],
            [['image1', 'image2', 'image3'], 'string', 'max' => 100],
            ['capacity_min', 'compare', 'compareAttribute' => 'capacity_max', 'operator' => '<=', 'enableClientValidation' => false],
            [['store_id'], 'required', 'when' => function() {
                $role = \common\components\FindPermission::getRole();
                return ($role->permission_id == Constants::STORE_MANAGER_ROLE || $role->permission_id == Constants::STAFF_ROLE);
            }],
            
        ];
    }

    /**
     * @inheritdoc
     */
    
    
    public static function find()
    {
        return parent::find()->where((new \common\components\FindPermission)->getPermissionCondition(self::tableName()));
    }
    
    public static function findFrontEnd()
    {
            return parent::find(['or', [self::tableName() . '.del_flg' => '0'], [self::tableName() . '.del_flg' => null]]);
    }
    
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend', 'ID'),
            'store_id'=>Yii::t('backend', 'Store Id'),
            'name' => Yii::t('backend', 'Name Seat'),
            'type_seat_id' => Yii::t('backend', 'Type Seat Name'),
            'capacity_min' => Yii::t('backend', 'Capacity Min'),
            'capacity_max' => Yii::t('backend', 'Capacity Max'),
            'smoking_flg' => Yii::t('backend', 'Smoking Flg'),
            'introduce' => Yii::t('app', 'Introduce'),
            'image1' => Yii::t('backend', 'Image1'),
            'image2' => Yii::t('backend', 'Image2'),
            'image3' => Yii::t('backend', 'Image3'),
            'type_seat_name' => Yii::t('backend', 'Type Seat Name'),
            'show_flg' => Yii::t('backend', 'Show Flg'),
            'del_flg' => Yii::t('backend', 'Del Flg'),
            'created_by' => Yii::t('backend', 'Created By'),
            'updated_by' => Yii::t('backend', 'Updated By'),
            'created_at' => Yii::t('backend', 'Created At'),
            'updated_at' => Yii::t('backend', 'Updated At'),
        ];
    }
        
    
    public function getSeatType()
    {
        return $this->hasOne(MasterTypeSeat::className(), ['id' => 'type_seat_id']);
    }
    
    public static function listSeatByStore($shopId = null) {
        $seat_flag = \common\components\Constants::SELECT_SEAT_TYPE;
        if ($shopId != null) {
            return ArrayHelper::map(MasterSeat::find()->innerJoin(MasterStore::tableName(), 'mst_store.id = '.MasterSeat::tableName().'.store_id')->andWhere(['and', ['=', MasterSeat::tableName().'.store_id' , $shopId], ['=', 'booking_resources', $seat_flag]])->orderBy(['name'=>SORT_ASC])->all(), 'id', 'name');
        }
        return [];
    }
    public static function listSeatByType($shopId = null, $typeSeat = null , $noSmoking = null) {
        $seat_flag = \common\components\Constants::SELECT_SEAT_TYPE;
        if ($shopId != null) {
            return ArrayHelper::map(
                MasterSeat::find()->innerJoin(MasterStore::tableName(), 'mst_store.id = '.MasterSeat::tableName().'.store_id')
                                  ->andFilterWhere(['and', ['=', MasterSeat::tableName().'.store_id' , $shopId], ['=', 'booking_resources', $seat_flag], ['=','type_seat_id',$typeSeat],['=','smoking_flg',$noSmoking]])
                                  ->orderBy(['seat_code'=>SORT_ASC])->all(),
                'id', 'name');
        }
        return [];
    }
    
    public function checkDeleteBooking($id) {
        $check = 0;
        $check = MasterSeat::find()->join('INNER JOIN', 'booking_seat', 'mst_seat.id = booking_seat.seat_id')->andWhere(['mst_seat.id' => $id])->count();
        return ($check > 0) ? FALSE : TRUE;
    }
    
    // **************************************** //
    // Generate Store Code //   
    // **************************************** //    
    public function generateSeatCode($store_id) {
        $code = MasterSeat::find()->andFilterWhere(['mst_seat.del_flg' => '0', 'mst_seat.store_id' => $store_id])->max('seat_code');
        $code = (!isset($code) || empty($code)) ? 0 : $code;
        $code = (int) $code + 1;
        $count = strlen((string) $code);
        switch ($count) {
            case '1': $code = '00' . $code;
                break;
            case '2': $code = '0' . $code;
                break;
            case '3': break;
        }
        return $code;
    }
    
}
