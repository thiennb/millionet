<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii2tech\ar\softdelete\SoftDeleteBehavior;
use yii\behaviors\BlameableBehavior;
/**
 * This is the model class for table "staff_schedule".
 *
 * @property integer $id
 * @property integer $staff_id
 * @property string $schedule_date
 * @property string $work_flg
 * @property integer $shift_id
 * @property string $start_time
 * @property string $end_time
 * @property string $del_flg
 * @property integer $created_at
 * @property integer $created_by
 * @property integer $updated_at
 * @property integer $updated_by
 */
class StaffSchedule extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'staff_schedule';
    }
    
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
            BlameableBehavior::className(),
            'softDeleteBehavior' => [
                'class' => SoftDeleteBehavior::className(),
                'softDeleteAttributeValues' => [
                    'del_flg' => true
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['staff_id', 'shift_id', 'created_at', 'created_by', 'updated_at', 'updated_by'], 'integer'],
            [['schedule_date','created_at', 'created_by', 'updated_at', 'updated_by'], 'safe'],
            
            [['work_flg', 'del_flg'], 'string', 'max' => 1],
         
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend', 'ID'),
            'staff_id' => Yii::t('backend', 'Staff ID'),
            'schedule_date' => Yii::t('backend', 'Schedule Date'),
            'work_flg' => Yii::t('backend', 'Work Flg'),
            'shift_id' => Yii::t('backend', 'Shift ID'),
            'start_time' => Yii::t('backend', 'Start Time'),
            'end_time' => Yii::t('backend', 'End Time'),
            'del_flg' => Yii::t('backend', 'Del Flg'),
            'created_at' => Yii::t('backend', 'Created At'),
            'created_by' => Yii::t('backend', 'Created By'),
            'updated_at' => Yii::t('backend', 'Updated At'),
            'updated_by' => Yii::t('backend', 'Updated By'),
        ];
    }
    
    public static function find() {
        return parent::find()->where([StaffSchedule::tableName().'.del_flg' => 0]);
    }
    
    //get relate table staff_not_working
    public function getStaffNotWorking() {
        return $this->hasMany(StaffNotWorking::className(), ['staff_schedule_id' => 'id']);
      
    } 


    public static function getStaffSchedule($shift_id) {
 
        $query = StaffSchedule::find();
        $query->andWhere('shift_id = :shift_id', [':shift_id' => $shift_id]);
       
        return $query->all();
    }
    
}
