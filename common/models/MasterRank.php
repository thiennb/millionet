<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\behaviors\TimestampBehavior;
use yii2tech\ar\softdelete\SoftDeleteBehavior;
use yii\behaviors\BlameableBehavior;
use common\components\Util;

/**
 * This is the model class for table "mst_rank".
 *
 * @property integer $id
 * @property string $rank_apply_type
 * @property string $rank_condition
 * @property integer $days_reset_rank
 * @property string $bronze_point_rate
 * @property integer $bronze_time_visit
 * @property integer $bronze_point_total
 * @property string $bronze_use_money
 * @property string $sliver_point_rate
 * @property integer $sliver_time_visit
 * @property integer $sliver_point_total
 * @property string $sliver_use_money
 * @property string $gold_point_rate
 * @property integer $gold_time_visit
 * @property integer $gold_point_total
 * @property string $gold_use_money
 * @property string $black_point_rate
 * @property integer $black_time_visit
 * @property integer $black_point_total
 * @property string $black_use_money
 * @property string $del_flg
 * @property integer $created_at
 * @property integer $created_by
 * @property integer $updated_at
 * @property integer $updated_by
 */
class MasterRank extends \yii\db\ActiveRecord {

    public $check_rank_condition;
    public $company_code;

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'rank';
    }

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            TimestampBehavior::className(),
            BlameableBehavior::className(),
            'softDeleteBehavior' => [
                'class' => SoftDeleteBehavior::className(),
                'softDeleteAttributeValues' => [
                    'del_flg' => true
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
                [['black_status', 'gold_status', 'silver_status', 'bronze_status', 'check_rank_condition', 'company_code'], 'safe'],
                [['normal_point_rate', 'normal_time_visit', 'normal_point_total', 'normal_use_money'], 'default', 'value' => '0'],
                [['created_at', 'created_by', 'updated_at', 'updated_by'], 'safe'],
            //[['bronze_point_rate', 'normal_point_rate', 'sliver_point_rate', 'gold_point_rate', 'black_point_rate', 'bronze_use_money', 'sliver_use_money', 'gold_use_money', 'normal_use_money', 'normal_point_total', 'normal_time_visit',
            //'black_use_money', 'bronze_point_total', 'sliver_point_total', 'gold_point_total', 'black_point_total', 'normal_point_total', 'bronze_time_visit', 'sliver_time_visit', 'gold_time_visit', 'black_time_visit'], 'required'],
            [['bronze_time_visit', 'normal_time_visit', 'sliver_time_visit', 'gold_time_visit', 'black_time_visit', 'created_at', 'created_by', 'updated_at', 'updated_by'], 'integer'],
                [['bronze_point_rate', 'normal_point_rate', 'bronze_use_money', 'normal_use_money', 'sliver_point_rate', 'sliver_use_money', 'gold_point_rate', 'gold_use_money', 'black_point_rate', 'black_use_money'], 'number'],
                [['bronze_use_money', 'sliver_use_money', 'gold_use_money', 'black_use_money'], 'number', 'max' => 9999999999, 'min' => 0],
                [['bronze_use_money', 'sliver_use_money', 'gold_use_money', 'black_use_money'], 'string', 'max' => 13],
                [['bronze_point_rate', 'normal_point_rate', 'sliver_point_rate', 'gold_point_rate', 'black_point_rate'], 'number', 'max' => 9999.999, 'min' => 0],
                [['bronze_point_rate', 'normal_point_rate', 'sliver_point_rate', 'gold_point_rate', 'black_point_rate'], 'string', 'max' => 8],
                [['bronze_point_total', 'sliver_point_total', 'gold_point_total', 'black_point_total', 'normal_point_total'], 'number', 'max' => 9999999.999, 'min' => 0],
                [['bronze_point_total', 'sliver_point_total', 'gold_point_total', 'black_point_total', 'normal_point_total'], 'string', 'max' => 9],
                [['bronze_time_visit', 'sliver_time_visit', 'gold_time_visit', 'black_time_visit'], 'string', 'max' => 6],
                [['bronze_time_visit', 'sliver_time_visit', 'gold_time_visit', 'black_time_visit'], 'integer', 'max' => 99999, 'min' => 0],
                [['days_reset_rank'], 'integer', 'max' => 999, 'min' => 0],
                [['days_reset_rank'], 'string', 'max' => 3],
                [['rank_apply_type', 'rank_condition'], 'string', 'max' => 2],
                [['del_flg'], 'string', 'max' => 1],
                ['rank_condition', 'default', 'value' => '00'],
                [['rank_condition'], 'checkRankCondition'],
            //Validate Use Money
            //['bronze_use_money', 'compare', 'compareAttribute' => 'normal_use_money', 'operator' => '>', 'message' => \Yii::t('backend', 'This must be greater than normal rank.'), 'skipOnEmpty' => false],
            ['sliver_use_money', 'compare', 'compareAttribute' => 'bronze_use_money', 'operator' => '>', 'message' => \Yii::t('backend', 'This must be greater than bronze rank.'), 'skipOnEmpty' => false,
                'when' => function ($model, $attribute) {
                    return $model->bronze_use_money != null && $model->bronze_use_money > 0 && $model->silver_status == 1 && $model->bronze_status == 1;
                }
            ],
                ['gold_use_money', 'compare', 'compareAttribute' => 'sliver_use_money', 'operator' => '>', 'message' => \Yii::t('backend', 'This must be greater than silver rank.'), 'skipOnEmpty' => false,
                'when' => function ($model, $attribute) {
                    return $model->sliver_use_money != null && $model->sliver_use_money > 0 && $model->gold_status == 1 && $model->silver_status == 1;
                }
            ],
                ['black_use_money', 'compare', 'compareAttribute' => 'gold_use_money', 'operator' => '>', 'message' => \Yii::t('backend', 'This must be greater than gold rank.'), 'skipOnEmpty' => false,
                'when' => function ($model, $attribute) {
                    return $model->gold_use_money != null && $model->gold_use_money > 0 && $model->black_status == 1 && $model->gold_status == 1;
                }
            ],
            //Validate point total
            //['bronze_point_total', 'compare', 'compareAttribute' => 'normal_point_total', 'operator' => '>', 'message' => \Yii::t('backend', 'This must be greater than normal rank.'), 'skipOnEmpty' => false],
            ['sliver_point_total', 'compare', 'compareAttribute' => 'bronze_point_total', 'operator' => '>', 'message' => \Yii::t('backend', 'This must be greater than bronze rank.'), 'skipOnEmpty' => false,
                'when' => function ($model, $attribute) {
                    return $model->bronze_point_total != null && $model->bronze_point_total > 0 && $model->silver_status == 1 && $model->bronze_status == 1;
                }
            ],
                ['gold_point_total', 'compare', 'compareAttribute' => 'sliver_point_total', 'operator' => '>', 'message' => \Yii::t('backend', 'This must be greater than silver rank.'), 'skipOnEmpty' => false,
                'when' => function ($model, $attribute) {
                    return $model->sliver_point_total != null && $model->sliver_point_total > 0 && $model->gold_status == 1 && $model->silver_status == 1;
                }
            ],
                ['black_point_total', 'compare', 'compareAttribute' => 'gold_point_total', 'operator' => '>', 'message' => \Yii::t('backend', 'This must be greater than gold rank.'), 'skipOnEmpty' => false,
                'when' => function ($model, $attribute) {
                    return $model->gold_point_total != null && $model->gold_point_total > 0 && $model->black_status == 1 && $model->gold_status == 1;
                }
            ],
            //validate time vist
            //['bronze_time_visit', 'compare', 'compareAttribute' => 'normal_time_visit', 'operator' => '>', 'message' => \Yii::t('backend', 'This must be greater than normal rank.'), 'skipOnEmpty' => false],
            ['sliver_time_visit', 'compare', 'compareAttribute' => 'bronze_time_visit', 'operator' => '>', 'message' => \Yii::t('backend', 'This must be greater than bronze rank.'), 'skipOnEmpty' => false,
                'when' => function ($model, $attribute) {
                    return $model->bronze_time_visit != null && $model->bronze_time_visit > 0 && $model->silver_status == 1 && $model->bronze_status == 1;
                }
            ],
                ['gold_time_visit', 'compare', 'compareAttribute' => 'sliver_time_visit', 'operator' => '>', 'message' => \Yii::t('backend', 'This must be greater than silver rank.'), 'skipOnEmpty' => false,
                'when' => function ($model, $attribute) {
                    return $model->sliver_time_visit != null && $model->sliver_time_visit > 0 && $model->gold_status == 1 && $model->silver_status == 1;
                }
            ],
            ['black_time_visit', 'compare', 'compareAttribute' => 'gold_time_visit', 'operator' => '>', 'message' => \Yii::t('backend', 'This must be greater than gold rank.'), 'skipOnEmpty' => false,
                'when' => function ($model, $attribute) {
                    return $model->gold_time_visit != null && $model->gold_time_visit > 0 && $model->black_status == 1 && $model->gold_status == 1;
                }
            ],
            //=================================== 4 - 1
            ['black_time_visit', 'compare', 'compareAttribute' => 'bronze_time_visit', 'operator' => '>', 'message' => \Yii::t('backend', 'This must be greater than bronze rank.'), 'skipOnEmpty' => false,
                'when' => function ($model, $attribute) {
                    return $model->bronze_time_visit != null && $model->bronze_time_visit > 0 && $model->black_status == 1 && $model->bronze_status == 1 &&  $model->silver_status != 1 && $model->gold_status != 1;
                }
            ], 
            ['black_point_total', 'compare', 'compareAttribute' => 'bronze_point_total', 'operator' => '>', 'message' => \Yii::t('backend', 'This must be greater than bronze rank.'), 'skipOnEmpty' => false,
                'when' => function ($model, $attribute) {
                    return $model->bronze_point_total != null && $model->bronze_point_total > 0 && $model->black_status == 1 && $model->bronze_status == 1 &&  $model->silver_status != 1 && $model->gold_status != 1;
                }
            ],
            ['black_use_money', 'compare', 'compareAttribute' => 'bronze_use_money', 'operator' => '>', 'message' => \Yii::t('backend', 'This must be greater than bronze rank.'), 'skipOnEmpty' => false,
                'when' => function ($model, $attribute) {
                    return $model->bronze_use_money != null && $model->bronze_use_money > 0 && $model->black_status == 1 && $model->bronze_status == 1 &&  $model->silver_status != 1 && $model->gold_status != 1;
                }
            ],
            
            //================================= 4 - 2
            ['black_time_visit', 'compare', 'compareAttribute' => 'sliver_time_visit', 'operator' => '>', 'message' => \Yii::t('backend', 'This must be greater than silver rank.'), 'skipOnEmpty' => false,
                'when' => function ($model, $attribute) {
                    return $model->sliver_time_visit != null && $model->sliver_time_visit > 0 && $model->black_status == 1  &&  $model->silver_status == 1 && $model->gold_status != 1;
                }
            ],
            ['black_point_total', 'compare', 'compareAttribute' => 'sliver_point_total', 'operator' => '>', 'message' => \Yii::t('backend', 'This must be greater than silver rank.'), 'skipOnEmpty' => false,
                'when' => function ($model, $attribute) {
                    return $model->sliver_point_total != null && $model->sliver_point_total > 0 && $model->black_status == 1 &&  $model->silver_status == 1 && $model->gold_status != 1;
                }
            ],
            ['black_use_money', 'compare', 'compareAttribute' => 'sliver_use_money', 'operator' => '>', 'message' => \Yii::t('backend', 'This must be greater than silver rank.'), 'skipOnEmpty' => false,
                'when' => function ($model, $attribute) {
                    return $model->sliver_use_money != null && $model->sliver_use_money > 0 && $model->black_status == 1  &&  $model->silver_status == 1 && $model->gold_status != 1;
                }
            ],
            
            //================================= 3 - 1
            ['gold_time_visit', 'compare', 'compareAttribute' => 'bronze_time_visit', 'operator' => '>', 'message' => \Yii::t('backend', 'This must be greater than bronze rank.'), 'skipOnEmpty' => false,
                'when' => function ($model, $attribute) {
                    return $model->bronze_time_visit != null && $model->bronze_time_visit > 0  && $model->bronze_status == 1 &&  $model->silver_status != 1 && $model->gold_status == 1;
                }
            ],
            ['gold_point_total', 'compare', 'compareAttribute' => 'bronze_point_total', 'operator' => '>', 'message' => \Yii::t('backend', 'This must be greater than bronze rank.'), 'skipOnEmpty' => false,
                'when' => function ($model, $attribute) {
                    return $model->bronze_point_total != null && $model->bronze_point_total > 0  && $model->bronze_status == 1 &&  $model->silver_status != 1 && $model->gold_status == 1;
                }
            ],
            ['gold_use_money', 'compare', 'compareAttribute' => 'bronze_use_money', 'operator' => '>', 'message' => \Yii::t('backend', 'This must be greater than bronze rank.'), 'skipOnEmpty' => false,
                'when' => function ($model, $attribute) {
                    return $model->bronze_use_money != null && $model->bronze_use_money > 0  && $model->bronze_status == 1 &&  $model->silver_status != 1 && $model->gold_status == 1;
                }
            ],
            
            [['days_reset_rank'], 'required', 'when' => function() {
                    return $this->rank_apply_type === '00';
            }],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => Yii::t('backend', 'ID'),
            'rank_apply_time' => Yii::t('backend', 'Rank period'),
            'rank_condition' => Yii::t('backend', 'Rank up conditions'),
            'days_reset_rank' => Yii::t('backend', 'Date Reset Rank'),
            'bronze_point_rate' => Yii::t('backend', 'Rank point rate'),
            'bronze_time_visit' => Yii::t('backend', 'Rank time visit'),
            'bronze_point_total' => Yii::t('backend', 'Point total'),
            'bronze_use_money' => Yii::t('backend', 'Rank use money'),
            'sliver_point_rate' => Yii::t('backend', 'Rank point rate'),
            'sliver_time_visit' => Yii::t('backend', 'Rank time visit'),
            'sliver_point_total' => Yii::t('backend', 'Point total'),
            'sliver_use_money' => Yii::t('backend', 'Rank use money'),
            'gold_point_rate' => Yii::t('backend', 'Rank point rate'),
            'gold_time_visit' => Yii::t('backend', 'Rank time visit'),
            'gold_point_total' => Yii::t('backend', 'Point total'),
            'gold_use_money' => Yii::t('backend', 'Rank use money'),
            'black_point_rate' => Yii::t('backend', 'Rank point rate'),
            'black_time_visit' => Yii::t('backend', 'Rank time visit'),
            'black_point_total' => Yii::t('backend', 'Point total'),
            'black_use_money' => Yii::t('backend', 'Rank use money'),
            'del_flg' => Yii::t('backend', 'Del Flg'),
            'created_at' => Yii::t('backend', 'Created At'),
            'created_by' => Yii::t('backend', 'Created By'),
            'updated_at' => Yii::t('backend', 'Updated At'),
            'updated_by' => Yii::t('backend', 'Updated By'),
            'rankup_type' => Yii::t('backend', 'Rankup type'),
            'rank_apply_type' => Yii::t('backend', 'Rankup type'),
            'normal_point_rate' => Yii::t('backend', 'Rank point rate'),
            'normal_time_visit' => Yii::t('backend', 'Rank time visit'),
            'normal_point_total' => Yii::t('backend', 'Point total'),
            'normal_use_money' => Yii::t('backend', 'Rank use money'),
            'bronze_status' => Yii::t('backend', 'Bronze'),
            'silver_status' => Yii::t('backend', 'Silver'),
            'gold_status' => Yii::t('backend', 'Gold'),
            'black_status' => Yii::t('backend', 'Black'),
        ];
    }

    public function beforeSave($insert) {
        if (parent::beforeSave($insert)) {
            // Get Value masterrank-sliver_time_visit,masterrank-sliver_point_total,masterrank-sliver_use_money : position 2
            $sliver_time_visit = $this->sliver_time_visit;
            $sliver_point_total = $this->sliver_point_total;
            $sliver_use_money = $this->sliver_use_money;
            // ===============================================
            // ===============================================
            // Get Value masterrank-bronze_time_visit,masterrank-bronze_point_total,masterrank-bronze_use_money : position 1
            $bronze_time_visit = $this->bronze_time_visit;
            $bronze_point_total = $this->bronze_point_total;
            $bronze_use_money = $this->bronze_use_money;

            // Get Value masterrank-gold_time_visit,masterrank-gold_point_total,masterrank-gold_use_money : position 3
            $gold_time_visit = $this->gold_time_visit;
            $gold_point_total = $this->gold_point_total;
            $gold_use_money = $this->gold_use_money;
            // ===============================================
            // Get Value masterrank-black_time_visit,masterrank-black_point_total,masterrank-black_use_money : position 4
            $black_time_visit = $this->black_time_visit;
            $black_point_total = $this->black_point_total;
            $black_use_money = $this->black_use_money;
            // ===============================================
            if ($sliver_time_visit == null) {
                $this->sliver_time_visit = 0;
            }
            if ($sliver_point_total == null) {
                $this->sliver_point_total = 0;
            }
            if ($sliver_use_money == null) {
                $this->sliver_use_money = 0;
            }

            if ($bronze_time_visit == null) {
                $this->bronze_time_visit = 0;
            }
            if ($bronze_point_total == null) {
                $this->bronze_point_total = 0;
            }
            if ($bronze_use_money == null) {
                $this->bronze_use_money = 0;
            }

            if ($gold_time_visit == null) {
                $this->gold_time_visit = 0;
            }
            if ($gold_point_total == null) {
                $this->gold_point_total = 0;
            }
            if ($gold_use_money == null) {
                $this->gold_use_money = 0;
            }

            if ($black_time_visit == null) {
                $this->black_time_visit = 0;
            }
            if ($black_point_total == null) {
                $this->black_point_total = 0;
            }
            if ($black_use_money == null) {
                $this->black_use_money = 0;
            }
            return true;
        } else {
            return false;
        }
    }

    public function findRankbyCompanyId($companyId = null) {
        if ($companyId != null) {
            $rank = (new \yii\db\Query())
                    ->select(['*'])
                    ->from(["r" => MasterRank::tableName()])
//                    ->innerJoin(['m' => 'company'], "m.id = r.company_id")
                    ->where(['r.company_id' => $companyId]);

            return $rank->one();
        }

        return null;
    }

    public static function find() {
        $company_id = Util::getCookiesCompanyId();
        return parent::find()->innerJoin('company', 'company.id = rank.company_id')->andFilterWhere(['rank.del_flg' => '0', 'company.id' => $company_id]);
    }

    // checkRankCondition
    public function checkRankCondition($attribute, $params) {

        $radio_rank_condition = $this->rank_condition;

        if ($radio_rank_condition == '00') {
            if ($this->checkValueRankCondition($radio_rank_condition) == false) {
                $this->addError('check_rank_condition', Yii::t('backend', "In case of all achievement, please set all conditions."));
            }
        }
        if ($radio_rank_condition == '01') {
            if ($this->checkValueRankCondition($radio_rank_condition) == false) {
                $this->addError('check_rank_condition', Yii::t('backend', "In case of achieving more than two, please set only two conditions."));
            }
        }
        if ($radio_rank_condition == '02') {
            if ($this->checkValueRankCondition($radio_rank_condition) == false) {
                $this->addError('check_rank_condition', Yii::t('backend', "If you achieve one or more, please set only one condition."));
            }
        }
    }

    public function checkValueRankCondition($radio_rank_condition) {
        // Get Value masterrank-sliver_time_visit,masterrank-sliver_point_total,masterrank-sliver_use_money : position 2
        $sliver_time_visit = $this->sliver_time_visit;
        $sliver_point_total = $this->sliver_point_total;
        $sliver_use_money = $this->sliver_use_money;
        // ===============================================
        // ===============================================
        // Get Value masterrank-bronze_time_visit,masterrank-bronze_point_total,masterrank-bronze_use_money : position 1
        $bronze_time_visit = $this->bronze_time_visit;
        $bronze_point_total = $this->bronze_point_total;
        $bronze_use_money = $this->bronze_use_money;

        // Get Value masterrank-gold_time_visit,masterrank-gold_point_total,masterrank-gold_use_money : position 3
        $gold_time_visit = $this->gold_time_visit;
        $gold_point_total = $this->gold_point_total;
        $gold_use_money = $this->gold_use_money;
        // ===============================================
        // Get Value masterrank-black_time_visit,masterrank-black_point_total,masterrank-black_use_money : position 4
        $black_time_visit = $this->black_time_visit;
        $black_point_total = $this->black_point_total;
        $black_use_money = $this->black_use_money;
        // ===============================================

        if ($radio_rank_condition == '00') {
            if ($this->bronze_status == 1) {
                if (!$this->checkNumberCanNull(0, $bronze_time_visit, $bronze_point_total, $bronze_use_money)) {
                    return false;
                }
            }
            if ($this->silver_status == 1) {
                if (!$this->checkNumberCanNull(0, $sliver_time_visit, $sliver_point_total, $sliver_use_money)) {
                    return false;
                }
            }
            if ($this->gold_status == 1) {
                if (!$this->checkNumberCanNull(0, $gold_time_visit, $gold_point_total, $gold_use_money)) {
                    return false;
                }
            }
            if ($this->black_status == 1) {
                if (!$this->checkNumberCanNull(0, $black_time_visit, $black_point_total, $black_use_money)) {
                    return false;
                }
            }
        }

        if ($radio_rank_condition == '01') {
            if ($this->bronze_status == 1) {
                if (!$this->checkNumberCanNull(1, $bronze_time_visit, $bronze_point_total, $bronze_use_money)) {
                    return false;
                }
            }
            if ($this->silver_status == 1) {
                if (!$this->checkNumberCanNull(1, $sliver_time_visit, $sliver_point_total, $sliver_use_money)) {
                    return false;
                }
            }
            if ($this->gold_status == 1) {
                if (!$this->checkNumberCanNull(1, $gold_time_visit, $gold_point_total, $gold_use_money)) {
                    return false;
                }
            }
            if ($this->black_status == 1) {
                if (!$this->checkNumberCanNull(1, $black_time_visit, $black_point_total, $black_use_money)) {
                    return false;
                }
            }
        }

        if ($radio_rank_condition == '02') {
             if ($this->bronze_status == 1) {
                if (!$this->checkNumberCanNull(2, $bronze_time_visit, $bronze_point_total, $bronze_use_money)) {
                    return false;
                }
            }
            if ($this->silver_status == 1) {
                if (!$this->checkNumberCanNull(2, $sliver_time_visit, $sliver_point_total, $sliver_use_money)) {
                    return false;
                }
            }
            if ($this->gold_status == 1) {
                if (!$this->checkNumberCanNull(2, $gold_time_visit, $gold_point_total, $gold_use_money)) {
                    return false;
                }
            }
            if ($this->black_status == 1) {
                if (!$this->checkNumberCanNull(2, $black_time_visit, $black_point_total, $black_use_money)) {
                    return false;
                }
            }
        }
        return true;
    }

    public function checkNumberCanNull($numberCanNull, $param1, $param2, $param3) {
        $arrayParam = func_get_args();
        $numberNull = 0;
        $arrayCover = array_shift($arrayParam);
        foreach ($arrayParam as $key => $value) {
            if ($value == null || $value == 0) {
                $numberNull++;
            }
        }
        return $numberCanNull == $numberNull;
    }

}
