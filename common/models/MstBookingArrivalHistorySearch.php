<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\MstBookingArrivalHistory;

/**
 * MstBookingArrivalHistorySearch represents the model behind the search form about `common\models\MstBookingArrivalHistory`.
 */
class MstBookingArrivalHistorySearch extends MstBookingArrivalHistory
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'coupon_id', 'customer_id', 'shop_id', 'staff_id', 'seat_id', 'menu_coupon_id', 'status', 'action', 'created_at', 'updated_at', 'black_list'], 'integer'],
            [['memo', 'memo2', 'memo3', 'fax', 'memo1', 'start_time', 'end_time', 'booking_date', 'demand', 'del_flg'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MstBookingArrivalHistory::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'coupon_id' => $this->coupon_id,
            'customer_id' => $this->customer_id,
            'shop_id' => $this->shop_id,
            'staff_id' => $this->staff_id,
            'seat_id' => $this->seat_id,
            'menu_coupon_id' => $this->menu_coupon_id,
            'status' => $this->status,
            'action' => $this->action,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'booking_date' => $this->booking_date,
            'black_list' => $this->black_list,
        ]);

        $query->andFilterWhere(['like', 'memo', $this->memo])
            ->andFilterWhere(['like', 'memo2', $this->memo2])
            ->andFilterWhere(['like', 'memo3', $this->memo3])
            ->andFilterWhere(['like', 'fax', $this->fax])
            ->andFilterWhere(['like', 'memo1', $this->memo1])
            ->andFilterWhere(['like', 'start_time', $this->start_time])
            ->andFilterWhere(['like', 'end_time', $this->end_time])
            ->andFilterWhere(['like', 'demand', $this->demand])
            ->andFilterWhere(['like', 'del_flg', $this->del_flg]);

        return $dataProvider;
    }
}
