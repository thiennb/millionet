<?php

namespace common\models;

use Yii;
use Carbon\Carbon;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "booking".
 *
 * @property integer $id
 * @property string $booking_code
 * @property integer $customer_id
 * @property integer $store_id
 * @property string $booking_date
 * @property string $memo
 * @property string $memo_manager
 * @property integer $staff_id
 * @property integer $seat_id
 * @property string $start_time
 * @property string $end_time
 * @property string $status
 * @property string $action
 * @property string $demand
 * @property integer $number_of_people
 * @property string $booking_price
 * @property string $booking_discount
 * @property string $booking_total
 * @property string $to_home_delivery_flg
 * @property string $del_flg
 * @property integer $created_at
 * @property integer $created_by
 * @property integer $updated_at
 * @property integer $updated_by
 */
class FrontFormBookingOrder extends \yii\base\Model
{
	public $isFirstTime;
	public $demand;
	public $storeId;
	public $couponId;
	public $date;
	public $staff;
	public $points;
	public $products;
	public $customerId;
	public $executeTime;
	public $options;

	//static $action = 'detail';

    /**
     * @inheritdoc
     */
    public function rules()
    {
		return [
			[['storeId', 'customerId', 'date', 'staff', 'isFirstTime'], 'required'],
			[['storeId', 'date', 'staff'], 'integer'],
			[['storeId'], 'validateAll'],
			//['points', 'validatePoints'],
			//['demand', 'string', 'max' => 200],
			//['is_first_time', 'boolean']
		];
    }

	/**
     * Validates the company_code
     * This method serves as the inline validation for company code.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
	public function validatePoints ($attribute, $params) {

		$point = CustomerStore::find()->where([
			'customer_id' => Yii::$app->user->id,
			'store_id' => Yii::$app->request->get('storeId')
		])->one();

		$total_point = $point ? (int) $point->total_point : 0;
		$points = (int) $this->points;

		if ($points > $total_point) {
			$this->addError($attribute, Yii::t('frontend', 'Invalid points used'));
		}

		return false;
	}

	public function validateAll ($attribute, $params) {
		// check coupons and products format
		if ((!preg_match('/^\d+(,\d+){0,}$/', $this->couponId)) || ($this->products && !preg_match('/^\d+(,\d+){0,}$/', $this->products))) {
			$this->addError($attribute, Yii::t('frontend', 'Invalid coupon(s) or product(s)'));
		}

		$total_time = 0;

		$coupons = BookingSearch::searchCouponsById($this->couponId, $this->storeId);
		$products = BookingSearch::searchProductsById($this->products, $this->storeId);

		if ((!empty($this->couponId) && !count($coupons))
		 	|| (!empty($this->products) && !count($products))) {
			$this->addError($attribute, Yii::t('frontend', 'Invalid coupon(s) or product(s)'));
		}
		else {
			$total_time += array_sum(ArrayHelper::getColumn($coupons, 'time_require'));
			$total_time += array_sum(ArrayHelper::getColumn($products, 'time_require'));
		}

		$number_row = 60 * $total_time / 1800;

		$execute_time = $number_row * 1800 / 60;

		$hours = floor($execute_time / 60);
		$minutes = $execute_time % 60;

		if ($hours < 10) {
			$hours = '0' . $hours;
		}

		if ($minutes < 10) {
			$minutes = '0' . $minutes;
		}

		$execute_time = sprintf('%s:%s:00', (string) $hours, (string) $minutes);

		$date = Carbon::createFromTimestamp($this->date);

		$store = MasterStore::find()->where(['id' => $this->storeId])->one();

		if (!$store) {
			return false;
		}

		$staff = BookingSearch::searchStaffByDateTime($this->storeId, $number_row, $execute_time, $date->format('Y-m-d'), $date->format('Y-m-d'), $store->reservations_possible_time , $date->format('H:i:00'));

		$found_staff = false;

		foreach ($staff as $value) {
			if ($value['id'] == $this->staff) {
				$found_staff = true;
			}
		}

		if (!$found_staff) {
			$this->addError($attribute, Yii::t('frontend', 'Invalid staff or schedule requested'));
		}

		return true;
	}

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'booking_code' => 'Booking Code',
            'customer_id' => 'Customer ID',
            'store_id' => 'Store ID',
            'booking_date' => 'Booking Date',
            'memo' => 'Memo',
            'memo_manager' => 'Memo Manager',
            'staff_id' => 'Staff ID',
            'seat_id' => 'Seat ID',
            'start_time' => 'Start Time',
            'end_time' => 'End Time',
            'status' => 'Status',
            'action' => 'Action',
            'demand' => 'Demand',
            'number_of_people' => 'Number Of People',
            'booking_price' => 'Booking Price',
            'booking_discount' => 'Booking Discount',
            'booking_total' => 'Booking Total',
            'to_home_delivery_flg' => 'To Home Delivery Flg',
            'del_flg' => 'Del Flg',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
        ];
    }
}
