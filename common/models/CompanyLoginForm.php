<?php

namespace common\models;

use Yii;
use yii\base\Model; use common\models\MasterStaff;

/**
 * Login form with company code
 */
class CompanyLoginForm extends Model {

    public $company_code;
    public $management_id;
    public $password;
    public $rememberMe = true;
    private $_user;

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            // username and password are both required
            [['management_id', 'password', 'company_code'], 'required'],
            [['password', 'company_code'], 'string', 'max' => 20],
            [['management_id'], 'string', 'max' => 5],
            // rememberMe must be a boolean value
            ['rememberMe', 'boolean'],
            // company_code existence is checked
            ['company_code', 'validateCompanyCode'],
            // password is validated by validatePassword()
            ['management_id', 'validatePassword']
        ];
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validatePassword($attribute, $params) {
        /*
          if (!$this->hasErrors()) {
          return false;
          } */

        $user = $this->getUser();
        if (!$user || !$user->validatePassword($this->password)) {
            $this->addError($attribute, Yii::t('backend', 'Incorrect username or password'));
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'company_code' => Yii::t('backend', 'Company Code'),
            'management_id' => Yii::t('backend', 'Admin Login ID'),
            'password' => Yii::t('backend', 'Password'),
            'rememberMe' => Yii::t('backend', 'Save Login Information')
        ];
    }

    /**
     * @inheritdoc
     */
    public function login() {
        // validate company code, user name and password
        if (/* $this->checkCompanyCode() && */ $this->validate()) {
            return Yii::$app->user->login($this->getUser(), $this->rememberMe ? 3600 * 24 * 30 : 0);
        } else {
            return false;
        }
    }

    /**
     * Validates the company_code
     * This method serves as the inline validation for company code.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validateCompanyCode($attribute, $params) {
        /* if ($this->hasErrors()){
          return false;
          } */

        if (!Company::find()->where(['company_code' => $this->company_code])->exists()) {
            $this->addError($attribute, Yii::t('backend', 'Company code doesn\'t exist'));
            return false;
        } else {
            return true;
        }
    }

    /**
     * Finds user by [[username]]
     *
     * @return User|null
     */
    protected function getUser() {
        if ($this->_user === null) {
            $this->_user = MasterStaff::findByManagementId($this->management_id);
        }

        return $this->_user;
    }

}
