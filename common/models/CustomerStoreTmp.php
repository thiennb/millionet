<?php

namespace common\models;

use yii\behaviors\TimestampBehavior;
use yii2tech\ar\softdelete\SoftDeleteBehavior;
use yii\behaviors\BlameableBehavior;
use Yii;
use yii\web\NotFoundHttpException;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;
use common\components\RegisterFromBehavior;
/**
 * This is the model class for table "customer_store".
 *
 * @property integer $id
 * @property integer $customer_id
 * @property integer $store_id
 * @property string $total_point
 * @property string $total_amount
 * @property string $rank_id
 * @property integer $last_staff_id
 * @property integer $number_visit
 * @property integer $number_booking
 * @property string $last_visit_date
 * @property string $first_visit_date
 * @property integer $tickets_balance
 * @property string $charge_balance
 * @property string $news_transmision_flg
 * @property string $black_list_flg
 * @property string $del_flg
 * @property integer $created_at
 * @property integer $created_by
 * @property integer $updated_at
 * @property integer $updated_by
  * @property string $register_kbn 
 */
class CustomerStoreTmp extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public $name;
    public $website;
    public $time_open;
    public $time_close;
    public $regular_holiday;
    public $tel;
    public $image1;
    public $introduce;
    public $store_code;
    public $process_date;
    public $max_date;
    public $max_time;
    public static function tableName() {
        return 'customer_store_tmp';
    }

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            TimestampBehavior::className(),
            BlameableBehavior::className(),
            'softDeleteBehavior' => [
                'class' => SoftDeleteBehavior::className(),
                'softDeleteAttributeValues' => [
                    'del_flg' => true
                ],
            ],
             RegisterFromBehavior::className()
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['customer_id', 'last_staff_id', 'number_visit', 'number_booking', 'tickets_balance', 'created_at', 'created_by', 'updated_at', 'updated_by'], 'integer'],
            [['total_point', 'total_amount', 'charge_balance'], 'number'],
            [['store_id', 'last_visit_date', 'first_visit_date','register_kbn'], 'safe'],
            [['rank_id'], 'string', 'max' => 2],
            [['rank_id'], 'string', 'max' => 2],
            [['news_transmision_flg', 'black_list_flg', 'del_flg'], 'string', 'max' => 1],
            [['store_id'], 'required', 'except' => 'giftinsert'],
                //[['store_id'], 'checkChosseStoreId'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => Yii::t('frontend', 'ID'),
            'customer_id' => Yii::t('frontend', 'Customer ID'),
            'store_id' => Yii::t('backend', 'Register store'),
            'total_point' => Yii::t('frontend', 'Total Point'),
            'total_amount' => Yii::t('frontend', 'Total Amount'),
            'rank_id' => Yii::t('frontend', 'Rank ID'),
            'last_staff_id' => Yii::t('frontend', 'Last Staff ID'),
            'number_visit' => Yii::t('frontend', 'Number Visit'),
            'number_booking' => Yii::t('frontend', 'Number Booking'),
            'last_visit_date' => Yii::t('frontend', 'Last Visit Date'),
            'first_visit_date' => Yii::t('frontend', 'First Visit Date'),
            'tickets_balance' => Yii::t('frontend', 'Tickets Balance'),
            'charge_balance' => Yii::t('frontend', 'Charge Balance'),
            'news_transmision_flg' => Yii::t('backend', 'News Transmision Flg'),
            'black_list_flg' => Yii::t('backend', 'Black List Flg'),
            'del_flg' => Yii::t('frontend', 'Del Flg'),
            'created_at' => Yii::t('frontend', 'Created At'),
            'created_by' => Yii::t('frontend', 'Created By'),
            'updated_at' => Yii::t('frontend', 'Updated At'),
            'updated_by' => Yii::t('frontend', 'Updated By'),
        ];
    }

    // Select Information Customer Store
    public function findModelCustomerStore($id) {

        if (($model = CustomerStore::findOne($id)) !== null) {
            return parent::find()->andWhere(['customer_store_tmp.customer_id' => $id]);
        } else {
            throw new \yii\web\NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    // Select Information Customer Store
    public function findModel($id) {

        if (($model = CustomerStore::findOne($id)) !== null) {
            return $model;
        } else {
            throw new \yii\web\NotFoundHttpException('The requested page does not exist.');
        }
    }

    // Select store
    public function findModelIdStore($id) {

        $customer_store = self::find()
                ->andWhere(['customer_store_tmp.customer_id' => $id])
                ->select(['customer_store_tmp.id'])
                ->all();
        return $customer_store;
    }

    // Select customer_id from store
    public function findModelGetCustomerID($id) {

        if (($model = CustomerStore::findOne($id)) !== null) {
            return $model;
        } else {
            throw new \yii\web\NotFoundHttpException('The requested page does not exist.');
        }
    }

    public static function find() {
        return parent::find()->where(['or', ['customer_store_tmp.del_flg' => '0'], ['customer_store_tmp.del_flg' => null]]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomer() {
        return $this->hasOne(MasterCustomer::className(), ['id' => 'customer_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStore() {
        return $this->hasOne(MasterStore::className(), ['id' => 'store_id']);
    }

    public function getIdStore() {
        return $this->id;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getChargeHistory() {
        return $this->hasMany(ChargeHistory::className(), ['customer_id' => 'customer_id']);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getChargeHistoryNoUse() {
        return $this->hasMany(ChargeHistory::className(), ['customer_id' => 'customer_id'])
                ->where(['process_type' => '0'])
                ->orderBy('process_date DESC, process_time DESC');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPointHistory() {
        return $this->hasMany(PointHistory::className(), ['customer_id' => 'customer_id'])
                ->innerJoin('mst_store', 'mst_store.id = '.$this->store_id)
                ->where('point_history.store_code = mst_store.store_code')
                ->orderBy('point_history.created_at DESC');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder() {
        return $this->hasMany(MstOrder::className(), ['customer_id' => 'customer_id']);
    }

     public function getMasterStore() {
        return $this->hasOne(MasterStore::className(), ['id' => 'store_id']);
    }
    /*
    * Author vuong_vt
    * @param (int) customer_id
    */
    public function searchStoreHistory($customerId = null ,$company_id = null)
    {  
        $query = MasterStore::find();
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 3
            ],
            'sort' => false
        ]);
        $query->joinWith("booking", true, "INNER JOIN");
        $query->andFilterWhere(['=', Booking::tableName() . '.customer_id', $customerId]);
        $query->andFilterWhere(['=', MasterStore::tableName() . '.company_id', $company_id]);
        $query->groupBy("mst_store.id");
        $query->orderBy(['mst_store.created_at' => SORT_DESC]);
        return $dataProvider ;
    }
    
    public function getRankInfo($customerId = null){
        if($customerId != null){
            $query = CustomerStore::find()->leftJoin('mst_store', 'mst_store.id = customer_store.store_id');
            $query->select([
                'customer_store_tmp.id',
                'customer_store_tmp.customer_id',
                'customer_store_tmp.store_id',
                'customer_store_tmp.total_point',
                'customer_store_tmp.number_visit',
                'customer_store_tmp.total_amount',
                'customer_store_tmp.rank_id',
                'customer_store_tmp.updated_at',
                'mst_store.name',
                'mst_store.website',
                'mst_store.time_open',
                'mst_store.time_close',
                'mst_store.regular_holiday',
                'mst_store.tel',
                'mst_store.image1',
                'mst_store.introduce',
            ]);
            $query->groupBy([
                'customer_store_tmp.id',
                'customer_store_tmp.customer_id',
                'customer_store_tmp.store_id',
                'customer_store_tmp.total_point',
                'customer_store_tmp.number_visit',
                'customer_store_tmp.total_amount',
                'customer_store_tmp.rank_id',
                'customer_store_tmp.updated_at',
                'mst_store.name',
                'mst_store.website',
                'mst_store.time_open',
                'mst_store.time_close',
                'mst_store.regular_holiday',
                'mst_store.tel',
                'mst_store.image1',
                'mst_store.introduce',
            ]);
            $query->andFilterWhere(['=', CustomerStore::tableName() . '.customer_id', $customerId]);
            $query->andFilterWhere(['=', CustomerStore::tableName() . '.del_flg', 0]); 
            $query->orderBy('customer_store.updated_at DESC');
            
            return $query;
        }
        
        return null;
    }
    
    /**
     * Insert customer store
     * When CustomerStore is not found, create fast data
     * 
     * @param int/string store id
     * @param customer id
     * @return CustomerStore object 
     */
    public static function addCustomerStore($store_id, $customer_id){
        $model = new CustomerStore();
        $model->store_id = $store_id;
        $model->customer_id = $customer_id;
        $model->rank_id = '00';
        $model->news_transmision_flg = '0';
        $model->save();
        return $model;
    }

    public function searchTop ($customer_id)
    {
        $query = CustomerStore::find();
        // add conditions that should always apply here
        $query->select([
                'customer_store_tmp.id',
                'mst_store.id as store_id',
                'mst_store.name',
                'mst_store.image1',
                'mst_store.company_id',
                'customer_store_tmp.rank_id',
                'customer_store_tmp.total_point',
                'customer_store_tmp.number_visit',
                'customer_store_tmp.total_amount'

        ])->distinct();
        $query->innerJoin(MasterStore::tableName(), CustomerStoreTmp::tableName() . '.store_id = '. MasterStore::tableName() . '.id');  
        $query->innerJoin(MasterRank::tableName(), CustomerStoreTmp::tableName() . '.rank_id = '. MasterRank::tableName() . '.rank_condition');  
        $query->andFilterWhere(['=', CustomerStoreTmp::tableName() . '.customer_id', $customer_id]);
        return $query;
    }

    public function getNotice($customer_id)
    {
        $query = NoticeCustomer::find();
        $query->select([
                'notice_customer.id',
                'notice_customer.customer_id',
                'mst_notice.send_date',
                'mst_notice.send_time',
                'mst_notice.title',
                'mst_notice.content',

        ])->distinct();
        $query->innerJoin(MasterNotice::tableName(), MasterNotice::tableName() . '.id = '. NoticeCustomer::tableName() . '.notice_id'); 
        $query->innerJoin(CustomerStore::tableName(), CustomerStoreTmp::tableName() . '.customer_id = '. NoticeCustomer::tableName() . '.customer_id');   
        $query->andFilterWhere(['=', NoticeCustomer::tableName() . '.customer_id', $customer_id]);
        $query->andFilterWhere(['=', NoticeCustomer::tableName() . '.del_flg', 0]);
        $query->andFilterWhere(['=', CustomerStoreTmp::tableName() . '.news_transmision_flg', 1]);
        $query->orderBy([
            'mst_notice.send_date' => SORT_DESC,
            'mst_notice.send_time' => SORT_DESC,
        ]);    
        return $query;
    }

    public function getBooking($customer_id = null) {
        $query = Booking::find();
        $query->innerJoin(MasterStore::tableName(), Booking::tableName() . '.store_id=' . MasterStore::tableName() . '.id'
                . ' and ' . MasterStore::tableName() . '.del_flg = :zero');
        $query->addParams([
            'zero' => '0'
        ]);
        $query->orderBy([
            'booking_date' => SORT_DESC,
            'start_time' => SORT_DESC
        ]);
        if (isset($customer_id)) {
            $query->andFilterWhere(['=', 'customer_id', $customer_id]);
        }       
        $query->groupBy('booking.id');
        return $query;
     } 
     
     public function getCustomerStoreDetail($customer_id = null, $store_id = null){
        if($customer_id !== null && $store_id !== null){
            $query = $detailOrder = (new \yii\db\Query())->select(['*'])->from('customer_store_tmp');
            $query->where(['customer_store_tmp.customer_id' => $customer_id]);
            $query->andWhere(['customer_store_tmp.store_id' => $store_id]);
            $query->andWhere(['customer_store_tmp.del_flg' => 0]);
            return $query->one();
        }
        
        return null;
     }

}
