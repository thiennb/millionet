<?php

namespace common\models;

use Yii;

class ApiFormBookingSeat extends \yii\base\Model {

    public $capacity;
    public $seat = '';
    //=== unused
    public $storeId;
    public $products;
    public $coupons;
    public $options;
    public $noOption;
    public $executeTime;
    public $date;

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['storeId', 'products', 'coupons', 'options', 'noOption', 'executeTime', 'capacity', 'seat', 'date'], 'safe'],
            ['capacity', 'required'],
            ['capacity', 'integer', 'min' => 1],
            ['capacity', 'validateSeat']
        ];
    }

    public function validateSeat($attribute, $params = []) {
        if ($this->date) {
            /* @var $type_seat \common\models\MasterTypeSeat */
            if ($this->seat) {
                $type_seats = MasterTypeSeat::findFrontEnd()
                        ->where(['id' => $this->seat])
                        ->all();
            } else {
                $type_seats = MasterTypeSeat::findFrontEnd()
                        ->where(['store_id' => $this->storeId])
                        ->all();
            }

            $date = \Carbon\Carbon::createFromTimestamp($this->date);
            $ymd = $date->format('Y-m-d');
            $hi = $date->format('H:i');
            $date_time = $ymd . ' ' . $hi . ':00';
            $found_seat = false;
            foreach ($type_seats as $type_seat) {
                $shift = $type_seat->getDetailedSchedule([
                    'from_date' => $ymd,
                    'to_date' => $ymd,
                    'from_time' => $hi,
                    'to_time' => $hi
                ]);

                if (isset($shift[$date_time])) {
                    $capacity_min = $shift[$date_time]['capacity_min'];
                    $capacity_max = $shift[$date_time]['capacity_max'];
                } else {
                    $capacity_min = $type_seat['capacity_min'];
                    $capacity_max = $type_seat['capacity_max'];
                }

                if ($this->capacity >= $capacity_min && $this->capacity <= $capacity_max) {
                    $found_seat = true;
                }
            }

            if ($found_seat === false) {
                $this->addError($attribute, '座席数が不足しています');
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'capacity' => '人数選択'
        ];
    }

}
