<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use yii2tech\ar\softdelete\SoftDeleteBehavior;
use common\components\Util;

/**
 * This is the model class for table "company".
 *
 * @property integer $id
 * @property string $company_code
 * @property string $name
 * @property string $name_kana
 * @property string $tel
 * @property string $fax
 * @property string $email
 * @property string $home_page
 * @property string $post_code
 * @property string $address
 * @property string $del_flg
 * @property integer $created_at
 * @property integer $created_by
 * @property integer $updated_at
 * @property integer $updated_by
 * @property string $app_header_background_color 
 * @property string $app_content_background_color 
 * @property string $app_footer_background_color 
 * @property string $app_header_character_color 
 * @property string $app_content_character_color 
 * @property string $app_footer_character_color 
 */
class Init extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public $company_code;
    public $name_company;
    public $name_store;
    public $name_staff;
    public $short_name_staff;
    public $pass_word;

    public static function tableName() {
        return 'init';
    }

    public function behaviors() {
        return [
            TimestampBehavior::className(),
            BlameableBehavior::className(),
            'softDeleteBehavior' => [
                'class' => SoftDeleteBehavior::className(),
                'softDeleteAttributeValues' => [
                    'del_flg' => true
                ],
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['company_code', 'name_company', 'name_store', 'name_staff', 'short_name_staff', 'pass_word'], 'required'],
            [['company_code'], 'unique',
                'targetClass' => 'common\models\Company',
                'when' => function ($model, $attribute) {
            return $model->{$attribute} !== $model->getOldAttribute($attribute);
        },
            ],
            [['company_code', 'name_company', 'name_store', 'name_staff', 'short_name_staff', 'pass_word'], 'safe'],
        ];
    }
     /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
                'company_code' => Yii::t('backend', '全店舗'),
            ];
    }

    public static function find() {
        $command = Yii::$app->getDb()->createCommand('	SELECT
                                                        company.company_code,
                                                        company.name  as company_name,
                                                        mst_store.name as store_name,
                                                        management_login.management_id as management_id
                                                        FROM 
                                                        company
                                                        INNER JOIN mst_store
                                                        ON
                                                        company.id = mst_store.company_id
                                                        INNER JOIN mst_staff
                                                        ON
                                                        mst_staff.store_id = mst_store.id
                                                        INNER JOIN management_login
                                                        ON
                                                        management_login.id = mst_staff.management_login_id
                                                        GROUP BY
                                                        company.company_code,company.name,mst_store.name,management_login.management_id'
        );

        return $result = $command->queryAll();
    }

}
