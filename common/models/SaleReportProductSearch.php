<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\MasterSettingAutoSendNotice;

class SaleReportProductSearch extends MstOrderDetail {

    /**
     * @inheritdoc
     */
    public $store_id, $start_date, $end_date, $category_id;

    public function rules() {
        return [
            [['start_date', 'end_date', 'category_id', 'store_id'], 'safe'],
            [['start_date', 'end_date'], 'date', 'format' => 'php:Y/m/d'],
//            [['booking_id'], 'required'],
//            [['booking_id', 'created_at', 'created_by', 'updated_at', 'updated_by'], 'integer'],
//            [['point_use', 'change_use', 'discount_percent', 'discount_value', 'discount_coupon', 'detail_money', 'subtotal', 'total', 'tax', 'point_add', 'money_credit', 'money_ticket', 'money_other', 'money_receive', 'money_refund'], 'number'],
//            [['order_code'], 'string', 'max' => 10],
//            [['process_time'], 'string', 'max' => 5],
//            [['del_flg'], 'string', 'max' => 1],
        ];
    }

    public function search($params) {
        $query = MstOrderDetail::find();
        $query->groupBy(['jan_code']);

        $order_detail = MstOrderDetail::find();

        $query->joinWith('order', true, 'INNER JOIN');
        $order_detail->joinWith('order', true, 'INNER JOIN');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => false
        ]);
        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            $query->where('0=1');
            return $dataProvider;
        }
        if (!empty($this->store_id)) {
            $store_code = MasterStore::findOne($this->store_id);
            $query->andFilterWhere(['=', "SUBSTRING(" . MstOrderDetail::tableName() . ".order_code, 1, 5)", $store_code->store_code]);
            $order_detail->andFilterWhere(['=', "SUBSTRING(" . MstOrderDetail::tableName() . ".order_code, 1, 5)", $store_code->store_code]);
        }
        $query->andFilterWhere(['>=', MstOrder::tableName() . '.process_date', $this->start_date]);
        $query->andFilterWhere(['<=', MstOrder::tableName() . '.process_date', $this->end_date]);
        $order_detail->andFilterWhere(['>=', MstOrder::tableName() . '.process_date', $this->start_date]);
        $order_detail->andFilterWhere(['<=', MstOrder::tableName() . '.process_date', $this->end_date]);



        $total_price = $order_detail->sum('price*quantity');
        $total_profit = $order_detail->sum('product_total*quantity');
        $total_quantity = $order_detail->sum('quantity');
        $total_price = empty($total_price) ? '0' : $total_price;
        $total_profit = empty($total_profit) ? '0' : $total_profit;
        $total_quantity =empty($total_quantity) ? '0' : $total_quantity;
        $query->select([
            'SUM(price*quantity) as revenue',
            "(" . $total_price . ") as total_revenue",
            'SUM(product_total*quantity) as profit',
            "(" . $total_profit . ") as total_profit",
            'SUM(quantity) as quantitys',
            "(" . $total_quantity . ") as total_quantitys",
            'jan_code'
        ]);

        return $dataProvider;
    }

}
