<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\StoreShift;

/**
 * StoreShiftSearch represents the model behind the search form about `common\models\StoreShift`.
 */
class StoreShiftSearch extends StoreShift
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
//    public function search($params)
//    {
//        $query = StoreShift::find();
//        
//        // add conditions that should always apply here
//
//        $dataProvider = new ActiveDataProvider([
//            'query' => $query,
//            'pagination' => false
//        ]);
//
//        $this->load($params);
//
////        if (!$this->validate()) {
////            // uncomment the following line if you do not want to return any records when validation fails
////            return $dataProvider;
////        }
//        // grid filtering conditions
//        
//        $query->andFilterWhere([
//            'store_id' => $this->store_id,
//        ]);
//
//        return $dataProvider;
//    }
}
