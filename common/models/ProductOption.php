<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use yii2tech\ar\softdelete\SoftDeleteBehavior;

/**
 * This is the model class for table "mst_product_option".
 *
 * @property integer $id
 * @property integer $option_id
 * @property integer $product_id
 * @property string $del_flg
 * @property integer $created_at
 * @property integer $created_by
 * @property integer $updated_at
 * @property integer $updated_by
 */
class ProductOption extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product_option';
    }
    
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),            
            BlameableBehavior::className(),
            'softDeleteBehavior' => [
                'class' => SoftDeleteBehavior::className(),
                'softDeleteAttributeValues' => [
                    'del_flg' => '1'
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['option_id', 'product_id'], 'required'],
            [['option_id', 'product_id', 'created_at', 'created_by', 'updated_at', 'updated_by'], 'integer'],
            [['del_flg'], 'string', 'max' => 1],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend', 'ID'),
            'option_id' => Yii::t('backend', 'Option ID'),
            'product_id' => Yii::t('backend', 'Product ID'),
            'del_flg' => Yii::t('backend', 'Del Flg'),
            'created_at' => Yii::t('backend', 'Created At'),
            'created_by' => Yii::t('backend', 'Created By'),
            'updated_at' => Yii::t('backend', 'Updated At'),
            'updated_by' => Yii::t('backend', 'Updated By'),
        ];
    }
    
    /**
     * @get product by option
     */
    public function getProduct()
    {
        return $this->hasOne(MstProduct::className(), ['id' => 'product_id']);
    }
            
    public static function find()
    {
        return parent::find()->where(['product_option.del_flg'=>'0']);
    }
    
     /**
     * @return \yii\db\ActiveQuery
     */
    public function getOption()
    {
        return $this->hasOne(MasterOption::className(), ['id' => 'option_id']);
    }
   
}