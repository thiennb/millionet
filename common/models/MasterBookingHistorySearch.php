<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\MasterBookingHistory;

/**
 * MasterBookingHistorySearch represents the model behind the search form about `common\models\MasterBookingHistory`.
 */
class MasterBookingHistorySearch extends MasterBookingHistory {

    /**
     * @inheritdoc
     */
    //product_category
    public $list_category, $product_name;
    //table: mst_customer
    public $birth_date_from, $birth_date_to;
    public $customer_first_name, $customer_last_name, $sex, $rank, $customer_jan_code, $mobile;
    //table: customer_store
    public $number_visit_min, $number_visit_max;
    public $last_visit_date_from, $last_visit_date_to;
    //table: booking
    public $booking_time_min, $booking_time_max;
    //table:
    public $last_seat_id, $last_staff_id;
    public $seat_type, $service_staff_id;

    public function rules() {
        return [
            [['id', 'customer_id', 'booking_time_max', 'booking_time_min', 'last_seat_id', 'seat_type', 'service_staff_id', 'shop_id', 'store_id', 'staff_id', 'seat_id', 'menu_coupon_id', 'status', 'action', 'created_at', 'updated_at', 'black_list'], 'integer'],
            [['memo', 'list_category', 'product_name', 'memo2', 'memo3', 'fax', 'memo1', 'start_time', 'end_time', 'booking_date', 'demand', 'del_flg'], 'safe'],
            [['booking_date_from', 'booking_date_to', 'birth_date_to', 'birth_date_from', 'last_visit_date_from', 'last_visit_date_to'], 'date', 'format' => 'php:Y/m/d'],
            [['customer_first_name', 'customer_last_name', 'sex', 'rank', 'mobile', 'customer_jan_code', 'last_staff_id'], 'safe'],
            [['product_name'], 'string', 'max' => 100],
            [['customer_jan_code'], 'string', 'max' => 20],
            [['mobile'], 'string', 'max' => 11],
            [['mobile'], 'integer', 'min' => 0],
            [['customer_last_name', 'customer_first_name'], 'string', 'max' => 40],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public $shop_id, $menu_coupon_id, $black_list, $memo2, $memo3, $fax, $memo1;



}
