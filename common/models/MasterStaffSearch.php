<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\MasterStaff;
use yii\helpers\ArrayHelper;
use common\components\Util;
use common\components\Constants;
/**
 * MasterStaffSearch represents the model behind the search form about `common\models\MasterStaff`.
 */
class MasterStaffSearch extends MasterStaff
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['sex', 'permission_id','show_flg'], 'string','max'=>1],
            [['name'],'string', 'max'=>80],
            [['email'],'string', 'max'=>60],
            [['memo'],'string', 'max'=>100],            
            [['management_login_id','store_id'],'integer'],
            [['management_login_id'],'string','max'=>5],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MasterStaff::findByStore()->innerJoin('management_login','management_login.id = mst_staff.management_login_id')
                                    ->innerJoin('mst_store','mst_store.id = mst_staff.store_id')
                                    // Join : Company store
                                    ->innerJoin('company', 'company.id = mst_store.company_id');
        $company_id =  Util::getCookiesCompanyId();
        $query->andFilterWhere([
            'company.id' => $company_id,
        ]);
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' =>false
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        //HoangNQ   Fix Bug
//        if(is_null($this->show_flg))
//            $this->show_flg = '0';
        //HoangNQ Fix Bug    
        // grid filtering conditions
        $query->andFilterWhere([
            'mst_staff.sex' => $this->sex,
            'mst_staff.permission_id' => $this->permission_id,
            'mst_staff.show_flg' => $this->show_flg,
            'mst_staff.store_id' => $this->store_id,
        ]);
        
        // Get Role
        
        //        $role = \common\components\FindPermission::getRole();
        //        
        //        if ($role->permission_id == Constants::COMPANY_MANAGER_ROLE){
        //           $query->where("mst_staff.permission_id IN ('1','2','3')");
        //        } else if ($role->permission_id == Constants::STORE_MANAGER_ROLE || $role->permission_id == Constants::STAFF_ROLE){
        //           $query->where("mst_staff.permission_id IN ('2','3')");
        //        }
        
        $query->andFilterWhere(['like', 'mst_staff.name', $this->name])
              ->andFilterWhere(['like', 'mst_staff.email', $this->email])
              ->andFilterWhere(['like', 'management_login.management_id', $this->management_login_id])
              ->andFilterWhere([
                'or',
                ['like', 'mst_staff.item_option_1', $this->memo],
                ['like', 'mst_staff.item_option_2', $this->memo],
                ['like', 'mst_staff.item_option_3', $this->memo],
                ['like', 'mst_staff.item_option_4', $this->memo],
                ['like', 'mst_staff.item_option_5', $this->memo],
            ]);
        
        $query->orderBy('mst_staff.name ASC');

        return $dataProvider;
    }
    
    // Datpdt 19/09/2016 start, edit by HoangNQ 20161101
    public function searchStaffByCustomer(){
       
        $query = (new \yii\db\Query())
                ->select(['mst_staff.id','mst_staff.name'])
                ->from('mst_staff')
                 //->join('INNER JOIN','customer_store','customer_store.last_staff_id = mst_staff.id')
                ->join('INNER JOIN','customer_store','customer_store.store_id = mst_staff.store_id')
                ->join('INNER JOIN','mst_customer','mst_customer.id = customer_store.customer_id')
                ->andWhere(['company_id' => Util::getCookiesCompanyId()])
                ->all();
       // $query->orderBy('mst_staff.name ASC');
        
        return ArrayHelper::map($query, 'id', 'name');
    }
    // Datpdt 19/09/2016 end
}
