<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\helpers\ArrayHelper;
use yii2tech\ar\softdelete\SoftDeleteBehavior;
use yii\behaviors\BlameableBehavior;
use common\components\Constants;
use common\components\Util;

/**
 * This is the model class for table "mst_categories_product".
 *
 * @property integer $id
 * @property string $name
 * @property integer $created_at
 * @property integer $updated_at
 * @property boolean $del_flg
 *
 * @property MstProduct[] $mstProducts
 */
class MstCategoriesProduct extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'mst_product_category';
    }
    
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
            BlameableBehavior::className(),
            'softDeleteBehavior' => [
                'class' => SoftDeleteBehavior::className(),
                'softDeleteAttributeValues' => [
                    'del_flg' => '1'
                ],
            ],
        ];
    }
    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name','company_id'], 'required'],
            [['name'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend', 'ID'),
            'name' => Yii::t('backend', 'Name category product'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMstProducts()
    {
        return $this->hasMany(MstProduct::className(), ['category_id' => 'id']);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public static function listCategory()
    {
        $company_id =  Util::getCookiesCompanyId();
        return ArrayHelper::map(self::find()->andFilterWhere(['mst_product_category.del_flg'=>'0','company.id'=>$company_id])->all(), 'id', 'name');
    }
    
    public static function find()
    {
        $company_id =  Util::getCookiesCompanyId();
        return parent::find()->innerJoin('company', 'company.id = mst_product_category.company_id')->andFilterWhere(['mst_product_category.del_flg'=>'0','company.id'=>$company_id]);
    }
    
    public static function findFrontend()
    {
        return parent::find(['or', [self::tableName() . '.del_flg' => '0'], [self::tableName() . '.del_flg' => null]]);
    }
    
    public function  checkDelete($id){
        $check = 0;
        $check = MstProduct::find()->where(['category_id'=>$id])->count();
        return ($check>0)?FALSE:TRUE;
    }
    // ===============================================
    // Select List Category By StoreID
    // ===============================================
     public static function listCategoryByStoreId($id_store)
    {
        return ArrayHelper::map(self::find()->innerJoin('mst_product', 'mst_product.category_id = mst_product_category.id')
                                            ->where(['mst_product.store_id'=>$id_store])
                                            ->all(), 'id', 'name');
    }
    
    public function beforeDelete()
   {
        parent::beforeDelete();
        $role = \common\components\FindPermission::getRole();
        if ($role->permission_id == Constants::STORE_MANAGER_ROLE || $role->permission_id == Constants::STAFF_ROLE) {
            return false;
        }
        return true;        
   }
   
   public function beforeSave($insert)
   {
        parent::beforeSave($insert);
        $role = \common\components\FindPermission::getRole();
        if ($role->permission_id == Constants::STORE_MANAGER_ROLE || $role->permission_id == Constants::STAFF_ROLE) {
            return false;
        }
        return true;
   }
   
   public function findCategory($storeId = null){
       if($storeId !== null){
           $subquery = (new \yii\db\Query())
               ->select(['distinct(id)'])
               ->from('mst_product')->where(['store_id'=>$storeId]);
                      
           $query = (new \yii\db\Query())
                   ->select(['distinct(mst_product_category.id)'])
                   ->from('mst_product')->innerJoin('mst_product_category', 'mst_product.category_id = mst_product_category.id')
                   ->where(['in', 'mst_product.id', $subquery]);
           
           $categories = MstCategoriesProduct::find()->where(['in', 'mst_product_category.id', $query])->andWhere(['mst_product_category.del_flg'=>'0'])->all();
            return $categories;
       }
       return null;
   }
    
}
