<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\MstProduct;
use common\components\Util;
use yii\db\Query;

/**
 * MstProductSearch represents the model behind the search form about `common\models\MstProduct`.
 */
class MstProductSearch extends MstProduct {

    /**
     * @inheritdoc
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['name'], 'string', 'max' => 80],
            [['show_flg', 'category_id', 'store_id'], 'safe'],
        ];
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = MstProduct::find()->innerJoin('mst_tax', 'mst_product.tax_rate_id = mst_tax.id')
                ->innerJoin('mst_product_category', 'mst_product.category_id = mst_product_category.id')
                ->innerJoin('mst_store', 'mst_store.id = mst_product.store_id')
                // Join : Company store
                ->innerJoin('company', 'company.id = mst_store.company_id');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => false
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        $company_id = Util::getCookiesCompanyId();
        // grid filtering conditions
        $query->andFilterWhere([
            'mst_product.category_id' => $this->category_id,
            'mst_product.show_flg' => $this->show_flg,
            'mst_product.store_id' => $this->store_id,
            'company.id' => $company_id,
        ]);
        $query->select([
            'mst_product.name',
            'mst_product_category.name as category_name',
            'mst_product.unit_price',
            'mst_product.id',
            'mst_tax.name as rate',
            'mst_store.name as name_store']);

        $query->andFilterWhere(['like', 'mst_product.name', $this->name]);
        $query->orderBy('mst_product.name ASC');
        $query->groupBy([
            'mst_product.name',
            'mst_product_category.name',
            'mst_product.unit_price',
            'mst_product.id',
            'mst_tax.name',
            'mst_store.name']);

        return $dataProvider;
    }

    /**
     * Get product in list id
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function searchProduct($params) {
        $query = MstProduct::find()->leftJoin('mst_tax', 'mst_product.tax_rate_id = mst_tax.id');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => false
        ]);
        if (empty($params)) {
            $params = -1;
        } else {
            $params = explode(",", $params);
            foreach ($params AS $index => $value)
                $params[$index] = (int) $value;
        }
        $query->andFilterWhere(['mst_product.id' => $params]);

        $query->select([
            'mst_product.name',
            'mst_product.unit_price',
            'mst_tax.name as rate',
            'mst_product.tax_display_method',
            'mst_product.time_require',
            'mst_product.id']);

        $query->orderBy('mst_product.name ASC');

        return $dataProvider;
    }

    /**
     * Get UnitPrice of Product in list id
     *
     * @param array $params
     *
     * @return Array List UnitPrice Product
     */
    public function searchUnitPriceByListIdProduct($params) {

        $command = (new Query())
                ->select(['SUM(mst_product.unit_price)'])
                ->from('mst_product')
                ->where(['or', ['mst_product.del_flg' => '0'], ['mst_product.del_flg' => null]]);


        if (empty($params)) {
            $params = '0';
            $command->andWhere(['mst_product.id' => $params]);
        } else {
            $params = explode(",", $params);
            foreach ($params AS $index => $value) {
                $params[$index] = (int) $value;
            }
            $command->where(['mst_product.id' => $params]);
        }

        $result = $command->all();

        return $result;
    }

}
