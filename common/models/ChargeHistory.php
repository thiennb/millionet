<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use yii2tech\ar\softdelete\SoftDeleteBehavior;

/**
 * This is the model class for table "charge_history".
 *
 * @property integer $id
 * @property integer $customer_id
 * @property string $process_date
 * @property string $process_time
 * @property string $process_type
 * @property string $input_money
 * @property string $process_money
 * @property string $charge_balance
 * @property string $del_flg
 * @property integer $created_at
 * @property integer $created_by
 * @property integer $updated_at
 * @property integer $updated_by
 */
class ChargeHistory extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public $register_staff_id;
//    public $charge_balance;
    public $name ;
    public static function tableName()
    {
        return 'charge_history';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['customer_id'], 'required'],
            [['customer_id', 'created_at', 'created_by', 'updated_at', 'updated_by'], 'integer'],
            [['process_date', 'register_staff_id'], 'safe'],
            [['input_money', 'process_money', 'charge_balance'], 'number'],
            [['process_time'], 'string', 'max' => 5],
            [['process_type', 'del_flg'], 'string', 'max' => 1],
            [['input_money', 'process_money', 'charge_balance'], 'number',  'max'=>  9999999999, 'min' => 0],
        ];
    }
    
    public function behaviors() {
        return [
            TimestampBehavior::className(),
            BlameableBehavior::className(),
            'softDeleteBehavior' => [
                'class' => SoftDeleteBehavior::className(),
                'softDeleteAttributeValues' => [
                    'del_flg' => '1'
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'customer_id' => Yii::t('app', 'Customer ID'),
            'process_date' => Yii::t('backend', 'Process Date'),
            'process_time' => Yii::t('app', 'Process Time'),
            'process_type' => Yii::t('app', 'Process Type'),
            'input_money' => Yii::t('app', 'Input Money'),
            'process_money' => Yii::t('app', 'Process Money'),
            'charge_balance' => Yii::t('app', 'Charge Balance'),
            'del_flg' => Yii::t('app', 'Del Flg'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'register_store_id' => Yii::t('backend', 'Store Id'),
            'register_staff_id' => Yii::t('backend', 'Register staff'),
            'customer_jan_code' => Yii::t('backend', 'Code Membership'),
            'name' => Yii::t('backend', 'Name Customer'),
            'sex' => Yii::t('backend', 'Sex'),
            'rank' => Yii::t('backend', 'Customers rank'),
            'number_visit_min' => Yii::t('backend', 'Visit number'),
            'number_visit_max' => Yii::t('backend', 'Visit number'),
            'birth_date_from' => Yii::t("backend", "Birthday"),
            'birth_date_to' => Yii::t("backend", "Birthday"),
            'history_date_from' => Yii::t('backend', 'Process Date'),
            'history_date_to' => Yii::t('backend', 'Process Date'),
            'charge_amount_from' => Yii::t('backend','Charge amount'),
            'charge_amount_to' => Yii::t('backend','Charge amount'),
            
            
        ];
    }
    public function getCustomerMaster()
    {   
      return $this->hasOne(MasterCustomer::className(), ['id' => 'customer_id']);
    }
    
    public function getManagement()
    {   
      return $this->hasOne(ManagementLogin::className(), ['management_id' => 'staff_management_id']);
    }
    
    public function getStore()
    {   
      return $this->hasOne(MasterStore::className(), ['store_code' => 'store_code']);
    }
    
    public static function find()
    {
        return parent::find()->where((new \common\components\FindPermission)->getPermissionCondition(self::tableName(), false, true));
    }
    
    public static function findFrontEnd () {
        return parent::find()->where(['or', [self::tableName() . '.del_flg' => '0'], [self::tableName() . '.del_flg' => null]]);
    }
    
}
