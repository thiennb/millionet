<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\MasterNotice;
use common\models\NoticeCustomer;
use common\models\MasterStaff;
use common\components\Util;

/**
 * MasterNoticeSearch represents the model behind the search form about `common\models\MasterNotice`.
 */
class MasterNoticeSearch extends MasterNotice {

    public $store_id;
    public $deliver_status;
    public $start_send_date;
    public $end_send_date;

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['store_id', 'deliver_status'], 'integer'],
            [['start_send_date', 'end_send_date'], 'date', 'format' => 'php:Y/m/d'],
            //['end_send_date', 'compare', 'compareAttribute' => 'start_send_date', 'operator' => '>=', 'message' => Yii::t('backend', "End date must great than Start date")],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = NoticeCustomer::find();
        $query->joinWith('customer', true, 'INNER JOIN');
        $query->joinWith('notice', true, 'INNER JOIN');
        $query->joinWith('store', true, 'INNER JOIN');
//        $query->innerJoin('company_store', 'company_store.store_id = mst_store.id');

        //->innerJoin('mst_notice', 'mst_notice.id = notice_customer.notice_id')
        //->leftJoin('mst_customer', 'mst_customer.id = notice_customer.customer_id')
        //->leftJoin('mst_store', 'mst_store.id = notice_customer.store_id');
        $query->andFilterWhere(['=', 'mst_notice.auto_push_flg', MasterNotice::PUSH_BY_HAND]);
//        $company_code =  Util::getCookiesCompanyCode();
        $query->andFilterWhere(['=', 'mst_customer.company_id',  Util::getCookiesCompanyId()]);
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => false
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
             $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'notice_customer.store_id' => $this->store_id,
        ]);

        
        if(!empty($this->start_send_date)){
            $query->andFilterWhere(['>=', 'notice_customer.push_date', strtotime($this->start_send_date)]);
        }
        if(!empty($this->end_send_date)){
            $query->andFilterWhere(['<', 'notice_customer.push_date', strtotime($this->end_send_date. "+1 day")]);
        }

        $query->select([
//            'mst_notice.send_date as send_date',
//            'mst_notice.send_time',
            'mst_store.name as store_name',
            'mst_customer.first_name as first_name',
            'mst_customer.last_name as last_name',
            'mst_notice.title as title',
            'notice_customer.id',
            'notice_customer.push_date',
        ]);
        $query->orderBy('mst_notice.created_at DESC');
        return $dataProvider;
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function searchListAutoSend($params) {
        $query = NoticeCustomer::find();
        $query->joinWith('customer', true, 'INNER JOIN');
        $query->joinWith('notice', true, 'INNER JOIN');
        $query->joinWith('store', true, 'INNER JOIN');
//        $query->innerJoin('company_store', 'company_store.store_id = mst_store.id');
        $query->andFilterWhere(['=', 'mst_notice.auto_push_flg', MasterNotice::PUSH_AUTOMATIC]);
//        $company_code =  Util::getCookiesCompanyCode();
        $query->andFilterWhere(['=', 'mst_customer.company_id',  Util::getCookiesCompanyId()]);
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => false
        ]);
        $this->load($params);
        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
             $query->where('0=1');
            return $dataProvider;
        }
        // grid filtering conditions
        $query->andFilterWhere([
            'notice_customer.store_id' => $this->store_id,
        ]);
        if(!empty($this->start_send_date)){
            $query->andFilterWhere(['>=', 'notice_customer.push_date', strtotime($this->start_send_date)]);
        }
        if(!empty($this->end_send_date)){
            $query->andFilterWhere(['<', 'notice_customer.push_date', strtotime($this->end_send_date. "+1 day")]);
        }

        $query->select([
//            'mst_notice.send_date as send_date',
//            'mst_notice.send_time',
            'mst_store.name as store_name',
            'notice_customer.status_send',
            'mst_customer.first_name',
            'mst_customer.last_name',
            'mst_notice.title as title',
            'notice_customer.id',
            'notice_customer.push_date',
        ]);
//        $query->groupBy([
//            'mst_notice.send_date',
//            'mst_notice.send_time',
//            'mst_store.name',
//            'mst_customer.first_name',
//            'mst_customer.last_name',
//            'mst_notice.title',
//            'notice_customer.id',
//            'notice_customer.status_send',
//            'notice_customer.push_date',
//        ]);
        $query->orderBy('mst_notice.send_date DESC');
        return $dataProvider;
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function searchListHistory($params) {
        $query = NoticeCustomer::find();
        $query->joinWith('customer', true, 'INNER JOIN');
        $query->joinWith('notice', true, 'INNER JOIN');
        $query->joinWith('store', true, 'LEFT JOIN');
        $query->joinWith('customerStore', true, 'INNER JOIN');
//        $query->innerJoin('company_store', 'company_store.store_id = mst_store.id');
//        $company_code =  Util::getCookiesCompanyCode();
        $query->andFilterWhere(['=', 'mst_customer.company_id',  Util::getCookiesCompanyId()]);
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => false
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
             $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'notice_customer.store_id' => $this->store_id,
        ]);
        // grid filtering conditions
        $query->andFilterWhere([
            'notice_customer.status_send' => $this->deliver_status,
        ]);

        
        if(!empty($this->start_send_date)){
            $query->andFilterWhere(['>=', 'notice_customer.push_date', strtotime($this->start_send_date)]);
        }
        if(!empty($this->end_send_date)){
            $query->andFilterWhere(['<', 'notice_customer.push_date', strtotime($this->end_send_date. "+1 day")]);
        }


        $query->select([
//            'mst_notice.send_date as send_date',
//            'mst_notice.send_time',
            'mst_store.name as store_name',
            'mst_customer.first_name as first_name',
            'mst_customer.last_name as last_name',
            'mst_notice.title as title',
            'notice_customer.id',
            'notice_customer.status_send',
            'notice_customer.push_date',
        ]);
        $query->orderBy('notice_customer.push_date, mst_notice.title DESC');
        $query->groupBy([
//            'mst_notice.send_date',
//            'mst_notice.send_time',
            'mst_store.name',
            'mst_customer.first_name',
            'mst_customer.last_name',
            'mst_notice.title',
            'notice_customer.id',
            'notice_customer.status_send',
            'notice_customer.push_date',
        ]);
        return $dataProvider;
    }

    /**
     * Find info of customer and notice
     *
     * @param integer $id
     *
     * @return ActiveDataProvider
     */
    public function searchDetailDeliverHistory($id) {
//        $company_code =  Util::getCookiesCompanyCode();
        $model = NoticeCustomer::find()
                ->join('INNER JOIN', 'mst_notice', "mst_notice.id = notice_customer.notice_id AND mst_notice.del_flg <> '1'")
                ->join('INNER JOIN', 'mst_customer', "mst_customer.id = notice_customer.customer_id AND mst_customer.del_flg <> '1'")
                ->join('LEFT JOIN', 'customer_store', "mst_customer.id = customer_store.customer_id AND customer_store.store_id = notice_customer.store_id AND mst_customer.del_flg <> '1'")
//                ->join('LEFT JOIN', 'mst_staff', "mst_staff.id = customer_store.last_staff_id AND mst_staff.del_flg <> '1'")
                ->join('LEFT JOIN', 'mst_store', "mst_store.id = notice_customer.store_id AND mst_store.del_flg <> '1'")
//                ->innerJoin('company_store', 'company_store.store_id = notice_customer.store_id')
                ->andWhere(['=', 'mst_customer.company_id',  Util::getCookiesCompanyId()])
                ->select([
                    'mst_notice.send_date',
                    'mst_notice.title',
                    'mst_notice.content',
                    'mst_store.name as store_name',
                    'mst_customer.customer_jan_code',
                    'mst_customer.first_name',
                    'mst_customer.last_name',
                    'mst_customer.sex',
                    'mst_customer.birth_date',
                    'customer_store.last_visit_date',
//                    'mst_staff.name as last_staff_name',
                    'customer_store.number_visit',
                    'customer_store.rank_id as rank',
                    'notice_customer.customer_id',
                ])
                ->andFilterWhere(['notice_customer.id' => $id])
                ->one();
          
//        $last_staff_name = MasterStaff::find()->join('INNER JOIN', 'booking', 'booking.staff_id = mst_staff.id')
//                ->where(['booking.customer_id' => $model->customer_id])
//                ->orderBy('booking.updated_at DESC')
//                ->select('mst_staff.name')
//                ->one();
        
        $model->last_staff_name = $this->getLastStaffName($model->customer_id);
        return $model;
    }

    /**
     * Find info of customer and notice
     *
     * @param integer $id
     *
     * @return ActiveDataProvider
     */
    public function getLastStaff($customer_jan_code) {
//        $company_code =  Util::getCookiesCompanyCode();
            $model = \common\models\MstOrderDetail::find()
                ->innerJoin('mst_order', "mst_order.order_code = order_detail.order_code")
//                ->innerJoin('company_store', 'company_store.store_id = notice_customer.store_id')
                ->andWhere(['mst_order.customer_jan_code' =>  $customer_jan_code])
                ->orderBy(["SUBSTRING(mst_order.order_code,6,11)" => SORT_DESC])
                ->select([
                    'order_detail.product_management_id',
                ])
                ->one();
        if(empty($model)){
            return '';
        }else{
            $staff = \common\models\MasterStaff::find()->innerJoin('management_login','mst_staff.management_login_id = management_login.id')->andWhere(['management_id' => $model->product_management_id])->one();
            return empty($staff)?'':$staff->name;
        }
    }

    /**
     * Find info of customer and notice
     *
     * @param integer $id
     *
     * @return ActiveDataProvider
     */
    public function getLastStaffName($customer_id) {
        $customer = MasterCustomer::findOne($customer_id);
        if(empty($customer)) return '';
//        $company_code =  Util::getCookiesCompanyCode();
        $model = \common\models\MasterStaff::find()
                ->innerJoin('management_login', "management_login.id = mst_staff.management_login_id")
                ->innerJoin('mst_order', "mst_order.order_management_id = management_login.management_id")
//                ->innerJoin('company_store', 'company_store.store_id = notice_customer.store_id')
                ->andWhere(['mst_order.customer_jan_code' =>  $customer->customer_jan_code])
                ->orderBy('mst_order.process_date DESC, mst_order.process_time DESC')
                ->one();
        if(empty($model)){
            return '';
        }else{
            return $model->name;
        }
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function searchDetailDeliverSchedule($notice_id = null) {
//        $company_code =  Util::getCookiesCompanyCode();
        $query = NoticeCustomer::find()
                ->leftJoin('mst_customer', 'mst_customer.id = notice_customer.customer_id')
                ->leftJoin('mst_notice', 'mst_notice.id = notice_customer.notice_id')
                ->leftJoin('customer_store', 'customer_store.store_id = notice_customer.store_id and customer_store.customer_id = notice_customer.customer_id')
                ->andWhere(['notice_customer.notice_id' => $notice_id])
//                ->innerJoin('company_store', 'company_store.store_id = notice_customer.store_id')
                ->andWhere(['=', 'mst_customer.company_id',  Util::getCookiesCompanyId()])
                ->distinct();
        // add conditions that should always apply here

        $query->select([
            'mst_customer.first_name',
            'mst_customer.last_name',
            'mst_customer.sex',
            'customer_store.last_visit_date',
            'customer_store.number_visit',
            'customer_store.rank_id as rank',
            'notice_customer.id',
        ]);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => false
        ]);

        return $dataProvider;
    }

}
