<?php

namespace common\models;

/**
 * This is the ActiveQuery class for [[MasterBooking]].
 *
 * @see Booking
 */
class MstBookingQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return Booking[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Booking|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
