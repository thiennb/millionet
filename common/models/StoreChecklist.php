<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use yii2tech\ar\softdelete\SoftDeleteBehavior;

/**
 * This is the model class for table "store_checklist".
 *
 * @property integer $id
 * @property integer $store_id
 * @property integer $checklist_id
 * @property string $del_flg
 * @property integer $created_at
 * @property integer $created_by
 * @property integer $updated_at
 * @property integer $updated_by
 */
class StoreChecklist extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'store_checklist';
    }

      /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),            
            BlameableBehavior::className(),
            'softDeleteBehavior' => [
                'class' => SoftDeleteBehavior::className(),
                'softDeleteAttributeValues' => [
                    'del_flg' => '1'
                ],
            ],
        ];
    }
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            //[['store_id', 'created_at', 'created_by', 'updated_at', 'updated_by'], 'required'],
            [['store_id', 'checklist_id', 'created_at', 'created_by', 'updated_at', 'updated_by'], 'integer'],
            [['del_flg'], 'string', 'max' => 1],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('frontend', 'ID'),
            'store_id' => Yii::t('frontend', 'Store ID'),
            'checklist_id' => Yii::t('frontend', 'Checklist ID'),
            'del_flg' => Yii::t('frontend', 'Del Flg'),
            'created_at' => Yii::t('frontend', 'Created At'),
            'created_by' => Yii::t('frontend', 'Created By'),
            'updated_at' => Yii::t('frontend', 'Updated At'),
            'updated_by' => Yii::t('frontend', 'Updated By'),
        ];
    }
}
