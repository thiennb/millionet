<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\MasterTax;
use common\components\Util;
use common\models\Company;
/**
 * MasterTaxSearch represents the model behind the search form about `common\models\MasterTax`.
 */
class MasterTaxSearch extends MasterTax {

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = MasterTax::find()->innerJoin('tax_detail', 'mst_tax.id = tax_detail.tax_id')
                ->innerJoin('company', 'company.id = mst_tax.company_id');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => false
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $now_date = date('Y/m/d');

        $query->andFilterWhere([
            'mst_tax.del_flg' => '0',
            'tax_detail.del_flg' => '0',
        ]);
        $query->andFilterWhere(['>=', 'tax_detail.end_date', $now_date]);
        $query->andFilterWhere(['<=', 'tax_detail.start_date', $now_date]);
        // Get company_code from session
        
        $query->andFilterWhere(['=', 'mst_tax.company_id', Util::getCookiesCompanyId()]);
        $query->select([
            'tax_detail.id as detail_id',
            'mst_tax.name',
            'tax_detail.rate',
            'tax_detail.start_date as start_date',
            'tax_detail.end_date as end_date'
        ]);

        $query->orderBy('mst_tax.updated_at DESC');
        $query->groupBy([
            'tax_detail.id',
            'mst_tax.name',
            'tax_detail.rate',
            'tax_detail.start_date',
            'tax_detail.end_date',
            'mst_tax.updated_at'
        ]);

        return $dataProvider;
    }

}
