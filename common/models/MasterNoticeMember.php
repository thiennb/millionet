<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii2tech\ar\softdelete\SoftDeleteBehavior;

/**
 * This is the model class for table "mst_notice".
 * * ------------------------
 * @property string $member_code;
 * @property string $first_name;
 * @property string $last_name;
 * @property string $first_name_kana;
 * @property string $last_name_kana;
 * @property string $sex;
 * @property string $start_birth_date;
 * @property string $end_birth_date;
 * @property string $start_visit_date;
 * @property string $end_visit_date;
 * @property integer $max_visit_number;
 * @property integer $min_visit_number;
 * @property integer $rank_id;
 * @property integer $last_staff;
 * @property integer $black_list;
 * @property integer $shop_id;
 */
class MasterNoticeMember extends \yii\db\ActiveRecord
{
    public $customer_jan_code;
    public $first_name;
    public $last_name;
    public $first_name_kana;
    public $last_name_kana;
    public $sex;
    public $start_birth_date;
    public $end_birth_date;
    public $start_visit_date;
    public $end_visit_date;
    public $max_visit_number;
    public $min_visit_number;
    public $rank_id;
    public $last_staff;
    public $black_list = true;
    public $store_id;
    
    public $data;
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'mst_notice';
    }
    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type_id', 'object_id', 'title', 'created_at', 'updated_at'], 'required'],
            [['object_id', 'created_at', 'updated_at', 'hours_start', 'minute_start', 'hours_end', 'minute_end'], 'integer'],
            [['note'], 'string'],
            [['del_flg'], 'boolean'],
            [['start_date', 'end_date','data','store_id','rank_id'], 'safe'],
            [['type_id'], 'string', 'max' => 3],
            [['title'], 'string', 'max' => 100],
            [['min_visit_number', 'max_visit_number'], 'string', 'max' => 6],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'type_id' => Yii::t('app', 'Type ID'),
            'object_id' => Yii::t('app', 'Object ID'),
            'title' => Yii::t('app', 'Title'),
            'note' => Yii::t('app', 'Note'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'del_flg' => Yii::t('app', 'Del Flg'),
            'hours_start' => Yii::t('app', 'Hours Start'),
            'minute_start' => Yii::t('app', 'Minute Start'),
            'hours_end' => Yii::t('app', 'Hours End'),
            'minute_end' => Yii::t('app', 'Minute End'),
            'start_date' => Yii::t('app', 'Start Date'),
            'end_date' => Yii::t('app', 'End Date'),
            //-------------------------------
            'membership_card_number' => Yii::t('app', 'Membership card number'),
            'first_name' => Yii::t('app', 'First name'),
            'last_name' => Yii::t('app', 'Last name'),
            'first_name_kana' => Yii::t('app', 'First name kana'),
            'last_name_kana' => Yii::t('app', 'Last name kana'),
            'sex' => Yii::t('app', 'Sex'),
            'start_birth_date' => Yii::t('app', 'start_birth_date'),
            'end_birth_date' => Yii::t('app', 'end_birth_date'),
            'start_visit_date' => Yii::t('backend', 'Last visit time'),
            'end_visit_date' => Yii::t('backend', 'Last visit time'),
            'max_visit_number' => Yii::t('app', 'Visit number'),
            'min_visit_number' => Yii::t('app', 'Visit number'),
            'rank_id' => Yii::t('app', 'Customers rank'),
            'last_staff' => Yii::t('app', 'Last time staff'),
            'black_list' => Yii::t('app', 'black_list'),
        ];
    }
}
