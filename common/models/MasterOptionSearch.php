<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\MasterOption;
use common\components\Util;
/**
 * MasterOptionSearch represents the model behind the search form about `common\models\MasterOption`.
 */
class MasterOptionSearch extends MasterOption
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['store_id'], 'integer'],
            [['name'],'string','max'=>60],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MasterOption::find()
                ->innerJoin('mst_store','mst_store.id = mst_option.store_id')
                // Join : Company store
                ->innerJoin('company', 'company.id = mst_store.company_id');

        // add conditions that should always apply here
        $company_id =  Util::getCookiesCompanyId();
        $query->andFilterWhere([
            'company.id' => $company_id,
        ]);
        
        $dataProvider = new ActiveDataProvider([
            'query' => $query,        
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' =>false
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'mst_option.store_id' => $this->store_id,
        ]);
       
        $query->andFilterWhere(['like', 'mst_option.name', $this->name]);
        $query->orderBy('mst_option.name ASC');
        
        return $dataProvider;
    }
}
