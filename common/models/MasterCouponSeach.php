<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\MasterCoupon;
use common\components\Util;

/**
 * MasterCouponSeach represents the model behind the search form about `common\models\MasterCoupon`.
 */
class MasterCouponSeach extends MasterCoupon {

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['id', 'exp_date', 'start_date', 'end_date', 'sex', 'visit_number_min', 'visit_number_max', 'black_list_flg', 'grant_point', 'title_font_size', 'title_postion', 'introduction_font_size_1', 'introduction_postion_1', 'introduction_font_size_2', 'introduction_postion_2', 'introduction_font_size_3', 'introduction_postion_3', 'combine_with_other_coupon_flg', 'created_at', 'updated_at', 'created_by', 'updated_by', 'display_barcode_flg'], 'safe'],
            [['coupon_jan_code', 'name_product', 'category', 'benefit_content', 'last_staff_id', 'sex', 'name_customer', 'store_id', 'coupon_date_from', 'coupon_date_to', 'introduction', 'id', 'exp_date', 'start_date', 'end_date', 'show_coupon', 'sex', 'visit_number_min', 'visit_number_max', 'black_list_flg', 'benefits_content', 'grant_point', 'title_font_size', 'title_postion', 'introduction_font_size_1', 'introduction_postion_1', 'introduction_font_size_2', 'introduction_postion_2', 'introduction_font_size_3', 'introduction_postion_3', 'combine_with_other_coupon_flg', 'created_at', 'updated_at', 'created_by', 'updated_by', 'name', 'memo', 'title', 'content', 'type_id', 'start_date', 'code_membership', 'name_kana', 'birthday_from', 'birthday_to', 'last_visit_date_from', 'last_visit_date_to', 'image', 'introduction_1', 'introduction_2', 'introduction_3', 'del_flg', 'rank_id', 'coupon_details_description'], 'safe'],
            [['staff_id', 'rank_customer', 'code_membership', 'discount_yen', 'discount_percent', 'discount_price_set', 'discount_price_set_tax_type', 'discount_drink_eat', 'discount_drink_eat_tax_type'], 'safe'],
            [['expire_date', 'start_date', 'coupon_date_to', 'coupon_date_from', 'birthday_from', 'birthday_to', 'last_visit_date_from', 'last_visit_date_to'], 'date', 'format' => 'php:Y/m/d'],
                // [['expire_date', 'start_date', 'coupon_date_to', 'coupon_date_from', 'birthday_from', 'birthday_to', 'last_visit_date_from', 'last_visit_date_to'], 'date', 'format' => 'php:Y/m/d'],
                // Validate
//            [['coupon_date_to'], 'checkStartDate'],
//            [['birthday_to'], 'checkStartBirthDay'],
//            [['last_visit_date_to'], 'checkStartLastVisit'],
//            [['visit_number_max'], 'checkStartVisitTime'],
            [['visit_number_min','visit_number_max'], 'string', 'max' => 6],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {

        $this->load($params);

        // ==========================================================================
        // Query
        // ==========================================================================

        if (empty($this->category) && empty($this->name_product)) {
            $query = MasterCoupon::find()
                    // Join :  store
                    ->innerJoin('mst_store', 'mst_store.id = coupon.store_id')
                    // Join : Company store
                    ->innerJoin('company', 'company.id = mst_store.company_id');
        }

        if (empty($this->name_product) && !empty($this->category)) {
            $query = MasterCoupon::find()
                    // Join : product_coupon
                    ->innerJoin('product_coupon', 'product_coupon.coupon_id = coupon.id')
                    // Join : mst_product
                    ->innerJoin('mst_product', 'product_coupon.product_id = mst_product.id')
                    // Join : mst_product_category
                    ->innerJoin('mst_product_category', 'mst_product.category_id = mst_product_category.id')
                    // Join :  store
                    ->innerJoin('mst_store', 'mst_store.id = coupon.store_id')
                    // Join : Company store
                    ->innerJoin('company', 'company.id = mst_store.company_id');
        }

        if (!empty($this->name_product) && empty($this->category)) {
            $query = MasterCoupon::find()
                    // Join : product_coupon
                    ->innerJoin('product_coupon', 'product_coupon.coupon_id = coupon.id')
                    // Join : mst_product
                    ->innerJoin('mst_product', 'product_coupon.product_id = mst_product.id')
                    // Join :  store
                    ->innerJoin('mst_store', 'mst_store.id = coupon.store_id')
                    // Join : Company store
                    ->innerJoin('company', 'company.id = mst_store.company_id');
        }

        if (!empty($this->name_product) && !empty($this->category)) {
            $query = MasterCoupon::find()
                    // Join : product_coupon
                    ->innerJoin('product_coupon', 'product_coupon.coupon_id = coupon.id')
                    // Join : mst_product
                    ->innerJoin('mst_product', 'product_coupon.product_id = mst_product.id')
                    // Join : mst_product_category
                    ->innerJoin('mst_product_category', 'mst_product.category_id = mst_product_category.id')
                    // Join :  store
                    ->innerJoin('mst_store', 'mst_store.id = coupon.store_id')
                    // Join : Company store
                    ->innerJoin('company', 'company.id = mst_store.company_id');
        }
        // ==========================================================================
        // End Query
        // ==========================================================================

        $query->select([
            'coupon.id',
            'coupon.start_date',
            'coupon.title',
            'coupon.benefits_content',
            'coupon.coupon_jan_code',
            'coupon.expire_date',
            'coupon.show_coupon',
            'coupon.code_membership',
            'coupon.first_name',
            'coupon.last_name',
            'coupon.first_name_kana',
            'coupon.last_name_kana',
            'coupon.sex',
            'coupon.birthday_from',
            'coupon.birthday_to',
            'coupon.last_visit_date_from',
            'coupon.last_visit_date_to',
            'coupon.visit_number_min',
            'coupon.visit_number_max',
            'coupon.store_id',
            'coupon.rank_id',
            'coupon.last_staff_id',
            'coupon.black_list_flg',
            'coupon.discount_yen',
            'coupon.discount_percent',
            'coupon.discount_price_set',
            'coupon.discount_price_set_tax_type',
            'coupon.discount_drink_eat',
            'coupon.discount_drink_eat_tax_type',
            'coupon.grant_point',
            'coupon.image',
            'coupon.title',
            'coupon.title_font_size',
            'coupon.title_postion',
            'coupon.introduction_1',
            'coupon.introduction_font_size_1',
            'coupon.introduction_postion_1',
            'coupon.introduction_2',
            'coupon.introduction_font_size_2',
            'coupon.introduction_postion_2',
            'coupon.introduction_3',
            'coupon.introduction_font_size_3',
            'coupon.introduction_postion_3',
            'coupon.coupon_details_description',
            'coupon.display_barcode_flg',
            'coupon.combine_with_other_coupon_flg',
        ]);

        // add conditions that should always apply here
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => false,
        ]);



        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            $query->where('0=1');
            return $dataProvider;
        }

        // Datpdt 19/09/2016
        // =================================================
        // Datpdt 19/09/2016
        // =================================================
        $filtercoupon_store_id = ['=', 'coupon.del_flg', 0];
        //$cookies = Yii::$app->request->cookies;
        $company_id = Util::getCookiesCompanyId();
        $filtercoupon_store_id = (['=', 'company.id', $company_id]);
        // Search where : coupon_date_from and coupon_date_to
        $filtercoupon_date_from = ['=', 'coupon.del_flg', 0];
        if (!empty($this->coupon_date_from)) {
            $filtercoupon_date_from = (['>=', 'coupon.start_date', $this->coupon_date_from]);
        }
        $filtercoupon_date_to = ['=', 'coupon.del_flg', 0];
        if (!empty($this->coupon_date_to)) {
            $filtercoupon_date_to = (['<=', 'coupon.start_date', $this->coupon_date_to]);
        }
        // End  Search where : coupon_date_from and coupon_date_to
        // =================================================  
        // =================================================
        // Search where : Display Coupon
        $filtershow = ['=', 'coupon.del_flg', 0];
        if (!empty($this->show_coupon)) {

            $show_coupon = null;
            $check = 0;
            foreach ($this->show_coupon as $value) {
                $check++;
                if ($check === 1) {
                    $show_coupon.= $value;
                } else {
                    $show_coupon .= "," . $value;
                }
            }
//            $arr[] = ['=', 'coupon.show_coupon', $show_coupon];
            // $filterstaff = 
            $filtershow = (['LIKE', 'coupon.show_coupon', $show_coupon]);
            //$filtershow = (['AND', $arr]);
        }
        // End Search where : Display coupon
        // =================================================
        // =================================================
        // Search where : code membership
        $filtercode_membership = ['=', 'coupon.del_flg', 0];
        if (!empty($this->code_membership)) {
            $filtercode_membership = (['=', 'coupon.code_membership', $this->code_membership]);
        }
        // End Search where : code membership
        // =================================================   
        // =================================================
        // Search where : store_id (table product_store)
        $filterdistributing_store = ['=', 'coupon.del_flg', 0];
        if (!empty($this->store_id)) {
            $filterdistributing_store = (['=', 'coupon.store_id', $this->store_id]);
        }
        // End Search where : store_id
        // =================================================
        // =================================================     
        // Search where : name
        $filtername = ['=', 'coupon.del_flg', 0];
        if (!empty($this->name_customer)) {
            $filtername = (['OR',
                ['LIKE', "concat(coupon.first_name_kana,' ',coupon.last_name_kana)", $this->name_customer],
                ['LIKE', "concat(coupon.first_name,' ',coupon.last_name)", $this->name_customer],
                ['LIKE', "concat(coupon.first_name_kana,coupon.last_name_kana)", $this->name_customer],
                ['LIKE', "concat(coupon.first_name,coupon.last_name)", $this->name_customer],
                ['LIKE', "coupon.first_name", $this->name_customer],
                ['LIKE', "coupon.last_name", $this->name_customer],
                ['LIKE', "coupon.first_name_kana", $this->name_customer],
                ['LIKE', "coupon.last_name_kana", $this->name_customer]]
                    );
        }
        // End Search where : name
        // =================================================
        // Search where : Rank
        $filterrank = ['=', 'coupon.del_flg', 0];
        if (!empty($this->rank_id)) {
            $filterrank = (['=', 'coupon.rank_id', $this->rank_id]);
        }
        // End Search where : Rank
        // =================================================
        // =================================================
        // Search where : Sex
        $filtersex = ['=', 'coupon.del_flg', 0];
        if ($this->sex != null) {
            $filtersex = (['=', 'coupon.sex', $this->sex]);
        }
        // End Search where : Sex
        // =================================================
        // Search where : Staff
        $filterstaff = ['=', 'coupon.del_flg', 0];
        if (!empty($this->last_staff_id)) {
            $filterstaff = (['=', 'coupon.last_staff_id', $this->last_staff_id]);
        }
        // End Search where : Staff
        // =================================================
        // =================================================
        // Search where : visit_number_min AND visit_number_max
        // visit_number_max = null
        $filternumbervisit = ['=', 'coupon.del_flg', 0];
        if ($this->visit_number_min != null && $this->visit_number_max == null) {
            $filternumbervisit = (['=', 'coupon.visit_number_min', $this->visit_number_min]);
        }
        // visit_number_min = null
        if ($this->visit_number_min == null && $this->visit_number_max != null) {
            $filternumbervisit = (['=', 'coupon.visit_number_max', $this->visit_number_max]);
        }

        if ($this->visit_number_min != null && $this->visit_number_max != null) {
            $filternumbervisit = ['OR', ['BETWEEN', 'coupon.visit_number_min', $this->visit_number_min, $this->visit_number_max],
                ['BETWEEN', 'coupon.visit_number_max', $this->visit_number_min, $this->visit_number_max],
                ['AND', ['<', 'coupon.visit_number_min', $this->visit_number_min], ['>', 'coupon.visit_number_max', $this->visit_number_max]]];
        }
        // End Search where : visit_number_min AND visit_number_max
        // ===================================================
        // ===================================================
        // Search where : Birth Date
        $filterbirthday = ['=', 'coupon.del_flg', 0];
        if (!empty($this->birthday_from)) {
            $query->andFilterWhere(['>=', 'coupon.birthday_from', $this->birthday_from]);
        }

        // visit_number_min = null
        if (!empty($this->birthday_to)) {
            $query->andFilterWhere(['<=', 'coupon.birthday_to', $this->birthday_to]);
        }

        /* if (!empty($this->birthday_from) && !empty($this->birthday_to)) {
            $filterbirthday = ['OR', ['BETWEEN', 'coupon.birthday_from', $this->birthday_from, $this->birthday_to],
                ['BETWEEN', 'coupon.birthday_to', $this->birthday_from, $this->birthday_to],
                ['AND', ['<', 'coupon.birthday_from', $this->birthday_from], ['>', 'coupon.birthday_to', $this->birthday_to]]];
        } */
        // End Search where : Birth Date
        // ===================================================
        // ===================================================
        // Search where : last_visit_date_from and last_visit_date_to
        // last_visit_date_to = null
        $filterlastvisit = ['=', 'coupon.del_flg', 0];
        if (!empty($this->last_visit_date_from) && empty($this->last_visit_date_to)) {
            $filterlastvisit = (['=', 'coupon.last_visit_date_from', $this->last_visit_date_from]);
        }

        // last_visit_date_from = null
        if (empty($this->last_visit_date_from) && !empty($this->last_visit_date_to)) {
            $filterlastvisit = (['=', 'coupon.last_visit_date_to', $this->last_visit_date_to]);
        }

        if (!empty($this->last_visit_date_from) && !empty($this->last_visit_date_to)) {
            $filterlastvisit = ['OR', ['BETWEEN', 'coupon.last_visit_date_from', $this->last_visit_date_from, $this->last_visit_date_to],
                ['BETWEEN', 'coupon.last_visit_date_to', $this->last_visit_date_from, $this->last_visit_date_to],
                ['AND', ['<', 'coupon.last_visit_date_from', $this->last_visit_date_from], ['>', 'coupon.last_visit_date_to', $this->last_visit_date_to]]];
        }
        // End Search where : last_visit_date_from and last_visit_date_to
        // ====================================================
        // ====================================================
        // Search where : benefit_content
        $filterbenefit = ['=', 'coupon.del_flg', 0];
        if (!empty($this->benefit_content)) {
            $filterbenefit = (['=', 'coupon.benefits_content', $this->benefit_content]);
        }
        // End Search where : benefit_content
        // ====================================================        
        // ====================================================
        // Search where : category
        $filtercategory = ['=', 'coupon.del_flg', 0];
        if (!empty($this->category)) {
            foreach ($this->category as $cate_id) {
                //select id FROM mst_product where category_id = {param1}
                $product = MstProduct::find()->where([MstProduct::tableName() . '.category_id' => $cate_id])->select('id');
                //SELECT "coupon_id" FROM "product_coupon" 
                //WHERE "product_id" IN (SELECT "id" 
                //                          FROM "mst_product" 
                //                          WHERE "mst_product"."category_id"= {param1}) 
                //                          GROUP BY "coupon_id" HAVING COUNT(coupon_id) > 0
                $count_booking_id = ProductCoupon::find()->select("coupon_id")->where(['product_id' => $product])->groupBy("coupon_id")->having([">", "COUNT(coupon_id)", 0]);
                //coupon_id IN (SELECT "coupon_id" FROM "product_coupon" 
                //              WHERE "product_id" IN (SELECT "id" 
                //                          FROM "mst_product" 
                //                          WHERE "mst_product"."category_id"= {param1}) 
                //                          GROUP BY "coupon_id" HAVING COUNT(coupon_id) > 0
                $arr[] = [ ProductCoupon::tableName() . '.coupon_id' => $count_booking_id];
            }
            $filtercategory = array_merge(['AND'], $arr);
        }
        // End Search where : category
        // ====================================================      
        // ====================================================
        // Search where : name product
        $filternameproduct = ['=', 'coupon.del_flg', 0];
        if (!empty($this->name_product)) {
            $filternameproduct = (['LIKE', 'mst_product.name', $this->name_product]);
        }
        // End Search where : name product
        // ====================================================   
        // ====================================================
        // Search where : title
        $filtertitle = ['=', 'coupon.del_flg', 0];
        if (!empty($this->title)) {
            $filtertitle = (['LIKE', 'coupon.title', $this->title]);
        }
        // End Search where : title
        // ====================================================   
        // ====================================================
        // Search where : introduction_1, introduction_2, introduction_3
        $filterintroduction = ['=', 'coupon.del_flg', 0];
        if (!empty($this->introduction)) {
            $filterintroduction = (['OR', ['LIKE', 'coupon.introduction_1', $this->introduction], ['LIKE', 'coupon.introduction_2', $this->introduction], ['LIKE', 'coupon.introduction_3', $this->introduction]]);
        }
        // End Search where : title
        // ====================================================  
        // ====================================================
        // Search coupon_jan_code
        $filtercoupon_jancode = ['=', 'coupon.del_flg', 0];
        if (!is_null($this->coupon_jan_code)) {
            $filtercoupon_jancode = (['=', 'coupon.coupon_jan_code', $this->coupon_jan_code]);
        }
        // End Search coupon_jan_code
        // Rank
        $filtercoupon_rank = ['=', 'coupon.del_flg', 0];
        if (!is_null($this->coupon_jan_code)) {
            $filtercoupon_rank = (['=', 'coupon.rank_id', $this->rank_customer]);
        }

        $query->andFilterWhere([
            'AND',
            $filtercoupon_store_id,
            $filternumbervisit,
            $filterbirthday,
            $filterlastvisit,
            $filterintroduction,
            $filtertitle,
            $filterbenefit,
            $filtercategory,
            $filternameproduct,
            $filterstaff,
            $filtersex,
            $filterrank,
            $filtername,
            $filterdistributing_store,
            $filtercode_membership,
            $filtercoupon_date_from,
            $filtercoupon_date_to,
            $filtershow,
            $filtercoupon_jancode,
            $filtercoupon_rank
        ]);
        
        $query->groupBy([
           'coupon.id',
            'coupon.start_date',
            'coupon.title',
            'coupon.benefits_content',
            'coupon.coupon_jan_code',
            'coupon.expire_date',
            'coupon.show_coupon',
            'coupon.code_membership',
            'coupon.first_name',
            'coupon.last_name',
            'coupon.first_name_kana',
            'coupon.last_name_kana',
            'coupon.sex',
            'coupon.birthday_from',
            'coupon.birthday_to',
            'coupon.last_visit_date_from',
            'coupon.last_visit_date_to',
            'coupon.visit_number_min',
            'coupon.visit_number_max',
            'coupon.store_id',
            'coupon.rank_id',
            'coupon.last_staff_id',
            'coupon.black_list_flg',
            'coupon.discount_yen',
            'coupon.discount_percent',
            'coupon.discount_price_set',
            'coupon.discount_price_set_tax_type',
            'coupon.discount_drink_eat',
            'coupon.discount_drink_eat_tax_type',
            'coupon.grant_point',
            'coupon.image',
            'coupon.title',
            'coupon.title_font_size',
            'coupon.title_postion',
            'coupon.introduction_1',
            'coupon.introduction_font_size_1',
            'coupon.introduction_postion_1',
            'coupon.introduction_2',
            'coupon.introduction_font_size_2',
            'coupon.introduction_postion_2',
            'coupon.introduction_3',
            'coupon.introduction_font_size_3',
            'coupon.introduction_postion_3',
            'coupon.coupon_details_description',
            'coupon.display_barcode_flg',
            'coupon.combine_with_other_coupon_flg',
        ]);
        
        $query->orderBy(['(coupon.coupon_jan_code,coupon.start_date)' => SORT_ASC]);

        return $dataProvider;
    }

    // **************************************** //
    // Check Display Start Date And Valid Period
    // **************************************** //
    public function checkStartDate($attribute, $params) {
        $start_date = $this->coupon_date_from;
        $expire_date = $this->coupon_date_to;
        if (!empty($expire_date)) {
            if ($start_date > $expire_date) {
                $key = $attribute;
                $this->addError($key, Yii::t('backend', "Expiration date, please specify the date of the later display start"));
            }
        }
    }

    // **************************************** //
    // Check Display BirthdayFROM And BirthdayTo
    // **************************************** //
    public function checkStartBirthDay($attribute, $params) {
        $birthday_from = $this->birthday_from;
        $birthday_to = $this->birthday_to;
        if (!empty($birthday_to)) {
            if ($birthday_from > $birthday_to) {
                $key = $attribute;
                $this->addError($key, Yii::t('backend', "End date must be greater than Start date."));
            }
        }
    }

    // **************************************** //
    // Check Display Last visit date TO And Last visit date From
    // **************************************** //
    public function checkStartLastVisit($attribute, $params) {
        $last_visit_date_from = $this->last_visit_date_from;
        $last_visit_date_to = $this->last_visit_date_to;
        if ($last_visit_date_from > $last_visit_date_to) {
            $key = $attribute;
            $this->addError($key, Yii::t('backend', "End date must be greater than Start date."));
        }
    }

    // **************************************** //
    // Check Display visit time TO And visit time From
    // **************************************** //
    public function checkStartVisitTime($attribute, $params) {
        $visit_number_min = $this->visit_number_min;
        $visit_number_max = $this->visit_number_max;
        if ($visit_number_min > $visit_number_max) {
            $key = $attribute;
            $this->addError($key, Yii::t('backend', "Exit number, please specify the number since the start of the number."));
        }
    }

}
