<?php

namespace common\models;

use Yii;
use yii\helpers\ArrayHelper;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use yii2tech\ar\softdelete\SoftDeleteBehavior;
use common\models\ProductOption;
use common\models\BookingProduct;
use common\components\Constants;
/**
 * This is the model class for table "mst_option".
 *
 * @property integer $id
 * @property string $name
 * @property string $type
 * @property string $del_flg
 * @property integer $created_at
 * @property integer $created_by
 * @property integer $updated_at
 * @property integer $updated_by
 * @property integer $store_id
 */
class MasterOption extends \yii\db\ActiveRecord
{
    //list product selected
    public $option;
    public $option_hidden;
    public $id_store_option;
    public $_role;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'mst_option';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),            
            BlameableBehavior::className(),
            'softDeleteBehavior' => [
                'class' => SoftDeleteBehavior::className(),
                'softDeleteAttributeValues' => [
                    'del_flg' => '1'
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['created_at', 'created_by', 'updated_at', 'updated_by', 'store_id'], 'integer'],
            [['name'], 'string', 'max' => 60],
            [['type', 'del_flg'], 'string', 'max' => 1],
            [['option_hidden','option','id_store_option','_role'],'safe'],
            [['store_id'], 'required', 'when' => function() {
                $role = \common\components\FindPermission::getRole();
                return ($role->permission_id == Constants::STORE_MANAGER_ROLE || $role->permission_id == Constants::STAFF_ROLE);
            }],
            
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend', 'ID'),
            'name' => Yii::t('backend', 'Option name'),
            'type' => Yii::t('backend', 'Type'),
            'del_flg' => Yii::t('backend', 'Del Flg'),
            'created_at' => Yii::t('backend', 'Created At'),
            'created_by' => Yii::t('backend', 'Created By'),
            'updated_at' => Yii::t('backend', 'Updated At'),
            'updated_by' => Yii::t('backend', 'Updated By'),
            'store_id' => Yii::t('backend', 'Store Id'),
        ];
    }
    
    public static function findFrontEnd()
    {
            return parent::find(['or', [self::tableName() . '.del_flg' => '0'], [self::tableName() . '.del_flg' => null]]);
    }
    
    public function listOption(){
        return ArrayHelper::map(self::find()->all(), 'id', 'name');
    }
    
    public function listOptionStore($store_id = ''){
        return ArrayHelper::map(self::find()->where(['store_id'=>$store_id,'del_flg'=>'0'])->orderBy('name DESC')->all(), 'id', 'name');
    }
            
    public static function find()
    {
        return parent::find()->where((new \common\components\FindPermission)->getPermissionCondition(self::tableName(), true));
    }
    
    //save into table product_option
    public function saveProductOption(){
        ProductOption::updateAll(['del_flg' => '1','updated_at' => strtotime("now")],['option_id'=>  $this->id]);
        $product_arr = empty($this->option_hidden)?"":explode(",",$this->option_hidden);
        if(!empty($product_arr)){
            foreach ($product_arr as $val){
                $product_option = new ProductOption();
                $product_option->option_id = $this->id;
                $product_option->product_id = $val;
                $product_option->save();
            }
        }
    }
    
    public function  checkDelete($id){
        $check = 0;
        $check = ProductOption::find()->where(['option_id'=>$id])->count();
        $check += BookingProduct::find()->where(['option_id'=>$id])->count();
        return ($check>0)?FALSE:TRUE;
    }
    public function getProductOption()
    {
        return $this->hasMany(ProductOption::className(), ['option_id' => 'id']);
    }
}
