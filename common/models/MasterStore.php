<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\helpers\ArrayHelper;
use yii\behaviors\BlameableBehavior;
use yii2tech\ar\softdelete\SoftDeleteBehavior;
// Use Delete
use common\models\MasterTicket;
use common\models\MasterTypeSeat;
use common\models\StoreSchedule;
use common\models\MasterSeat;
use common\models\MasterCoupon;
use common\models\Booking;
use common\models\StoreShift;
use common\models\MasterStaff;
use common\models\CustomerStore;
use common\models\MstProduct;
use \common\components\Constants;
use common\components\Util;

/**
 * This is the model class for table "mst_store".
 *
 * @property integer $id
 * @property string $name
 * @property string $address
 * @property string $directions_address
 * @property string $website
 * @property string $time_open
 * @property string $time_close
 * @property integer $regular_holiday
 * @property integer $tel
 * @property string $introduction
 * @property string $image1
 * @property string $image2
 * @property string $image3
 * @property boolean $show
 * @property string $point_conversion
 * @property string $reservations_possible_time
 * @property integer $booking_resources
 * @property string $name_item_1
 * @property string $name_item_2
 * @property string $name_item_3
 * @property string $name_item_4
 * @property string $name_item_5
 * @property string $name_item_6
 * @property string $name_item_7
 * @property string $name_item_8
 * @property string $name_item_9
 * @property string $name_item_10
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $people_created
 * @property string $people_updated
 * @property string $link_title_1
 * @property string $link_url_1
 * @property string $link_icon_1
 * @property string $link_title_2
 * @property string $link_url_2
 * @property string $link_icon_2
 * @property string $link_title_3
 * @property string $link_url_3
 * @property string $link_icon_3
 * @property string $staff_item_1
 * @property string $staff_item_2
 * @property string $staff_item_3
 * @property string $staff_item_4
 * @property string $staff_item_5
 * @property string $store_code
 */
class MasterStore extends \yii\db\ActiveRecord {

    public $tmp_image1;
    public $tmp_image2;
    public $tmp_image3;
    public $tmp_image4;
    public $tmp_image5;
    public $tmp_icon_1;
    public $tmp_icon_2;
    public $tmp_icon_3;
    public $tmp_app_logo_img;
    public $tmp_receipt_logo_img;
    public $tmp_gold_ticket_image;
    // Hiden Image For Delete
    public $hidden_image1;
    public $hidden_image2;
    public $hidden_image3;
    public $hidden_image4;
    public $hidden_image5;
    public $hidden_app_logo_img;
    public $hidden_receipt_logo_img;
    public $hidden_gold_ticket_image;
    public $hidden_icon_1;
    public $hidden_icon_2;
    public $hidden_icon_3;
    public $jan_code_before_create;
    public $store_name;
    public $ticket_balance;
    public $ticket_jan_code;
    public $store_id;

    const BOOKING_RESULT_RESTAURANCE = '01';
    const BOOKING_RESULT_SALON = '02';
    const BOOKING_RESULT_NEITHER = '00';

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'mst_store';
    }

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            TimestampBehavior::className(),
            BlameableBehavior::className(),
            'softDeleteBehavior' => [
                'class' => SoftDeleteBehavior::className(),
                'softDeleteAttributeValues' => [
                    'del_flg' => '1'
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['regular_holiday', 'address', 'introduce'], 'string'],
            [['booking_resources'], 'integer'],
            [['tel'], 'double', 'message' => Yii::t('backend', '{attribute} phone is not valid')],
            [['show_flg'], 'boolean'],
            [['name'], 'required', 'message' => Yii::t('backend', 'Name Store Valid')],
            [['name', 'image1', 'image2', 'image3', 'image4', 'image5', 'reservations_possible_time', 'store_item_1', 'store_item_2', 'store_item_3', 'store_item_4', 'store_item_5', 'store_item_6', 'store_item_7', 'store_item_8', 'store_item_9', 'store_item_10', 'link_title_1', 'link_icon_1', 'link_title_2', 'link_icon_2', 'link_title_3', 'link_icon_3', 'staff_item_1', 'staff_item_2', 'staff_item_3', 'staff_item_4', 'staff_item_5'], 'string', 'max' => 100],
            [['directions', 'introduce'], 'string', 'max' => 500],
            [['website', 'link_url_1', 'link_url_2', 'link_url_3'], 'string', 'max' => 300],
            [['name_kana', 'name', 'regular_holiday', 'link_title_1', 'link_title_2', 'link_title_3', 'store_item_7', 'store_item_8'], 'string', 'max' => 100],
            [['time_open', 'time_close'], 'string', 'max' => 20],
            [['address'], 'string', 'max' => 240],
            //[['tel'], 'integer', 'max' => 17],
            //[['supply_condition_point'], 'decimal', 'max' => 11],
            //[['supply_condition_yen'], 'integer', 'max' => 13],
            //[['supply_year'], 'integer', 'max' => 2],
            [['gold_ticket_description1', 'gold_ticket_description2', 'store_item_1', 'store_item_2', 'store_item_3', 'store_item_4', 'store_item_5', 'store_item_6', 'store_item_9', 'store_item_10', 'staff_item_1', 'staff_item_2', 'staff_item_3', 'staff_item_4', 'staff_item_5'], 'string', 'max' => 100],
            //[['reservations_possible_time','cancel_possible_time'], 'integer', 'max' => 3],
            [['store_item_1_title', 'store_item_2_title', 'store_item_3_title', 'store_item_4_title', 'store_item_5_title', 'store_item_6_title', 'store_item_7_title', 'store_item_8_title', 'store_item_9_title', 'store_item_10_title'], 'string', 'max' => 25],
            [['tmp_image1', 'tmp_image2', 'tmp_image3'], 'file', 'extensions' => 'png, jpg, gif, jpge', 'skipOnEmpty' => true],
            [['show_in_booking_flg', 'show_in_notice_flg', 'show_in_coupon_flg', 'show_in_product_category_flg', 'show_in_my_page_flg', 'show_in_staff_flg',
            'link_icon_1', 'link_icon_2', 'link_icon_3', 'post_code',
            'hidden_image1', 'hidden_image2', 'hidden_image3', 'hidden_image4', 'hidden_image5', 'hidden_icon_1', 'hidden_icon_2', 'hidden_icon_3',
            'latitude', 'longitude', 'store_code', 'store_item_1_title', 'store_item_2_title', 'store_item_3_title', 'store_item_4_title', 'store_item_5_title', 'store_item_6_title', 'store_item_7_title', 'store_item_8_title', 'store_item_9_title', 'store_item_10_title', 'name_kana', 'cancel_possible_time'
            , 'pos_use', 'temporary_stop', 'round_down_flg', 'round_process_flg', 'app_logo_img', 'receipt_logo_img', 'gold_ticket_supply_flg', 'gold_ticket_supply_method', 'supply_condition_point'
            , 'supply_condition_yen', 'expiration_date_flg', 'supply_year', 'gold_ticket_image', 'gold_ticket_description1', 'gold_ticket_description2', 'away_dates', 'away_gimmick_dates', 'hidden_app_logo_img', 'hidden_receipt_logo_img', 'hidden_gold_ticket_image'], 'safe'],
            [['time_open'], 'checkTimeOpen'],
            [ 'reservations_possible_time', 'match', 'not' => true, 'pattern' => '/[^0-9]/', 'message' => Yii::t('backend', 'Invalid Reservations Possible Time Characters')],
            [['link_url_1'], 'url', 'defaultScheme' => ''],
            [['link_url_2'], 'url', 'defaultScheme' => ''],
            [['link_url_3'], 'url', 'defaultScheme' => ''],
            [['website'], 'url', 'defaultScheme' => ''],
            // Check size image
            [['image1', 'image2', 'image3', 'image4', 'image5'], 'file', 'extensions' => 'png, jpg, jpeg, gif', 'skipOnEmpty' => true, 'maxSize' => 1024 * 1024 * 2],
            [['link_icon_1', 'link_icon_2', 'link_icon_3'], 'file', 'extensions' => 'png, jpg, jpeg, gif', 'skipOnEmpty' => true, 'maxSize' => 1024 * 1024 * 2],
            [['store_item_1_title', 'store_item_2_title', 'store_item_3_title', 'store_item_4_title', 'store_item_5_title', 'store_item_6_title', 'store_item_7_title', 'store_item_8_title', 'store_item_9_title', 'store_item_10_title'], 'string', 'max' => 25],
            [['name_kana'], 'trim'],
            [['name_kana'], 'match', 'pattern' => '/^[ァ-ヴー]+$/u', 'message' => Yii::t('backend', '{attribute} invalid characters full size only')],
            [['expiration_date_flg'], 'checkExpirationDateFlg'],
            [['supply_condition_yen'], 'checkSupplyConditionYen'],
            [['supply_condition_point'], 'checkSupplyConditionPoint'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => Yii::t('backend', 'ID'),
            'name' => Yii::t('backend', 'Name Store'),
            'address' => Yii::t('backend', 'Address Store'),
            'directions' => Yii::t('app', 'Directions Address'),
            'website' => Yii::t('backend', 'Website'),
            'time_open' => Yii::t('backend', 'Time Open'),
            'time_close' => Yii::t('backend', 'Time Close'),
            'regular_holiday' => Yii::t('backend', 'Regular Holiday'),
            'tel' => Yii::t('backend', 'Phone'),
            'introduce' => Yii::t('backend', 'Introduction Store'),
            'image1' => Yii::t('backend', 'Image1'),
            'image2' => Yii::t('backend', 'Image2'),
            'image3' => Yii::t('backend', 'Image3'),
            'image4' => Yii::t('backend', 'Image4'),
            'image5' => Yii::t('backend', 'Image5'),
            'show_flg' => Yii::t('backend', 'show_flg'),
            'reservations_possible_time' => Yii::t('app', 'Reservations Possible Time'),
            'booking_resources' => Yii::t('app', 'Booking Resources'),
            'store_item_1' => Yii::t('backend', 'Name Item 1'),
            'store_item_2' => Yii::t('backend', 'Name Item 2'),
            'store_item_3' => Yii::t('backend', 'Name Item 3'),
            'store_item_4' => Yii::t('backend', 'Name Item 4'),
            'store_item_5' => Yii::t('backend', 'Name Item 5'),
            'store_item_6' => Yii::t('backend', 'Name Item 6'),
            'store_item_7' => Yii::t('backend', 'Name Item 7'),
            'store_item_8' => Yii::t('backend', 'Name Item 8'),
            'store_item_9' => Yii::t('backend', 'Name Item 9'),
            'store_item_10' => Yii::t('backend', 'Name Item 10'),
            'created_at' => Yii::t('backend', 'Created At'),
            'updated_at' => Yii::t('backend', 'Updated At'),
            'created_by' => Yii::t('backend', 'People Created'),
            'updated_by' => Yii::t('backend', 'People Updated'),
            'link_title_1' => Yii::t('backend', 'Link Title 1'),
            'link_url_1' => Yii::t('backend', 'Link Url 1'),
            'link_icon_1' => Yii::t('backend', 'Link Icon 1'),
            'link_title_2' => Yii::t('backend', 'Link Title 2'),
            'link_url_2' => Yii::t('backend', 'Link Url 2'),
            'link_icon_2' => Yii::t('backend', 'Link Icon 2'),
            'link_title_3' => Yii::t('backend', 'Link Title 3'),
            'link_url_3' => Yii::t('backend', 'Link Url 3'),
            'link_icon_3' => Yii::t('backend', 'Link Icon 3'),
            'staff_item_1' => Yii::t('backend', 'Staff Item 1'),
            'staff_item_2' => Yii::t('backend', 'Staff Item 2'),
            'staff_item_3' => Yii::t('backend', 'Staff Item 3'),
            'staff_item_4' => Yii::t('backend', 'Staff Item 4'),
            'staff_item_5' => Yii::t('backend', 'Staff Item 5'),
            'show_in_booking_flg' => Yii::t('backend', 'Booking'),
            'show_in_notice_flg' => Yii::t('backend', 'Notice'),
            'show_in_coupon_flg' => Yii::t('backend', 'Coupon'),
            'show_in_product_category_flg' => Yii::t('backend', 'Product Catalog'),
            'show_in_my_page_flg' => Yii::t('backend', 'My Page'),
            'show_in_staff_flg' => Yii::t('backend', 'Censorship Information Staff'),
            'post_code' => Yii::t('backend', 'PostCode'),
            'name_kana' => Yii::t('backend', 'Name Kana'),
            'temporary_stop' => Yii::t('backend', 'Pause'),
            'app_logo_img' => Yii::t('backend', 'App Logo Img'),
            'receipt_logo_img' => Yii::t('backend', 'Receipt Logo Img'),
        ];
    }

    /*
     * Get option attribute with relation hasMany
     */

    public function getOption() {
        return $this->hasMany(MasterOption::className(), ['store_id' => 'id']);
    }

    public function getCustomerStore() {
        return $this->hasMany(CustomerStore::className(), ['store_id' => 'id']);
    }

    public function getbooking() {
        return $this->hasMany(Booking::className(), ['store_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMasterCompany() {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    public function listShowBooking() {
        return array(
            '00' => \Yii::t("app", "席タイプ選択"),
            '01' => \Yii::t("app", "スタッフ選択"),
            '02' => \Yii::t("app", "どちらも選択しない"),
        );
    }

    public static function find($findAll = true) {
        if ($findAll)
            return parent::find()
                            //->innerJoin('company_store', 'company_store.store_id = mst_store.id')
                            ->where(['mst_store.del_flg' => '0']);
        else
            return parent::find();
    }

    public static function findFrontEnd() {
        return parent::find(['or', [self::tableName() . '.del_flg' => '0'], [self::tableName() . '.del_flg' => null]]);
    }

    public static function findByCompany() {
        return self::find()->innerJoin('company', 'company.id = mst_store.company_id')->where(['mst_store.del_flg' => '0']);
    }

    public static function findStoreCommon($store_code) {
        $company_id = Util::getCookiesCompanyId();
        return self::find()->innerJoin('company', 'company.id = mst_store.company_id')->andFilterWhere(['mst_store.del_flg' => '0', 'company.id' => $company_id, 'mst_store.store_code' => $store_code])->select('mst_store.id')->one();
    }

    public function listShow() {
        return array(
            0 => \Yii::t("app", "表示"),
            1 => \Yii::t("app", "非表示"),
        );
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMasterStaff() {
        return $this->hasMany(MasterStaff::className(), ['store_id' => 'id']);
    }

    public static function listMasterShop() {
        $company_id = Util::getCookiesCompanyId();
        return ArrayHelper::map(MasterStore::find()->innerJoin('company', 'company.id = mst_store.company_id')->andFilterWhere(['mst_store.del_flg' => '0', 'company.id' => $company_id])->all(), 'id', 'name');
    }

    public static function getNameMasterShop($id) {
        $model = self::findOne($id);
        return isset($model) ? $model->name : "";
    }

    public static function getListStore() {
        $role = \common\components\FindPermission::getRole();
        $store = MasterStore::findByCompany();
        //->innerJoin('company_store', 'company_store.store_id = mst_store.id');
        if ($role->permission_id == Constants::STORE_MANAGER_ROLE || $role->permission_id == Constants::STAFF_ROLE)
            $store->andWhere(['mst_store.id' => $role->store_id]);
        
        if(Yii::$app->controller->id == 'booking' && Yii::$app->controller->action->id =='accounting-management'){
            $store->andWhere(['mst_store.pos_use' => '0']);
            $store->orWhere(['mst_store.pos_use' => '1', 'mst_store.temporary_stop' => '1']);
        }
        $company_id = Util::getCookiesCompanyId();
        $store->andFilterWhere(['company.id' => $company_id])->andFilterWhere(['!=', 'mst_store.store_code', '00000'])->orderBy('mst_store.id');
        return ArrayHelper::map($store->all(), 'id', 'name');
    }
    
     public static function getListStoreHaveStoreCommon() {
        $role = \common\components\FindPermission::getRole();
        $store = MasterStore::findByCompany();
        //->innerJoin('company_store', 'company_store.store_id = mst_store.id');
        if ($role->permission_id == Constants::STORE_MANAGER_ROLE || $role->permission_id == Constants::STAFF_ROLE)
            $store->andWhere(['mst_store.id' => $role->store_id]);
        
        if(Yii::$app->controller->id == 'booking' && Yii::$app->controller->action->id =='accounting-management'){
            $store->andWhere(['mst_store.pos_use' => '0']);
            $store->orWhere(['mst_store.pos_use' => '1', 'mst_store.temporary_stop' => '1']);
        }
        $company_id = Util::getCookiesCompanyId();
        $store->andFilterWhere(['company.id' => $company_id])->orderBy('mst_store.id');
        return ArrayHelper::map($store->all(), 'id', 'name');
    }

    public static function getListStoreNotStoreCommon() {
        $role = \common\components\FindPermission::getRole();
        $store = MasterStore::findByCompany();
        //->innerJoin('company_store', 'company_store.store_id = mst_store.id');
        if ($role->permission_id == Constants::STORE_MANAGER_ROLE || $role->permission_id == Constants::STAFF_ROLE)
            $store->andWhere(['mst_store.id' => $role->store_id]);
        $company_id = Util::getCookiesCompanyId();
        $store->andFilterWhere(['company.id' => $company_id])->andFilterWhere(['!=', 'mst_store.store_code', '00000'])->orderBy('mst_store.id');
        return ArrayHelper::map($store->all(), 'id', 'name');
    }

    public static function getListStoreTypeSeat() {
        $role = \common\components\FindPermission::getRole();
        $store = MasterStore::findByCompany();
        //->innerJoin('company_store', 'company_store.store_id = mst_store.id');
        if ($role->permission_id == Constants::STORE_MANAGER_ROLE || $role->permission_id == Constants::STAFF_ROLE) {
            $store->andWhere(['mst_store.id' => $role->store_id]);
        }
        $company_id = Util::getCookiesCompanyId();
        $store->andFilterWhere(['company.id' => $company_id])->andFilterWhere(['!=', 'mst_store.store_code', '00000']);
        $store->andWhere(['mst_store.booking_resources' => '01']);
        return ArrayHelper::map($store->all(), 'id', 'name');
    }
    
     public static function getListStoreTypeSeatCustom($check) {
        $role = \common\components\FindPermission::getRole();
        $store = MasterStore::findByCompany();
        //->innerJoin('company_store', 'company_store.store_id = mst_store.id');
        if ($role->permission_id == Constants::STORE_MANAGER_ROLE || $role->permission_id == Constants::STAFF_ROLE) {
            $store->andWhere(['mst_store.id' => $role->store_id]);
        }
        $company_id = Util::getCookiesCompanyId();
        $store->andFilterWhere(['company.id' => $company_id]);
        if($check == 0){
           $store->andFilterWhere(['!=', 'mst_store.store_code', '00000']); 
        }
        $store->andWhere(['mst_store.booking_resources' => '01']);
        return ArrayHelper::map($store->all(), 'id', 'name');
    }

    public function getListStoreFrontend() {
        $session = Yii::$app->session;
        return ArrayHelper::map(MasterStore::find()->all(), 'id', 'name');
    }

    // **************************************** //
    // Generate Store Code //   
    // **************************************** //    
    public function generateStoreCode() {
        $company_id = Util::getCookiesCompanyId();
        $code = self::find(false)->andFilterWhere(['mst_store.del_flg' => '0', 'mst_store.company_id' => $company_id])->max('store_code');
        $code = (!isset($code) || empty($code)) ? 0 : $code;
        $code = (int) $code + 1;
        $count = strlen((string) $code);
        switch ($count) {
            case '1': $code = '0000' . $code;
                break;
            case '2': $code = '000' . $code;
                break;
            case '3': $code = '00' . $code;
                break;
            case '4': $code = '0' . $code;
                break;
            case '5': break;
        }
        return $code;
    }

    // **************************************** //
    // Check Time Open And Time Close           //
    // **************************************** //
    public function checkTimeOpen($attribute, $params) {
        $time_close = $this->time_close;
        $time_open = $this->time_open;
        if ($time_open >= $time_close) {
            //$key = $attribute;
            $this->addError('time_close', Yii::t('backend', "Please Reset Larger Closing Time Opening Time"));
        }
    }

    // **************************************** //
    // Check Exit Id Of Table Store Other Table //
    // **************************************** //
    public function checkDelete($id) {
        $check = 0;
        $check = MasterTicket::find()->andWhere(['store_id' => $id])->count();
        $check += MasterTypeSeat::find()->andWhere(['store_id' => $id])->count();
        $check += StoreSchedule::find()->andWhere(['store_id' => $id])->count();
        $check += MasterSeat::find()->andWhere(['store_id' => $id])->count();
        $check += MasterCoupon::find()->andWhere(['store_id' => $id])->count();
        $check += Booking::find()->andWhere(['store_id' => $id])->count();
        //$check += StoreShift::find()->andWhere(['store_id' => $id])->count();
        $check += MasterStaff::find()->andWhere(['store_id' => $id])->count();
        $check += CustomerStore::find()->andWhere(['store_id' => $id])->count();
        $check += MstProduct::find()->andWhere(['store_id' => $id])->count();
        return ($check > 0) ? FALSE : TRUE;
    }

    public function getRestDaySchedule() {
        return $this->hasMany(StoreSchedule::className(), ['store_id' => 'id'])->andWhere(['work_flg' => 0]);
    }

    // =========================================================
    // Select Check List Store
    // =========================================================
    public function selectCheckListStore($id_store) {
        $command = "SELECT
                            checklist.id AS id_checklist,
                            question.id AS id_question,
                            cl.id_answer AS id_answer,
                            checklist.type,
                            checklist.display_flg,
                            checklist.del_flg as del_flg_checklist,
                            question.option,
                            question.question_content,
                            question.max_choice,
                            question.notice_content,
                            cl.answer_content,
                            cl.del_flg_anwers as del_flg_anwers,
                            cl.del_flg_question
                    FROM 
                    (
                             SELECT 
                                question.id,
                                array_to_json(array_agg(question_answer.content))  answer_content,
                                array_to_string(array_agg(question_answer.id),',')  id_answer,
                                array_to_string(array_agg(question_answer.del_flg),',')  del_flg_anwers,
                                array_to_string(array_agg(question.del_flg),',')  del_flg_question
                            FROM
                                store_checklist
                            INNER JOIN mst_store
                            ON 
                              store_checklist.store_id = mst_store.id
                            INNER JOIN checklist
                            ON
                              store_checklist.checklist_id = checklist.id
                            INNER JOIN question
                            ON
                              checklist.question_id = question.id
                            LEFT JOIN question_answer
                            ON
                              question.id = question_answer.question_id
                            WHERE
			      checklist.del_flg = '0'
                            GROUP BY
                              question.id

                    ) cl
                    INNER JOIN checklist
                    ON
                    checklist.question_id =  cl.id
                    INNER JOIN store_checklist
                    ON
                    store_checklist.checklist_id = checklist.id
                    INNER JOIN question
                    ON
                    question.id = cl.id
                    WHERE
                    store_checklist.store_id = :id_store
                    ORDER BY
                    checklist.id
                    ";

        $command = Yii::$app->db->createCommand($command);

        $command->bindParam(':id_store', $id_store);

        $check_list = $command->queryAll();

        for ($i = 0; $i < count($check_list); $i++) {
            if ($check_list[$i]['type'] == '2') {

                if ($check_list[$i]['option'] == '1' || $check_list[$i]['option'] == '2') {

                    // Get Content Answer
                    $value_answer_content = $check_list[$i]['answer_content'];
                    $sub_answer_content = json_decode($value_answer_content);
                    // End
                    $id_answer = explode(",", $check_list[$i]['id_answer']);
                    $del_flg_anwers_array = explode(",", $check_list[$i]['del_flg_anwers']);
                    $count_del = count($del_flg_anwers_array);

                    for ($j = 0; $j < $count_del; $j++) {
                        if ($del_flg_anwers_array[$j] == '1') {
                            unset($id_answer[$j]);
                            unset($sub_answer_content[$j]);
                            unset($del_flg_anwers_array[$j]);
                        }
                    }
                    $check_list[$i]['id_answer'] = implode(",", $id_answer);
                    $check_list[$i]['answer_content'] = array_values($sub_answer_content);
                    $check_list[$i]['del_flg_anwers'] = implode(",", $del_flg_anwers_array);
                }
            }
        }
//        var_dump($check_list);
//        exit();

        return $check_list;
    }

    // =========================================================
    // Select id question_answer exit
    // =========================================================
    public function selectIdQuestionAnswer($question_id) {
        $command = "SELECT  question_answer.id as question_answer_id FROM 
                    question
                    INNER JOIN question_answer
                    ON
                    question.id = question_answer.question_id
                    WHERE
                    question.id = :question_id
                    AND question_answer.del_flg = '0'
                    ";

        $command = Yii::$app->db->createCommand($command);

        $command->bindParam(':question_id', $question_id);

        return $command->queryAll();
    }

    /*
     * List info detail of store by customer
     * Author : vuong_vt
     * @param (int) store id
     */

    public function StoreDetail($id) {
        $query = $detailOrder = (new \yii\db\Query())->select([
                    'mst_store.id',
                    'mst_store.name',
                    'mst_store.name_kana',
                    'mst_store.introduce',
                    'mst_store.store_code',
                    'mst_store.address',
                    'mst_store.website',
                    'mst_store.regular_holiday',
                    'mst_store.introduce',
                    'mst_store.time_open',
                    'mst_store.time_close',
                    'mst_store.tel',
                    'mst_store.directions',
                    'mst_store.image1',
                    'mst_store.image2',
                    'mst_store.image3',
                    'mst_store.image4',
                    'mst_store.image5',
                    'mst_staff.id AS staff_id',
                    'mst_staff.avatar as staff_image1',
                    'mst_staff.name AS staff_name',
                    'mst_staff.position',
                    'mst_staff.catch',
                    'mst_staff.introduction',
                    'mst_staff.career',
                ])->from('mst_store');
        $query->leftJoin('mst_staff', 'mst_staff.store_id = mst_store.id');
        $query->where(['mst_store.id' => $id]);
        $query->andWhere(['mst_store.del_flg' => 0]);
        return $query;
    }

    public function checkExpirationDateFlg($attribute, $params) {
        if ($this->expiration_date_flg == '1') {

            if ($this->supply_condition_point == null) {
                $this->addError('supply_condition_point', Yii::t('backend', "Points should not be blank."));
            }
            if ($this->supply_condition_yen == null) {
                $this->addError('supply_condition_yen', Yii::t('backend', "The yen should not be blank."));
            }
            if ($this->supply_condition_point <= 0) {
                $this->addError('supply_condition_point', Yii::t('backend', "Supply Condition Point of the format is not correct."));
            }
            if ($this->supply_condition_point > 9999999.999) {
                $this->addError('supply_condition_point', Yii::t('backend', "Please enter Supply Condition Point only from 0 to 9999999.999."));
            }
        }
    }

    public function checkSupplyConditionYen($attribute, $params) {
        if ($this->supply_condition_yen <= 0) {
            $this->addError('supply_condition_yen', Yii::t('backend', "Circle, please input greater than 0."));
        }
    }

    public function checkSupplyConditionPoint($attribute, $params) {
        if ($this->supply_condition_point > 9999999.999) {
            $this->addError('supply_condition_point', Yii::t('backend', "Please enter Supply Condition Point only from 0 to 9999999.999."));
        }
        if ($this->supply_condition_point <= 0) {
            $this->addError('supply_condition_point', Yii::t('backend', "Supply Condition Point of the format is not correct."));
        }
    }
    

}
