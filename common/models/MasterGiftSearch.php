<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\MasterGift;
use common\components\Util;
/**
 * MasterGiftSearch represents the model behind the search form about `common\models\MasterGift`.
 */
class MasterGiftSearch extends MasterGift {

    /**
     * @inheritdoc
     */
    public $store_id, $gift_date_from, $gift_date_to, $product_name;
    public $customer_first_name, $customer_last_name;

    public function rules() {
        return [
            [['id', 'total_quantity', 'store_id', 'order_id', 'customer_id', 'created_at', 'created_by', 'updated_at', 'updated_by'], 'integer'],
            [['gift_code', 'gift_date', 'del_flg', 'customer_first_name', 'customer_last_name', 'product_name'], 'safe'],
            [['gift_date_from', 'gift_date_to'], 'date', 'format' => 'php:Y/m/d'],
            [['customer_first_name', 'customer_last_name'], 'string', 'max' => 40],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = MasterGift::find()
                ->innerJoin('mst_store', 'mst_store.id = gift.store_id')
                // Join : Company store
                ->innerJoin('company', 'company.id = mst_store.company_id');
        $company_id =  Util::getCookiesCompanyId();
        $query->andFilterWhere([
            'company.id' => $company_id,
        ]);
        // add conditions that should always apply here
        $dataProvider = new ActiveDataProvider([
            'query' => $query->with('masterCustomer'),
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => false,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            $query->where('0=1');
            return $dataProvider;
        }
        // grid filtering conditions
        $query->andFilterWhere(['gift.store_id' => $this->store_id]);
        //$query->andFilterWhere(['gift_code' => $this->gift_code]);
        if($this->gift_code != '')
            $query->andFilterWhere(['LIKE','lower(gift_code)', strtolower($this->gift_code)]);
        $query->andFilterWhere(['>=', 'gift_date', $this->gift_date_from]);
        $query->andFilterWhere(['<=', 'gift_date', $this->gift_date_to]);
        if (!empty($this->customer_first_name) || !empty($this->customer_last_name)) {
            $query->joinWith('masterCustomer', true, 'INNER JOIN');
            $query->andFilterWhere(['or', ['like', 'first_name', $this->customer_first_name], ['like', 'first_name_kana', $this->customer_first_name]]);
            $query->andFilterWhere(['or', ['like', 'last_name', $this->customer_last_name], ['like', 'last_name_kana', $this->customer_last_name]]);
        }
        if (!empty($this->product_name)) {
            $query->joinWith([
                'masterGiftDetail' => function (\yii\db\ActiveQuery $query) {
                    $query->joinWith('product', true, 'INNER JOIN');
                }
                    ], true, 'INNER JOIN');
            $query->andFilterWhere(['or', ['like', MstProduct::tableName() . '.name', $this->product_name], ['like', MstProduct::tableName() . '.name_kana', $this->product_name]]);
        }
        return $dataProvider;
    }

}
