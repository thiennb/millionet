<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\MstOrderDetail;

/**
 * MstOrderDetailSearch represents the model behind the search form about `common\models\MstOrderDetail`.
 */
class MstOrderDetailSearch extends MstOrderDetail {

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['id', 'order_id', 'created_at', 'created_by', 'updated_at', 'updated_by'], 'integer'],
            [['jan_code', 'product_display_name', 'price_down_type', 'tax_display_method', 'gift_flg', 'del_flg'], 'safe'],
            [['quantity', 'price', 'price_down_value', 'product_sub_total', 'product_total', 'product_tax'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function searchProduct($id) {
        $gift_id = MasterGift::findOne(['order_id'=> $id]);
        $query = MstOrderDetail::find()->andWhere(['=', 'gift_flg', 1]);
        // add conditions that should always apply here
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => false,
        ]);
        $order_code = MstOrder::findOne($id)->order_code;
        $query->andFilterWhere(['order_code' => $order_code]);
        if (!empty($gift_id)) {
            $product_id = MasterGiftDetails::find()->andWhere(['gift_id' => $gift_id->id])->select('product_id');
            if (!empty($product_id)) {
                $query->innerJoin(MstProduct::tableName(), MstProduct::tableName() . '.jan_code = ' . MstOrderDetail::tableName() . '.jan_code');
                $query->andFilterWhere(['NOT IN', MstProduct::tableName() . '.id', $product_id]);
            }
        }

        return $dataProvider;
    }

}
