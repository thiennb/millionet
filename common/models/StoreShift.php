<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii2tech\ar\softdelete\SoftDeleteBehavior;
use yii\helpers\ArrayHelper;
use yii\validators\RequiredValidator;
use yii\validators\NumberValidator;
use yii\validators\DateValidator;
use yii\behaviors\BlameableBehavior;
use common\models\MasterStore;
use common\components\Util;
/**
 * This is the model class for table "store_shift".
 *
 * @property integer $id
 * @property string $name
 * @property string $short_name
 * @property integer $store_id
 * @property string $start_time
 * @property string $end_time
 * @property string $del_flg
 * @property integer $created_at
 * @property integer $created_by
 * @property integer $updated_at
 * @property integer $updated_by
 */
class StoreShift extends \yii\db\ActiveRecord
{
    
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),          
            BlameableBehavior::className(),
            'softDeleteBehavior' => [
                'class' => SoftDeleteBehavior::className(),
                'softDeleteAttributeValues' => [
                    'del_flg' => '1'
                ],
            ],
        ];
    }
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'store_shift';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'short_name', 'store_id', 'start_time', 'end_time'], 'required'],
            [['store_id', 'created_at', 'created_by', 'updated_at', 'updated_by'], 'integer'],
            [['name'], 'string', 'max' => 50],
            [['short_name'], 'string', 'max' => 25],
            [['start_time', 'end_time'], 'string', 'max' => 5],
            [['del_flg'], 'string', 'max' => 1],
            // Validate time_open, time_close
            [['end_time'], 'checkTimeOpen'],
            [['start_time','end_time'], \common\components\HourFormatValidator::className()]
        ];
    }

    /**
     * @inheritdoc  
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend', 'ID'),
            'name' => Yii::t('backend', 'Shift Name'),
            'short_name' => Yii::t('backend', 'Short Name'),
            'store_id' => Yii::t('backend', 'Store ID'),
            'start_time' => Yii::t('backend', 'Start Time'),
            'end_time' => Yii::t('backend', 'End Time'),
            'del_flg' => Yii::t('backend', 'Del Flg'),
            'created_at' => Yii::t('backend', 'Created At'),
            'created_by' => Yii::t('backend', 'Created By'),
            'updated_at' => Yii::t('backend', 'Updated At'),
            'updated_by' => Yii::t('backend', 'Updated By'),
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStores()
    {
        return $this->hasMany(MasterStore::className(), ['store_id' => 'id']);
    }
    
    public static function find()
    {
        return parent::find()->where([StoreShift::tableName().'.del_flg' => 0]);
    }
    
    public static function search($store_id) {
        
        $query = StoreShift::find();

        if ( $store_id != null ) {
            $query->andWhere('store_id = :store_id', [':store_id' => $store_id]);
            $query->orderBy('created_at');
        }
        
        return $query;
    }
    
    public function checkTimeOpen($attribute, $params) {
        $end_time = $this->end_time;
        $start_time = $this->start_time;
        if ( !empty($start_time) && !empty($end_time) ) {
            if ($start_time >= $end_time) {
                $key = $attribute;
                $this->addError($key, Yii::t('backend', "please set the end time after the start time"));
            }
        }
    }
    
    public static function getListStore() {
        $company_id =  Util::getCookiesCompanyId();
        return MasterStore::find()->innerJoin('company', 'company.id = mst_store.company_id')->andFilterWhere(['mst_store.del_flg' => '0','company.id'=>$company_id])->orderBy(['id' => SORT_ASC])->all();
        //return MasterStore::find()->orderBy(['id' => SORT_ASC])->all();
    }
    
    public static function getListStoreID($store_id) {
        return MasterStore::find()->andWhere(['id'=> $store_id])->all();
    }
    
     /**
     * Generates slug using configured callback or increment of iteration.
     * @param string $baseSlug base slug value
     * @param integer $iteration iteration number
     * @return string new slug value
     * @throws \yii\base\InvalidConfigException
     */
    public static function getAllShift($storeId = null)
    {
      if($storeId!= null)
      return ArrayHelper::map(parent::find()->where([StoreShift::tableName().'.del_flg' => 0,'store_id'=>$storeId])->all(), 'id', 'name');
      
        return ArrayHelper::map(parent::find()->where([StoreShift::tableName().'.del_flg' => 0])->all(), 'id', 'name');
    }
    
}
