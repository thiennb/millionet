<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\MasterTypeSeat;
use common\components\Util;
use yii\helpers\ArrayHelper;

/**
 * MasterTypeSeatSearch represents the model behind the search form about `common\models\MasterTypeSeat`.
 */
class MasterTypeSeatSearch extends MasterTypeSeat {
    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['id', 'created_at', 'updated_at'], 'integer'],
            [['name', 'introduce', 'image1', 'image2', 'image3', 'show_flg', 'del_flg', 'created_by', 'updated_by', 'store_id'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = MasterTypeSeat::find()
                ->innerJoin('mst_store', 'mst_store.id = mst_type_seat.store_id')
                // Join : Company store
                ->innerJoin('company', 'company.id = mst_store.company_id');
        $company_id = Util::getCookiesCompanyId();
        $query->andFilterWhere([
            'company.id' => $company_id,
        ]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => false,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'mst_type_seat.store_id' => $this->store_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'mst_type_seat.name', $this->name])
                ->andFilterWhere(['like', 'introduce', $this->introduce])
                ->andFilterWhere(['like', 'image1', $this->image1])
                ->andFilterWhere(['like', 'image2', $this->image2])
                ->andFilterWhere(['like', 'image3', $this->image3])
                ->andFilterWhere(['like', 'show_flg', $this->show_flg])
                ->andFilterWhere(['like', 'del_flg', $this->del_flg])
                ->andFilterWhere(['like', 'created_at', $this->created_at])
                ->andFilterWhere(['like', 'updated_at', $this->updated_at]);

        $query->orderBy('name ASC');

        return $dataProvider;
    }
}
