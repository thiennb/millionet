<?php
namespace common\models;

use Yii;
use yii\base\Model;
use common\models\User;
use yii\behaviors\TimestampBehavior;
use DateTime;

/**
 * Login form
 */
class LoginForm extends Model
{
    public $username;
    public $password;
    public $rememberMe = true;

    private $_user;

    public $phone_1;
    public $phone_2;
    public $phone_3;
    
    public $email;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // username and password are both required
            [['phone_1','phone_2','phone_3','password'], 'required','on' => 'login_phone'],
            [['email','password'], 'required','on' => 'login_email'],
            [['email'], 'string','max'=>60],
            [['email'], 'checkEmail'],
            // rememberMe must be a boolean value
            ['rememberMe', 'boolean'],
            // password is validated by validatePassword()
            ['password', 'validatePassword'],
            ['password', 'string','min'=>6, 'max'=>20],
            [['phone_1','phone_2','phone_3'], 'integer'],
            [['phone_1'], 'string', 'min' => 2, 'max' => 3],
            [['phone_2', 'phone_3'], 'string', 'min' => 4, 'max' => 4],
        ];
    }
    
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'username' => Yii::t('backend', 'User login'),
            'password' => Yii::t('backend', 'Password'),
            'rememberMe' => Yii::t('backend', 'Save Login Information'),
        ];
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = $this->getPhone();
            if (!$user || !$user->validatePassword($this->password)) {
                $this->addError($attribute, Yii::t('backend', ''));
                $this->addError('phone_1', Yii::t('backend', ''));
                $this->addError('phone_2', Yii::t('backend', ''));
                $this->addError('phone_3', Yii::t('backend', ''));
                Yii::$app->session->setFlash('error', [
                    'error'=>Yii::t('frontend',"Phone number or password is incorrect.")
                ]);
            }
        }
    }

    /**
     * Logs in a user using the provided username and password.
     *
     * @return boolean whether the user is logged in successfully
     */
    public function login()
    {
        if ($this->validate()) {
            return Yii::$app->user->login($this->getUser(), $this->rememberMe ? 3600 * 24 * 30 : 0);
        } else {
            return false;
        }
    }

    /**
     * Finds user by [[username]]
     *
     * @return User|null
     */
    protected function getUser()
    {
        if ($this->_user === null) {
            $this->_user = User::findByUsername($this->username);
        }

        return $this->_user;
    }

    /**
     * Logs in a user using the provided username and password.
     *
     * @return boolean whether the user is logged in successfully
     */
    public function loginPhone()
    {
        $user = $this->getPhone();
        if (empty($user)) {
            Yii::$app->session->setFlash('error', [
                'error'=>Yii::t('frontend',"Phone number or password is incorrect.")
            ]);
            $this->addErrorIdPassword();
            return false;
        }
        if($user->time_fail >= 10 && $this->countTimeLoginFail($user)){
            Yii::$app->session->setFlash('error', [
                'error'=>Yii::t('backend',"You login faile 10 time. Please login affter 5 minute.")
            ]);
            $this->addErrorIdPassword();
            return false;
        }
        if ($this->validate()) {
            Yii::$app->session->set('store_id_rg', empty($user->register_store_id)?1:$user->register_store_id);
            return Yii::$app->user->login($user, $this->rememberMe ? 3600 * 24 * 30 : 0);
        } else {
            $this->countLoginFail($user);
            return false;
        }
    }

    /**
     * Logs in a user using the provided username and password.
     *
     * @return boolean whether the user is logged in successfully
     */
    public function loginEmail()
    {
        $user = $this->getEmail();
        if (empty($user)) {
            Yii::$app->session->setFlash('error', [
                'error'=>Yii::t('frontend',"Email or password is incorrect.")
            ]);
            return false;
        }
        if($user->time_fail >= 10 && $this->countTimeLoginFail($user)){
            Yii::$app->session->setFlash('error', [
                'error'=>Yii::t('backend',"You login faile 10 time. Please login affter 5 minute.")
            ]);
            return false;
        }
        if ($this->validate()) {
            Yii::$app->session->set('store_id_rg', empty($user->register_store_id)?1:$user->register_store_id);
            return Yii::$app->user->login($user, $this->rememberMe ? 3600 * 24 * 30 : 0);
        } else {
            $this->countLoginFail($user);
            return false;
        }
    }

    /**
     * Finds user by [[username]]
     *
     * @return User|null
     */
    protected function addErrorIdPassword()
    {
        $this->addError('password', Yii::t('backend', ''));
        $this->addError('phone_1', Yii::t('backend', ''));
        $this->addError('phone_2', Yii::t('backend', ''));
        $this->addError('phone_3', Yii::t('backend', ''));
    }

    /**
     * Finds user by [[username]]
     *
     * @return User|null
     */
    protected function getPhone()
    {
        $phone = $this->phone_1.$this->phone_2.$this->phone_3;
        if ($this->_user === null) {
            $this->_user = User::findByPhone($phone);
        }

        return $this->_user;
    }    

    /**
     * Finds user by [[username]]
     *
     * @return User|null
     */
    protected function getEmail()
    {
        if ($this->_user === null) {
            $this->_user = User::findByEmail($this->email);
        }

        return $this->_user;
    }    

    /**
     * Count login fail
     *
     * @return User|null
     */
    protected function countLoginFail($user = null)
    {
        if (!empty($user)) {
            empty($user->time_fail)?$user->time_fail = 1: $user->time_fail++;
            $user->save();
        }
    }    

    /**
     * Count login fail
     *
     * @return User|null
     */
    protected function countTimeLoginFail($user = null)
    {
        $now = DateTime::createFromFormat('Y-m-d H:i:s',date('Y-m-d H:i:s'));
        if (!empty($user) && ($now->getTimestamp() - $user->updated_at) <=300) {
            return true;
        }
        $user->time_fail = 0;
        $user->save();
        return false;
    }
    /**
     * Check email
     * @param string $attribute, $params
     * @return error 
     * @throws 
     */
    public function checkEmail($attribute, $params) {

        $check = preg_match(
           '/^[A-z]+[.A-z0-9_\-]+[@][A-z0-9_\-]+([.][A-z0-9_\-]+)+[A-z.]{2,4}$/', $this->email
        );

        //check valid email
        if (!$check) {
            $key = $attribute;
            //add message error
            $this->addError($key, Yii::t('backend', "email not valid"));
        }
        
    }
}
