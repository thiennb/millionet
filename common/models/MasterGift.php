<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii2tech\ar\softdelete\SoftDeleteBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the model class for table "mst_gift".
 *
 * @property integer $id
 * @property string $gift_code
 * @property string $gift_date
 * @property string $del_flg
 * @property integer $created_at
 * @property integer $updated_at
 */
class MasterGift extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'gift';
    }
    public $check_list;
    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            TimestampBehavior::className(),
            BlameableBehavior::className(),
            'softDeleteBehavior' => [
                'class' => SoftDeleteBehavior::className(),
                'softDeleteAttributeValues' => [
                    'del_flg' => '1'
                ],
            ],
        ];
    }

    public function rules() {
        return [
            [['gift_date', 'check_list'], 'safe'],
            [['total_quantity', 'order_id', 'customer_id', 'created_at', 'created_by', 'updated_at', 'updated_by'], 'integer'],
            [['gift_code'], 'required'],
            [['gift_code'], 'string', 'max' => 20],
            [['del_flg'], 'string', 'max' => 1],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => Yii::t('backend', 'ID'),
            'gift_code' => Yii::t('backend', 'Gift Code'),
            'gift_date' => Yii::t('backend', 'Date'),
            'total_quantity' => Yii::t('backend', 'Total Quantity'),
            'order_id' => Yii::t('backend', 'Order ID'),
            'customer_id' => Yii::t('app', 'Customer ID'),
            'del_flg' => Yii::t('backend', 'Del Flg'),
            'created_at' => Yii::t('backend', 'Created At'),
            'created_by' => Yii::t('backend', 'Created By'),
            'updated_at' => Yii::t('backend', 'Updated At'),
            'updated_by' => Yii::t('backend', 'Updated By'),
            'customer_name' => Yii::t('backend', 'Customer Name'),
            'customer_send_name' => Yii::t('backend', 'People Send'),
            'gift_date_from' => Yii::t('backend', 'Date'),
            'gift_date_to' => Yii::t('backend', 'Date'),
            'store_id' => Yii::t('backend', 'Store Id'),
            'product_name' => Yii::t('backend', 'Product name'),
            'quantity_receive_quantity_total' => Yii::t('backend', 'quantity receive quantity total'),
            
        ];
    }

    public function getMasterGiftDetail() {
        return $this->hasOne(MasterGiftDetails::className(), ['gift_id' => 'id']);
    }
    
    public function getMasterGiftDetails(){
        return $this->hasMany(MasterGiftDetails::className(), ['gift_id' => 'id']);
    }
    
    public function getMasterGiftReceivers(){
        return $this->hasMany(MasterGiftReceiver::className(), ['gift_id' => 'id']);
    }

    public function getMasterCustomer() {
        return $this->hasOne(MasterCustomer::className(), ['id' => 'customer_id']);
    }

    public static function find() {
        return parent::find()->where((new \common\components\FindPermission)->getPermissionCondition(self::tableName()));
    }

    public function insertGiftFromOrder($order_id) {
        if (empty($oldGift = MasterGift::findOne(['order_id' => $order_id]))) {
            $newGift = new MasterGift();
            $modelOrder = new MstOrder();
            $order = $modelOrder->find()->andWhere(['id' => $order_id])->one();
            // Create gift code
            $newGift->gift_code = MasterGift::createGiftCode(null);
            $store_id = MasterStore::find()->andwhere(['store_code' => substr($order->order_code, 0, 5)])->one();
            $newGift->store_id = is_null($store_id) ? null : $store_id->id;
            $newGift->gift_date = $order->process_date;
            $newGift->order_id = $order_id;
            $newGift->order_code = $order->order_code;
            $newGift->customer_id = isset($order->masterCustomer) ? $order->masterCustomer->id : null;
            $newGift->save();
            return $newGift->id;
        }
        return $oldGift->id;
    }
    
    public function updateTotalQuantity($id, $total) {
        $gift = MasterGift::findOne(['id' => $id]);
        $gift->total_quantity = $total;
        $gift->save();
    }
    
    public function checkDelete($id) {
        $check = 0;
        $check += MasterGiftDetails::find()->andWhere(['gift_id' => $id])->count();
        $check += MasterGiftReceiver::find()->andWhere(['gift_id' => $id])->count();
        return ($check > 0) ? FALSE : TRUE;
    }
    
    /**
     * Get order by has one relation
     * @return order
     */
    public function getOrder(){
        return $this->hasOne(MstOrder::className(), ['order_code' => 'order_code']);
    }
    
    /**
     * Create gift code
     * @param type $company
     * @return string
     */
    public static function createGiftCode($company = null){
        if(empty($company)){
            return 'CHUA XAC DINH';
        }
        
        // Get last gift
        $lastGift = $this->find()->orderBy(['created_at' => SORT_DESC])->one();
        
        if($lastGift){
            $num = (int)substr($lastGift->gift_code, 4);
        } else {
            $num = 0;
        }
        
        $companyPrefix = substr($company->company_code, 0, 4);
        
        return $companyPrefix;
    }
}
