<?php

namespace common\models;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use yii2tech\ar\softdelete\SoftDeleteBehavior;
use Yii;

/**
 * This is the model class for table "company_store".
 *
 * @property integer $id
 * @property integer $store_id
 * @property string $company_code
 * @property string $del_flg
 * @property integer $created_at
 * @property integer $created_by
 * @property integer $updated_at
 * @property integer $updated_by
 */
class CompanyStore extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'company_store';
    }
    
    public function behaviors() {
      return [
        TimestampBehavior::className(),
        BlameableBehavior::className(),
        'softDeleteBehavior' => [
          'class' => SoftDeleteBehavior::className(),
          'softDeleteAttributeValues' => [
            'del_flg' => true
          ],
        ]
      ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['store_id', 'company_code'], 'required'],
            [['store_id', 'created_at', 'created_by', 'updated_at', 'updated_by'], 'integer'],
            [['company_code'], 'string', 'max' => 20],
            [['del_flg'], 'string', 'max' => 1],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('frontend', 'ID'),
            'store_id' => Yii::t('frontend', 'Store ID'),
            'company_code' => Yii::t('frontend', 'Company Code'),
            'del_flg' => Yii::t('frontend', 'Del Flg'),
            'created_at' => Yii::t('frontend', 'Created At'),
            'created_by' => Yii::t('frontend', 'Created By'),
            'updated_at' => Yii::t('frontend', 'Updated At'),
            'updated_by' => Yii::t('frontend', 'Updated By'),
        ];
    }
    
    public static function findFrontEnd ($store_id = null) {
        return parent::find()->where(['or', ['del_flg' => '0'], ['del_flg' => null]]);
    }
}
