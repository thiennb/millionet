<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "booking".
 *
 * @property integer $id
 * @property integer $coupon_id
 * @property integer $customer_id
 * @property integer $shop_id
 * @property string $memo
 * @property string $memo2
 * @property string $memo3
 * @property integer $staff_id
 * @property string $fax
 * @property string $memo1
 * @property string $start_time
 * @property string $end_time
 * @property integer $seat_id
 * @property integer $menu_coupon_id
 * @property integer $status
 * @property integer $action
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $booking_date
 * @property string $demand
 * @property integer $black_list
 * @property string $del_flg
 */
class MasterBookingOrder extends \yii\db\ActiveRecord
{
    // Category Product
    public $category;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'booking';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'customer_id', 'store_id', 'created_at', 'updated_at'], 'required'],
            [['id', 'customer_id', 'shop_id', 'staff_id', 'seat_id', 'menu_coupon_id', 'status', 'action', 'created_at', 'updated_at', 'black_list'], 'integer'],
            [['booking_date','category'], 'safe'],
            [['memo', 'memo2', 'memo3', 'memo1'], 'string', 'max' => 60],
            [['fax'], 'string', 'max' => 12],
            [['start_time', 'end_time'], 'string', 'max' => 5],
            [['demand'], 'string', 'max' => 300],
            [['del_flg'], 'string', 'max' => 1],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('frontend', 'ID'),
            'customer_id' => Yii::t('frontend', 'Customer ID'),
            'store_id' => Yii::t('frontend', 'Shop ID'),
            'memo' => Yii::t('frontend', 'Memo'),
            'memo2' => Yii::t('frontend', 'Memo2'),
            'memo3' => Yii::t('frontend', 'Memo3'),
            'staff_id' => Yii::t('frontend', 'Staff ID'),
            'fax' => Yii::t('frontend', 'Fax'),
            'memo1' => Yii::t('frontend', 'Memo1'),
            'start_time' => Yii::t('frontend', 'Start Time'),
            'end_time' => Yii::t('frontend', 'End Time'),
            'seat_id' => Yii::t('frontend', 'Seat ID'),
            'menu_coupon_id' => Yii::t('frontend', 'Menu Coupon ID'),
            'status' => Yii::t('frontend', 'Status'),
            'action' => Yii::t('frontend', 'Action'),
            'created_at' => Yii::t('frontend', 'Created At'),
            'updated_at' => Yii::t('frontend', 'Updated At'),
            'booking_date' => Yii::t('frontend', 'Booking Date'),
            'demand' => Yii::t('frontend', 'Demand'),
            'black_list' => Yii::t('frontend', 'Black List'),
            'del_flg' => Yii::t('frontend', 'Del Flg'),
        ];
    }

	// generate booking code
	public function randBookingCode () {
		$booking_code = (int) MasterCustomer::find(false)->select('customer_jan_code')->max('customer_jan_code');

		$booking_code = $booking_code + 1;

		$booking_code = str_pad($booking_code, 10, '0', STR_PAD_LEFT);

		$this->booking_code = $booking_code;
	}
}
