<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "booking".
 *
 * @property integer $id
 * @property integer $coupon_id
 * @property integer $customer_id
 * @property integer $shop_id
 * @property string $memo
 * @property string $memo2
 * @property string $memo3
 * @property integer $staff_id
 * @property string $fax
 * @property string $memo1
 * @property string $start_time
 * @property string $end_time
 * @property integer $seat_id
 * @property integer $menu_coupon_id
 * @property integer $status
 * @property integer $action
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $booking_date
 * @property string $demand
 * @property integer $black_list
 * @property string $del_flg
 */
class MasterBookingHistory extends \yii\db\ActiveRecord {

    // Booking Date
    public $booking_date_from;
    public $booking_date_to;
    // Category Product
    public $category;
    // Name Product
    public $name_product;
    // Car Number Customer
    public $customer_jan_code;
    // Name Customer
    public $name_customer;
    // Sex Customer
    public $sex;
    // Rank Customer (Waiting QA)
    public $rank_customer;
    // Visit Customer
    public $visit;
    public $number_visit_below;
    public $number_visit_above;
    // Birth Date
    public $birth_date_from;
    public $birth_date_to;
    // First Visit
    public $first_visit_date_from;
    public $first_visit_date_to;
    // Chairs (Waiting QA)
    public $chairs;

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'booking';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['coupon_id', 'customer_id', 'shop_id'], 'required'],
            [['coupon_id', 'customer_id', 'shop_id', 'staff_id', 'seat_id', 'menu_coupon_id', 'status', 'action', 'created_at', 'updated_at', 'black_list'], 'integer'],
            [['booking_date', 'booking_date_from', 'booking_date_to', 'category', 'name_product', 'customer_jan_code', 'name_customer', 'sex', 'rank_customer', 'number_visit_below', 'number_visit_above', 'birth_date_from', 'birth_date_to', 'first_visit_date',
            'first_visit_date_from', 'first_visit_date_to', 'chairs'], 'safe'],
            [['memo', 'memo2', 'memo3', 'memo1'], 'string', 'max' => 60],
            [['fax'], 'string', 'max' => 12],
            [['start_time', 'end_time'], 'string', 'max' => 5],
            [['demand'], 'string', 'max' => 300],
            [['del_flg'], 'string', 'max' => 1],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
        'id' => Yii::t('backend', 'ID'),
        'coupon_id' => Yii::t('backend', 'Coupon ID'),
        'customer_id' => Yii::t('backend', 'Customer ID'),
        'shop_id' => Yii::t('backend', 'Shop ID'),
        'memo' => Yii::t('backend', 'Memo'),
        'memo2' => Yii::t('backend', 'Memo2'),
        'memo3' => Yii::t('backend', 'Memo3'),
        'staff_id' => Yii::t('backend', 'Staff ID'),
        'fax' => Yii::t('backend', 'Fax'),
        'memo1' => Yii::t('backend', 'Memo1'),
        'start_time' => Yii::t('backend', 'Start Time'),
        'end_time' => Yii::t('backend', 'End Time'),
        'seat_id' => Yii::t('backend', 'Seat ID'),
        'menu_coupon_id' => Yii::t('backend', 'Menu Coupon ID'),
        'status' => Yii::t('backend', 'Booking status'),
        'action' => Yii::t('backend', 'Action'),
        'created_at' => Yii::t('backend', 'Created At'),
        'updated_at' => Yii::t('backend', 'Updated At'),
        'booking_date' => Yii::t('backend', 'Booking Date'),
        'demand' => Yii::t('backend', 'Demand'),
        'black_list' => Yii::t('backend', 'Black List'),
        'del_flg' => Yii::t('backend', 'Del Flg'),
        'booking_date_from' => Yii::t('backend', 'Booking Date From'),
        'name_product' => Yii::t('backend', 'Name Product'),
        'category' => Yii::t('backend', 'Category'),
        'customer_jan_code' => Yii::t('backend', 'Membership Card Number'),
        'name_customer' => Yii::t('backend', 'Name Customer'),
        'sex' => Yii::t('backend', 'Sex'),
        'rank_customer' => Yii::t('backend', 'Rank Customer'),
        'number_visit_below' => Yii::t('backend', 'Number Visit Below'),
        'number_visit_above' => Yii::t('backend', 'Number Visit Above'),
        'birth_date_from' => Yii::t('backend', 'Birth Date From'),
        'birth_date_to' => Yii::t('backend', 'Birth Date To'),
        'first_visit_date' => Yii::t('backend', 'Birth Date'),
        'first_visit_date_from' => Yii::t('backend', 'Birth Date From'),
        'first_visit_date_to' => Yii::t('backend', 'Birth Date To'),
        'chairs' => Yii::t('backend', 'Chairs'),
        'booking_date_from' => Yii::t('backend', 'Booking Date'),
        'booking_date_to' => Yii::t('backend', 'Booking Date'),
        'store_id' => Yii::t('backend', 'Booking Store'),
        'customer_jan_code' => Yii::t('backend', 'Code Membership'),
        'customer_name' => Yii::t('backend', 'Name Customer'),
        'rank' => Yii::t('backend', 'Rank Customer'),
        'number_visit_min' => Yii::t('backend', 'Visit number'),
        'number_visit_max' => Yii::t('backend', 'Visit number'),
        'birth_date_from' => Yii::t("backend", "Birthday"),
        'birth_date_to' => Yii::t("backend", "Birthday"),
        'history_date_from' => Yii::t('backend', 'Process Date'),
        'history_date_to' => Yii::t('backend', 'Process Date'),
        'product_name' => Yii::t('backend', 'Name Product Booking'),
        'list_catogory' => Yii::t('backend', 'Category'),
        'last_visit_date_from' => Yii::t('backend', 'Last visit time'),
        'last_visit_date_to' => Yii::t('backend', 'Last visit time'),
        'last_staff_id' => Yii::t('backend', 'Last time staff'),
        'booking_method' => Yii::t('backend', 'Booking Method'),
        'list_category' => Yii::t('backend', 'Product Category'),
        'customer_first_name' => Yii::t('backend', 'First name'),
        'customer_last_name' => Yii::t('backend', 'Last name'),
        'mobile' => Yii::t('backend', 'Phone'),
        'booking_time_min' => Yii::t('backend', 'Booking Times'),
        'booking_time_max' => Yii::t('backend', 'Booking Times'),
        'last_seat_id' => Yii::t('backend', 'Last Seat'),
        'service_staff_id' => Yii::t('backend', 'Service staff'),
        'seat_type' => Yii::t('backend', 'Type Seat Name'),
        ];
    }

    public function getCustomerMaster() {
        return $this->hasOne(MasterCustomer::className(), ['id' => 'customer_id'])->with('customer');
    }

    public function getStoreMaster() {
        return $this->hasOne(MasterStore::className(), ['id' => 'store_id']);
    }

    public function getBookingProduct() {
        return $this->hasMany(BookingProduct::className(), ['booking_id' => 'id'])->with('product');
    }
    
    public function getBookingCoupon() {
        return $this->hasMany(BookingCoupon::className(), ['booking_id' => 'id'])->with('coupon');
    }
    
    public function getStaff()
    {
        return $this->hasOne(MasterStaff::className(), ['id' => 'staff_id']);
    }
    
    public function getSeat()
    {
        return $this->hasOne(MasterSeat::className(), ['id' => 'seat_id']);
    }
    
    public static function find() {
        return parent::find()->where(['or', [MasterBookingHistory::tableName() . '.del_flg' => '0'], [MasterBookingHistory::tableName() . '.del_flg' => null]]);
    }

}
