<?php

namespace common\models;

use Yii;
use Carbon\Carbon;
use common\models\MasterSeat;
use yii\helpers\ArrayHelper;
/**
 * This is the model class for table "seat_schedule_detail".
 *
 * @property integer $id
 * @property integer $seat_id
 * @property string $booking_date
 * @property string $booking_time
 * @property string $booking_status
 * @property integer $store_id
 * @property string $booking_code
 */
class SeatScheduleDetail extends \yii\db\ActiveRecord
{
    const BOOKING_STATUS_FREE = '1';
    const BOOKING_STATUS_BUSY = '2';
    const BOOKING_STATUS_OFF  = '3';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'seat_schedule_detail';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['seat_id', 'store_id'], 'integer'],
            [['booking_date', 'booking_time'], 'safe'],
            [['store_id'], 'required'],
            [['booking_status'], 'string', 'max' => 1],
            [['booking_code'], 'string', 'max' => 10],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend', 'ID'),
            'seat_id' => Yii::t('backend', 'Seat ID'),
            'booking_date' => Yii::t('backend', 'Booking Date'),
            'booking_time' => Yii::t('backend', 'Booking Time'),
            'booking_status' => Yii::t('backend', 'Booking Status'),
            'store_id' => Yii::t('backend', 'Store ID'),
            'booking_code' => Yii::t('backend', 'Booking Code'),
        ];
    }
    
    /**
     * check if booking seat is valid.
     * @param int $seat_id 
     * @param string $time_booking
     * @param int $time_execute number minute
     * @param string $start_date
     * @param string $booking_code
     * @return bool
     */
    public static function checkBookingSeatIsValid($seat_id,$time_booking,$time_execute,$start_date ,$booking_code = null) {
        
        $time_end     = Carbon::parse( $time_booking)->addMinute($time_execute)->format('H:i:s');
        
        //if time booking is past return false
        if(Carbon::parse($start_date.' '. $time_booking)->isPast()  )
            return 'false';

//          \common\components\Util::dd($booking_code);
        $seatSchedule = (new \yii\db\Query())
                        ->select('*')
                        ->from('seat_schedule_detail as t')
                        ->where(['or',['t.booking_status'=>  self::BOOKING_STATUS_BUSY],
                                      ['and',['t.booking_status'=> self::BOOKING_STATUS_BUSY],['<>','t.booking_code',$booking_code]]
                                ])
//                        ->andWhere(['t.seat_id'=>$seat_id])
//                        ->andWhere(['between','t.booking_time',$time_booking ,$time_end ])
                        ->andWhere(['t.booking_date'=>$start_date])
                        ->all();
        \common\components\Util::dd($seatSchedule);
        //if not have row  mean don't have booking in that time . Return true
        return  count($seatSchedule) == 0 ? "true" : "false";
    }
    
    /**
     * check if booking seat is valid.
     * @param int $seat_id 
     * @param string $time_booking
     * @param int $time_execute number minute
     * @param string $start_date
     * @param string $booking_code
     * @return bool
     */
    public static function checkBookingSeatTypeIsValid($storeId,$seat_type_id,$numberPeople,$noSmoking,$time_booking,$time_execute,$start_date ,$booking_code = null) {
        if($time_execute > 30 ){
            $time_end     = Carbon::parse( $time_booking)->addMinute($time_execute)->subMinute(30)->format('H:i:s');
        }else{
            $time_end     = Carbon::parse( $time_booking)->addMinute($time_execute)->format('H:i:s');
        }
        
        //if time booking is past return false
        if(Carbon::parse($start_date.' '. $time_booking)->isPast()  )
            return 'false';

        
        $seatBusySchedule = (new \yii\db\Query())
                            ->select('t.booking_time,t.seat_id,t.booking_date,t.booking_time,t.booking_code,mst_seat.*')
                            ->from('seat_schedule_detail as t')
                            ->innerJoin('mst_seat', 't.seat_id = mst_seat.id')
                            ->where(
                                          ['and',['t.booking_status'=> self::BOOKING_STATUS_BUSY]]
                                    )
                            ->andFilterWhere(['<>','t.booking_code',$booking_code])
                            ->andWhere(['mst_seat.type_seat_id'=>$seat_type_id])
                            ->andWhere(['mst_seat.store_id'=>$storeId])
    //                        ->andWhere(['<=','mst_seat.capacity_min',$numberPeople])
    //                        ->andWhere(['>=','mst_seat.capacity_max',$numberPeople])
                            ->andWhere(['between','t.booking_time',$time_booking ,$time_end ])
                            ->andWhere(['t.booking_date'=>$start_date])
                            ->orderBy('mst_seat.seat_code ,mst_seat.capacity_max')                        
                            ->all();
        $arrayResult = array_keys(ArrayHelper::index($seatBusySchedule, 'seat_id'));
        $seatFreeSchedule = MasterSeat::find()
                            ->andWhere(['NOT IN','id',$arrayResult])
                            ->andWhere(['store_id'=>$storeId])
                            ->andWhere(['type_seat_id'=>$seat_type_id])
                            ->andWhere(['<=','mst_seat.capacity_min',$numberPeople])
                            ->andFilterWhere(['=','smoking_flg',$noSmoking])
//                            ->andWhere(['>=','mst_seat.capacity_max',$numberPeople])
                            ->orderBy('mst_seat.seat_code ,mst_seat.capacity_max')
                            ->asArray()
                            ->all();
        $result = self::genArraySeatBookingOrder($seatFreeSchedule);
        $result2= self::deepLevelGroupBookingSeat($result, 0 ,$numberPeople);
//        \common\components\Util::dd($result2);
        
        //if not have row  mean don't have booking in that time . Return true
//        return  count($seatSchedule) == 0 ? "true" : "false";
        return ['seatChoose'=>$result2,'seatFree'=>$seatFreeSchedule];
    }
    /**
     * gen array use for sort seat .
     * @param array $seatFreeSchedule 
     * @return array
     */
    public static function genArraySeatBookingOrder($seatFreeSchedule = null){
        $result = null;
        foreach ((array)$seatFreeSchedule as $seat) {
            $arr= [
                  'id'        => $seat['id'],
                  'seat_code' => $seat['seat_code'],
                  'capacity'  => $seat['capacity_max'],
                  'near_max'  => 0,
                  'near_level'=> 0
                  ];  
            $result[] = $arr;
        }
        return $result;
      
    }
    
    /**
     * gen array use for sort seat with deep lever is number seat can booking.
     * @param array $freeSeat 
     * @param int $deepLevel 
     * @param int $numberPeople 
     * @return array
     */
    public static function deepLevelGroupBookingSeat($freeSeat , $deepLevel , $numberPeople){
        $result = $freeSeat;
//        \common\components\Util::dd($freeSeat);
        //return null if don't have any seat
        if(empty($freeSeat)) return null;
        foreach ((array)$freeSeat as $index => $value) {
            for ($j = 0 ; $j<=$deepLevel ; $j++){
                $result[$index]['near_max'] += isset($freeSeat[$index+$j])?$freeSeat[$index+$j]['capacity']:0;
                $near_level   = isset($freeSeat[$index+$deepLevel]) ? (int)$freeSeat[$index+$deepLevel]['seat_code'] : 9999;
                $result[$index]['near_level'] = $near_level - (int)$freeSeat[$index]['seat_code'];
            }
            
        }
        
        $suitSeat = self::seachSeatSuitAble($result,$deepLevel,$numberPeople);
        if($suitSeat == null && $deepLevel<= count($freeSeat)){
           return self::deepLevelGroupBookingSeat($freeSeat, $deepLevel +1, $numberPeople);
        }
        
        return $suitSeat;
      
    }
    /**
     * seach in array seat sult able and return array seat can booking .
     * @param array $freeSeat 
     * @param int $deepLevel 
     * @param int $numberPeople 
     * @return array  0 => [
              'id' => 12
              'seat_code' => '002'
              'capacity' => 4
              'near_max' => 4
              'near_level' => 0
              'actual_load'=> 4
          ]
     */
    public static function seachSeatSuitAble($freeSeat,$deepLevel,$numberPeople ){
        $origin   = $freeSeat ;
        $reIndex  = ArrayHelper::index($origin, 'id');
        ArrayHelper::multisort($freeSeat, ['near_level', 'near_max'], [SORT_ASC, SORT_ASC]);
        if(!empty($freeSeat)){
        foreach ((array)$freeSeat as $index => $item){
            if($item['near_max'] >= $numberPeople){
                $key = array_search($item['id'], array_column($origin, 'id'));
                $result =  array_slice($origin, $key , $deepLevel+1 );
                $number =  $numberPeople;
                foreach ((array)$result as $key => $seat){
                    if($number >= $result[$key]['capacity'] &&$number>0 ){
                      $temp =  $result[$key]['capacity'];
                      $number -=  $result[$key]['capacity'];
                    }else{
                      $temp = $number;
                    }
                    $result[$key]['actual_load'] = $temp;
                    
                }
//              return array_slice($reIndex, 1 , $deepLevel+1 );
                return $result;
            }
        }
        }
        return null;
    }
  
    
  }
