<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use common\components\RegisterFromBehavior;
use yii2tech\ar\softdelete\SoftDeleteBehavior;
/**
 * This is the model class for table "booking_seat".
 *
 * @property integer $id
 * @property integer $booking_id
 * @property integer $seat_id
 * @property integer $actual_load
 * @property string $del_flg
 * @property integer $created_at
 * @property integer $created_by
 * @property integer $updated_at
 * @property integer $updated_by
 */
class BookingSeat extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'booking_seat';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['booking_id', 'seat_id'], 'required'],
            [['booking_id', 'seat_id', 'actual_load', 'created_at', 'created_by', 'updated_at', 'updated_by'], 'integer'],
            [['created_at', 'created_by', 'updated_at', 'updated_by'],'safe'],
            [['del_flg'], 'string', 'max' => 1],
        ];
    }
    public function behaviors() {
      return [
        TimestampBehavior::className(),
        BlameableBehavior::className(),
        //RegisterFromBehavior::className(),  
        'softDeleteBehavior' => [
          'class' => SoftDeleteBehavior::className(),
          'softDeleteAttributeValues' => [
            'del_flg' => true
          ],
        ]
      ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend', 'ID'),
            'booking_id' => Yii::t('backend', 'Booking ID'),
            'seat_id' => Yii::t('backend', 'Seat ID'),
            'actual_load' => Yii::t('backend', 'Actual Load'),
            'del_flg' => Yii::t('backend', 'Del Flg'),
            'created_at' => Yii::t('backend', 'Created At'),
            'created_by' => Yii::t('backend', 'Created By'),
            'updated_at' => Yii::t('backend', 'Updated At'),
            'updated_by' => Yii::t('backend', 'Updated By'),
        ];
    }
    
    public function getSeat()
    {
        return $this->hasOne(MasterSeat::className(), ['id' => 'seat_id']);
    }
    public static function find()
    {
            return parent::find(['or', [self::tableName() . '.del_flg' => '0'], [self::tableName() . '.del_flg' => null]]);
    }
    
    public static function findFrontEnd()
    {
            return parent::find(['or', [self::tableName() . '.del_flg' => '0'], [self::tableName() . '.del_flg' => null]]);
    }
}
