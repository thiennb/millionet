<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii2tech\ar\softdelete\SoftDeleteBehavior;
use yii\behaviors\BlameableBehavior;
use common\models\ProductOption;
use common\models\ProductCoupon;
use common\models\ProductTicket;
use common\models\BookingProduct;
use yii\helpers\ArrayHelper;
use common\components\Util;
use common\components\Constants;

/**
 * This is the model class for table "mst_product".
 *
 * @property integer $id
 * @property string $name
 * @property string $name_kana
 * @property string $name_order
 * @property string $name_bill
 * @property string $description
 * @property integer $category_id
 * @property string $jan_code
 * @property double $price
 * @property double $cost
 * @property string $image1
 * @property string $image2
 * @property string $image3
 * @property integer $tax_display_method
 * @property integer $tax_rate_id
 * @property string $show_flg
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $del_flg
 * @property string $time_require
 * * 
 * @property MstCategoriesProduct $category
 */
class MstProduct extends \yii\db\ActiveRecord {

    public $tmp_image1;
    public $tmp_image2;
    public $tmp_image3;
    public $rate;
    public $option;
    public $option_hidden;
    public $hidden_image1;
    public $hidden_image2;
    public $hidden_image3;
    public $category_name;
    public $id_hd;
    public $name_store;
    public $option_name;
    public $total_price;

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'mst_product';
    }

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            TimestampBehavior::className(),
            BlameableBehavior::className(),
            'softDeleteBehavior' => [
                'class' => SoftDeleteBehavior::className(),
                'softDeleteAttributeValues' => [
                    'del_flg' => '1'
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
                [['name', 'category_id', 'tax_display_method', 'unit_price'], 'required'],
                [['description'], 'string', 'max' => 500],
                [['category_id', 'tax_display_method', 'tax_rate_id', 'store_id'], 'integer', 'min' => 0],
                [['unit_price', 'cost', 'jan_code'], 'number', 'min' => 0],
                [['unit_price', 'cost', 'jan_code'], 'string', 'max' => 13],
                [['jan_code'], 'string', 'min' => 13],
//            [['jan_code'], 'unique',
//                'targetClass' => 'common\models\MstProduct',
//                'when' => function ($model, $attribute) {
//            return $model->{$attribute} !== $model->getOldAttribute($attribute);
//        },
//            ],
//            [['tax_rate_id'], 'required',
//                'when' => function ($model, $attribute) {
//                    return $model->tax_display_method !== '02';
//                },
//            ],
            [['tax_rate_id'], 'required'],
                ['jan_code', 'check_jan_code'],
                [['time_require'], 'integer', 'min' => 0, 'max' => 999],
            //[['time_require'], 'string', 'max' => 3],
            [['show_flg', 'del_flg'], 'string', 'max' => 1],
                [['name', 'name_order', 'name_bill'], 'string', 'max' => 100],
            //[['image1', 'image2', 'image3'], 'string', 'max' => 100],
            [['category_id'], 'exist', 'skipOnError' => true, 'targetClass' => MstCategoriesProduct::className(), 'targetAttribute' => ['category_id' => 'mst_product_category.id']],
                [['tmp_image1', 'tmp_image2', 'tmp_image3'], 'file', 'extensions' => 'png, jpg, gif, jpeg', 'skipOnEmpty' => true, 'maxSize' => 1024 * 1024 * 2],
                [['option', 'hidden_image1', 'hidden_image2', 'hidden_image3', 'category_name', 'assign_fee_flg', 'name_store'], 'safe'],
                [['id_hd', 'tel_booking_flg', 'option_condition_flg'], 'safe'],
                [['store_id'], 'required', 'when' => function() {
                    $role = \common\components\FindPermission::getRole();
                    return ($role->permission_id == Constants::STORE_MANAGER_ROLE || $role->permission_id == Constants::STAFF_ROLE);
                }],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => Yii::t('backend', 'ID'),
            'name' => Yii::t('backend', 'Product name'),
            'name_kana' => Yii::t('backend', 'Name kana'),
            'name_order' => Yii::t('backend', 'Name Order'),
            'name_bill' => Yii::t('backend', 'Name Bill'),
            'description' => Yii::t('backend', 'Description'),
            'category_id' => Yii::t('backend', 'Category'),
            'jan_code' => Yii::t('backend', 'Jan Code'),
            'unit_price' => Yii::t('backend', 'Price'),
            'cost' => Yii::t('backend', 'Cost'),
            'image1' => Yii::t('backend', 'Image1'),
            'image2' => Yii::t('backend', 'Image2'),
            'image3' => Yii::t('backend', 'Image3'),
            'tax_display_method' => Yii::t('backend', 'Tax Internal Foreign'),
            'tax_rate_id' => Yii::t('backend', 'Tax'),
            'show_flg' => Yii::t('backend', 'Show'),
            'created_at' => Yii::t('backend', 'Created At'),
            'updated_at' => Yii::t('backend', 'Updated At'),
            'del_flg' => Yii::t('backend', 'Del Flg'),
            'tmp_image1' => Yii::t('backend', 'Image 1'),
            'tmp_image2' => Yii::t('backend', 'Image 2'),
            'tmp_image3' => Yii::t('backend', 'Image 3'),
            'time_require' => Yii::t('backend', 'The time required'),
            'store_id' => Yii::t('backend', 'Store Id'),
            'category_name' => Yii::t('backend', 'Category'),
            'assign_fee_flg' => Yii::t('backend', 'Assign fee'),
            'tel_booking_flg' => Yii::t('backend', 'Tel booking flg'),
            'option_condition_flg' => Yii::t('backend', 'Option condition flg'),
            'name_store' => Yii::t('backend', 'Name Store'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory() {
        return $this->hasOne(MstCategoriesProduct::className(), ['id' => 'category_id']);
    }

    public function getMasterTax() {
        return $this->hasOne(MasterTax::className(), ['id' => 'tax_rate_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductOption() {
        return $this->hasMany(ProductOption::className(), ['product_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStore() {
        return $this->hasMany(MasterStore::className(), ['id' => 'store_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function saveOption() {
        ProductOption::updateAll(['del_flg' => '1', 'updated_at' => strtotime("now")], ['product_id' => $this->id]);
        foreach ($this->option as $val) {
            $product_option = new ProductOption();
            $product_option->product_id = $this->id;
            $product_option->option_id = $val;
            $product_option->save();
        }
    }

    public static function find($findAll = true) {
        if ($findAll)
            return parent::find()->where((new \common\components\FindPermission)->getPermissionCondition(self::tableName(), true));
        else
            return parent::find();
    }

    public static function findFrontEnd() {
        return parent::find(['or', [self::tableName() . '.del_flg' => '0'], [self::tableName() . '.del_flg' => null]]);
    }

    public function checkDelete($id) {
        $check = 0;
        $check = ProductCoupon::find()->andWhere(['product_id' => $id])->count();
        $check += ProductOption::find()->andWhere(['product_id' => $id])->count();
        $check += ProductTicket::find()->andWhere(['product_id' => $id])->count();
        $check += BookingProduct::find()->andWhere(['product_id' => $id])->count();
        $check += MasterStaff::find()->andWhere(['assign_product_id' => $id])->count();
        return ($check > 0) ? FALSE : TRUE;
    }

    public function getListProduct() {
        return ArrayHelper::map(self::find()->all(), 'id', 'name');
    }

    public function getListProductAll($listCategory = []) {
        $product_list = [];
        if (count($listCategory) > 0) {
            foreach ($listCategory as $key => $val) {
                $product_list[$key] = ArrayHelper::map(self::find()->andWhere(['category_id' => $key, 'del_flg' => '0'])->all(), 'id', 'name');
            }
        }
        return $product_list;
    }

    public function getListProductCheck($listCategory = []) {
        $product_list = [];
        if (count($listCategory) > 0) {
            foreach ($listCategory as $key => $val) {
                $product_list[$key] = ArrayHelper::map(self::find()->andWhere(['mst_product.category_id' => $key, 'mst_product.del_flg' => '0', 'mst_product.assign_fee_flg' => '0'])->all(), 'id', 'name');
            }
        }
        return $product_list;
    }

    public function getListProductCheckByStore($listCategory = [], $store) {
        $product_list = [];
        if (count($listCategory) > 0) {
            foreach ($listCategory as $key => $val) {
                $product_list[$key] = ArrayHelper::map(self::find()->andWhere(['mst_product.category_id' => $key, 'mst_product.del_flg' => '0', 'mst_product.assign_fee_flg' => '0', 'mst_product.store_id' => $store])->all(), 'id', 'name');
            }
        }
        return $product_list;
    }

    public function getListProductStoreAll($listCategory = [], $store_id = '') {
        $product_list = [];
        if (count($listCategory) > 0) {
            foreach ($listCategory as $key => $val) {
                $product_list[$key] = ArrayHelper::map(self::find()->andWhere(['category_id' => $key, 'del_flg' => '0', 'store_id' => $store_id])->all(), 'id', 'name');
            }
        }
        return $product_list;
    }

    // Datpdt 23/09/2016 Start
    // Get List Product : assign_fee_flg
    public function getListProductByAssignFeeFlg() {
        $resul = self::find()->select(['id', 'name'])->andWhere(['assign_fee_flg' => '1'])->all();

        return ArrayHelper::map($resul, 'id', 'name');
    }

    // Datpdt 23/09/2016 End

    public function getListProductByProductId($list_product) {
        $resul = self::find()->select(['id', 'name'])->andWhere(['id' => $list_product])->all();
        return ArrayHelper::map($resul, 'id', 'name');
    }

    /**
     * array list staff 
     */
    public static function listProductByStoreId($storeId = null) {
        if ($storeId != null) {
            return ArrayHelper::map(self::find()->andWhere(['store_id' => $storeId, 'assign_fee_flg' => '1'])->orderBy(['name' => SORT_ASC])->all(), 'id', 'name');
        }
        return ['' => ''] + ArrayHelper::map(self::find()->andWhere(['assign_fee_flg' => '1'])->orderBy(['name' => SORT_ASC])->all(), 'id', 'name');
    }

    /**
     * Check jan code
     * @param string $attribute, $params
     * @return error 
     * @throws 
     */
    public function check_jan_code($attribute, $params) {
        $check_jan_code = true;
        $check_jan_code_exist = true;
        if (!empty($this->$attribute)) {
            $check_jan_code = Util::checkJanCodeProduct(Constants::JAN_TYPE_PRODUCT, $this->$attribute);
            if (!$this->checkExistJanCodeByIdStore($this->store_id, $this->$attribute)) {
                $check_jan_code_exist = false;
            }
        }
        if (!$check_jan_code) {
            $this->addError($attribute, Yii::t('backend', "JAN_Code invalid, please try again."));
            //return view create
        }
        if (!$check_jan_code_exist) {
            $this->addError($attribute, Yii::t('backend', "{attribute} jan code exist", ['attribute' => $this->$attribute]));
            //return view create
        }
    }

    public static function getProductBybooking($booking_id) {
        $query = MstProduct::findFrontEnd()
                ->select([
                    MstProduct::tableName() . '.*',
                    'option_name' => MasterOption::tableName() . '.name',
                    'total_price' => BookingProduct::tableName() . '.unit_price'
                ])
                ->innerJoin(BookingProduct::tableName(), BookingProduct::tableName() . '.booking_id = :bookingId'
                        . ' and ' . BookingProduct::tableName() . '.product_id = ' . MstProduct::tableName() . '.id'
                        . ' and ' . BookingProduct::tableName() . '.del_flg = :zero'
                )
                ->leftJoin(MasterOption::tableName(), MasterOption::tableName() . '.id = ' . BookingProduct::tableName() . '.option_id');
        $query->addParams([
            'zero' => '0',
            'bookingId' => $booking_id
        ]);

        $query->orderBy([
            BookingProduct::tableName() . '.option_id' => SORT_DESC
        ]);

        return $query;
    }

    public function getCouponbyBooking($booking_id) {
        $query = MasterCoupon::find()
                ->select([
                    MasterCoupon::tableName() . '.*',
                    'total_price' => BookingCoupon::tableName() . '.unit_price'
                ])
                ->innerJoin(BookingCoupon::tableName(), BookingCoupon::tableName() . '.booking_id = :bookingId'
                . ' and ' . BookingCoupon::tableName() . '.coupon_id = ' . MasterCoupon::tableName() . '.id'
                . ' and ' . BookingCoupon::tableName() . '.del_flg = :zero');
        $query->addParams([
            'zero' => '0',
            'bookingId' => $booking_id
        ]);

        return $query;
    }

    public function getProductByJancode($jancode = null, $storeId = null) {
        if ($jancode !== null && $storeId !== null) {
            $product = (new \yii\db\Query())
                    ->select(['*'])
                    ->from('mst_product')
                    ->where(['jan_code' => $jancode])
                    ->andWhere(['store_id' => $storeId])
                    ->andWhere(['del_flg' => '0'])
                    ->one();

            return $product;
        }

        return null;
    }

    /**
     * Count Products model effect based on its primary key value.
     * @param integer $id
     * @return count
     */
    static function getCountProductsExists($id) {
        $command = Yii::$app->getDb()->createCommand(
                "SELECT
                    COUNT(*)
                FROM
                (
                    SELECT
                        mst_product.del_flg
                    FROM
                        mst_product
                    WHERE
                        mst_product.id IN (" . $id . ")
                        AND
                        mst_product.del_flg = '0'
                )
                ct"
        );

        if (preg_match('/^\d+(,\d+){0,}$/', $id)) {
            $result = $command->queryAll();
            return (int) $result[0]['count'];
        } else {
            return 0;
        }
    }

    /**
     * Count Products model exists based on its primary key value.
     * @param integer $id_option
     * @return count
     */
    static function getCountOption($option) {

        $id_option = null;
        $bl_check = true;

        foreach ($option as $val) {
            if ($bl_check == true) {
                $id_option = $val;
                $bl_check = false;
            } else {
                $id_option .= ',' . $val;
            }
        }

        $command = Yii::$app->getDb()->createCommand(
                "SELECT
                    COUNT(*)
                FROM
                (
                    SELECT
                        mst_option.del_flg
                    FROM
                        mst_option
                    WHERE
                        mst_option.id IN (" . $id_option . ")
                        AND
                        mst_option.del_flg = '0'
                )
                ct"
        );

        if (preg_match('/^\d+(,\d+){0,}$/', $id_option)) {
            $result = $command->queryAll();
            return (int) $result[0]['count'];
        } else {
            return 0;
        }
    }

    /*
     * Check jan code exist when have general store id
     * If count jan code > 1 return false
     * @param integer $store_id
     * @param interger $jan_code_product
     * @return true
     */

    protected function checkExistJanCodeByIdStore($store_id, $jan_code_product) {
        
        $company_id = Util::getCookiesCompanyId();
        $url_referrer = substr(strrchr(Yii::$app->request->referrer, "/"), 1); 
        $check_jan_code = 0;
        $query = self::find()->innerJoin('mst_store', 'mst_store.id = mst_product.store_id')->innerJoin('company', 'company.id = mst_store.company_id')->andFilterWhere(['company.del_flg' => '0','mst_store.del_flg' => '0', 'company.id' => $company_id])->andWhere(['jan_code' => $jan_code_product]);
        $query_store = MasterStore::findStoreCommon('00000');
        $query_common = self::find()->innerJoin('mst_store', 'mst_store.id = mst_product.store_id')->innerJoin('company', 'company.id = mst_store.company_id')->andFilterWhere(['company.del_flg' => '0','mst_store.del_flg' => '0', 'company.id' => $company_id])->andWhere(['mst_store.store_code' => '00000']);
        // Case : Update
        if (!empty($query_store->id)) {
            $store_id_sl = $query_store->id;
        } else {
            $store_id_sl = null;
        }

        if ($url_referrer != 'create') {
            $id_update = substr(strrchr($url_referrer, "="), 1);

            if (empty($store_id)) {
                $count_jan_code_store_comon = $query_common->andWhere(['jan_code' => $jan_code_product])->andFilterWhere(['<>', 'mst_product.store_id', $store_id_sl])->count();
                $check_jan_code = $query->andWhere(['<>', 'mst_product.id', $id_update])->count();
            } else {
                $count_jan_code = $query->andWhere(['mst_product.store_id' => $store_id])->andWhere(['<>', 'mst_product.id', $id_update])->count();
            }

            if (empty($store_id)) {
                if ($check_jan_code > 0) {
                    return false;
                }
                if ($count_jan_code_store_comon > 0) {
                    return false;
                } else {
                    return true;
                }
            } else {
                if ($store_id_sl) {
                    $jan_code_store_common = self::find()->innerJoin('mst_store', 'mst_store.id = mst_product.store_id')->innerJoin('company', 'company.id = mst_store.company_id')->andFilterWhere(['company.del_flg' => '0','mst_store.del_flg' => '0', 'company.id' => $company_id])->andFilterWhere(['mst_product.store_id' => $store_id_sl])->andWhere(['mst_product.jan_code' => $jan_code_product])->count();
                    if ($jan_code_store_common > 0) {
                        return false;
                    }
                }
                if ($count_jan_code > 0) {
                    return false;
                } else {
                    return true;
                }
            }
        } else {
            // Case : Create   
            $check_jan_code = 0;

            if (empty($store_id)) {
                $count_jan_code = self::find()->innerJoin('mst_store', 'mst_store.id = mst_product.store_id')->innerJoin('company', 'company.id = mst_store.company_id')->andFilterWhere(['company.del_flg' => '0','mst_store.del_flg' => '0', 'company.id' => $company_id])->andFilterWhere(['mst_product.store_id' => $store_id_sl])->andWhere(['mst_product.jan_code' => $jan_code_product])->count();
                $check_jan_code = self::find()->innerJoin('mst_store', 'mst_store.id = mst_product.store_id')->innerJoin('company', 'company.id = mst_store.company_id')->andFilterWhere(['company.del_flg' => '0','mst_store.del_flg' => '0', 'company.id' => $company_id])->andWhere(['mst_product.jan_code' => $jan_code_product])->count();
            } else {
                $count_jan_code = self::find()->andWhere(['mst_product.store_id' => $store_id])->andWhere(['mst_product.jan_code' => $jan_code_product])->count();
            }

            $jan_code_store_common = self::find()->innerJoin('mst_store', 'mst_store.id = mst_product.store_id')->innerJoin('company', 'company.id = mst_store.company_id')->andFilterWhere(['company.del_flg' => '0','mst_store.del_flg' => '0', 'company.id' => $company_id])->andFilterWhere(['mst_product.store_id' => $store_id_sl])->andWhere(['mst_product.jan_code' => $jan_code_product])->one();
            if ($jan_code_store_common != null) {
                $check_jan_code_store_common = $jan_code_store_common->jan_code;
                if ($check_jan_code_store_common == $jan_code_product) {
                    return false;
                }
            }

            if ($check_jan_code > 0) {
                return false;
            }
            if ($count_jan_code > 0) {
                return false;
            } else {
                return true;
            }
        }
    }

    public function selectCountJanCodeExitsStoreCommon($jan_code_product) {
        return $query = self::find()->andWhere(['jan_code' => $jan_code_product]);
    }

}
