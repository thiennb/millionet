<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii2tech\ar\softdelete\SoftDeleteBehavior;
use yii\behaviors\BlameableBehavior;
/**
 * This is the model class for table "store_schedule".
 *
 * @property integer $id
 * @property integer $store_id
 * @property string $schedule_date
 * @property string $work_flg
 * @property string $start_time
 * @property string $end_time
 * @property string $del_flg
 * @property integer $created_at
 * @property integer $created_by
 * @property integer $updated_at
 * @property integer $updated_by
 */
class StoreSchedule extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'store_schedule';
    }
    
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
            BlameableBehavior::className(),
            'softDeleteBehavior' => [
                'class' => SoftDeleteBehavior::className(),
                'softDeleteAttributeValues' => [
                    'del_flg' => true
                ],
            ],
        ];
    }
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['store_id', 'created_at', 'created_by', 'updated_at', 'updated_by'], 'integer'],
            [['schedule_date'], 'safe'],
            [['work_flg'], 'default','value'=>1],
            [['created_at', 'created_by', 'updated_at', 'updated_by'], 'required'],
            [['work_flg', 'del_flg'], 'string', 'max' => 1],
            [['start_time', 'end_time'], 'string', 'max' => 5],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend', 'ID'),
            'store_id' => Yii::t('backend', 'Store ID'),
            'schedule_date' => Yii::t('backend', 'Schedule Date'),
            'work_flg' => Yii::t('backend', 'Work Flg'),
            'start_time' => Yii::t('backend', 'Start Time'),
            'end_time' => Yii::t('backend', 'End Time'),
            'del_flg' => Yii::t('backend', 'Del Flg'),
            'created_at' => Yii::t('backend', 'Created At'),
            'created_by' => Yii::t('backend', 'Created By'),
            'updated_at' => Yii::t('backend', 'Updated At'),
            'updated_by' => Yii::t('backend', 'Updated By'),
        ];
    }
    
    public static function find()
    {
        return parent::find()->where(['or',['store_schedule.del_flg'=> '0'] , ['store_schedule.del_flg'=>null]]);
    }
}
