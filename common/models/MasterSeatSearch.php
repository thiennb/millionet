<?php

namespace common\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\MasterSeat;
use common\components\Util;
use yii\helpers\ArrayHelper;
use Carbon\Carbon;

/**
 * MasterSeatSearch represents the model behind the search form about `common\models\MasterSeat`.
 */
class MasterSeatSearch extends MasterSeat {

    /**
     * @inheritdoc
     */
    public $capacity;
    public $from_date;
    public $to_date;
    public $from_time;
    public $to_time;
    public $time_require;

    public function rules() {
        return [
            [['id', 'type_seat_id', 'capacity_min', 'capacity_max', 'created_at', 'updated_at', 'capacity', 'time_require'], 'integer'],
            [['name', 'smoking_flg', 'introduce', 'image1', 'image2', 'image3', 'show_flg', 'del_flg', 'created_by', 'updated_by', 'store_id', 'type_seat_id'], 'safe'],
            [['from_date', 'to_date'], 'date', 'format' => 'yyyy-mm-dd'],
            [['from_time', 'to_time'], 'date', 'format' => 'H:i']
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = MasterSeat::find()
                ->innerJoin('mst_store', 'mst_store.id = mst_seat.store_id')
                ->leftJoin('mst_type_seat', 'mst_type_seat.id = mst_seat.type_seat_id');

        $query->andFilterWhere([
            'mst_store.company_id' => Util::getCookiesCompanyId(),
        ]);

        // add conditions that should always apply here
        $query->select([

            'mst_seat.id',
            'mst_seat.store_id',
            'mst_seat.seat_code',
            'mst_seat.name',
            'mst_seat.show_flg',
            'mst_seat.capacity_min',
            'mst_seat.capacity_max',
            'mst_seat.smoking_flg',
            'mst_type_seat.name as type_seat_name',
        ]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => false,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'mst_seat.store_id' => $this->store_id,
            'type_seat_id' => $this->type_seat_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'mst_seat.name', $this->name])
                ->andFilterWhere(['like', 'smoking_flg', $this->smoking_flg])
                ->andFilterWhere(['like', 'mst_seat.show_flg', $this->show_flg])
                ->andFilterWhere(['like', 'introduce', $this->introduce]);

        if (!empty($this->capacity_min))
            $query->andFilterWhere(['<=', 'capacity_min', $this->capacity_min]);
        $query->andFilterWhere(['>=', 'capacity_max', $this->capacity_min]);

        $query->groupBy([
            'mst_seat.id',
            'mst_seat.store_id',
            'mst_seat.seat_code',
            'mst_seat.name',
            'mst_seat.show_flg',
            'mst_seat.capacity_min',
            'mst_seat.capacity_max',
            'mst_seat.smoking_flg',
            'mst_type_seat.name',
        ]);
        $query->orderBy('name ASC');
        return $dataProvider;
    }

    /**
     * Search list of seat - schedule (\common\models\MasterSeat => Y-m-d H:i)
     * @param type $params
     * @param type $autobinding if provided, no class name namespace required
     * @return \common\models\MasterSeat
     */
    public function searchSeatSchedule($params = [], $autobinding = false) {
        if ($autobinding) {
            $className = array_pop(explode('\\', self::className()));
            if (!isset($params[$className])) {
                $params = [
                    $className => $params
                ];
            }
        }

        $this->load($params);

        if (!$this->validate()) {
            return self::find()->where('0=1');
        }

        $seat_search = MasterSeat::findFrontEnd();

        $seat_search->select([
            self::tableName() . '.*',
            'schedule' => "array_to_string(array_agg(concat(ssd.booking_date, ' ', ssd.booking_time)), ',', '*')"
        ]);

        /** REQUIRED CONDITIONS * */
        // del_flg condition included in frontFrontEnd
        $seat_search->andWhere([
            self::tableName() . '.show_flg' => '1'
        ]);

        $seat_search->innerJoin(['seat_type' => MasterTypeSeat::tableName()], self::tableName() . ".type_seat_id = seat_type.id "
                . "and (seat_type.del_flg = '0' or seat_type.del_flg = null) "
                . "and seat_type.store_id = " . self::tableName() . ".store_id "
                . "and seat_type.show_flg = '1'");

        $seat_search->leftJoin([
            'ssd' => SeatScheduleDetail::tableName()
                ], self::tableName() . ".id = ssd.seat_id "
                . "and ssd.booking_status <> '1' "
                . "and ssd.store_id = " . self::tableName() . ".store_id ");
        /** END REQUIRED CONDITIONS * */
        // seat spec conditions
        $seat_search->andFilterWhere([
            self::tableName() . '.id' => $this->id,
            self::tableName() . '.store_id' => $this->store_id,
            self::tableName() . '.type_seat_id' => $this->type_seat_id,
            self::tableName() . '.smoking_flg' => $this->smoking_flg
        ]);

        // capacity conditions
        $seat_search->andFilterWhere([
            '<=', self::tableName() . '.capacity_min', $this->capacity
        ]);

        $seat_search->andFilterWhere([
            '<=', self::tableName() . '.capacity_min', $this->capacity_min
        ]);

        $seat_search->andFilterWhere([
            '>=', self::tableName() . '.capacity_max', $this->capacity
        ]);

        $seat_search->andFilterWhere([
            '>=', self::tableName() . '.capacity_max', $this->capacity_max
        ]);

        // date conditions
        $seat_search->andFilterWhere([
            '>=', 'ssd.booking_date', $this->from_date
        ]);

        $seat_search->andFilterWhere([
            '<=', 'ssd.booking_date', $this->to_date
        ]);

        // time conditions
        $seat_search->andFilterWhere([
            '>=', 'ssd.booking_time', $this->from_time
        ]);

        $seat_search->andFilterWhere([
            '<=', 'ssd.booking_time', $this->to_time
        ]);

        $seat_search->groupBy([
            self::tableName() . '.id'
        ]);

        return $seat_search;
    }

    /**
     * Search list of schedule - seats (Y-m-d H:i => id list)
     * @param type $params
     * @param type $autobinding if provided, no class name namespace required
     * @return \yii\db\Query
     */
    public function searchSchedule($params = [], $autobinding = false) {
        if ($autobinding) {
            $className = array_pop(explode('\\', self::className()));
            if (!isset($params[$className])) {
                $params = [
                    $className => $params
                ];
            }
        }

        $this->load($params);

        if (!$this->validate()) {
            return self::find()->where('0=1');
        }

        if (key_exists('type_seat_id', $params)) {
            $type_seat_id = $params['type_seat_id'];
        } else {
            $type_seat_id = null;
        }

        /** capacity search */
        $capacity_search = (new \yii\db\Query())
                ->select([
                    MasterTypeSeat::tableName() . ".id",
                    'capacity_min' => "min(" . self::tableName() . ".capacity_min)",
                    'capacity_max' => "sum(" . self::tableName() . ".capacity_max)",
                    'total_seat_count' => "count(" . self::tableName() . ".id)"
                ])
                ->from(MasterTypeSeat::tableName());

        $capacity_search->innerJoin(self::tableName(), self::tableName() . ".type_seat_id = " . MasterTypeSeat::tableName() . ".id "
                . "and " . self::tableName() . ".show_flg = '1'"
                . "and (" . self::tableName() . ".del_flg = '0' or " . self::tableName() . ".del_flg = null)");
        $capacity_search->andFilterWhere([
            MasterTypeSeat::tableName() . '.id' => $type_seat_id
        ]);
        $capacity_search->groupBy([
            MasterTypeSeat::tableName() . ".id"
        ]);
        /** end capacity search */
        $query = (new \yii\db\Query())
                ->select([
                    'shift' => "concat(ssd.booking_date, ' ', ssd.booking_time)",
                    'seat_list' => "array_to_string(array_agg(distinct(s.id)), ',', '*')",
                    'type_seat_list' => "array_to_string(array_agg(distinct(s.type_seat_id)), ',', '*')",
                    'capacity_min' => "capacity_search.capacity_min",
                    'capacity_max' => "capacity_search.capacity_max - sum(s.capacity_max)",
                    'total_seat_count' => "capacity_search.total_seat_count",
                    'seat_count' => "count(distinct(s.id))"
                ])
                ->from(['ssd' => SeatScheduleDetail::tableName()]);
        /** REQUIRED CONDITIONS * */
        $query->innerJoin(['s' => self::tableName()], "s.id = ssd.seat_id "
                . "and (s.del_flg = '0' or s.del_flg = null) "
                . "and s.store_id = ssd.store_id "
                . "and ssd.booking_status <> '1' "
                . "and s.show_flg = '1'");
        $query->innerJoin(['seat_type' => MasterTypeSeat::tableName()], "s.type_seat_id = seat_type.id "
                . "and (seat_type.del_flg = '0' or seat_type.del_flg = null) "
                . "and seat_type.show_flg = '1'");
        $query->innerJoin(['capacity_search' => $capacity_search], 'capacity_search.id = seat_type.id');
        /** END REQUIRED CONDITIONs * */
        $query->groupBy([
            'ssd.booking_date',
            'ssd.booking_time',
            'capacity_search.capacity_min',
            'capacity_search.capacity_max',
            'capacity_search.total_seat_count'
        ]);

        // seat spec conditions
        $query->andFilterWhere([
            's.id' => $this->id,
            's.store_id' => $this->store_id,
            's.type_seat_id' => $this->type_seat_id,
            's.smoking_flg' => $this->smoking_flg
        ]);

        // capacity conditions
        $query->andFilterWhere([
            '<=', 's.capacity_min', $this->capacity
        ]);

        $query->andFilterWhere([
            '<=', 's.capacity_min', $this->capacity_min
        ]);

        $query->andFilterWhere([
            '>=', 's.capacity_max', $this->capacity
        ]);

        $query->andFilterWhere([
            '>=', 's.capacity_max', $this->capacity_max
        ]);

        // date conditions
        $query->andFilterWhere([
            '>=', 'ssd.booking_date', $this->from_date
        ]);

        $query->andFilterWhere([
            '<=', 'ssd.booking_date', $this->to_date
        ]);

        // time conditions
        $query->andFilterWhere([
            '>=', 'ssd.booking_time', $this->from_time
        ]);

        $query->andFilterWhere([
            '<=', 'ssd.booking_time', $this->to_time
        ]);


        return $query;
    }

    /**
     * Get a ready-to-use detailed schedule for seats
     * @param type $params
     *   'from_date' => '2016-12-01',
     *   'to_date' => '2016-12-01',
     *   'from_time' => '10:00',
     *   'to_time' => '10:00',
     *   'smoking_flg' => 0,
     *   'store_id' => 7,
     *   'id' => 11,
     *   'type_seat_id' => 1,
     *   'capacity' => 8, // khoang giua capacity_min va capacity_max // so nguoi yeu cau
     *   'capacity_min' => 2,
     *   'capacity_max' => 10
     * @return array
     * date-time => seat_list
     */
    public static function getDetailedSchedule($params = []) {
        $schedule_search = new MasterSeatSearch();

        $schedule = ArrayHelper::map($schedule_search->searchSchedule($params, true)->all(), 'shift', 'seat_list');

        $shift_count = ceil($schedule_search->time_require / 30) - 1;

        foreach ($schedule as $date_time => $seat_list) {
            $schedule[$date_time] = explode(',', $seat_list);
            self::_expandSchedule($schedule, $date_time, $schedule[$date_time], $shift_count);
        }

        ksort($schedule);

        return $schedule;
    }

    /**
     * Get a ready-to-use detailed seat list with schedule
     *  by accessing shifts in each seat model
     * @param type $params
     *   'from_date' => '2016-12-01',
     *   'to_date' => '2016-12-01',
     *   'from_time' => '10:00',
     *   'to_time' => '10:00',
     *   'smoking_flg' => 0,
     *   'store_id' => 7,
     *   'id' => 11,
     *   'type_seat_id' => 1,
     *   'capacity' => 8, // khoang giua capacity_min va capacity_max // so nguoi yeu cau
     *   'capacity_min' => 2,
     *   'capacity_max' => 10
     * @return array of \common\model\MasterSeat
     */
    public static function getDetailedSeatSchedule($params = [], $search_method = 'searchSeatSchedule') {
        $schedule_search = new MasterSeatSearch();

        $schedule = $schedule_search->$search_method($params, true)->orderBy([
                    'capacity_max' => SORT_ASC,
//            'ssd.booking_date' => SORT_ASC,
//            'ssd.booking_time' => SORT_ASC
                ])->all();
        $shift_count = ceil($schedule_search->time_require / 30) - 1;

        foreach ($schedule as $seat) {
            /* @var $seat \common\models\MasterSeat */
            $shifts = array_filter(explode(',', trim($seat->schedule)));
            foreach ($shifts as $date_time) {
                self::_expandSeatSchedule($shifts, $date_time, $shift_count);
            }
            //sort($shifts);

            $seat->shifts = $shifts;
        }

        return ArrayHelper::index($schedule, 'id');
    }

    /**
     * Get a ready-to-use detailed schedule for seats
     * @param type $params
     * @return array
     * date-time => seat_list
     */
    public static function getSeatBusySchedule($params = []) {
        $schedule_search = new MasterSeatSearch();

        $schedule = ArrayHelper::map($schedule_search->searchSchedule($params, true)->all(), 'shift', 'seat_list');

        $shift_count = ceil($schedule_search->time_require / 30) - 1;

        foreach ($schedule as $date_time => $seat_list) {
            $schedule[$date_time] = explode(',', $seat_list);
            self::_expandSchedule($schedule, $date_time, $schedule[$date_time], $shift_count);
        }

        ksort($schedule);

        return $schedule;
    }

    public static function getDetailedTypeSchedule($type_seat_id = -1, $params = []) {
        $schedule_search = new MasterSeatSearch();
        $params['type_seat_id'] = $type_seat_id;

        $schedule = ArrayHelper::index($schedule_search->searchSchedule($params, true)->all(), 'shift');

        $shift_count = ceil($schedule_search->time_require / 30) - 1;

        foreach ($schedule as $date_time => $schedule_item) {
            $seat_list = $schedule_item['seat_list'];
            if (!key_exists('seat_list', $schedule[$date_time])) {
                $schedule[$date_time]['seat_list'] = explode(',', $seat_list);
            } elseif (!is_array($schedule[$date_time]['seat_list'])) {
                $schedule[$date_time]['seat_list'] = explode(',', $schedule[$date_time]['seat_list']);
            } else {
                $schedule[$date_time]['seat_list'] = array_unique(array_merge($schedule[$date_time]['seat_list'], explode(',', $seat_list)));
            }
            self::_expandSchedule($schedule, $date_time, $schedule[$date_time]['seat_list'], $shift_count);
        }
        //ksort($schedule);

        return $schedule;
    }

    //== HELPERS
    public static function _expandSeatSchedule(&$schedule, $date_time, $shift_count) {
        if ($shift_count <= 0) {
            return;
        }
        $shifts = range(-$shift_count, $shift_count);
        $shift_length = count($shifts);

        for ($i = 0; $i < $shift_length; $i++) {
            $new_shift_date_time = Carbon::createFromFormat('Y-m-d H:i:00', $date_time)
                    ->addMinutes(30 * $shifts[$i])
                    ->format('Y-m-d H:i:00');
            if (array_search($new_shift_date_time, $schedule) === false) {
                $schedule[] = $new_shift_date_time;
            }
        }
    }

    public static function _expandSchedule(&$schedule, $date_time, $seat_list, $shift_count) {
        if ($shift_count <= 0) {
            return;
        }
        $shifts = range(-$shift_count, $shift_count);
        $shift_length = count($shifts);

        for ($i = 0; $i < $shift_length; $i++) {
            $new_shift_date_time = Carbon::createFromFormat('Y-m-d H:i:00', $date_time)
                    ->addMinutes(30 * $shifts[$i])
                    ->format('Y-m-d H:i:00');
            if (!isset($schedule[$new_shift_date_time])) {
                $schedule[$new_shift_date_time] = $seat_list;
            } else {
                if (!is_array($schedule[$new_shift_date_time])) {
                    $schedule[$new_shift_date_time] = explode(',', $schedule[$new_shift_date_time]);
                }
                $schedule[$new_shift_date_time] = array_unique(array_merge($schedule[$new_shift_date_time], $seat_list));
            }
        }
    }

    public static function _expandTypeSchedule(&$schedule, $date_time, $seat_list, $shift_count) {
        if ($shift_count <= 0) {
            return;
        }
        $shifts = range(-$shift_count, $shift_count);
        $shift_length = count($shifts);

        for ($i = 0; $i < $shift_length; $i++) {
            $new_shift_date_time = Carbon::createFromFormat('Y-m-d H:i:00', $date_time)
                    ->addMinutes(30 * $shifts[$i])
                    ->format('Y-m-d H:i:00');
            if (!isset($schedule[$new_shift_date_time])) {
                $schedule[$new_shift_date_time] = $schedule[$date_time];
                $schedule[$new_shift_date_time]['seat_list'] = $seat_list;
            } else {
                if (!key_exists('seat_list', $schedule[$new_shift_date_time])) {
                    $schedule[$new_shift_date_time]['seat_list'] = $seat_list;
                } else if (!is_array($schedule[$new_shift_date_time]['seat_list'])) {
                    $schedule[$new_shift_date_time]['seat_list'] = explode(',', $schedule[$new_shift_date_time]['seat_list']);
                } else {
                    $schedule[$new_shift_date_time]['seat_list'] = array_unique(array_merge($schedule[$new_shift_date_time]['seat_list'], $seat_list));
                }
            }
        }
    }

}
