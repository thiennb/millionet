<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii2tech\ar\softdelete\SoftDeleteBehavior;
use yii\behaviors\BlameableBehavior;
use common\components\Util;
use common\components\Constants;
use common\models\CustomerStore;
use common\components\RegisterFromBehavior;
use common\components\CompanyIdBehavior;

/**
 * This is the model class for table "mst_member".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $first_name
 * @property string $last_name
 * @property string $first_name_kana
 * @property string $last_name_kana
 * @property string $birth_date
 * @property string $sex
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $del_flg
 * @property string $register_kbn 
 * @property User $user
 */
class MasterMember extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'mst_customer';
    }

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            TimestampBehavior::className(),
            BlameableBehavior::className(),
            'CompanyIdBehavior' => [
                'class' => CompanyIdBehavior::className()
            ],
            'softDeleteBehavior' => [
                'class' => SoftDeleteBehavior::className(),
                'softDeleteAttributeValues' => [
                    'del_flg' => '1'
                ],
            ],
            RegisterFromBehavior::className()
        ];
    }

    public $post_code_1;
    public $post_code_2;
    public $phone_1;
    public $phone_2;
    public $phone_3;
    public $phone_1_new;
    public $phone_2_new;
    public $phone_3_new;
    public $phone_1_re;
    public $phone_2_re;
    public $phone_3_re;
    public $key_confirm;
    public $key_confirm_again;
    public $password;
    public $password_re;
    public $password_old;
    public $email_re;
    public $store_id;
    public $mode;

    const REGISTER_PHONE = 1;
    const REGISTER_EMAIL = 2;

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['first_name', 'last_name', 'first_name_kana', 'last_name_kana', 'password', 'password_re'], 'required', 'on' => 'step_3'],
            [['first_name', 'last_name', 'first_name_kana', 'last_name_kana', 'password', 'password_re'], 'required', 'on' => 'step_3_email'],
            [['first_name', 'last_name', 'first_name_kana', 'last_name_kana'], 'required', 'on' => 'update'],
            [['email'], 'required', 'on' => 'step_1_email'],
            [['email'], 'string', 'max' => 60],
            [['key_confirm_again', 'updated_by', 'register_kbn','company_id'], 'safe'],
            ['key_confirm', 'required', 'on' => 'step_2'],
            [['password', 'key_confirm', 'password_re'], 'string', 'min' => 6, 'message' => Yii::t('backend', "The number of digits is not enough"),],
            [['password', 'key_confirm', 'password_re'], 'string', 'max' => 20],
            [['last_name_kana', 'first_name_kana'], 'match', 'pattern' => '/^([ｧ-ﾝﾞﾟァ-ヺ・ーヽヾヿ]+)$/u', 'message' => Yii::t('backend', 'Please input Kana character.')],
            ['birth_date', 'compare', 'compareValue' => '1900/01/01', 'operator' => '>=', 'message' => Yii::t('yii', '{attribute} must be greater than "{compareValueOrAttribute}".'), 'skipOnEmpty' => true],
            [['birth_date'], 'date', 'format' => 'yyyy/MM/dd', 'message' => Yii::t('backend', "Date format don't match"), 'skipOnEmpty' => true],
            [['email', 'email_re'], 'checkEmail'],
            [['first_name', 'last_name', 'first_name_kana', 'last_name_kana'], 'string', 'max' => 40],
            [['phone_1', 'phone_2', 'phone_3', 'store_id'], 'integer'],
            [['phone_1', 'phone_1_new', 'phone_1_re'], 'string', 'min' => 2, 'max' => 3],
            [['post_code_1'], 'string', 'min' => 3, 'max' => 3],
            [['phone_2', 'phone_3', 'phone_2_re', 'phone_3_re', 'phone_2_new', 'phone_3_new', 'post_code_2'], 'string', 'min' => 4, 'max' => 4],
            [['phone_1', 'phone_2', 'phone_3'], 'required', 'on' => 'create_phone'],
            [['sex'], 'string', 'max' => 1],
            [['sex'], 'default', 'value' => null],
//            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
            ['password_re', 'compare', 'compareAttribute' => 'password', 'message' => Yii::t('backend', "Input content does not match"), 'skipOnEmpty' => false],
            ['email_re', 'compare', 'compareAttribute' => 'email', 'message' => Yii::t('backend', "Email don't match"), 'skipOnEmpty' => false, 'on' => 'step_3'],
            [['email'], 'unique',
                'targetClass' => 'common\models\MasterMember',
                'when' => function ($model, $attribute) {
                    return $model->{$attribute} !== $model->getOldAttribute($attribute);
                },
            ],
            [['phone_1', 'phone_2', 'phone_3'], 'required', 'on' => 'step_3_email'],
            [['phone_1', 'phone_2', 'phone_3', 'phone_1_new', 'phone_2_new', 'phone_3_new', 'phone_1_re', 'phone_2_re', 'phone_3_re'], 'required', 'on' => 'update_phone'],
            ['phone_1_re', 'compare', 'compareAttribute' => 'phone_1_new', 'on' => 'update_phone'],
            ['phone_2_re', 'compare', 'compareAttribute' => 'phone_2_new', 'on' => 'update_phone'],
            ['phone_3_re', 'compare', 'compareAttribute' => 'phone_3_new', 'on' => 'update_phone'],
            [['email', 'email_re'], 'required', 'on' => 'update_email'],
            [['password', 'password_re', 'password_old'], 'required', 'on' => 'update_password'],
            [['password_re', 'password'], 'match', 'pattern' => "/^([a-zA-Z0-9!@#$%^&*]+)$/u", 'message' => Yii::t('backend', 'Please enter alphanumeric symbols.')],
            [['post_code'], 'string', 'max' => 7, 'min' => 7],
            [['prefecture'], 'string', 'max' => 80],
            [['address',], 'string', 'max' => 240],
            ['email_re', 'compare', 'compareAttribute' => 'email', 'message' => Yii::t('backend', "Email don't match"), 'on' => 'update_email'],
//            [['birth_date',],'safe'],
            [['address', 'prefecture'], 'required',
                'when' => function ($model) {
                    return !empty($model->post_code_1) || !empty($model->post_code_2);
                },
                'on' => 'update'
            ],
            [['post_code_1', 'post_code_2', 'prefecture'], 'required',
                'when' => function ($model) {
                    return !empty($model->address);
                },
                'on' => 'update'
            ],
            [['post_code_1', 'post_code_2', 'address'], 'required',
                'when' => function ($model) {
                    return !empty($model->prefecture);
                },
                'on' => 'update'
            ],
            [['post_code_1'], 'required',
                'when' => function ($model) {
                    return !empty($model->post_code_2);
                },
                'on' => 'update'
            ],
            [['post_code_2'], 'required',
                'when' => function ($model) {
                    return !empty($model->post_code_1);
                },
                'on' => 'update'
            ],
            [['phone_1'], 'checkMobileUnique', 'on' => 'step_3_email'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => Yii::t('backend', 'ID'),
            'user_id' => Yii::t('backend', 'User ID'),
            'first_name' => Yii::t('backend', 'First name'),
            'last_name' => Yii::t('backend', 'Last name'),
            'first_name_kana' => Yii::t('backend', 'First name kana'),
            'last_name_kana' => Yii::t('backend', 'Last name kana'),
            'birth_date' => Yii::t('backend', 'Birthday'),
            'sex' => Yii::t('backend', 'Sex'),
            'created_at' => Yii::t('backend', 'Created At'),
            'updated_at' => Yii::t('backend', 'Updated At'),
            'del_flg' => Yii::t('backend', 'Del Flg'),
            'email' => Yii::t('backend', 'Email'),
            'email_re' => Yii::t('backend', 'Email Re'),
            'password_old' => Yii::t('frontend', '※ password old'),
            'password' => Yii::t('frontend', '※ password'), //Yii::t('backend', 'Password'),
            'password_re' => Yii::t('frontend', '※ password re'), //Yii::t('backend','Password Re'),
            'phone_1' => Yii::t('frontend', '※ mobile'),
            'phone_2' => Yii::t('frontend', '※ mobile'),
            'phone_3' => Yii::t('frontend', '※ mobile'),
            'phone_1_new' => Yii::t('frontend', 'New phone number'),
            'phone_2_new' => Yii::t('frontend', 'New phone number'),
            'phone_3_new' => Yii::t('frontend', 'New phone number'),
            'phone_1_re' => Yii::t('frontend', 'Re New phone number'),
            'phone_2_re' => Yii::t('frontend', 'Re New phone number'),
            'phone_3_re' => Yii::t('frontend', 'Re New phone number'),
            'key_confirm' => Yii::t('frontend', '※ Key confirm'),
            'post_code' => Yii::t('frontend', 'Postal code'),
            'prefecture' => Yii::t('frontend', 'Prefectures'),
            'address' => Yii::t('frontend', 'The city where your business is located below'),
            'post_code_1' => Yii::t('frontend', 'Postal code'),
            'post_code_2' => Yii::t('frontend', 'Postal code'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser() {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public static function find($findAll = true) {
        if ($findAll)
            if(\Yii::$app->controllerNamespace != "api\controllers"){
                return parent::find()->where(['or', ['mst_customer.del_flg' => '0'], ['mst_customer.del_flg' => null]])->andWhere(['company_id' => Util::getCookiesCompanyId()]);
            }else{
                return parent::find()->where(['or', ['mst_customer.del_flg' => '0'], ['mst_customer.del_flg' => null]]);
            }
        else
            return parent::find();
    }

    /**
     * Save data member register
     * @return none
     */
    public function saveData(MasterMember $model = null) {
        //save user
        $user = new User();
        $user->tel_no = $model->mobile;
        $user->setPassword($model->password);
        $user->generateAuthKey();
        $user->save();

        $store_id = $model->register_store_id;

        Yii::$app->user->login($user, 3600 * 24 * 30);

        //save member
        $model->customer_jan_code = Util::genJanCodeAuto(Constants::JAN_TYPE_CUSTORMER);
        $model->user_id = $user->id;
        $model->save();

        if (!empty($store_id)) {
            $customer_store = new CustomerStore();
            $customer_store->store_id = $store_id;
            $customer_store->customer_id = $model->id;
            $customer_store->rank_id = '00';
            $customer_store->black_list_flg = '0';
            $customer_store->save();
        }
    }

    /**
     * Send mail after register success
     * @return none
     */
    public function sendEmailRegister(MasterMember $model = null) {
        Yii::$app->mailer->compose(
                        ['html' => 'registerMember-html', 'text' => 'registerMember-text'], ['model' => $model]
                )
                ->setFrom('amslaypham@gmail.com')
                ->setTo($model->email)
                ->setSubject('Message subject')
                ->send();
    }

    // ======================
    // == Check mobile unique in customer
    // ======================
    public function checkMobileUnique($attribute, $params) {
        $this->mobile = $this->phone_1 . $this->phone_2 . $this->phone_3;
        $phone = MasterCustomer::findOne(['mobile' => $this->mobile]);
        if (!empty($phone->mobile)) {
            $this->addError('phone_1', Yii::t('frontend', "Sory Phone Number was been used."));
            $this->addError('phone_2', '');
            $this->addError('phone_3', '');
        }
    }

    /**
     * Process before save into database
     * @return none
     */
    public function beforeSave($insert) {
        if (parent::beforeSave($insert)) {
            (!empty($this->phone_1) && !empty($this->phone_2) && !empty($this->phone_3)) ? $this->mobile = $this->phone_1 . $this->phone_2 . $this->phone_3 : $this->mobile;
            (!empty($this->post_code_1) && !empty($this->post_code_2)) ? $this->post_code = $this->post_code_1 . $this->post_code_2 : $this->post_code;
            return true;
        } else {
            return false;
        }
    }

    /**
     * Check email
     * @param string $attribute, $params
     * @return error 
     * @throws 
     */
    public function checkEmail($attribute, $params) {

        $check = preg_match(
            '/^[A-z]+[.A-z0-9_\-]+[@][A-z0-9_\-]+([.][A-z0-9_\-]+)+[A-z.]{2,4}$/', $this->$attribute
        );

        //check valid email
        if (!$check) {
            $key = $attribute;
            //add message error
            $this->addError($key, Yii::t('backend', "email not valid"));
        }
    }

    /**
     * Send mail after register success
     * @return none
     */
    public function sendKeyConfirmEmail(MasterMember $model = null) {
        Yii::$app->mailer->compose(
                        ['html' => 'registerMemberKeyConfirm-html', 'text' => 'registerMemberKeyConfirm-text'], ['model' => $model]
                )
                ->setFrom('amslaypham@gmail.com')
                ->setTo($model->email)
                ->setSubject('Message subject')
                ->send();
    }

}
