<?php

namespace common\models;

use Yii;

class ApiFormBookingDetail extends \yii\base\Model {

    public $demand;
    public $point_use;

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            ['point_use', 'integer'],
            ['demand', 'string', 'max' => 200],
            ['point_use', 'validatePoints']
        ];
    }

    /**
     * Validates the company_code
     * This method serves as the inline validation for company code.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validatePoints($attribute) {

        // query customer
        $customer = MasterCustomer::find()->where(['user_id' => Yii::$app->user->id])->one();

        if ($customer) {
            // query current user point
            $point = CustomerStore::find()->where([
                        'customer_id' => $customer->id,
                        'store_id' => Yii::$app->request->get('storeId')
                    ])->one();

            $total_point = $point ? (int) $point->total_point : 0;
        } else {
            $total_point = 0;
        }

        $points = (int) $this->point_use;

        if ($points < 0) {
            $this->addError($attribute, 'ご利用ポイント は0よりてはいけません。');
        }

        if ($points > $total_point) {
            $this->addError($attribute, '利用ポイント は現在ポイントより大きくてはいけません。');
        }

        return false;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'point_use' => 'ご利用ポイント'
        ];
    }

}
