<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii2tech\ar\softdelete\SoftDeleteBehavior;
use yii\behaviors\BlameableBehavior;
use common\components\Util;
use common\components\Constants;

/**
 * This is the model class for table "mst_ticket".
 *
 * @property integer $id
 * * @property integer $store_id
 * @property string $name
 * @property string $set_ticket
 * @property string $date_expiration
 * @property string $price
 * @property string $del_flg
 * @property string $people_created
 * @property string $people_updated
 * @property integer $created_at
 * @property integer $updated_at
 */
class MasterTicket extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public $store_name;
    public $ticket_balance;
    public $total;
    public $option;
    public $option_hidden, $product_table_error;
    public $ticket_jan_code_old;
    public $start_date;
    public $end_date;
    public $price_to;
    public $id_store_comon;
    public $_role;
    

    public static function tableName() {
        return 'ticket';
    }

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            TimestampBehavior::className(),
            BlameableBehavior::className(),
            'softDeleteBehavior' => [
                'class' => SoftDeleteBehavior::className(),
                'softDeleteAttributeValues' => [
                    'del_flg' => '1'
                ],
            ],
        ];
    }

    public function rules() {
        return [
            [['option', 'option_hidden', 'ticket_jan_code_old', 'id_store_comon', '_role'], 'safe'],
            [['name', 'date_expiration', 'price', 'set_ticket'], 'required'],
            [['option_hidden'], 'required', 'message' => Yii::t('backend', "You must add product to update ticket")],
            [['created_at', 'updated_at', 'store_id', 'set_ticket'], 'integer'],
            [['price'], 'number', 'min' => 0, 'max' => 9999999999],
            [['price'], 'string', 'max' => 13],
            [['name'], 'string', 'max' => 60],
            [['ticket_jan_code'], 'string', 'max' => 13],
            [['ticket_jan_code'], 'unique', 'when' => function($model) {
                return $model->ticket_jan_code != $model->ticket_jan_code_old;
            }],
            [['ticket_jan_code'], 'checkJanCode', 'skipOnEmpty' => TRUE],
            //[['date_expiration'], 'date', 'format' => 'php:Y/m/d'],
            // ['date_expiration', 'checkDateTime'],
            [['set_ticket'], 'number', 'min' => 0, 'max' => 99999],
            [['set_ticket'], 'string', 'max' => 6],
            ['price', 'checkPrice'],
            [['store_id'], 'required', 'when' => function() {
                $role = \common\components\FindPermission::getRole();
                return ($role->permission_id == Constants::STORE_MANAGER_ROLE || $role->permission_id == Constants::STAFF_ROLE);
            }],
        ];
    }

    // ======================
    // == Check Jan Code start
    // ======================
    public function checkJanCode($attribute, $params) {
        $check_jan_code = Util::checkJanCodeTicket(Constants::JAN_TYPE_TICKET, $this->ticket_jan_code);
        if (!$check_jan_code)
            $this->addError('ticket_jan_code', Yii::t('backend', "JAN_Code invalid, please try again."));
    }

    // ======================
    // == Check Jan Code end
    // ======================
    // ======================
    // == Check Date Time  expiration start
    // ======================
    public function checkDateTime($attribute, $params) {
        if ($this->date_expiration < date('Y/m/d'))
            $this->addError($attribute, Yii::t('backend', "Please select date time great than now date"));
    }

    public function checkPrice($attribute, $params) {
        if ($this->price > 9999999999) {
            $this->addError('price', Yii::t('backend', "The ticket selling price must not be larger than 9,999,999,999."));
        }
    }

    // ======================
    // == Check Date Time  expiration end
    // ======================
    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => Yii::t('backend', 'ID'),
            'store_id' => Yii::t('backend', 'Store Ticket'),
            'name' => Yii::t('backend', 'Name Ticket'),
            'set_ticket' => Yii::t('backend', 'Set Ticket'),
            'date_expiration' => Yii::t('backend', 'Date Expiration Ticket'),
            'price' => Yii::t('backend', 'Price Ticket'),
            'price_to' => Yii::t('backend', 'Price Ticket'),
            'ticket_jan_code' => Yii::t('backend', 'Ticket Jan Code'),
            'del_flg' => Yii::t('backend', 'Del Flg'),
            'created_by' => Yii::t('backend', 'Created By'),
            'updated_by' => Yii::t('backend', 'Updated By'),
            'created_at' => Yii::t('backend', 'Created At'),
            'updated_at' => Yii::t('backend', 'Updated At'),
            'product_name' => Yii::t('backend', 'Product Name'),
            'set_ticket_min' => Yii::t('backend', 'Set Ticket'),
            'set_ticket_max' => Yii::t('backend', 'Set Ticket'),
            'date_expiration_from' => Yii::t('backend', 'Date Expiration Ticket'),
            'date_expiration_to' => Yii::t('backend', 'Date Expiration Ticket'),
            'price_from' => Yii::t('backend', 'Price Ticket'),
            'price_from' => Yii::t('backend', 'Price Ticket'),
        ];
    }

    public function uniqueJanCode() {
        if ($this->ticket_jan_code !== $this->getOldAttribute('ticket_jan_code')) {
            $this->addError('ticket_jan_code', Yii::t('backend', "JAN_Code invalid, please try again."));
        }
    }

    public function getOldAttributes($name) {
        var_dump($this->_oldAttributes);
        return isset($this->_oldAttributes[$name]) ? $this->_oldAttributes[$name] : null;
    }

    public static function find($findAll = true) {
        if ($findAll)
            return parent::find()->where((new \common\components\FindPermission)->getPermissionCondition(self::tableName(), true));
        else
            return parent::find();
    }

    public function getProductTicket() {
        return $this->hasMany(ProductTicket::className(), ['ticket_id' => 'id']);
    }

    public function saveProductTicket($id, $option_hidden) {
        ProductTicket::updateAll(['del_flg' => '1','updated_at' => strtotime("now")], ['ticket_id' => $id]);
        $product_arr = empty($option_hidden) ? "" : explode(",", $option_hidden);
        if (!empty($product_arr)) {
            foreach ($product_arr as $val) {
                $product_ticket = new ProductTicket();
                $product_ticket->ticket_id = $id;
                $product_ticket->product_id = $val;
                $product_ticket->save();
            }
        }
    }

    public function checkDelete($id) {
        $check = 0;
        //$check += ProductTicket::find()->andWhere(['ticket_id' => $id])->count();
        $ticket = MasterTicket::findOne($id);
        //$check += BookingChecklist::find()->andWhere(['ticket_id' => $id])->count();
        $check += TicketHistory::find()->andWhere(['ticket_jan_code' => $ticket->ticket_jan_code])->count();
        return ($check > 0) ? FALSE : TRUE;
    }

    /*
     * get histories method
     * 
     * @return TicketHistory
     */

    public function getHistories() {
        return $this->hasMany(TicketHistory::className(), ['ticket_jan_code' => 'ticket_jan_code'])
                        ->where(['ticket_history.del_flg' => 0])
                        ->andWhere(['ticket_history.process_type' => 0])
                        ->orderBy('updated_at');
    }
    
    public function getTicketByJancode($jancode = null, $store_id = null){
        if ($jancode !== null && $store_id !== null) {
            $result = (new \yii\db\Query())
                    ->select(['*'])
                    ->from('ticket')
                    ->where(['ticket_jan_code' => $jancode])
                    ->andWhere(['store_id' => $store_id])
                    ->andWhere(['del_flg' => '0'])
                    ->one();
            return $result;
        }

        return null;
    }
    
    public function getAllTicketByStoreId($storeId = null){
        if($storeId !== null){
            return MasterTicket::find()->where(['store_id'=> $storeId])->all();
        }
    }

}
