<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "store_schedule_temp".
 *
 * @property integer $id
 * @property integer $store_id
 * @property string $schedule_date
 * @property string $time
 * @property string $work_flg
 * @property string $del_flg
 * @property integer $created_at
 * @property integer $created_by
 * @property integer $updated_at
 * @property integer $updated_by
 */
class StoreScheduleTemp extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'store_schedule';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['store_id', 'created_at', 'created_by', 'updated_at', 'updated_by'], 'integer'],
            [['schedule_date'], 'safe'],
            [['time'], 'string', 'max' => 5],
            [['work_flg', 'del_flg'], 'string', 'max' => 1],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'store_id' => 'Store ID',
            'schedule_date' => 'Schedule Date',
            'time' => 'Time',
            'work_flg' => 'Work Flg',
            'del_flg' => 'Del Flg',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
        ];
    }

    public static function find()
    {
        return parent::find()->where([StoreScheduleTemp::tableName().'.del_flg' => 0]);
    }


	public static function getSchuduleStoreTempBetweenDates($store_id,$number_row,$time_execute,$start_date,$end_date,$reservation_possible_time)
	{
        $connection = \Yii::$app->db;

        return $connection->createCommand("
                select *
                from (
                    select
                            *
                    from (
                            select
                                ssd1.id,
                                ssd1.staff_id,
                                ssd1.booking_date,
                                ssd1.store_id,
                                case
                                    when to_timestamp(ssd1.booking_date||' '||(ssd1.booking_time),'YYYY/MM/DD HH24:MI:SS' ) > (now() + interval '" . $reservation_possible_time . " hour')
                                    then 1 else 2
                                end as booking_status,
                                case
                                    when
                                        (
                                            select
                                                    count(*)
                                            from
                                                    staff_schedule_detail as ssd
                                            where
                                                    ssd.booking_status = '1'
                                            and
                                                    ssd.staff_id = ssd1.staff_id
                                            and
                                                    ssd.store_id = ssd1.store_id
                                            and
                                                    ssd.booking_time between ssd1.booking_time and ssd1.booking_time + '".$time_execute."'
                                            and
                                                    ssd1.booking_date = ssd.booking_date

                                        ) = ".ceil($number_row)."
                                    then
                                            ssd1.booking_time
                                    else
                                            null
                                end as booking_time
                            from
                                    staff_schedule_detail as ssd1
                            where
                                    booking_status = '1' and store_id = '" . $store_id . "'
                            and
                                     booking_date between '".$start_date."' and '".$end_date."'
                            order by
                                    staff_id,booking_date,booking_time
                            ) as staff_schedule_detail
                    where
                            booking_time is not null
                    and
                            to_timestamp(booking_date||' '||booking_time,'YYYY/MM/DD HH24:MI:SS' ) > now()
                    ) as store_schedule_temps
        ")->queryAll();
    }
}
