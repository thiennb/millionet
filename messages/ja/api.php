<?php
/*
 * Language for api
 */
return [
    'Parameter "{param}" is invalid' => 'Parameter {param} is invalid',
    'Parameter "{param}" is missing' => 'Parameter "{param}" is missing',
    '{param} is not found' => '{param} is not found',
    'You must login before' => 'You must login before',
    'Register unsuccess, please try again!' => 'Register unsuccess, please try again!',
    'Request method is not supported' => 'Request method is not supported',
    "System error happen. Please contact with System manager." => "システムエラーが発生しました。システム管理者にお問い合わせください。",
    'You must create step 1!'   =>  'このステップで登録をしてください。',
    'Member registration completion'  =>  '会員登録完了',
    'Completion of registration'  =>  '登録完了',
    'Your membership registration has been completed.'    =>  'ご会員登録が完了しました。',
    'next'  =>  '次へ',
    'Sory Phone Number was been used.' => '電話番号が重複しています。もう一度入力してください。',
    'Sory Email was been used.' => 'このメールは存在しています。',
    'Confirmation of member information'    =>'会員情報の確認',
    'Change of member information has been completed.'    =>  '会員情報の変更が完了しました。',

	//============================
	//    BOOKING ORDER
	//============================
	// index
	'Deselect this' => '選択を解除',
	'Select this' => '商品を選択',
	'Coupon and product filters' => '商品カテゴリで絞り込む',
	'Filter by coupons' => 'クーポン表示',
	'Filter by products' => '商品表示',
	'Filter' => '絞り込み',
	// options
	'Option name' => 'メニュー',
	'minutes' => '分',
	'Cost' => '料金',
	'Total time (approximate)' => '所要時間(目安)',
	'Total' => '合計',
	// staff
	'Nomination fee' => '指名料',
	// errors
	'Sorry, nothing found' => '何も見つかりませんでした。',
	//====================
	// steps
	'Select coupon' => 'クーポン・メニューを選ぶ',
	'Please select date time' => 'ご希望の来店日時を選択してください',
	'Please select staff' => 'スタッフを選択してください',
	'Provide information' => 'お客様情報入力',
	'Confirm booking' => '予約内容の確認',
	'Finish booking' => '予約完了',
	// index
	'Coupon' => 'ごクーポン',
	'Product' => 'ご予約商品',
	'Back' => '戻る',
	'Filter menu' => 'メニューを絞る',
	'Choose coupon' => 'クーポン',
	'Choose product' => '商品',
	// filter_menu
	'Include menu' => 'Include menu',
	'Load all' => 'すべて',
	'My first time' => '初来店',
	'After second time' => '2 回目以降',
	'Choose Menu' => 'メニューを選ぶ',
	'Include menu' => '上記メニューに',
	// confirm_menu
	'Add to booking menu' => 'このクーポンで<br>空席・日時を検索',
	'To next page with this content' => 'この内容で次へ',
	'Expire date' => '有効期限',
	'Displaying condition' => '提示条件',
	'Applying condition' => '利用条件',
	'Please confirm the chosen menus' => '選択済みメニューを確認してください',
	'Nail off Choice' => 'ネイルオフの選択',
	// schedule
	'Total duration (approximate)' => '所要時間合計（目安）',
	'Store availability' => 'サロンの空き状況',//'お店の空き状況',
	'Staff availability' => 'スタッフ別空き状況',
	// detail
	'Booking name' => 'ご予約者名',
	'Phone number' => '緊急時ご連絡先',
	'Demand' => 'ご要望・ご相談',
	'Please provide more information' => 'お客様情報を入力してください',
	'200 zenkaku characters' => '全角 200 字',
	'Required' => '必須',
	'Is this your first time at this store?' => 'このサロンに行くのは初めてですか？',
	'Yes, this is my first time' => 'はい。初めてです',
	'No, I have been here before' => 'いいえ。以前行ったことがあります。',
	'Staff' => '指名スタッフ',
	'Total amount' => '合計金額',
	'Using points' => 'ご利用ポイント',
	'Current points' => '現在ポイント',
	'You will need to pay' => 'お支払い予定金額',
	'Please set number of points you want to spend' => 'ご利用ポイントを設定してください',
	// Staff schedule
	'Detail' => '詳細',
	// confirm
	'Please confirm your booking order' => 'ご予約内容の最終確認を行ってください',
	'Booking date time' => '来店日時',
	'Total time' => '所要時間(目安)',
	'Total execute time' => '所要合計',
	'You will have to pay' => 'お支払い予定金額',
	// errors
	'Invalid points used' => '入力したポイントが無効です。',
	'Invalid coupon(s) or product(s)' => 'クーポンまたは商品が無効です。',
	'Invalid staff or schedule requested' => 'スタッフまたは予約日が無効です。',
	// success
	'Booking success' => '予約完了',
	'Please use app or this site\'s Booking history page to view your booking history' => 'ご予約内容の確認はご予約された店舗専用アプリ、もしくは、当サイトの「マイページ」の「予約履歴一覧」ページをよりご確認ください。',
	'Thank you for completing your booking process' => 'ご予約ありがとうございました。',
	'Recommended apps' => 'ご予約店舗のアプリ紹介',
	'This store has distributed free useful apps. Hot news and limited coupons only available there. <br> Don\'t miss them out!' => 'ご予約を行った店舗では、店舗からのお知らせや、アプリ限定のクーポン、ポイントが受け取れる、<br> お得な専用アプリ(インストール無料)を配信しております。この機会に、ぜひご活用ください!',
	'To My Page' => 'イページへマイページ へ 移動する',
	// day name
	'Mon' => '月',
	'Tue' => '火',
	'Wed' => '水',
	'Thu' => '木',
	'Fri' => '金',
	'Sat' => '土',
	'Sun' => '日',
	'No product selected' => 'オプション選択なし',
	// pagination
	'Prev page' => '前へ',
	'Next page' => '次へ',
	'Current page' => 'ページ',
	'Select date time' => '日時を指定する',
    'Time of the authentication key input has exceeded the time limit.'  =>  '認証キー入力の時間が制限時間を超えています。',
];
